GCode Interface
=============
Background
----------------
This document describes the GCode interface. The GCode interface is used in files to provide the printer with instructions on how to print a certain file.
The GCode interface is provided as a file reading interface. A GCode file is a list of instructions for the printer to execute.

Base format
-----------------
The base file format is a plain text file with text lines in it. Line endings maybe line feed or carriage return+line feed. (LF, CRLF) for compatibility with both Unix and Windows style text output mode.
Every single line can be a single instruction for the printer to execute.
The origin for this interface is the [NIST RS274NGC G-code standard](http://www.nist.gov/manuscript-publication-search.cfm?pub_id=823374). However, the RS274NGC standard is not used as a base for this interface. As this standard describes many features and functions that are not applicable or implemented.

Note: To reduce file size, it is recommended to use Unix style (LR) line ends.

.gcode .g
---------
The base file format may have an file extension of .gcode or .g, this means it is a plain text file containing plain text gcode.

.gcode.gz .gcode.bz2 .gcode.xz
-------------
These extensions are a stream compressed file containing plain text gcode. Using these compression methods you can reduce the overal file size and thus storage and data transfer times requires for the file.
.gz uses the gzip algorithm and saves about 70% of the file size.
.bz2 uses the bzip2 algorithm and saves about 80% of the file size.
.xz uses the LZMA algorithm and saves about 90% of the file size.

Note: While LZMA provides the best compression ratio, it also takes significantly longer to compress. Gzip is the fastest compression method in a quick test.
Note: All 3 methods are well known and have common tools and have python implementation available.

Line format
=========
A line in [EBNF](https://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_Form):
```
line = [white space], {tag, [white space]}, [white space], [comment], newline;
tag = letter, [number];
comment = ";", {all characters};
newline = "\n" | "\r\n";
white space = " " | "\t";
number = ? float format ?;
letter = "A".."Z"|"a".."z"
all characters = ? all visible characters ? ;
```
The basic buildup is 2 parts. Tags and comments. Comments are ignored for actual instructions, but can provide meta-data.

Tags build up the actual instructions. The order of the tags is irrelevant. `G1 X100` is the same as `X100 G1`. The amount of white space between tags is irrelevant, but for readability a single space is usually used.

Tags are case sensitive, `G1` is not the same as `g1`

Commands
=========
Every line with a command should contain one of the command tags. There are 3 types of command tags:
 - G: Movement commands
 - M: Other commands
 - T: Tool change command

Every instruction line should only contain a single command tag, the rest of the tags are parameters for this command. If multiple command tags are discovered in a single instruction line, they have preference from top to bottom.
Example: A line with both a G and M tag will be executed as a G command. A line with an M and T command will be executed as a M command.

Note: This behavior of command preference is needed for certain M commands. Which have a T parameter.

Incorrect input
--------------------
When the machine discovers incorrect input, for example, none-existing commands, a parameters that are unused, then these are silently ignored.

G: Movement commands
===================
Movement commands are all defined as G[number] commands. These commands cause stepper motor movement.
G0, G1: Move head
---------
```
G0 X[position] Y[position] Z[position] E[position] F[speed]
G1 X[position] Y[position] Z[position] E[position] F[speed]
```
Move to a position. X/Y/Z are the position of the left nozzle tip in mm. Where X0 Y0 Z0 is the front left bottom corner of build volume.
E is the position of the material. This is an absolute position starting to count at 0.
F is the speed in mm per minute. If the speed is left out, the last used speed in the previous `G0` or `G1` is used. This is a single previous speed for both commands. A `G1` without a speed following a `G0` with a speed will use the speed of the `G0`.
**[TODO] Describe absolute/relative modes from G90/G91/M82/M83**
G4: Wait
--------------
```
G4 S[seconds]
G4 P[milliseconds]
```
The G4 command waits a certain amount of time till the next command will be processed. The wait will start after all previous moves have been finished.
This command accepts a time in seconds as well as in milliseconds. If both are provided, the time in seconds will be used.

G90: Absolute mode
------------------
Set the printer in absolute mode. See G0/G1 for details.

G91: Relative mode
------------------
Set the printer in absolute mode. See G0/G1 for details.

G92: Set current position
------------------
Set the current absolute position of axis.
```
G92 X0 Y0 Z0
```
Sets the current X/Y/Z position to 0,0,0
```
G92 E0
```
Set the current extruder position to 0.

G280: Prime extruder train for use
------------------
Prime the currently active extruder train (see T0/T1) at the current position. And reset the E value to zero afterwards.
```
G280
G280 F[speed in mm/sec]
```
**[currently not implemented]** Optionally a speed in mm/sec can be given, this is used to prime the extruder at a different priming speed then the default priming speed defined by the machine.


M: Other commands
=================

M104: Set extruder train target temperature
------------------
M109: Set extruder train target temperature, and wait till it is reached
------------------
M140: Set heated bed target temperature
------------------
M190: Set heated bed target temperature, and wait till it is reached
------------------
M106: Set print cooling fan speed
------------------
M107: Disable print cooling fan
------------------
M82: Absolute E mode
------------------
M83: Relative E mode
------------------
M201: Set maximum acceleration per axis
------------------
M204: Set default total acceleration
------------------
M205: Extra motion settings (minimum speed and jerk)
------------------
M302: Allow cold extrusion
------------------
M400: Wait till all planned moves have finished
------------------
Do not continue till all planned moves with G0/G1 have been finished.

T0, T1: Switch to extruder-train
================================
```
T0
T1
```
T0 or T1 switches the currently active extruder train to a different one. This might do movements in the machine if it needs to do an active switch (for example, activate a lifting mechanism). This means the X/Y position after the T0 or T1 of the head are undefined.

The E position is not modified and continues to be measured from the same position.
**[Implementation specific]** The E axis is put into absolute mode after this command, if it was not in absolute mode before this.
