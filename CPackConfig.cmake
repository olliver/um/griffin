set(CPACK_PACKAGE_VENDOR "Ultimaker")
set(CPACK_PACKAGE_CONTACT "Arjen Hiemstra <a.hiemstra@ultimaker.com>")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Ultimaker Embedded services")
set(CPACK_PACKAGE_VERSION_MAJOR 9999)
set(CPACK_PACKAGE_VERSION_MINOR 99)
set(CPACK_PACKAGE_VERSION_PATCH 99)
set(CPACK_GENERATOR "DEB")

set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE all)

set(DEB_DEPENDS
	"python3 (>= 3.4.0)"
	"python3-numpy (>= 1.8.0)"
	"python3-gi (>= 3.14.0)"
	"python3-dbus (>= 1.2.0)"
	"python3-serial (>= 2.6)"
	"python3-flask (>= 0.10.0)"
    "arcus (>= 15.05.90)"
    "dbus (>= 1.8.0)"
    "protobuf (>= 3.0.0)"
)
string(REPLACE ";" ", " DEB_DEPENDS "${DEB_DEPENDS}")
set(CPACK_DEBIAN_PACKAGE_DEPENDS ${DEB_DEPENDS})
set(CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA "${CMAKE_CURRENT_SOURCE_DIR}/system_config/postinst")

include(CPack)
