import logging
import logging.config
import sys
import os
import importlib

from griffin import dbusif
debug_file = "/var/lib/griffin/debug_level"

def readLogLevelFile(service_name):
    if not os.path.isfile(debug_file):
        with open(debug_file, "w") as f:
            f.write("#you can specify loggers as in an ini file " +
                    "#[logger_<filename>]" +
                    "#level=DEBUG\n\n")

    try:
        logging.config.fileConfig(debug_file, defaults={"loglevel": logging.INFO, "format": "%(levelname)-8s %(name)-15s %(message)s"})
    except:
        logging.basicConfig(format="%(asctime)-15s %(levelname)-8s %(name)-15s %(message)s", level=logging.INFO)

def startService(module_name, service_name, *args):
    log_level = readLogLevelFile(service_name)
    #logging.basicConfig(format="%(asctime)-15s %(levelname)-8s %(name)-15s %(message)s", level=log_level)
    if log_level == logging.DEBUG:
        logging.basicConfig(format="%(levelname)-8s %(name)-15s (%(filename)s/%(funcName)s): %(message)s", level=log_level)
    else:
        logging.basicConfig(format="%(levelname)-8s %(name)-15s %(message)s", level=log_level)

    logging.info("Creating service: %s", service_name)
    module = importlib.import_module(module_name)
    service = getattr(module, service_name)
    service(*args)
    logging.info("Starting service: %s", service_name)
    dbusif.runMainLoop()

if __name__ == "__main__":
    if len(sys.argv) > 2:
        startService(sys.argv[1], sys.argv[2], *sys.argv[3:])
    else:
        print("Call with: %s [module_name] [service_name] [args...]" % (sys.argv[0]))
