import re
import logging
import copy

from .material.materialExtrusionMonitor import MaterialExtrusionMonitor

log = logging.getLogger(__name__.split(".")[-1])


## Private class to represent a line of GCode.
#  This class can be used to get values behind "keys" in GCode lines.
class _GCodeLine:

    def __init__(self, line):
        self.__line = line

    ## Function used to extract a value behind a "key" in GCode lines.
    # @param key: String with the length of 1. For example "G"
    # @param default: Return value if the key is not found, or the type conversion fails.
    # @param return_type: Type to which the return value should be cast.
    # @return: Returns the value after the [key] parameter as found in the line given during construction.
    #          If the key is not found, or type conversion fails, the default value is returned.
    def getValue(self, key, *, default=None, return_type=float):
        res = re.search("%s(-?[0-9\.]+)" % key, self.__line)
        if res is None:
            return default
        try:
            return return_type(res.group(1))
        except ValueError:
            return default

    ## Return all parameters after the initial command.
    #  This assumes that the line of gcode is in the format of "M115 Data"
    #  @return string: Containing all parameters.
    def getAllParameters(self):
        if " " not in self.__line:
            return ""
        return self.__line.split(" ", 1)[1]

    ## Get the complete line that is stored in this GCodeLine object.
    #  @return string: The complete line of gcode.
    def getLine(self):
        return self.__line


## The GCode processor.
#  This class keeps track of the latest used position in GCode,
#  has the option to run specific functions on G/M/T commands (GCodeMapping)
#  and keeps track of the current filament position and amount of retraction per hotend.
#  Every G/M/T command that has no mapping is passed to the driver.
class GCodeProcessor:
    def __init__(self, controller, driver):
        self.__controller = controller
        self.__driver = driver
        # parses X0.4 etc. [XYZEF] followed by a digit (floating point or int)
        self.__track_axis_position_expression = re.compile("([XYZEF])(-?\d+(?:\.\d+)?|-?\.\d+)", flags=re.MULTILINE)

        self.__gcode_function_mapping = {}

        hotend_count = controller.getPropertyValue("hotend_count")

        self.__relative_positioning_mode = False
        self.__position = {"X": 0, "Y": 0, "Z": 0, "F": 1200}
        self.__position.update({"E%d" % i: 0 for i in range(0, hotend_count)})

        # is not needed as it should be set to 1. Flow compensation change could then also be applied to retracted length.
        self.__retracted_length = [None] * hotend_count
        self.__hotend_extruded_length = [0] * hotend_count
        self.__material_extrusion_monitors = [None] * hotend_count
        for hotend_index in range(0, hotend_count):
            self.__material_extrusion_monitors[hotend_index] = MaterialExtrusionMonitor(controller, hotend_index)

    def initialize(self):
        length = self.__controller.getPropertyValue("idle_retraction_amount") + self.__controller.getPropertyValue("deprime_retraction_amount")
        for hotend_nr in range(self.__controller.getPropertyValue("hotend_count")):
            self.__retracted_length[hotend_nr] = 0 if self.__controller.getPrintHeadController().getHotendSlot(hotend_nr).getPropertyValue("is_primed") else length

    ## Add a new function that will be executed on a certain GCode command
    # @param code: A G/M/T code in the form of [Tag][number]. Example G28
    # @param function: A function that will be executed when this code is queued. The function will be run with a GCodeLine object as parameter.
    def addGCodeMapping(self, code, function):
        assert code not in self.__gcode_function_mapping
        self.__gcode_function_mapping[code] = function

    ## Process a gcode line, and queue it if it does not have a mapping.
    #  The line of GCode is parsed for position tracking as well as GCode mapping handling.
    #  @param gcode_line_string: A string containing GCode.
    def processAndQueueGCode(self, gcode_line_string):
        source = "processAndQueueGCode"
        # Strip comments from the line so we can better parse it, and never waste time sending comments to the driver.
        if ";" in gcode_line_string:
            gcode_line_string = gcode_line_string[:gcode_line_string.find(";")]
        gcode_line_string = gcode_line_string.strip()

        gcode = _GCodeLine(gcode_line_string)

        g_value = gcode.getValue("G", default=None, return_type=int)
        if g_value is not None:
            if self.__runMapping("G%d" % (g_value), gcode):
                return
            else:
                self.__testAndMaybeTrackPosition(gcode_line_string, g_value=g_value, source=source)
        else:
            m_value = gcode.getValue("M", default=None, return_type=int)
            if m_value is not None:
                if self.__runMapping("M%d" % (m_value), gcode):
                    return
            else:
                t_value = gcode.getValue("T", default=None, return_type=int)
                if t_value is not None:
                    if self.__runMapping("T%d" % (t_value), gcode):
                        return

        if len(gcode_line_string) > 0:
            self.__driver.queueNormal(gcode_line_string)

    ## Send an line of GCode to Marlin ASAP.
    #  This function tracks the position of the GCode to make sure we know what the last position of the head is.
    #  @param gcode_line: a string containing GCode
    #  @param wait: If wait is True, then this function will block and wait for the reply from Marlin and return this reply.
    #  @return: empty string if wait=False
    #  @return: Reply from Marlin if wait=True
    def sendInstantRawGCode(self, gcode_line, wait=False):
        self.__testAndMaybeTrackPosition(gcode_line, False, source="sendInstantRawGCode")
        if wait:
            return self.__driver.queueAndWaitForReply(gcode_line)
        else:
            self.__driver.queueInstant(gcode_line)
            return ""

    ## Send an line of GCode to Marlin ASAP.
    #  This function tracks the position of the GCode to make sure we know what the last position of the head is.
    #  This function will not block if the queue is full.
    #  @return bool: Returns true if the command is successfully queued. False if the queue is full and thus cannot be queued.
    def sendInstantRawGCodeNoBlock(self, gcode_line):
        if self.__driver.queueInstant(gcode_line, allow_block=False):
            self.__testAndMaybeTrackPosition(gcode_line, False, source="sendInstantRawGCodeNoBlock")
            return True
        return False

    ## Process a gcode line, and queue it. Do not run any GCode mappings.
    #  The line of GCode is parsed for position tracking. GCode mapping functions are bypassed.
    #  @param gcode_line_string: A string containing GCode.
    #  @param track_e: Whether or not to track E-movements
    def queueRawGCode(self, gcode_line, track_e=True):
        self.__testAndMaybeTrackPosition(gcode_line, source="queueRaw", track_e=track_e)
        self.__driver.queueNormal(gcode_line)

    ## Private function to run the gcode mapping of a GCode line.
    #  @param code: The code in string form, example "G28"
    #  @param gcode: A GCodeLine object containing the full line of GCode that was processed.
    def __runMapping(self, code, gcode):
        if code in self.__gcode_function_mapping:
            try:
                self.__gcode_function_mapping[code](gcode)
            except:
                log.exception("Exception while trying to run mapping for gcode: %s", gcode.getLine())
            return True
        return False

    # Test if the gcode line needs tracing and then calls the _trackPosition() function if it does.
    # @param line the line of gcode to track
    # @param track_e track E movements as well as X, Y and Z? It is useful that e movements in the maintenance functions
    # aren't tracked otherwise filling the bowden tube would cause material to be consumed as tracked in the hotend eeprom.
    # @param source The source of the movement for debugging.
    # @param g_value Optional parameter, can be parsed by this function; The parsed value that comes after G, or None if there is no G in the line.
    def __testAndMaybeTrackPosition(self, gcode_line, track_e=True, source=None, g_value="NOT_SET"):
        if g_value == "NOT_SET":
            g_value = _GCodeLine(gcode_line).getValue("G", default=None, return_type=int)

        if g_value is not None:
            if g_value in (0, 1, 92):
                self.__trackPosition(gcode_line, track_e, source)
            elif g_value == 91:
                log.debug("tracking now in RELATIVE positioning mode from {source}".format(source=source))
                self.__relative_positioning_mode = True
            elif g_value == 90:
                log.debug("tracking now in ABSOLUTE positioning mode from {source}".format(source=source))
                self.__relative_positioning_mode = False

    # tracks the position of all axis by parsing the Gcode put into the queue.
    # This means position is now tracked at 3 locations:
    # 1) top of the queue in griffin (aka here)
    # 2) planner position top of the queue in marlin
    # 3) stepper position = the closest to the actual position.
    #
    # Positions 2 and 3 can be requested using a M114.
    #
    # @param line the line of gcode to track
    # @param track_e track E movements as well as X, Y and Z? It is useful that e movements in the maintenance functions
    # aren't tracked otherwise filling the bowden tube would cause material to be consumed as tracked in the hotend eeprom.
    # @param source The source of the movement for debugging.
    def __trackPosition(self, line, track_e=True, source=None):
        e = self.__controller.getActiveHotend()
        if e is None:
            e = 0   # default e value if no nozzle switch has happened before.

        if self.__driver.getQueueSize() >= 1 and "Instant" in str(source):
            log.warning("Detected insertion of movement command(%s) in instant queue while the normal queue is not empty!", line)

        # G0 G1 are same G92 only works in absolute mode
        # but it is obviously useless to use G92 in relative mode
        for axis, pos in self.__track_axis_position_expression.findall(line):
            position = float(pos)

            if axis == "E":
                axis += str(e)  # E0, E1, ....

            if "G92" not in line:   # no e tracking and no abs/relative correction.
                if self.__relative_positioning_mode:
                    delta_position = position
                else:   # !relative_mode: AKA absolute
                    delta_position = position - self.__position[axis]

                if track_e and axis.startswith("E"):
                    # compensate for flow
                    delta_e = delta_position * self.__controller.getPropertyValue("extrusion_amount_modifier") / 100.0
                    self.__hotend_extruded_length[e] += delta_e
                    self.__retracted_length[e] -= delta_e
                    log.debug("e tracking E%d to retracted length %f because delta %f from %s", e, self.__retracted_length[e], delta_e, source)

                    # Ensure retracted_length is positive.
                    if self.__retracted_length[e] < 0:
                        log.debug("e tracking E%d to 0 because clamping from %s", e, source)
                        self.__retracted_length[e] = 0.0

                    # track only when track_e is true
                    self.__position[axis] += delta_position
                    self.__material_extrusion_monitors[e].updateDelta(delta_e)
                elif not axis.startswith("E"):
                    self.__position[axis] += delta_position # e is tracked seperately
            else:
                self.__position[axis] = position

    def getRetractedLengthForHotend(self, hotend_nr):
        return self.__retracted_length[hotend_nr]

    def setRetractedLength(self, extruder_nr, length):
        log.debug("setRetractedLength() E%d to %f", extruder_nr, length)
        self.__retracted_length[extruder_nr] = length

    def getAndResetFilamentUsageForHotend(self, hotend_nr):
        extruded_mm = self.__hotend_extruded_length[hotend_nr]
        extruded_cm = int(extruded_mm / 10)     # round down to cm

        # If we have extruded (can also be negative from retractions).
        if extruded_cm > 0:
            # Reset internal extrusion counter centimeters but keep the milimeters.
            self.__hotend_extruded_length[hotend_nr] -= extruded_cm * 10
            return extruded_cm
        else:
            return 0

    def getAxes(self):
        return self.__position.keys()

    def getPositionArray(self, f=False):
        position = [None] * 3
        axis_to_position_in_array_map = {"X": 0, "Y": 1, "Z": 2}
        if f:
            position.append(None)
            axis_to_position_in_array_map.update({"F": 3})

        for map_key in axis_to_position_in_array_map:
            if self.getPosition(map_key) == None:
                position[axis_to_position_in_array_map[map_key]] = float(0)
            else:
                position[axis_to_position_in_array_map[map_key]] = float(self.getPosition(map_key))
        return position

    # returns position either as dictionary if no axis argument is given or as value belonging to the axis requested.
    def getPosition(self, axis=None):
        if axis is None:
            return copy.deepcopy(self.__position)

        try:
            return self.__position[axis]
        except KeyError:
            return None
