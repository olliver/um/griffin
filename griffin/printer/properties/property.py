from griffin import signal
import time


## Base class for properties in the printer service.
#  Each property has a unique key as identifier.
#  Properties are held by a PropertyContainer for the lifetime of the service.
#  Each property can be read to get its value.
#  Each property has a set function, which can be used to set the value of this property.
#  Set function return true on success, or false on failure. As not all values are allowed for all settings.
class Property:
    def __init__(self, key):
        self.__key = key
        self._validator_functions = []
        # onChange signal, gets two parameters. First one this Property, second is the new value.
        self.onChange = signal.Signal()
        self._last_change = None
        self.onChange.connect(self._valueChanged)

    ## Get the key (unique name) of this property, which is set during construction of this object.
    #  @return: The key of this property.
    def getKey(self):
        return self.__key

    ## The get function of a Property. This is not implemented in the base class and should be overridden by all subclasses.
    def get(self):
        raise NotImplementedError

    ## The getDefault function of a Property. This is not implemented in the base class and should be overridden by all subclasses.
    def getDefault(self):
        raise NotImplementedError

    ## The set function of a Property. This is not implemented in the base class and should be overridden by all subclasses.
    # @param value: New value to set this property to.
    def set(self, value):
        raise NotImplementedError

    ## This function checks whether the value is valid and can be set.
    # @param value The value that is to be tested and only set when this function returns true.
    # @return True or False
    def _isValid(self, value):
        for validator in self._validator_functions:
            if not validator(value):
                return False
        return True

    ## This function allows the user to add a constraint to the values that can be set.
    # @param validator_function A function pointer that takes one argument, the value to be set/constrained.
    def addConstraint(self, validator_function):
        self._validator_functions.append(validator_function)

    ## @return The time (in seconds since 1970) the value was set last or None.
    def getLastChanged(self):
        return self._last_change

    ## Callback of the onchange signal to log the time the property changed, since it is the first event handler added it is automatically the first one called.
    def _valueChanged(self, property, value):
        property._last_change = time.time() # non monotonic time because if we have proper time via the network then we should use it!
