import copy

from .property import Property


## Read only variant of a property. The value of this property is set at construction and cannot be modified afterwards.
#
# This object is used to set machine specific properties during initialization of the printer service.
class ReadOnlyProperty(Property):
    def __init__(self, key, value):
        super().__init__(key)
        self._value = value

    # @return the constant value of this property.
    def get(self):
        # As this value is created from json, it could be a complex type (dict, list) copy it so it cannot be modified.
        return copy.deepcopy(self._value)

    ## Get the default value for this property
    # @return: The default value set during the creation of this property.
    def getDefault(self):
        return self.get()

    ## Try to set this property value, which is not allowed.
    #  This function can be called through DBus, and thus we return false to indicate that we cannot set this property.
    #  @param value: Ignored, required by base class.
    #  @return False, as we can never set this property.
    def set(self, value):
        return False
