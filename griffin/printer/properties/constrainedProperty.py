import logging

from .basicProperty import BasicProperty

log = logging.getLogger(__name__.split(".")[-1])


## Variant of the Basic property. This property can be constrained to only allow certain values by a validator functions.
#
#  This object is used to track runtime values that can be set by various externals. But need to be constrained to certain values.
class ConstrainedProperty(BasicProperty):
    def __init__(self, key, default_value, validator_function):
        super().__init__(key, default_value)
        self.addConstraint(validator_function)

    ## Set the property to a value.
    #  This will validate the value with the function given in the constructor of this object.
    #  If the value is valid according to that validator, it is set.
    #  @param value: New value to store in this property.
    #  @return bool: True on success, False on failure.
    def set(self, value):
        try:
            if not self._isValid(value):
                return False
        except Exception:
            log.exception("Validator function threw an exception for property '%s' when setting to value '%r'", self.getKey(), value)
            return False
        return super().set(value)
