## Properties module
#  This module provides classes to build properties on.
#  The printer service controller holds a set of properties.
#  The printer service controller is responsible creating the properties.
#  Properties should not be created at runtime, only during the creation of the controller.
#  Each property is defined with a unique name.
#  Each property have set and get functions.
#  Setting a property to a certain value could return failure (invalid value, read-only)
#  Properties can optionally be made none-volatile by storing them in the preference file.
