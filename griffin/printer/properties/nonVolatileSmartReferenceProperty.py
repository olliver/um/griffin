from .nonVolatileProperty import NonVolatileProperty
from threading import RLock


## A property that can be read and set. And is backed by a changing registry.
#  It solves the problem of changing hardware backed by changing storage locations.
#  If this registry is (backed by) a FileRegistry then this property will be stored on disk and read on a reboot.
class NonVolatileSmartReferenceProperty(NonVolatileProperty):
    def __init__(self, base_registry, key, default_value):
        super().__init__(None, key, default_value)
        self._base_registry = base_registry
        self._lock = RLock() # This lock is on the reference and the change event triggered by changing the reference.

    def referenceChanged(self, reference):
        with self._lock:
            old_value = self.get()
            if reference is not None:
                self._registry = self._base_registry.getAsRegistry(reference)

                try:
                    self.get() # get of an empty reference gives an AttributeError.
                except AttributeError:
                    self._setDefaults()
            else:
                self._registry = None

            # some code to fire the onchange handler if the reference change has changed the value.
            new_value = self.get()
            if old_value != new_value:
                self.onChange.emit(self, new_value)

    ## Get the value of this property.
    #  @return: The value stored in this property.
    def get(self):
        with self._lock:
            if self._registry is not None:
                return super().get()
        return ""

    ## Set the value of this property. This property is stored in the registry on the set.
    # @param value: New value to store.
    # @return True, to indicate success.
    def set(self, value):
        with self._lock:
            if self._registry is not None:
                return super().set(value)
        return False
