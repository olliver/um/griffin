import logging

from griffin.printer.properties.property import Property
from griffin import signal

log = logging.getLogger(__name__.split(".")[-1])


## A container that contains Property instances, can be queried for all properties and fires onChanged events when one or more properties change in value.
class PropertyContainer:
    def __init__(self):
        self.__properties = {}

        # Signal which is fired when one of the properties inside of this container is changed.
        self.onChange = signal.Signal()

    ## Add a new property to the PropertyContainer.
    #  @param property: an instance of subclass of the Property class
    def addProperty(self, property):
        # Check if the property is really a subclass of the Property base class, and the key is not in this container.
        assert isinstance(property, Property)
        assert property.getKey() not in self.__properties

        # Attach ourselves to the onChange event of the property to fire our general changed. Use a lambda to capture the property reference.
        self.__properties[property.getKey()] = property
        property.onChange.connect(self.__propertyChangedCallback)
        self.__propertyChangedCallback(property, property.get())

    ## Retrieve a property instance by it's key
    #  @param key: The key of the property to fetch.
    #  @return a Property instance
    #  @return None if no property with that key is found
    def get(self, key):
        return self.__properties.get(key, None)

    ## Check if a property with a key exists.
    #  @param key: The key of the property to check for existence.
    #  @return bool, True if the property key exists, alse otherwise.
    def hasKey(self, key):
        return key in self.__properties

    ## Set a value of a property by the key of the property.
    #  @param key: The key of the property to fetch.
    #  @return bool, True if the property was changed. False if the property does not exists or cannot be set to this value.
    def setPropertyValue(self, key, value):
        property = self.__properties.get(key, None)
        if property is None:
            return False
        return property.set(value)

    ## Get all the properties as a dictionary, with the property key as the dictionary key, and the properties current value as the value for this key.
    #  @return dict containing all key->value properties in this container.
    def getPropertiesAsKeyValueDictionary(self):
        result = {}
        for key, property in self.__properties.items():
            result[key] = property.get()
        return result

    ## Callback when one of the properties in the PropertyContainer fires it onChange event.
    #  @param property: an instance of the Property class
    #  @param value: the new value of the property
    def __propertyChangedCallback(self, property, value):
        self.onChange.emit(property, value)
