from .property import Property


## Basic variant of the property. This property can be set to any value, and will fire an onChange event in this case.
#
#  This object is used to track various simple runtime values during operation.
class BasicProperty(Property):
    def __init__(self, key, default_value):
        super().__init__(key)
        self.__value = default_value
        self.__default = default_value

    ## Get the value of this property.
    #  @return: The value stored in this property.
    def get(self):
        return self.__value

    ## Get the default value for this property
    # @return: The default value set during the creation of this property.
    def getDefault(self):
        return self.__default

    ## Set the property to a value.
    #  @param value: New value to store in this property. Will emit a change event if the value is changed.
    #  @return bool: True on success, False on failure.
    def set(self, value):
        if not self._isValid(value):
            return False
        elif self.__value == value:
            return True
        self.__value = value
        self.onChange.emit(self, value)
        return True
