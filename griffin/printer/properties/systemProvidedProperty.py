from .readOnlyProperty import ReadOnlyProperty


## The system provided property is a form of a ReadOnly property, with the difference that it will be updated by the system.
#
#  The normal set function will always report failure, however, the system can update the value by using the setBySystem function
class SystemProvidedProperty(ReadOnlyProperty):
    def __init__(self, key, value):
        super().__init__(key, value)

    ## Set the value of this property from within the system.
    #  The end user should never be able to call this function by any means, and the programmer should take extra care that there is
    #  only a single entity setting this value.
    def setBySystem(self, value):
        if self._value == value:
            return
        self._value = value
        self.onChange.emit(self, value)
