import logging

from ..property import Property
from griffin.printer import faultHandler


log = logging.getLogger(__name__.split(".")[-1])


## The printer state property.
# This property updates itself with new printer states as soon as the state of the printer changes.
class PrinterStateProperty(Property):
    # available printer states
    IDLE = "idle"               #< The machine is not doing anything and can be used.
    PRINTING = "printing"       #< The machine is busy with a print job. (see job state for paused, etc...)
    ERROR = "error"             #< The machine is in a critical error and can no longer operate.
    MAINTENANCE = "maintenance" #< The machine is running maintenance actions.
    BOOTING = "booting"         #< The machine is still starting up and not available for use yet.

    ## Create the PrinterState property
    # @param controller: controller is passed as parameter instead of retrieved as singleton to prevent recursive imports.
    # @param key: key at which this property can be accessed from it's container.
    def __init__(self, controller, key):
        super().__init__(key)
        self.__state = self.BOOTING
        self.__fault_handler = faultHandler.FaultHandler.getInstance()
        self.__controller = controller

        self.__fault_handler.onError.connect(self.__updatePrinterState)
        controller.onProcedureStart.connect(self.__updatePrinterState)
        controller.onProcedureFinished.connect(self.__updatePrinterState)
        controller.getDriver().onStatusChanged.connect(self.__updatePrinterState)
        controller.getPrintHeadController().getHotendCartridgeManager().onInitialized.connect(self.__updatePrinterState)

    # @return the current printer state
    def get(self):
        return self.__state

    ## Try to set this property value, which is not allowed.
    #  @param value: Ignored, required by base class.
    #  @return False, as we can never set this property.
    def set(self, value):
        return False

    ## Private function which is called when something in the printer service changes and the state of the printer needs to be re-evaluated.
    #  @param *args: so all arguments can be given to this function which will be ignored. Makes it easier to attach different signals to this function.
    def __updatePrinterState(self, *args):
        err = self.__fault_handler.getError()
        # get just the procedure keys
        procedures = self.__controller.getAllProcedures()
        running_procedures = []
        for procedure in procedures:
            step = procedure.getActiveStep()
            if step is not None and procedure.isActive():
                running_procedures.append(procedure)

        if err.getLevel() <= faultHandler.FaultLevel.ERROR:
            state = self.ERROR
        elif len([p for p in running_procedures if p.isBootProcedure()]):
            state = self.BOOTING
        elif not self.__controller.getDriver().isConnected():
            # Bit of complexity here. When the controller is not yet properly connected we will report booting if that was the previous state.
            # Else we report an error condition, as we disconnected after we where booted properly.
            if self.__state != self.BOOTING:
                state = self.ERROR
            else:
                state = self.BOOTING
        elif not self.__controller.getPrintHeadController().getHotendCartridgeManager().isInitialized():
            # Remain in BOOTING until the cartridges have been initialized.
            state = self.BOOTING
        elif len([p for p in running_procedures if p.isPrintProcedure()]):
            state = self.PRINTING
        elif len([p for p in running_procedures if p.isMaintenanceProcedure()]):
            state = self.MAINTENANCE
        else:
            state = self.IDLE

        if state != self.__state:
            log.info("%s -> %s", self.__state, state)

            self.__state = state
            self.onChange.emit(self, self.__state)
