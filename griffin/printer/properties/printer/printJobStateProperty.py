import logging

from ..property import Property


log = logging.getLogger(__name__.split(".")[-1])


## The printer state property.
# This property updates itself with new printer states as soon as the state of the printer changes.
class PrintJobStateProperty(Property):
    # Possible job states
    NO_JOB = "none"
    PRINTING = "printing"
    PAUSING = "pausing"
    PAUSED = "paused"
    RESUMING = "resuming"
    PRE_PRINT = "pre_print"
    POST_PRINT = "post_print"
    WAIT_CLEANUP = "wait_cleanup"

    ## Create the PrinterState property
    # @param controller: controller is passed as parameter instead of retieved as singleton to prevent recursive imports.
    # @param key: key at which this property can be accessed from it's container.
    def __init__(self, controller, key):
        super().__init__(key)
        self.__state = self.NO_JOB
        self.__controller = controller
        self.__controller.onPauseChanged.connect(self.__updatePrintJobState)

        self.__print_procedure = controller.getProcedure("PRINT")
        self.__print_procedure.onStart.connect(self.__updatePrintJobState)
        self.__print_procedure.onNextStep.connect(self.__updatePrintJobState)
        self.__print_procedure.onFinished.connect(self.__updatePrintJobState)

        self.__pause_procedure = controller.getProcedure("PAUSE_PRINT")
        self.__pause_procedure.onStart.connect(self.__updatePrintJobState)
        self.__pause_procedure.onFinished.connect(self.__updatePrintJobState)

        self.__resume_procedure = controller.getProcedure("RESUME_PRINT")
        self.__resume_procedure.onStart.connect(self.__updatePrintJobState)
        self.__resume_procedure.onFinished.connect(self.__updatePrintJobState)

    # @return the current printer state
    def get(self):
        return self.__state

    ## Try to set this property value, which is not allowed.
    #  @param value: Ignored, required by base class.
    #  @return False, as we can never set this property.
    def set(self, value):
        return False

    ## Private function which is called when something in the printer service changes and the state of the printer needs to be re-evaluated.
    #  @param *args: so all arguments can be given to this function which will be ignored. Makes it easier to attach different signals to this function.
    def __updatePrintJobState(self, *args):
        active_step = self.__print_procedure.getActiveStep()
        new_state = self.NO_JOB

        if active_step is not None:
            active_step = active_step.getKey()

            if active_step == "RUN_PRE_PRINT_SETUP":
                new_state = self.PRE_PRINT
            elif active_step == "PRINTING":
                if self.__pause_procedure.isActive():
                    new_state = self.PAUSING
                elif self.__resume_procedure.isActive():
                    new_state = self.RESUMING
                elif self.__controller.getPaused():
                    new_state = self.PAUSED
                else:
                    new_state = self.PRINTING
            elif active_step == "RUN_POST_PRINT":
                new_state = self.POST_PRINT
            elif active_step == "WAIT_FOR_CLEANUP":
                new_state = self.WAIT_CLEANUP

        if new_state != self.__state:
            log.info("New print job state: %s -> %s", self.__state, new_state)

            self.__state = new_state
            self.onChange.emit(self, new_state)
