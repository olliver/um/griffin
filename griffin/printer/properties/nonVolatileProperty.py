from .property import Property
import time

## A property that can be read and set. And is backed by a registry.
#  If this registry is a FileRegistry then this property will be stored on disk and read on a reboot.
class NonVolatileProperty(Property):
    __NVM_PROPERTY_VERSION = 0.1

    def __init__(self, registry, key, default_value):
        super().__init__(key)
        self._registry = registry
        self.__default = default_value

        # If the default is not set in the registry, set it, so we only emit 1 warning, not a warning with every get()
        if self._registry is not None:
            retrieved_value = self._registry.get(self.getMangledKey(), None)
            if retrieved_value is None:
                retrieved_value_old = self._registry.get(self.getKey(), None)
                if retrieved_value_old is not None:
                    self._registry.set(self.getMangledKey(), {"value": retrieved_value_old, "last_changed": -1, "version": self.__NVM_PROPERTY_VERSION})
                else:
                    self._setDefaults()

    ## set the default value so there is something to be gotten when you do a get or set.
    def _setDefaults(self):
        self._registry.set(self.getMangledKey(), {"value": self.__default, "last_changed": time.time(), "version": self.__NVM_PROPERTY_VERSION})

    ## The property key is mangled for backwards/forwards compatibility
    def getMangledKey(self):
        return self.getKey() + "_PROPERTY"

    ## Get the value of this property.
    #  @return: The value stored in this property.
    def get(self):
        return self._registry.get(self.getMangledKey()).get("value", self.__default) # should already be set so default should be superfluous.

    ## Get the default value for this property
    # @return: The default value set during the creation of this property.
    def getDefault(self):
        return self.__default

    ## @return The time the value was set last or None.
    # @note overridden by this class as the leading time now comes from what is written in the file.
    def getLastChanged(self):
        return self._registry.get(self.getMangledKey()).get("last_changed", -1)

    ## Set the value of this property. This property is stored in the registry on the set.
    # @param value: New value to store.
    # @return True, to indicate success.
    def set(self, value):
        if not self._isValid(value):
            return False
        if self.get() == value:
            return True
        self._registry.set(self.getMangledKey(), {"value": value, "last_changed": time.time(), "version": self.__NVM_PROPERTY_VERSION})
        self.onChange.emit(self, value)
        return True
