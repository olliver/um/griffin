import logging
import time
from .controller import Controller
from .serviceWithProperties import DBusServiceWithProperties
from griffin import dbusif
from griffin import dataLogger
from griffin.timer import Timer
from .faultHandler import FaultHandler, FaultCode

log = logging.getLogger(__name__.split(".")[-1])


## Printer service DBus object.
#  This ServiceObject will create the DBus interface which the rest of the system can use to talk to the printer service.
#  This object acts as a interface wrapper around the rest of the printer service.
class PrinterService(DBusServiceWithProperties):
    def __init__(self):
        self._controller = Controller.getInstance()

        super().__init__(self._controller.getPropertyContainer(), "printer")

        self._fault = FaultHandler.getInstance()
        self._fault.onError.connect(self._onError)
        self._controller.onProcedureStart.connect(self._onProcedureStart)
        self._controller.onProcedureNextStep.connect(self._onProcedureNextStep)
        self._controller.onProcedureFinished.connect(self._onProcedureFinished)
        self._controller.getPrintHeadController().getHotendCartridgeManager().onHotendDataReady.connect(self.onCartridgeDataReady)

        # Create service objects for the heated bed, and hotends which serve properties
        DBusServiceWithProperties(self._controller.getBed().getPropertyContainer(), "printer", "printer/bed")
        print_head_controller = self._controller.getPrintHeadController()
        head_index = 0
        DBusServiceWithProperties(print_head_controller.getPropertyContainer(), "printer", "printer/head/%d" % (head_index))
        for n in range(0, self._controller.getPropertyValue("hotend_count")):
            DBusServiceWithProperties(print_head_controller.getHotendSlot(n).getPropertyContainer(), "printer", "printer/head/%d/slot/%d" % (head_index, n))

        ## Add callback to dbus printer service which implements the call to the resetToFactorySettings
        dbusif.RemoteObject("system").addFactoryResetCallback("printer", "resetSettings")

        self._controller.onExtrusionAmountChanged.connect(self.onExtrusionAmountChanged)

        # the next 2 lines may be removed at any time, it is just meant not to show the factory calibration every time so the normal user experience can be tested.
        if self._controller.getPreference("FIRST_RUN_COMPLETED"):
            self._controller.setPreference("FACTORY_SETUP_WIZARD", True)

        self.__checkSetupWizards()

    ## This function checks whether either of the _setup wizards should be ran.
    def __checkSetupWizards(self):
        factory_setup_wizard_completed = self._controller.getPreference("FACTORY_SETUP_WIZARD")
        welcome_setup_wizard_completed = self._controller.getPreference("WELCOME_SETUP_WIZARD")
        log.info("FACTORY_SETUP_WIZARD completed: %s" % factory_setup_wizard_completed)
        log.info("WELCOME_SETUP_WIZARD completed: %s" % welcome_setup_wizard_completed)
        if not factory_setup_wizard_completed:
            # Start the factory setup wizard from a timer thread. This because the startProcedure can block when Marlin isn't connected yet.
            Timer("_runFactorySetup ", 0, self._runFactorySetup ).setThreaded(True).start()
        elif welcome_setup_wizard_completed == False:
            # Start the welcome setup wizard from a timer thread. This because the startProcedure can block when Marlin isn't connected yet.
            Timer("_runWelcomeSetup", 0, self._runWelcomeSetup).setThreaded(True).start()

    def _onProcedureStart(self, procedure, step):
        self.onProcedureStart(procedure.getKey(), step.getKey())

    def _onProcedureNextStep(self, procedure, step):
        self.onProcedureNextStep(procedure.getKey(), step.getKey())

    def _onProcedureFinished(self, procedure):
        self.onProcedureFinished(procedure.getKey())

    @dbusif.method("sa{sv}", "b")
    def startProcedure(self, key, parameters):
        return self._controller.startProcedure(key, parameters)

    @dbusif.method("", "a(sb)")
    def getAvailableProcedures(self):
        procedures = self._controller.getAllProcedures()
        return list(map(lambda p: (p.getKey(), p.canBeExecuted()), procedures))

    @dbusif.method("ss", "b")
    def messageProcedure(self, procedure_key, value):
        log.info("Message procedure: %s: %s", procedure_key, value)
        try:
            procedure = self._controller.getProcedure(procedure_key)
            if procedure is None:
                log.error("Procedure is None")
                return False
            if not procedure.isActive():
                log.error("Procedure not active")
                return False
            procedure.receiveMessage(value)
        except:
            log.exception("Exception handling message")
            return False

        return True

    @dbusif.method("", "a(ss)")
    def getActiveProcedures(self):
        return self._controller.getActiveProcedures()

    @dbusif.method("s", "a{sv}")
    def getProcedureMetaData(self, key):
        try:
            procedure = self._controller.getProcedure(key)
            if procedure is None:
                return {}
            return procedure.getMetaData()
        except Exception:
            log.exception("Exception in getMetaData")
            return {}

    @dbusif.signal("ss")
    def onProcedureStart(self, procedure_key, step_key):
        log.info("Procedure start: %s: %s", procedure_key, step_key)
        return True

    @dbusif.signal("s")
    def onProcedureFinished(self, procedure_key):
        log.info("Procedure finished: %s", procedure_key)
        if procedure_key.endswith("_SETUP_WIZARD"):
            self.__checkSetupWizards()
        return True

    def _runFactorySetup(self):
        while len([p for p in self._controller.getAllProcedures() if p.isActive() and p.isMaintenanceProcedure()]) > 0:
            time.sleep(0.1)
        self.startProcedure("FACTORY_SETUP_WIZARD", {})

    def _runWelcomeSetup(self):
        while len([p for p in self._controller.getAllProcedures() if p.isActive() and p.isMaintenanceProcedure()]) > 0:
            time.sleep(0.1)
        self.startProcedure("WELCOME_SETUP_WIZARD", {})

    @dbusif.signal("ss")
    def onProcedureNextStep(self, procedure_key, step_key):
        log.info("Procedure next step: %s: %s", procedure_key, step_key)
        return True

    ## Dbus signal which is called when a hotend cartridge data is updated.
    #  This is called after the onCartridgePresentChanged, after the hotendCardridgeManager has read the data.
    #  Actual data can be read with getHotendCartridgeProperty
    @dbusif.signal("i")
    def onCartridgeDataReady(self, hotend_nr):
        log.debug("hotend %d data is ready", hotend_nr)
        return True

    @dbusif.method("", "yqsa{sv}")
    def getError(self):
        return self._fault.getError().getErrorRepr()    # returns a fault object

    @dbusif.method("yq", "")
    def clearError(self, level, code):
        self._fault.clear(level, code)

    def _onError(self, fault_object):
        return self.onError(*fault_object.getErrorRepr())

    # DBus signal that is raised when a new top level fault is in the system.
    # This is linked to the fault handler, that triggers the onError when a new fault is added, or a previous fault is cleared but a lower level fault is still in the system.
    @dbusif.signal("yqsa{sv}")
    def onError(self, error_level, error_code, error_message, data):
        return True

    ## @brief Gets the XY offset indexes for the right nozzle
    #  @return Returns a dictionary for x and y offset indexes
    @dbusif.method("", "a{sv}")
    def getXyOffsetIndexes(self):
        return self._controller.getPrintHeadController().getNozzleOffsetIndexes(1)

    # TODO: Will be removed in  EM-284 EM-556
    @dbusif.method("is", "v")
    def getHotendCartridgeProperty(self, hotend_nr, key):
        value = self._controller.getPrintHeadController().getHotendCartridgeProperty(hotend_nr, key)

        if value is None:   # DBus cannot encode None, so return an empty string in this case.
            return ""
        return value

    # TODO: Will be removed in  EM-284
    @dbusif.method("i", "s")
    def getHotendCartridgeSerial(self, hotend_nr):
        value = self._controller.getPrintHeadController().getHotendCartridgeManager().getSerial(hotend_nr)
        if value is None:   # DBus cannot encode None, so return an empty string in this case.
            return ""
        return value

    ## Get data from the high volume data logging objects.
    #  @param logger_name: The name of the data logger that we want to get data from.
    #  @param low_rows: The amount of data logging rows that we want to retrieve
    #  @return a array of arrays that contain the logging data. The first column will contain the time of the log, the rest is instance specific.
    @dbusif.method("si", "asaad")
    def getDataLoggerData(self, logger_name, log_rows):
        data_logger = dataLogger.DataLogger.getInstance(logger_name)
        if data_logger is None:
            return [], []
        return data_logger.getColumnNames(), data_logger.getData(log_rows)

    # Send a GCode to Marlin and do not wait for the reply.
    # This to prevent the dbus service from blocking. Due to odd electrical architecture the led service uses this call
    # This call returns True or False depending on if the command is successfully queued or not.
    @dbusif.method("s", "b")
    def debugSendGCodeNoWait(self, gcode_line):
        if not self._controller.getDriver().isConnected():
            return False
        return self._controller.sendRawGCodeNoBlock(gcode_line)

    @dbusif.method("s", "s")
    def debugSendGCode(self, gcode_line):
        if not self._controller.getDriver().isConnected():
            return "NOT CONNECTED"
        return self._controller.sendRawGCode(gcode_line, True)

    @dbusif.method("", "a{sv}")
    def debugDriverInfo(self):
        return self._controller.debugGetDriverInfo()

    @dbusif.method("s", "")
    def debugSetLogLevel(self, level):
        # Set the root logging level, this effects all logging messages in the printer service
        # This function is to facilitate problem diagnosing at runtime.
        # @param level: needs to be a string matching a logging level from https://docs.python.org/3/library/logging.html#levels
        # Example: printer_service.debugSetLogLevel("DEBUG")
        logging.getLogger().setLevel(level)

    ## @brief The callback function to register which needs to be called when a factory reset is issued
    #  @param reset_type One of the defined values from FactoryReset which would indicate a hard or soft reset
    @dbusif.method("s", "")
    def resetSettings(self, reset_type):
        self._controller.resetToFactorySettings(reset_type)

    ## @brief The function to check if there is build plate level data available
    #  @return bool Return true if there is, otherwise false
    @dbusif.method("","b")
    def hasBuildPlateLevelData(self):
        return self._controller.hasBuildPlateLevelData()

    ## @brief Signals that material for a specific hotend has been extruded
    #  This signal is being given by the Material Extrusion Monitor in 2 specific circumstances:
    #  1. If a minute has passed (and material has been extruded since the last signal)
    #  2. If a minimum of, in this case 10mm, has been extruded and the last signal was less then a minute ago
    #  @param hotend_nr The hotend index which was monitored
    #  @param extrusion_amount The amount extruded
    #  @param usage_time The time it took to handle the extrusion (retration) amount (rough estimate based on max e speed)
    @dbusif.signal("idd")
    def onExtrusionAmountChanged(self, hotend_nr, extrusion_amount, usage_time):
        return True
