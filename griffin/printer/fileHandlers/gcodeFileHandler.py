from .fileHandler import FileHandler
from .timeEstimators import getTimeEstimator

import logging
import os

from griffin.printer.faultHandler import FaultHandler, FaultCode

log = logging.getLogger(__name__.split(".")[-1])


class GCodeFileHandler(FileHandler):
    # constructor

    ## construct a GCodeFileHandler,
    # pass the file name here, it is available at construction time anyway and it saves passing it around via the run/start mechanism.
    # @param file_name The file name to handle
    def __init__(self, file_name):
        super().__init__()
        # Flag to indicate abort of the print process
        self.__abort = False

        # The fully qualified file name to the (gcode) file to print
        self._file_path = file_name
        self._file_name = os.path.basename(self._file_path)
        self._metadata = None
        self._time_estimator = None

    # Public Section

    # @brief Starts the printing process
    def run(self):
        assert self._metadata is not None

        if self._metadata.getFlavor() == self.__ULTIGCODE_FLAVOR:
            old_flow = self._controller.getPropertyValue("extrusion_amount_modifier") / 100
            self._controller.setPropertyValue("extrusion_amount_modifier", 17) # HACK; We don't correctly support UltiGCode. Note that this does not provide good results (the 17% is not quite right)
            # to correct for the retraction that was previously done on this nozzle we need to adjust the e position if it is negative.
            e = self._controller.getActiveHotend()
            pos = self._controller.getPosition("E" + str(e))
            if pos < 0:
                new_inverse_flow = 1 / 0.17
                new_pos = pos * old_flow * new_inverse_flow
                self._controller.queueGCode("G92 E%f" % new_pos)

        self._time_estimator = getTimeEstimator(self._metadata)
        self._time_estimator.startPrint()

        try:
            with self.openFileStream() as f:
                # Use of 'while line in f:' cannot be done here, because then the f.tell() cannot be used
                while True:
                    line = f.readline()
                    if len(line) < 1:
                        break

                    self._time_estimator.updateBytesConsumed(int(f.tell()))  # Require f.tell() because 'len(line)' counts CR/LF as 1 character which causes +/-10% file size mismatch

                    if line.startswith(";"):
                        self._time_estimator.parseLine(line)
                        continue

                    self._controller.queueGCode(line)

                    if self.__abort:
                        self._controller.abortQueue()
                        log.info("G-Code file handler aborted")
                        break
        except OSError:
            log.exception("Error accessing file '%s'", self._file_name)
            FaultHandler.warning("Error while reading file '" + self._file_name + "'", FaultCode.FILE_ERROR)

        self._controller.waitForQueueToBeEmpty(wait_for_motion_controller = True)

        self._time_estimator.stopPrint(self._file_name, self.__abort)
        self.onFinished.emit()

    # @brief Sets the abort flag indicating process abort interupt
    def abort(self):
        self.__abort = True

    # @brief Returns the relevant process info
    # @return Returns a KVP list
    def getMetaData(self):
        metadata = {
            "filename": self._getBasename()
        }
        if self._time_estimator is not None:
            metadata.update(self._time_estimator.getMetaData())
        return metadata

    # @brief Open the file stream
    # @return Returns the filestream object
    # This function can be overridden by derived classes to implement their logic
    def openFileStream(self):
        return open(self._file_path, "rt")

    # Protected Section
    # These functions can be overridden by derived classes to implement their logic

    # @brief Extracts the file name from the full qualified path
    # @return Returns the file name only
    def _getBasename(self):
        return self._file_name

    # @brief Determines the filesize
    # @return Returns the size of the file in bytes
    def getFileSize(self):
        return os.stat(self._file_path).st_size

    def setGcodeMetadata(self, metadata):
        self._metadata = metadata

    # Private Section

    # Special UltiGCode hack identification
    __ULTIGCODE_FLAVOR = "UltiGCode"
