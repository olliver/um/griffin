
## abstract factory
class FileHandlerFactory():
    # @param file_path The complete path to the file to handle.
    def __init__(self, file_path):
        raise NotImplementedError("constructor not implemented in base class")
    
    def getFileHandler(self):
        return self._file_handler

    def getMetaDataHandler(self):
        return self._metadata_handler
