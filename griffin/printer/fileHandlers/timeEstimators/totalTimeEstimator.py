from .timeEstimator import TimeEstimator
import datetime
import logging

log = logging.getLogger(__name__.split(".")[-1])

## This class estimates progress and time remaining based on a total time in the file and the layer count
# in the file as well as the time elapsed.
class TotalTimeEstimator(TimeEstimator):
    def __init__(self, gcode_metadata):
        super().__init__(log.name)
        self._total_time = gcode_metadata.getTimeEstimate() # starts as non initialized
        self._current_layer = 0
        self._nr_of_layers = gcode_metadata.getLayerCount()
        self._time_elapsed_on_start_of_current_layer = 0.0

    def getTotalTime(self):
        if self._time_elapsed_on_start_of_current_layer == 0 or self._total_time is None:
            return float("inf"), 1.0
        estimated_time_elapsed_till_start_of_layer = self._progress * self._total_time
        scaling_factor = estimated_time_elapsed_till_start_of_layer / self._time_elapsed_on_start_of_current_layer
        scaling_factor_influence = self._progress

        # The scaling factor is only as useful as the data that is put into it, so it has no knowledge (or impact) at the
        # start of the print and has the most knowledge and therefor impact at the end of the print.
        # At progress 0.2 with a scaling factor of 1 you get 0.2 + 0.8 = 1 if the scaling factor is 2: 0.2*2 + 0.8 = 1.2

        scaling_factor = scaling_factor *  scaling_factor_influence + (1.0  - scaling_factor_influence)

        compensated_total_time = self._total_time * scaling_factor
        return compensated_total_time, scaling_factor

    def getTimeRemaining(self):
        if self._time_elapsed_on_start_of_current_layer == 0 or self._total_time is None:
            return float("inf")
        elif self._progress <= 0:
            return self._total_time - self.getTimeElapsed()

        return super().getTimeRemaining()

    def getProgress(self):
        if self._time_elapsed_on_start_of_current_layer == 0 or self._total_time is None or self._progress <= 0:
            return 0.0

        return super().getProgress()

    def parseLine(self, line):
        with self._lock:
            line = line.upper()
            if line.startswith(";LAYER:"):
                self._current_layer = int(self._parseCommentValue(line))
                self._time_elapsed_on_start_of_current_layer = self.getTimeElapsed()
                if self._nr_of_layers is not None:
                    self._progress = self._current_layer / self._nr_of_layers
