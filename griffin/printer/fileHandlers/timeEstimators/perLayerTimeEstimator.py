from .timeEstimator import TimeEstimator
import datetime
import logging

log = logging.getLogger(__name__.split(".")[-1])

## This class estimates progress and time remaining based on a total time in the file and a time estimate
# how long it took to reach each layer in the file, as well as the time elapsed.
class PerLayerTimeEstimator(TimeEstimator):
    def __init__(self, gcode_metadata):
        super().__init__(log.name)
        self._total_time = gcode_metadata.getTimeEstimate()
        self._estimate_till_current_layer = 0.0
        self._time_elapsed_on_start_of_current_layer = 0.0

    # A common method to determine the total_time after considering progress in the file and time elapsed.
    # @return A tuple of the total estimated time the print will take and the scaling factor used to calculate it, for debugging purposes
    def getTotalTime(self):
        if self._estimate_till_current_layer == 0:
            return self._total_time, 1

        scaling_factor = self._time_elapsed_at_start_of_current_layer / self._estimate_till_current_layer

        # The scaling factor is only as useful as the data that is put into it, so it has no knowledge (or impact) at the
        # start of the print and has the most knowledge and therefor impact at the end of the print.
        # At progress 0.2 with a scaling factor of 1 you get 0.2 + 0.8 = 1 if the scaling factor is 2: 0.2*2 + 0.8 = 1.2

        scaling_factor_influence = self._estimate_till_current_layer / self._total_time
        scaling_factor = scaling_factor *  scaling_factor_influence + (1.0  - scaling_factor_influence)

        compensated_total_time = self._total_time * scaling_factor
        return compensated_total_time, scaling_factor

    def getTimeRemaining(self):
        if self._estimate_till_current_layer == 0:
            return self._limitTime(self._total_time - self.getTimeElapsed())

        return super().getTimeRemaining()

    def getProgress(self):
        if self._estimate_till_current_layer == 0:
            return self._limitProgress(self.getTimeElapsed() / self._total_time)

        return super().getProgress()

    def parseLine(self, line):
        if line.startswith(";TIME_ELAPSED:"):
            with self._lock:
                self._estimate_till_current_layer = float(self._parseCommentValue(line))
                self._time_elapsed_at_start_of_current_layer = self.getTimeElapsed()
