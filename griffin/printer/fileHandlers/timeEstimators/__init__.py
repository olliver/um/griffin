from .fileSizeTimeEstimator import FileSizeTimeEstimator
from .totalTimeEstimator import TotalTimeEstimator
from .perLayerTimeEstimator import PerLayerTimeEstimator


## factory function to instantiate a time estimator.
def getTimeEstimator(gcode_metadata):
    if gcode_metadata.isValid():
        return PerLayerTimeEstimator(gcode_metadata)
    elif gcode_metadata.hasOldStyleTimeEstimate():
        return TotalTimeEstimator(gcode_metadata)
    else:
        return FileSizeTimeEstimator(gcode_metadata)