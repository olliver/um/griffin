from .timeEstimator import TimeEstimator
import datetime
import logging

log = logging.getLogger(__name__.split(".")[-1])

## This class estimates progress and time remaining based on the filesize and bytes read
class FileSizeTimeEstimator(TimeEstimator):
    # Determine how many bytes as a percentage to skip before starting time measurement during the calibration phase
    __PERCENTAGE_BYTES_TO_SKIP_DURING_CALIBRATION_PHASE = 0.1 / 100

    def __init__(self, metadata):
        super().__init__(log.name)
        try:
            self._total_bytes = metadata.getFileSize()
        except Exception:
            self._total_bytes = None
            self._progress = -1.0
        self._time_elapsed_after_skip = None

    def updateBytesConsumed(self, bytes_consumed):
        if self._total_bytes is not None:
            with self._lock:
                self._progress = bytes_consumed / self._total_bytes
            log.debug("progress update {progress}".format(progress=self._progress))

    def getTimeRemaining(self):
        if self._total_bytes is None or self._progress <= self.__PERCENTAGE_BYTES_TO_SKIP_DURING_CALIBRATION_PHASE:
            return float("inf")
        time_elapsed = self.getTimeElapsed()
        total_time, scaling_factor = self.getTotalTime()
        estimated_time_remaining = total_time - time_elapsed

        ## log every 5 minutes or every minute in the last 5 minutes or every second/request during the last 10 seconds.
        if int(time_elapsed) % self._FIVE_MINUTES < 1 or (estimated_time_remaining < self._FIVE_MINUTES and int(time_elapsed) % self._ONE_MINUTE < 1) or estimated_time_remaining < 10 and total_time != float('inf'):
            log.info(("getTimeRemaining() calculated_total_time {calculated_total_time}" +
                 " time remaining {estimated_time_remaining} time elapsed {time_elapsed} progress {progress}")
                 .format(calculated_total_time=datetime.timedelta(seconds=total_time), estimated_time_remaining=datetime.timedelta(seconds=estimated_time_remaining), time_elapsed=datetime.timedelta(seconds=self.getTimeElapsed()), progress=self._progress))
        return self._limitTime(estimated_time_remaining)

    def getTotalTime(self):
        if self._total_bytes is None or self._progress <= self.__PERCENTAGE_BYTES_TO_SKIP_DURING_CALIBRATION_PHASE:
            return float("inf"), 1.0
        elif self._time_elapsed_after_skip is None:
            self._time_elapsed_after_skip = self.getTimeElapsed()
            return float("inf"), 1.0

        time_elapsed = self.getTimeElapsed() - self._time_elapsed_after_skip
        total_time = time_elapsed * ( 1 / (self._progress - self.__PERCENTAGE_BYTES_TO_SKIP_DURING_CALIBRATION_PHASE))

        return self._limitTime(total_time), 1.0 # no scaling factor ot base nominal time.

    def getProgress(self):
        if self._progress > self.__PERCENTAGE_BYTES_TO_SKIP_DURING_CALIBRATION_PHASE:
            return self._limitProgress(self._progress - self.__PERCENTAGE_BYTES_TO_SKIP_DURING_CALIBRATION_PHASE)
        return 0