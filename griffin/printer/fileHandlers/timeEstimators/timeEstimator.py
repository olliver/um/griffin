from griffin.printer.controller import Controller
from threading import Lock
import time
import logging
import datetime
import json

log = logging.getLogger(__name__.split(".")[-1])


## This is the base class for the printing time estimates, all it does is track time elapsed.
# subclasses can use this time in their calculations for print progress and estimated time remaining.
class TimeEstimator():
    _ONE_MINUTE = 60
    _FIVE_MINUTES = 5 * _ONE_MINUTE

    def __init__(self, name):
        self._start_time = None # not started or restarted after paused
        self._time_booked = 0.0 # track time elapsed outside of start time for instance
        Controller.getInstance().onPauseChanged.connect(self.__onPauseChanged)
        self._paused = Controller.getInstance().getPaused() # should be False
        assert self._paused == False
        self._print_running = False
        self._lock = Lock()
        self._progress = 0.0
        log.info("{name}, I choose you!".format(name=name))

    ## @return The time elapsed during the printing in seconds.
    def getTimeElapsed(self):
        if self._start_time is None:
            return 0

        if self._paused or self._print_running == False:
            return self._time_booked
        elif self._print_running:
            return time.monotonic() - self._start_time + self._time_booked

    ## called to update the time estimator on file progress, depending on the time estimation strategy this may
    #  or may not be used/implemented in the child class.
    #  @param line A line from the file being read that may or may not contain information that can be used in the time estimate.
    def parseLine(self, line):
        pass

    ## called to update the time estimator on file progress, depending on the time estimation strategy this may
    #  or may not be used/implemented in the child class.
    #  @param bytes_consumed A count of the bytes of the print file already sent to the motion controller
    def updateBytesConsumed(self, bytes_consumed):
        pass

    ## start the timer running.
    def startPrint(self):
        with self._lock:
            self._start_time = time.monotonic()
            self._print_running = True

    ## tell the time Estimate the print was stopped.
    # reports the time spent in actual printing step for this print job.
    # @param filename the filename to report the time statistics for
    # @param abort a boolean indicating whether the print was aborted as thn the time is invalid for statistical use.
    def stopPrint(self, filename, abort):
        Controller.getInstance().waitForQueueToBeEmpty(wait_for_motion_controller=True)

        with self._lock:
            self._time_booked = self.getTimeElapsed()
            self._print_running = False

        stats = {
            "filename": filename,
            "original_estimate": "unknown",
            "original_estimate_in_seconds": "unknown",
            "print_time": str(datetime.timedelta(seconds=self._time_booked)),
            "print_time_in_seconds":self._time_booked
        }
        total_time, scaling_factor = self.getTotalTime()

        if scaling_factor != 1.0:
            stats["original_estimate_in_seconds"] = total_time / scaling_factor
            stats["original_estimate"] = str(datetime.timedelta(seconds=stats["original_estimate_in_seconds"]))

        aborted_msg = "Warning print was aborted!\n" if abort else ""

        log.info(aborted_msg + "Print job statistics " + json.dumps(stats, indent=4))

    ## the all important progress report!
    # @return should be a value between 0 amd 1 and should be a float, as an error value indicating progress is unknown -1.0 may be returned.
    # when time remaining is not known in the start of a print it should return 0.0
    def getProgress(self):
        compensated_total_time, _ = self.getTotalTime()
        if compensated_total_time < 1:
            return 0.0
        time_elapsed = self.getTimeElapsed()
        return self._limitProgress(time_elapsed / compensated_total_time)

    ## utility function to clamp progress between 0 and 1.
    def _limitProgress(self, progress):
        return min(1.0, max(0.0, progress))

    ## @return estimated time remaining in seconds(must be float); The return value may be infinite when no time is known.
    def getTimeRemaining(self):
        compensated_total_time, scaling_factor = self.getTotalTime()
        total_time = compensated_total_time / scaling_factor # added due to move to base class since the base class should have no idea how time is tracked in the child classes I oppose this Idea.
        time_elapsed = self.getTimeElapsed()
        estimated_time_remaining = compensated_total_time - time_elapsed

        ## log every 5 minutes or every minute in the last 5 minutes or every second/request during the last 10 seconds.
        if int(time_elapsed) % self._FIVE_MINUTES < 1 or (estimated_time_remaining < self._FIVE_MINUTES and int(time_elapsed) % self._ONE_MINUTE < 1) or estimated_time_remaining < 10:
            log.info(("getTimeRemaining() total_time {total_time} scaling_factor {scaling_factor} compensated_total_time {compensated_total_time}" +
                 " time remaining {estimated_time_remaining} pure_cura_time_remaining {pure_cura_time_remaining}")
                 .format(total_time=datetime.timedelta(seconds=total_time), scaling_factor=scaling_factor, compensated_total_time=str(datetime.timedelta(seconds=compensated_total_time)),
                         estimated_time_remaining=str(datetime.timedelta(seconds=estimated_time_remaining)), pure_cura_time_remaining=str(datetime.timedelta(seconds=total_time - self.getTimeElapsed()))))

        return self._limitTime(estimated_time_remaining)

    ## utility function to avoid negative times.
    def _limitTime(self, time):
        return max(0.0, time)

    ## @return A tuple with the total estimated duration, and if available the scaling factor that was
    # applied to the original total time estimate if applicable.
    def getTotalTime(self):
        raise NotImplementedError("function not implemented in base class")

    ## return a dictionary with the time estimates.from
    # @note Please note that time_remaining and time_total are sent as floating point number and can be infinite!
    def getMetaData(self):
        return {
            "progress": float(self.getProgress()),
            "time_remaining":float(self.getTimeRemaining()),
            "time_elapsed": int(self.getTimeElapsed()),
            "time_total": float(self.getTotalTime()[0])
        }
    ## callback for the pauseChanged event
    def __onPauseChanged(self, pause):
        if pause:
            # Only when pausing we want to wait until the pause has actually taken effect, we might (very
            # unlikely as there are 2 wait-fo-_queue-to-be-empty's in the pause print procedure...)
            # loose a second or so due to the parking of the head but this is minor in comparison to the potential time in the queue.
            Controller.getInstance().waitForQueueToBeEmpty(wait_for_motion_controller=True)

        with self._lock:
            if pause:
                self._time_booked = self.getTimeElapsed()
            else:
                self._start_time = time.monotonic()
            self._paused = pause

    ## Gets the value after the : from the comment line while removing leading spaces
    # Used as a common helper function in child classes
    # @return Returns the value found after the :
    def _parseCommentValue(self, line):
        value_start = line.index(':') + 1
        return (line[value_start:]).strip()
