from griffin import signal
from griffin import thread

from griffin.printer.controller import Controller

import logging

log = logging.getLogger(__name__.split(".")[-1])


class FileHandler(signal.SignalEmitter):
    onFinished = signal.Signal()

    def __init__(self):
        self._controller = Controller.getInstance()

        super().__init__()

    def start(self):
        thread.Thread("FileHandler", self._run).start()

    def _run(self):
        try:
            self.run()
        except:
            log.exception("Exception in file handling")
            # TODO: Proper error handling
            self.onFinished.emit()

    def run(self):
        raise NotImplementedError()

    def cleanup(self):
        pass    # Override in subclass if required.

    def abort(self):
        pass    # Override in subclass, called when the user wants to abort the print

    ## @return the total filesize in bytes of the (uncompressed) file.
    def getFileSize(self):
        raise NotImplementedError("not implemented in base class")

    def getMetaData(self):
        return {
            "progress": float(-1.0),
            "time_elapsed": int(-1),
            "time_total": int(-1),
        }
