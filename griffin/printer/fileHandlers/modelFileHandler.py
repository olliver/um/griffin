import logging
import os

from .fileHandler import FileHandler
from .cura.curaInterface import CuraInterface

log = logging.getLogger(__name__.split(".")[-1])


# 3D model file handler. Pass the model to the Cura slicer and let that process the model into a gcode file.
class ModelFileHandler(FileHandler):
    def __init__(self):
        super().__init__()

        # self._printer = dbusif.RemoteObject("printer")
        # self._output_stream = None
        self._cura = CuraInterface.getInstance()
        self._cura.onError.connect(self.onError)
        self._cura.onFinished.connect(self.onCuraFinished)
        self._cura.onGCodeData.connect(self.onGCodeData)
        self._size_total = 0

    def run(self, filename):
        self._output_tmp = open("/tmp/tmp.gcode", "w")

        filenames = []
        for name in filename.split("|"):
            filenames.append(os.path.join(os.path.dirname(filename), name))
        self._size_total = 0
        self._cura.startSlice(filenames, {})    # missing parameters

    def onError(self, error):
        self.error.emit("Error during slicing: %s" % (error))

    def onCuraFinished(self):
        log.info("Cura reports done, waiting for queue to be empty")
        self._controller.waitForQueueToBeEmpty()
        log.info("queue is empty")
        self.onFinished.emit()

    def onGCodeData(self, data):
        log.info("Got GCode layer")
        self._size_total += len(data)
        lines = data.split("\n")
        for line in lines:
            self._controller.queueGCode(line)
        self._output_tmp.write(data)

    def abort(self):
        self._cura.abortSlice()

    def cleanup(self):
        log.info("Cleaning up")
        self._cura.onError.disconnect(self.onError)
        self._cura.onFinished.disconnect(self.onFinished)
        self._cura.onGCodeData.disconnect(self.onGCodeData)

#    def getProgress(self):
#        if self._size_total == 0:
#            return 0.0
#        #bytes_done = self._printer.getStreamProgress()
#        bytes_done = self._controller.getBytes()
#        return float(bytes_done) / float(self._size_total)
