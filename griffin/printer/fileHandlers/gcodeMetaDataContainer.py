from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
import logging
import uuid
import re

log = logging.getLogger(__name__.split(".")[-1])

## contains the print jobs's metadata and provides access functions to retrieve it.
class GCodeMetaDataContainer():
    HAS_OLD_PRINT_TIME = "HAS_OLD_PRINT_TIME"
    HAS_LAYER_COUNT = "HAS_LAYER_COUNT"
    FILE_SIZE = "FILE_SIZE"
    EMPTY_HEADER = {
        "HEADER_VERSION": -1.0, # no header parsed use old behaviour
        "FLAVOR": None,
        "GENERATOR": { "NAME": None, "VERSION": None, "BUILD_DATE": None },
        "EXTRUDER_TRAIN": {},
        "BUILD_PLATE": { "INITIAL_TEMPERATURE": None },
        "PRINT": {
            "TIME": -1, # same as time_total
            "SIZE": {
                "MIN": { "X": -float("inf"), "Y": -float("inf"), "Z": -float("inf") },
                "MAX": { "X":  float("inf"), "Y":  float("inf"), "Z":  float("inf") }
            }
        }
    }
    EMPTY_EXTRUDER_TRAIN = {
        "INITIAL_TEMPERATURE": 0, # leave heater off when left out
        "MATERIAL": {
            "VOLUME_USED": None,
            "GUID": None
        },
        "NOZZLE": { "DIAMETER": None }
    }
    CHECK_ALWAYS = "CHECK_ALWAYS"       ## checks returned by getHeaderChecks() marked with this flag should always be ran.
    CHECK_SKIPABLE = "CHECK_SKIPABLE"   ## checks returned by getHeaderChecks() marked with this flag may be skipped if they are covered elsewhere.
    # in the future we might communicate the specific checks done/not done, this flag then still indicates whether the specific test is allowed to be skipped at all.

    def __init__(self, metadata):
        self._metadata = metadata

    ## Returns the header version.
    # @return A floating point header version.
    def getVersion(self):
        return self._metadata.get("HEADER_VERSION")

    ## Returns the Flavor.
    # @return A sting representing the flavor.
    def getFlavor(self):
        return self._metadata.get("FLAVOR")

    ## Returns whether the header is valid.
    # @return A boolean indicating header validity.
    def isValid(self):
        return self.getVersion() == 0.1

    ## @return the (uncompressed) filesize in bytes.
    def getFileSize(self):
        return self._metadata.getAsInt(self.FILE_SIZE)

    ## @return A boolean indicating whether an old style time estimate is available
    def hasOldStyleTimeEstimate(self):
        return self._metadata.getAsBoolean(self.HAS_LAYER_COUNT) and self._metadata.getAsBoolean(self.HAS_OLD_PRINT_TIME)

    ## Returns the initial temperature of the hotend specified.
    # @return temperature in degrees Celsius, or None.
    def getInitialTemperature(self, hotend_nr):
        hotend = self._metadata.getAsRegistry("EXTRUDER_TRAIN").getAsRegistry(str(hotend_nr))
        if hotend is None:
            return None
        return hotend.get("INITIAL_TEMPERATURE")

    ## returns the initial temperature of the build-plate.
    # @return temperature in degrees Celsius, or None.
    def getInitialBuildplateTemperature(self):
        return self._metadata.get("BUILD_PLATE").get("INITIAL_TEMPERATURE")

    ## Returns the time estimate found in the header.
    # @return time in seconds or -1 if no time was found in the header.
    def getTimeEstimate(self):
        return self._metadata.getAsRegistry("PRINT").getAsInt("TIME")

    # @return The old style layer count that does give some vague indication of progress in the file.
    def getLayerCount(self):
        return self._metadata.getAsInt("LAYER_COUNT", None)

    # @return A list of extruder trains actively used in this print.
    def getExtruderTrains(self):
        extruder_train = self._metadata.get("EXTRUDER_TRAIN")
        return [ x for x in extruder_train.keys() if extruder_train.get(x).get("MATERIAL").get("VOLUME_USED") is not None]

    ## Returns the material estimated to be used by this print of the hotend specified.
    # @return amount of material in mm3 or None.
    def getMaterialToBeUsed(self, hotend_nr):
        hotend = self._metadata.getAsRegistry("EXTRUDER_TRAIN").getAsRegistry(str(hotend_nr))
        if hotend is None:
            return None
        return hotend.getAsRegistry("MATERIAL").get("VOLUME_USED")

    ## runs tests on the header after it is parsed.
    #  @return a list of tuples, each tuple consists of a flag(CHECK_ALWAYS or CHECK_SKIPABLE) and a function pointer.
    def getHeaderChecks(self):
        controller = Controller.getInstance()
        list = [
            (self.CHECK_ALWAYS,     self.__executeHeadeCheckAndChangeHeader),
            (self.CHECK_SKIPABLE,   self.__executePrintSizeCheck),
            (self.CHECK_ALWAYS,     self.__executeGeneratorNameCheck),
            (self.CHECK_ALWAYS,     self.__executeBuildplateTemperatureCheck),
            (self.CHECK_SKIPABLE,   self.__executeGeneratorDateCheck),
            (self.CHECK_ALWAYS,     self.__executeGeneratorVersionCheck),
            (self.CHECK_SKIPABLE,   self.__executeTimeEstimateCheck),
            (self.CHECK_ALWAYS,     self.__executeNrOfExtrudersCheck),
            (self.CHECK_SKIPABLE,   self.__hasBuildplateTemperature)
        ]

        for extruder_nr, extruder in self.__iterateExtruders():
            list.append((self.CHECK_ALWAYS, self.__executeExtruderNrCheck(extruder_nr)))
            if controller.getPrintHeadController().getHotendCartridgeManager().isPresent(extruder_nr) and self.getInitialTemperature(extruder_nr) > 0.0:
                list.append((self.CHECK_SKIPABLE, self.__executeNozzleDiameterCheck(extruder_nr, extruder)))
                list.append((self.CHECK_SKIPABLE, self.__executeGuidCheck(extruder_nr, extruder)))
                list.append((self.CHECK_SKIPABLE, self.__executeVolumeUsedCheck(extruder_nr, extruder)))

        return list

    # Private
    def __hasBuildplateTemperature(self):
        if self.getInitialBuildplateTemperature() is None:
            FaultHandler.warning("buildplate must be set", FaultCode.HEADER_MISSING_ITEM)


    ## @return A list of (extruder_nr, Registry) tuples.
    def __iterateExtruders(self):
        extruder_keys = self._metadata.get("EXTRUDER_TRAIN").keys()
        extruder_train = self._metadata.getAsRegistry("EXTRUDER_TRAIN")
        return [(int(extruder), extruder_train.getAsRegistry(extruder)) for extruder in extruder_keys]

    ## Checks the header and sets it to defaults if no header is found
    def __executeHeadeCheckAndChangeHeader(self):
        msg = None
        if not self.isValid():
            msg = "header was not present"
        elif self.getFlavor() != "Griffin":
            msg = "flavor is missing or incorrect (gcode header '%s' expected by machine 'Griffin')" % self._metadata.getFlavor()
        if msg:
            FaultHandler.warning(msg, FaultCode.HEADER_NOT_PRESENT)
            self.__setDefaultsForNoHeader()

    ## Sets the header's default for when no header is present to simulate the old behaviour (almost).
    def __setDefaultsForNoHeader(self):
        controller = Controller.getInstance()
        self._metadata.setAsFloat("HEADER_VERSION", -1.0)
        p = self._metadata.getAsRegistry("PRINT")
        if p.get("TIME") == -1:
            p.set("TIME", 0) # preserve old school parsed value!
        self._metadata.getAsRegistry("BUILD_PLATE").set("INITIAL_TEMPERATURE", controller.getMaterialManager().getRequiredBedTemperature())
        self._metadata.getAsRegistry("PRINT").getAsRegistry("SIZE").set("MIN", { "X":0.0, "Y":0.0, "Z":0.0 })
        self._metadata.getAsRegistry("PRINT").getAsRegistry("SIZE").set("MAX", { "X":200.0, "Y":200.0, "Z":200.0 }) # exact size somewhat irrelevant as it might have been sliced for an extended as well as a 2go
        for i in range(0, controller.getPropertyValue("hotend_count")):
            # only enable present extruders
            if controller.getPrintHeadController().getHotendCartridgeManager().isPresent(i):
                extruder = self._metadata.getAsRegistry("EXTRUDER_TRAIN").getAsRegistry(str(i))
                extruder.set("INITIAL_TEMPERATURE", controller.getMaterialManager().getPrintingTemperature(i))

    ## Checks the printer size.
    def __executePrintSizeCheck(self):
        controller = Controller.getInstance()
        printer_size = { axis: controller.getPropertyValue("build_volume_" + axis.lower()) for axis in ("X", "Y", "Z") }

        for side in [self._metadata.getAsRegistry("PRINT").getAsRegistry("SIZE").get(key) for key in self._metadata.getAsRegistry("PRINT").get("SIZE").keys()]:
            for axis in side:
                if float(side[axis]) < 0.0 or float(side[axis]) > printer_size[axis]:
                    msg = "Gcode expected a machine that can reach %.1fmm in the %s axis. Machine limits are (0 - %d) for this axis." % (side[axis], axis, printer_size[axis])
                    FaultHandler.warning(msg, FaultCode.MACHINE_TOO_SMALL_FOR_GCODE)

    ## Checks the generator name.
    def __executeGeneratorNameCheck(self):
        if not self.isValid():
            return
        if self._metadata.getAsRegistry("GENERATOR").getAsString("NAME") is None:
            msg = "generator name was missing from header"
            FaultHandler.warning(msg, FaultCode.HEADER_MISSING_ITEM)

    ## Checks the build-plate temperature.
    def __executeBuildplateTemperatureCheck(self):
        if self._metadata.getAsRegistry("BUILD_PLATE").get("INITIAL_TEMPERATURE") is None:
            msg = "BUILD_PLATE.INITIAL_TEMPERATURE missing from header"
            FaultHandler.warning(msg, FaultCode.HEADER_MISSING_ITEM)

    ## Checks the generator version.
    def __executeGeneratorVersionCheck(self):
        if not self.isValid():
            return
        if self._metadata.getAsRegistry("GENERATOR").getAsString("VERSION") is None:
            msg = "GENERATOR.VERSION missing from header"
            FaultHandler.warning(msg, FaultCode.HEADER_MISSING_ITEM)

    ## Checks the generator build-date.
    def __executeGeneratorDateCheck(self):
        if not self.isValid():
            return
        if self._metadata.getAsRegistry("GENERATOR").get("BUILD_DATE") is not None:
            # matching the date property of the header, for definition of the header see https://docs.google.com/document/d/1ex3VDTSN0PipNgon1rXXXeWti1jPYhrnAjOCanxJKAg
            match = re.match(r"^(\d{4})-(\d\d)-(\d\d)$", self._metadata.getAsRegistry("GENERATOR").get("BUILD_DATE"))
            if match:
                year, month, day = match.groups()
                log.debug("GENERATOR BUILD_DATE: %4d-%2d-%2d", year, month, day)
                # perhaps date could be checked in the future
            else:
                msg = "GENERATOR.BUILD_DATE in the header was malformed"
                FaultHandler.warning(msg, FaultCode.HEADER_MISSING_ITEM)
        else:
            msg = "GENERATOR.BUILD_DATE was missing from header"
            FaultHandler.warning(msg, FaultCode.HEADER_MISSING_ITEM)

    ## Checks the time estimate.
    def __executeTimeEstimateCheck(self):
        time = self._metadata.getAsRegistry("PRINT").getAsInt("TIME")
        try:
            if self._metadata.getAsRegistry("PRINT").getAsInt("TIME") < 0:
                msg = "time estimate is missing from header"
                FaultHandler.warning(msg, FaultCode.HEADER_MISSING_ITEM)
        except Exception:
            FaultHandler.warning("time estimate was malformed", FaultCode.HEADER_MISSING_ITEM)

    ## Checks that the number of extruder referred to exist.
    def __executeNrOfExtrudersCheck(self):
        controller = Controller.getInstance()
        nr_of_extruders = len(self._metadata.get("EXTRUDER_TRAIN").keys()) # get as dictionary so there is a .keys() function...

        if nr_of_extruders > controller.getPropertyValue("hotend_count"):
            msg = "gcode expected %d nozzles, yet the machine has %d nozzles" % (nr_of_extruders, controller.getPropertyValue("hotend_count"))
            FaultHandler.warning(msg, FaultCode.NOZZLE_AMOUNT_MISMATCH)

    ## Checks that the extruder number referred to exists.
    def __executeExtruderNrCheck(self, extruder_nr):
        def lambda_function():
            controller = Controller.getInstance()
            if extruder_nr < 0 or extruder_nr >= controller.getPropertyValue("hotend_count"):
                FaultHandler.warning("gcode indicates nozzle cartridge (%d) not present in machine (1 t/m %d)" %
                    (extruder_nr + 1, controller.getPropertyValue("hotend_count")), FaultCode.NOZZLE_AMOUNT_MISMATCH, data={"display_hotend_nr":extruder_nr + 1})
        return lambda_function

    ## Checks the print's nozzle diameter against the machine's nozzle diameter.
    def __executeNozzleDiameterCheck(self, extruder_nr, extruder):
        def lambda_function():
            if not self.isValid():
                return
            controller = Controller.getInstance()
            hotend_cartridge_manager = controller.getPrintHeadController().getHotendCartridgeManager()
            try:
                nozzle_diam = extruder.getAsRegistry("NOZZLE").get("DIAMETER")
                if nozzle_diam is None:
                    FaultHandler.warning("nozzle diameter not set", FaultCode.NOZZLE_MISMATCH)
                elif hotend_cartridge_manager.getValue(extruder_nr, "nozzle_size") != nozzle_diam:
                    FaultHandler.warning("Nozzle diameter mismatch. Gcode expected %.2f, machine has %.2f for extruder %d" %
                        (nozzle_diam, hotend_cartridge_manager.getValue(extruder_nr, "nozzle_size"), extruder_nr + 1), FaultCode.NOZZLE_MISMATCH, data={"display_hotend_nr":extruder_nr + 1})
            except Exception as e:
                FaultHandler.warning("%s nozzle diameter malformed for extruder %d" % (str(e), extruder_nr + 1), FaultCode.NOZZLE_MISMATCH, data={"display_hotend_nr":extruder_nr + 1})
        return lambda_function

    ## Checks the volume used.
    def __executeVolumeUsedCheck(self, extruder_nr, extruder):
        def lambda_function():
            if not self.isValid():
                return
            try:
                vol = extruder.getAsRegistry("MATERIAL").get("VOLUME_USED")
                if vol is None:
                    FaultHandler.warning("VOLUME_USED is missing (for nozzle cartridge %d)" % (extruder_nr + 1), FaultCode.HEADER_MISSING_ITEM, data={"display_hotend_nr":extruder_nr + 1})
                elif vol < 0.0:
                    FaultHandler.warning("VOLUME_USED can not be negative (for nozzle cartridge %d)" % (extruder_nr + 1), FaultCode.HEADER_MISSING_ITEM, data={"display_hotend_nr":extruder_nr + 1})
            except Exception:
                FaultHandler.warning("VOLUME_USED malformed (for extruder %d)" % (extruder_nr + 1), FaultCode.HEADER_MISSING_ITEM, data={"display_hotend_nr":extruder_nr + 1})
        return lambda_function

    ## Checks the GUID.
    def __executeGuidCheck(self, extruder_nr, extruder):
        def lambda_function():
            controller = Controller.getInstance()
            tmp_material = controller.getPrintHeadController().getHotendSlot(extruder_nr).getPropertyValue("material_guid")
            if tmp_material == "":
                msg = "gcode file uses slot %d where no material is loaded in the machine" % (extruder_nr + 1 )
                FaultHandler.warning(msg, FaultCode.MATERIAL_NOT_LOADED, data={"display_hotend_nr":extruder_nr + 1})
                return

            active_material_guid = uuid.UUID(tmp_material)

            guid = extruder.getAsRegistry("MATERIAL").get("GUID") # should be of type uuid.UUID
            if guid is not None and isinstance(guid, uuid.UUID):
                if guid != active_material_guid: #also avoids unknown guids
                    if not controller.getMaterialManager().areMaterialsMatching(guid, active_material_guid):
                        msg = "gcode file was sliced for material %s in extruder %d where machine had %s loaded" % (guid, extruder_nr + 1, str(active_material_guid))
                        FaultHandler.warning(msg, FaultCode.GUID_MISMATCH, data={"display_hotend_nr":extruder_nr + 1})

        return lambda_function
