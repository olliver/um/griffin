from .gcodeFileHandler import GCodeFileHandler

import gzip
import logging
import os

import subprocess

log = logging.getLogger(__name__.split(".")[-1])


class GzipGCodeFileHandler(GCodeFileHandler):

    # Public

    # @brief Open the file stream
    # @return Returns the filestream object
    def openFileStream(self):
        return gzip.open(self._file_path, "rt")

    # Protected

    # @brief Extracts the filename from the full qualified path
    # @return Returns the filename as it is from the gz file
    def _getBasename(self):
        self.__readGzipData()
        return self.__gZipInfo["filename"]

    # @brief Determines the filesize
    # @return Returns the size of the file in bytes
    def getFileSize(self):
        self.__readGzipData()
        return self.__gZipInfo["uncompressed"]

    # Private

    def __init__(self, filename):
        super().__init__(filename)
        self.__gZipInfo = {}

    # @brief Store the gzip info into the dictionary
    def __readGzipData(self):
        # Only get the data once
        if len(self.__gZipInfo) > 0:
            log.debug("gZipInfo already read, skipping...")
            return

        # Load in local variable - without this step, due to multitasking' __gZipInfo might be filled with data but not with the data that is requested
        zip_info = {}
        result = self.__executeGzip().split()

        zip_info["compressed"] = int(result[0])
        zip_info["uncompressed"] = int(result[1])
        ratio = result[2][:-1]  # Remove % sign
        zip_info["ratio"] = float(ratio)
        zip_info["filename"] = os.path.basename(result[3])

        self.__gZipInfo = zip_info
        log.debug("gZip Info" + str(self.__gZipInfo))

    # @brief Executes gzip -l to get information about the compressed data (propably only works on files, not streams)
    # @return Returns the nht item found
    def __executeGzip(self):
        cmd = ["/bin/gzip", "-l", self._file_path]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        try:
            stdout, stderr = process.communicate(timeout=1.0)
        except subprocess.TimeoutExpired:
            process.kill()
            stdout, stderr = process.communicate()
        return stdout.splitlines()[-1].strip()
