from .fileHandlerFactory import FileHandlerFactory
from .gcodeFileHandler import GCodeFileHandler
from .gcodeMetaDataHandler import GCodeMetaDataHandler

## implements the constuction of the gcode file handler factory
class GCodeFileHandlerFactory(FileHandlerFactory):
    def __init__(self, file_path):
        self._file_handler = GCodeFileHandler(file_path)
        self._metadata_handler = GCodeMetaDataHandler(self._file_handler)
