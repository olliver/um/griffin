from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
from .gcodeMetaDataContainer import GCodeMetaDataContainer
from griffin.preferences.registry import Registry
import logging
import uuid
import copy
import re

log = logging.getLogger(__name__.split(".")[-1])


## handles gcode metadata parsing (the gcode header agreed with Cura
# and defined here: https://docs.google.com/document/d/1ex3VDTSN0PipNgon1rXXXeWti1jPYhrnAjOCanxJKAg
# There are no other metadata parsers yet.
class GCodeMetaDataHandler():
    MAX_LINES_TILL_START_OF_HEADER = 50
    MAX_LINES_TILL_END_OF_HEADER = 100
    PARSE_AS = {
        "INITIAL_TEMPERATURE":[ Registry.setAsFloat, lambda x: x >= 0.0,      "must be greater or equal to 0"],
        "HEADER_VERSION":     [ Registry.setAsFloat, lambda x: x == 0.1,      "must be 0.1"],
        "NAME":               [ Registry.setAsString,lambda x: x is not None, "must not be None"],
        "VERSION":            [ Registry.setAsString,lambda x: x is not None, "must not be None"],
        "BUILD_DATE":         [ Registry.setAsString,lambda x: re.match("^\d{4}-\d\d-\d\d$", x), "must match 'yyyy-mm-dd' format"],
        "X":                  [ Registry.setAsFloat, lambda x: x >= 0,        "must be greater or equal to 0"],
        "Y":                  [ Registry.setAsFloat, lambda x: x >= 0,        "must be greater or equal to 0"],
        "Z":                  [ Registry.setAsFloat, lambda x: x >= 0,        "must be greater or equal to 0"],
        "TIME":               [ Registry.setAsInt,   lambda x: x > 0,         "must be greater than 0"],
        "VOLUME_USED":        [ Registry.setAsFloat, lambda x: x > 0,         "must be greater than 0"],
        "GUID":               [ lambda reg, key, x: reg.set(key, uuid.UUID(x)), lambda x: True, "must be a valid GUID/UUID" ],
        "DIAMETER":           [ Registry.setAsFloat, lambda x: x > 0,         "must be greater than 0"],
        "FLAVOR":             [ Registry.setAsString,lambda x: x == "Griffin","must be 'Griffin'"]
    }
    PARSE_SET_FUNC = 0
    PARSE_TEST_FUNC = 1
    PARSE_TEST_TEXT = 2

    # @param file_parser the fileHandler object
    def __init__(self, file_handler):
        self._file_handler = file_handler

    ## this function parses the header and returns a dictionary that is a mix of actual header seen and some basic defaults.
    # If no header is seen the defaults are returned, for instance the HEADER_VERSION will be set to -1 which can be used to
    # give a warning OR error to the user and/or to restore normal behaviour.
    # @return A dictionary containing the parsed header settings or the defaults or a combination thereof.
    def parseMetaData(self):
        # first initialize the header with some defaults
        header = copy.deepcopy(GCodeMetaDataContainer.EMPTY_HEADER)
        for i in range(0, Controller.getInstance().getPropertyValue("hotend_count")):
            header["EXTRUDER_TRAIN"][(str(i))] = copy.deepcopy(GCodeMetaDataContainer.EMPTY_EXTRUDER_TRAIN)

        # make the header dictionary a registry!
        header = Registry(header)

        with self._file_handler.openFileStream() as f:
            header_started = False

            for idx, line in enumerate(f):
                # parse some items outside of header so print time estimates work for old Cura generated files.
                if line.startswith(";TIME:"):
                    header.setAsBoolean(GCodeMetaDataContainer.HAS_OLD_PRINT_TIME, True)
                    header.getAsRegistry("PRINT").setAsInt("TIME", int(line[line.index(':') + 1:].strip()))
                elif line.upper().startswith(";LAYER_COUNT") or line.upper().startswith(";LAYER COUNT"):
                    header.setAsBoolean(GCodeMetaDataContainer.HAS_LAYER_COUNT, True)
                    header.setAsInt("LAYER_COUNT", int(line[line.index(':') + 1:].strip()))
                elif line.startswith(";FLAVOR:"):
                    key_value = line[1:].split(":")
                    keys = key_value[0].split(".")
                    value = key_value[1].strip()
                    self._setHeader(header, keys, value)

                if not header_started:
                    if line.startswith(";START_OF_HEADER"):
                        header_started = True
                    elif idx > self.MAX_LINES_TILL_START_OF_HEADER:
                        break
                else:
                    if idx > self.MAX_LINES_TILL_END_OF_HEADER or line.startswith(";END_OF_HEADER"):
                        break
                    if line.startswith(";"):
                        key_value = line[1:].split(":")
                        keys = key_value[0].split(".")
                        value = key_value[1].strip()
                        self._setHeader(header, keys, value)

        header.set("FILE_SIZE", self._file_handler.getFileSize())

        metadata = GCodeMetaDataContainer(header)
        log.info("header %s", str(header))

        self._file_handler.setGcodeMetadata(metadata)

        return metadata

    ## Helper function for the header parsing, Sets the header struct based on an array of keys
    # @param local_header must be a dictionary which will be the output of parsing.
    # @param keys an array of keys if keys is ["a", "b"] then the value is stored in header["a"]["b"] = value
    # @param value The value to be stored.
    # @param previous_key is an internal parameter used in recursive calls.
    # @param previous_local_header is an internal parameter used in recursive calls.
    # @return header dict or value
    def _setHeader(self, local_header, keys, value, previous_key=None, previous_local_header=None):
        if keys: # empty list is treated as false
            key_value = local_header.get(keys[0])
            if key_value is None:
                key_value = dict()
                local_header.set(keys[0], key_value) # this may be overwritten by an actual value
            if isinstance(key_value, dict):
                key_value = local_header.getAsRegistry(keys[0])

            self._setHeader(key_value, keys[1:], value, keys[0], local_header)  # recursive call
            return local_header
        else:
            try:
                if previous_key in self.PARSE_AS:
                    self.PARSE_AS[previous_key][self.PARSE_SET_FUNC](previous_local_header, previous_key, value)
                    parsed_value = previous_local_header.get(previous_key)
                    log.debug("%s returns %s of type %s", previous_key, str(parsed_value), str(type(parsed_value)))
                    if parsed_value is None:
                        raise ValueError("failed to parse")
                    if not self.PARSE_AS[previous_key][self.PARSE_TEST_FUNC](parsed_value):
                        raise ValueError("test '%s %s' failed" % (value, self.PARSE_AS[previous_key][self.PARSE_TEST_TEXT]))
                    return parsed_value
                raise ValueError("can't parse to unknown key")
            except Exception as e:
                if Controller.getInstance().getProcedure("PRINT").isActive():
                    msg = "%s for %s with '%s' as input" % (str(e), previous_key, value)
                    FaultHandler.warning(msg, FaultCode.HEADER_MISSING_ITEM)
                    raise ValueError(msg)
