import os.path
from .gcodeFileHandlerFactory import GCodeFileHandlerFactory
from .gzipgcodeFileHandlerFactory import GzipGCodeFileHandlerFactory
#from .modelFileHandler import ModelFileHandler

## Create file handler factory based on extension
#  @param string filename The filename
#  @return The filehandlerFactory
def createFileHandlerFactory(filename):
    ext = os.path.splitext(filename)[1].upper()
    if ext == ".GCODE" or ext == ".G":
        return GCodeFileHandlerFactory(filename)
    if ext == ".GZGCODE" or ext == ".GZ":
        return GzipGCodeFileHandlerFactory(filename)
    #if ext == ".STL" or ext == ".OBJ":
    #    return ModelFileHandlerFactory()
    return None

## Returns the list of supported extensions
# @return The list of extensions
def getSupportedFileTypes():
    return [ ".gcode", ".g", ".gzgcode", ".gcode.gz" ]
