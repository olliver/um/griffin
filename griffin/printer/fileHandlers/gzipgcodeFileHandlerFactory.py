from .fileHandlerFactory import FileHandlerFactory
from .gzipgcodeFileHandler import GzipGCodeFileHandler
from .gcodeMetaDataHandler import GCodeMetaDataHandler

class GzipGCodeFileHandlerFactory(FileHandlerFactory):
    # @param file_path The complete path to the file to handle.
    def __init__(self, file_path):
        self._file_handler = GzipGCodeFileHandler(file_path)
        self._metadata_handler = GCodeMetaDataHandler(self._file_handler)
