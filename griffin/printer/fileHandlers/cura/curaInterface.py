import logging
import subprocess
import json
import os

from griffin import thread
#from . import Cura_pb2
from .fileReaders.stl import STLReader
from griffin import signal
#import Arcus

log = logging.getLogger(__name__.split(".")[-1])


class CuraInterface(signal.SignalEmitter):
    instance = None

    def __init__(self):
        super().__init__()

        self._engine_executable_name = "/usr/bin/CuraEngine"
        self._process = None

        self._port_nr = 49674
        self._socket = None

        self._createSocket()

    onError = signal.Signal()
    onFinished = signal.Signal()
    onGCodeData = signal.Signal()

    @classmethod
    def getInstance(cls):
        if cls.instance is None:
            cls.instance = CuraInterface()
        return cls.instance

    def _createSocket(self):
        pass
        #if self._socket:
        #    # Close the old socket on a thread to prevent the close from blocking.
        #    thread.Thread("Close old socket", self._socket.close).start()
        #
        #self._socket = Arcus.Socket()
        #self._socket.setStateChangedCallback(self._socketStateChanged)
        #self._socket.setMessageReceivedCallback(self._socketMessage)
        #self._socket.setErrorCallback(self._socketError)
        #self._socket.listen("127.0.0.1", self._port_nr)

        #self._socket.registerMessageType(1, Cura_pb2.ObjectList)
        #self._socket.registerMessageType(2, Cura_pb2.SlicedObjectList)
        #self._socket.registerMessageType(3, Cura_pb2.Progress)
        #self._socket.registerMessageType(4, Cura_pb2.GCodeLayer)
        #self._socket.registerMessageType(5, Cura_pb2.ObjectPrintTime)
        #self._socket.registerMessageType(6, Cura_pb2.SettingList)
        #self._socket.registerMessageType(7, Cura_pb2.GCodePrefix)

    def _socketStateChanged(self, state):
        log.info("Socket state changed: %s", state)
        if state == Arcus.Socket.ListeningState:
            self._startEngineProcess()

    def _socketMessage(self):
        message = self._socket.takeNextMessage()
        if type(message) == Cura_pb2.SlicedObjectList:
            self.onFinished.emit()
        elif type(message) == Cura_pb2.Progress:
            pass
        elif type(message) == Cura_pb2.GCodeLayer:
            self.onGCodeData.emit(message.data.decode("utf-8", "replace"))
        else:
            log.info("Socket message %s", type(message))


    def _socketError(self, error):
        if hasattr(error. errno) and error.errno == 98: #Socket already in use
            self._port_nr += 1
            self._createSocket()
        elif hasattr(error. errno) and (error.errno == 104 or error.errno == 32):
            self._createSocket()
            self.onError.emit("Engine crashed or terminated")
        else:
            log.error("Socket error: %s", error)
            self.onError.emit(error)

    def _startEngineProcess(self):
        command_list = [self._engine_executable_name]
        command_list += ["-j", os.path.join(os.path.dirname(__file__), "fdmprinter.json"), "-vv", "--connect", "127.0.0.1:%d" % (self._port_nr)]
        #command_list = ["gdbserver", "127.0.0.1:12345"] + command_list
        self.abortSlice()

        self._process = subprocess.Popen(command_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        t = thread.Thread("Read stdout", self._readOutputThread, args=(self._process.stdout,))
        t.daemon = True
        t.start()
        t = thread.Thread("Read stderr", self._readOutputThread, args=(self._process.stderr,))
        t.daemon = True
        t.start()

        if self._process is None:
            self.onError.emit("Failed to start engine process")

    def startSlice(self, filenames, parameters):
        profile = "default"
        if "profile" in parameters:
            profile = parameters["profile"]
        with open("/var/lib/griffin/profile/%s.json" % profile) as f:
            settings = json.load(f)

        msg = Cura_pb2.SettingList()
        for key, value in settings.items():
            s = msg.settings.add()
            s.name = key
            s.value = str(value).encode("utf-8")
        self._socket.sendMessage(msg)

        msg = Cura_pb2.ObjectList()
        index = 0
        for filename in filenames:
            mesh = STLReader().read(filename)
            obj = msg.objects.add()
            obj.id = 1
            obj.vertices = mesh.getVertices().tostring()

            #Cheat by setting the extruder to the mesh index. Not best solution, but works for now.
            s = obj.settings.add()
            s.name = "extruder_nr"
            s.value = str(index).encode("utf-8")
            index += 1
        self._socket.sendMessage(msg)

    def abortSlice(self):
        if self._process is not None:
            self._process.terminate()
            self._process.wait()
            self._process = None

    def _readOutputThread(self, stream):
        while True:
            line = stream.readline()
            if line == b"":
                return
            log.info("Log: %s" % (line.decode("utf-8", "replace").strip()))
        log.info("Engine process finished.")
