import logging
import os

from .schemeHandler import SchemeHandler

log = logging.getLogger(__name__.split(".")[-1])


class FileSchemeHandler(SchemeHandler):
    def run(self, url_data):
        if os.path.isfile(url_data.path):
            self.onFinished.emit(url_data.path)
        else:
            # TODO: Proper error handling
            raise ValueError()
