from .fileSchemeHandler import FileSchemeHandler
from .httpSchemeHandler import HttpSchemeHandler
from .uploadSchemeHandler import UploadSchemeHandler


def createSchemeHandler(scheme):
    if scheme == "file":
        return FileSchemeHandler()
    elif scheme == "http":
        return HttpSchemeHandler()
    elif scheme == "https":
        return HttpSchemeHandler()
    elif scheme == "upload":
        return UploadSchemeHandler()
    return None
