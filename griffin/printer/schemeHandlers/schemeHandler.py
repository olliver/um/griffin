from griffin import signal
from griffin import thread

import logging

log = logging.getLogger(__name__.split(".")[-1])

# URL Scheme handler is a base class for retrieving a printable file, for example over HTTP or from local storage.
class SchemeHandler(signal.SignalEmitter):
    onFinished = signal.Signal()

    def __init__(self):
        pass

    def start(self, url_data):
        thread.Thread("SchemeHandler", self._run, (url_data,)).start()

    def _run(self, url_data):
        try:
            self.run(url_data)
        except:
            log.exception("Exception in scheme handling")
            # TOFIX: Right now just tell we are done
            self.onFinished.emit("")

    def run(self, url_data):
        raise NotImplementedError()

    def cleanup(self):
        pass # Override in subclass when needed.

    def getMetaData(self):
        return {
            "progress": float(-1.0),
            "time": int(-1),
            "time_total": int(-1),
        }
