import logging
import os

from .schemeHandler import SchemeHandler

log = logging.getLogger(__name__.split(".")[-1])


class UploadSchemeHandler(SchemeHandler):
    def run(self, url_data):
        self._filename = None
        if os.path.isfile(url_data.path):
            self._filename = url_data.path
            self.onFinished.emit(url_data.path)
        else:
            # TODO: Proper error handling
            raise ValueError()

    def cleanup(self):
        if self._filename is not None:
            try:
                os.unlink(self._filename)
            except:
                pass
