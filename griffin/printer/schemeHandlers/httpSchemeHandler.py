import logging
import os
import urllib.request
import urllib.error
import urllib.parse

from .schemeHandler import SchemeHandler

log = logging.getLogger(__name__.split(".")[-1])


class HttpSchemeHandler(SchemeHandler):
    def run(self, url_data):
        log.info("getting url: %s Referer: %s\n", url_data.geturl(), url_data[1])
        try:
            # TODO: Set security certificates for https connections
            req = urllib.request.Request(url_data.geturl())
            req.add_header("Referer", url_data[1])
            # req.add_header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko")
            response = urllib.request.urlopen(req)
            # response = urllib.request.urlopen(url_data.geturl())
        except urllib.error.URLError:
            self.error.emit("Failed to fetch file")
            return
        if response.getcode() != 200:
            self.error.emit("Failed to fetch file")
            return
        url_data = urllib.parse.urlparse(response.geturl())
        basename = os.path.basename(url_data.path)
        # TODO: Get length from response.info for download progress
        # log.info("response.info\n%s\n", response.info() )
        self._tempfile = "/tmp/download_%s" % (basename)
        try:
            self._file_handle = open(self._tempfile, "wb")
            while True:
                data = response.read(2048)
                if len(data) == 0:
                    break
                self._file_handle.write(data)
            self._file_handle.close()
        except OSError:
            self._file_handle.close()
            self.error.emit("Failed to fetch file")
            return
        self.onFinished.emit(self._tempfile)

    def abort(self):
        if self._file_handle is not None:
            self._file_handle.close()

    def cleanup(self):
        if self._tempfile is not None:
            try:
                os.unlink(self._tempfile)
            except:
                pass
