from griffin.printer.properties.propertyContainer import PropertyContainer
from griffin.printer.properties.basicProperty import BasicProperty
from griffin.printer.properties.constrainedProperty import ConstrainedProperty
from griffin.printer.properties.systemProvidedProperty import SystemProvidedProperty
import logging

log = logging.getLogger(__name__.split(".")[-1])


## Base class for heatable objects.
#  A heatable object is a thing that can be heated. This means it has the following properties:
#  * "current_temperature": the current temperature, this is read-only.
#  * "target_temperature": the actual target temperature, this is read-only.
#  * "pre_tune_target_temperature": the target temperature as requested. Usually requested by a print.
#  * "tune_offset_temperature": a tuning offset supplied by the user. This offset is applied on the
#     pre_tune_target to get to the actual target temperature.
class HeatableObject:
    ## Allowed difference between the current temperature and the target temperature for the heatable object
    # to accept the temperature reached.
    __ALLOWED_TEMPERATURE_DIFFERENCE = 2.0
    ## Range of tuning the temperature, maximum allowed to tune the temperature up/down 25C, to prevent people
    # tuning a huge offset when they assume they are setting the temperature directly.
    __TUNING_RANGE = 25.0

    ## Create a new heatable object.
    #  @param name: string which can be used to identify this heatable object for debugging and logging.
    #  @param pid_settings_dictionary, the dictionary defined in the machine definition to be used as the default values for the pid_* properties
    def __init__(self, name, pid_settings_dictionary):
        self.__name = name
        self._lowest_possible_target_temperature = 40.0
        self._allowed_temperature_difference_low = HeatableObject.__ALLOWED_TEMPERATURE_DIFFERENCE
        self._allowed_temperature_difference_high = HeatableObject.__ALLOWED_TEMPERATURE_DIFFERENCE

        self.__property_container = PropertyContainer()
        # Current temperature of this heatable object.
        self.__current_property = SystemProvidedProperty("current_temperature", 0.0)
        # Current target temperature after applying the tuning offset and limits.
        self.__target_property = SystemProvidedProperty("target_temperature", 0.0)
        # Maximum target temperature possible. Initialised on 350C, but can be modified by the system later on.
        # The target temperature is clamped to this value, so it is not possible to set a temperature higher than this max_target.
        self.__max_target_property = SystemProvidedProperty("max_target_temperature", 350.0)

        self.__pre_tune_property = ConstrainedProperty("pre_tune_target_temperature", 0.0, lambda value: 0.0 <= value <= self.__max_target_property.get())
        self.__tune_offset_property = ConstrainedProperty("tune_offset_temperature", 0.0, lambda value: -self.__TUNING_RANGE <= value <= self.__TUNING_RANGE)

        # When the tune_offset or pre_tune_target changes, update our target temperature.
        self.__pre_tune_property.onChange.connect(self.__updateTarget)
        self.__tune_offset_property.onChange.connect(self.__updateTarget)

        self._addProperty(self.__current_property)
        self._addProperty(self.__target_property)
        self._addProperty(self.__pre_tune_property)
        self._addProperty(self.__tune_offset_property)
        self._addProperty(self.__max_target_property)

        # Properties for the PID controller.
        self._addProperty(BasicProperty("pid_Kff", pid_settings_dictionary.get("Kff", 0.0)))
        self._addProperty(BasicProperty("pid_Kp", pid_settings_dictionary.get("Kp", 0.0)))
        self._addProperty(BasicProperty("pid_Ki", pid_settings_dictionary.get("Ki", 0.0)))
        self._addProperty(BasicProperty("pid_Ki_max", pid_settings_dictionary.get("Ki_max", 255.0)))
        self._addProperty(BasicProperty("pid_Kd", pid_settings_dictionary.get("Kd", 0.0)))
        self._addProperty(BasicProperty("pid_Kpcf", pid_settings_dictionary.get("Kpcf", 0.0)))
        self._addProperty(BasicProperty("pid_Ke", pid_settings_dictionary.get("Ke", 0.0)))
        self._addProperty(BasicProperty("pid_functional_range", 25.0))

    def getPropertyContainer(self):
        return self.__property_container

    ## Return the name of this heatable object.
    #  @return string: name of this heatable object, for logging and debugging goals.
    def getName(self):
        return self.__name

    ## Private function to add properties. Subclasses may add extra properties to a heatable object.
    #  @param property: Instance of subclasses of the griffin.printer.properties.property.Property class.
    def _addProperty(self, property):
        self.__property_container.addProperty(property)

    ## Check if we have reached our target temperature.
    # This function returns true if the current temperature is within +- 2C of the target, OR if we are cooling down, and we are cool enough,
    # we also report that we have reached our target.
    # @return bool: True if our target is reached.
    def isTargetTemperatureReached(self):
        current_temperature = self.__current_property.get()
        target_temperature = self.__target_property.get()
        if target_temperature < self._lowest_possible_target_temperature and current_temperature < self._lowest_possible_target_temperature:
            return True
        if target_temperature - self._allowed_temperature_difference_low <= current_temperature <= target_temperature + self._allowed_temperature_difference_high:
            return True
        return False

    # @return an instance of the Property class
    def getPropertyAsObject(self, key):
        return self.__property_container.get(key)

    ## Directly get a property value, shorthand function for getPropertyAsObject(key).get()
    # @param key: Name of the property to set
    # @return: The value stored in the property.
    def getPropertyValue(self, key):
        return self.__property_container.get(key).get()

    ## Directly set a property value, shorthand function for getPropertyAsObject(key).set(value)
    # @param key: Name of the property to set
    # @param value: New value to store in the property
    # @return bool: True on success, False on failure.
    def setPropertyValue(self, key, value):
        return self.__property_container.get(key).set(value)

    ## Callback when the target temperature needs to be updated. Call from multiple property changes
    # @param updated_property: Ignored, required parameter from property update callback
    # @param updated_value: Ignored, required parameter from property update callback
    def __updateTarget(self, updated_property, updated_value):
        pre_tune_target = self.__pre_tune_property.get()
        tune_offset = self.__tune_offset_property.get()

        # If we try to set to 0C as target, we want the the heater to turn off. So do not apply the tuning offset.
        if pre_tune_target == 0.0:
            self.__target_property.setBySystem(pre_tune_target)
        else:
            target = pre_tune_target + tune_offset
            target = max(0.0, target)
            target = min(self.__max_target_property.get(), target)
            self.__target_property.setBySystem(target)

    ## This method derives the temperature from the arguments passed to the function and the material profile..
    #  This function should be overridden in the subclasses.
    # @param kwargs A dictionary, the arguments passed to the setup function of a procedure.
    def deriveTemperature(self, **kwargs):
        return -1
