from .heatableObject import HeatableObject
from griffin.printer.properties.nonVolatileProperty import NonVolatileProperty
from griffin.printer.properties.constrainedProperty import ConstrainedProperty
import logging

log = logging.getLogger(__name__.split(".")[-1])


## A class representing the heated print bed.
#  This class is a subclass of the HeatableObject, which means is has all the temperature properties.
class HeatablePrintbed(HeatableObject):
    # Temperature target range, we need to be very close to our target temperature when heating up.
    # Because our power supply browns out if we are not PWMing the bed when we put on the hotends to 100% heat.
    # See EM-739 for details.
    __TEMPERATURE_TARGET_REACHED_LOW = 0.1
    __TEMPERATURE_TARGET_REACHED_HIGH = 5.0

    ## Create a new heatable bed object.
    # @param controller: Reference to the printer service controller.
    # @param bed_registry: Reference to the registry where the bed should store its preferences.
    def __init__(self, controller, bed_registry):
        super().__init__("bed", controller.getPropertyValue("default_bed_pid"))

        self.__controller = controller

        # Bind ourselves to intercept M140 and send a M140 to the driver when we need to update the target temperature.
        controller.getGCodeProcessor().addGCodeMapping("M140", self.__handleTemperatureSetCommand)

        # Bind ourselves to temperature updates from the driver.
        controller.getDriver().onBedTemperatureUpdate.connect(self.__onTemperatureUpdateFromDriver)

        # Set the maximum temperature of the bed to a static 115C. The bed of the Jedi almost cannot get hotter than this,
        # so no need to have a higher temperature than this.
        self.getPropertyAsObject("max_target_temperature").setBySystem(115.0)

        self.__skew_x = NonVolatileProperty(bed_registry , "SKEW_X", 0)
        self.__skew_y = NonVolatileProperty(bed_registry , "SKEW_Y", 0)

        self._addProperty(self.__skew_x)
        self._addProperty(self.__skew_y)
        self._addProperty(ConstrainedProperty("nominal_resistance", controller.getPropertyValue("default_power_budget_management")["nominal_bed_resistance"], lambda value: 0 < value <= 1000)) # should not be 0 and more then 1000 ohm seems unlikely/illogical
        self._addProperty(ConstrainedProperty("resistance_per_degree", controller.getPropertyValue("default_power_budget_management")["bed_resistance_per_degree"], lambda value: -1 <= value <= 1)) # since this value tends to be less then 0.005 ohm/degC this seems like it should be fine.
        self._addProperty(ConstrainedProperty("voltage", controller.getPropertyValue("default_power_budget_management")["bed_voltage"], lambda value: 1 <= value <= 1000))

        self._allowed_temperature_difference_low = HeatablePrintbed.__TEMPERATURE_TARGET_REACHED_LOW
        self._allowed_temperature_difference_high = HeatablePrintbed.__TEMPERATURE_TARGET_REACHED_HIGH

    ## Callback from the gcode processor when an M140 is encountered. This sets the pre_tune_target temperature.
    #  The heatable object class will set the propert target temperature property.
    def __handleTemperatureSetCommand(self, gcode_line):
        s_value = gcode_line.getValue("S", default=None)
        if s_value is not None:
            self.setPropertyValue("pre_tune_target_temperature", s_value)

    ## Callback from the driver when the current temperate of the bed is updated.
    #  Update our internal property with this new temperature.
    def __onTemperatureUpdateFromDriver(self, temperature):
        self.getPropertyAsObject("current_temperature").setBySystem(temperature)

    ## This method derives the temperature from the arguments passed to the function and the material profile..
    # @param args A dictionary, the arguments passed to the setup function of a procedure.
    def deriveTemperature(self, target=None, **kwargs):
        result_target = self.__controller.getMaterialManager().getRequiredBedTemperature()
        if target is not None:
            if target == "initial_temperature":
                printProcedure = self.__controller.getProcedure("PRINT")
                if printProcedure.isActive():
                    metadata = printProcedure.getGcodeMetaData() # getGcodeMetaData() raises exception when None
                    result_target = metadata.getInitialBuildplateTemperature()
            else:
                result_target = float(target)

        return result_target
