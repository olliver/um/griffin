from .heatableObject import HeatableObject
from griffin.preferences.registryFile import RegistryFile
from griffin.printer.properties.basicProperty import BasicProperty
from griffin.printer.properties.constrainedProperty import ConstrainedProperty
from griffin.printer.properties.nonVolatileProperty import NonVolatileProperty
from griffin.printer.properties.nonVolatileSmartReferenceProperty import NonVolatileSmartReferenceProperty
from griffin.printer.properties.readOnlyProperty import ReadOnlyProperty
from griffin.printer.properties.property import Property
from griffin.printer.properties.systemProvidedProperty import SystemProvidedProperty

from griffin import dbusif
import logging
import uuid
from builtins import isinstance

log = logging.getLogger(__name__.split(".")[-1])


## Class to represent a hotend in a slot.
#  This object handles the temperature handle as per the base class HeatableObject,
#  as well as the enabling/disable and present detection of the hotend.
#  It also keeps track of the currently inserted material GUID.
class HeatableHotendSlot(HeatableObject):
    __PREFERENCES_FILE = "printer_hotend_%d_preferences.json"
    # Range in which we decide that the target temperature is reached.
    # Our top boundary is larger than our bottom boundary as we cool down quite slow and do not want to wait too long for cooling down.
    __TEMPERATURE_TARGET_REACHED_LOW = 3.0
    __TEMPERATURE_TARGET_REACHED_HIGH = 5.0

    ## Create a new heatable hotend.
    # @param controller The print controller object.
    # @param printhead_controller The print head controller object.
    # @param nozzle_offset_registry A subset of the normal registry where nozzle offsets(x,y and Z) are stored.
    # @param index The index number of the hotend.
    def __init__(self, controller, printhead_controller, nozzle_offset_registry, index):
        super().__init__("hotend-%d" % (index), controller.getPropertyValue("default_hotend_pid"))

        self.__index = index
        self.__controller = controller
        self.__material_service = dbusif.RemoteObject("material")
        self.__registry = RegistryFile(self.__PREFERENCES_FILE % (index))

        ## remove this when migration from old dictionary layout is no longer required.
        self.__nozzle_offset_registry = nozzle_offset_registry

        # Is the hotend enabled, this means the hotend can be heated by the hardware, and cannot be removed from the system right now.
        self.__enabled_property = SystemProvidedProperty("is_enabled", False)
        # Is the hotend present, this means the hotend is present in the system, if false, there is no hotend, and thus it is not possible to do something with it.
        self.__present_property = SystemProvidedProperty("is_present", False)

        # Property to store the current state of retraction/ primed vs not primed of the nozzle.
        self.__primed_property = NonVolatileProperty(self.__registry, "is_primed", True) # assume the nozzle is primed if unknown since this will be fixed if it is empty by the load material, etc.

        # Property to store the selected material GUID, can be an empty string or a GUID in the uuid.UUID4 format as a string.
        self.__material_property = NonVolatileProperty(self.__registry, "material_guid", "")

        self.__z_height_property = NonVolatileSmartReferenceProperty(nozzle_offset_registry, "z_height", "")
        if index == 0: # This index is the reference for all other xy offsets!
            self.__x_offset_property = ReadOnlyProperty("x_offset", 0)
            self.__y_offset_property = ReadOnlyProperty("y_offset", 0)
        else:
            self.__x_offset_property = NonVolatileSmartReferenceProperty(nozzle_offset_registry, "x_offset", "")
            self.__y_offset_property = NonVolatileSmartReferenceProperty(nozzle_offset_registry, "y_offset", "")
        printhead_controller.getHotendCartridgeManager().onHotendDataReady.connect(self.__onHotendDataReady)

        # Property to store the source that provided the GUID. Possible values: "USER", "RFID"
        self.__material_source_property = NonVolatileProperty(self.__registry, "material_guid_source", "")
        self.__material_property.addConstraint(self.__checkMaterialValidBeforeSet)

        self.__interaction_required_property = BasicProperty("interaction_required", False)

        self._addProperty(self.__enabled_property)
        self._addProperty(self.__present_property)
        self._addProperty(self.__primed_property)
        self._addProperty(self.__material_property)
        self._addProperty(self.__z_height_property)
        self._addProperty(self.__x_offset_property)
        self._addProperty(self.__y_offset_property)
        self._addProperty(self.__material_source_property)
        self._addProperty(self.__interaction_required_property)
        self._addProperty(ConstrainedProperty("max_power", self.__controller.getPropertyValue("default_power_budget_management")["max_power_per_hotend_slot"], lambda value: 0 <= value <= 100))
        self._addProperty(ConstrainedProperty("voltage", self.__controller.getPropertyValue("default_power_budget_management")["hotend_voltage"], lambda value: 0 <= value <= 48))

        # The following properties will be filled by with eeprom content.
        self._addProperty(SystemProvidedProperty("manufacturer_id", ""))
        self._addProperty(SystemProvidedProperty("hotend_cartridge_id", ""))
        self._addProperty(SystemProvidedProperty("hardware_revision", ""))
        self._addProperty(SystemProvidedProperty("nozzle_size", ""))
        self._addProperty(SystemProvidedProperty("hot_zone_size", ""))
        self._addProperty(SystemProvidedProperty("maximum_temperature", ""))
        self._addProperty(SystemProvidedProperty("filament_size", ""))
        self._addProperty(SystemProvidedProperty("angle", ""))
        self._addProperty(SystemProvidedProperty("flat_size", ""))
        self._addProperty(SystemProvidedProperty("insert_type", ""))
        self._addProperty(SystemProvidedProperty("nominal_resistance", ""))
        self._addProperty(SystemProvidedProperty("last_material", ""))
        self._addProperty(SystemProvidedProperty("material_extruded", ""))
        self._addProperty(SystemProvidedProperty("time_spend_hot", ""))
        self._addProperty(SystemProvidedProperty("max_exp_temperature", ""))
        self._addProperty(SystemProvidedProperty("serial_number", ""))

        printhead_controller.getHotendCartridgeManager().onHotendDataChanged.connect(self.__onHotendDataChanged)

        if controller.getPropertyValue("feature_filament_flow_sensor"):
            # Property that enables/disables the flow sensor. This setting is persistent.
            self.__flow_sensor_enabled = NonVolatileProperty(self.__registry, "enable_flow_errors", True)
            # Property that will suppress flow sensor errors till a certain time.
            # The time in a unix-timestamp with can be obtained with time.time() in python or the time(nullptr) function in C
            self.__flow_sensor_suppress = ConstrainedProperty("ignore_flow_errors_until", 0.0, lambda n: n >= 0.0)

            self._addProperty(self.__flow_sensor_enabled)
            self._addProperty(self.__flow_sensor_suppress)
        else:
            self.__flow_sensor_enabled = None
            self.__flow_sensor_suppress = None

        self.getPropertyAsObject("target_temperature").onChange.connect(self.__onTargetUpdate)

        # Attach to driver events to update our properties.
        driver = controller.getDriver()
        driver.onHotendTemperatureUpdate.connect(self.__onTemperatureUpdateFromDriver)
        driver.onCartridgeEnabledChanged.connect(self.__onEnabledChangedFromDriver)
        driver.onCartridgePresentChanged.connect(self.__onPresentChangedFromDriver)

        self._allowed_temperature_difference_low = HeatableHotendSlot.__TEMPERATURE_TARGET_REACHED_LOW
        self._allowed_temperature_difference_high = HeatableHotendSlot.__TEMPERATURE_TARGET_REACHED_HIGH

    def __checkMaterialValidBeforeSet(self, guid):
        # guid must be either an empty string or a valid GUID.

        if guid == "":
            return True

        try:
            parsed_guid = uuid.UUID(guid)
        except ValueError:
            return False

        return True

    ## Check if we have reached our target temperature.
    # This function returns true if the current temperature is within +- 2C of the target, OR if we are cooling down, and we are cool enough,
    # we also report that we have reached our target.
    # @return bool: True if our target is reached.
    def isTargetTemperatureReached(self):
        if self.getPropertyValue("target_temperature") == 0.0 and not self.__enabled_property.get():
            return True
        return super().isTargetTemperatureReached()

    ## Callback when the target temperature is updated.
    #  We use this to send the M104 to our driver to actually update the hardware.
    #  We also use this to check if we need to enable the hotend, which happens if we set a target temperature above 0 and the hotend is not enabled yet.
    # @param property: The property object that is updated, in this case always the "target_temperature" property of ourselves.
    # @param value: float value containing the new target temperature for this hotend.
    def __onTargetUpdate(self, property, value):
        if value > 0 and not self.__enabled_property.get() and self.__present_property.get():
            log.info("Going to enable hotend %d to %gC", self.__index, value)
            # Disable the temperature error suppression, so we can heatup the hotend. (will generate a max temp error if no hotend is present)
            self.__controller.queueRawGCode("M145 T%d" % (self.__index))
            # Force a target temperature update after the M145, Marlin does not accept the M104 when the hotend is disabled.
            # And the normal M104 mechanism can send the M104 before the M145 is send.
            self.__controller.queueRawGCode("M104 T%d S%d" % (self.__index, value))

    ## Callback when we have a temperature update from the driver.
    #  We use this to set our "current_temperature" property, and to check if we need to disable the hotend.
    #  @param hotend_index: integer, index of the hotend of which we get a temperature update (from the driver)
    #  @param temperature: float, new temperature value.
    def __onTemperatureUpdateFromDriver(self, hotend_index, temperature):
        if hotend_index == self.__index:
            self.getPropertyAsObject("current_temperature").setBySystem(temperature)
            self.__checkForDisable()

    ## Callback from the driver when an enabled update is received. Update the enabled property when this is applicable for this hotend slot.
    #  @param hotend_index: integer, index of the hotend of which we get a temperature update (from the driver)
    #  @param enabled: bool, true if the hotend is now enabled, false if not.
    def __onEnabledChangedFromDriver(self, hotend_index, enabled):
        if hotend_index == self.__index:
            self.__enabled_property.setBySystem(enabled)

    ## Callback from the driver when a present update is received. Update the present property when this is applicable for this hotend slot.
    #  @param hotend_index: integer, index of the hotend.
    #  @param present: bool, true if the hotend is now present, false if not.
    def __onPresentChangedFromDriver(self, hotend_index, present):
        if hotend_index == self.__index:
            self.__present_property.setBySystem(present)
            if not present:
                self.__z_height_property.referenceChanged(None)

        self.__referenceToHotendsChanged()

    def __onHotendDataReady(self, hotend_nr):
        if hotend_nr == self.__index:
            hotend_cartridge_manager = self.__controller.getPrintHeadController().getHotendCartridgeManager()
            if hotend_cartridge_manager.isPresent(hotend_nr):
                serial = hotend_cartridge_manager.getSerial(hotend_nr)
                self.__z_height_property.referenceChanged(serial + "-" + str(hotend_nr))

        self.__referenceToHotendsChanged()

    def __referenceToHotendsChanged(self):
        # Only update references for writable Properties
        if self.__index == 0:
            return

        combo = self.__controller.getPrintHeadController().getNozzleCombo(self.__index)
        if "None" in combo:
            combo = None # prevents storing of actual values when it really should not be possible in this case!
        self.__x_offset_property.referenceChanged(combo)
        self.__y_offset_property.referenceChanged(combo)

    ## callback for the onHotendDataChanged signal of the hotend cartridge manager.
    def __onHotendDataChanged(self, hotend_nr):
        if hotend_nr != self.__index:
            return

        hotend_cartridge_manager = self.__controller.getPrintHeadController().getHotendCartridgeManager()
        eeprom_data = hotend_cartridge_manager.getAllValues(self.__index)
        if eeprom_data is None:
            return

        for property_key in self.getPropertyContainer().getPropertiesAsKeyValueDictionary().keys():
            if property_key in  eeprom_data:
                if "last_material" == property_key and eeprom_data[property_key] != "":
                    eeprom_data[property_key] = str(uuid.UUID(int=eeprom_data[property_key]))
                if "pid_" in property_key and eeprom_data[property_key] == 0:
                    continue

                property = self.getPropertyAsObject(property_key)
                if isinstance(property, SystemProvidedProperty):
                    property.setBySystem(eeprom_data[property_key])
                elif isinstance(property, Property):
                    # is the value is empty then set it to the default
                    if eeprom_data[property_key] != "":
                        property.set(eeprom_data[property_key])
                    else:
                        log.info("setting default on %s", property.getKey())
                        property.set(property.getDefault())

        # set the serial number so this is also available as a property of the hotend, no more need for functions to get this.
        serial = hotend_cartridge_manager.getSerial(self.__index)
        self.getPropertyAsObject("serial_number").setBySystem(serial if serial is not None else "")

    ## Check if we need to disable the hotend.
    #  When a hotend is present, enabled but the target is 0C, and we are below a human safe temperature, we can disable this hotend.
    #  This allows the user to remove this hotend to replace it, without the system throwing a major error because it thinks the hotend is broken.
    def __checkForDisable(self):
        if self.__present_property.get() and self.__enabled_property.get():
            if self.getPropertyValue("target_temperature") == 0.0 and self.getPropertyValue("current_temperature") < self.__controller.getPropertyValue("safe_human_touchable_hotend_temperature") - 1.0:
                log.info("Going to disable hotend %d", self.__index)
                self.__controller.sendRawGCode("M144 T%d" % self.__index)

    ## This method derives the temperature from the arguments passed to the function and the material profile..
    # @param target The input target temperature or temperature type string.
    def deriveTemperature(self, target=None, **kwargs):
        active_material_profile = self.__controller.getMaterialManager()

        result_target = active_material_profile.getPrintingTemperature(self.__index)
        maximum = active_material_profile.getMaximumTemperature(self.__index)

        if target is not None:
            if target == "printing_temperature":
                result_target = active_material_profile.getPrintingTemperature(self.__index)
            elif target == "standby_temperature":
                result_target = active_material_profile.getStandbyTemperature(self.__index)
            elif target == "remove":
                result_target = active_material_profile.getHotendRemovalTemperature(self.__index)
            elif target == "initial_temperature":
                printProcedure = self.__controller.getProcedure("PRINT")
                if printProcedure.isActive():
                    result_target = printProcedure.getGcodeMetaData().getInitialTemperature(self.__index)   # can only go wrong if hotend > #hotends
            else:
                result_target = float(target)

        if result_target > 0:
            hotend_cartridge_manager = self.__controller.getPrintHeadController().getHotendCartridgeManager()
            if not hotend_cartridge_manager.isPresent(self.__index):
                raise RuntimeError("heating up a missing hotend")

        if maximum > 0:
            result_target = min(result_target, maximum)

        return result_target

    ## @brief Reset the heatable hotend settings, which is simply erasing the current settings (and file)
    def resetSettings(self):
        self.__registry.erase()


    ## This method derives the hotend number from the arguments passed to the function.
    # @param hotendNr The input target hotend number.
    # @param from_gcode Boolean indicating if this procedure is called from GCode.
    @classmethod
    def deriveHotendNr(cls, hotend_nr=None, from_gcode=None, **kwargs):
        from griffin.printer.controller import Controller
        controller = Controller.getInstance()

        if hotend_nr is not None:
            target_hotend = int(hotend_nr)
        elif from_gcode is not None:   # Plain Gcode is allowed to omit the hotend, then use active hotend.
            target_hotend = controller.getActiveHotend()
        else:
            raise ValueError("expected hotend_nr to be set")

        return target_hotend
