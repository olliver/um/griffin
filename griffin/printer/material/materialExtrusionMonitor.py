import logging
import threading
import time

from griffin.timer import Timer

log = logging.getLogger(__name__.split(".")[-1])

## @brief This class handles the monitoring of extruded materials.
# This is done by 2 ways:
# 1. Based on a timer, in case the amount extruded is very low
# 2. Based on the amount extruded, in case a lot of it being used
# In either case, the changed extruded amount (delta) is send using a signal to the controller
# This signal will only be send if the delta is bigger thern the minimal threshold
class MaterialExtrusionMonitor():
    ##< How often to send a signal in case of very low delta / hq printing in seconds
    __TIMER_INTERVAL_BETWEEN_UPDATES = 60
    ##< Threshold to send a signal after a minimum amount changed in mm
    __UPDATE_THRESHOLD = 10
    ##< Minimal threshold to emit (in case of timer interval update) in mm
    __MINIMAL_THRESHOLD = 0.001
    ##< Threshold to mark this delta update is being part of the previous one, timewise (seconds)
    __UPDATE_THRESHOLD_MAX_DELAY = 1

    ## @brief Setup the Material Extrustion Monitor
    #  @param controller The Nfc Controller instance
    #  @param hotend_index Which hotend / extruder train to monitor
    def __init__(self, controller, hotend_index):
        super().__init__()

        ##< The controller instance
        self.__controller = controller
        ##< For which hotend this monitor is used
        self.__hotend_index = hotend_index
        ##< The accumulated delta of the extrusion(s)
        self.__delta_extrusion = 0.0
        ##< Keep track of the usage time
        self.__usage_time = 0.0
        ##< Keep track of extrusion time estimate
        self.__last_extrusion_time = 0.0

        ##< When called from within this thread (class), the RLock will ensure that code will continue, in contrast to an external call (from a different thread)
        self.__lock = threading.RLock()

        # Start the timer
        self.__timer = Timer("MaterialExtrusionMonitorTimerHotend-" + str(hotend_index), MaterialExtrusionMonitor.__TIMER_INTERVAL_BETWEEN_UPDATES, self.__executeAction, "timer")
        self.__timer.setSingleShot(False)

    ## @brief Updates the amount extruded (or negative for retraction) material. If this is more
    #  @param delta The amount of material extruded/retracted since the last time
    def updateDelta(self, delta):
        with self.__lock:
            log.debug("updateDelta: adding %f", delta)
            self.__delta_extrusion += delta
            now = time.monotonic()
            time_delta = now - self.__last_extrusion_time
            if time_delta < MaterialExtrusionMonitor.__UPDATE_THRESHOLD_MAX_DELAY:
                self.__usage_time += time_delta
            self.__last_extrusion_time = now

            ## Not caring about retraction or extruding, calculate the minimal time estimated for this to take
            if self.__delta_extrusion > MaterialExtrusionMonitor.__UPDATE_THRESHOLD or self.__delta_extrusion < -MaterialExtrusionMonitor.__UPDATE_THRESHOLD:
                self.__executeAction("threshold")
                # Reset timer
                self.__timer.restart()
            else:
                # Start timer, but only if it was not started before
                if not self.__timer.isRunning():
                    self.__timer.start()

    ## @brief Executes the action to be performed. Protected and must be overriden by derived classes
    #  @param origin The caller id of this function
    def __executeAction(self, origin):
        log.info("Execute action initiated by %s", origin)
        # Quick lock to get the current delta and reset the counter to 0.0
        with self.__lock:
            current_delta = self.__delta_extrusion
            self.__delta_extrusion = 0.0
            current_usage_time = self.__usage_time
            self.__usage_time = 0.0
        # Only send the signal when there is data to send
        if current_delta > MaterialExtrusionMonitor.__MINIMAL_THRESHOLD or current_delta < -MaterialExtrusionMonitor.__MINIMAL_THRESHOLD:
            self.__controller.onExtrusionAmountChanged.emit(self.__hotend_index, current_delta, current_usage_time)

    ## @brief Gets some identification string used for thread naming and possible identification
    #  @return Returns a string to identify the kind of registry
    def _getRegistryId(self):
        return log.name + "_hotend_" + str(self.__hotend_index)
