# @package docstring
# Material manager module
#
# This module is responsible to get material profiles from the materialService

import uuid
import logging
from griffin import dbusif
from griffin.printer.faultHandler import FaultHandler, FaultCode

log = logging.getLogger(__name__.split(".")[-1])


# Interface to the MaterialService
class MaterialManager():
    ## Public string indicating hotend <-> material compatibility.
    COMPATIBLE = "COMPATIBLE"
    ## Public string indicating hotend <-> material of unknown compatibility.
    UNKNOWN_COMPATIBILITY = "UNKNOWN COMPATIBILITY"
    ## Public string indicating hotend <-> material incompatibility.
    INCOMPATIBLE = "INCOMPATIBLE"


    ## Default printing temperature when no print temperature is specified in the material profile.
    #  This is used as backup when no material or an unknown material is currently in the hotend but we still want to warm up.
    __DEFAULT_PRINTING_TEMPERATURE = 200.0
    ## Default heated bed temperature when no bed temperature is specified in any of the material profiles.
    __DEFAULT_HEATED_BED_TEMPERATURE = 0.0

    def __init__(self, controller):
        self.__controller = controller
        self.__material_service = dbusif.RemoteObject("material")

        hotend_count = controller.getPropertyValue("hotend_count")

        self.__material_name = [("?", "?", "?")] * hotend_count
        self.__material_settings = [{}] * hotend_count
        self.__hotend_material_guid_property = [None] * hotend_count

        self.__material_service.setMachineType(controller.getPropertyValue("machine_manufacturer_name"), controller.getPropertyValue("machine_type_name"))

    def initialize(self):

        hotend_count = self.__controller.getPropertyValue("hotend_count")

        for hotend_nr in range(hotend_count):
            hotend = self.__controller.getPrintHeadController().getHotendSlot(hotend_nr)
            material_guid_property = hotend.getPropertyAsObject("material_guid")
            self.__hotend_material_guid_property[hotend_nr] = material_guid_property
            material = self.__hotend_material_guid_property[hotend_nr].get()
            material_guid_property.onChange.connect(self.__generateHotendMaterialChangeCallback(hotend_nr))
            self.__loadMaterial(hotend_nr, material)

        self.__controller.getPrintHeadController().getHotendCartridgeManager().onHotendDataReady.connect(self.__onHotendDataReady)

    ## Generate a closure that can be used as callback from property changes to update the material settings for a certain hotend nr.
    #  This is done to capture the hotend_nr variable in the closure.
    def __generateHotendMaterialChangeCallback(self, hotend_nr):
        return lambda property, value: self.__loadMaterial(hotend_nr, value)

    # Load a specific material profile for a specific hotend
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to retrieve the material settings
    # @param guid: The GUID of the material to load
    def __loadMaterial(self, hotend_nr, guid):
        brand, material, color = self.__material_service.getMaterialNameFields(guid)
        self.__material_name[hotend_nr] = (brand, material, color)
        self.__updateMaterialSettingsForHotend(hotend_nr, update_reason="material")

    def __onHotendDataReady(self, hotend_nr):
        # Update the settings for this hotend when the hotend changes ID.
        self.__updateMaterialSettingsForHotend(hotend_nr, update_reason="hotend")

    def __updateMaterialSettingsForHotend(self, hotend_nr, update_reason=None):
        phc = self.__controller.getPrintHeadController()
        hotend_id = phc.getHotendCartridgeProperty(hotend_nr, "hotend_cartridge_id")
        if hotend_id is None:
            hotend_id = ""
        log.info("Updating material settings for hotend %d with id %s", hotend_nr, hotend_id)
        guid = self.__hotend_material_guid_property[hotend_nr].get()
        if not guid:
            self.__material_settings[hotend_nr] = {}
        else:
            self.__material_settings[hotend_nr] = self.__material_service.getMaterialSettings(guid, hotend_id)

        # the HOTEND_INCOMPATIBLE_WITH_MATERIAL and MATERIAL_INCOMPATIBLE_WITH_HOTEND are intended to match with the change/load/unload hotend/material wizards,
        compatibility = self.isHotendCompatibleWithMaterial(hotend_id, guid)
        if compatibility == self.INCOMPATIBLE:
            fault_code = FaultCode.MATERIAL_HOTEND_INCOMPATIBILITY
            if update_reason == "hotend":
                fault_code = FaultCode.HOTEND_INCOMPATIBLE_WITH_MATERIAL
            elif update_reason == "material":
                fault_code = FaultCode.MATERIAL_INCOMPATIBLE_WITH_HOTEND
                phc.getHotendSlot(hotend_nr).setPropertyValue("material_guid", "")

            data = {"display_hotend_nr": hotend_nr + 1, "detected_cartridge_type": hotend_id, "material_name":self.getMaterialName(hotend_nr)}
            FaultHandler.warning("hotend in slot {display_hotend_nr} loaded with cartridge type {detected_cartridge_type} is not compatible with material {material_name}"
                .format(**data), fault_code, data=data)
        elif (update_reason == "hotend" and compatibility == self.UNKNOWN_COMPATIBILITY
              and self.__controller.getPropertyValue("state") != "booting"):        # Suppress the warning on startup
            data = {"display_hotend_nr": hotend_nr + 1, "detected_cartridge_type": hotend_id, "material_name":self.getMaterialName(hotend_nr)}
            FaultHandler.warning("hotend in slot {display_hotend_nr} loaded with cartridge type {detected_cartridge_type} has an unknown compatibility with material {material_name}"
                .format(**data), FaultCode.UNKNOWN_MATERIAL_COMPATIBILITY_WITH_HOTEND, data=data)

    # Return the best bed temperature for the currently selected material(s)
    # Handled here for now, as we need to make a choice when multiple hotends
    # are active with different materials. This needs to be improved to the upper
    # layers.
    # @param self; The object pointer
    # @param hotend_nr: Optional parameter to load the bed temperature settings for.
    #                   If left empty (the default), then an average of all hotends is calculated
    # @return -1 on error
    # @return 0 on don't know or don't care about a heated bed
    # @return best temperature suitable for materials in hotends.
    def getRequiredBedTemperature(self, hotend_nr = None):
        if not self.__controller.getPropertyValue("feature_heated_bed"):
            return -1

        hotend_count = self.__controller.getPropertyValue("hotend_count")
        bed_temp = self.__DEFAULT_HEATED_BED_TEMPERATURE
        bed_temp_max = 9999
        bed_temp_min = 0

        if hotend_count < 1:
            return -1

        if hotend_nr:
            return self.__material_settings[hotend_nr].get("heated bed temperature", 0)

        for hotend_nr in range(hotend_count):
            settings = self.__material_settings[hotend_nr]

            material_bed_temp = settings.get("heated bed temperature", bed_temp)
            bed_temp = max(material_bed_temp, bed_temp)

            # Retrieve the maximum bed temperature, and from that the lowest across all hotends
            material_bed_temp_max = settings.get("maximum heated bed temperature", bed_temp_max)
            bed_temp_max = min(bed_temp_max, material_bed_temp_max)

            material_bed_temp_min = settings.get("minimum heated bed temperature", bed_temp_min)
            bed_temp_min = max(bed_temp_min, material_bed_temp_min)

        if bed_temp_min > bed_temp_max:
            log.error("Bed temperature with material combinations was invalid")
            return -1

        # Clamp the bed temperature to the min and max values.
        bed_temp = min(bed_temp_max, bed_temp)
        bed_temp = max(bed_temp_min, bed_temp)

        return bed_temp

    # Get the maximum temperature for a specific material
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the temperature settings
    # @return the maximum temperature
    def getMaximumTemperature(self, hotend_nr):
        settings = self.__material_settings[hotend_nr]
        if "processing temperature graph" not in settings:
            log.warning("No maximum temperature for material in hotend %d\n" % hotend_nr)
            return -1
        # Get the temperature from the last point in the temperature processing graph.
        return settings["processing temperature graph"][-1][1]

    # Get the ideal printing temperature for a specific material
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the ideal printing temperature
    # @return the ideal printing temperature
    def getPrintingTemperature(self, hotend_nr):
        if hotend_nr is not None:
            settings = self.__material_settings[hotend_nr]
            if "print temperature" in settings:
                return settings["print temperature"]
            log.warning("No print temperature for material in hotend %d with material %s, defaulting to 200C\n", hotend_nr, self.__hotend_material_guid_property[hotend_nr].get())
        return self.__DEFAULT_PRINTING_TEMPERATURE

    # Get the ideal standby temperature for a specific material
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the standby temperature
    # @return the ideal standby temperature
    def getStandbyTemperature(self, hotend_nr):
        settings = self.__material_settings[hotend_nr]
        if "standby temperature" in settings:
            return settings["standby temperature"]
        log.warning("No standby temperature for material in hotend %d with material %s\n", hotend_nr, self.__hotend_material_guid_property[hotend_nr].get())
        return 0.0

    # Get the removal temperature for a specific material
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the removal temperature
    # @return the removal temperature
    def getHotendRemovalTemperature(self, hotend_nr):
        standby = self.getStandbyTemperature(hotend_nr)
        if standby > 0.0:
            return standby
        # If we do not have a standby temperature, use the print temperature for material removal.
        return self.getPrintingTemperature(hotend_nr)

    # Get the type of specific material
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the type
    # @return the name of the material type
    def getMaterialType(self, hotend_nr):
        return self.__material_name[hotend_nr][1]

    # Get the brand of specific material
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the brand
    # @return the name of the material brand
    def getMaterialBrand(self, hotend_nr):
        return self.__material_name[hotend_nr][0]

    # Get the color of specific material
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the color
    # @return the name of the material color
    def getMaterialColor(self, hotend_nr):
        return self.__material_name[hotend_nr][2]

    # Get the compound name of specific material (Brand color type)
    # @param self; The object pointer
    # @param hotend_nr: The hotend for which to get the compound name
    # @return the compound name of the material, or None if unknown
    def getMaterialName(self, hotend_nr):
        brand = self.getMaterialBrand(hotend_nr)
        material = self.getMaterialType(hotend_nr)
        color = self.getMaterialColor(hotend_nr)

        name = ""
        if brand is not None:
            name += brand
        if type is not None:
            name += material
        if color is not None:
            name += color

        if name == "":
            return None
        return name

    # Test is a hotend_cartridge_id is compatible with a specific material.
    # @param hotend_cartridge_id: The hotend type for which to determine whether it is compatible with the material.
    # @param guid: The GUID of the material to test the hotend with
    def isHotendCompatibleWithMaterial(self, hotend_cartridge_id, material_guid):
        compatible = self.UNKNOWN_COMPATIBILITY
        if hotend_cartridge_id is None or material_guid is None:
            return compatible
        elif material_guid == "" or hotend_cartridge_id == "":
            return self.COMPATIBLE

        material_settings = self.__material_service.getMaterialSettings(material_guid, hotend_cartridge_id)
        if "hardware compatible" in material_settings:
            if material_settings["hardware compatible"] == "no":
                compatible = self.INCOMPATIBLE
            elif material_settings["hardware compatible"] == "yes":
                compatible = self.COMPATIBLE

        return compatible

    ## Test if the materials match by type.
    # @param guid0 A material guid.
    # @param guid1 A material guid.
    # @return True when materials match by type.
    def areMaterialsMatching(self, guid0, guid1):
        assert isinstance(guid0, uuid.UUID)
        assert isinstance(guid0, uuid.UUID)

        type0 = self.__material_service.getMaterialNameFields(str(guid0))[1]
        type1 = self.__material_service.getMaterialNameFields(str(guid1))[1]

        if type0 != type1:
            log.info("materials %s DO NOT MATCH!", [type0, type1])
            return False

        log.debug("materials %s match!", [type0, type1])
        return True
