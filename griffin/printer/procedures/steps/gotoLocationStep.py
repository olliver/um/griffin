
from .gotoPositionStep import GotoPositionStep
from griffin.printer.buildPlateLocations import BuildPlateLocations
import logging

log = logging.getLogger(__name__.split(".")[-1])


# Procedure step to move the printer head to a location specified by name.
class GotoLocationStep(GotoPositionStep):

    # @brief When this step is executed the printer head will move to the specified location in the printer.
    # @param position Positions can be "LEFT_FRONT", "RIGHT_FRONT", "CENTER_REAR", etc.
    # @param hotend_nr Indicating the nozzle to use (with relative offsets)
    def __init__(self, key, location, hotend_nr=0, overrule_coordinates={}):

        # Default init for the step
        super().__init__(key)
        self._hotend_nr = hotend_nr
        self._location = location
        self._overrule_coordinates = overrule_coordinates

    def run(self):
        position = BuildPlateLocations.getInstance().getPosition(self._location, self._hotend_nr, self._overrule_coordinates)
        assert position is not None, "Unknown location specified: %s" % self._location

        self._x = position.get("x", None)
        self._y = position.get("y", None)
        self._z = position.get("z", None)

        log.info("Moving to location '{loc}': x={x}, y={y}, z={z}".format(loc=self._location, x=self._x, y=self._y, z=self._z))
        super().run()
