import logging

from griffin.printer.properties.propertyContainer import PropertyContainer
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

log = logging.getLogger(__name__.split(".")[-1])


## A step to set a property to a value.
#  The constructor accepts both a property container as well as a string to indicate the property container. This is to make it work with the machine json file.
class SetPropertyStep(ProcedureStep):
    # @param step_key: The name of this step.
    # @param property_owner: The propertyContainer object, or a string to indicate where to get the propertyContainer from.
    # @param property_key: The name of the property to set.
    # @param property_value: The value to set the property to.
    def __init__(self, step_key, property_owner, property_key, property_value):
        super().__init__(step_key)

        c = Controller.getInstance()
        property_owner_map = {
            "printer": c.getPropertyContainer(),
            "printer.bed": c.getBed().getPropertyContainer(),
            "printer.head.0": c.getPrintHeadController().getPropertyContainer(),
        }
        for index in range(0, c.getPropertyValue("hotend_count")):
            property_owner_map["printer.head.0.hotend.%d" % index] = c.getPrintHeadController().getHotendSlot(index).getPropertyContainer()

        self._property_container = None
        if isinstance(property_owner, PropertyContainer):
            self._property_container = property_owner
        elif property_owner in property_owner_map:
            self._property_container = property_owner_map[property_owner]
        else:
            raise RuntimeError("Unknown property owner: %s in SetProcedureStep" % (property_owner))
        self._property_key = property_key
        self._property_value = property_value

    # This function is allowed to block as it is called from a separate thread and will
    # not block the rest of the service.
    def run(self):
        if not self._property_container.get(self._property_key).set(self._property_value):
            log.warning("Failed to set property %s to value %s", self._property_key, self._property_value)
