from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import logging

log = logging.getLogger(__name__.split(".")[-1])


# Procedure step to move the printer head to a certain position, at a certain speed.
class GotoPositionStep(ProcedureStep):
    # @brief Create a new GotoPosition step. When this step is executed the printer head will move to a certain position in the printer.
    # Axis that are not specified will not be moved.
    #
    # @param x position in mm, or None if the head should not move in the X direction.
    # @param y position in mm, or None if the head should not move in the Y direction.
    # @param z position in mm, or None if the head should not move in the Z direction.
    # @param speed in mm/sec
    # @param wait_for_queue_empty if true, this step waits till the move is executed and finished.
    #        (Warning: Should not be true during printing! As will cause blobs/delays in the movement)
    def __init__(self, key, x=None, y=None, z=None, speed=None, wait_for_queue_empty=True, e=None, relative_positions=False):
        super().__init__(key)
        if isinstance(x, dict):
            self._relative = x.get("relative_positions", False)
            self._relative_z = x.get("relative_z", False)
            self._wait_for_queue_empty = x.get("wait_for_queue_empty", True)
            self._speed = x.get("speed", None)
            self._e = x.get("e", None)
            self._z = x.get("z", None)
            self._y = x.get("y", None)
            self._x = x.get("x", None)
        else:
            self._x = x
            self._y = y
            self._z = z
            self._e = e
            self._speed = speed
            self._wait_for_queue_empty = wait_for_queue_empty
            self._relative = relative_positions

    def run(self):
        log.info("STEP x: %s y: %s z: %s speed: %s" % (self._x, self._y, self._z, self._speed))
        c = Controller.getInstance()
        positions = c.getPosition()
        hotend_nr = c.getActiveHotend()
        gcode = "G1 "
        if self._x is not None:
            gcode += "X%f " % (self._x + (positions["X"] if self._relative else 0))
        if self._y is not None:
            gcode += "Y%f " % (self._y + (positions["Y"] if self._relative else 0))
        if self._z is not None:
            gcode += "Z%f " % (self._z + (positions["Z"] if self._relative else 0))
        if self._e is not None:
            gcode += "E%f " % (self._e + (positions["E{hotend_nr}".format(hotend_nr=hotend_nr)] if self._relative else 0))

        speed = self._speed
        if speed is None:
            speed = c.getPropertyValue("default_travel_speed")
        gcode += "F%f" % (speed * 60.0) # Convert mm/s to mm/min

        c.queueRawGCode(gcode)
        # should only be used in pre or post print situation !
        if self._wait_for_queue_empty:
            c.waitForQueueToBeEmpty()
