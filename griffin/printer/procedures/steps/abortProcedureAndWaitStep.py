from ..procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin import thread

import logging

log = logging.getLogger(__name__.split(".")[-1])


class AbortProcedureAndWaitStep(ProcedureStep):
    def __init__(self, key, procedure_key):
        super().__init__(key)

        self._procedure = None
        self._procedure_key = procedure_key

        # Flag to abort the run() on _onProcedureFinished.
        self._finished_event = thread.Event()

    # Getter for the called procedure
    def getProcedure(self):
        return self._procedure

    # This function is allowed to block as it is called from a separate thread
    # and will not block the rest of the service.
    def run(self):
        self._finished_event.clear()
        self._procedure = Controller.getInstance().getProcedure(self._procedure_key)

        if self._procedure is None:
            log.error("Other procedure not found: %s", self._procedure_key)

            # Procedure is not started, so we only have to abort ourselves.
            self._abort = True
            return

        # First register the onFinished handler, this to prevent the race condition where the procedure could finish between the
        # isActive check and the registering of the onProcedureFinished handler.
        self._procedure.onFinished.connect(self._onProcedureFinished)
        if not self._procedure.isActive():
            self._procedure.onFinished.disconnect(self._onProcedureFinished)
            return

        self._procedure.abort()
        # Wait till the other procedure is finished, this should be quite quick on an abort.
        while not self._finished_event.is_set() and not self._abort:
            self._finished_event.wait(self.TIME_OUT_WAIT_FINISHED_EVENT)

    def _onProcedureFinished(self, procedure):
        self._procedure.onFinished.disconnect(self._onProcedureFinished)
        self._finished_event.set()
