from griffin.printer.procedures.queueRawGCodeWaitStep import QueueRawGCodeWaitStep


## Home the bed and wait till the homing is finished.
class HomeBedStep(QueueRawGCodeWaitStep):
    def __init__(self, key):
        super().__init__(key, "G28 Z0")
