from .setPropertyStep import SetPropertyStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


## A step to set a property back to a default value. This allows for easy of value resets without duplicated numbers in the json file.
class ResetPropertyToDefaultStep(SetPropertyStep):
    # @param step_key: The name of this step.
    # @param property_owner: The propertyContainer object, or a string to indicate where to get the propertyContainer from.
    # @param property_key: The name of the property to set.
    def __init__(self, step_key, property_owner, property_key):
        super().__init__(step_key, property_owner, property_key, None)

    # This function is allowed to block as it is called from a separate thread and will
    # not block the rest of the service.
    def run(self):
        property = self._property_container.get(self._property_key)
        if not property.set(property.getDefault()):
            log.warning("Failed to set property %s to value %s", self._property_key, property.getDefault())
