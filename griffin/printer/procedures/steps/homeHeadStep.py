from griffin.printer.procedures.queueRawGCodeWaitStep import QueueRawGCodeWaitStep


## Home the head to the end switches and wait till the homing is finished.
class HomeHeadStep(QueueRawGCodeWaitStep):
    def __init__(self, key):
        super().__init__(key, "G28 X0 Y0")
