from .procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class MoveBedStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)
        self._target_position = None
        self._target_speed = None

    def setTarget(self, speed, position):
        self._target_position = position
        self._target_speed = speed

    def run(self):
        assert(self._target_position is not None)

        c = Controller.getInstance()
        c.sendRawGCode("G91")   # Set to relative positioning
        gcode = "G1 Z%f F%f" % (self._target_position, self._target_speed * 60)
        # Z or E moves do not need to be speed compensated.
        c.sendRawGCode(gcode)
        c.sendRawGCode("G90", wait = True)  # Set back to absolute positioning & ensure all are finished before emiting finished
