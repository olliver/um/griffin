from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.diagnostics.calculateSensorNoiseStep import CalculateSensorNoiseStep
from griffin.printer.procedures.wizard.sleepStep import SleepStep

import time
import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure calculates the sensor noise values
class CalculateSensorNoiseProcedure(Procedure):

    ## @brief Execute <number_of_samples> times sampling of data. Each sample takes <samples_count> measurements...
    #  @param key The procedure key
    #  @param number_of_samples The amount of samples to takes
    #  @param samples_count The number of measurements to take per sample
    def __init__(self, key, number_of_samples=10, samples_count=1000):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)
        self.__meta_data = {
            "sensor_data": "No data available",
            "timestamp":"None"
        }
        ## These will be set as defaults, incase they're not specified during setup
        self.__number_of_samples = number_of_samples
        self.__samples_count = samples_count

    # Setup the steps for the procedure. The following params can be put in the args dictionary (to overrule the default values)
    #  @param number_of_samples The amount of samples to takes
    #  @param samples_count The number of measurements to take per sample
    def setup(self, args):
        any_active_procedures =  Controller.getInstance().getActiveProcedures()
        if any_active_procedures:
            return False

        number_of_samples = args.get("number_of_samples", self.__number_of_samples)
        samples_count = args.get("samples_count", self.__samples_count)
        self.__step = CalculateSensorNoiseStep("CalculateSensorNoiseStep", number_of_samples, samples_count)
        ## Wait for the step to finish in order to get the final data
        self.__step.stepFinished.connect(self._getStepData)
        self.setFirstStep(self.__step).setNextStep(SleepStep("SLEEP", 0.5))
        return True

    ## @brief This function will be called when the step ends (before the procedure is finished) and gets the accumulated data
    def _getStepData(self):
        self.__meta_data = {
            "timestamp": str(time.monotonic())
        }
        self.__meta_data.update(self.__step.getMetaData())

    def getMetaData(self):
        return self.__meta_data
