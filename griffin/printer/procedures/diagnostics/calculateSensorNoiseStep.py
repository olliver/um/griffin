from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

import re
import logging

log = logging.getLogger(__name__.split(".")[-1])


class CalculateSensorNoiseStep(ProcedureStep):

    def __init__(self, key, number_of_samples, sample_count):
        super().__init__(key)
        self.__sample_count = sample_count
        self.__number_of_samples = number_of_samples
        self.__find_numbers_re = re.compile(r"^Average:\s*(?P<average>(\d+))\s*Min:\s*(?P<minimum>(\d+))\s*Max:\s*(?P<maximum>(\d+))\s*Stddev\^2:\s*(?P<stddev_squared>(\d+))")
        self.__sensor_data = []
        self.__data = None

    def run(self):
        c = Controller.getInstance()
        self.__sensor_data = []
        c.waitForQueueToBeEmpty(wait_for_motion_controller=True)
        self.__data = c.sendRawGCode("G31 V1", wait=True) # reset cap offset

        for measurement_index in range (0, self.__number_of_samples):
            gcode = "M311 S{0}".format(self.__sample_count)
            result = c.sendRawGCode(gcode, wait = True)
            matches = self.__find_numbers_re.match(result)

            if matches:
                self.__sensor_data.append(matches.groupdict())
            else:
                log.info("Unable to match raw data?")

        c.waitForQueueToBeEmpty(wait_for_motion_controller=True)

    ## @brief Returns the accumulated sensor data being measured
    def getMetaData(self):
        return {
            "G31": self.__data,
            "sensor_data": self.__sensor_data
        };
