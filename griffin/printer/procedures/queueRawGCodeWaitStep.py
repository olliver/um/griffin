from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

# Write a GCode command to the GCodeProcessor queue and wait for it to finish. This as opposed to the
# gcodeStep procedure which bypasses the queue.
# Generic rule is to put all motions into the queued buffer as this will give more fluid motions.
#
# Putting something in the queue and then waiting doesn't make much sense at first, until you
# realise that some commands, like an offset reset, should only be executed when the queue is totally empty.
# So, instead of first waiting for the queue to be empty and then running your reset command, it
# is just as efficient to use this function which puts your reset command at the end of the queue
# and waits for the response.
#
# This function should only be used in pre or post print situation! Adding a wait to the queue
# breaks the whole purpose of the queue: creating a fluid nozzle motion.
class QueueRawGCodeWaitStep(ProcedureStep):
    def __init__(self, key, gcode):
        super().__init__(key)
        self._gcode = gcode

    def run(self):
        c = Controller.getInstance()
        c.queueRawGCode(self._gcode)
        c.waitForQueueToBeEmpty(wait_for_motion_controller = True)
