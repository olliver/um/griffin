from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class SetHotendTemperatureStep(ProcedureStep):
    def __init__(self, key, hotend = None, target = None):
        super().__init__(key)

        self._target_temperature = target
        if hotend is not None:
            self._target_hotend = int(hotend)
        else:
            self._target_hotend = hotend

    def setTargetTemperature(self, temperature):
        self._target_temperature = temperature

    def setTargetHotend(self, hotend):
        self._target_hotend = int(hotend)

    def run(self):
        controller = Controller.getInstance()
        controller.getPrintHeadController().getHotendSlot(self._target_hotend).setPropertyValue("pre_tune_target_temperature", self._target_temperature)
