from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.temperature.setTemperatureTweakStep import SetTemperatureTweakStep


# This procedure sets the user addition or subtraction of the printing temperature of the hotends OR buildplate!
class SetTemperatureTweakProcedure(Procedure):
    BED_HOTEND_NR = -1

    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._step = SetTemperatureTweakStep("TWEAKING")
        self.setFirstStep(self._step)

    def setup(self, args):
        target_hotend = self.BED_HOTEND_NR
        if "hotend_nr" in args and args["hotend_nr"] is not None:
            target_hotend = int(args["hotend_nr"])

        if target_hotend == self.BED_HOTEND_NR:
            self._step.setTargetHotend(None)
        else:
            self._step.setTargetHotend(target_hotend)

        if "target" in args:
            target = float(args["target"])

        self._step.setOffset(target)
        return True
