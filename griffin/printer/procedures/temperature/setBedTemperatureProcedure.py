from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.temperature.setBedTemperatureStep import SetBedTemperatureStep

import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure sets the Bed temperature.
class SetBedTemperatureProcedure(Procedure):
    BED_HOTEND_NR = -1

    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._url_data = None

        self._step = None

    # Setup the steps for the procedure.
    # the target temperature may be influenced by the temperature tweak, the user adjustable value.
    def setup(self, args):
        controller = Controller.getInstance()
        target = controller.getBed().deriveTemperature(**args)

        if target < 0:
            FaultHandler.warning("Bed temperature out of spec", FaultCode.BED_TEMPERATURE)   # For displaying
            return False

        # clamp temperature to be a sane value
        self._step = SetBedTemperatureStep("SET_BED_TEMPERATURE", target)
        self.setFirstStep(self._step)
        return True

    # Returns the metadata of the procedure.
    def getMetaData(self):
        controller = Controller.getInstance()
        return {
            "original_target": controller.getBed().getPropertyValue("pre_tune_target_temperature"),
            "user_tweak": controller.getBed().getPropertyValue("tune_offset_temperature"),
            "current": controller.getBed().getPropertyValue("current_temperature"),
            "target": controller.getBed().getPropertyValue("target_temperature"),
            "max_temperature": controller.getBed().getPropertyValue("max_target_temperature")
        }
