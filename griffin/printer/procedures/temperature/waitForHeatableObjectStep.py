import time

from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.heatableObject.heatableObject import HeatableObject
import logging

log = logging.getLogger(__name__.split(".")[-1])


## Base class to set a temperature on a heatable object and wait for it to be reached.
#  Never use this class directly, but use one of it's subclasses.
class WaitForHeatableObjectStep(ProcedureStep):
    ## Create a new WaitForHeatableObjectStep.
    # @param key: string, name of this step.
    # @param target_temperature_configuration: None or dictionary, containing the key/values that are used to configure this step.
    #                               see HeatableObject.deriveTemperature functions for details.
    def __init__(self, key, target_temperature_configuration, heatable_object):
        super().__init__(key)

        assert isinstance(heatable_object, HeatableObject), "WaitForHeatableObjectStep requires a heatable object."

        self.__target_temperature_configuration = target_temperature_configuration
        self.__target_temperature = None
        self.__target_heatable_object = heatable_object
        self.__progress = -1.0
        self.__skip = False

    ## Update the target temperature with a dictionary of parameters.
    #  see HeatableObject.deriveTemperature functions for details.
    # @param **args: Key value arguements passed to deriveTemperature.
    def updateTargetTemperature(self, **args):
        target = self.__target_heatable_object.deriveTemperature(**args)
        if target < 0:
            return False
        self.__target_temperature = target
        return True

    ## Private function to update the heatable object that we are pointing at.
    #  this is used by the hotend subclass to update to a specific hotend.
    #  @param new_heatable_object: HeatableObject subclass, new heatable object to set the temperature on and wait for with this instance.
    def _updateHeatableObject(self, new_heatable_object):
        assert isinstance(new_heatable_object, HeatableObject), "WaitForHeatableObjectStep requires a heatable object."

        self.__target_heatable_object = new_heatable_object

    ## Overruled run function from base class.
    def run(self):
        self.__skip = False

        if self.__target_temperature_configuration is not None:
            self.updateTargetTemperature(**self.__target_temperature_configuration)

        self.__target_heatable_object.setPropertyValue("pre_tune_target_temperature", self.__target_temperature)

        log.info("Heating up %s to %.0fC", self.__target_heatable_object.getName(), self.__target_temperature)

        start_temperature = self.__target_heatable_object.getPropertyValue("current_temperature")
        last_log_time = time.monotonic()
        while not self._abort and not self.__skip:
            if self.__target_heatable_object.isTargetTemperatureReached():
                break

            current = self.__target_heatable_object.getPropertyValue("current_temperature")
            target = self.__target_heatable_object.getPropertyValue("target_temperature")

            start_delta = abs(start_temperature - target)
            current_delta = abs(current - target)

            if start_delta != 0.0:
                progress = 1.0 - (current_delta / start_delta)
            else:
                progress = 0.0

            self.__progress = max(min(progress, 1.0), 0.0)
            time.sleep(0.5)

            if time.monotonic() > last_log_time + 5.0:
                last_log_time = time.monotonic()
                if current < target:
                    log.info("Waiting for %s heatup: %.1f/%.1f", self.__target_heatable_object.getName(), current, target)
                else:
                    log.info("Waiting for %s cooling: %.1f/%.1f", self.__target_heatable_object.getName(), current, target)

        if self._abort:
            self.__target_heatable_object.setPropertyValue("pre_tune_target_temperature", 0.0)

    ## Overruled reveiveMessage class.
    #  Called when a different service messages this procedure.
    #  @param message: string, containing the message that was send to the procedure in which this step was created.
    def receiveMessage(self, message):
        # This allows waiting for the target to be reached to be skipped, this does not turn off the target, so it will be reached eventually.
        if message == "SKIP":
            self.__skip = True

    ## Overrules metadata function.
    #  Overruled to give progress information as well as other data about this temperature step.
    # @return dictionary
    def getMetaData(self):
        return {
            "progress": float(self.__progress),
            "time": int(-1),
            "time_total": int(-1),
            "original_target": self.__target_heatable_object.getPropertyValue("pre_tune_target_temperature"),
            "user_tweak": self.__target_heatable_object.getPropertyValue("tune_offset_temperature"),
            "current": self.__target_heatable_object.getPropertyValue("current_temperature"),
            "target": self.__target_heatable_object.getPropertyValue("target_temperature"),
            "max_temperature": self.__target_heatable_object.getPropertyValue("max_target_temperature")
        }
