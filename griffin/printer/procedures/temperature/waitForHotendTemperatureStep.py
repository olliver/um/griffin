import logging

from griffin.printer.heatableObject.heatableHotend import HeatableHotendSlot
from griffin.printer.controller import Controller
from .waitForHeatableObjectStep import WaitForHeatableObjectStep

log = logging.getLogger(__name__.split(".")[-1])


## Class to set a hotend temperature and wait for it to be reached.
class WaitForHotendTemperatureStep(WaitForHeatableObjectStep):
    ## Create a new WaitForHotendTemperatureStep.
    #  @param key: string, name of this step.
    #  @param target_configuration: optional, dictionary, containing the key/values that are used to configure this step. Can be used to both fill the hotend_index and the target temperature.
    #                               see HeatableHotendSlot.deriveHotendNr and HeatableHotendSlot.deriveTemperature functions for details.
    def __init__(self, key, target_configuration=None):
        self.__hotend_index = 0
        if target_configuration is not None:
            self.__hotend_index = HeatableHotendSlot.deriveHotendNr(**target_configuration)
        super().__init__(key, target_configuration, Controller.getInstance().getPrintHeadController().getHotendSlot(self.__hotend_index))

    ## Function to update the selected hotend from a set of parameters.
    #  Called from the waitForHotendTemperatureProcedure to configure the proper hotend
    #  @param **args: keyword arguements that are passed to the deriveHotendNr function.
    def updateTargetHotend(self, **args):
        self.__hotend_index = HeatableHotendSlot.deriveHotendNr(**args)
        self._updateHeatableObject(Controller.getInstance().getPrintHeadController().getHotendSlot(self.__hotend_index))
        return True

    ## Overruled from WaitForHeatableObjectStep to add the hotend index for this step.
    def getMetaData(self):
        meta_data = super().getMetaData()
        meta_data.update({
            "hotend_nr": self.__hotend_index,
            "display_hotend_nr": self.__hotend_index + 1,
        })
        return meta_data
