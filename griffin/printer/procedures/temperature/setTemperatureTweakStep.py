from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class SetTemperatureTweakStep(ProcedureStep):
    def __init__(self, key, hotend = None, target = None):
        super().__init__(key)

        self.__offset = target
        if hotend is not None:
            self._target_hotend = int(hotend)
        else:
            self._target_hotend = hotend

    def setOffset(self, offset):
        self.__offset = offset

    def setTargetHotend(self, hotend):
        self._target_hotend = int(hotend)

    def run(self):
        controller = Controller.getInstance()
        if self._target_hotend is None:
            controller.getBed().setPropertyValue("tune_offset", self.__offset)
        else:
            controller.getPrintHeadController().getHotendSlot(self._target_hotend).setPropertyValue("tune_offset", self.__offset)
