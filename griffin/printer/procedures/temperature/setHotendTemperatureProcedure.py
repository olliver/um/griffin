from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
from griffin.printer.heatableObject.heatableHotend import HeatableHotendSlot
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.temperature.setHotendTemperatureStep import SetHotendTemperatureStep

import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure sets the hotend temperature
class SetHotendTemperatureProcedure(Procedure):
    def __init__(self, key, hotend_nr = 0):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        if hotend_nr is not None:
            self._target_hotend = int(hotend_nr)
        else:
            self._target_hotend = None

        self._step = SetHotendTemperatureStep("SET_HOTEND_TEMPERATURE")
        self.setFirstStep(self._step)
        self._step.setTargetHotend(self._target_hotend)

    # Setup the steps for the procedure.
    # The target temperature may be influenced by the temperature tweak, the user adjustable value.
    # The hotend_nr parameter MUST be set in the args dictionary except when this command is used from gcode (M104).
    def setup(self, args):
        self._target_hotend = HeatableHotendSlot.deriveHotendNr(**args)
        target = Controller.getInstance().getPrintHeadController().getHotendSlot(self._target_hotend).deriveTemperature(**args)

        if target < 0:
            FaultHandler.warning("Incorrect printing temperature", FaultCode.MATERIAL_TEMPERATURE)   # For displaying
            return False

        log.info("Heating up hotend %d to %.0fC", self._target_hotend, target)
        self._step.setTargetHotend(self._target_hotend)
        self._step.setTargetTemperature(target)
        return True

    # Returns the metadata of the procedure.
    def getMetaData(self):
        controller = Controller.getInstance()
        hotend = controller.getPrintHeadController().getHotendSlot(self._target_hotend)
        return {
            "original_target": hotend.getPropertyValue("pre_tune_target_temperature"),
            "user_tweak": hotend.getPropertyValue("tune_offset_temperature"),
            "current": hotend.getPropertyValue("current_temperature"),
            "target": hotend.getPropertyValue("target_temperature"),
            "hotend_nr": self._target_hotend,
            "display_hotend_nr": self._target_hotend + 1,
            "max_temperature": hotend.getPropertyValue("max_target_temperature")
        }
