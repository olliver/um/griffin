import logging

from griffin.printer.controller import Controller
from .waitForHeatableObjectStep import WaitForHeatableObjectStep

log = logging.getLogger(__name__.split(".")[-1])


## Class to set a bed temperature and wait for it to be reached.
class WaitForBedTemperatureStep(WaitForHeatableObjectStep):

    ## Create a new WaitForHotendTemperatureStep.
    #  @param key: string, name of this step.
    #  @param target_temperature_configuration: optional, dictionary, containing the key/values that are used to configure this step.
    #                               see HeatablePrintBed.deriveTemperature functions for details.
    def __init__(self, key, target_temperature_configuration=None):
        super().__init__(key, target_temperature_configuration, Controller.getInstance().getBed())
