from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.temperature.waitForBedTemperatureStep import WaitForBedTemperatureStep

import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure sets the Bed temperature and waits for that temperature to be reached.
class WaitForBedTemperatureProcedure(Procedure):
    BED_HOTEND_NR = -1

    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._url_data = None

        self._step = WaitForBedTemperatureStep("SET_BED_TEMPERATURE_WAIT")
        self.setFirstStep(self._step)

    # Setup the steps for the procedure.
    # the target temperature may be influenced by the temperature tweak, the user adjustable value.
    def setup(self, args):
        if not self._step.updateTargetTemperature(**args):
            FaultHandler.warning("Bed temperature out of spec", FaultCode.BED_TEMPERATURE)   # For displaying
            return False
        return True
