import time

from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import logging

log = logging.getLogger(__name__.split(".")[-1])


class WaitForAllHotendTemperatureStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

        self._progress = -1.0

    def run(self):
        controller = Controller.getInstance()
        hotends = []
        for i in range(controller.getPropertyValue("hotend_count")):
            hotends.append(controller.getPrintHeadController().getHotendSlot(i))

        start_temperature = 0
        for hotend in hotends:
            start_temperature += hotend.getPropertyValue("current_temperature")

        start_temperature /= len(hotends)
        last_log_time = time.monotonic()
        while not self._abort:
            current = 0
            target = 0
            temperature_reached = 0

            for hotend in hotends:
                if hotend.isTargetTemperatureReached():
                    temperature_reached += 1

                current += hotend.getPropertyValue("current_temperature")
                target += hotend.getPropertyValue("target_temperature")

            if temperature_reached == len(hotends):
                break

            current /= len(hotends)
            target /= len(hotends)

            start_delta = abs(start_temperature - target)
            current_delta = abs(current - target)

            if start_delta != 0.0:
                progress = 1.0 - (current_delta / start_delta)
            else:
                progress = 0.0

            self._progress = max(min(progress, 1.0), 0.0)
            time.sleep(0.5)

            if time.monotonic() > last_log_time + 5.0:
                last_log_time = time.monotonic()
                if current < target:
                    log.info("Waiting for all hotends heatup: %.1f/%.1f", current, target)
                else:
                    log.info("Waiting for all hotends cooling: %.1f/%.1f", current, target)

        if self._abort:
            for hotend in hotends:
                hotend.setPropertyValue("pre_tune_target_temperature", 0.0)

    def getMetaData(self):
        return {
            "progress": float(self._progress),
            "time": int(-1),
            "time_total": int(-1)
        }
