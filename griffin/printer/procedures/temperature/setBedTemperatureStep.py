from griffin.printer.controller import Controller
from griffin.printer.procedures.procedureStep import ProcedureStep


class SetBedTemperatureStep(ProcedureStep):
    def __init__(self, key, temperature):
        super().__init__(key)
        self.__temperature = temperature

    def run(self):
        Controller.getInstance().getBed().setPropertyValue("pre_tune_target_temperature", self.__temperature)
