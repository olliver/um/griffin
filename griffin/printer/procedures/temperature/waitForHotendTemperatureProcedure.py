from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
from griffin.printer.heatableObject.heatableHotend import HeatableHotendSlot
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.temperature.waitForHotendTemperatureStep import WaitForHotendTemperatureStep

import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure sets the hotend temperature and waits for that temperature to be reached.
class WaitForHotendTemperatureProcedure(Procedure):
    ## Create a new WaitForHotendTemperatureProcedure.
    # @param key: unique key for this procedure.
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)

        self._step = WaitForHotendTemperatureStep("SET_HOTEND_TEMPERATURE_WAIT")
        self.setFirstStep(self._step)

    # Setup the steps for the procedure.
    # The target temperature may be influenced by the temperature tweak, the user adjustable value.
    # The hotend_nr parameter MUST be set in the args dictionary except when this command is used from gcode (M109).
    def setup(self, args):
        self._step.updateTargetHotend(**args)
        if not self._step.updateTargetTemperature(**args):
            FaultHandler.warning("Incorrect printing temperature", FaultCode.MATERIAL_TEMPERATURE)   # For displaying
            return False
        return True
