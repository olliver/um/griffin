from griffin.printer.procedures.procedure import Procedure
import urllib.parse
import time
import logging
from griffin.printer.procedures.print.parseHeaderStep import ParseHeaderStep
import math

log = logging.getLogger(__name__.split(".")[-1])

class ParseHeaderProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)

        self._parse_header_step = ParseHeaderStep("PARSE_HEADER", key)
        self._metadata = None
        self._last_metadata_timestamp = None

    def setup(self, args):
        self._metadata = None
        self._last_metadata_timestamp = None
        self._url = None

        if "url" not in args:
            log.warning("No URL given to print procedure")
            return False
        try:
            url_data = urllib.parse.urlparse(args["url"], allow_fragments=False)
            self._url = args["url"]
        except Exception:
            log.exception("Failed to parse URL data")
            return False

        self._parse_header_step.setUrlData(url_data)

        self.setFirstStep(self._parse_header_step)#.setNextStep(SleepStep("SLEEPING", 1))
        self._parse_header_step.stepFinished.connect(self._parse_finished)

        return True

    def _parse_finished(self):
        self._last_metadata_timestamp = int(time.monotonic())

    def setGcodeMetaData(self, metadata):
        self._metadata = metadata

    def getMetaData(self):
        metadata = {}
        if self._metadata is None or self._last_metadata_timestamp is None:
            log.info("metadata {metadata}, timestamp {timestamp}".format(metadata=str(self._metadata), timestamp=str(self._last_metadata_timestamp)))
            return metadata

        if self._metadata.isValid():
            metadata.update({"time": float(self._metadata.getTimeEstimate())})
            for extruder_train_nr in self._metadata.getExtruderTrains():
                material_used =  self._metadata.getMaterialToBeUsed(extruder_train_nr)
                if material_used is not None:
                    material_used = "%.1f" % (material_used / (1/4 * math.pi * 2.85**2) / 1000) # Convert mm3 to meters
                else:
                    material_used = "UNKNOWN"
                metadata.update({"material_" + extruder_train_nr: material_used})

        elif self._metadata.hasOldStyleTimeEstimate():
            metadata.update({"time": float(self._metadata.getTimeEstimate())})

        else:
            metadata.update({"error": True})

        metadata.update({
            "last_metadata_timestamp": self._last_metadata_timestamp,
            "url": self._url # included so no listItem will display info not belonging to that item!
        })

        return  metadata
