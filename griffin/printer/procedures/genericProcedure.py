import importlib

from griffin.printer.procedures.procedure import Procedure

import logging

log = logging.getLogger(__name__.split(".")[-1])


# The generic procedure is a procedure that can be build up from the machine configuration json file.
# The steps for this procedure fill be dynamically loaded and filled with the given parameters.
class GenericProcedure(Procedure):
    def __init__(self, key, *args):
        super().__init__(key, Procedure.TYPE_UNKNOWN)

        self._steps = []
        for step_data in args:
            step_key = step_data[0]
            procedure_name = step_data[1]
            procedure_package, procedure_class = procedure_name.rsplit(".", 1)

            module = importlib.import_module("griffin.printer.procedures.%s" % procedure_package)
            constructor = getattr(module, procedure_class)

            args = step_data[2:]
            try:
                self._steps.append(constructor(step_key, *args))
            except:
                log.exception("Failed to construct: %s [%s] %s", key, procedure_name, args)

        self.setup(None)

    def setup(self, args):
        self.setFirstStep(self._steps[0])
        for n in range(0, len(self._steps) - 1):
            self._steps[n].setNextStep(self._steps[n + 1])
        return True
