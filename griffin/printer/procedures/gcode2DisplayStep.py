from .procedureStep import ProcedureStep
from griffin.printer.faultHandler import FaultHandler, FaultLevel


class GCode2DisplayStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)
        self._target = "None"

    def setTarget(self, target):
        if self._target is not "None":
            FaultHandler.clear(FaultLevel.INFO)
        self._target = target

    def run(self):
        FaultHandler.info(self._target)
