from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.moveBedStep import MoveBedStep

import logging

log = logging.getLogger(__name__.split(".")[-1])


class MoveBedProcedure(Procedure):
    def __init__(self, key, speed = 100, length = 1.0):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._step = MoveBedStep("MOVING_BED")
        self.setFirstStep(self._step)
        self._target_speed = speed
        self._target_length = length

    def setup(self, args):
        if "target" in args:
            self._target_length = float(args["target"])
        else:
            self._target_speed = 0
            self._target_length = 0

        self._step.setTarget(self._target_speed, self._target_length)

        return True
