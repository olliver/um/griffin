from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import logging
log = logging.getLogger(__name__.split(".")[-1])


class ApplyXyAlignmentStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()
        for hotend_nr in range(c.getPropertyValue("hotend_count")):
            hotend = c.getPrintHeadController().getHotendSlot(hotend_nr)
            x_offset = hotend.getPropertyValue("x_offset")
            y_offset = hotend.getPropertyValue("y_offset")

            if x_offset != "" and y_offset != "":
                log.info("setting T%d (x,y) offset (%.5f, %.5f)" % (hotend_nr, x_offset, y_offset))
                c.queueRawGCode("M218 T%d X%f Y%f" % (hotend_nr, x_offset, y_offset))
            else:
                log.warning("X_offset and/or Y_offset missing. Default setting T%d (x,y) offset (0, 0)" % (hotend_nr))
                c.queueRawGCode("M218 T%d X0 Y0" % hotend_nr)
