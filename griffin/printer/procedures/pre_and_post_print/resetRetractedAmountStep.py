from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import logging

log = logging.getLogger(__name__.split(".")[-1])


## This step would not be necessary it the gcodeParser worked like it should.
class ResetRetractedAmountStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()
        amount = c.getPropertyValue("deprime_retraction_amount") + c.getPropertyValue("idle_retraction_amount")

        for hotend_nr in range(c.getPropertyValue("hotend_count")):
            c.getGCodeProcessor().setRetractedLength(hotend_nr, amount)