from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class WipeNozzleStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        controller = Controller.getInstance()

        x = controller.getPosition("X")

        if controller.getPropertyValue("build_volume_x") / 2.0 > x:
            move = 10
        else:
            move = -10

        speed = (controller.getPropertyValue("default_travel_speed")) * (1.0 / (controller.getPropertyValue("movement_speed_modifier") / 100.0))
        controller.sendRawGCode("G1 X%d F%d" % (x + move, speed * 60.0), wait=False)
