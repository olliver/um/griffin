from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.queueRawGCodeWaitStep import QueueRawGCodeWaitStep
from griffin.printer.procedures.pre_and_post_print.wipeNozzleStep import WipeNozzleStep
from griffin.printer.procedures.steps.homeHeadStep import HomeHeadStep
from griffin.printer.procedures.steps.setPropertyStep import SetPropertyStep
from griffin.printer.procedures.steps.resetPropertyToDefaultStep import ResetPropertyToDefaultStep
from griffin.printer.procedures.steps.abortProcedureAndWaitStep import AbortProcedureAndWaitStep
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep

import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure does all the steps to secure the printer after the print.
class PostPrintProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self.setAbortDisabled(True)

    def setup(self, args):
        c = Controller.getInstance()
        steps = [
            AbortProcedureAndWaitStep("ABORT_PAUSE", "PAUSE_PRINT"),
            AbortProcedureAndWaitStep("ABORT_RESUME", "RESUME_PRINT"),
            WipeNozzleStep("WIPE_BEFORE_Z"),
            ResetPropertyToDefaultStep("RESET_PRINT_SPEED", "printer", "movement_speed_modifier"),
            ResetPropertyToDefaultStep("RESET_MATERIAL_FLOW", "printer", "extrusion_amount_modifier"),
            ResetPropertyToDefaultStep("RESET_ACCELERATION_XYZ", "printer", "acceleration_xzy"),
            ResetPropertyToDefaultStep("RESET_ACCELERATION_E", "printer", "acceleration_e"),
            ResetPropertyToDefaultStep("RESET_JERK_XY", "printer", "jerk_xy"),
            ResetPropertyToDefaultStep("RESET_JERK_Z", "printer", "jerk_z"),
            ResetPropertyToDefaultStep("RESET_JERK_E", "printer", "jerk_e"),
            ResetPropertyToDefaultStep("RESET_MAX_SPEED_X", "printer", "max_speed_x"),
            ResetPropertyToDefaultStep("RESET_MAX_SPEED_Y", "printer", "max_speed_y"),
            ResetPropertyToDefaultStep("RESET_MAX_SPEED_Z", "printer", "max_speed_z"),
            ResetPropertyToDefaultStep("RESET_MAX_SPEED_E", "printer", "max_speed_e"),
            WaitForQueueToBeEmptyStep("WAITING"),
            SetPropertyStep("COOLDOWN_BED", "printer.bed", "pre_tune_target_temperature", 0.0),
            ResetPropertyToDefaultStep("RESET_TUNE_OFFSET_TEMPERATURE_BED", "printer.bed", "tune_offset_temperature"),
            SetPropertyStep("STOP_FANS", "printer.head.0", "cooling_fan_speed", 0.0),
            OtherProcedureStep("DEPRIME", "DEPRIME"), # this procedure requires the extrusion_amount_modifier to be 100%
            HomeHeadStep("HOME_HEAD")  # We need to home here, an abort will have messed up the positioning of the print head, as it has done a quickstop.
        ]
        for hotend_nr in range(c.getPropertyValue("hotend_count")):
            steps.append(QueueRawGCodeWaitStep("RESET_OFFSET_{hotend_nr}".format(hotend_nr=hotend_nr), "M218 T{hotend_nr} X0 Y0 Z0".format(hotend_nr=hotend_nr)))
            steps.append(ResetPropertyToDefaultStep("RESET_TUNE_OFFSET_TEMPERATURE_{hotend_nr}".format(hotend_nr=hotend_nr), "printer.head.0.hotend.{hotend_nr}".format(hotend_nr=hotend_nr), "tune_offset_temperature"))
        steps += [
            WaitForQueueToBeEmptyStep("WAITING"),
            OtherProcedureStep("SWITCH_TO_HOTEND", "SWITCH_ACTIVE_HOTEND", {"target": 1}),
            HomeHeadStep("HOME_HEAD"),
            WaitForQueueToBeEmptyStep("WAITING"),
            QueueRawGCodeWaitStep("POWER_DOWN_STEPPERS", "M84"),
            WaitForQueueToBeEmptyStep("WAITING")
        ]
        for i, step in enumerate(steps):
            if i != 0:
                steps[i-1].setNextStep(step)
            else:
                self.setFirstStep(step)

        return True
