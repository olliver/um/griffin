import logging
import time

from griffin.printer.buildPlateLocations import BuildPlateLocations
from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.pre_and_post_print.auto_bed_level_adjust.calcAndSetZDifferenceStep import CalcAndSetZDifferenceStep
from griffin.printer.procedures.pre_and_post_print.auto_bed_level_adjust.capacitiveOffsetStep import CapacitiveOffsetStep
from griffin.printer.procedures.pre_and_post_print.auto_bed_level_adjust.gotoPositionAndSetZ0Step import GotoPositionAndSetZ0Step
from griffin.printer.procedures.pre_and_post_print.auto_bed_level_adjust.probeZStep import ProbeZStep
from griffin.printer.procedures.steps.homeHeadStep import HomeHeadStep
from griffin.printer.procedures.steps.gotoPositionStep import GotoPositionStep
from griffin.printer.procedures.steps.gotoLocationStep import GotoLocationStep

log = logging.getLogger(__name__.split(".")[-1])


## Procedure to measure bed position and skew, as well as the height difference between hotends.
class AlignZAxisProcedure(Procedure):
    AUTOMATIC_BED_LEVEL_MODE_REDO_AFTER_X_DAYS = 2
    AUTOMATIC_BED_LEVEL_MODE_BED_TEMP = 35

    ALL_BED_LEVEL_CORRECTION_MODES = ["manual", "always", "automatic", "once a day", "once a week", "at boot"]
    DEFAULT_BED_LEVEL_CORRECTION_MODE = "automatic"

    __SECONDS_PER_DAY = 24 * 60 * 60
    __SECONDS_PER_WEEK = __SECONDS_PER_DAY * 7

    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)

        self._at_boot = True

        self.__nr_of_probes = 1
        self.__progress = 0

        self.onFinished.connect(self.__onProcedureFinished)

    def setup(self, args):
        self.__progress = 0
        nr_of_probes = 0

        try:
            require_active_leveling = args.get("force_probing", False) or self.__requireActiveLeveling()
        except ValueError:
            return False

        # Get the list of steps that we want for this form of the procedure.
        if require_active_leveling:
            self._at_boot = False
            steps = self.__generateStepsForActiveLeveling(args.get("end_early", False))
        else:
            steps = self.__generateApplySavedSettingsSteps()

        # Check for Probe steps, we want to report progress on probe steps.
        for step in steps:
            if isinstance(step, ProbeZStep):
                nr_of_probes += 1
                step.stepFinished.connect(self._reportProbeDone)

        self.__nr_of_probes = max(1, nr_of_probes)

        # Generate the step chain.
        for n in range(len(steps) - 1):
            steps[n].setNextStep(steps[n + 1])
        self.setFirstStep(steps[0])
        return True

    ## Generate the steps for a full active leveling cycle,
    #  This will probe the bed at 6 points and calculate the resulting skew/height and store it.
    #  After this it will apply these parameters and setup the printer ready for printing (unless early_end is set)
    #  @param early_end: bool, if is set, then end after saving the data, do not setup the data to be used.
    #  @return a list of ProcedureSteps to go trough this procedure.
    def __generateStepsForActiveLeveling(self, early_end):
        controller = Controller.getInstance()
        hotend_count = controller.getPropertyValue("hotend_count")
        verbosity = controller.getSystemService().isDeveloperModeActive()

        # Generate a probe data storage, this is an array with elements for each hotend, and in each hotend the probe locations.
        probe_hotend_storage = []
        for hotend_index in range(hotend_count):
            probe_hotend_storage.append([{}, {}, {}])

        ######################################################################
        # Setup the first flow, doing the full alignment.
        # Precondition: buildplate should be >150mm from nozzle, i.e. homed.
        ######################################################################
        steps = [
            OtherProcedureStep("SWITCH_TO_HOTEND_1", "SWITCH_ACTIVE_HOTEND", args={"target": 1}),
            CapacitiveOffsetStep("SETTING_CAPACITVE_SENSOR_OFFSET", verbosity),     # Reset the capacitive offset. Should be done far away from the build plate, i.e. 150 mm. or more.
            # Heat up nozzles to melt any filament stuck to the nozzle and push it aside on touching the buildplate.
        ]
        for hotend_index in range(hotend_count):
            steps.append(OtherProcedureStep("HEATUP_HOTEND_%d" % (hotend_index), "SET_HOTEND_TEMPERATURE", {"hotend_nr": hotend_index, "target": "standby_temperature"}))
        for hotend_index in range(hotend_count):
            steps.append(OtherProcedureStep("HEATUP_HOTEND_WAIT_%d" % (hotend_index), "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": hotend_index, "target": "standby_temperature"}))

        steps += [
            GotoLocationStep("MOVE_TO_POS", location="ACTIVE_LEVEL_1", hotend_nr=1),
            ProbeZStep("PROBE_Z_AXIS", probe_hotend_storage[1][0], verbosity),

            GotoLocationStep("MOVE_TO_POS", location="ACTIVE_LEVEL_2", hotend_nr=1),
            ProbeZStep("PROBE_Z_AXIS", probe_hotend_storage[1][1], verbosity),

            GotoLocationStep("MOVE_TO_POS", location="ACTIVE_LEVEL_3", hotend_nr=1),
            ProbeZStep("PROBE_Z_AXIS", probe_hotend_storage[1][2], verbosity),

            GotoPositionStep("LOWER_BED", z=BuildPlateLocations.getInstance().getBottomHeight()),
            HomeHeadStep("RE-HOMING"), # homing before switching as the stepper motors might have missed a few steps.
            OtherProcedureStep("SWITCH_TO_HOTEND_0", "SWITCH_ACTIVE_HOTEND", args={"target": 0}),

            # During the probing Marlin temporarily switched off the nozzle heaters to minimize PWM noise.
            # Nozzle temperature dropped about 10 degrees during probing.
            # Wait for the left nozzle to be on target temperature again (we don't care for the right nozzle as that one is already finished).
            OtherProcedureStep("HEATUP_HOTEND_0", "SET_HOTEND_TEMPERATURE", {"hotend_nr": 0, "target": "standby_temperature"}),

            # do wait for heat up with bed down again, this is to satisfy DEKRA SAFETY requirements!
            GotoLocationStep("MOVE_TO_PRIME_POSITION", "CLEAN_NOZZLES_MAINTENANCE"),
            OtherProcedureStep("HEATUP_HOTEND_WAIT_0", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": 0, "target": "standby_temperature"}),
        ]

        # Switch off all the nozzles, they will remain hot enough for some time.
        # Required to switch off because we want to switch on the buildplate and the
        # power supply has not enough power to heat up both nozzles and build plate from a cold situation.
        for hotend_index in range(hotend_count):
            steps.append(OtherProcedureStep("COOLDOWN_HOTEND_%d" % (hotend_index), "SET_HOTEND_TEMPERATURE", {"hotend_nr": hotend_index, "target": 0}))

        steps += [
            GotoLocationStep("MOVE_TO_POS", location="ACTIVE_LEVEL_1"),
            ProbeZStep("PROBE_Z_AXIS", probe_hotend_storage[0][0], verbosity),

            GotoLocationStep("MOVE_TO_POS", location="ACTIVE_LEVEL_2"),
            ProbeZStep("PROBE_Z_AXIS", probe_hotend_storage[0][1], verbosity),

            GotoLocationStep("MOVE_TO_POS", location="ACTIVE_LEVEL_3"),
            ProbeZStep("PROBE_Z_AXIS", probe_hotend_storage[0][2], verbosity),

            # Switch on the buildplate temperature. Non-blocking
            # This saves time as automatic bed leveling is often executed
            # just before a print is started and then this step has to be performed anyway.
            OtherProcedureStep("START_HEATUP_BED", "SET_BED_TEMPERATURE", {"target": "initial_temperature"}),

             # Home again after probing to ensure X-Y positions are calibrated again (motors are unpowered during probing and will shift).
            GotoPositionStep("LOWER_BED", z=BuildPlateLocations.getSafeTravelHeight()),
            HomeHeadStep("RE_HOMING"), # this must happen before the GotoPositionAndSetZ0Step because homing resets the skew.

            CalcAndSetZDifferenceStep("CALCULATING_ALIGN_Z_AXIS", probe_hotend_storage),
        ]
        if early_end:
            return steps

        steps += [
            GotoLocationStep("MOVE_TO_PRIME_POSITION", "CLEAN_NOZZLES_MAINTENANCE"),

            # Do heat-up here so that we only move the bed up once we start printing, this is to satisfy DEKRA SAFETY requirements!
            # Also do it in this procedure because otherwise the bed moves up, down and up again later etc.
            OtherProcedureStep("WAIT_BED_TEMPERATURE", "SET_BED_TEMPERATURE_WAIT", {"target": "initial_temperature"}),
        ]
        for i in range(hotend_count):
            steps.append(OtherProcedureStep("HEATUP_HOTEND_" + str(i), "SET_HOTEND_TEMPERATURE", {"hotend_nr": i, "target": "initial_temperature"}))
        for i in range(hotend_count):
            steps.append(OtherProcedureStep("HEATUP_HOTEND_WAIT_" + str(i), "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": i, "target": "initial_temperature"}))
        steps += [
            ## NOTE: after this step you are not allowed to home because it resets the skew(because homing resets your reference framework)
            GotoPositionAndSetZ0Step("MOVE_TO_POS_AND_SET_0", BuildPlateLocations().getPosition("ACTIVE_LEVEL_3")["x"], BuildPlateLocations().getPosition("ACTIVE_LEVEL_3")["y"])
        ]

        return steps

    ## Generate the steps needed to use previously measured/setup data.
    #  This is used when a print is started and the decision was made not to do an active leveling probe but use old data.
    #  @return list of ProcedureSteps
    def __generateApplySavedSettingsSteps(self):
        controller = Controller.getInstance()
        hotend_count = controller.getPropertyValue("hotend_count")

        steps = [
            OtherProcedureStep("SWITCH_TO_HOTEND_0", "SWITCH_ACTIVE_HOTEND", args={"target": 0})
        ]
        # Switch off all the nozzles. Required because we want to switch on the buildplate and the
        # power supply has not enough power to heat up both nozzles and build plate from a cold situation.
        for hotend_index in range(hotend_count):
            steps.append(OtherProcedureStep("COOLDOWN_HOTEND_%d" % (hotend_index), "SET_HOTEND_TEMPERATURE", {"hotend_nr": hotend_index, "target": 0}))
        steps += [
            GotoLocationStep("MOVE_TO_PRIME_POSITION", "CLEAN_NOZZLES_MAINTENANCE"),

            # Do heat-up here so that we only move the bed up once we start printing, this is to satisfy DEKRA SAFETY requirements!
            # Also do it in this procedure because otherwise the bed moves up, down and up again later etc.
            OtherProcedureStep("WAIT_BED_TEMPERATURE", "SET_BED_TEMPERATURE_WAIT", {"target": "initial_temperature"}),
        ]

        for hotend_index in range(hotend_count):
            steps.append(OtherProcedureStep("HEATUP_HOTEND_%d" % (hotend_index), "SET_HOTEND_TEMPERATURE", {"hotend_nr": hotend_index, "target": "initial_temperature"}))
        for hotend_index in range(hotend_count):
            steps.append(OtherProcedureStep("HEATUP_HOTEND_WAIT_%d" % (hotend_index), "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": hotend_index, "target": "initial_temperature"}))
        steps += [
            ## NOTE: after this step you are not allowed to home because it resets the skew(because homing resets your reference framework)
            GotoPositionAndSetZ0Step("MOVE_TO_POS_AND_SET_0", BuildPlateLocations().getPosition("ACTIVE_LEVEL_3")["x"], BuildPlateLocations().getPosition("ACTIVE_LEVEL_3")["y"])
        ]

        return steps


    ## Check if we need to do a active leveling or not
    # @return bool: True if we want to active level, False if not.
    def __requireActiveLeveling(self):
        controller = Controller.getInstance()
        print_head_controller = controller.getPrintHeadController()

        # Figure out the last time the current combination of hotends was updated with leveling data.
        # For this we use the lowest possible update time.
        last_change = float("inf")
        for hotend_index in range(controller.getPropertyValue("hotend_count")):
            z_height_property = print_head_controller.getHotendSlot(hotend_index).getPropertyAsObject("z_height")
            last_change = min(last_change, z_height_property.getLastChanged())

        # Get our current active leveling mode, and do a basic sanity check.
        leveling_mode = controller.getPreference("BED_LEVEL_CORRECTION_MODE")
        if leveling_mode is None:
            leveling_mode = self.DEFAULT_BED_LEVEL_CORRECTION_MODE
            controller.setPreference("BED_LEVEL_CORRECTION_MODE", leveling_mode)
        elif leveling_mode == "manual" and not controller.hasBuildPlateLevelData():
            # no bed level known stop here.
            FaultHandler.warning("no bed level known, please level manually first", FaultCode.BUILD_PLATE_LEVEL_UNKNOWN)
            raise ValueError("No bed level known")
        elif leveling_mode not in self.ALL_BED_LEVEL_CORRECTION_MODES:
            leveling_mode = self.DEFAULT_BED_LEVEL_CORRECTION_MODE

        # Now that we have data about the active leveling mode, figure out if we want to active level or not.
        log.info("using leveling mode %s (configured mode %s)", leveling_mode, str(controller.getPreference("BED_LEVEL_CORRECTION_MODE")))
        if leveling_mode == "at boot":
            if self._at_boot:
                log.info("probe because boot flag is set!")
                return True
        elif leveling_mode == "automatic":
            if not controller.hasBuildPlateLevelData():
                log.info("probe because BED or Z is not set!")
                return True
            elif last_change == -1:
                log.info("probe because LAST_PROBE is not set!")
                return True
            elif last_change + self.AUTOMATIC_BED_LEVEL_MODE_REDO_AFTER_X_DAYS * self.__SECONDS_PER_DAY < time.time():
                log.info("probe because LAST_PROBE is not recent enough(%d days)!", self.AUTOMATIC_BED_LEVEL_MODE_REDO_AFTER_X_DAYS)
                return True
            elif controller.getBed().getPropertyValue("current_temperature") < self.AUTOMATIC_BED_LEVEL_MODE_BED_TEMP:
                log.info("probe because bed temp(%dC) is less than %dC", int(controller.getBed().getPropertyValue("current_temperature")), self.AUTOMATIC_BED_LEVEL_MODE_BED_TEMP)
                return True
            # probe data retrieved based on nozzle pair so re-leveling on a changed pair is implied.
        elif leveling_mode == "once a week":
            if last_change == -1:
                log.info("probe because LAST_PROBE is not set!")
                return True
            elif last_change + self.__SECONDS_PER_WEEK < time.time():
                log.info("probe because LAST_PROBE is not recent enough(7 days)!")
                return True
        elif leveling_mode == "once a day":
            if last_change == -1:
                log.info("probe because LAST_PROBE is not set!")
                return True
            elif last_change + self.__SECONDS_PER_DAY < time.time():
                log.info("probe because LAST_PROBE is not recent enough(1 day)!")
                return True
        elif leveling_mode == "manual":
            return False    # no probing, manual already reverted to automatic if BED is not set in offset
        elif leveling_mode == "always":
            log.info("probe because leveling_mode == \"always\"!")
            return True
        return False


    ## callback function for the probe step being finished.
    def _reportProbeDone(self):
        self.__progress += 1

    ## Procedure is finished, reset the progress.
    def __onProcedureFinished(self, procedure):
        self.__progress = 0

    def getMetaData(self):
        c = Controller.getInstance()
        mode = c.getPreference("BED_LEVEL_CORRECTION_MODE")

        if mode not in self.ALL_BED_LEVEL_CORRECTION_MODES:
            mode = self.DEFAULT_BED_LEVEL_CORRECTION_MODE
        progress = float(self.__progress / self.__nr_of_probes)
        if self.__progress == 0:
            progress = -1
        return {"current": mode, "target": mode, "progress": progress}
