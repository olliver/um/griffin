import time
import re
import logging
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
log = logging.getLogger(__name__.split(".")[-1])


class ProbeZStep(ProcedureStep):
    __PROBE_MAX_RETRY = 5 #< Maximum amount of retries on probing.
    __ERROR_HEIGHT = 150          # A special height where Marlin sends the Z-level after a probing error. This value was chosen as the next capacitive probe reset should be done far away from the build plate.

    ## @brief Initializes the class
    #  @param key The name of the step
    #  @param result_storage: dictionary, used as output from this step. Will be filled with x,y and z keys containing the final probe result position.
    #  @param verbosity Whether or not to output some debug data onto the filesytem
    def __init__(self, key, result_storage, verbosity=False):
        super().__init__(key)
        self.__result_height = None
        self.__start_position = [None, None, None]
        self.__verbosity = verbosity
        self.__retry_counter = 0
        self.__result_storage = result_storage

    def run(self, retry_counter=0):
        """ Calling run() recursive.

        Note when thread started as run(), no "lc" argument present and thus 0.
        """

        # On count exceeded, error and return
        if retry_counter > ProbeZStep.__PROBE_MAX_RETRY:
            FaultHandler.error("Automatic buildplate level correction: Probe retry count exceeded!", FaultCode.ACTIVE_LEVELING_PROBE_RETRIES_EXCEEDED)
            return

        # May be called recursive, return on _abort
        if self._abort:
            return

        c = Controller.getInstance()
        c.waitForQueueToBeEmpty(wait_for_motion_controller=True)
        self.__start_position = c.getPositionArray()
        log.info("saved position: %f, %f, %f", self.__start_position[0], self.__start_position[1], self.__start_position[2])

        data = c.sendRawGCode("G30 V1 D10", wait=True)
        # first a greedy classifier so we match the last number to come out of the thing
        # (in case debug is turned on in marlin)
        mlist = re.findall(r"(-?\d+\.\d+)", data, flags=re.MULTILINE)
        m = None
        if len(mlist) > 0:
            m = mlist[-1]

        if self.__verbosity:
            filename = "/tmp/cap_%s_%s_N%d_X%dY%dZ%f.log" % (
                c.getSystemService().getMachineName(), time.strftime("%Y-%m-%d_%H.%M.%S"), c.getActiveHotend(), int(self.__start_position[0]), int(self.__start_position[1]), float(m))

            try:
                with open(filename, "w") as f:
                    f.write(data)
                log.info("Created capacitance log file: %s", filename)
            except OSError as ex:
                log.warning("Error writing capacitance log file '%s'. Details: ", filename, ex)

        result_height = float(m)
        if m and int(result_height) != self.__ERROR_HEIGHT:
            log.info("G30 parsed %f", result_height)
            # Note  the function returns after this.
            self.__retry_counter = retry_counter

            # Store the result of this probe into our result dictionary.
            self.__result_storage["x"] = self.__start_position[0]
            self.__result_storage["y"] = self.__start_position[1]
            self.__result_storage["z"] = result_height
        else:
            log.error("ERROR in Z probe, retrying\n'%s'" % data)
            c.sendRawGCode("G28 Z0", wait=True)     # move Z-Axis to origin (home)
            time.sleep(1)
            c.sendRawGCode("G31", wait=True)    # reset cap offset
            # Move the head back to the the start position for probing again.
            c.sendRawGCode("G1 Z%g F%g" % (self.__start_position[2], c.getPropertyValue('default_travel_speed') * 60), wait=False)
            self.run(retry_counter=retry_counter + 1)

        c.sendRawGCode("G1 Z%g F%g" % (self.__start_position[2], c.getPropertyValue('default_travel_speed') * 60), wait=False)

    def getRetryCount(self):
        return self.__retry_counter
