from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.math.vector3 import Vector3
from griffin.math.plane import Plane
import logging

log = logging.getLogger(__name__.split(".")[-1])


class CalcAndSetZDifferenceStep(ProcedureStep):
    __ALLOWED_Z_DIFFERENCE_FROM_NOMINAL = 0.7
    __ALLOWED_Z_DIFFERENCE_FOR_EQUALITY = 0.5

    # As we have 3 z difference measurements, each of the individual measurements should not differ too much from the average, else we have a measurement error.
    # This value controls how much difference we allow.
    __ALLOWED_Z_DIFFERENCE_NOISE = 0.1

    def __init__(self, key, probe_hotend_storage):
        super().__init__(key)
        self.__probe_hotend_storage = probe_hotend_storage
        self.__NOMINAL_Z_DIFFERENCE = Controller.getInstance().getPropertyValue("nominal_nozzle_z_offset")

    def run(self):
        controller = Controller.getInstance()
        hotend_count = controller.getPropertyValue("hotend_count")

        assert(isinstance(self.__probe_hotend_storage, list))
        assert(len(self.__probe_hotend_storage) == hotend_count)
        for probe_hotend_storage in self.__probe_hotend_storage:
            assert(len(probe_hotend_storage) == 3)
            for probe_result in probe_hotend_storage:
                assert("x" in probe_result)
                assert("y" in probe_result)
                assert("z" in probe_result)

        log.info("Calculating probe results:")
        for hotend_index in range(hotend_count):
            log.info("Hotend %d probes:" % (hotend_index))
            for probe_result in self.__probe_hotend_storage[hotend_index]:
                log.info("%s", probe_result)

        ## calculate and verify the difference between hotend 0 and hotend 1.
        #  This is a sanity check on the data, if our probes differ too much we know we have a measuring error and thus should abort the calculations.
        nozzle_diff_per_probe_location = []
        for hotend_index in range(1, hotend_count):
            for n in range(len(self.__probe_hotend_storage[hotend_index])):
                nozzle_diff_per_probe_location.append(self.__probe_hotend_storage[hotend_index][n]["z"] - self.__probe_hotend_storage[0][n]["z"])
        nozzle_diff = sum(nozzle_diff_per_probe_location) / len(nozzle_diff_per_probe_location)
        for nozzle_diff_at_location in nozzle_diff_per_probe_location:
            if abs(nozzle_diff_at_location - nozzle_diff) > self.__ALLOWED_Z_DIFFERENCE_NOISE:
                FaultHandler.warning("Difference between detected height of both nozzles exceeds realistic values.", FaultCode.NOZZLE_Z_DIFF_TOO_LARGE)
                raise ValueError("Z offset noise out of range: %f: %s" % (nozzle_diff, nozzle_diff_per_probe_location))

        # The nozzle_diff is self.__NOMINAL_Z_DIFFERENCE on a perfect mechanically machine.
        if abs(nozzle_diff - self.__NOMINAL_Z_DIFFERENCE) <= self.__ALLOWED_Z_DIFFERENCE_FROM_NOMINAL:
            pass
        elif abs(nozzle_diff) <= self.__ALLOWED_Z_DIFFERENCE_FOR_EQUALITY:
            FaultHandler.warning("Same height detected for both nozzles. Verify switching bay calibration.", FaultCode.NOZZLE_Z_DIFF_TOO_SMALL)
            raise ValueError("Z offset out of range: %f" % (nozzle_diff))
        else:
            FaultHandler.warning("Difference between detected height of both nozzles exceeds realistic values.", FaultCode.NOZZLE_Z_DIFF_TOO_LARGE)
            raise ValueError("Z offset out of range: %f" % (nozzle_diff))

        # Calculate the bed_leveling_factors (the gradients in x- and y-direction)
        planes = []
        for positions in self.__probe_hotend_storage:
            plane = Plane.fitPlane(Vector3(positions[0]["x"], positions[0]["y"], positions[0]["z"]), Vector3(positions[1]["x"], positions[1]["y"], positions[1]["z"]), Vector3(positions[2]["x"], positions[2]["y"], positions[2]["z"]))
            if plane.normal.z < 0:
                # We want all our normals to point in the same direction. We know we the plane should point upwards or downwards.
                # If it points downwards, flip the plane.
                plane = Plane(-plane.normal, -plane.distance)
            planes.append(plane)

        # Calculate the average normal from all the fitted planes.
        normal = Vector3(0, 0, 0)
        for plane in planes:
            normal += plane.normal
        normal = normal.normalized()

        # Now that we have the normal, we need to calculate the gradient from it.
        # The gradient is the amount of Z movement done per 1mm of X or Y movement.
        x_gradient = -normal.x / normal.z
        y_gradient = -normal.y / normal.z

        log.info("X_gradient=%f  Y_gradient=%f", x_gradient, y_gradient)

        # For each hotend, calculate the height at position 0,0
        # We do this by calculating every probe point back to 0,0 with the gradients, and getting the average of that.
        # We do this because this because the gradients is an average of the two sets of probes, which will contain less noise then the plane per nozzle.
        z_at_0_0 = []
        for hotend_index in range(hotend_count):
            z = 0.0
            for probe_hotend_result in self.__probe_hotend_storage[hotend_index]:
                z += probe_hotend_result["z"] - probe_hotend_result["x"] * x_gradient - probe_hotend_result["y"] * y_gradient
            z /= len(self.__probe_hotend_storage[hotend_index])
            z_at_0_0.append(z)
            log.info("z_at_0_0[%d] = %f" % (hotend_index, z))

        ## Store the result in the properties after we calculated and verified everything
        printHeadController = controller.getPrintHeadController()
        bed = controller.getBed()
        # properties automatically store their last changed timestamp.
        for hotend_index in range(hotend_count):
            printHeadController.getHotendSlot(hotend_index).setPropertyValue("z_height", z_at_0_0[hotend_index])
        bed.setPropertyValue("SKEW_X", x_gradient)
        bed.setPropertyValue("SKEW_Y", y_gradient)
