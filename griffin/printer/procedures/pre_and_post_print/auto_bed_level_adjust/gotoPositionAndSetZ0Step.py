import logging

from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

log = logging.getLogger(__name__.split(".")[-1])


class GotoPositionAndSetZ0Step(ProcedureStep):
    def __init__(self, key, x, y):
        super().__init__(key)
        self._x = x
        self._y = y

    def setX(self, x):
        self._x = x

    def setY(self, y):
        self._y = y

    def run(self):
        controller = Controller.getInstance()
        printHeadController = controller.getPrintHeadController()
        z_hotend_0_at_0_0 = printHeadController.getHotendSlot(0).getPropertyValue("z_height")
        x_gradient = controller.getBed().getPropertyValue("SKEW_X")
        y_gradient = controller.getBed().getPropertyValue("SKEW_Y")

        z = z_hotend_0_at_0_0 + self._x * x_gradient + self._y * y_gradient

        gcode = "G1 "
        if isinstance(self._x, int) or isinstance(self._x, float):
            gcode += "X%f " % self._x
        if isinstance(self._y, int) or isinstance(self._y, float):
            gcode += "Y%f " % self._y
        gcode += "Z%f " % z

        travel_speed = controller.getPropertyValue("default_travel_speed")
        # Compensate for the current movement_speed_modifier that is being applied on all moves.
        speed = travel_speed * (1 / (controller.getPropertyValue("movement_speed_modifier") / 100))
        gcode += "F%d" % (speed * 60)

        log.info("move to: '%s' to apply bed level parameters and set Z0 position", gcode)

        controller.sendRawGCode(gcode, True)

        controller.waitForQueueToBeEmpty(wait_for_motion_controller=True)

        # set G92 Z0 after setting the leveling factors!
        controller.sendRawGCode("G32 X%f Y%f" % (x_gradient, y_gradient), True)
        for hotend_index in range(controller.getPropertyValue("hotend_count")):
            z_at_0_0 = printHeadController.getHotendSlot(hotend_index).getPropertyValue("z_height")
            controller.sendRawGCode("M218 T%d Z%f" % (hotend_index, z_at_0_0 - z_hotend_0_at_0_0))
        controller.sendRawGCode("G92 Z0", True)
        # should only be used in pre or post print situation ! Since a G92 not on the E axis breaks continuity of movements.
