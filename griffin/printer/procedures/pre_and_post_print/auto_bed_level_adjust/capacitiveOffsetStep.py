import time
import logging
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

log = logging.getLogger(__name__.split(".")[-1])


# This class will set the offset value for the capacitance sensor in the cartridge head.
#
# Precondition: In order to get a good base-line offset value, the build plate should be far away from
#               the capacitive sensor (more than 15 cm away).
class CapacitiveOffsetStep(ProcedureStep):
    ## @brief Initializes the class
    #  @param key The name of the step
    #  @param verbosity Whether or not to output some debug data onto the filesytem
    def __init__(self, key, verbosity=False):
        super().__init__(key)
        self.__verbosity = verbosity

    def run(self):
        c = Controller.getInstance()
        c.waitForQueueToBeEmpty(wait_for_motion_controller=True)

        if self.__verbosity:
            verbosity = 1
        else:
            verbosity = 0

        data = c.sendRawGCode("G31 V%d" % verbosity, wait=True)

        if self.__verbosity:
            filename = "/tmp/cap_%s_%s_offset.log" % (
                c.getSystemService().getMachineName(), time.strftime("%Y-%m-%d_%H.%M.%S"))

            try:
                with open(filename, "w") as f:
                    f.write(data)
                log.info("Created capacitance offset log file: %s", filename)
            except OSError as ex:
                log.warning("Error writing capacitance offset log file '%s'. Details: ", filename, ex)

        c.waitForQueueToBeEmpty(wait_for_motion_controller=True)
