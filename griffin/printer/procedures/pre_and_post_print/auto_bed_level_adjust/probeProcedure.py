from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.steps.gotoPositionStep import GotoPositionStep
from griffin.printer.procedures.pre_and_post_print.auto_bed_level_adjust.probeZStep import ProbeZStep
from griffin.printer.procedures.wizard.sleepStep import SleepStep
from griffin.printer.buildPlateLocations import BuildPlateLocations
import time
import logging

log = logging.getLogger(__name__.split(".")[-1])

class ProbeProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)
        self.__probe_result_storage = {}
        self._last_probe_height = None
        self._last_probe_timestamp = None
        self._probe_step = None
        self._lc = None

    def setup(self, args):
        self._probe_step = ProbeZStep("PROBING", self.__probe_result_storage)
        self._probe_step.stepFinished.connect(self._probe_finished)

        steps = [
            GotoPositionStep("GOTO_POS", **args),
            SleepStep("SLEEP", 0.01),
            self._probe_step,
            GotoPositionStep("MOVE_BED_FOR_SAFE_TRAVEL", z=BuildPlateLocations.getSafeTravelHeight()),
        ]

        for n in range(0, len(steps) - 1):
            steps[n].setNextStep(steps[n + 1])
        self.setFirstStep(steps[0])
        return True

    def _probe_finished(self):
        self._last_probe_height = self.__probe_result_storage.get("z", default=150)
        self._probe_retry_counter = self._probe_step.getRetryCount()
        self._last_probe_timestamp = int(time.monotonic())

    def getMetaData(self):
        if self._last_probe_timestamp is None:
            return {}
        return {
            "last_probe_height": self._last_probe_height,
            "loopcount_of_probe": self._probe_retry_counter,
            "last_probe_timestamp": self._last_probe_timestamp
        }
