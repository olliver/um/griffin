from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.queueRawRelativeGCodeStep import QueueRawRelativeGCodeStep
from griffin.printer.procedures.queueRawGCodeWaitStep import QueueRawGCodeWaitStep
from griffin.printer.procedures.steps.setPropertyStep import SetPropertyStep
from griffin.printer.procedures.steps.homeBedStep import HomeBedStep
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.pre_and_post_print.resetRetractedAmountStep import ResetRetractedAmountStep
from griffin.printer.procedures.temperature.waitForAllHotendsTemperatureStep import WaitForAllHotendTemperatureStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


## This procedure deprimes the nozzles so that the next print can start with approximately known start conditions,
##  the material is not stuck to the hotend and no drop forming at the end of the filament.
class DeprimeProcedure(Procedure):
    FUDGE_FACTOR_PRINTING_TEMPERATURE = 35.0 # degC
    FUDGE_FACTOR_RETRACTION_LENGTH = 1 # mm
    QUICK_RETRACT_DIST = 4.5

    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)

    def setup(self, args):
        c = Controller.getInstance()
        hotend_count = c.getPropertyValue("hotend_count")

        hotends = range(hotend_count)
        # Determine whether or not to deprime all hotends or just 1
        if "hotend_nr" in args:
            try:
                hotend_nr = int(args.get("hotend_nr"))
                # Make sure the range is from nr to nr +1 , otherwise this gives a []
                if hotend_nr < hotend_count:
                    hotends = range(hotend_nr, hotend_nr + 1)
            except ValueError:
                # Do nothing, assume(!) all hotends are to be deprimed
                pass

        deprime_amount = c.getPropertyValue("deprime_retraction_amount")
        deprime_speed = c.getPropertyValue("deprime_retraction_speed") * 60
        idle_amount = c.getPropertyValue("idle_retraction_amount")

        set_temp_steps = []
        wait_steps = []
        after_temp_steps = []
        second_wait_steps =[]
        after_second_wait = []
        cooldown_steps = []

        active_hotend = c.getActiveHotend()
        if active_hotend is None:
            # set the 0'th controller as default if the active hotend is None/unknown.
            active_hotend = 0

        printing_temperature = c.getMaterialManager().getPrintingTemperature(active_hotend)
        temp = c.getPrintHeadController().getHotendSlot(active_hotend).getPropertyValue("current_temperature")
        retracted = c.getHotendRetractedLength(active_hotend)
        quick_retracted = False

        if temp > printing_temperature - self.FUDGE_FACTOR_PRINTING_TEMPERATURE and retracted < self.QUICK_RETRACT_DIST:
            log.info("quick retracting hotend(%d) %.1f mm to prevent oozing", active_hotend, self.QUICK_RETRACT_DIST)
            set_temp_steps.append(QueueRawRelativeGCodeStep("QUICK_RETRACT", "G1 E{distance} F{speed}".format(distance=-self.QUICK_RETRACT_DIST, speed=deprime_speed)))
            quick_retracted = True
        set_temp_steps.append(HomeBedStep("HOME_BED"))

        for hotend_nr in hotends:
            hotend = c.getPrintHeadController().getHotendSlot(hotend_nr)
            present = hotend.getPropertyValue("is_present")
            if not present:
                log.info("hotend({hotend_nr}) is not present".format(hotend_nr=hotend_nr))
                continue

            standby_temperature = c.getMaterialManager().getStandbyTemperature(hotend_nr)
            printing_temperature = c.getMaterialManager().getPrintingTemperature(hotend_nr)
            retracted = c.getHotendRetractedLength(hotend_nr)

            log.info("hotend({hotend_nr}) is already {retracted} mm retracted.".format(hotend_nr=hotend_nr, retracted=retracted))
            # Retract even more when the temperature has dropped below the standby temperature (+ 5C).
            # This extra retraction is used to break off the string that is attached to the end of the material.
            # Often this is used in a two-stage process: first deprime to stop oozing and then in a second
            # call, with lower temperature, retract the idle_amount to break off the string.

            moved = 0
            if hotend_nr == active_hotend and quick_retracted:
                moved -= self.QUICK_RETRACT_DIST

            if retracted <= deprime_amount - self.FUDGE_FACTOR_RETRACTION_LENGTH:
                if hotend.getPropertyValue("current_temperature") < printing_temperature - self.FUDGE_FACTOR_PRINTING_TEMPERATURE:
                    log.info("going to heat up hotend(%d) because it is %d degC", hotend_nr, hotend.getPropertyValue("current_temperature"))
                    set_temp_steps.append(SetPropertyStep("START_SETTLE_HOTEND_{hotend_nr}".format(hotend_nr=hotend_nr),
                        "printer.head.0.hotend.{hotend_nr}".format(hotend_nr=hotend_nr), "pre_tune_target_temperature", printing_temperature))
                    wait_steps.append(OtherProcedureStep("HEATUP_HOTEND_WAIT_{hotend_nr}".format(hotend_nr=hotend_nr),
                        "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": hotend_nr, "target": printing_temperature}))
                after_temp_steps.append(QueueRawGCodeWaitStep("SELECT_HOTEND", "T{hotend_nr}".format(hotend_nr=hotend_nr)))
                moved2 = retracted + -moved - deprime_amount
                after_temp_steps.append(QueueRawRelativeGCodeStep("RETRACT", "G1 E{distance} F{speed}".format(distance=moved2, speed=deprime_speed)))
                log.info("retracting hotend(%d) %d mm", hotend_nr, moved2)
                moved += moved2

            # moved is negative but added up to the retracted amount so inverted.
            if retracted + -moved <= deprime_amount + idle_amount - self.FUDGE_FACTOR_RETRACTION_LENGTH:
                after_temp_steps.append(SetPropertyStep("START_COOLDOWN_HOTEND_{hotend_nr}".format(hotend_nr=hotend_nr),
                    "printer.head.0.hotend.{hotend_nr}".format(hotend_nr=hotend_nr), "pre_tune_target_temperature", standby_temperature))
                after_second_wait.append(QueueRawGCodeWaitStep("SELECT_HOTEND", "T{hotend_nr}".format(hotend_nr=hotend_nr)))
                distance = retracted + -moved - deprime_amount - idle_amount
                after_second_wait.append(QueueRawRelativeGCodeStep("DEPRIME", "G1 E{distance} F{speed}".format(distance=distance, speed=deprime_speed)))
                log.info("depriming hotend(%d) %d mm", hotend_nr, distance)

            cooldown_steps.append(SetPropertyStep("START_COOLDOWN_HOTEND_{hotend_nr}".format(hotend_nr=hotend_nr), "printer.head.0.hotend.{hotend_nr}".format(hotend_nr=hotend_nr), "pre_tune_target_temperature", 0.0))

        second_wait_steps.append(WaitForAllHotendTemperatureStep("COOLDOWN_HOTENDS_WAIT"))
        steps = set_temp_steps + wait_steps + after_temp_steps + second_wait_steps + after_second_wait

        # Reset both values tracking filament position
        steps.append(ResetRetractedAmountStep("RESET_RETRACTED_AMOUNT")) # this should not be neccesary however the position tracking keeps fucking up the amount.
        for hotend_nr in range(c.getPropertyValue("hotend_count")):
            if c.getPrintHeadController().getHotendSlot(hotend_nr).getPropertyValue("is_present"):
                steps.append(SetPropertyStep("SET_DEPRIMED_{hotend_nr}".format(hotend_nr=hotend_nr), "printer.head.0.hotend.{hotend_nr}".format(hotend_nr=hotend_nr), "is_primed", False))
        # the previous 4 lines must be happen before the cooldown_steps as that is the point where the abort hooks in!

        steps += cooldown_steps
        steps.append(QueueRawGCodeWaitStep("RESET_HOTEND", "T{hotend_nr}".format(hotend_nr=active_hotend)))

        # Set the abort step as the first cooldown step. This aborts the depriming, but leaves the system in a proper state.
        self.setAbortStep(cooldown_steps[0])

        for i, step in enumerate(steps):
            if i != 0:
                steps[i-1].setNextStep(step)
            else:
                self.setFirstStep(step)

        return True

    def getMetaData(self):
        metadata = super().getMetaData()
        if metadata is None:
            metadata = {}

        active_step = self.getActiveStep()
        if active_step is None or active_step.getKey() !=  "COOLDOWN_HOTENDS_WAIT":
            metadata["progress"] = 0.0
        return metadata
