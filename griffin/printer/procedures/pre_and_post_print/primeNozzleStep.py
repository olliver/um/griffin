from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import time
import logging

log = logging.getLogger(__name__.split(".")[-1])

class PrimeNozzleStep(ProcedureStep):
    STRATEGIES = [ "turd", "none" ] # the list of priming strategies
    # in the Turd priming strategy some time should be spent waiting for the pressure created to subside, and material stops oozing too much.
    RELIEVE_PRESSURE_WAIT_TIME_1 = 0.2
    RELIEVE_PRESSURE_WAIT_TIME_2 = 2 * RELIEVE_PRESSURE_WAIT_TIME_1

    def __init__(self, key):
        super().__init__(key)
        self._prime_strategy = self.STRATEGIES[0]

    ## Sets the strategy to be used, the strategy must be a string from from the STRATEGIES variable in this class.
    # @param strategy The prime strategy to be used.
    def setStrategy(self, strategy):
        if strategy not in self.STRATEGIES:
            raise ValueError
        self._prime_strategy = strategy

    def run(self):
        controller = Controller.getInstance()
        inverse_flow = 1 / (controller.getPropertyValue("extrusion_amount_modifier") / 100)
        active_hotend_nr = controller.getActiveHotend()
        retracted_amount = controller.getHotendRetractedLength(active_hotend_nr) * inverse_flow
        prime_speed = controller.getPropertyValue("deprime_retraction_speed") * 60 / 2
        prime_amount = controller.getPropertyValue("prime_amount") * inverse_flow
        deprime_amount = controller.getPropertyValue("deprime_retraction_amount") * inverse_flow
        y = controller.getPosition("Y")
        retract = not controller.getProcedure("PRINT").getGcodeMetaData().isValid()
        material_type = controller.getMaterialManager().getMaterialType(active_hotend_nr)


        # Set E position to 0
        controller.queueGCode("G92 E0")
        # extrude the configured amount and undo the presumed de-priming done at the end of the last print
        controller.queueGCode("G1 Z2")
        hotend_nr = controller.getActiveHotend()

        # Add tracking of the primed state.
        controller.getPrintHeadController().getHotendSlot(hotend_nr).setPropertyValue("is_primed", True)

        # TODO: remove the turd priming strategy with EM-1161
        if self._prime_strategy == "turd":
            # when PVA is retracted it seems to form a thick string which then hardens and is not in contact with the heated part of the nozzle.
            # to compensate for this prime a bit slower, this hack can be removed when CURA takes over the priming move completely with the "none" strategy.
            if material_type == "PVA":
                prime_speed /= 4

            # Set E position to 0
            controller.queueGCode("G92 E0")
            # extrude the configured amount and undo the presumed de-priming done at the end of the last print
            controller.queueGCode("G1 Z2")
            e = retracted_amount
            controller.queueGCode("G1 E%f F%f" % (e, prime_speed))
            log.info("unretracting %0.2f mm", e)
            e +=  prime_amount * 0.5
            controller.queueGCode("G1 E%f F%f" % (e, prime_speed / 4))
            # move up while priming so you attach your noodles to the buildplate

            e +=  prime_amount * ((2 / 3) - 0.5)
            controller.queueGCode("G1 E%f Z6 F%f" % (e, prime_speed / 2))

            e += prime_amount * (1 / 3)
            controller.queueGCode("G1 E%f F%f" % (e, prime_speed / 4))
            log.info("priming %0.2f mm (a total including unretract of %0.2f mm)", prime_amount, e)
            controller.waitForQueueToBeEmpty()
            time.sleep(self.RELIEVE_PRESSURE_WAIT_TIME_1) # replaced marlin dwell with sleep here.
            controller.queueGCode("G92 E0")

            controller.setRetractedLength(0) # zero extruded length

            if retract:
                controller.queueGCode("G1 E%f F%f" % (-deprime_amount, prime_speed * 2)) # retract for old style prime/print
            controller.waitForQueueToBeEmpty()
            time.sleep(self.RELIEVE_PRESSURE_WAIT_TIME_2) # replaced marlin dwell with sleep here.

            # Z or E moves do not need to be speed compensated, this is the only here that has a XY component.
            speed = 20.0 * (1 / (controller.getPropertyValue("movement_speed_modifier") / 100))
            controller.queueGCode("G1 Y%f Z2 F%d" % (y + 10, speed * 60.0))    # wipe nozzle down and to the center of the machine.

        elif self._prime_strategy == "none":
            controller.queueGCode("G92 E%f" % -retracted_amount)
