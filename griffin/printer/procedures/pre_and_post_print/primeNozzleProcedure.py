import logging

from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.steps.gotoPositionStep import GotoPositionStep
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.pre_and_post_print.primeNozzleStep import PrimeNozzleStep
from griffin.printer.procedures.dummyStep import DummyStep

log = logging.getLogger(__name__.split(".")[-1])


## The PrimeNozzleProcedure makes reasonably sure the material will be at the tip of the nozzle when the print starts extruding.
# There are some agreements made with Cura to facilitate this, this boils down to Cura/the gcode is in charge,
# a prime here gcode has been made in griffin (not in marlin).
# the prime gcode can cause this procedure to be called but for backwards compatibility when no header is present
# we prime before the gcode starts, we don't retract though which will cause stringing and makes sure everyone will want to use the header.
# cura start state: https://docs.google.com/document/d/1zU30i5x3ll53R2cjhmtx4DibKlJQmmqF5vGUmZdI97c
# header definition: https://docs.google.com/document/d/1ex3VDTSN0PipNgon1rXXXeWti1jPYhrnAjOCanxJKAg
class PrimeNozzleProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)

    # args from_gcode can be set to true.
    # if from_gcode is set speed may be set as well.
    # no other settings are parsed.
    def setup(self, args):
        controller = Controller.getInstance()
        valid_header = controller.getProcedure("PRINT").getGcodeMetaData().isValid()

        if "from_gcode" in args and args["from_gcode"]:
            prime = PrimeNozzleStep("PRIME_NOZZLE")
            if "strategy" in args and args["strategy"] is not None and args["strategy"] != "":
                prime.setStrategy(PrimeNozzleStep.STRATEGIES[min(len(PrimeNozzleStep.STRATEGIES), max(0, int(args["strategy"])))])
            self.setFirstStep(prime)
        elif not valid_header:
            # if we don't have a header react like normal
            prime_location = controller.getPropertyValue("hotend_prime_location")
            goto_0 = GotoPositionStep("MOVE_TO_POS", x=prime_location[0], y=prime_location[1], z=20)
            goto_1 = GotoPositionStep("MOVE_TO_POS", x=prime_location[0] - 5, y=prime_location[1], z=20)
            
            switch_to_hotend_0 = OtherProcedureStep("SWITCH_TO_HOTEND_0", "SWITCH_ACTIVE_HOTEND", args={"target": 0})
            switch_to_hotend_1 = OtherProcedureStep("SWITCH_TO_HOTEND_1", "SWITCH_ACTIVE_HOTEND", args={"target": 1})

            prime_0 = PrimeNozzleStep("PRIME_0")# primes active nozzle/hotend
            prime_1 = PrimeNozzleStep("PRIME_1")

            step = self.setFirstStep(switch_to_hotend_1).setNextStep(goto_0).setNextStep(prime_1)
            step.setNextStep(switch_to_hotend_0).setNextStep(goto_1).setNextStep(prime_0)
        else:
            step = DummyStep("DUMMY_PRIME")
            self.setFirstStep(step)

        return True
