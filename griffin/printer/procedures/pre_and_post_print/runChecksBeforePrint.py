from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode, FaultLevel
from griffin.thread import Event
import time
import logging
import traceback

log = logging.getLogger(__name__.split(".")[-1])


## This class is a step that is meant to delay printing while the user is prompted to fulfill some necessary preconditions for printing.
class RunChecksBeforePrint(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)
        self._message = None
        self._event = Event()
        self._wait_reason = None

    def run(self):
        c = Controller.getInstance()
        origin_is_web_api = False
        # check bed level and z offset are known or auto-magical probing is on.
        p = c.getProcedure("MANUAL_BED_PLUS_NOZZLE_OFSET_WIZARD")
        p.onFinished.connect(self._triggerEvent)
        leveling_mode = c.getPreference("BED_LEVEL_CORRECTION_MODE")
        while (leveling_mode == "manual" and not c.hasBuildPlateLevelData()) and not self._abort:
            log.info("(%s == \"manual\" and not %s) and not %s" % (leveling_mode, c.hasBuildPlateLevelData(), self._abort))
            self._wait_reason = "MANUAL_BED_LEVEL_UNKNOWN"
            self._event.wait()
            self._event.clear()
            # leveling mode can no longer be changed in manual level wizard

        p.onFinished.disconnect(self._triggerEvent)
        if self._abort:
            return # return now so that if other checks are added it is clear we will stop here.

        gcode_metadata = c.getProcedure("PRINT").getGcodeMetaData()

        if len(gcode_metadata.getExtruderTrains()) > 1 and not c.hasXYOffsetData():
            FaultHandler.warning("no xy calibration data for this nozzle combo", FaultCode.XY_CALIBRATION_NOT_DONE)
            while FaultHandler.getInstance().getError().getLevel() != FaultLevel.NO_ERROR and not self._abort:
                time.sleep(0.1)

            if self._abort or c.getProcedure("PRINT").isAborted():
                # if I clear the warning, then abort the print, the cleared error arrives here first and displays a
                # new warning, After which the print is aborted and the next warning is visible, this is very confusing!
                FaultHandler.clear(FaultLevel.WARNING, FaultCode.XY_CALIBRATION_NOT_DONE) # so instead of clearing the warning from the display when the print is aborted, all warnings are cleared!
                return

        metadata = c.getProcedure("PRINT").getMetaData()
        if "origin" in metadata and metadata["origin"] == "WEB_API":
            origin_is_web_api = True
            # origin_is_web_api header checks Cura/The app will have to give warnings for all of this!
            # it would be annoying to OK the same error twice, once on the printer and once in cura!

        check_functions = gcode_metadata.getHeaderChecks()
        for (check_if, check_function) in check_functions:
            if origin_is_web_api and check_if != gcode_metadata.CHECK_ALWAYS:
                continue

            try:
                check_function()
            except Exception:
                traceback.print_stack()

            while FaultHandler.getInstance().getError().getLevel() != FaultLevel.NO_ERROR and not self._abort:
                time.sleep(0.1)

            if self._abort or c.getProcedure("PRINT").isAborted():
                # if I clear the warning, then abort the print, the cleared error arrives here first and displays a
                # new warning, After which the print is aborted and the next warning is visible, this is very confusing!
                FaultHandler.clear(FaultLevel.WARNING) # so instead of clearing the warning from the display when the print is aborted, all warnings are cleared!
                return

        ## next check goes here!

        self._wait_reason = None

    def getMetaData(self):
        if self._wait_reason is not None:
            return {"wait_reason": self._wait_reason}
        return {}

    ## need to break out of wait for event loop to abort.
    def abort(self):
        super().abort()
        self._event.set()

    ## either a message or a different trigger can be used to continue from the check such as an ignore this check message.
    def receiveMessage(self, message):
        self._message = message
        self._event.set()

    ## an internal message can be used as the user can choose to start a bed leveling wizard, when that is finished the reason to wait might be changed!
    def _triggerEvent(self, *args, **kwargs):
        time.sleep(0.5) # TODO: this is a fix for race condition like behavior described in EM-675 when fixed this line can be removed.
        self._event.set()