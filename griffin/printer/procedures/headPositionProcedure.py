import logging

from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.steps.gotoPositionStep import GotoPositionStep
from griffin.printer.controller import Controller


log = logging.getLogger(__name__.split(".")[-1])


# This procedure is used from the HTTP API to get the head position and move the head to a certain position.
class HeadPositionProcedure(Procedure):
    def __init__(self, key, x=.0, y=.0, z=.0):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        log.info("x: %s y: %s z: %s" % (x, y, z))
        self._x = x
        self._y = y
        self._z = z
        self._step = GotoPositionStep("MOVING_HEAD", x, y, z, wait_for_queue_empty=False)
        self.setFirstStep(self._step)

    def findPosition(self):
        c = Controller.getInstance()

    def setup(self, args):
        position = Controller.getInstance().getPositionArray()
        if "x" in args:
            self._x = int(args["x"])
        else:
            self._x = position[0]
        if "y" in args:
            self._y = int(args["y"])
        else:
            self._y = position[1]
        if "z" in args:
            self._z = int(args["z"])
        else:
            self._z = position[2]

        self._step._x = self._x
        self._step._y = self._y
        self._step._z = self._z
        return True

    def getMetaData(self):
        position = Controller.getInstance().getPositionArray()
        return {
            "target": {
                "x": self._x,
                "y": self._y,
                "z": self._z
            },
            "current": {
                "x": position[0],
                "y": position[1],
                "z": position[2]
            }
        }
