from griffin import signal
from griffin import thread
from threading import Lock
from .procedureStep import StepAndProcedureState
from .procedureStep import ProcedureStep

import time
import logging

log = logging.getLogger(__name__.split(".")[-1])


#  Base class for all procedures.
#  Procedures are created during startup of the printer service by the controller.
#  Each procedure has a unique key which identifies this procedure.
#  Every procedure has one or more steps that get executed when this procedure is ran.
#  The procedure has a setup step with parameters, during this setup step, the procedure can report that it cannot run.
class Procedure(signal.SignalEmitter):
    # Not every procedure currently has a clearly defined type. Procedures that not clearly need to set the printer general state have this type.
    TYPE_UNKNOWN = 0
    # When the machine is printing, a procedure with this type is active.
    TYPE_PRINT = 1
    # We have a lot of maintenance related procedures, like change material. When these are active the machine should be marked as in maintenance and not ready for printing.
    TYPE_MAINTENANCE = 2
    # We have some procedures that run during booting. When these procedures are active, the machine is not ready yet and thus should report that.
    TYPE_BOOT = 3

    # Procedure types. Every procedure has a type, which is used to generate the general printer service state. (see printerService.py)
    TYPES = [TYPE_UNKNOWN, TYPE_PRINT, TYPE_MAINTENANCE, TYPE_BOOT]

    onStart = signal.Signal()
    onNextStep = signal.Signal()
    onFinished = signal.Signal()

    def __init__(self, key, procedure_type):
        super().__init__()

        assert procedure_type in Procedure.TYPES

        self._key = key
        self.__type = procedure_type

        # The current state of this procedure.
        self._procedure_state = StepAndProcedureState.Setup

        # Changing the active_step and _abort is critical.
        self._active_step_lock = Lock()

        # The current step being executed. This can also be one of the abort
        # steps.
        self._active_step = None

        # Initial step of the normal path to be executed.
        self._initial_step = None

        # Disables the abort for this procedure.
        self._abort_disabled = False

        # May be set by abort() or by receiving the ABORT message. During
        # normal path, don't start the next step, but the first step of the
        # "abort path" instead. The "abort path" isn't affected.
        self._abort = False

        # The abort step to be executed on abort.
        self._abort_step = None

        self._ready_event = thread.Event()
        self._ready_event.set()
        self._dependents = list()

    def setup(self, args):
        return True

    def start(self):
        """ The default start()

        Note it's not allowed to start the same instance in different threads,
        but it's common that the previous thread continues because steps are
        run in a seperate thread.
        """
        return self._protectedStart(False)

    def _protectedStart(self, skip_controller_check):
        """ Protected start enabling skip of controller

        Expected to be called from this class or from firmwareUpdateProcedure.
        """

        # Just don't run with error logged if trying to run twice. The
        # caller should first check canBeExecuted().
        if not self._ready_event.is_set():
            log.error("Start procedure failed, because previous not ready")
            return

        assert(self._initial_step is not None)
        assert(self._active_step is None)

        if not skip_controller_check:
            from griffin.printer.controller import Controller
            while not Controller.getInstance().getDriver().isConnected():
                time.sleep(0.5)

        self._startCriticalInitialStep()

    def getMetaData(self):
        if self._active_step is not None and isinstance(self._active_step, ProcedureStep):
            return self._active_step.getMetaData()
        return {"EMPTY_ORIGIN": "Procedure"}

    def _startActiveStep(self):
        """ Start the active step.

        The step start() will run in a seperate thread. When finished, the
        self._stepDone() will be called.
        """

        self._active_step.stepFinished.connect(self._stepDone)

        self._active_step.start()

    def _startCriticalInitialStep(self):
        """ Set the initial step.

        Note it doesn't require locking, since
        """

        with self._active_step_lock:

            # Note this doesn't neet
            self._ready_event.clear()
            self._procedure_state = StepAndProcedureState.Running
            self._abort = False
            self._active_step = self._initial_step
            self.onStart.emit(self, self._active_step)
            self._startActiveStep()

    def __criticalActiveStepHandling(self):
        """ Handle the step result and return the next step """

        with self._active_step_lock:

            # Finishes the step, returning the finish reason.
            finish_reason = self._active_step.finishStep()

            # Still running the procedure as expected.
            if self._procedure_state is StepAndProcedureState.Running:

                if finish_reason is StepAndProcedureState.Finished or (self._abort_disabled and finish_reason is StepAndProcedureState.Aborted):

                    # Step finished normal and no abort requested. Next step.
                    if not self._abort:
                        self._active_step = self._active_step.getNextStep()

                        # Last step, thus really done
                        if self._active_step is None:
                            self._procedure_state = StepAndProcedureState.Finished

                    # We do initiate the abort ourselves.
                    else:
                        log.info("Procedure '%s' to Aborted state (internal)", self._key)
                        self._active_step = self._abort_step
                        self._procedure_state = StepAndProcedureState.Aborting

                # We start the abort procedure because the step returned an abort.
                elif finish_reason is StepAndProcedureState.Aborted:
                    self._active_step = self._abort_step
                    self._procedure_state = StepAndProcedureState.Aborting
                    log.info("Procedure '%s' to Aborted state (following step state)", self._key)

                # We start the abort procedure because the step returned an error.
                elif finish_reason is StepAndProcedureState.Error:
                    log.info("Procedure '%s' to Error state (following step state)", self._key)
                    self._active_step = self._abort_step
                    self._procedure_state = StepAndProcedureState.ErrorAborting

            # We are aborting already, either due to a normal abort or due to
            # an error.
            elif (self._procedure_state is StepAndProcedureState.Aborting or
                  self._procedure_state is StepAndProcedureState.ErrorAborting):

                self._active_step = self._active_step.getNextStep()

                if self._active_step is not None:
                    log.info("Aborting, active step: %r", self._active_step)
                else:
                    # Really finished.

                    # To Aborted if Aborting finished.
                    if self._procedure_state is StepAndProcedureState.Aborting:
                        log.info("Procedure abort finished")
                        self._procedure_state = StepAndProcedureState.Aborted

                    # To Error if ErrorAborting finished.
                    elif self._procedure_state is StepAndProcedureState.ErrorAborting:
                        log.info("Procedure error finished")
                        self._procedure_state = StepAndProcedureState.Error

            # Should not have occured.
            else:
                self._active_step = None
                self._procedure_state = StepAndProcedureState.Error
                log.error("Unexpected state")

    def _stepDone(self):
        """ Step finished, start next step if available or finish """

        # Note this can only be reached on stepFinished of _active_step.
        self._active_step.stepFinished.disconnect(self._stepDone)

        self.__criticalActiveStepHandling()

        # Next step
        if self._active_step is not None:
            log.debug("stepDone starts next active_step: %s", self._active_step._key)
            self.onNextStep.emit(self, self._active_step)
            self._startActiveStep()

        # All steps finished. Either normal or after abort or error
        else:
            log.debug("stepDone is finished")
            self._ready_event.set()
            self.onFinished.emit(self)

    def isActive(self):
        return not self._ready_event.is_set()

    def canBeExecuted(self):
        if self._ready_event.is_set():
            return True
        return False

    def getKey(self):
        return self._key

    def isPrintProcedure(self):
        return self.__type == Procedure.TYPE_PRINT

    def isMaintenanceProcedure(self):
        return self.__type == Procedure.TYPE_MAINTENANCE

    def isBootProcedure(self):
        return self.__type == Procedure.TYPE_BOOT

    def setFirstStep(self, step):
        assert(isinstance(step, ProcedureStep))
        self._initial_step = step
        return step

    def setAbortStep(self, step):
        """ The steps from here will be completed after abort.  """
        assert(step is None or isinstance(step, ProcedureStep))
        self._abort_step = step

    def getActiveStep(self):
        """ Returns the reference to the active step.

        Note this reference SHOULD only be used for read only access. The caller
        will never be sure it this is still the active step.

        """

        return self._active_step

    def setAbortDisabled(self, abort_disabled):
        """ Set the disable abort for the procedure.

        Note this should be handled with care, normally every step and procedure
        is expected to be able to abort again, within a reasonable amount of
        time. However, for example for "shutdown/postprint" actions, the
        usage might be legit.
        """
        self._abort_disabled = abort_disabled

    def isAbortDisabled(self):
        return self._abort_disabled

    def abort(self):
        """ Abort the procedure if abort isn't disabled. """

        # We want to be sure that we inform the real active step, not "half way
        # in between". Otherwise the new step may become active without being
        # notified.
        with self._active_step_lock:

            # Abort only possible during Running state.
            if (self._procedure_state is StepAndProcedureState.Running and
                    not self._abort_disabled):
                self._abort = True
                if self._active_step:
                    self._active_step.abort()

    def receiveMessage(self, message):
        """ Handle the default procedure message and pass on messages to step.

        ABORT message is handled by the procedure, but still passed on for
        printFileStep compatibility. Rest aborts on the _abort flag.
        """

        # We want to be sure that we inform the real active step, not "half way
        # in between". Otherwise the new step may become active without being
        # notified.

        log.info("Message: %s", message)
        with self._active_step_lock:

            if message == "ABORT":
                # Abort only possible during Running state.
                log.info("Abort message received")
                if (self._procedure_state is StepAndProcedureState.Running and
                        not self._abort_disabled):

                    log.info("Start abort")

                    # We do pass the message, but also make sure we will stop the
                    # next steps. Also cancel the active step.
                    self._abort = True
                    if self._active_step:
                        log.info("Pass abort to %s", self._active_step.getKey())
                        self._active_step.abort()

            else:
                # Pass on. Note the default step handler handles ABORT. But if it's
                # superseeded it might be blocked anyway.
                if self._active_step is not None:
                    self._active_step.receiveMessage(message)

    def isAborted(self):
        return self._abort

    def join(self):
        self._ready_event.wait()

    def getFinishReason(self):
        return self._procedure_state
