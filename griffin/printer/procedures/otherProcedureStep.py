from .procedureStep import ProcedureStep
from .procedureStep import StepAndProcedureState
from griffin.printer.controller import Controller
from griffin import signal
from griffin import thread

import logging

log = logging.getLogger(__name__.split(".")[-1])


class OtherProcedureStep(ProcedureStep):
    def __init__(self, key, procedure_key, args={}):
        super().__init__(key)

        self._procedure = None
        self._procedure_key = procedure_key
        self._procedure_setup_args = args

        # Flag to abort the run() on _onProcedureFinished.
        self._finished_event = thread.Event()

        # May be set by abort() or by receiving the ABORT message. During
        # normal path, don't start the next step, but the first step of the
        # "abort path" instead. The "abort path" isn't affected.
        self._abort = False

        # The abort step to be exeucted on abort.
        self._abort_step = None

    # Getter for the called procedure
    def getProcedure(self):
        return self._procedure

    # This function is allowed to block as it is called from a separate thread
    # and will not block the rest of the service.
    def run(self):
        self._finished_event.clear()
        self._procedure = Controller.getInstance().getProcedure(self._procedure_key)

        if self._procedure is None:
            log.error("Other procedure not found: %s", self._procedure_key)

            # Procedure is not started, so we only have to abort ourselves.
            self._abort = True
            return

        if not self._procedure.setup(self._procedure_setup_args):
            log.warning("Failed to setup procedure: %s", self._procedure_key)
            log.warning("With parameters: %s", self._procedure_setup_args)

            # Procedure is not started, so we only have to abort ourselves.
            self._abort = True
            return

        self._procedure.onFinished.connect(self._onProcedureFinished)
        self._procedure.start()

        # No abort implementation here, it's the "procedure" that has to abort.
        while not self._finished_event.is_set() and not self._abort:
            self._finished_event.wait(self.TIME_OUT_WAIT_FINISHED_EVENT)

        # If this step is aborting, then notify the downstream procedure before this step is really aborted.
        # This we call the downstream abortflow.
        if self._abort:
            self._procedure.abort()

            while not self._finished_event.is_set():
                self._finished_event.wait(self.TIME_OUT_WAIT_FINISHED_EVENT)
        # When the abortflow is upstream (our called procedure was aborted).
        elif self._procedure.getFinishReason() != StepAndProcedureState.Finished:
            self._abort = True

    def setAbortStep(self, step):
        """ This step will be completed after abort.

        Note the abort step may also be a "otherProcedureStep" to finish a
        series of steps. Note "next steps" might be added, but will be
        ignored. A chain of steps on abort should be a procedure, not abusing
        the current procedure to abort.
        """
        assert(step is None or isinstance(step, ProcedureStep))
        self._abort_step = step

    def _onProcedureFinished(self, procedure):
        self._procedure.onFinished.disconnect(self._onProcedureFinished)
        self._finished_event.set()

    def getMetaData(self):
        if self._procedure is not None:
            return self._procedure.getMetaData()
        else:
            return {"EMPTY_ORIGIN": "OtherProcedureStep"}

    def abort(self):
        super().abort()
        if self._procedure is not None:
            self._procedure.receiveMessage("ABORT")
