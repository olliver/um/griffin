from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.gcode2DisplayStep import GCode2DisplayStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


class GCode2DisplayProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._step = GCode2DisplayStep("DISPLAYING")
        self.setFirstStep(self._step)

    def setup(self, args):
        if "text" in args:
            self._step.setTarget(args["text"].strip())
        return True
