from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class WaitForQueueToBeEmptyStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()
        c.waitForQueueToBeEmpty(wait_for_motion_controller = True)
