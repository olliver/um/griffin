from enum import Enum
from griffin import signal
from griffin.thread import Thread

import logging

log = logging.getLogger(__name__.split(".")[-1])


class StepAndProcedureState(Enum):
    """ Provide some states to make everything safer. """
    Setup = 1
    Running = 2
    Finished = 3
    Aborting = 4
    ErrorAborting = 5
    Aborted = 6
    Error = 7


class ProcedureStep(signal.SignalEmitter):
    """ Runs a step in its own thread.

    Expected flows that are handled:

        Setup --> Running on start()
                  +--> Finished on normal run() return
                  +--> Aborted if run() returns with _abort flag set.
                       +--> Setup on finishStep()
                  +--> Error if run() returns due to an exception.
                       +--> Setup on finishStep()
    """

    TIME_OUT_WAIT_FINISHED_EVENT = 0.1      # A time-out value used in thread wait events. Use low values like 0.1s to improve quick system responses.

    stepFinished = signal.Signal()

    def __init__(self, key):
        super().__init__()

        self._key = key
        self._next_step = None
        self._step_state = StepAndProcedureState.Setup

        # May be set by abort() or by receiving the ABORT message. During
        # normal path, don't start the next step, but the first step of the
        # "abort path" instead. The "abort path" isn't affected.
        self._abort = False

    # Start the current procedure. Start this procedure on a new step.
    def start(self):
        """ Start this procedure step, which runs in a seperate thread. """

        # Something went wrong before. Refuse to start (possible second) thread.
        assert(self._step_state is StepAndProcedureState.Setup)
        self._abort = False
        self._step_state = StepAndProcedureState.Running
        thread = Thread("Procedure step %s" % (self._key), self.__private_run, daemon=True)
        thread.start()

    def __private_run(self):
        """ Local run to handle exceptions of the run() thread.

        This makes the implementation safer, since exiting threads will lock up
        everything because of missing stepFinished events.
        """
        try:
            self.run()

            if self._abort:
                log.info("Step to Aborted state: %s", self._key)
                self._step_state = StepAndProcedureState.Aborted
            else:
                log.debug("Step to Finished state: %s", self._key)
                self._step_state = StepAndProcedureState.Finished

        except:
            log.exception("Exception caught from step: %s", self._key)
            self._step_state = StepAndProcedureState.Error

        # The step is finished, either normal or with abort or error.
        self.stepFinished.emit()

    # This function is allowed to block as it is called from a separate thread and will
    # not block the rest of the service.
    def run(self):
        raise NotImplementedError()

    def finishStep(self):
        """ Return the finish reason and reset our state.

        This implies that the run() function returned and the controlling
        function is getting the results now. Not our responsibility anymore and
        this step is ready to be started again.

        Note there's no abort or recovery of an infinite run() yet. Doing
        nothing in this case is at least more secure than continuing, because
        otherwise the thread may wake-up and execute code on really unexpected
        moments.
        """
        retval = self._step_state

        self._step_state = StepAndProcedureState.Setup

        return retval

    # Get the next procedure step in the chain.
    # If the next step is None the procedure is finished and will be marked as inactive.
    def getNextStep(self):
        return self._next_step

    def abort(self):
        """ Abort the step. """
        self._abort = True

    def receiveMessage(self, message):
        """ Default receive message handler.

        External services can send a message to active procedures. These
        messages are passed to the currently active procedure step at which
        point this default handler is called if the real step doesn't implement
        its own handler.
        """
        return

    def _protectedSetNextStepObsolete(self, step):
        """ Custom protected set next step - OBSOLETE

        The following classes abuse the setNextStep runtime. This should not be
        allowed and should be changed. Only because it's handled in the run()
        before returning and finishing, it's "sort of" safe.

        - procedures/wizard/doOnceStep.py
        - procedures/wizard/nextWizardStepChain.py
        - procedures/wizard/waitForLoadStepChainMessageStep.py
        """
        self._next_step = step
        return step

    # Set the next step which will be ran when this step is finished.
    # Only set during setup of the procedure.
    def setNextStep(self, step):
        """ Set the next step which will be ran when this step is finished.

        Only set during setup of the procedure.
        """
        assert self._step_state is StepAndProcedureState.Setup, "the step is already running, the nextStep cannot be set anymore at this point."
        assert step is None or isinstance(step, ProcedureStep), "step is not of an accepted type"

        self._next_step = step
        return step

    def getKey(self):
        return self._key

    def getMetaData(self):
        return {"EMPTY_ORIGIN": "ProcedureStep"}
