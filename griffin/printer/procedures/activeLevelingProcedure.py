import logging

from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.print.switchActiveHotendStep import SwitchActiveHotendStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.queueRawGCodeWaitStep import QueueRawGCodeWaitStep
from griffin.printer.procedures.steps.homeBedStep import HomeBedStep
from griffin.printer.procedures.steps.homeHeadStep import HomeHeadStep
from griffin.printer.procedures.temperature.setBedTemperatureStep import SetBedTemperatureStep

log = logging.getLogger(__name__.split(".")[-1])


# The procedure which handles selecting "Active leveling" from the "build plate" menu in the "system" menu.
# This does some setup and calls the ALIGN_Z_AXIS procedure, which does the actual measurements and is shared with the printing procedure.
class ActiveLevelingProcedure(Procedure):
    def __init__(self, key, wizard_type="change"):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)
        self._hotend_nr = 0
        self._wizard_type = wizard_type

    def setup(self, args):
        self._hotend_nr = int(args.get("hotend_nr", 0))

        # Generate all the steps for this wizard procedure.
        steps = [
            WaitForQueueToBeEmptyStep("WAITING"),
            QueueRawGCodeWaitStep("RESET_OFFSET_1", "M218 T1 X0 Y0 Z0"),
            QueueRawGCodeWaitStep("RESET_OFFSET_0", "M218 T0 X0 Y0 Z0"),
            HomeBedStep("HOMING"),
            HomeHeadStep("HOMING"),
            SwitchActiveHotendStep("RESET_ACTIVE_HOTEND", "reset"),
            OtherProcedureStep("ALIGNING", "ALIGN_Z_AXIS", {"force_probing": True, "end_early": True}),
            SetBedTemperatureStep("SET_BED_OFF", 0),
            QueueRawGCodeWaitStep("RESET_OFFSET_1", "M218 T1 X0 Y0 Z0"),
            QueueRawGCodeWaitStep("RESET_OFFSET_0", "M218 T0 X0 Y0 Z0"),
            HomeBedStep("HOMING"), # home here since the bed may be hot for DEKRA safty
            WaitForQueueToBeEmptyStep("WAITING"),
            OtherProcedureStep("BEDLEVEL_CORRECTION_MODE", "SET_BED_LEVEL_CORRECTION_MODE_WIZARD"),
            OtherProcedureStep("FINISHING_CHAIN", "GENERIC_WIZARD_FINISHING_PROCEDURE")
        ]
        abort_steps = [
            HomeBedStep("HOMING"),
            OtherProcedureStep("FINISHING_CHAIN", "GENERIC_WIZARD_FINISHING_PROCEDURE")
        ]

        for n in range(0, len(steps) - 1):
            steps[n].setNextStep(steps[n + 1])
        for n in range(0, len(abort_steps) - 1):
            abort_steps[n].setNextStep(abort_steps[n + 1])

        self.setFirstStep(steps[0])
        # Set the abort step to the last bed homing, as the GENERIC_WIZARD_FINISHING_PROCEDURE does not home the bed.
        self.setAbortStep(abort_steps[0])

        return True
