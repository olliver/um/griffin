from .procedureStep import ProcedureStep


# This class exists to make sure we always have a step to provide in a procedure.
# As a procedure always has to contain at least a single step.
# This is used in the instance where a procedure discovers it has nothing to do during the setup() of the procedure
# and it is used to create the "IGNORE" procedure which does nothing.
# The "IGNORE" PROCEDURE is in turn used to intercept the G28 and map G28 to nothing so homing does not re-calibrate the positions.
class DummyStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        pass
