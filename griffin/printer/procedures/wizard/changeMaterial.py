import logging

from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.steps.homeHeadStep import HomeHeadStep
from griffin.printer.procedures.steps.homeBedStep import HomeBedStep
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.print.switchActiveHotendStep import SwitchActiveHotendStep
from griffin.printer.procedures.steps.gotoLocationStep import GotoLocationStep
from griffin.printer.procedures.wizard.waitForUserToSelectMaterialStep import WaitForUserToSelectMaterialStep
from griffin.printer.procedures.wizard.moveMaterialWaitMessageStep import MoveMaterialWaitMessageStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.wizard.waitMessageStep import WaitMessageStep
from griffin.printer.procedures.wizard.unloadMaterialStep import UnloadMaterialStep
from griffin.printer.procedures.temperature.setHotendTemperatureStep import SetHotendTemperatureStep
from griffin.printer.procedures.queueRawGCodeWaitStep import QueueRawGCodeWaitStep

log = logging.getLogger(__name__.split(".")[-1])


# The wizard procedure which handles the change material option for the end user.
class ChangeMaterial(Procedure):
    EXTRA_RETRACT_LENGTH = 40
    INSERT_SAFETY_LENGTH = 40
    SKIP_START = "skip_start"
    SKIP_END = "skip_end"
    EXPECTED_MATERIAL = "expected_material"

    def __init__(self, key, wizard_type="change"):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)
        self._hotend_nr = 0
        self._wizard_type = wizard_type
        self._bowden_tube_length = Controller.getInstance().getPropertyValue("bowden_tube_length")
        self._skip_start = False
        self._skip_end = False
        self._expected_material = None

    def setup(self, args):
        self._hotend_nr = int(args.get("hotend_nr", 0))
        self._skip_start = bool(args.get(self.SKIP_START, False))
        self._skip_end = bool(args.get(self.SKIP_END, False))
        if self._wizard_type == "select":
            self._skip_start = True
            self._skip_end   = True
        self._expected_material = args.get(self.EXPECTED_MATERIAL, None)

        ## the expected material is used for display in the welcome setup but since it has to be changed dynamically, the expected material can be set to the wizard running this change material,
        # aka, the welcome setup wizard. The welcome setup stores the type of nozzle that the user has elected to use and from that an expected material can be derived,
        if isinstance(self._expected_material, Procedure):
            meta = self._expected_material.getMetaData()
            if meta["next_expected_hotend_id"] == "BB 0.4":
                self._expected_material = "PVA"
            else:
                self._expected_material = None

        if self._expected_material is not None:
            log.info("expected material: %s", self._expected_material)

        controller = Controller.getInstance()
        # If the hotend is not there, then materials should not be loaded/unloaded (changed) nor moved since all of that requires a heating up the hotend
        if not controller.getPrintHeadController().getHotendCartridgeManager().isPresent(self._hotend_nr):
            return False

        # Generate all the steps for this wizard procedure.
        steps = []
        if not self._skip_start:
            steps += self._generatePreperationSteps()
        else:
            steps.append(QueueRawGCodeWaitStep("SELECT_HOTEND", "T{hotend_nr}".format(hotend_nr=self._hotend_nr)))

        if self._wizard_type == "change":
            steps += self._generateUnloadMaterialSteps()
            steps += self._generateLoadMaterialSteps()
        elif self._wizard_type == "select":
            steps += self._generateSelectMaterialSteps()
        elif self._wizard_type == "unload":
            steps += self._generateUnloadMaterialSteps()
        elif self._wizard_type == "load":
            steps += self._generateLoadMaterialSteps()
        else:
            log.error("Unknown change material wizard type: %s", self._wizard_type)

        if not self._skip_end:
            steps += self._generateFinalizationSteps()
            self.setAbortStep(steps[-1])

        for n in range(0, len(steps) - 1):
            steps[n].setNextStep(steps[n + 1])
        self.setFirstStep(steps[0])

        return True

    def _generatePreperationSteps(self):
        return [
            # Prepare the printer for material change. The printer state is unknown, so home, and move the head switch into a known position
            HomeHeadStep("HOME_HEAD"),
            HomeBedStep("HOME_BED"),
            SwitchActiveHotendStep("RESET_ACTIVE_HOTEND", "reset"),
            OtherProcedureStep("SWITCH_TO_HOTEND", "SWITCH_ACTIVE_HOTEND", {"target": self._hotend_nr}),
            GotoLocationStep("MOVE_HEAD_TO_MAINTENANCE_POS", "CLEAN_NOZZLES_MAINTENANCE"),
        ]

    def _generateUnloadMaterialSteps(self):
        primed = Controller.getInstance().getPrintHeadController().getHotendSlot(self._hotend_nr).getPropertyValue("is_primed")
        steps = []
        if primed:
            # Heat up the hotend to printing temperature, so we can extract the material properly.
            steps.append(OtherProcedureStep("HEATUP_WAIT", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr}))
            # Move the material backwards, first to a short pull, and then the long move.
            steps.append(OtherProcedureStep("REVERSE_MATERIAL_A", "MOVE_MATERIAL", {"hotend_nr": self._hotend_nr, "target": -10, "speed": 10}))

        steps += [
            OtherProcedureStep("REVERSE_MATERIAL_B", "MOVE_MATERIAL", {"hotend_nr": self._hotend_nr, "target": -self._bowden_tube_length - self.EXTRA_RETRACT_LENGTH}),
            WaitForQueueToBeEmptyStep("WAIT_REVERSE_MATERIAL_FINISHED"),
            # Wait for the user to remove the old material from the machine.
            WaitMessageStep("WAIT_FOR_USER_TO_REMOVE_MATERIAL", "DONE"),
            UnloadMaterialStep("UNLOADING_MATERIAL", self._hotend_nr),
            SetHotendTemperatureStep("START_COOLDOWN_HOTEND", self._hotend_nr, 0)

        ]
        return steps

    def _generateSelectMaterialSteps(self):
        return [
            # TODO: remove after EM-403
            # This step was introduced as a result of EM-129 but should be removed after EM-403 implementation.
            # This step is a clumsy implementation for changing the current selected material type. In fact,
            # what we want is a simple menu routine instead of calling this wizard. Problem is that the material
            # property is now saved by the Wizard. A cleaner implementation can be created when we have material
            # properties and then the Wizard can wait for the next step by being triggered on a material change.

            # explain which nozzle material is supposed to be inserted into in selection step because we're already using rfid detection
            # Have the user select the material that he will insert.
            WaitForUserToSelectMaterialStep("USER_SELECT_MATERIAL", self._hotend_nr, only_select_material=True)
        ]

    def _generateLoadMaterialSteps(self):
        steps = []

        # Start pre-heating the nozzle to speed-up the loading. A safe value of 100C is choosen that won't degrade the materials.
        steps.append(OtherProcedureStep("PRE_HEAT_NOZZLE", "SET_HOTEND_TEMPERATURE", {"hotend_nr": self._hotend_nr, "target": 100}))

        # Special actions when loading Material 1
        if self._hotend_nr == 0:
            # Wait for the user to install the material with installed material guide
            steps.append(WaitMessageStep("WAIT_FOR_USER_TO_PLACE_MATERIAL_WITH_GUIDE", "DONE"))
        else:
            # Wait for the user to install the material
            steps.append(WaitMessageStep("WAIT_FOR_USER_TO_PLACE_MATERIAL", "DONE"))


        # Have the user select the material that he will insert. this step will also wait for the insertion! and have the material nr listed so people will know which material they are changing
        steps.append(WaitForUserToSelectMaterialStep("USER_SELECT_MATERIAL", self._hotend_nr))
        # Heatup to the new temperature for this material.
        steps.append(OtherProcedureStep("HEATUP_TO_NEW_MATERIAL_WAIT", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr}))
        # then forward material with a fast move.
        steps.append(OtherProcedureStep("FORWARD_MATERIAL", "MOVE_MATERIAL", {"hotend_nr": self._hotend_nr, "target": self._bowden_tube_length - self.INSERT_SAFETY_LENGTH}))
        steps.append(WaitForQueueToBeEmptyStep("WAIT_FORWARD_MATERIAL_FINISHED"))
        # Wait till the user reports that the material is coming out of the hotend. At the same time, keep feeding forwards.
        steps.append(MoveMaterialWaitMessageStep("WAIT_FOR_MATERIAL_TO_COME_OUT_OF_HOTEND", self._hotend_nr, "forward"))

        if not self._skip_end:
            # Deprime a little to stop oozing.
            steps.append(OtherProcedureStep("DEPRIME", "DEPRIME", {"hotend_nr": self._hotend_nr}))
        else:
            steps.append(OtherProcedureStep("START_COOLDOWN", "SET_HOTEND_TEMPERATURE", {"hotend_nr": self._hotend_nr, "target": "standby_temperature"}))

        return steps

    def _generateFinalizationSteps(self):
        return [
            # Finalization step. Also used as abort step.
            OtherProcedureStep("FINISHING", "GENERIC_WIZARD_FINISHING_PROCEDURE"),
        ]

    # Override the getMetaData function to add our selected hotend nr.
    # This so consumers of the dbus-API know which hotend is currently being accessed with this procedure.
    def getMetaData(self):
        result = super().getMetaData()
        if result is None:
            result = {}
        result["hotend_nr"] = self._hotend_nr
        result["display_hotend_nr"] = self._hotend_nr + 1
        if isinstance(self._expected_material, str) or self._expected_material is None:
            result["expected_material"] = str(self._expected_material)

        return result
