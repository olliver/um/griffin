from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

import logging
import math

log = logging.getLogger(__name__.split(".")[-1])


class MoveMaterialWaitMessageStep(ProcedureStep):
    ##< The default diameter of the material (see EM-952) in mm
    __DEFAULT_MATERIAL_DIAMETER = 2.85
    ##< The expected speed for the final step in mm3/s
    __DEFAULT_SPEED = 6.0

    def __init__(self, key, hotend = 0, direction = "forward"):
        super().__init__(key)
        self._target_hotend = int(hotend)
        self._dir = direction
        # Movement speed converted from 6 mm3/s to mm/s: calculate the surface of the material using pi * r^2 formula.
        # The default diameter is 2.85 hence the half. Also, this hardcoded should not be changed in the future to use the actual material profile
        # TODO: EM-952
        self.__target_speed = self.__DEFAULT_SPEED / (math.pi * ((self.__DEFAULT_MATERIAL_DIAMETER / 2.0) ** 2))

    def run(self):
        self._done_received = False
        c = Controller.getInstance()
        original_active_hotend = c.getActiveHotend()
        c.queueRawGCode("M302 S0")
        c.queueRawGCode("T%d" % self._target_hotend)
        c.setActiveHotend(self._target_hotend)

        # this is essentially priming, so add tracking of the primed state.
        c.getPrintHeadController().getHotendSlot(self._target_hotend).setPropertyValue("is_primed", True)
        while not self._done_received and not self._abort:
            c.queueRawGCode("G92 E0")

            if self._dir == "reverse" or self._dir == "backwards":
                c.queueRawGCode("G1 E-0.3 F%f" % (self.__target_speed * 60))
            else:
                c.queueRawGCode("G1 E0.3 F%f" % (self.__target_speed * 60))
        # Z or E moves do not need to be speed compensated.

        # Abort again to remove possible remaining queue.
        c.abortQueue()
        c.queueRawGCode("G92 E0")
        c.queueRawGCode("M302 S%d" % c.getPropertyValue("default_minimal_extrusion_temperature"))    # safety back on

        # re-set extruded length after the user acknowledges material is coming out of the nozzle.
        # Use this step only to prime the nozzle after a (re)loading of material,
        # or to feed material into the feeder.
        c.setRetractedLength(0)
        c.setActiveHotend(original_active_hotend)


    def receiveMessage(self, message):
        """ Handle the "DONE" message. Pass rest to super() """

        log.info("Message: %s", message)

        if message == "DONE":
            # Abort queue independend of loop.
            c = Controller.getInstance()
            c.abortQueue()
            self._done_received = True
        else:
            # Pass the message
            super().receiveMessage(message)
