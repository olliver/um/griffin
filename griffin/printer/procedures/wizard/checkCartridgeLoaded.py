from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultCode, FaultHandler

# Checks for the specified cartridge number to be present.
# When not present, the specified step_chain in given wizard is called.
class CheckCartridgeLoaded(ProcedureStep):
    def __init__(self, key, hotend_nr):
        super().__init__(key)
        self._target_hotend = int(hotend_nr)

    def run(self):
        controller = Controller.getInstance()

        if not controller.getPrintHeadController().getHotendCartridgeManager().isPresent(self._target_hotend):
            FaultHandler.warning("missing printcore %d!" % self._target_hotend, FaultCode.CARTRIDGE_GONE, data={"display_hotend_nr":self._target_hotend+1})

        assert controller.getPrintHeadController().getHotendCartridgeManager().isPresent(self._target_hotend)
        # cause exception and therefore abort if assert is not true.
