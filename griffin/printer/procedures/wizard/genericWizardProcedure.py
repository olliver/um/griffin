import importlib
import copy
import re
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.wizard.nextWizardStepChain import NextWizardStepChain
import logging
from griffin.timer import Timer

log = logging.getLogger(__name__.split(".")[-1])


# The genericWizard procedure is a procedure that can be build up from the machine configuration json file.
# The steps for this procedure will be dynamically loaded and filled with the given parameters.
# Unlike the generic procedure which just executes a single flow of steps the genericWizard procedure has step chains which it can load.
# From any point in the wizard you can switch to a different step chain
# using either NextWizardStepChain or WaitForLoadStepChainMessageStepgenericWizard,
# this allows a wizard to skip certain steps.
# Chain A can end with a WaitForLoadStepChainMessageStepgenericWizard step and continue with chain B or C after which it can finish with D.
# Step chains are constructed as they are loaded, allowing you to use parameters to change the behaviour of the steps.
# Any parameters passed to the procedure's setup function (or through the LoadStepChainMessage) can be accessed if a step has a text parameter
# containing "{key}" then it will be replaced with the value in the dictionary of parameters matching the key (or self._parameters[key])
# The wizard can be looped if in a stepchain you use a NextWizardStepChain or WaitForLoadStepChainMessageStepgenericWizard
# to load the same step you can keep doing so ad infinitum.
class GenericWizardProcedure(Procedure):

    def __init__(self, key, **args):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)

        self._steps_from_json = args
        self._parameters = {"self": self}
        self._start_wizard_step = NextWizardStepChain("START", "START_WIZARD", self)
        self.setFirstStep(self._start_wizard_step)
        self._meta_data = {}

    def setup(self, args):
        from griffin.printer.controller import Controller
        self._parameters.update(args)
        if "START" in args:
            self._start_wizard_step = NextWizardStepChain("START", args["START"], self)
            self.setFirstStep(self._start_wizard_step)

        # Setup the steps to be executed on timeout abort.
        self.setAbortStep(self.getSteps("FINISHING_CHAIN"))

        return True

    def loadStepChain(self, key):
        log.info("loadStepChain(%s)", key)
        if key not in self._steps_from_json:
            log.error("unknown step used in loadStepChain key %s", key)
            return None

        steps = list()

        for step_data in self._steps_from_json[key]:
            step_key = step_data[0]
            procedure_name = step_data[1]
            procedure_package, procedure_class = procedure_name.rsplit(".", 1)

            module = importlib.import_module("griffin.printer.procedures.%s" % procedure_package)
            constructor = getattr(module, procedure_class)

            # _deepFindParameters replaces variables so deepcopy to not overwrite data read from json
            args2 = self._deepFindParameters(copy.deepcopy(step_data[2:]))

            try:
                steps.append(constructor(step_key, *args2))
            except:
                log.exception("Failed to construct: %s [%s] %s", key, procedure_name, args2)


            if len(steps) > 1:
                steps[-2].setNextStep(steps[-1])

        return steps

    def _deepFindParameters(self, item):
        if isinstance(item, dict):
            for key in item:
                item[key] = self._deepFindParameters(item[key])
            return item
        elif isinstance(item, list):
            i = 0
            for list_entry in item:
                item[i] = self._deepFindParameters(list_entry)
                i += 1
            return item
        elif isinstance(item, str):
            if re.match("^{([^}]+)}$", item):
                return self._parameters[item[1:-1]]
            else:
                return item
        else:
            return item

    def getSteps(self, step_name):
        steps = self.loadStepChain(step_name)
        try:
            if len(steps) > 0:
                return steps[0]
            else:
                return None
        except:
            return None

    # Override the getMetaData, to include our configuration parameters in the meta-data.
    def getMetaData(self):
        result = self._parameters.copy()
        # Remove "self" from the result, as this is a reference to the object. Which is something that dbus cannot serialize.
        if "self" in result:
            del result["self"]
        result.update(super().getMetaData())
        result.update(self._meta_data)
        return result


    ## @brief Allow to set meta data (from steps for example) to the procedure.
    #  @param key The identification of the data
    #  @param value The value of the data to save
    def setMetaData(self, key, value):
        self._meta_data[key] = value

    ## @brief Allow to remove meta data (from steps for example) from the procedure.
    #  @param key The identification of the data to be removed
    def unsetMetaData(self, key):
        self._meta_data.pop(key)
