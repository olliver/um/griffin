from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.wizard.moveMaterialStep import MoveMaterialStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.steps.setPropertyStep import SetPropertyStep
import math
import time
import logging

log = logging.getLogger(__name__.split(".")[-1])


class MoveMaterialProcedure(Procedure):
    # values roughly chosen to also work with the (old/first) drybox.
    __LONG_MOVE_ACCELERATION = 10 # mm/s^2
    __LONG_MOVE_SPEED = 85 # mm/s
    __LONG_MOVE_MINIMUM_LENGTH = 100 # mm
    __LONG_MOVE_MOTOR_POWER_PERCENTAGE = 66 # 66% of the motor power, this matches the Ultimaker2+ which has the same feeder.

    def __init__(self, key, hotend = 0, speed = 25.0, length = 1.0):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._step = MoveMaterialStep("MOVING_MATERIAL")
        self.setFirstStep(self._step)
        self._target_speed = speed
        self._target_length = length
        self._target_hotend = int(hotend)
        self._time = None
        self._start_time = None

#       float retract_length=4.5, retract_feedrate=25*60, retract_zlift=0.8
#       float retract_recover_length=0, retract_recover_feedrate=25*60
    def setup(self, args):
        c = Controller.getInstance()
        if "hotend_nr" in args:
            self._target_hotend = int(args["hotend_nr"])
        if "target" in args:
            self._target_length = float(args["target"])
            if abs(self._target_length) < self.__LONG_MOVE_MINIMUM_LENGTH:
                self._target_speed = abs(self._target_length) / 0.02    # x mm in 20ms -> mm/s
                self.setFirstStep(self._step).setNextStep(None)
            else:
                acceleration = self.__LONG_MOVE_ACCELERATION
                self._target_speed = self.__LONG_MOVE_SPEED

                hotend_object = c.getPrintHeadController().getHotendSlot(self._target_hotend)

                old_mfr = c.getPropertyValue("max_speed_e")
                old_acc = c.getPropertyValue("acceleration_e")
                old_cur = c.getPropertyValue("motor_current_e")
                old_Ke = hotend_object.getPropertyValue("pid_Ke")
                jerk = c.getPropertyValue("jerk_e")
                pre_move_steps = [
                    SetPropertyStep("SET_MAX_SPEED_E", "printer", "max_speed_e", self._target_speed),
                    SetPropertyStep("SET_ACCELERATION_E", "printer", "acceleration_e", acceleration),
                    SetPropertyStep("SET_MOTOR_CURRENT_E", "printer", "motor_current_e", old_cur * self.__LONG_MOVE_MOTOR_POWER_PERCENTAGE / 100),
                    SetPropertyStep("SET_PID_KE", hotend_object.getPropertyContainer(), "pid_Ke", 0.0) # Set the Ke value to 0 in the PID controller, so we don't switch heaters on/off due to the long extrusion.
                ]
                # Actual move is inserted in between here.
                post_move_steps = [
                    WaitForQueueToBeEmptyStep("POST_MOVE_WAIT"),
                    SetPropertyStep("RESET_MAX_SPEED_E", "printer", "max_speed_e", old_mfr),
                    SetPropertyStep("RESET_ACCELERATION_E", "printer", "acceleration_e", old_acc),
                    SetPropertyStep("RESET_MOTOR_CURRENT_E", "printer", "motor_current_e", old_cur),
                    SetPropertyStep("RESET_PID_KE", hotend_object.getPropertyContainer(), "pid_Ke", old_Ke),
                    WaitForQueueToBeEmptyStep("WAITING")
                ]

                self.setFirstStep(pre_move_steps[0])
                for n in range(1, len(pre_move_steps)):
                    pre_move_steps[n - 1].setNextStep(pre_move_steps[n])
                pre_move_steps[-1].setNextStep(self._step)
                self._step.setNextStep(post_move_steps[0])
                for n in range(1, len(post_move_steps)):
                    post_move_steps[n - 1].setNextStep(post_move_steps[n])

                # These time estimations are loosely based on those in the curaEngine.
                acceleration_distance = self.__estimate_acceleration_distance(jerk, self._target_speed, acceleration)

                if acceleration_distance * 2 < abs(self._target_length):
                    # if acceleration + deceleration does not fit inside the target length half is acceleration the other half is deceleration.
                    acceleration_distance = abs(self._target_length) / 2

                plateau_distance = abs(self._target_length) - 2 * acceleration_distance

                time  = 2 * self.__acceleration_time_from_distance(jerk, acceleration_distance, acceleration)
                time += plateau_distance / self._target_speed
                # can be omitted since it is the same distance/acceleration twice
                # time += self.__acceleration_time_from_distance(jerk, acceleration_distance, acceleration)
                self._time = time
                log.info("long move material time is estimated to be %s", time)
                pre_move_steps[-1].stepFinished.connect(self.__stepStarted)
        else:
            self._target_speed = 0
            self._target_length = 0
        if "speed" in args:
            self._target_speed = float(args["speed"])

        self._step.setTarget(self._target_speed, self._target_length, self._target_hotend)

        return True

    def __stepStarted(self):
        self._start_time = time.monotonic()

    def getMetaData(self):
        progress = 0.0
        if self._start_time is not None and self._time is not None:
            elapsed = time.monotonic() - self._start_time
            progress = min(1.0, elapsed / self._time)
            log.debug("progress %s raw %s elapsed time %s, time left %s", progress, elapsed / self._time, elapsed, self._time - elapsed )

        return {"progress": progress}

    ## calculate the distance the acceleration will take
    # @param initial_rate the speed when starting the movement (or the jerk)
    # @param target_rate The intended maximal/nominal speed.
    # @param acceleration The acceleration in mm/s^2
    def __estimate_acceleration_distance(self, initial_rate, target_rate, acceleration):
        return (target_rate ** 2 - initial_rate ** 2) / (2.0 * acceleration)

    ## calculate the time the acceleration will take
    # @param initial_rate the speed when starting the movement (or the jerk)
    # @param distance The distance the acceleration is constant.
    # @param acceleration The acceleration in mm/s^2
    def __acceleration_time_from_distance(self, initial_feedrate, distance, acceleration):
        discriminant = initial_feedrate ** 2 - 2 * acceleration * -distance
        # If discriminant is negative, we're moving in the wrong direction.
        # Making the discriminant 0 then gives the extremum of the parabola instead of the intersection.
        discriminant = max(0.0, discriminant)
        return (-initial_feedrate + math.sqrt(discriminant)) / acceleration

