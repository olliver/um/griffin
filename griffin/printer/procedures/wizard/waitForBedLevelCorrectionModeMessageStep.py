from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.procedures.pre_and_post_print.auto_bed_level_adjust.alignZAxisProcedure import AlignZAxisProcedure
import time
import logging

log = logging.getLogger(__name__.split(".")[-1])


class WaitForBedLevelCorrectionModeMessageStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)
        self._finished = False

    # Do nothing when this step is started. Just wait for the proper message to be received.
    def run(self):
        while not self._finished and not self._abort:
            time.sleep(0.5)

    def receiveMessage(self, message):
        log.info("Message: %s", message)
        c = Controller.getInstance()
        levelling_modes = AlignZAxisProcedure.ALL_BED_LEVEL_CORRECTION_MODES
        if message in levelling_modes:
            c.setPreference("BED_LEVEL_CORRECTION_MODE", message)
            log.info("BED_LEVEL_CORRECTION_MODE set to \"%s\"", message)
        else:
            raise ValueError("message format wrong, message: %s" % message)

        self._finished = True
