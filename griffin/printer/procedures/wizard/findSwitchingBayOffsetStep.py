import re
import logging
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
log = logging.getLogger(__name__.split(".")[-1])


# the 0.5 is extra move into the slider block creating some pressure to keep the lifting switch in the switching bay.
# the rest is moving the model I got the move coordinates from to the position the switching bay was in my test-machine.
def _x(v):
    return v + 0.5 - (223.821600 - 119.7) # These magic values somehow relate to the switching bay model in SwitchActiveHotendStep


# here no extra offset is used/applied.
def _y(v):
    build_volume_y = Controller.getInstance().getPropertyValue("build_volume_y")
    return build_volume_y - v + 0 - (219.333910 - 122.6) # These magic values somehow relate to the switching bay model in SwitchActiveHotendStep


class FindSwitchingBayOffsetStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def findOffset(self, axis):
        c = Controller.getInstance()
        data = c.sendRawGCode("G28 %s0" % axis, wait=True)
        m = re.match(r"home_distance = (\d+\.\d+)", data)
        if not m:
            return None
        if axis == "X":
            return _x(float(m.group(1)))
        if axis == "Y":
            return _y(float(m.group(1)))

    def run(self):
        c = Controller.getInstance()
        c.setActiveHotend(0)
        offX = self.findOffset("X")
        offY = self.findOffset("Y")
        if offX is None or offY is None:
            raise ValueError("could not parse one of the found offsets!")
        c.setPreference("SWITCH_OFFSET", [offX, offY])
