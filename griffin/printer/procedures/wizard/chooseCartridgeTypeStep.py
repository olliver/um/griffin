import logging
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.thread import Event

log = logging.getLogger(__name__.split(".")[-1])


## This class is meant to receive from the GUI the cartridge the user chose to use during the welcome setup.
# This is then stored and used by later steps to indicate to the user which cartridge to inset.
class ChooseCartridgeTypeStep(ProcedureStep):
    def __init__(self, key, wizard, *accepted_list):
        super().__init__(key)
        self._wizard = wizard
        self._accepted_list = accepted_list
        self._event = Event()

    def run(self):
        self._event.clear()
        self._event.wait()

    def receiveMessage(self, message):
        if message in self._accepted_list:
            self._wizard.setMetaData("next_expected_hotend_id", message)
            self._event.set()
