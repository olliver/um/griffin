from griffin.printer.controller import Controller
from griffin.printer.procedures.procedureStep import ProcedureStep

import logging

log = logging.getLogger(__name__.split(".")[-1])

# This step checks the nozzle combination to have valid data.
class CheckNozzleDataStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()
        c.getPrintHeadController().getHotendCartridgeManager().checkNozzleCombi()
