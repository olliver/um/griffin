from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class UnloadMaterialStep(ProcedureStep):
    def __init__(self, key, hotend):
        super().__init__(key)

        self._target_hotend = int(hotend)

    # Set that there is no material selected anymore in the material manager.
    # This step finishes almost instantly.
    def run(self):
        Controller.getInstance().getPrintHeadController().getHotendSlot(self._target_hotend).setPropertyValue("material_guid", "")
