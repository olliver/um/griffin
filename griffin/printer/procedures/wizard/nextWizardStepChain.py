from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.procedures.wizard.doOnceStep import DoOnceStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


class NextWizardStepChain(ProcedureStep):
    def __init__(self, key, next_step_chain, wizard):
        super().__init__(key)

        self._next_step_chain = next_step_chain
        self._wizard = wizard

    def run(self):
        # getSteps() might return None in that case it's seen as abort!
        step = DoOnceStep(self._wizard.getSteps(self._next_step_chain))
        self._protectedSetNextStepObsolete(step)

        if step is None:
            log.warning("selected step chain '%s' was not found!" % self._next_step_chain)
            raise ValueError("step is None")
        else:
            log.info("wizard %s continuing with next step chain '%s'!" % (self._wizard.getKey(), self._next_step_chain))
