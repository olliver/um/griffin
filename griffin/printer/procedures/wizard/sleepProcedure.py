from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.wizard.sleepStep import SleepStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


## This class allows the gcode G4 command to be executed inside griffin instead of blocking communication by waiting in marlin.
# if you need a wait/sleep inside a procedure, use the sleepStep instead.
class SleepProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._wait = WaitForQueueToBeEmptyStep("WAIT_FOR_EMPTY_QUEUE_BEFORE_SLEEP")
        self._step = SleepStep("SLEEPING")
        self.setFirstStep(self._wait)
        self._wait.setNextStep(self._step)

    def setup(self, args):
        time = 0.0
        if "seconds" in args and args["seconds"] is not None:
            time = float(args["seconds"])
        if "milliseconds" in args and args["milliseconds"] is not None:
            time += (float(args["milliseconds"]) / 1000)

        self._step.setTime(time)

        return True
