import re
import time
import logging

from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.translationXYNozzleOffset import TranslationXYNozzleOffset

log = logging.getLogger(__name__.split(".")[-1])


class AlignXyStep(ProcedureStep):
    def __init__(self, key, axis, value = None):
        super().__init__(key)
        self._axis = axis.upper()
        self._value = value
        self._finished = False

    def run(self):
        # set the value so the offset can be cleared so a calibration print
        if self._value is not None:
            self.setAxis(self._value)
            return
        while not self._finished and not self._abort:
            time.sleep(0.5)

    def receiveMessage(self, message):
        log.info("message: %s", message)
        m = re.match(r"^USER_INPUT\s+(-?\d+)$", message)
        if m:
            offset_index = str(m.group(1))
            self.setAxis(TranslationXYNozzleOffset.getOffset(**{self._axis: offset_index}))
            self._finished = True

    def setAxis(self, value):
        c = Controller.getInstance()

        c.getPrintHeadController().getHotendSlot(1).setPropertyValue(self._axis.lower() + "_offset", value)
        log.info("new %s value %s\n", self._axis, str(value))
