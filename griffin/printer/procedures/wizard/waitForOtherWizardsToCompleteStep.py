from griffin.printer.procedures.wizard.genericWizardProcedure import GenericWizardProcedure
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import time


class WaitForOtherWizardsToCompleteStep(ProcedureStep):

    def __init__(self, key):
        super().__init__(key)

    def run(self):
        while len([p for p in Controller.getInstance().getAllProcedures() if p.isActive() and isinstance(p, GenericWizardProcedure)]) > 1 and not self._abort:
            time.sleep(0.5)
