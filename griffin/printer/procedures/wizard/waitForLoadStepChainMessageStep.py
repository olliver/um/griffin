import time
import json
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.procedures.wizard.doOnceStep import DoOnceStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


# This class waits for a message to load a new step chain, think of it as a decision moment.
# chain A can end with this step and continue with chain B or C after which it can finish with D.
# \param message this must contain a step chain that exists in the self._wizard!
#
# Potentially the message may contain a ':' after which a json dictionary is expected, this json data is passed to the wizard's
# setup function so it can be used to, for example, set the hotend_nr so a specific hotend is used.
class WaitForLoadStepChainMessageStep(ProcedureStep):
    def __init__(self, key, wizard):
        super().__init__(key)
        self._wizard = wizard
        self._finished = False

    # Do nothing when this step is started. Just wait for the proper message to be received.
    def run(self):
        while not self._finished and not self._abort:
            time.sleep(0.5)

        if not self._abort:
            if self._step is None:
                raise ValueError("Step was not set")

            once_step = DoOnceStep(self._step)
            self._protectedSetNextStepObsolete(once_step)

    def receiveMessage(self, message):
        log.info("Message: %s", message)
        if ":" in message:
            msg_parts = message.split(":")
            message = msg_parts[0]
            parsed_json = json.loads(":".join(msg_parts[1:]))
            log.info("calling %s.setup(%s)", self._wizard.getKey(), str(parsed_json))
            self._wizard.setup(parsed_json)

        self._step = self._wizard.getSteps(message)
        self._finished = True
