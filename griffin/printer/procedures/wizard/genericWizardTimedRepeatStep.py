from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.procedures.wizard.doOnceStep import DoOnceStep
import logging

import time

log = logging.getLogger(__name__.split(".")[-1])


## This class implements a simple step that will repeat until a certain time interval is left
class GenericWizardTimedRepeatStep(ProcedureStep):
    ## Define the key to use
    __KEY_END_TIME = "end_time"

    def __init__(self, key, duration, repeat_step, finishing_step, wizard):
        super().__init__(key)

        self.__next_step = repeat_step
        self.__finishing_step = finishing_step
        self.__wizard = wizard
        self.__end_time = self.__wizard.getMetaData().get(self.__KEY_END_TIME, time.monotonic() + duration)
        log.debug("End time set to %r", self.__end_time)
        self.__wizard.setMetaData(self.__KEY_END_TIME, self.__end_time)

    def run(self):
        now = time.monotonic()
        log.debug("Current time is %r", now)
        if now >= self.__end_time:
            self.__next_step  = self.__finishing_step
            # Remove it so it can be set again (after reset)
            self.__wizard.unsetMetaData(self.__KEY_END_TIME)

        step = DoOnceStep(self.__wizard.getSteps(self.__next_step))
        self._protectedSetNextStepObsolete(step)