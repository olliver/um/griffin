from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class MoveMaterialStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)
        self._target_length = None
        self._target_speed = None

    def setTarget(self, speed, length, hotend):
        self._target_length = length
        self._target_speed = speed
        self._target_hotend = int(hotend)

    def run(self):
        assert(self._target_length is not None)

        c = Controller.getInstance()
        hotend = c.getPrintHeadController().getHotendSlot(self._target_hotend)
        flow = 1 / (c.getPropertyValue("extrusion_amount_modifier") / 100)
        c.waitForQueueToBeEmpty()

        original_active_hotend = c.getActiveHotend()
        if original_active_hotend is None:
            original_active_hotend = 0

        c.queueRawGCode("M302 S0")  # even move when cold
        c.queueRawGCode("T%d" % self._target_hotend)
        c.setActiveHotend(self._target_hotend)
        c.queueRawGCode("G92 E0")
        gcode = "G1 E%f F%f" % (self._target_length * flow, self._target_speed * 60)
        # Z or E moves do not need to be speed compensated.
        c.queueRawGCode(gcode)
        c.queueRawGCode("M302 S%d" % c.getPropertyValue("default_minimal_extrusion_temperature"))    # safety back on
        c.queueRawGCode("T%d" % original_active_hotend)
        c.setActiveHotend(original_active_hotend)
        c.waitForQueueToBeEmpty(max_planned_motions=3)  # reduce the number of planned motions to improve responsiveness
