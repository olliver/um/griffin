from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.thread import Event

import logging

log = logging.getLogger(__name__.split(".")[-1])


class WaitMessageStep(ProcedureStep):
    def __init__(self, key, message):
        super().__init__(key)

        self._message = message
        self._message_received_event = Event()

    # Do nothing when this step is started. Just wait for the proper message to be received.
    def run(self):
        self._message_received_event.clear()
        while not self._message_received_event.is_set() and not self._abort:
            self._message_received_event.wait(self.TIME_OUT_WAIT_FINISHED_EVENT)

    def receiveMessage(self, message):

        log.info("Message: %s", message)

        """ Handle the "self._message" message. Pass rest to super() """
        if message == self._message:
            self._message_received_event.set()
        else:
            super().receiveMessage(message)
