from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


## This class lets you set a preference such as whether the FACTORY_SETUP_WIZARD was completed as one of the last steps of such a procedure.
class SetPreferenceStep(ProcedureStep):
    def __init__(self, key, preference, value):
        super().__init__(key)
        self.__preference = preference
        self.__value = value

    def run(self):
        Controller.getInstance().setPreference(self.__preference, self.__value)
