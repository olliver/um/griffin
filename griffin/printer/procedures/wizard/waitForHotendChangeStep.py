import time
import re
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.procedures.procedure import Procedure

from griffin.printer.controller import Controller
import logging

log = logging.getLogger(__name__.split(".")[-1])


## This class can wait for any hotend to be inserted/present or removed, or a specific type of hotend to be inserted/present.
class WaitForHotendChangeStep(ProcedureStep):
    def __init__(self, key, hotend_nr, action__hotend_id_or_wizard):
        super().__init__(key)

        self._action__hotend_id_or_wizard = action__hotend_id_or_wizard
        self._target_hotend = int(hotend_nr)
        self.__expected_hotend_found = False

    def run(self):
        controller = Controller.getInstance()
        hcm = controller.getPrintHeadController().getHotendCartridgeManager()
        expected_name = None

        if self._action__hotend_id_or_wizard == "inserted":
            expected = True
        elif self._action__hotend_id_or_wizard == "removed":
            expected = False
        elif isinstance(self._action__hotend_id_or_wizard, Procedure):
            # This is done for the welcome setup, not only can we wait for a specific statically chosen hotend, we can alo dynamically get the hotend to be used from the wizard procedure that instantiates this class.
            # the wizard procedure then uses the action_or_hotend_type to pass a link to the wizard procedure to this class.
            expected = True
            expected_name = self._action__hotend_id_or_wizard.getMetaData()["next_expected_hotend_id"]
            self._action__hotend_id_or_wizard = expected_name
        elif re.findall("^\w{2} \d+(?:\.\d+)?$", self._action__hotend_id_or_wizard):
            expected = True
            expected_name = self._action__hotend_id_or_wizard
        else:
            raise ValueError("Undefined action '%s' specified" % self._action__hotend_id_or_wizard)

        if expected_name is None and expected == False:
            while hcm.isPresent(self._target_hotend) != expected and not self._abort:
                time.sleep(0.1)
        else:
            hcm.onHotendDataReady.connect(self._onHotendData_ready)

            if hcm.isPresent(self._target_hotend) and hcm.getValue(self._target_hotend, "hotend_cartridge_id") is not None:
                self._onHotendData_ready(self._target_hotend)

            while not self.__expected_hotend_found:
                time.sleep(0.1)

            hcm.onHotendDataReady.disconnect(self._onHotendData_ready)

    ## called by the onHotendDataReady signal after the hotend's eeprom is read and calls to the hcm will return with valid data.
    def _onHotendData_ready(self, hotend_nr):
        if hotend_nr == self._target_hotend:
            hcm = Controller.getInstance().getPrintHeadController().getHotendCartridgeManager()
            mm = Controller.getInstance().getMaterialManager()
            hotend_cartridge_id = hcm.getValue(self._target_hotend, "hotend_cartridge_id")
            hotend = Controller.getInstance().getPrintHeadController().getHotendSlot(self._target_hotend)
            material_guid = hotend.getPropertyValue("material_guid")

            if mm.isHotendCompatibleWithMaterial(hotend_cartridge_id, material_guid) == mm.INCOMPATIBLE:
                return

            if self._action__hotend_id_or_wizard == "inserted":
                self.__expected_hotend_found = True

            log.info("detected %s type cartridge in slot %d, expected %s", hotend_cartridge_id, self._target_hotend, self._action__hotend_id_or_wizard)
            if hotend_cartridge_id != self._action__hotend_id_or_wizard and hotend_cartridge_id is not None:
                log.warning("wrong cartridge inserted expected %s to be inserted but found %s instead.", self._action__hotend_id_or_wizard, hotend_cartridge_id)
            else:
                self.__expected_hotend_found = True

    def getMetaData(self):
        hcm = Controller.getInstance().getPrintHeadController().getHotendCartridgeManager()
        hotend_cartridge_id = hcm.getValue(self._target_hotend, "hotend_cartridge_id")
        return {
            "detected": str(hotend_cartridge_id),
            "expected": self._action__hotend_id_or_wizard if isinstance(self._action__hotend_id_or_wizard, str) else "",
            "hotend_nr": self._target_hotend,
            "display_hotend_nr": self._target_hotend + 1
        }

