from griffin.gcodeGenerator import GcodeGenerator
from griffin.printer.translationXYNozzleOffset import TranslationXYNozzleOffset

import re
import copy
import math
import logging

log = logging.getLogger(__name__.split(".")[-1])

# This generates gcode
# All values can be assumed to be in mm in this file.
# Points are calculated and gcode is generated printing between those points.


## Generates the pints of a box and lines of brim around it.
# @param axis A string being either "X" or "Y".
# @param line_len Length of the lines that are used to compare if they line up.
# @param distance The distance over which to draw the lines (not the length of the lines).
# @param lines The number of brim lines to add
# @return list of coordinate for the generated pattern.
def __genBrimAndBox(axis, line_len, distance, lines, nozzle_diam):
    # i%4/2 selects left side vs right side (or up/down if in y parameter).
    # the rest is to give the box size and compensate for diameter when doing multiple lines.
    calcX = lambda i: int((i + 1) % 4 / 2) * line_len * 2 + int((4 * lines - i - 1) / 4) * nozzle_diam * (2 * int((i + 1) % 4 / 2) - 1)
    calcY = lambda i: int(i % 4 / 2) * distance + int((4 * lines - i - 1) / 4) * nozzle_diam * (2 * int(i % 4 / 2) - 1)
    points = [[calcX(i), calcY(i)] for i in range(0, 4 * lines + 1) ]
    points = GcodeGenerator.translate(points, -15, 0)
    if axis != "Y":
        points = [[x, y] for (y, x) in points]
    return points


# Calculates the X position used by the ruler pattern
# @param i the index of the x of the point to generate.
# @param line_len lenght of the lines that are used to compare if they line up.
# @param decade_lines boolean enabling slightly longer lines in the case of every 5th and 10th line.
# @return float x coordinate of generated point.
def __calcX2(i, line_len, decade_lines):
    ret = int((i + 1) % 4 / 2) * line_len
    if decade_lines:    # every 5th and tenth line extends a bit for easier counting
        if ((i - 2) % 10 / 9) >= 8 / 9:
            if ret == line_len:
                ret += int(1) * line_len / 4
        ret += int((i - 2) % 20 / 19) * line_len / 4
    return ret


# Generates a ruler like pattern of lines.
# @param axis A string being either "X" or "Y".
# @param freq Frequency or inter line distance (distance/freq is the nr of lines).
# @param line_len lenght of the lines that are used to compare if they line up.
# @param distance The distance over which to draw the lines (not the length of the lines).
# @param decade_lines boolean enabling slightly longer lines in the case of every 5th and 10th line.
# @param offset offsets the line frequency at the starting point.
# @return list List of coordinate for the generated pattern.
def __getRulerPattern(axis, freq, line_len = 10, distance = 180, decade_lines = True, offset = 0.0):
    nr_of_lines = distance / freq
    points = [[__calcX2(i, line_len, decade_lines), int(i / 2) * freq + offset] for i in range(0, int(nr_of_lines * 2))]
    if axis != "Y":
        points = [ [x, y] for (y, x) in points ]
    points2 = []

    for i, point in enumerate(points):
        if i%2 == 0:
            points2.append([point])
        else:
            points2[-1].append(point)
    return points2


# Creates file to be printed for XY alignment.
# @param filename Path to the file to write the generated gcode to.
# @param nozzle_0_diameter Diameter that is used to calculate the amount of material to extrude for extruder 0.
# @param nozzle_1_diameter Diameter that is used to calculate the amount of material to extrude for extruder 1.
# @return a dictionary converting found offset numbers into actual ofset in mm.
def generateFile(filename, nozzle_0_diameter = 0.4, nozzle_1_diameter = 0.4, temperature_n0 = 210, temperature_n1 = 210, standby_temperature_n0 = 175, standby_temperature_n1 = 175, bed_temperature = 60, machine_size=[None] * 4):
    gen = GcodeGenerator()

    # basic settings for size and resolution.
    l_len = 15  # length of the lines on the ruler.
    brim_line_count = 4 # amount of brim lines
    stretch_percentage_for_better_PVA_adhesion = 1.1 # 10 percent stretch.
    shift_amount_for_better_PVA_adhesion = ((l_len * stretch_percentage_for_better_PVA_adhesion) - l_len) / 2


    # secondary derived values
    nr_of_lines = TranslationXYNozzleOffset.DISTANCE / TranslationXYNozzleOffset.FREQUENCY_DISTANCE # nr of lines on the ruler (for the base frequency lines).
    offset = TranslationXYNozzleOffset.DISTANCE / 2 - nr_of_lines * TranslationXYNozzleOffset.FREQUENCY_DISTANCE_2 / 2 # the difference between the 2 frequencies.

    comment_lines = []
    comment_lines += [";smallest offset detected %.5f\n" % (TranslationXYNozzleOffset.FREQUENCY_DISTANCE - TranslationXYNozzleOffset.FREQUENCY_DISTANCE_2)]
    comment_lines += [";largest offset detected + or - %.5f\n" % ((TranslationXYNozzleOffset.FREQUENCY_DISTANCE - TranslationXYNozzleOffset.FREQUENCY_DISTANCE_2) * TranslationXYNozzleOffset.DISTANCE / TranslationXYNozzleOffset.FREQUENCY_DISTANCE)]

    # line down the center of the ruler deviding the 1st and 2nd material.
    center = [[[-nozzle_0_diameter / 2,0], [-nozzle_0_diameter / 2, TranslationXYNozzleOffset.DISTANCE]], [[nozzle_0_diameter / 2, TranslationXYNozzleOffset.DISTANCE], [nozzle_0_diameter / 2, 0]]]

    n1_y_brim  = gen.translate(__genBrimAndBox("Y", l_len, TranslationXYNozzleOffset.DISTANCE, brim_line_count, nozzle_0_diameter),                   127.5 - 25 + 14 -46, 20 + 2.6 + 11)
    n1_y_ooze_shield = gen.translate(__genBrimAndBox("Y", l_len + 5, TranslationXYNozzleOffset.DISTANCE + 10, 1, nozzle_0_diameter),                  127.5 - 31 + 14 -46, 15 + 2.6 + 11)
    n1_y_center= gen.translate(center,                                                                                                    127.5 - 25 + 14 -46, 20 + 2.6 + 11)
    # the alignment lines for this direction for both nozzles.
    n1_y_align = gen.translate(__getRulerPattern("Y", TranslationXYNozzleOffset.FREQUENCY_DISTANCE_2, l_len, TranslationXYNozzleOffset.DISTANCE, False),            127.5 - 25 + 14 -46, 20 + 2.6 + 11)
    n2_y_align = gen.translate(__getRulerPattern("Y", TranslationXYNozzleOffset.FREQUENCY_DISTANCE, -l_len * stretch_percentage_for_better_PVA_adhesion, TranslationXYNozzleOffset.DISTANCE, True, offset),  127.5 - 25 + shift_amount_for_better_PVA_adhesion + 14 -46, 20 + 2.6 + 11)

    center_point = gen.findCenterPoint(n1_y_brim)
    n1_y_brim = gen.rotateAbout(center_point, n1_y_brim, 180)
    n1_y_ooze_shield = gen.rotateAbout(center_point, n1_y_ooze_shield, 180)
    n1_y_center = gen.rotateAbout(center_point, n1_y_center, 180)
    n1_y_align = gen.rotateAbout(center_point, n1_y_align, 180)
    n2_y_align = gen.rotateAbout(center_point, n2_y_align, 180)

    comment_lines += [";n1_y_align %d n2_y_align %d\n" % (len(n1_y_align), len(n2_y_align))]

    # finds the point 0 offset point.
    # where if the nozzles were perfectly aligned the match would be made by the user.
    matches = 0
    y_match_in_n2_index = []
    for p1 in n1_y_align:
        mc = 0
        for p2 in n2_y_align:
            if p1[0][gen.Y] == p2[0][gen.Y]:
                comment_lines += [";y match " + str(p1[0][gen.Y]) + "mc %d\n" % mc]
                y_match_in_n2_index.append(mc)
                matches += 1
            mc += 1

    if matches != 1:
        log.error("aborting because there is no single 0 offset point for the Y calibration, there were %d 0 offset point(s) found", matches)
        return

    n2_y_align = gen.translate(n2_y_align, -18, 0) # base nozzle offset

    n1_x_brim  = gen.translate(__genBrimAndBox("X", l_len, TranslationXYNozzleOffset.DISTANCE, brim_line_count, nozzle_0_diameter),                  42 + 14     , 210 - 25 + 6.5)
    n1_x_ooze_shield = gen.translate(__genBrimAndBox("X", l_len + 5, TranslationXYNozzleOffset.DISTANCE + 10, 1, nozzle_0_diameter),                 37 + 14     , 210 - 47 + 6.5 + 16)
    n1_x_center= gen.translate([[[y,x] for x,y in cl] for cl in center],                                                                 42 + 14     , 210 - 25 + 6.5)
    # the alignment lines for this direction for both nozzles, reversed is no longer necessary but was for less travel in continuation.
    n1_x_align = gen.translate(__getRulerPattern("X", TranslationXYNozzleOffset.FREQUENCY_DISTANCE_2, l_len, TranslationXYNozzleOffset.DISTANCE, False),           42 + 14     , 210 - 25 + 6.5)
    n2_x_align = gen.translate(__getRulerPattern("X", TranslationXYNozzleOffset.FREQUENCY_DISTANCE, -l_len * stretch_percentage_for_better_PVA_adhesion, TranslationXYNozzleOffset.DISTANCE, True, offset), 42 + 14     , 210 - 25 + 6.5 + shift_amount_for_better_PVA_adhesion)


    comment_lines += [";n1_x_align %d n2_x_align %d\n" % (len(n1_x_align), len(n2_x_align))]

    # finds the point of 0 offset point.
    # where if the nozzles were perfectly aligned the match would be made by the user.
    matches = 0
    x_match_in_n2_index = []
    for p1 in n1_x_align:
        mc = 0
        for p2 in n2_x_align:
            if p1[0][0] == p2[0][0]:
                comment_lines += [";x match " + str(p1[0][0]) + "mc %d\n" % mc]
                x_match_in_n2_index.append(mc)
                matches += 1
            mc += 1

    if matches != 1:
        log.error("aborting because there is no single 0 offset point for the X calibration, there were %d 0 offset point(s) found", matches)
        return

    n2_x_align = gen.translate(n2_x_align, -18, 0) # base nozzle offset

    gen.setMachineSize(machine_size) # set the actual max X, Y and Z so we'd never get an error even if we should.

    gen.createTool(nozzle_0_diameter)
    gen.createTool(nozzle_1_diameter)
    gen.getTool(0).setTemperature(temperature_n0)
    gen.getTool(1).setTemperature(standby_temperature_n1)
    gen.setBuildplateTemperature(bed_temperature)

    gen.switchToTool(0)
    gen.prime()

    gen.plot(n1_y_ooze_shield)
    gen.plot(n1_y_brim)
    gen.plot(n1_y_center)
    gen.plot(n1_x_ooze_shield)
    gen.plot(n1_x_brim)
    gen.plot(n1_x_center)
    gen.travelTo(n1_y_align[0][0], zHop=True)
    gen.plot([n1_y_align], retract=False)
    # Already start heating up the second nozzle
    gen.travelTo(n1_x_align[0][0], zHop=True)
    gen.plot([n1_x_align], retract=False)
    gen.retract()

    gen.lines.append(";Second nozzle patterns\n")
    gen.switchToTool(1)
    gen.getTool(1).setTemperature(temperature_n1)
    gen.prime()
    gen.setExtrudeSpeed(in_mmps=10)

    gen.travelTo(n2_y_align[0][0])
    gen.plot([n2_y_align], retract=True)
    gen.travelTo(n2_x_align[0][0], zHop=True)
    gen.plot([n2_x_align], retract=True)


    # adding combing for every fifth and tenth line which are longer
    # the printhead should move back on the line extruded/to be extruded since it doesn't retract.
    def combing(lines):
        pos = {"Y":0, "X":0, "Z":0}
        key_map = {"Y": "X", "X": "Y" }
        codes = ["X", "Y"]
        for (i, l) in enumerate(lines):
            new_pos = {key: float(value) for (key, value) in re.findall("([XY])(-?\d+(?:\.\d+)?)", l, flags=re.MULTILINE)}
            diffs = 0
            key = None
            for code in codes:
                if new_pos.get(code) is None:
                    continue
                if pos[code] != new_pos[code] and "G92 E0" != lines[i - 1]:
                    diff = abs(pos[code] - new_pos[code])
                    if diff == l_len / 4 * 2:
                        key = str(key_map.get(code))
                    elif diff == l_len / 4:
                        key = code

                    #if i > len(lines) / 2:
                    #    key = str(key_map.get(key))

                    diffs += 1

            if diffs > 1 and key is not None:
                not_key = str(key_map.get(key))
                lines[i] = ("G1 %s%.5f ;EXTRA\n" % (not_key, new_pos[not_key])) + l
            pos.update(new_pos)

    gen.postProcess(combing)
    gen.writeToFile(filename, comment_lines, time=518) # time measured during test

if __name__ == "__main__":
    generateFile("a.gcode")
    print("created file \"a.gcode\".")
