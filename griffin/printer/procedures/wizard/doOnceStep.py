from griffin.printer.procedures.procedureStep import ProcedureStep


# this step breaks its link to the next step it's started once.
class DoOnceStep(ProcedureStep):
    def __init__(self, next_step = None):
        super().__init__("ONCE")
        self.setNextStep(next_step)
        self._has_ran = False

    def run(self):
        if self._has_ran:
            self._protectedSetNextStepObsolete(None)
        self._has_ran = True
