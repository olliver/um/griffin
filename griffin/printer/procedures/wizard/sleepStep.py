from griffin.printer.procedures.procedureStep import ProcedureStep
import time


## This class sleeps in its run procedure for as long as you set it to.
#  The only purpose is if some action needs to be delayed, etc.
#  This step is also used by the G4 gcode command to wait inside griffin instead of blocking communication by waiting in marlin.
class SleepStep(ProcedureStep):
    def __init__(self, key, time=0):
        super().__init__(key)
        self._time = time

    def setTime(self, seconds):
        self._time = seconds

    def run(self):
        time.sleep(self._time)
