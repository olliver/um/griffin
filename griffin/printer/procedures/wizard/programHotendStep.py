from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


class ProgramHotendStep(ProcedureStep):
    def __init__(self, key, hotend_nr, hotend_type = None):
        super().__init__(key)
        self._hotend_nr = hotend_nr
        self._type = hotend_type
        # TODO remove dirty hack!
        if self._type is None:
            self._type = ["AA", "BB"][hotend_nr]

    def run(self):
        c = Controller.getInstance()
        c.getPrintHeadController().getHotendCartridgeManager().programHotend(self._hotend_nr, self._type)
