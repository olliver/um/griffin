from griffin.printer.controller import Controller
from griffin.printer.procedures.procedureStep import ProcedureStep
import logging
log = logging.getLogger(__name__.split(".")[-1])


class SetBedZeroPosition(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()

        found_offset = c.getPosition("Z")

        log.info("found_offset %s", found_offset)

        z_max_pos_offset = found_offset - c.getPropertyValue("leveling_paper_thickness")
        log.info("Found offset for manual bed leveling including taking into account the paper thickness: %s", z_max_pos_offset)

        c.getPrintHeadController().getHotendSlot(0).setPropertyValue("z_height", z_max_pos_offset)

        # Skew should be 0 after manual level.
        c.getBed().setPropertyValue("SKEW_X", 0)
        c.getBed().setPropertyValue("SKEW_Y", 0)
