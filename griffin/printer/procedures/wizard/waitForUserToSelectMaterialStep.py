import logging
from griffin.printer.faultHandler import FaultCode, FaultHandler
from griffin.thread import Event
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


class WaitForUserToSelectMaterialStep(ProcedureStep):
    def __init__(self, key, hotend, only_select_material=False):
        super().__init__(key)

        self._target_hotend = int(hotend)
        self._only_select_material = only_select_material
        self._finished_event = Event()

        self.__hotend_slot = Controller.getInstance().getPrintHeadController().getHotendSlot(self._target_hotend)
        # Monitor the material_guid for changes, if this is set by receiving a message on this step or by other means, we know that we can continue.
        self.__hotend_slot.getPropertyAsObject("material_guid").onChange.connect(self.__onMaterialGUIDUpdate)

        self.__metadata = {"selected_material": "", "material_source": "", "show_wait_for_rfid_tag": True}
        self.__material_service = dbusif.RemoteObject("material")

        self.__beep_service = dbusif.RemoteObject("beep")
        self.__led_service = dbusif.RemoteObject("led")

    # Do nothing when this step is started. Just wait for the proper message to be received.
    def run(self):
        self.__hotend_slot.setPropertyValue("material_guid", "")

        self._finished_event.clear()
        while not self._finished_event.is_set() and not self._abort:
            self._finished_event.wait(self.TIME_OUT_WAIT_FINISHED_EVENT)

    # When the material GUID is set for the hotend then this step is finished.
    def __onMaterialGUIDUpdate(self, property, new_guid):
        if new_guid != "":
            self._finished_event.set()

    # External services can send a message to active procedures.
    # These messages are passed to the currently active procedure step at which point this function is called.
    def receiveMessage(self, message):
        # When a material is selected, we get a message here to tell which material it is.
        # We get the message in the form of "SELECT_MATERIAL:{GUID}:{SELECTION SOURCE}"

        move_and_wait = Controller.getInstance().getProcedure("MOVE_MATERIAL_WAIT_MESSAGE_%d" % self._target_hotend)

        log.info("Message: %s", message)
        items = message.split(":")
        if len(items) == 3 and items[0] == "SELECT_MATERIAL":
            brand, name, color = self.__material_service.getMaterialNameFields(items[1])
            if brand == "?" and name == "?" and color == "?":
                log.warning("ignoring unknown material %s with brand %s, name %s, color %s", items[1], brand, name, color)
                return # ignore unknown material

            hotend_id = Controller.getInstance().getPrintHeadController().getHotendCartridgeProperty(self._target_hotend, "hotend_cartridge_id")
            mm = Controller.getInstance().getMaterialManager()

            compatibility = mm.isHotendCompatibleWithMaterial(hotend_id, items[1])
            if compatibility == mm.INCOMPATIBLE:
                log.warning("ignoring material %s with brand %s, name %s, color %s because it is not compatible with the hotend[%d] %s", items[1], brand, name, color, self._target_hotend, hotend_id)
                data = {"display_hotend_nr": self._target_hotend + 1, "detected_cartridge_type": hotend_id, "material_name": " ".join(self.__material_service.getMaterialNameFields(items[1]))}
                FaultHandler.warning("hotend in slot {display_hotend_nr} loaded with cartridge type {detected_cartridge_type} is not compatible with material {material_name}"
                .format(**data), FaultCode.MATERIAL_INCOMPATIBLE_WITH_HOTEND, data=data)
                return # ignore material that is not compatible.
            elif compatibility == mm.UNKNOWN_COMPATIBILITY:
                log.warning("material %s with brand %s, name %s, color %s may not be compatible with the hotend[%d] %s", items[1], brand, name, color, self._target_hotend, hotend_id)
                data = {"display_hotend_nr": self._target_hotend + 1, "detected_cartridge_type": hotend_id, "material_name": " ".join(self.__material_service.getMaterialNameFields(items[1]))}
                FaultHandler.warning("hotend in slot {display_hotend_nr} loaded with cartridge type {detected_cartridge_type} has unknown material compatibility with {material_name}"
                .format(**data), FaultCode.UNKNOWN_MATERIAL_COMPATIBILITY_WITH_HOTEND, data=data)

            settings = self.__material_service.getMaterialSettings(items[1], hotend_id)
            self.__metadata.update({"selected_material": items[1], "material_source": items[2], "brand": brand, "name": name, "color": color, "meters": "?"})
            if items[2] == "RFID":
                self.__beep_service.beep(440, 25)
                ## Blinking the led strip seems to cause the DBus timeouts!!! Disabled for now, after fixing EM-699
                #self.__led_service.blinkOnce("STRIP")

            # start heatup and start moving
            if not self._only_select_material:
                self.__hotend_slot.setPropertyValue("pre_tune_target_temperature", settings["print temperature"])
                move_and_wait.setup({})
                move_and_wait.start()

        elif message == "CONFIRM":
            if self.__hotend_slot.setPropertyValue("material_guid", self.__metadata["selected_material"]):
            #    # When we successfully set the material GUID, also store the source where we got this GUID from.
                self.__hotend_slot.setPropertyValue("material_guid_source", self.__metadata["material_source"])
                if not self._only_select_material:
                    move_and_wait.receiveMessage("DONE")

        elif message == "RESET_SELECTED":
            self.__metadata.update({"selected_material": "", "material_source": "", "show_wait_for_rfid_tag":True})
            if not self._only_select_material:
                # stop heatup (back to 100degC) and stop move
                self.__hotend_slot.setPropertyValue("pre_tune_target_temperature", 100) # back to safe temperature when material not known...
                move_and_wait.receiveMessage("DONE")
        elif message == "SELECT_MANUALLY":
            self.__metadata.update({"show_wait_for_rfid_tag": False})
        elif message == "TOO_MANY_MATERIALS_DETECTED":
            FaultHandler.warning("NFC service detects too many materials.", FaultCode.NFC_DETECTS_TOO_MANY_MATERIALS)
        else:
            log.error("Wrong message format: %s", message)
            raise ValueError("Wrong message format: %s" % message)

    def getMetaData(self):
        return self.__metadata

    def abort(self):
        super().abort()
        self._finished_event.set()
