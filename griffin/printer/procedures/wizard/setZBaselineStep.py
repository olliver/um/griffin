from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.buildPlateLocations import BuildPlateLocations

import logging
log = logging.getLogger(__name__.split(".")[-1])


## This class sets the baseline reference of the buildplate location to offset locations during manual leveling.
class SetZBaselineStep(ProcedureStep):
    def __init__(self, key, value):
        super().__init__(key)
        self._value = value

    def run(self):
        if self._value == "current":
            z = Controller.getInstance().getPosition("Z")
        elif self._value == "z_nozzle_0":
            z = Controller.getInstance().getPrintHeadController().getHotendSlot(0).getPropertyValue("z_height")
        else:
            z = self._value
        BuildPlateLocations.getInstance().setBaselineZ(z)
