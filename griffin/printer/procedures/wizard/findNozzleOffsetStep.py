from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

from griffin.printer.faultHandler import FaultHandler

import re
import logging
log = logging.getLogger(__name__.split(".")[-1])


class FindNozzleOffsetStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()
        found_offset = c.getPosition("Z")

        z0 = c.getPrintHeadController().getHotendSlot(0).getPropertyValue("z_height")
        z1 =  found_offset - c.getPropertyValue("leveling_paper_thickness")
        c.getPrintHeadController().getHotendSlot(1).setPropertyValue("z_height", z1)

        if z0 != "":
            log.info("Found height: %s, offset %s", z1, z1 - z0)
        else:
            log.info("Found height: %s", z1)
