from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.procedures.wizard.generateXyTestPattern import generateFile

import logging

log = logging.getLogger(__name__.split(".")[-1])


class GenerateXyTestPatternFileStep(ProcedureStep):
    def __init__(self, key, filename = "/tmp/testPattern.gcode"):
        super().__init__(key)
        self._filename = filename

    def run(self):
        c = Controller.getInstance()

        hcm = c.getPrintHeadController().getHotendCartridgeManager()
        mm = c.getMaterialManager()

        kwargs = {}
        for i in range(c.getPropertyValue("hotend_count")):
            if hcm.isPresent(i):
                diam = hcm.getValue(i, "nozzle_size")
                if diam is not None:
                    kwargs["nozzle_%d_diameter" % i] = float(diam)
                temp = mm.getPrintingTemperature(i)
                if temp is not None:
                    kwargs["temperature_n%d" % i] = float(temp)
                temp = mm.getStandbyTemperature(i)
                if temp is not None:
                    kwargs["standby_temperature_n%d" % i] = float(temp)

        temp = mm.getRequiredBedTemperature()
        if temp is not None:
            kwargs["bed_temperature"] = float(temp)

        machine_size = [None] * 4
        temp_table = ["x", "y", "z"]
        for axis in temp_table:
            machine_size[temp_table.index(axis)] = c.getPropertyValue("build_volume_{axis}".format(axis=axis))

        kwargs["machine_size"] = machine_size

        generateFile(self._filename, **kwargs)
