import logging

from griffin.printer.controller import Controller
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.print.switchActiveHotendStep import SwitchActiveHotendStep
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.steps.gotoLocationStep import GotoLocationStep
from griffin.printer.procedures.steps.homeBedStep import HomeBedStep
from griffin.printer.procedures.steps.homeHeadStep import HomeHeadStep
from griffin.printer.procedures.steps.setPropertyStep import SetPropertyStep
from griffin.printer.procedures.temperature.setHotendTemperatureStep import SetHotendTemperatureStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.wizard.checkNozzleDataStep import CheckNozzleDataStep
from griffin.printer.procedures.wizard.moveMaterialWaitMessageStep import MoveMaterialWaitMessageStep
from griffin.printer.procedures.wizard.sleepStep import SleepStep
from griffin.printer.procedures.wizard.waitForHotendChangeStep import WaitForHotendChangeStep
from griffin.printer.procedures.wizard.waitMessageStep import WaitMessageStep

log = logging.getLogger(__name__.split(".")[-1])


# The wizard procedure which handles the change hotend option for the end user.
class ChangeHotend(Procedure):
    # Amount of retraction done when the hotend needs to be removed. This is a long retraction as the filament needs to move all the way out of the hotend. In mm.
    RETRACT_LENGTH_ON_HOTEND_CHANGE = 90.0
    # Speed at which the retraction happens. in mm/s
    RETRACT_SPEED_ON_HOTEND_CHANGE = 10.0

    def __init__(self, key, wizard_type="change"):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)
        self._hotend_nr = 0
        self._wizard_type = wizard_type

    def setup(self, args):
        self._hotend_nr = int(args.get("hotend_nr", 0))
        self.__is_primed = Controller.getInstance().getPrintHeadController().getHotendSlot(self._hotend_nr).getPropertyValue("is_primed")

        # Generate all the steps for this wizard procedure.
        steps = []
        steps += self._generatePreperationSteps()
        if self._wizard_type == "change":
            steps += self._generateChangeHotendSteps()
        elif self._wizard_type == "unload":
            steps += self._generateUnloadHotendSteps()
        elif self._wizard_type == "load":
            steps += self._generateLoadHotendSteps()
        else:
            log.error("Unknown change hotend wizard type: %s", self._wizard_type)
        steps += self._generateFinalizationSteps()

        for n in range(0, len(steps) - 1):
            steps[n].setNextStep(steps[n + 1])
        self.setFirstStep(steps[0])
        self.setAbortStep(steps[-1])

        return True

    def _generatePreperationSteps(self):
        return [
            # Prepare the printer for material change. The printer state is unknown, so home, and move the head switch into a known position
            HomeHeadStep("HOME_HEAD"),
            HomeBedStep("HOME_BED"),
            SwitchActiveHotendStep("RESET_ACTIVE_HOTEND", "reset"),
            GotoLocationStep("MOVE_HEAD_TO_MAINTENANCE_POS", "CHANGE_HOTENDS_MAINTENANCE"),
        ]

    def _generateChangeHotendSteps(self):
        steps = []
        if self.__is_primed:
            # Heat up the hotend to printing temperature, so we can extract the material properly.
            steps.append(OtherProcedureStep("HEATUP_WAIT", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr, "target": "remove"}))
        steps += [
            # Move the material backwards, first to a short pull, and then the long move.
            OtherProcedureStep("REVERSE_MATERIAL", "MOVE_MATERIAL", {"hotend_nr": self._hotend_nr, "target": -self.RETRACT_LENGTH_ON_HOTEND_CHANGE, "speed": self.RETRACT_SPEED_ON_HOTEND_CHANGE}),
            WaitForQueueToBeEmptyStep("WAIT_REVERSE_MATERIAL_FINISHED"),
            # Wait for the user to remove the old material from the machine.
            OtherProcedureStep("COOLDOWN_WAIT", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr, "target": 0}),

            SetPropertyStep("PROP_ON", "printer.head.0.hotend.%d" % self._hotend_nr, "interaction_required", True),
            WaitMessageStep("WAIT_FOR_FAN_BRACKET_OPEN", "CONTINUE"),
            WaitForHotendChangeStep("WAIT_FOR_REMOVAL", self._hotend_nr, "removed"),
            SleepStep("REMOVE_HOTEND_WAIT", 1),
            WaitForHotendChangeStep("WAIT_FOR_INSERTION", self._hotend_nr, "inserted"),
            SetPropertyStep("PROP_OFF", "printer.head.0.hotend.%d" % self._hotend_nr, "interaction_required", False),

            WaitMessageStep("WAIT_FOR_FAN_CLOSE", "CLOSED"),
        ]
        if self.__is_primed:
            steps.append(OtherProcedureStep("HEATUP_WAIT_AFTER_LOAD", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr}))
        steps.append(OtherProcedureStep("FORWARD_MATERIAL", "MOVE_MATERIAL", {"hotend_nr": self._hotend_nr, "target": self.RETRACT_LENGTH_ON_HOTEND_CHANGE, "speed": self.RETRACT_SPEED_ON_HOTEND_CHANGE}))
        if self.__is_primed:
            # TODO: make this conditional based on material GUID's matching for last used materials in EM-386 (or replacement issue) this can only be done using a runtime condition between steps.
            steps += [
                MoveMaterialWaitMessageStep("CONTINUOUS_MOVE", self._hotend_nr, "forward"),

                OtherProcedureStep("DEPRIME", "DEPRIME", {"hotend_nr": self._hotend_nr}),
                SetHotendTemperatureStep("START_COOLDOWN_HOTEND", self._hotend_nr, 0),
            ]
        return steps

    def _generateUnloadHotendSteps(self):
        steps = []
        if self.__is_primed:
            # Heat up the hotend to printing temperature, so we can extract the material properly.
            steps.append(OtherProcedureStep("HEATUP_WAIT", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr, "target": "remove"}))
            # Move the material backwards, first to a short pull, and then the long move.
        steps += [
            OtherProcedureStep("REVERSE_MATERIAL", "MOVE_MATERIAL", {"hotend_nr": self._hotend_nr, "target": -self.RETRACT_LENGTH_ON_HOTEND_CHANGE, "speed": self.RETRACT_SPEED_ON_HOTEND_CHANGE}),
            WaitForQueueToBeEmptyStep("WAIT_REVERSE_MATERIAL_FINISHED"),
            # Wait for the user to remove the old material from the machine.
            OtherProcedureStep("COOLDOWN_WAIT", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr, "target": 0}),

            SetPropertyStep("PROP_ON", "printer.head.0.hotend.%d" % self._hotend_nr, "interaction_required", True),
            WaitMessageStep("WAIT_FOR_FAN_BRACKET_OPEN", "CONTINUE"),
            WaitForHotendChangeStep("WAIT_FOR_REMOVAL", self._hotend_nr, "removed"),
            WaitMessageStep("WAIT_FOR_FAN_CLOSE", "CLOSED"),
            SetPropertyStep("PROP_OFF", "printer.head.0.hotend.%d" % self._hotend_nr, "interaction_required", False),
        ]
        return steps

    def _generateLoadHotendSteps(self):
        steps = [
            SetPropertyStep("PROP_ON", "printer.head.0.hotend.%d" % self._hotend_nr, "interaction_required", True),
            WaitMessageStep("WAIT_FOR_FAN_BRACKET_OPEN_BEFORE_INSERT", "CONTINUE"),
            WaitForHotendChangeStep("WAIT_FOR_INSERTION_LOAD", self._hotend_nr, "inserted"),
            SetPropertyStep("PROP_OFF", "printer.head.0.hotend.%d" % self._hotend_nr, "interaction_required", False),
            WaitMessageStep("WAIT_FOR_FAN_CLOSE", "CLOSED"),
        ]
        if self.__is_primed:
            steps.append(OtherProcedureStep("HEATUP_WAIT_AFTER_LOAD", "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": self._hotend_nr}))
        steps.append(OtherProcedureStep("FORWARD_MATERIAL", "MOVE_MATERIAL", {"hotend_nr": self._hotend_nr, "target": self.RETRACT_LENGTH_ON_HOTEND_CHANGE, "speed": self.RETRACT_SPEED_ON_HOTEND_CHANGE}))
        if self.__is_primed:
            # TODO: make this conditional based on material GUID's matching for last used materials in EM-386 (or replacement issue) this can only be done using a runtime condition between steps.
            steps += [
                MoveMaterialWaitMessageStep("CONTINUOUS_MOVE", self._hotend_nr, "forward"),

                OtherProcedureStep("DEPRIME", "DEPRIME", {"hotend_nr": self._hotend_nr}),
                SetHotendTemperatureStep("START_COOLDOWN_HOTEND", self._hotend_nr, 0),
            ]
        return steps

    def _generateFinalizationSteps(self):
        return [
            # Finalization step. Also used as abort step.
            OtherProcedureStep("FINISHING", "GENERIC_WIZARD_FINISHING_PROCEDURE"),
            CheckNozzleDataStep("CHECK_NOZZLE_COMBI"),
        ]

    # Override the getMetaData function to add our selected hotend nr.
    # This so consumers of the dbus-API know which hotend is currently being accessed with this procedure.
    def getMetaData(self):
        result = super().getMetaData()
        result["hotend_nr"] = self._hotend_nr
        result["display_hotend_nr"] = self._hotend_nr + 1
        return result
