import logging

from griffin.printer.procedures.queueRawGCodeWaitStep import QueueRawGCodeWaitStep
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.steps.homeBedStep import HomeBedStep
from griffin.printer.procedures.steps.homeHeadStep import HomeHeadStep
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.print.switchActiveHotendStep import SwitchActiveHotendStep
from griffin.printer.procedures.steps.gotoLocationStep import GotoLocationStep
from griffin.printer.procedures.steps.gotoPositionStep import GotoPositionStep
from griffin.printer.procedures.wizard.setBedZeroPosition import SetBedZeroPosition
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.wizard.waitMessageStep import WaitMessageStep
from griffin.printer.procedures.wizard.setZBaselineStep import SetZBaselineStep
from griffin.printer.buildPlateLocations import BuildPlateLocations
from griffin.printer.procedures.wizard.checkCartridgeLoaded import CheckCartridgeLoaded

log = logging.getLogger(__name__.split(".")[-1])


## Manual bed leveling wizard. This wizard currently assumes 3 point leveling with 1 screw in the back and 2 in the front.
class ManualBedLeveling(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_MAINTENANCE)

    def setup(self, args):
        safe_travel_z = BuildPlateLocations().getSafeTravelHeight()
        steps = [
            CheckCartridgeLoaded("CHECK_CARTRIDGE_LOADED_0", 0),
            WaitMessageStep("WAIT_FOR_START", "START"),

            # Prepare the printer for manual bed leveling.
            HomeBedStep("HOME_BED_START"),
            HomeHeadStep("HOME_HEAD"),
            SwitchActiveHotendStep("RESET_ACTIVE_HOTEND", "reset"),
            OtherProcedureStep("SWITCH_TO_HOTEND", "SWITCH_ACTIVE_HOTEND", {"target": 0}),
            WaitForQueueToBeEmptyStep("WAIT_QUEUE"),

            #Move to back position and wait for user to set the proper Z height (which uses an different procedure...)
            GotoLocationStep("MOVE_HEAD_TO_BACK_POS", location="CENTER_REAR"),
            WaitForQueueToBeEmptyStep("INIT_COMPLETE"),
            WaitMessageStep("WAIT_MOVE_NEXT_POS_1", "MOVE_NEXT_POS"),
            WaitForQueueToBeEmptyStep("WAITING"),

            # Set current Z position as zero position.
            SetZBaselineStep("SETTING_BASE_0", "current"), # this essentially fixes the bed position.

            GotoLocationStep("MOVE_BED_DOWN", location="CENTER_REAR"),
            GotoLocationStep("MOVE_HEAD_TO_RIGHT_POS", location="RIGHT_FRONT"),
            GotoLocationStep("MOVE_BED_UP", location="RIGHT_FRONT", overrule_coordinates={"z": 0}),
            WaitMessageStep("WAIT_MOVE_NEXT_POS_2", "MOVE_NEXT_POS"),

            GotoLocationStep("MOVE_BED_DOWN", location="RIGHT_FRONT"),
            GotoLocationStep("MOVE_HEAD_TO_LEFT_POS", location="LEFT_FRONT"),
            GotoLocationStep("MOVE_BED_UP", location="LEFT_FRONT", overrule_coordinates={"z": 0}),
            WaitMessageStep("WAIT_MOVE_NEXT_POS_3", "MOVE_NEXT_POS"),

            GotoLocationStep("MOVE_BED_DOWN", location="LEFT_FRONT"),
            GotoLocationStep("MOVE_HEAD_TO_BACK_POS", location="CENTER_REAR"),
            GotoLocationStep("MOVE_BED_UP", location="CENTER_REAR", overrule_coordinates={"z": 0}),
            WaitMessageStep("WAIT_MOVE_NEXT_POS_4", "MOVE_NEXT_POS"),
            WaitForQueueToBeEmptyStep("WAITING"),

            # otherwise information from fine tuning step is discarded
            SetZBaselineStep("SETTING_BASE_1", "current"),

            GotoLocationStep("MOVE_BED_DOWN", location="CENTER_REAR"),
            GotoLocationStep("MOVE_HEAD_TO_RIGHT_POS", location="RIGHT_FRONT"),
            GotoLocationStep("MOVE_BED_UP", location="RIGHT_FRONT", overrule_coordinates={"z": 0}),
            WaitMessageStep("WAIT_MOVE_NEXT_POS_5", "MOVE_NEXT_POS"),

            GotoLocationStep("MOVE_BED_DOWN", location="RIGHT_FRONT"),
            GotoLocationStep("MOVE_HEAD_TO_LEFT_POS", location="LEFT_FRONT"),
            GotoLocationStep("MOVE_BED_UP", location="LEFT_FRONT", overrule_coordinates={"z": 0}),
            WaitMessageStep("WAIT_DONE", "DONE"),

            WaitForQueueToBeEmptyStep("WAITING_PRE_SAVE"),
            SetBedZeroPosition("SAVE_POSITION"),
            SetZBaselineStep("SETTING_BASE_0", 0),
            GotoPositionStep("MOVE_BED_DOWN", z=safe_travel_z), # this move is here to make sure the subsequent ChangeActiveHotendStep in the MANUAL_NOZZLE_OFFSET_WIZARD does not scrape over the bed.
        ]

        for n in range(0, len(steps) - 1):
            steps[n].setNextStep(steps[n + 1])
        self.setFirstStep(steps[0])
        abort_step = HomeBedStep("HOME_BED_ABORT")
        abort_step.setNextStep(QueueRawGCodeWaitStep("POWER_DOWN_STEPPERS", "M84"))
        self.setAbortStep(abort_step)

        return True
