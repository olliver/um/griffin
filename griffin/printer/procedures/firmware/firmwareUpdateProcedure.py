from griffin.printer.firmware.firmwareUpdate import FirmwareUpdate
from griffin.printer.procedures.firmware.checkFirmwareStep import CheckFirmwareStep
from griffin.printer.procedures.firmware.verifyFirmwareStep import VerifyFirmwareStep
from griffin.printer.procedures.firmware.writeFirmwareStep import WriteFirmwareStep
from griffin.printer.procedures.procedure import Procedure
import logging
import time

log = logging.getLogger(__name__.split(".")[-1])

# this class just tracks the status of the griffin.printer.firmware.firmwareUpdate package


class FirmwareUpdateProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_BOOT)
        self._start_time = None
        self._updater = FirmwareUpdate(self.writeStarted)
        self._step0 = CheckFirmwareStep("FIRMWARE_UPDATE_CHECKING", self._updater)
        self._step1 = WriteFirmwareStep("FIRMWARE_UPDATE_WRITING", self._updater)
        self._step2 = VerifyFirmwareStep("FIRMWARE_UPDATE_VERIFYING", self._updater)
        self.setFirstStep(self._step0).setNextStep(self._step1).setNextStep(self._step2)

    def setup(self, args):
        if "file" in args:
            self._updater.setFile(args["file"])
        self._step0.setNextStep(self._step1)
        return True

    def start(self):
        """ Override start() and start without controller """

        # this procedure can start itself even if not connected (especially then?)
        return self._protectedStart(True)

    def writeStarted(self):
        self._start_time = time.monotonic()

    def getMetaData(self):
        if self._updater is None:
            return {"EMPTY_ORIGIN": "FirmwareUpdateProcedure"}
        progress = self._updater.progress()
        if progress is None:
            return {"EMPTY_ORIGIN": "FirmwareUpdateProcedure"}

        estimate_total = 200
        estimate_remaining = 200
        if self._start_time is not None and progress[0] > 10:
            elapsed = time.monotonic() - self._start_time
            speed = elapsed/float(progress[0])
            estimate_total = progress[1] * speed
            estimate_remaining = estimate_total - elapsed

        return {
            "jobname": self._active_step.getKey(),
            "time": int(estimate_remaining),    # progress[1] - progress[0]
            "time_total": int(estimate_total),  # progress[1]
            "progress": float(progress[0] / progress[1] / 2 + (0.5 if self._active_step.getKey() == "FIRMWARE_UPDATE_VERIFYING" else 0))
        }
