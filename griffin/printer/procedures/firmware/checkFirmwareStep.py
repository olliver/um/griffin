import logging
from griffin.printer.procedures.procedureStep import ProcedureStep

log = logging.getLogger(__name__.split(".")[-1])


class CheckFirmwareStep(ProcedureStep):
    def __init__(self, key, updater):
        super().__init__(key)
        self._updater = updater

    # Do nothing when this step is started. Just wait for the proper message to be received.
    def run(self):
        if not self._updater.init():
            log.info("Failed to initialize firmware update tool")
            self._updater.close()

        log.info("Checking for firmware update")
        if self._updater.checkFirmware():
            log.info("Firmware already up-to-date")
            self.abort()
            self._updater.close()
        log.info("Firmware check done")
