import logging
from griffin.printer.procedures.procedureStep import ProcedureStep

log = logging.getLogger(__name__.split(".")[-1])


class VerifyFirmwareStep(ProcedureStep):
    def __init__(self, key, updater):
        super().__init__(key)
        self._updater = updater

    # Do nothing when this step is started. Just wait for the proper message to be received.
    def run(self):
        log.info("Verifying firmware")
        if not self._updater.programFirmwareVerify():
            log.error("Failed to properly update the firmware")
        else:
            log.info("Firmware update done.")
        self._updater.close()
