import time
from griffin.printer.schemeHandlers import createSchemeHandler
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

import logging

log = logging.getLogger(__name__.split(".")[-1])


# The main printing step. Will stream some form of code to the printing driver.
# Step will be completed when the file is completely processed.
# Step is started with the processUrl function, but actual GCode streaming is not started
# till the actual run is called and this step is started.
# Scheme handling can be done and finished before this step is run, to pre-process files or
# download files.
class PrintFileStep(ProcedureStep):
    def __init__(self, key, parse_header_step):
        super().__init__(key)

        self.__parse_header_step = parse_header_step
        self._file_handler = None
        self._url_data = None

        # passed from scheme handler to file handler
        self._filename = ""

        # Flag to abort the run()
        self._finished = False

    # Do nothing when this step is started. Just wait for the proper message to be received.
    def run(self):
        # if still running Gcode from pre-print-setup, wait!
        Controller.getInstance().waitForQueueToBeEmpty()

        self._file_handler_factory = self.__parse_header_step.getFactory()
        if self._file_handler_factory is None:
            raise ValueError("file handler factory is None")
        self._file_handler = self._file_handler_factory.getFileHandler()
        if self._file_handler is None:
            raise ValueError("file_handler is None")
        self._finished = False
        self._file_handler.onFinished.connect(self._onFileFinished)
        self._file_handler.start()

        while not self._finished and not self._abort:
            time.sleep(0.5)

        if self._abort:
            log.info("Print procedure aborted")
            # Ensure that controller is not paused.
            c = Controller.getInstance()
            c.setPaused(False)
            self._paused = False
            if self._file_handler is not None:
                self._file_handler.abort()

        while not self._finished:
            time.sleep(0.5)

        self.__parse_header_step.getSchemeHandler().cleanup()

        if self._file_handler is not None:
            self._file_handler.cleanup()

        self._file_handler = None
        self._file_handler_factory = None

    def _onFileFinished(self):
        log.info("File handler finished")
        Controller.getInstance().waitForQueueToBeEmpty()
        self._finished = True

    def getMetaData(self):
        meta_data = {}
        if self._file_handler is not None:
            meta_data.update(self._file_handler.getMetaData())

        return meta_data
