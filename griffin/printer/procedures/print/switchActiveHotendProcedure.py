from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.print.switchActiveHotendStep import SwitchActiveHotendStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep

import logging

log = logging.getLogger(__name__.split(".")[-1])

# The reason this procedure exists instead of directly calling the Step-function
# is that this procedure can be called from G-code and/or web-code.
class SwitchActiveHotendProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self._target_hotend = None
        self._current_hotend = None
        self._step = SwitchActiveHotendStep("SWITCH_ACTIVE_HOTEND")
        self._wait_step = WaitForQueueToBeEmptyStep("WAIT")
        self.setFirstStep(self._wait_step).setNextStep(self._step)
        self.setAbortDisabled(True)

    def setup(self, args):
        if "target" in args:
            self._target_hotend = int(args["target"])
        else:
            self._target_hotend = 0

        log.info("Target hotend = %d" % self._target_hotend)

        self._step.setTarget(self._target_hotend)
        return True

    def getMetaData(self):
        from griffin.printer.controller import Controller
        current = Controller.getInstance().getActiveHotend()
        if current is None:
            current = -1
        return {
            "target": self._target_hotend,
            "current": current
        }
