from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.print.testPausedStep import TestPausedStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.print.waitUntilAllowedToPause import WaitUntilAllowedToPause
from griffin.printer.procedures.queueRawRelativeGCodeStep import QueueRawRelativeGCodeStep
from griffin.printer.procedures.print.pauseStep import PauseStep
from griffin.printer.procedures.steps.setPropertyStep import SetPropertyStep
from griffin.printer.procedures.steps.gotoPositionStep import GotoPositionStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure pauses the print and saves the position, speed and temperature in the resume procedure
class PausePrintProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self.__steps =[
            TestPausedStep("TEST_IF_NOT_PAUSED", False, ["RESUME_PRINT"]),
            PauseStep("PAUSING"),
            WaitForQueueToBeEmptyStep("WAITING_1"),
            WaitUntilAllowedToPause("WAIT_FOR_PAUSE"),
            WaitForQueueToBeEmptyStep("WAITING_2"),
            QueueRawRelativeGCodeStep("RETRACT", "G1 E-10"),
            GotoPositionStep("MOVE_PLATFORM", z=Controller.getInstance().getPropertyValue("build_volume_z") - 10),  # DO NOT USE G28 here!!!!  A G28 will re-calibrate the Z-position.
            GotoPositionStep("MOVE_HEAD", x=10, y=10),
            WaitForQueueToBeEmptyStep("WAITING_TEMPERATURE_SHOULD_NOT_BE_SET_IMMEDIATELY"),
        ]
        for hotend_nr in range(Controller.getInstance().getPropertyValue("hotend_count")):
            self.__steps.append(SetPropertyStep("START_COOLDOWN_HOTEND_{hotend_nr}".format(hotend_nr=hotend_nr),
                "printer.head.0.hotend.{hotend_nr}".format(hotend_nr=hotend_nr), "pre_tune_target_temperature", 0))
        # the build-plate temperature is neither changed nor saved because if it were to be lowered the print could prematurely release from the build-plate.

        for i, step in enumerate(self.__steps):
            if i != 0:
                self.__steps[i-1].setNextStep(step)
            else:
                self.setFirstStep(step)
