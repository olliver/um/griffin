from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import logging

log = logging.getLogger(__name__.split(".")[-1])


## This step saves the position, speed and temperature data during the pausing of the print.
class PauseStep(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()
        speed = c.getPropertyValue("default_travel_speed") * (1 / (c.getPropertyValue("movement_speed_modifier") / 100)) * 60

        # Pause the controller so no more data is sent to the hardware.
        c.setPaused(True)
        c.waitForQueueToBeEmpty()

        # replaced the request for position from marlin  with the position tracked by the controller.
        # As the last commands have been sent(waitForQueueToBeEmpty()), so that is the tracked position.
        # this also removes the need to wait for the queue on marlin to finish!
        # also save last used speed!
        saved_state = c.getGCodeProcessor().getPosition()
        cooling_fan_speed = c.getPrintHeadController().getPropertyContainer().get("cooling_fan_speed")

        saved_state["cooling_fan_speed"] = cooling_fan_speed.get()
        cooling_fan_speed.set(0)

        log_msg = "saved printer state: {X:f}, {Y:f}, {Z:f}, {F:f} cooling_fan_speed {cooling_fan_speed:.3g}"
        for hotend_nr in range(c.getPropertyValue("hotend_count")):
            saved_state["T{}".format(hotend_nr)] = c.getPrintHeadController().getHotendSlot(hotend_nr).getPropertyValue("pre_tune_target_temperature")
            log_msg += " Temperature[{0}] {{T{0}:.3g}}".format(hotend_nr)

        log.info(log_msg.format(**saved_state))
        c.sendRawGCode("G0 F%d" % speed)

        c.getProcedure("RESUME_PRINT").setSavedState(saved_state)
