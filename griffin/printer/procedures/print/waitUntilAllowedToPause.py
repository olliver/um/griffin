from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

## Waits until all abortable procedures are finished
class WaitUntilAllowedToPause(ProcedureStep):
    def __init__(self, key):
        super().__init__(key)

    def run(self):
        c = Controller.getInstance()
        c.waitUntilAllowedToPause()
