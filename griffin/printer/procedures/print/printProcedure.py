import urllib.parse
import os

from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.wizard.waitMessageStep import WaitMessageStep
from griffin.printer.procedures.print.printFileStep import PrintFileStep
from griffin.printer.procedures.print.parseHeaderStep import ParseHeaderStep

from griffin.printer.fileHandlers import getSupportedFileTypes

import logging

log = logging.getLogger(__name__.split(".")[-1])


# The main printing procedure.
# Has the following steps:
# RUN_PRE_PRINT_SETUP - Runs the PRE_PRINT_SETUP procedure and waits for it to finish.
#       Sets up the printer for printing, does homing/leveling/etc if needed.
# WAIT_SCHEME - Waits for the scheme handler to finish. When a file needs to be downloaded from the internet, this
#       step might take a while.
# PRINT - Actual printing step, streams gcode to the printer.
# POST_PRINT - End of print handling, cooling down, homing head, retracting filament
# WAIT_FOR_CLEANUP - Wait till the printed is emptied.
class PrintProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_PRINT)
        self.__job_name = ""
        self.__origin = ""

        # Post step not allowed to abort. On abort in setup step, post step is
        # run. The abort message in the print step is catched, the file read
        # will be aborted, but no "real" abort of procedures is handled. The
        # post step will run as normal.
        self._parse_header_step = ParseHeaderStep("PARSE_HEADER")
        self._setup_step = OtherProcedureStep("RUN_PRE_PRINT_SETUP", "PRE_PRINT_SETUP")
        self._print_step = PrintFileStep("PRINTING", self._parse_header_step)
        self._post_step = OtherProcedureStep("RUN_POST_PRINT", "POST_PRINT")
        self._wait_step = WaitMessageStep("WAIT_FOR_CLEANUP", "PRINTER_CLEANED")

        self.setAbortStep(self._post_step)
        self._header = None
        self._metadata = None

    def setup(self, args):
        #re-set header from last print
        self._header = None

        if "url" not in args:
            log.warning("No URL given to print procedure")
            return False

        self.setFirstStep(self._parse_header_step)
        # Do we need to have a cleanup step or not.
        # If the print is done in a wizard (For example X/Y offset calibration) then the wait for cleanup step needs to be skipped.
        cleanup_step = args.get("cleanup_step", "True") == "True"

        # Re-create the setup step as this might have been changed by a previous abort.
        self._setup_step = OtherProcedureStep("RUN_PRE_PRINT_SETUP", "PRE_PRINT_SETUP")

        log.info("Setting up step chain, cleanup: %s", cleanup_step)
        # Setup the step chain. As this can change when the cleanup step is enabled or not.
        step = self._parse_header_step.setNextStep(self._setup_step).setNextStep(self._print_step).setNextStep(self._post_step)
        if cleanup_step:
            step.setNextStep(self._wait_step)
        else:
            step.setNextStep(None)

        try:
            url_data = urllib.parse.urlparse(args["url"], allow_fragments=False)
        except Exception:
            log.exception("Failed to parse URL data")
            return False

        if "jobname" in args:
            self.__job_name = args["jobname"]
        else:
            basename = os.path.splitext(os.path.basename(url_data.path))
            if basename[1].upper() == ".GZ" and basename[0].upper().endswith(".GCODE"):
                self.__job_name = os.path.splitext(basename[0])[0]
            else:
                self.__job_name = basename[0]

        self.__origin = args.get("origin", "Unknown")
        self._parse_header_step.setUrlData(url_data)
        return True

    def getMetaData(self):
        meta_data = {
            "jobname": self.__job_name,
            "origin" : self.__origin,
            "file_types": ",".join(getSupportedFileTypes()),
            "active_step": str(self._active_step.getKey() if self._active_step is not None else ""),
            "aborted": self._abort,
        }
        meta_data.update(self._print_step.getMetaData())
        return meta_data

    def setGcodeMetaData(self, metadata):
        self._metadata = metadata

    def getGcodeMetaData(self):
        if self._metadata is None:
            raise ValueError("metadata is None") # header should never be requested when None!
        return self._metadata
