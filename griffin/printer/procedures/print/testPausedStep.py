from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller


# convert paused boolean to either paused or running.
def toString(paused):
    return "paused" if paused else "running"


# Simple step to test if the procedure this is in can be ran or whether it should abort.
class TestPausedStep(ProcedureStep):
    def __init__(self, key, expected, procedures):
        super().__init__(key)
        self._expected = expected
        self._procedures = procedures

    def run(self):
        c = Controller.getInstance()
        procedures = c.getAllProcedures()
        running_procedures = [(p.getKey(), p.getActiveStep().getKey(), p) for p in procedures if p.isActive()]
        running_procedure_names = [p[0] for p in running_procedures]

        if (c.getPaused() and "PRINT" in running_procedure_names) != self._expected:
            raise ValueError("expected the system to be %s but is %s" % (toString(self._expected), toString(not self._expected)))

        for p in self._procedures:
            if p in running_procedure_names:
                raise ValueError("can't run this procedure while %s is running" % p)
