import re
import time
import uuid
from griffin.printer.schemeHandlers import createSchemeHandler
from griffin.printer.fileHandlers import createFileHandlerFactory
from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

import logging

log = logging.getLogger(__name__.split(".")[-1])


# This step will create the scheme and file handlers and check the header.
class ParseHeaderStep(ProcedureStep):
    STATE_NONE = "NONE"
    STATE_SCHEME = "SCHEME"
    STATE_FILE = "FILE"

    def __init__(self, key, procedure_key="PRINT"):
        super().__init__(key)

        self.__procedure_key = procedure_key
        self._handling_state = self.STATE_NONE
        self._scheme_handler = None
        self._file_handler = None
        self._saved_position = [None, None, None]
        self._url_data = None

        ## Passed from scheme handler to file handler
        self._filename = ""
        self._metadata = None

        ## Flag to abort the run()
        self._finished = False
        self._file_handler_factory = None

    def run(self):
        self._handling_state = self.STATE_NONE
        self._finished = False
        self._controller = Controller.getInstance()

        # if still running Gcode from pre-print-setup, wait!
        self._controller.waitForQueueToBeEmpty()

        self._scheme_handler = createSchemeHandler(self._url_data.scheme)
        log.info("Created scheme handler: %s", self._scheme_handler)
        self._file_handler = None
        if self._scheme_handler is None:
            log.warning("Failed to find scheme handler for: %s", self._url_data.scheme)
            raise ValueError("scheme_handler is None")

        self._scheme_handler.onFinished.connect(self._onSchemeFinished)
        self._handling_state = self.STATE_SCHEME
        self._scheme_handler.start(self._url_data)

        # No abort implementation here, it's the "procedure" that has to abort.
        while not self._finished and not self._abort:
            time.sleep(0.5)

        if not self._abort:
            self._file_handler_factory = createFileHandlerFactory(self._filename)
            self._file_handler = self._file_handler_factory.getFileHandler()
            self._metadata_handler = self._file_handler_factory.getMetaDataHandler()

            log.info("Created file handler: %s", self._file_handler)
            if self._file_handler is None:
                self._handling_state = self.STATE_NONE
                log.warning("Failed to find file handler for: %s", self._filename)
                raise ValueError("file_handler is None")
            self._handling_state = self.STATE_FILE
            self._metadata = self._metadata_handler.parseMetaData()

        if self._abort:
            self._scheme_handler.cleanup()
            log.info("Print procedure aborted")
            # Ensure that controller is not paused.
            self._controller.setPaused(False)
            if self._file_handler is not None:
                self._file_handler.abort()

        self._controller.getProcedure(self.__procedure_key).setGcodeMetaData(self._metadata)

    # setUrlData is called from the main procedure to setup the scheme handling.
    def setUrlData(self, url_data):
        self._url_data = url_data

    def _onSchemeFinished(self, filename):
        self._filename = filename
        self._finished = True

    def getMetaData(self):
        meta_data = {"header": self._metadata}

        if self._handling_state == self.STATE_SCHEME:
            meta_data.update(self._scheme_handler.getMetaData())
        elif self._handling_state == self.STATE_FILE:
            meta_data.update(self._file_handler.getMetaData())

        return meta_data

    def getFactory(self):
        return self._file_handler_factory

    def getSchemeHandler(self):
        return self._scheme_handler
