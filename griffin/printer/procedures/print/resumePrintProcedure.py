from griffin.printer.controller import Controller
from griffin.printer.procedures.procedure import Procedure
from griffin.printer.procedures.otherProcedureStep import OtherProcedureStep
from griffin.printer.procedures.print.testPausedStep import TestPausedStep
from griffin.printer.procedures.waitForQueueEmptyStep import WaitForQueueToBeEmptyStep
from griffin.printer.procedures.queueRawRelativeGCodeStep import QueueRawRelativeGCodeStep
from griffin.printer.procedures.steps.gotoPositionStep import GotoPositionStep
from griffin.printer.procedures.print.resumeStep import ResumeStep
from griffin.printer.procedures.steps.setPropertyStep import SetPropertyStep
import logging

log = logging.getLogger(__name__.split(".")[-1])


# This procedure resumes the print, the pause procedure gives the position, speed and temperature to this procedure for storage and usage upon resume.
class ResumePrintProcedure(Procedure):
    def __init__(self, key):
        super().__init__(key, Procedure.TYPE_UNKNOWN)
        self.__steps =[]
        self.__saved_state = {}

    def setup(self, args):
        c = Controller.getInstance()
        if not self.__saved_state or not c.getPaused():
            return False

        self.__steps = [
            TestPausedStep("TEST_IF_NOT_PAUSED", True, ["PAUSE_PRINT"]),
            WaitForQueueToBeEmptyStep("WAITING_1"),
        ]

        for hotend_nr in range(c.getPropertyValue("hotend_count")):
            self.__steps.append(SetPropertyStep("START_HEATUP_HOTEND_{hotend_nr}".format(hotend_nr=hotend_nr),
                "printer.head.0.hotend.{hotend_nr}".format(hotend_nr=hotend_nr), "pre_tune_target_temperature",
                self.__saved_state.get("T{hotend_nr}".format(hotend_nr=hotend_nr))))
        # new loop because we want the temperature to be set simultaneously so we don't have to wait on both nozzles heating up one after the other.
        for hotend_nr in range(c.getPropertyValue("hotend_count")):
            self.__steps.append(OtherProcedureStep("HEATUP_HOTEND_WAIT_{hotend_nr}".format(hotend_nr=hotend_nr),
                "SET_HOTEND_TEMPERATURE_WAIT", {"hotend_nr": hotend_nr, "target": self.__saved_state.get("T{hotend_nr}".format(hotend_nr=hotend_nr))}))

        x = {"X":"x","Y":"y"}
        self.__steps.append(GotoPositionStep("RETUNING_NO_Z", **{x[key]: self.__saved_state[key] for key in x}))
        x.update({"Z":"z"})
        self.__steps.append(GotoPositionStep("RETUNING", **{x[key]: self.__saved_state[key] for key in x}))

        self.__steps = self.__steps + [
            QueueRawRelativeGCodeStep("UNRETRACT", "G1 E10 F900"),
            WaitForQueueToBeEmptyStep("WAITING_2"),
            ResumeStep("RESUMING", self)
        ]

        for i, step in enumerate(self.__steps):
            if i != 0:
                self.__steps[i-1].setNextStep(step)
            else:
                self.setFirstStep(step)

        return True

    ## This function takes the state saved by the PausePrintProcedure and stores it here for when the print is resumed.
    # This function is only called by the pauseStep and the resumeStep to set and reset the printer state respectively.
    # @param values A dictionary containing the state of the printer at the time of pausing, or an empty dictionary.
    def setSavedState(self, values):
        self.__saved_state = values

    ## This function returns the printer state saved by the pause print procedure for use in the ResumeStep.
    # @return A dictionary containing the state of the printer at the time of pausing, or an empty dictionary.
    def getSavedState(self):
        return self.__saved_state
