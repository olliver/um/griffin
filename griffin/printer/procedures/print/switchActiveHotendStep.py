from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
from griffin.printer.faultHandler import FaultHandler, FaultCode
import math
import logging

log = logging.getLogger(__name__.split(".")[-1])

# This function will move the nozzle selection switch on the print head cartridge to activate
# the selected hotend. The move is executed in a controlled manner with compensation for the
# filament movement as the nozzle moves up/down.
# For situations where the current nozzle position is unknown it is possible to specify a 'reset'
# parameter which uses another movement to flip the switch to nozzle 1 position, but without
# filament position correction. Lacking this compensation is bad, but should happen sparingly as
# normal program flow always ends with nozzle 1 selected.
#
# Parameter 'target' will result in the following behavior:
#    0 = Switch to nozzle 0
#    1 = Switch to nozzle 1
#    'reset' = Will physically flip the switch to nozzle 1 position and marks nozzle 1 as active. However,
#              material position is not corrected so use this sparingly.
#    None = default value, will throw an exception.
class SwitchActiveHotendStep(ProcedureStep):
    _switch_model = [
        #[X, Y, extrude, travel, direction]           (execute when this == target (None is both directions))
        [ 95.6032, 79.79860, False, True, None],    # move to font of switching bay.
        [105.6032, 79.79860, True, False, None],    # start (or end) of switching arc.
        [104.9953, 80.73026, True, False, None],
        [104.4496, 81.69967, True, False, None],
        [103.9685, 82.70271, True, False, None],
        [103.5541, 83.73505, True, False, None],
        [103.2080, 84.79230, True, False, None],
        [102.9318, 85.86993, True, False, None],
        [102.7267, 86.96330, True, False, None],
        [102.5935, 88.06775, True, False, None],
        [102.5329, 89.17854, True, False, None],
        [102.5450, 90.29092, True, False, None],
        [102.6299, 91.40013, True, False, None],
        [102.7871, 92.50142, True, False, None],
        [103.0160, 93.59006, True, False, None],
        [103.3156, 94.66139, True, False, None],
        [103.6847, 95.71085, True, False, None],
        [104.1216, 96.73391, True, False, None],    # end (or start) of switching arc.
        # only use these in 1 -> 0
        [104.1216, 98.73391, False, False, 0],      # added linear continuation by hand to push the lever just a bit further.
        [094.1216, 98.73391, False, True,  0],      # move to front of switching bay.
        # only use this in 0 -> 1
        [ 94.1216, 96.73391, False, True,  1]]      # move to front of switching bay.
    _switch_model_path_length = 17.799160423823455  # in mm

    def __init__(self, key, target = None):
        super().__init__(key)
        self._prev = [None, None]
        self._off = [None, None]
        self._target = target
        self._c = Controller.getInstance()

    def setTarget(self, target):
        self._target = target

    def run(self):
        assert self._target is not None

        # When we don't have more then 1 hotend, we do not have any reason to switch between hotends.
        if self._c.getPropertyValue("hotend_count") < 2:
            self._c.setActiveHotend(0)
            return

        self._off = self._c.getPreference("SWITCH_OFFSET")
        if self._off is None:
            FaultHandler.critical("The switching bay setting is not calibrated", FaultCode.SWITCHING_CALIBRATION)
            log.error("The switching bay setting is not calibrated")
            raise ValueError("The switching bay setting is not calibrated")

        inverse_flow = 1 / (self._c.getPropertyValue("extrusion_amount_modifier") / 100)
        inverse_speed = 1 / (self._c.getPropertyValue("movement_speed_modifier") / 100)
        nozzle_lift_extrude_difference = self._c.getPropertyValue("nozzle_lifting_filament_movement_amount")
        travel_speed = self._c.getPropertyValue("default_travel_speed") * 60.0 * inverse_speed
        switch_speed = self._c.getPropertyValue("hotend_switching_speed") * 60.0 * inverse_speed
        extrude_min_temp = self._c.getPropertyValue("default_minimal_extrusion_temperature")
        active_hotend = self._c.getActiveHotend()

        if active_hotend == self._target:
            # already done, since that's the side this switches to
            return

        if active_hotend is None or self._target == "reset":
            # x, y values taken from switching model
            self.__queueGCodeFromValue( 94.1216 - 1, 96.73391 + 16, travel_speed)
            self.__queueGCodeFromValue(104.1216 - 1, 96.73391 + 16, switch_speed)
            self.__queueGCodeFromValue(104.1216 - 1, 96.73391 +  5, switch_speed)
            self.__queueGCodeFromValue( 94.1216 - 1, 96.73391 +  5, travel_speed)
            self._c.queueRawGCode("T1 F5000")   # Some Marlin magic. Random F parameter causes Marlin to move to the new nozzle offset position immediately.
            self._c.setActiveHotend(1)

            # Wait till the moves are done before the unabortable flag is released.
            self._c.waitForQueueToBeEmpty(wait_for_motion_controller=True)
            return
        # retrieve other hotend's E value so a continuous E can be maintained (kind of)
        e = 0
        if active_hotend is not None:
            e = self._c.getPosition("E" + str(active_hotend))

        mm = nozzle_lift_extrude_difference * inverse_flow
        epmm = mm/self._switch_model_path_length # mm Extrusion Per mm xy move
        if self._target == 0:
            epmm *= -1 # move filament up in the 1 -> 0 direction since the movable nozzle(1) is moved up in that case.
        self._prev = [None, None]

        if self._target == 1:
            # T1 now so all moves happen with T1 offsets
            self._c.queueRawGCode("T1 F5000")   # Some Marlin magic. Random F parameter causes Marlin to move to the new nozzle offset position immediately.
            self._c.setActiveHotend(1)
        # T0 later so all moves happen with T1 offsets

        self._c.queueRawGCode("M83") # relative E movements <- this borks up the position tracking completely, so no more tracking the posion of the e axis in here!
        self._c.queueRawGCode("M302 S0") # even when cold

        switch_moves = self._switch_model
        if self._target == 1:
            switch_moves = reversed(switch_moves)

        for (x, y, extrude, travel, direction) in switch_moves:
            if direction == None or direction == self._target:
                self.__queueGCodeFromValue(x, y, travel_speed if travel else switch_speed, epmm if extrude else None)

        # added this wait because otherwise the M commands which aren't queued by marlin are executed and could have effect on movements yet to be completed.
        self._c.waitForQueueToBeEmpty(wait_for_motion_controller=True)
        self._c.queueRawGCode("M82") # back to absolute E movements
        self._c.queueRawGCode("M302 S%d" % extrude_min_temp) # safety back on

        # T0 now so all moves happen with T1 offsets?
        if self._target == 0:
            self._c.queueRawGCode("T0 F5000")   # Some Marlin magic. Random F parameter causes Marlin to move to the new nozzle offset position immediately.
            self._c.setActiveHotend(0)
        
        self._c.queueGCode("G92 E%f" % e ) # no flow compensation because this is just the current value we put back.

        # Because Marlin does not know about separate E axis, we need to inform it about the position of our filament relative to the E position.
        self._c.queueGCode("G93 E%f" % self._c.getGCodeProcessor().getRetractedLengthForHotend(self._target))

        # do this so the moves are done before the unabortable flag is released.
        self._c.waitForQueueToBeEmpty(wait_for_motion_controller=True)

    def __calcLen(self, x, y):
        ret = 0
        if self._prev[0] is not None:
            ret = math.sqrt((self._prev[0] - x) ** 2 + (self._prev[1] - y) ** 2)
        self._prev[0] = x
        self._prev[1] = y
        return ret

    # Convert ASCII data into queued gcode.
    def __queueGCodeFromValue(self, x, y, s=2000, e=None):
        if e is not None:
            e *= self.__calcLen(x, y)
            self._c.queueRawGCode("G1 X%f Y%f E%f F%d" % (x + self._off[0], y + self._off[1], e, s), track_e=False)
        else:
            self._c.queueRawGCode("G1 X%f Y%f F%d" % (x + self._off[0], y + self._off[1], s))
