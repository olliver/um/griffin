from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller
import logging

log = logging.getLogger(__name__.split(".")[-1])


## This step does the final resume action of setting the speed and un-pauses the controller.
class ResumeStep(ProcedureStep):
    def __init__(self, key, resume):
        super().__init__(key)
        self.__resume_procedure = resume

    def run(self):
        c = Controller.getInstance()

        # First set the speed to our default travel speed.
        c.sendRawGCode("G0 F{F:0.0f}".format(F=Controller.getInstance().getPropertyValue("default_travel_speed")))
        # Then move to our last known position.
        c.sendRawGCode("G0 X{X:f} Y{Y:f} Z{Z:f}".format(**self.__resume_procedure.getSavedState()))
        # Finally restore the speed to what it was before.
        c.sendRawGCode("G0 F{F:0.0f}".format(**self.__resume_procedure.getSavedState()))

        c.getPrintHeadController().getPropertyContainer().get("cooling_fan_speed").set(self.__resume_procedure.getSavedState().get("cooling_fan_speed", 0))
        c.setPaused(False)

        # reset the saved state
        self.__resume_procedure.setSavedState({})
