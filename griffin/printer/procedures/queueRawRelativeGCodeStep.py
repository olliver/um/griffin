from griffin.printer.procedures.procedureStep import ProcedureStep
from griffin.printer.controller import Controller

# Write a GCode command to the GCodeProcessor queue. Enforces the positions to be usd as relative
# positions. Useful for extrusion moves.
class QueueRawRelativeGCodeStep(ProcedureStep):
    def __init__(self, key, gcode):
        super().__init__(key)
        self._gcode = gcode

    def run(self):
        c = Controller.getInstance()
        c.queueRawGCode("G91")
        c.queueRawGCode(self._gcode)
        c.queueRawGCode("G90")
