import configparser
import os
import logging
import threading
import time

from griffin.factoryReset import FactoryReset
from griffin import dbusif
from griffin import signal
from griffin import timer
from griffin.printer import faultHandler
from .procedures.procedure import Procedure
from .properties.propertyContainer import PropertyContainer
from .properties.readOnlyProperty import ReadOnlyProperty
from .properties.constrainedProperty import ConstrainedProperty
from .drivers.marlin.marlinDriver import MarlinDriver
from .drivers.dummy.dummyDriver import DummyDriver
from .printerData import PrinterData
from griffin.preferences.registryFile import RegistryFile
from .properties.printer.printerStateProperty import PrinterStateProperty
from .properties.printer.printJobStateProperty import PrintJobStateProperty
from .material.materialManager import MaterialManager
from .printHeadController import PrintHeadController
from .gcodeProcessor import GCodeProcessor
from .heatableObject.heatablePrintbed import HeatablePrintbed


log = logging.getLogger(__name__.split(".")[-1])


class Controller(signal.SignalEmitter):
    # Location of system wide printer preferences
    __PREFERENCES_FILE = "printer_preferences.json"

    # These settings needs to be saved during a soft factory reset
    __SAVE_SETTINGS_ON_SOFT_RESET = ["NOZZLE_OFFSET", "SWITCH_OFFSET", "FACTORY_SETUP_WIZARD"]
    # These settings need to be set
    __ADD_SETTINGS_ON_SOFT_RESET = {"WELCOME_SETUP_WIZARD": False}
    __MAX_LOGICAL_POWER_USAGE = 4000 ## a 1 phase power supply could pull 16 amps at 230 V this can differ between region but it gives 3680 as maximum wattage which makes this a fair estimate where if you have to specify more you are probably doing something wrong.

    onProcedureStart = signal.Signal()
    onProcedureNextStep = signal.Signal()
    onProcedureFinished = signal.Signal()
    onPauseChanged = signal.Signal()
    onExtrusionAmountChanged = signal.Signal()

    def __init__(self):
        super().__init__()

        self._system_service = dbusif.RemoteObject("system")
        self.__settings = RegistryFile(self.__PREFERENCES_FILE)
        self._procedures = {}
        self.__property_container = PropertyContainer()
        self._print_head_controller = None
        self._material_manager = None
        self._running_event = None
        self._paused = False

        self.__heated_bed_object = None

        self.__active_hotend = None

        self.__gcode_processor = None

        self._abortable = threading.Event()
        self._abortable.set()

        faultHandler.FaultHandler.getInstance().onNewError.connect(self._onNewError)

    ## @brief Implementation of the factory reset callback
    #  @param reset_type Specified which type of reset is requested
    def resetToFactorySettings(self, reset_type):
        log.info("Factory Reset executing...")
        self.__settings.stop()

        if reset_type == FactoryReset.FACTORY_RESET_KEYWORD_SOFT_DESTROY:
            self.__settings.backupAndSetup(self.__SAVE_SETTINGS_ON_SOFT_RESET, self.__ADD_SETTINGS_ON_SOFT_RESET)
        else:
            self.__settings.erase()

        # Wipe out nozzle settings
        for index in range(0, self.getPropertyValue("hotend_count")):
            self._print_head_controller.getHotendSlot(index).resetSettings()

    # Initialized is called after __init__, so the Controller.getInstance internal instance member is properly set when creating the rest of the objects.
    def initialize(self):
        # _parseConfig will parse the machine configuration json, and create the properties that the system needs.
        self.__parseConfigPhase1()

        # Create the gcode processor as soon as we know the basic properties of our printer.
        self.__gcode_processor = GCodeProcessor(self, self._driver)

        self._running_event = threading.Event()
        self._running_event.set()  # Controller starts not paused

        self._print_head_controller = PrintHeadController(self, self.__settings.getAsRegistry("NOZZLE_OFFSET"))
        self.__gcode_processor.initialize() # needs the PrintHeadController
        self._material_manager = MaterialManager(self)
        self._material_manager.initialize()
        self._print_head_controller.start()

        self.onProcedureStart.connect(self._procedureChanged)
        self.onProcedureFinished.connect(self._procedureChanged)

        self.__active_hotend = None

        self.__heated_bed_object = HeatablePrintbed(self, self.__settings.getAsRegistry("BED"))

        self.__parseConfigPhase2()

        # Add our dynamic properties, which depend on the rest of the service to be created.
        self.__property_container.addProperty(PrinterStateProperty(self, "state"))
        self.__property_container.addProperty(PrintJobStateProperty(self, "job_state"))

        # Global extrusion and speed modifiers.
        self.__property_container.addProperty(ConstrainedProperty("movement_speed_modifier", 100.0, lambda value: 10.0 <= value <= 1000.0))
        self.__gcode_processor.addGCodeMapping("M220", lambda gcode_line: self.setPropertyValue("movement_speed_modifier", gcode_line.getValue("S", default=100.0)))

        self.__property_container.addProperty(ConstrainedProperty("extrusion_amount_modifier", 100.0, lambda value: 10.0 <= value <= 1000.0))
        self.__gcode_processor.addGCodeMapping("M221", lambda gcode_line: self.setPropertyValue("extrusion_amount_modifier", gcode_line.getValue("S", default=100.0)))

        # Properties for motion control
        self.__property_container.addProperty(ConstrainedProperty("acceleration_xzy", 3000.0, lambda value: 5.0 <= value))
        self.__property_container.addProperty(ConstrainedProperty("acceleration_e", 3000.0, lambda value: 5.0 <= value))

        self.__property_container.addProperty(ConstrainedProperty("jerk_xy", 20.0, lambda value: 0.01 <= value))
        self.__property_container.addProperty(ConstrainedProperty("jerk_z", 0.4, lambda value: 0.01 <= value))
        self.__property_container.addProperty(ConstrainedProperty("jerk_e", 5.0, lambda value: 0.01 <= value))

        self.__property_container.addProperty(ConstrainedProperty("max_speed_x", 300.0, lambda value: 5.0 <= value))
        self.__property_container.addProperty(ConstrainedProperty("max_speed_y", 300.0, lambda value: 5.0 <= value))
        self.__property_container.addProperty(ConstrainedProperty("max_speed_z", 40.0, lambda value: 5.0 <= value))
        self.__property_container.addProperty(ConstrainedProperty("max_speed_e", 45.0, lambda value: 5.0 <= value))

        self.__property_container.addProperty(ConstrainedProperty("motor_current_xy", self.getPropertyValue("default_motor_current")["XY"], lambda value: 10.0 <= value <= 2000.0))
        self.__property_container.addProperty(ConstrainedProperty("motor_current_z", self.getPropertyValue("default_motor_current")["Z"], lambda value: 10.0 <= value <= 2000.0))
        self.__property_container.addProperty(ConstrainedProperty("motor_current_e", self.getPropertyValue("default_motor_current")["E"], lambda value: 10.0 <= value <= 2000.0))

        self.__property_container.addProperty(ConstrainedProperty("steps_per_mm_x", self.getPropertyValue("default_steps_per_mm")["X"], lambda value: 0.01 <= value))
        self.__property_container.addProperty(ConstrainedProperty("steps_per_mm_y", self.getPropertyValue("default_steps_per_mm")["Y"], lambda value: 0.01 <= value))
        self.__property_container.addProperty(ConstrainedProperty("steps_per_mm_z", self.getPropertyValue("default_steps_per_mm")["Z"], lambda value: 0.01 <= value))
        self.__property_container.addProperty(ConstrainedProperty("steps_per_mm_e", self.getPropertyValue("default_steps_per_mm")["E"], lambda value: 0.01 <= value))

        self.__property_container.addProperty(ConstrainedProperty("total_power_budget", self.getPropertyValue("default_power_budget_management")["total_power_budget"], lambda value: 0 <= value <= self.__MAX_LOGICAL_POWER_USAGE))
        self.__property_container.addProperty(ConstrainedProperty("idle_power_consumption", self.getPropertyValue("default_power_budget_management")["idle_power_consumption"], lambda value: 0 <= value <= self.__MAX_LOGICAL_POWER_USAGE))

        def __handleM203(gcode_line):
            value = gcode_line.getValue("X", default=None)
            if value is not None:
                self.getPropertyContainer().get("max_speed_x").set(value)
            value = gcode_line.getValue("Y", default=None)
            if value is not None:
                self.getPropertyContainer().get("max_speed_y").set(value)
            value = gcode_line.getValue("Z", default=None)
            if value is not None:
                self.getPropertyContainer().get("max_speed_z").set(value)
            value = gcode_line.getValue("E", default=None)
            if value is not None:
                self.getPropertyContainer().get("max_speed_e").set(value)
        self.__gcode_processor.addGCodeMapping("M203", __handleM203)

        def __handleM204(gcode_line):
            value = gcode_line.getValue("S", default=None)
            if value is not None:
                self.getPropertyContainer().get("acceleration_xzy").set(value)
            value = gcode_line.getValue("T", default=None)
            if value is not None:
                self.getPropertyContainer().get("acceleration_e").set(value)
        self.__gcode_processor.addGCodeMapping("M204", __handleM204)

        def __handleM205(gcode_line):
            value = gcode_line.getValue("X", default=None)
            if value is not None:
                self.getPropertyContainer().get("jerk_xy").set(value)
            value = gcode_line.getValue("Z", default=None)
            if value is not None:
                self.getPropertyContainer().get("jerk_z").set(value)
            value = gcode_line.getValue("E", default=None)
            if value is not None:
                self.getPropertyContainer().get("jerk_e").set(value)
        self.__gcode_processor.addGCodeMapping("M205", __handleM205)

        def __handleM907(gcode_line):
            value = gcode_line.getValue("X", default=None)
            if value is not None:
                self.getPropertyContainer().get("motor_current_xy").set(value)
            value = gcode_line.getValue("Z", default=None)
            if value is not None:
                self.getPropertyContainer().get("motor_current_z").set(value)
            value = gcode_line.getValue("E", default=None)
            if value is not None:
                self.getPropertyContainer().get("motor_current_e").set(value)
        self.__gcode_processor.addGCodeMapping("M907", __handleM907)

        #"M142 Hx Sx Vx" handling for the casing leds.
        def __handleM142(gcode_line):
            hue = gcode_line.getValue("H", default=None)
            if hue is not None:
                dbusif.RemoteObject("led").setHue("STRIP", hue)
            saturation = gcode_line.getValue("S", default=None)
            if saturation is not None:
                dbusif.RemoteObject("led").setSaturation("STRIP", saturation)
            value = gcode_line.getValue("V", default=None)
            if value is not None:
                dbusif.RemoteObject("led").setBrightness("STRIP", value)
        self.__gcode_processor.addGCodeMapping("M142", __handleM142)

        self._driver.start()

    # Initial step of configuration parsing. This searches for the proper json file to parse and sets the properties specified in that json file.
    # This is done before the rest of the controller is initialized.
    def __parseConfigPhase1(self):
        cp = configparser.ConfigParser()
        cp.read("/etc/griffin.conf")

        if "printer" not in cp:
            log.warning("Section 'printer' missing in config file. Using internal defaults.")

        serial_port = cp.get("printer", "port", fallback="/dev/ttyS1")

        # Try to find our json configuration file.
        # Get the BOM and revision number from the system service.
        # First try a config file that is "bom-revision.json", if that fails, try "bom.json", if that fails, use a fallback.
        bom_number, revision_number = self._system_service.getMachineBOM()
        json_filenames = [
            "%d-%d.json" % (bom_number, revision_number),
            "%d.json" % (bom_number),

            # Fallback, if the bom specific json file fails to load, load the general jedi.json
            "jedi.json",
            # Final fallback, if there is an override for the jedi.json that fails to load, load the original jedi.json
            "/usr/share/griffin/griffin/machines/jedi.json"
        ]

        data = None
        for json_filename in json_filenames:
            self._config_filename = json_filename
            try:
                data = PrinterData(self._config_filename)
                break
            except FileNotFoundError:
                pass
        for property_key, property_value in data.getMachineProperties().items():
            self.__property_container.addProperty(ReadOnlyProperty(property_key, property_value))

        if cp.getboolean("printer", "dummy", fallback=False):
            self._driver = DummyDriver(self, serial_port)
        else:
            self._driver = MarlinDriver(self, serial_port)

    # Second phase of configuration parsing. This reads the same json file again as in the first pass, and creates the procedures defined in the json file.
    #  This is done after the rest of the controller is initialized, as creating procedures depends on having certain objects available in the controller.
    def __parseConfigPhase2(self):
        data = PrinterData(self._config_filename)

        for procedure in data.getProcedures():
            self.addProcedure(procedure)

        procedure_mapping = data.getCodeToProcedureMapping()
        # Convert the procedure mapping data, in the data the "procedure" key is a string from the json file, map this to the actual procedure.
        for key, data in procedure_mapping.items():
            procedure = self.getProcedure(data["procedure_key"])
            if procedure is None:
                log.error("No procedure found for GCode procedure mapping: %s", key)
            else:
                self.__gcode_processor.addGCodeMapping(key, self.__createGCodeProcedureMapping(procedure, data))

    ## Create a mapping function to a procedure call.
    #  This captures the procedure and data objects in a lambda function in the form of a closure.
    #  @param procedure: Procedure object.
    #  @param data: dictionary containing information about the parameters that the procedure needs. See __runGCodeProcedureMapping for details
    #  @return function that requires a gcode line and will run the proper procedure mapping.
    def __createGCodeProcedureMapping(self, procedure, data):
        return lambda gcode_line: self.__runGCodeProcedureMapping(gcode_line, procedure, data)

    ## Run a procedure when a certain G, M or T code is encountered.
    #  @param gcode_line: A GCodeLine object from the GCodeProcessor
    #  @param procedure: A procedure object which needs to run when this mapping is handled
    #  @param mapping_data: A dict containing "arguments" and "static_arguments" as keys. Which are both dictionaries.
    #                       These dictionaries are used to fill the arguments towards the procedure.
    def __runGCodeProcedureMapping(self, gcode_line, procedure, mapping_data):
        args = {}
        for key, target in mapping_data["arguments"].items():
            if key == "ALL":
                args[target] = gcode_line.getAllParameters()
            else:
                args[target] = gcode_line.getValue(key, default=None, return_type=float)
        args.update(mapping_data["static_arguments"])

        # Start the given procedure and wait till it is finished.
        procedure.setup(args)
        procedure.start()
        procedure.join()

    # Called on Procedure Start/Stop as then the number of active + abort disabled procedures can change.
    # @param dummy_procedure Unused but is passed by the emit of the onProcedureFinished and onProcedureStart signals.
    # @param dummy_step Unused but is passed by the emit of the onProcedureStart signal.
    def _procedureChanged(self, dummy_procedure=None, dummy_step=None):
        if len([None for procedure in self.getAllProcedures() if procedure.isActive() and procedure.isAbortDisabled()]) > 0:
            self._abortable.clear()
        else:
            self._abortable.set()

    # Waits until all abortable procedures are finished
    def waitUntilAllowedToPause(self):
        # during the unabortable function.
        self._abortable.wait()

    def setPaused(self, paused):
        log.info("Setting paused state to: %s", paused)
        changed = paused != self._paused

        self._paused = paused
        if self._paused:
            self._running_event.clear()
        else:
            self._running_event.set()

        if changed:
            self.onPauseChanged.emit(paused)

    def getPaused(self):
        return self._paused

    def addProcedure(self, procedure):
        assert(isinstance(procedure, Procedure))
        assert(procedure.getKey() not in self._procedures)
        self._procedures[procedure.getKey()] = procedure

        procedure.onStart.connect(self.onProcedureStart)
        procedure.onNextStep.connect(self.onProcedureNextStep)
        procedure.onFinished.connect(self.onProcedureFinished)

    def getProcedure(self, key):
        if key not in self._procedures:
            return None
        return self._procedures[key]

    def getAllProcedures(self):
        return self._procedures.values()

    ## @brief Gets a list of procedures that are currently active (procedure and step)
    #  @return The list of active procedures
    def getActiveProcedures(self):
        procedures = self._procedures.values()
        return [(p.getKey(), p.getActiveStep().getKey()) for p in procedures if p.isActive()]

    ## Try to start a procedure.
    #  @param key: The unique name of the procedure.
    #  @param parameters: A dictionary of parameters that are passed to the setup of the procedure.
    #  @return bool: True if the procedure was successfully started, False if an error happened.
    def startProcedure(self, key, parameters):
        log.info("Request to start procedure: %s", key)
        try:
            procedure = self.getProcedure(key)
            if procedure is None:
                log.warning("Procedure %s does not exist!", key)
                return False
            if not procedure.canBeExecuted():
                log.warning("Procedure %s can not be executed!", key)
                return False
            if not procedure.setup(parameters):
                log.warning("Procedure %s parameters could not be set!", key)
                return False
            procedure.start()
        except Exception:
            log.exception("Exception in procedure start")
            return False
        return True


    def getPropertyContainer(self):
        return self.__property_container

    # Get the value of a property.
    # If a property does not exist, this function throws a KeyError
    def getPropertyValue(self, key):
        return self.__property_container.get(key).get()

    # Set the value of a property
    # This function returns if the set was succesful or not.
    # If a property does not exist, this function throws a KeyError
    def setPropertyValue(self, key, value):
        return self.__property_container.setPropertyValue(key, value)

    # Return True when a property exists.
    def hasProperty(self, key):
        return self.__property_container.hasKey(key)

    # Send raw gcode to the printer. Note that this ignores any paused state.
    def sendRawGCode(self, gcode_line, wait=False):
        # track position in maintenance mode (assumes no other moves are currently in buffer.
        return self.__gcode_processor.sendInstantRawGCode(gcode_line, wait)

    # Send raw gcode to the printer. Note that this ignores any paused state.
    # @param gcode_line: A string containing GCode that needs to be send to the driver.
    # @return bool: True if the command was queued for sending as soon as possible. False if the buffers where full or the driver is not connected yet.
    def sendRawGCodeNoBlock(self, gcode_line):
        # track position in maintenance mode (assumes no other moves are currently in buffer.
        return self.__gcode_processor.sendInstantRawGCodeNoBlock(gcode_line)

    ## This function is mainly used to queue gcode read from a gcode-file.
    # gcode queued in this way passes through the normal opinicus queue first and is then sent to marlin.
    # @param gcode_line the line of gcode to put in the queue
    def queueGCode(self, gcode_line):
        # only pause if no un-abortable(aka un-pausable) procedures are running.
        if self._abortable.is_set():
            self._running_event.wait()  # Block if the controller is paused

        self.__gcode_processor.processAndQueueGCode(gcode_line)

    ## This function is used by maintenance and pre/post print procedures/steps to send gcode mainly to avoid our
    # own mappings that could create a set in gcode loop.
    # gcode queued in this way passes through the normal opinicus queue first and is then sent to marlin.
    # @param gcode_line the line of gcode to put in the queue
    # @param track_e Is an optional parameter that can be used to plan motions that are then not counted towards
    # the e position/value this is useful for the switchActiveHotend for instance.
    def queueRawGCode(self, gcode_line, track_e=True):
        self.__gcode_processor.queueRawGCode(gcode_line, track_e=track_e)

    def abortQueue(self):
        # wait until no more un-abortable procedures are running.
        self._abortable.wait()  # since this is not happening from a dbus call...
        # only then toss away any queued commands.
        self._driver.abort()

    def setActiveHotend(self, hotend_nr):
        self.__active_hotend = hotend_nr

    def getActiveHotend(self):
        return self.__active_hotend

    def getFilamentUsedAndReset(self, hotend_nr):
        return self.__gcode_processor.getAndResetFilamentUsageForHotend(hotend_nr)

    def setRetractedLength(self, value):
        e = self.getActiveHotend()
        if e is None:
            e = 0   # default e value if no nozzle switch has happened before.
        self.__gcode_processor.setRetractedLength(e, value)
        # Signal Marlin that we know how far material is from the hotend.
        self.queueGCode("G93 E%g" % (value))

    # Wait for the transmit queue to the motion controller to be empty.
    # @param wait_for_motion_controller is an optional parameter defining that we wait until the motion controller
    #        has finished all moves. Has the same effect as specifying max_planned_motions=0.
    # @param max_planned_motions is an optional parameter defining that we wait until fewer than the specified number
    #        of motions are queued by the motion planner. This parameter does nothing when wait_for_motion_controller
    #        is specified.
    #        A value of 0 means that we effectively wait till the motion controller has finished all physical moves.
    #        With a None value we wait till the communication buffers are flushed, but the motion controller will
    #        continue to run all planned moves.
    def waitForQueueToBeEmpty(self, wait_for_motion_controller=False, max_planned_motions=None):
        assert not(wait_for_motion_controller == True and max_planned_motions is not None), "Not allowed to specify both parameters"

        if wait_for_motion_controller:
            max_planned_motions = 0
        self._driver.waitForQueueToBeEmpty(max_planned_motions)

    def getMaterialManager(self):
        return self._material_manager

    # returns the keys to use in getPosition, no key can also be used for a dictionary with all axes.
    #
    # Also apparently the plural of axis is axes...
    def getAxes(self):
        return self.__gcode_processor.getAxes()

    # Returns an array with [x, y, z] position, and potentially the speed param.
    # @param f If true the speed parameter (aka F) is included in the return value.
    def getPositionArray(self, f=False):
        return self.__gcode_processor.getPositionArray(f)

    # returns position either as dictionary if no axis argument is given or as value belonging to the axis requested.
    def getPosition(self, axis=None):
        return self.__gcode_processor.getPosition(axis)

    def getHotendRetractedLength(self, hotend_nr):
        return self.__gcode_processor.getRetractedLengthForHotend(hotend_nr)

    def getGCodeProcessor(self):
        return self.__gcode_processor

    def getBed(self):
        return self.__heated_bed_object

    # TODO This opens up the controller for all kinds of setting changes that the controller no longer has any influence on! (EM-429)
    def getPreference(self, key):
        return self.__settings.get(key)

    # TODO This opens up the controller for all kinds of setting changes that the controller no longer has any influence on! (EM-429)
    def setPreference(self, key, value):
        return self.__settings.set(key, value)

    def getDriver(self):
        return self._driver

    def debugGetDriverInfo(self):
        return self._driver.debugGetInfo()

    def getPrintHeadController(self):
        return self._print_head_controller

    ## @brief Checks for the build plate offset data
    #  @return True when all the nozzles have a known Z offset relative to the print bed.
    def hasBuildPlateLevelData(self):
        hotend_count = self.getPropertyValue("hotend_count")
        nozzle_offsets = [self.getPrintHeadController().getHotendSlot(i).getPropertyValue("z_height") for i in range(hotend_count)]
        tested = [offset is not None and offset != "" for offset in nozzle_offsets]
        return tested == [True] * hotend_count

    ## @brief Checks for the xy offset data
    #  @return True when the 2 key settings (X and Y) are stored for the nozzles.
    def hasXYOffsetData(self):
        nozzle_offset = self.getPrintHeadController().getHotendSlot(1).getPropertyAsObject("x_offset")

        # use timestamp to see if it is set or if it is the default value...
        if nozzle_offset.getLastChanged() is None or nozzle_offset.get() == "":
            return False

        return True

    # TODO: obsolete - system service should be loaded using dbusif.RemoteObject (EM-708)
    def getSystemService(self):
        return self._system_service

    ## Called when the fault handler sees a new error.
    #  This is called as soon as the new error is raised in the system, not when the error is brought up as top level error.
    #  The onError signal should be used for that
    #  @param fault_object: A faultHandler.Fault object that contains the information about this fault.
    def _onNewError(self, fault_object):
        error_level = fault_object.getLevel()
        error_code = fault_object.getCode()

        if error_level <= faultHandler.FaultLevel.ERROR:
            print_procedure = self.getProcedure("PRINT")
            if print_procedure.isActive():
                print_procedure.receiveMessage("ABORT")
                # kill all movement from the printer, if not already stopped.
                timer.Timer("send gcode", 0, self.sendRawGCode, "M998").start()

        elif error_code == faultHandler.FaultCode.FILAMENT_FLOW:
            if self.__shouldRaiseFilamentFlowError(fault_object):
                log.warning("Filament flow error: %s", repr(fault_object.getData()))
                self.startProcedure("PAUSE_PRINT", {})
            else:
                # Block propagation of the flow warning/error while printing.
                # Since this might perfectly normal for instance during the load filament procedure.
                faultHandler.FaultHandler.clear(error_level, error_code)

    ## Check if a filament flow error needs to be raised.
    #  We check if we are actually printing.
    #  @param fault_object: A faultHandler.Fault object that contains the information about the filament flow error. Used to get the hotend_nr.
    #  @return bool: True if a fault should be raised on this flow error, False if not.
    def __shouldRaiseFilamentFlowError(self, fault_object):
        # If we don't have the filament flow sensor feature, never pause.
        # (Just a preventive measure, if marlin hasn't properly disabled flow measurements and reports errors, we do not want the system to pause if this feature was disabled)
        if not self.getPropertyValue("feature_filament_flow_sensor"):
            return False

        print_procedure = self.getProcedure("PRINT")
        step = print_procedure.getActiveStep()
        # Check if a print is running, no print is no flow sensor error.
        if not print_procedure.isActive() or step is None or step.getKey() != "PRINTING":
            return False
        # Ignore first layer (due to issues with back pressure, inaccuracy in the bed and active leveling settings)
        if self.getPosition("Z") <= 0.5:
            return False
        # Check if the flow sensor is enabled for this hotend, if it's not enabled we do not want flow errors for this hotend.
        hotend_nr = fault_object.getData().get("hotend_nr", None)
        if hotend_nr is None:
            log.warning("Got a flow sensor error without a hotend nr, assuming current active hotend.")
            hotend_nr = self.getActiveHotend()
        hotend_slot = self.getPrintHeadController().getHotendSlot(hotend_nr)
        if not hotend_slot.getPropertyValue("enable_flow_errors"):
            return False
        if hotend_slot.getPropertyValue("ignore_flow_errors_until") >= time.time():
            return False
        return True

    _instance = None

    @classmethod
    def getInstance(cls):
        if cls._instance is None:
            cls._instance = Controller()
            cls._instance.initialize()
        return cls._instance
