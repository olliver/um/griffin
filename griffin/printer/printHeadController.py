import logging
import time
import re
from .hotendCartridgeManager.hotendCartridgeManager import HotendCartridgeManager
from .heatableObject.heatableHotend import HeatableHotendSlot
from .properties.propertyContainer import PropertyContainer
from .properties.constrainedProperty import ConstrainedProperty
from .translationXYNozzleOffset import TranslationXYNozzleOffset

log = logging.getLogger(__name__.split(".")[-1])

## This class aims to be an abstract representation of the printhead and have all object related to the printhead organized here.
class PrintHeadController():

    ## Construct a PrintHead Controller object.
    # @param controller The print controller object.
    # @param nozzle_offset_registry A subset of the normal registry where nozzle offsets(x,y and Z) are stored.
    def __init__(self, controller, nozzle_offset_registry):
        self.__controller = controller
        self.__property_container = PropertyContainer()
        self.__hotend_cartridge_manager = HotendCartridgeManager(self.__controller)
        self.__hotend_count = self.__controller.getPropertyValue("hotend_count")
        self.__hotend_slots = []
        self.__updatePreferencesForCompatibility(nozzle_offset_registry)
        for index in range(0, self.__hotend_count):
            self.__hotend_slots.append(HeatableHotendSlot(controller, self, nozzle_offset_registry, index))

        # Handle the M104 GCode from the print head controller. As this is a single GCode to control different hotend slots target temperatures.
        controller.getGCodeProcessor().addGCodeMapping("M104", self.__handleTemperatureSetCommand)

        # Add mappings to gcodes that we need to handle as properties
        self.__property_container.addProperty(ConstrainedProperty("cooling_fan_speed", 0.0, lambda value: 0.0 <= value <= 100.0))
        controller.getGCodeProcessor().addGCodeMapping("M106", lambda gcode_line: self.getPropertyContainer().get("cooling_fan_speed").set(gcode_line.getValue("S", default=255) * 100.0 / 255.0))
        controller.getGCodeProcessor().addGCodeMapping("M107", lambda gcode_line: self.getPropertyContainer().get("cooling_fan_speed").set(0.0))

    def __updatePreferencesForCompatibility(self, nozzle_offset_registry):
        # TODO: EM-1154 remove this code after a few releases as it only serves backwards configuration file compatibility!
        for key in nozzle_offset_registry.getAll().keys():
            # regex explained: "<12 hex characters>-<12 hex characters>" or "<serial>-<serial>"
            if re.match(r"^[0-9a-f]{12}-[0-9a-f]{12}$", key):
                new_key = key + "-1"
                offset = nozzle_offset_registry.getAsRegistry(key)
                x = offset.getAsFloat("X", None)
                y = offset.getAsFloat("Y", None)
                last_probe = offset.getAsInt("LAST_PROBE", time.time())
                if x is not None and y is not None and (nozzle_offset_registry.get(new_key, None) is None
                                              or nozzle_offset_registry.get(new_key, {}).get("x_offset", None) is None):
                    nozzle_offset_registry.set(new_key,
                        {
                            "x_offset_PROPERTY": {
                                "last_changed": last_probe,
                                "value": x,
                                "version": 0.1
                            },
                            "y_offset_PROPERTY": {
                                "last_changed": last_probe,
                                "value": -y, # this has flipped in marlin!
                                "version": 0.1
                            }
                        })
                    log.info("new entry \"%s\":%s", new_key, nozzle_offset_registry.get(new_key))
                nozzle_offset_registry.delete(key)

    def getPropertyContainer(self):
        return self.__property_container

    def getPropertyValue(self, key):
        return self.__property_container.get(key).get()

    def setPropertyValue(self, key, value):
        return self.__property_container.get(key).set(value)

    def __handleTemperatureSetCommand(self, gcode_line):
        t_value = gcode_line.getValue("T", default=None, return_type=int)
        if t_value is None:
            t_value = self.__controller.getActiveHotend()
        if 0 <= t_value < len(self.__hotend_slots):
            s_value = gcode_line.getValue("S", default=None)
            if s_value is not None:
                self.__hotend_slots[t_value].setPropertyValue("pre_tune_target_temperature", s_value)

    ## Starts the hotend cartridge manager
    def start(self):
        self.__hotend_cartridge_manager.start()

    ## Retrieve a HeatableHotend object as a hotend slot.
    def getHotendSlot(self, index):
        return self.__hotend_slots[index]

    ## Gets the combination of the hotend serial numbers.
    # @param the index of the hotend, the combo should only be used for storing things relative to the reference nozzle (0)
    def getNozzleCombo(self, index):
        assert index != 0, "nozzlecombo is already relative to position 0"
        nozzle_combo = []
        for i in [0, index]:
            serial = None
            if self.__hotend_cartridge_manager.isPresent(i):
                serial = self.__hotend_cartridge_manager.getSerial(i)
            if serial is None:
                serial = "None"
            nozzle_combo.append(serial)

        return "-".join(nozzle_combo) + "-" + str(index) # cut off the b'data' and keep only the data as a string

    ## @brief Gets the nozzle indexes for x and y offset
    #  @param index The index of the nozzle to get the offsetIndexes from.
    #  @return Returns the indexes as a dictionary; defaults to 0-index for x and y
    def getNozzleOffsetIndexes(self, index=1):
        slot = self.getHotendSlot(index)
        x = slot.getPropertyValue("x_offset")
        y = slot.getPropertyValue("y_offset")
        if x == "" or y == "":
            log.warning("No nozzle offset indexes found, returning default { 0, 0 } as indexes")
            return {"X": "0", "Y": "0"}
        nozzle_offset_indexes = TranslationXYNozzleOffset.getIndex(X=x, Y=y)
        log.info("Returning nozzle offset indexes: %r", nozzle_offset_indexes)
        return nozzle_offset_indexes

    ## Gets the hotend cartridge manager
    def getHotendCartridgeManager(self):
        return self.__hotend_cartridge_manager

    ## Gets a hotend property
    def getHotendCartridgeProperty(self, hotend_nr, key):
        if hotend_nr >= self.__controller.getPropertyValue("hotend_count") or hotend_nr < 0:
            raise ValueError("hotend_nr out of bounds.")
        return self.__hotend_cartridge_manager.getValue(hotend_nr, key)

    ## Sets a hotend property
    def setHotendCartridgeProperty(self, hotend_nr, key, value):
        if hotend_nr >= self.__controller.getPropertyValue("hotend_count") or hotend_nr < 0:
            raise ValueError("hotend_nr out of bounds.")
        self.__hotend_cartridge_manager.setValue(hotend_nr, key, value)
