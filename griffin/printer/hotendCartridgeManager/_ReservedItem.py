from ._RegisterItem import RegisterItem


##
# This is a RegisterItem type and is meant to access a specific type of data in the hotend eeprom.
#
# This class is meant to be a dummy item, it is not meant to store data but just meant to indicate data is reserved for future use.
class ReservedItem(RegisterItem):
    ##
    # constructs a ReservedItem which is a type of RegisterItem
    # \param from_address the address at which the string starts.
    # \param to_with_address the address at which the string ends.
    # \param description all reagisters could use some description this is used for debugging mostly but is incuded for clarity
    def __init__(self, from_address, to_with_address):
        self._addr = from_address
        self._to   = to_with_address
        self._dsc  = "reserved for future use"
        self._unit = "reserved"
