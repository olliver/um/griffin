from ._RegisterItem import RegisterItem
from griffin.crc8 import Crc8

# This is a RegisterItem type and is meant to access a specific type of data in the hotend eeprom.
#
# This class calculates the crc8 sum over a range and reads the value start at the address, it compares these to return a boolean value when read.
# when written to with a None value this item will update the crc8 value at the address to be correct for the given range.
class CrcItem(RegisterItem):
    # constructs a CrcItem which is a type of RegisterItem
    # \param address to store the crc8 byte at.
    # \param over_address_range a tuple indicating the start and end byte range that are to be part of the crc8 sum.
    # \param description optionally an alternative description could be added, a default description is used instead if left out.
    def __init__(self, address, over_address_range, description = "8 bit cyclic redundancy checksum"):
        self._addr  = address
        self._range = over_address_range
        self._dsc   = description   # description for debugging
        self._unit  = "boolean"

    def __calculateCrc(self, data):
        return Crc8(data[self._range[0]:self._range[1]]).digest()[0]

    ##
    # Returns True when CRC matches.
    def getValueFromData(self, data):
        return self.__calculateCrc(data) == data[self._addr]

    ##
    # this function for this class will only except a None value as it is meant to be used as a prompt
    # to recalculate the checksum and not write the given value
    def setValueInData(self, data, value):
        if value is not None:
            raise ValueError("The value passed to the write of a CrcItem should always be None.")

        new_data = list(data)   # creates copy
        new_data[self._addr] = self.__calculateCrc(data)
        return bytes(new_data)

    def getPossibleValues(self):
        return [True, False]
