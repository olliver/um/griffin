from .hotend import Hotend
from ._NumericItem import NumericItem
from ._StringItem import StringItem
from ._ReservedItem import ReservedItem
from ._CrcItem import CrcItem
from ._EnumItem import EnumItem

# This class contains the register mapping of the first version of the eeprom spec.
# can be updated with minor (backwards compatible) updates.
#
# Names are defined here: https://docs.google.com/a/ultimaker.com/document/d/1p9vtjOUcOcHKuqVDtoJu5O9XHtzC1eLe-OhNx4nY3xY/edit?usp=sharing
#
#   legend:
#   #       is insert material
#   |_-/\   are used as lines
#   <- ->   are arrows...
#
#        /----------- filament_size
#    <---------->
# | #            # |
# | #            # |
# | #            # |
# | #            # |
# | #            # |
# | |            | |    |
# | |            | |    |
# | |            | |    |
# | |            | |    |
# | |            | |    |
# | |            | |    |- hot_zone_size
#  \ \          / /     |
#   \ \__    __/ /      |
#    \___|  |___/       |
#
#         <>----------- nozzle_size
#      <>
#       \------------ flat_size (internal size, not the external size).


class HotendV1(Hotend):
    _layout = {
        "static":
        {
            # the mask hex data is entered as string so the total size of the mask can be calculated by the lower class including the 0"s of the most significant bits.
            "major_version_number": NumericItem(0x00, "fc", "major EEPROM layout specification number, changed when the spec changes are non backwards compatible"),
            "minor_version_number": NumericItem(0x00, "03", "minor eeprom version number, changed when a change is purely additive"),
            "manufacturer_id":      StringItem( 0x01, 13, "manufacturer identification string"),
            "hotend_cartridge_id":  StringItem( 0x0e, 10, "hotend cartridge identification string assigned by manufacturer"),
            "pid_Kp":               NumericItem(0x18, "ffff", "Proportional coefficient", *Hotend._FLOATING_POINT_PRECISION_NR_OF_DIGITS[2]),
            "pid_Ki":               NumericItem(0x1a, "ffff", "Integral coefficient", *Hotend._FLOATING_POINT_PRECISION_NR_OF_DIGITS[2]),
            "pid_Kd":               NumericItem(0x1c, "ffff", "Derivative coefficient", *Hotend._FLOATING_POINT_PRECISION_NR_OF_DIGITS[2]),
            "hardware_revision":    NumericItem(0x1e, "ff", "hardware revision number"),
            "crc8_page_0":          CrcItem(    0x1f, (0x00, 0x1e + 1)),

            "nozzle_size":          NumericItem(0x20, "ff000000", "size of the 'opening' of the nozzle", (lambda x: x / 40.0), (lambda x: x * 40.0), "mm"),
            "hot_zone_size":        NumericItem(0x20, "00ffc000", "length until the insert or cool zone", *Hotend._FLOATING_POINT_PRECISION_NR_OF_DIGITS[1], unit="mm"),
            "maximum_temperature":  NumericItem(0x20, "00003ff0", "maximum operating temperature that should not be exceeded, stuff will break", unit = "degC"),
            "filament_size":        EnumItem(   0x20, "0000000f", "filament diameter", [ 2.85, 1.75 ], unit = "mm"),
            "angle":                NumericItem(0x24, "ff000000", "internal angle of the hot end leading up to the 'opening'", unit = "degrees"),
            "flat_size":            NumericItem(0x24, "00ff0000", "internal flat section before rising with the 'angle' at the 'opening'", *Hotend._FLOATING_POINT_PRECISION_NR_OF_DIGITS[1], unit="mm"),
            "insert_type":          EnumItem(   0x24, "0000ff00", "Type of nozzle insert", [ "teflon", "metal/none" ], unit = "string" ),
            "nominal_resistance":   NumericItem(0x26, "00fff800", "this cartridges's heater resistance", *Hotend._FLOATING_POINT_PRECISION_NR_OF_DIGITS[2], unit="Ohm"),
            "crc8_page_1":          CrcItem(    0x3f, (0x20, 0x3e + 1)),

            "last_material":        NumericItem(0x40, "ffffffffffffffffffffffffffffffff", "128bit material GUID of the last material used with this nozzle"),
            "material_extruded":    NumericItem(0x50, "ffffff00", "approximate accumulative amount of material extruded during printing", unit="cm"),
            "time_spend_hot":       NumericItem(0x52, "00ffffff", "approximate time spent above 65 degC", unit="minutes"),
            "max_exp_temperature":  NumericItem(0x56, "ffc0",     "maximum temperature exposed to this nozzle", unit="degC"),
            "crc8_page_2":          CrcItem(    0x5f, (0x40, 0x5e + 1)),

            "crc8_page_3":          CrcItem(    0x7f, (0x60, 0x7e + 1)),
        },
        "reserved":
        [
            ReservedItem(0x27, 0x3e),
            ReservedItem(0x58, 0x5e),
            ReservedItem(0x60, 0x7e)
        ]
    }

    def __init__(self, nr, serial, data, empty = False):
        super().__init__(nr, serial, data, empty)
