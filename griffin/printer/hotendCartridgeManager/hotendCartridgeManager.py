import re
import json
import queue
import logging
import sys
import os
import uuid
from threading import Lock
from griffin.signal import Signal
from griffin.thread import Thread
from griffin.timer import Timer
from .hotend import Hotend
from .hotendV1 import HotendV1
from griffin.printer.faultHandler import FaultCode, FaultHandler, FaultLevel
log = logging.getLogger(__name__.split(".")[-1])


#  This class is responsible for managing the eeprom of the pluggable hotend cartridges.
#  Creates the hotend object with data read from eeprom via gcode.
#  Updates the statistical data based on the printer state.
#  (last material used, max experienced temperature, time spend hot, material extruded, etc.)
class HotendCartridgeManager():
    __EMPTY = 0.0
    __NOMINAL_RESISTANCE_OF_DEFAULT_HOTEND_CARTRIDGE = 23 # given 24 volts and 25 watts this should be the nominal resistance.

    def __init__(self, controller):
        self._controller = controller
        self.__nr_of_hotends = self._controller.getPropertyValue("hotend_count")
        self._hotends = [None] * self.__nr_of_hotends
        self._present = [False] * self.__nr_of_hotends
        self._enabled = [False] * self.__nr_of_hotends
        self.__initialized = [False] * self.__nr_of_hotends
        self.__last_hotend_eeprom_keys = [None] * self.__nr_of_hotends
        self._thread_events = Thread("HotendCartridgeManager events", self._runEvents, daemon=True)
        self._periodic_timer = Timer("HotendCartridgeManager(periodic)", 60, self._runPeriodic).setSingleShot(False).setThreaded(True)
        self._running = False

        # a mutex(Lock) or semaphore starts with one(or multiple) available immediately.
        self._hotend_lock = Lock()
        # an event idles with none available and will block until it has one available.
        self._event_queue = queue.Queue()

        controller.getDriver().onCartridgePresentChanged.connect(self.setPresentChanged)
        controller.getDriver().onCartridgeEnabledChanged.connect(self.setEnabledChanged)
        self.onHotendDataReady = Signal()
        self.onHotendDataChanged = Signal()
        self.onInitialized = Signal()

    ## Start the threads of the HotendCartridgeManager
    def start(self):
        if not self._running:
            self._running = True
            self._thread_events.start()
            self._periodic_timer.start()

    # Retrieve the CartridgeManager's initialized state, i.e. we know the state of all hotends (present or absent)
    # and the EEPROM data of these hotends has been read.
    # @return True when the CartridgeManager is initialized
    def isInitialized(self):
        return self.__initialized.count(True) == self.__nr_of_hotends

    # Mark the specified hotend as initialized. Emit an INITIALIZED signal on the first time all hotends are initialized.
    # @param hotend_nr is the initialized hotend number.
    def __markHotendInitialized(self, hotend_nr):
        # Has state changed?
        if not self.__initialized[hotend_nr]:
            self.__initialized[hotend_nr] = True

            # Only fire once, on the first time all hotends are initialized.
            if self.isInitialized():
                self.onInitialized.emit("INITIALIZED")

    # Programs a Hotend with the data in a file located in /usr/share/griffin/griffin/machines/hotend_eeprom_data/
    # @param hotend_nr nr of the hotend to program.
    # @param hotend_type should currently only be either "AA" or "BB" using filename hotend-AA.json or hotend-BB.json.
    def programHotend(self, hotend_nr, hotend_type):
        hotend_file = os.path.join(sys.path[0], "griffin/machines/hotend_eeprom_data/hotend-" + hotend_type + ".json")
        if not os.path.isfile(hotend_file):
            log.warning("unknown hotend_type\nfile not found %s", hotend_file)
            return
        elif self._hotends[hotend_nr] is None:
            log.warning("no hotend inserted")
            return

        with self._hotend_lock:
            with open(hotend_file, "r") as f:
                self._hotends[hotend_nr].setAll(json.load(f))

            self._hotends[hotend_nr].clearEmpty()
            if self._save(self._hotends[hotend_nr]):
                self._hotends[hotend_nr].clearLastChanged()
                return True

        return False
        # implicitly released lock on hotends

    # Function called by the internal hotend present signal generated in the marlin driver.
    # Since this is entered in the marlindriver thread it is not advisable to loiter here too long,
    # processing is offloaded to the event thread.
    def setPresentChanged(self, hotend_nr, present):
        log.info("setPresentChanged(%d %s present)", hotend_nr, "is" if present else "is not")
        # hotend presence change is detected by the system
        self._event_queue.put([hotend_nr, present])

    # Function that tests what type of hotend is present if any.
    # Constructs hotend model based on read results.
    # Called from the event thread.
    def _updateHotendPresent(self, hotend_nr):
        self._present[hotend_nr] = True
        new = False
        tmp_serial = self.__readSerial(hotend_nr)
        log.debug("_updateHotendPresent(%d, serial %s)", hotend_nr, str(tmp_serial))
        if self._hotends[hotend_nr] is not None:
            if self._hotends[hotend_nr].getSerial() == tmp_serial:
                return  # assume ok and only momentary unplugged.
            else:
                new = True
        else:
            new = True

        if new:
            data = self.read(hotend_nr)
            if tmp_serial is not None:
                version = (data[0] & 0xfc) >> 2
                if version == 0:
                    log.info("_updateHotendPresent constructing new empty HotendV1(%d, %s)", hotend_nr, str(tmp_serial))
                    with self._hotend_lock:
                        self._hotends[hotend_nr] = HotendV1(hotend_nr, tmp_serial, data, empty = True)
                    self._runUnknownHotendWizard(hotend_nr)
                elif version == 1:
                    log.info("_updateHotendPresent constructing new HotendV1(%d, %s)", hotend_nr, str(tmp_serial))
                    with self._hotend_lock:
                        self._hotends[hotend_nr] = HotendV1(hotend_nr, tmp_serial, data)
                    log.debug("dump \n%s\n%s", "|" + "\n|".join(json.dumps(self._hotends[hotend_nr].getAll(), indent = 4, sort_keys = True).split("\n")),
                                               "|" + "\n|".join(self._hotends[hotend_nr].getHexRepresentation().split("\n")))

                    if not self._hotends[hotend_nr].checkCrcItems():
                        log.warning("hotend %d eeprom data did not match checksum!", hotend_nr)
                else:
                    log.warning("unknown hotend eeprom layout detected %d", version)
            else:
                # new hotend with no eeprom present.
                log.info("_updateHotendPresent constructing new dumb Hotend(%s)", str(tmp_serial))
                with self._hotend_lock:
                    self._hotends[hotend_nr] = Hotend(hotend_nr, b'NODATA', bytes(4 * 32))

            # make new fields backwards compatible for old hotends!
            if self._hotends[hotend_nr].getValue("nominal_resistance") == self.__EMPTY:
                self._hotends[hotend_nr].setValue("nominal_resistance", self.__NOMINAL_RESISTANCE_OF_DEFAULT_HOTEND_CARTRIDGE)
                self._save(self._hotends[hotend_nr])
                log.info("hotend updated to 25 watt, %d Ohm hotend!", self.__NOMINAL_RESISTANCE_OF_DEFAULT_HOTEND_CARTRIDGE)

            self.onHotendDataReady.emit(hotend_nr)
            self.onHotendDataChanged.emit(hotend_nr)

            # When we are in idle, check the nozzle combi to have XY-data.
            if self._controller.getPropertyValue("state") == "idle":
                self.checkNozzleCombi()

        self.__markHotendInitialized(hotend_nr)         # Should be last call in function to avoid sequence error with the onHotendDataReady signal.

    # Check if the current nozzle combination is valid.
    def checkNozzleCombi(self):
        # Is this a nozzle combination without XY-calibration data?
        if self._present.count(True) > 1 and not self._controller.hasXYOffsetData():
            # Trigger error message for unknown XY calibration
            FaultHandler.warning("no xy calibration data for this nozzle combo", FaultCode.XY_CALIBRATION_NOT_DONE)
        else:
            # reset trigger
            FaultHandler.clear(FaultLevel.WARNING, FaultCode.XY_CALIBRATION_NOT_DONE)

    # Function that removes the hotend that was present if any.
    # Called from the event thread.
    def _updateHotendGone(self, hotend_nr):
        self._present[hotend_nr] = False
        if self._hotends[hotend_nr] is not None:
            # hotend has been removed.
            self.__last_hotend_eeprom_keys[hotend_nr] = self._hotends[hotend_nr].getAll().keys()
            log.info("_updateHotendGone deleting reference to hotend(%d, %s)", hotend_nr, str(self._hotends[hotend_nr].getSerial()))
            with self._hotend_lock:
                self._hotends[hotend_nr] = None

            self.checkNozzleCombi()

        self.__markHotendInitialized(hotend_nr)
        self.onHotendDataChanged.emit(hotend_nr)


    # Function called by the internal hotend enabled signal generated in the marlin driver.
    # Since this is entered in the marlindriver thread it is not advisable to loiter here too long,
    # currently not used but added for future use.
    def setEnabledChanged(self, hotend_nr, enabled):
        self._enabled[hotend_nr] = enabled
        log.debug("setEnabledChanged(%d %s enabled)", hotend_nr, "is" if enabled else "is not")
        pass

    #  Sets a Hotend property
    #  This should be the only way to write data to the hotend so that the hotend_lock wil be respected.
    #  @param hotend_nr nr of the hotend.
    #  @param key the name of the property (eg: major_version_number)
    #  @param value the value to write to the eeprom (value can be int, float or string depending on key and hotendType).
    #  @note please note that the eeprom is only written once in 15 minutes and not immediately for every change.
    def setValue(self, hotend_nr, key, value):
        with self._hotend_lock:
            if self._hotends[hotend_nr] is not None:
                self._hotends[hotend_nr].setValue(key, value)

    #  Gets a hotend property
    #  this should be the only way to read data from the hotend so that the hotend_lock wil be respected.
    #  @param hotend_nr nr of the hotend.
    #  @param key the name of the property (eg: major_version_number)
    #  @return the value read from the eeprom (interpreted value can be int, float or string
    #  @note please note the data is read from memory after reading the hotend eeprom
    def getValue(self, hotend_nr, key):
        if self._hotends[hotend_nr] is not None:
            return self._hotends[hotend_nr].getValue(key)
        return None

    ## Gets all the values tracked by the eeprom cartridge, is a cartridge is remover all previous values will be returned with "" as the value.
    # @param hotend_nr The nr of the hotend.
    # @return a dictionary with keys for all values in the current/last hotend that was inserted.
    def getAllValues(self, hotend_nr):
        if self._hotends[hotend_nr] is not None:
            return self._hotends[hotend_nr].getAll()
        elif self.__last_hotend_eeprom_keys[hotend_nr] is not None:
            return { key: "" for key in self.__last_hotend_eeprom_keys[hotend_nr] }

        return None

    # Get the number of hotends that are present
    # @return Returns the number of present (inserted) hotends
    def countPresentHotends(self):
        return self._present.count(True)

    # Runs the UNKNOWN_HOTEND_WIZARD procedure if it's not already running.
    # @param hotend_nr nr of the hotend.
    def _runUnknownHotendWizard(self, hotend_nr):
        c = self._controller
        if "UNKNOWN_HOTEND_WIZARD" not in [p.getKey() for p in c.getAllProcedures() if p.isActive()]:
            try:
                p = c.getProcedure("UNKNOWN_HOTEND_WIZARD")
                p.setup({"hotend_nr": hotend_nr})
                p.start()
            except:
                FaultHandler.error("Could not start UNKNOWN_HOTEND_WIZARD, unrecognized eeprom data in slot: {hotend_nr}.".format(hotend_nr=hotend_nr), FaultCode.EEPROM_NOT_RECOGNIZED, data={"display_hotend_nr": hotend_nr + 1})
                log.warning("starting of UNKNOWN_HOTEND_WIZARD failed.")

    # Internal function forming the event thread.
    def _runEvents(self):
        while self._running:
            # wait until an event is triggered
            item = self._event_queue.get()
            if item is None or not isinstance(item, list):
                break
            (hotend_nr, present) = item
            if present:
                self._updateHotendPresent(hotend_nr)
            else:
                self._updateHotendGone(hotend_nr)
            # implicitly released lock on hotends

        if self._running:
            log.error("_runEvents() event loop has been exited unexpectedly!")

    #  Internal function forming the periodic thread.
    #  The periodic thread runs once a minute via Timer.
    #  The periodic thread updates statistical data locally every minute
    #  and updates it to the eeprom only once every 15 minutes.
    #  It also retries running the _runUnknownHotendWizard() if the hotend is still marked as empty.
    def _runPeriodic(self):
        if not self._running:
            self._periodic_timer.stop()

        # check values every minute. only write every 15 minutes
        # every 15 min adds up to +- 2.9 years of wear leveling,
        # add to that the fact that this is only during printing
        # and you're looking at one long lasting nozzle.

        # acquire lock on hotends
        with self._hotend_lock:

            log.debug("periodic")

            for hotend in self._hotends:
                if hotend is None or type(hotend) == Hotend:
                    continue    # don't attempt to write undefined or dumb hotends

                if hotend.isEmpty():
                    self._runUnknownHotendWizard(hotend.getNr())
                    continue    # don't attempt to write new hotends yet, attempt to start wizard instead.

                c = self._controller
                hotend_slot = c.getPrintHeadController().getHotendSlot(hotend.getNr())
                temp = hotend_slot.getPropertyValue("current_temperature")
                if temp > 65:
                    hotend.setValue("time_spend_hot", hotend.getValue("time_spend_hot") + 1)

                if temp > hotend.getValue("max_exp_temperature"):
                    hotend.setValue("max_exp_temperature", temp)

                used = c.getFilamentUsedAndReset(hotend.getNr())
                hotend.setValue("material_extruded", hotend.getValue("material_extruded") + used)

                guid = hotend_slot.getPropertyValue("material_guid")
                if guid and temp > 65:  # only write GUID if it is actually used
                    hotend.setValue("last_material", int(uuid.UUID(guid)))

                log.debug("changed %s  empty %s  timeSc %s", str(hotend.hasChanged()), str(hotend.isEmpty()), str(hotend.getTimeSinceChanged()))
                if hotend.hasChanged() and not hotend.isEmpty():
                    self.onHotendDataChanged.emit(hotend.getNr())
                    if hotend.getTimeSinceChanged() is None:
                        hotend.setChanged()
                    elif hotend.getTimeSinceChanged() > 15 * 60:    # 15 minutes
                        self._save(hotend)
                        hotend.clearLastChanged()
                    else:
                        pass    # do nothing

            # implicitly released lock on hotends

    # Saves the current data in memory to the eeprom.
    # also updates the crc values to match.
    # @param hotend a Hotend object model
    def _save(self, hotend):
        if hotend is not None:
            log.debug("writing data to hotend %d", hotend.getNr())
            hotend.updateCrcItems()

            (current, new) = hotend.getCachedData()
            log.debug("dump b4 write\n%s\n%s", "|" + "\n|".join(json.dumps(hotend.getAll(), indent = 4, sort_keys = True).split("\n")),
                                               "|" + "\n|".join(hotend.getHexRepresentation().split("\n")))
            if self._write(hotend.getNr(), current, new):
                hotend.setWrittenData(new)
                return True
        return False

    #  Writes data that has changed to eeprom via gcode.
    #  @param old_data bytes object containing the data as read/last written to the eeprom.
    #  @param new_data bytes object containing the data to be written to the eeprom.
    #  data is written in 8 byte chunks and this function also sorts outs which chunks have
    #  changed and which haven't, those values aren't written as they should already be correct.
    def _write(self, hotend_nr, old_data, new_data):
        ret = True
        for i in range(0, int(len(old_data) / 8)):
            addr = i * 8
            end = addr + 8
            if old_data[addr:end] != new_data[addr:end]:
                hex_str = "%02x%02x%02x%02x%02x%02x%02x%02x" % tuple(new_data[addr:end])
                log.debug("_write() hotend_nr %d, addr 0x%x data 0x%s", hotend_nr, addr, hex_str)
                tmp = self._controller.sendRawGCode("M151 T%d A%d D%s" % (hotend_nr, addr, hex_str), wait = True)
                if "DATA_WRITTEN" not in tmp:
                    log.warning("_write failed at T%d addr 0x%x with data 0x%s\n%s", hotend_nr, addr, hex_str, tmp)
                    ret = False

        return ret

    # Reads data via gcode from eeprom.
    def read(self, hotend_nr):
        data = bytes(0)
        for i in range(0, 4):
            tmp = self._controller.sendRawGCode("M150 T%d P%d" % (hotend_nr, i), wait = True)
            try:
                hex_string = re.findall("[0-9a-fA-F]{64}", tmp)[0]  # now we have the whole page in a single string
                tmp_bytes = bytes([int(i, 16) for i in re.findall("[0-9a-fA-F]{2}", hex_string)])   # convert per nibble
                data += tmp_bytes
            except:
                log.warning("Reading data from hotend %d eeprom failed.", hotend_nr)
                data += b"deadbeef\xde\xad\xbe\xef\xca\xfe\xba\xbedeadbeef\xde\xad\xbe\xef\xca\xfe\xba\xbe"

        return data

    # Reads the serial ID hard coded in the eeprom chips.
    def __readSerial(self, hotend_nr):
        tmp = self._controller.sendRawGCode("M149 T%d" % hotend_nr, wait = True)
        try:
            tmp = re.findall("[0-9a-fA-F]{12}", tmp)[0]     # now we have the whole serial in a single string
            log.debug("readSerial %s", tmp)
            tmp_bytes = bytes([int(i, 16) for i in re.findall("[0-9a-fA-F]{2}", tmp)])
            return tmp_bytes
        except:
            return None

    def isPresent(self, hotend_nr):
        return self._present[hotend_nr]

    def isEnabled(self, hotend_nr):
        return self._enabled[hotend_nr]

    ## Get the hotend serial number as a string. This is retrieved from the hotend cartridge when available.
    #  @return string containing hotend cartridge serial
    #  @return None: When no hotend available.
    def getSerial(self, hotend_nr):
        hotend = self._hotends[hotend_nr]
        if hotend is None:
            return None
        try:
            return "".join(["%02x" % i for i in self._hotends[hotend_nr].getSerial()])
        except Exception:
            log.exception("could not get serial number from hotend %d", hotend_nr)
            return None
