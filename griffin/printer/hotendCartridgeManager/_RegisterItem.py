

##
# This class has dummy functions needed to write /read to/from hotend cartridge eeprom.
# it is not intended that this class is ever instantiated, but that it is used as base
# class for other _*Item classes.
class RegisterItem:
    def __init__(self):
        self._dsc = None    # no description in base class
        self._unit = None

    def getValueFromData(self, data):
        raise NotImplementedError("not implemented in base class")

    def setValueInData(self, data, value):
        raise NotImplementedError("not implemented in base class")

    def getDescription(self):
        return self._dsc

    def getUnit(self):
        return self._unit

    def getPossibleValues(self):
        raise NotImplementedError("not implemented in base class")
