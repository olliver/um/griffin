from ._RegisterItem import RegisterItem


##
# This is a RegisterItem type and is meant to access a specific type of data in the hotend eeprom.
#
# This class is meant to encode and decode utf-8 encoded strings from the eeprom data.
# It has a size parameter instead of a mask since strings always take up whole bytes.
class StringItem(RegisterItem):
    ##
    # constructs a StringItem which is a type of RegisterItem
    # \param address the address at which the string starts.
    # \param size nr of bytes reserved for the string.
    # \param description all reagisters could use some description this is used for debugging mostly but is incuded for clarity
    def __init__(self, address, size, description):
        self._addr = address
        self._to   = address + size
        self._dsc  = description    # description for debugging
        self._unit = "string"

    def getValueFromData(self, data):
        return data[self._addr:self._to].decode("utf-8").strip("\0")

    def setValueInData(self, data, value):
        encoded = value.encode("utf-8")
        if len(encoded) > self._to - self._addr:
            encoded = encoded[0:self._to - self._addr]
        elif len(encoded) < self._to - self._addr:
            encoded += data[self._addr + len(encoded):self._to]

        new_data = list(data)   # creates copy
        new_data[self._addr:self._to] = encoded
        return bytes(new_data)

    def getPossibleValues(self):
        return [" " * (self._to - self._addr), "X" * (self._to - self._addr)]
