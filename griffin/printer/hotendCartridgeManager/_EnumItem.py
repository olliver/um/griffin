from ._NumericItem import NumericItem


# This is a RegisterItem type and is meant to access a specific type of data in the hotend eeprom.
#
# This class has a list of possibilities and uses a NumericItem as base class to store the index in said list of possibilities.
class EnumItem(NumericItem):
    # constructs an EnumItem which is a type of RegisterItem
    # \param address the address at which the most significant byte is located
    # \param mask this is used to define # of bytes, bit shift and bitmask all in one.
    # \param description all registers could use some description this is used for debugging mostly but is included for clarity
    # \param enum must be a list of options, the options can be strings or any other value type such as int or float.
    # \param unit can be set to indicate the unit type(eg: mm, degC for degrees celsius or degrees), used as a description at this point.
    def __init__(self, address, mask, description, enum, unit = None):
        super().__init__(address, mask, description, unit = unit)
        self._enum = enum

    def getValueFromData(self, data):
        value = super().getValueFromData(data)
        if value > len(self._enum):
            return None

        try:
            return self._enum[value]
        except:
            print("at index %d no valid value was found" % value)
            return None

    def setValueInData(self, data, value):
        if value in self._enum:
            numeric_value = self._enum.index(value)
            return super().setValueInData(data, numeric_value)
        else:
            raise KeyError("key does not exist in EnumItem")

    def getPossibleValues(self):
        return self._enum
