import math
from ._RegisterItem import RegisterItem


##
# This is a RegisterItem type and is meant to access a specific type of data in the hotend eeprom.
#
# This class retrieves and stores numeric data.
# shift and length in bytes are calculated from the mask item
#
# the reason this class uses bit shift code is that other functions implementing
# this were either lacking in bitfields or lacking in support for 128 bit values which
# python does apparently support, which is nice and were needed for the material GUID.
# the struct.pack() module/functions also had this concern.
# this way alignment and sizes of bitfields are don't care.
# sizes supported is anything that can be an (integer) value in python.
# also a nice bonus by bit-shifting everything is stored in big endian implicitly
#
# the read/write functions allow a transform to be applied to the values, such as divide by 100 (and multiply
# by 100 in write) for floating point numbers with 2 decimal positions.
# but the functions allow more complex transforms to be used in the future as well.
class NumericItem(RegisterItem):
    ##
    # constructs a NumericItem which is a type of RegisterItem
    # \param address the address at which the most significant byte is located
    # \param mask this is used to define # of bytes, bit shift and bitmask all in one.
    # \param description all registers could use some description this is used for debugging mostly but is included for clarity
    # \param read can be used to set a lambda function to transform the value to be returned to the user after reading the raw value
    # \param write can be used to transform user data before writing the return value from this as the raw data.
    # \param unit can be set to indicate the unit type(eg: mm, degC for degrees celsius or degrees), used as a description at this point.
    def __init__(self, address, mask, description, read = (lambda x: x), write = (lambda x: x), unit = None):
        if not isinstance(mask, str):
            raise TypeError("the mask hex data is to be entered as string so the total size of the mask can be calculated by the lower class including the 0's of the most significant bits.")

        self._addr = address
        self._mask = int(mask, 16)
        self._dsc = description     # description for debugging
        self._read = read           # conversion function from read data to display_data
        self._write = write         # conversion function from display data to written data
        self._unit = unit           # unit eg Celcius or mm
        # nr of bytes needed to enclose this bitfield
        self._bytes = math.ceil(len(mask) / 2)

        self._shift = 0
        temp_mask = self._mask
        while temp_mask & 1 == 0:
            temp_mask >>= 1
            self._shift += 1

    def getValueFromData(self, data):
        temp_value = 0
        for b in data[self._addr:self._addr + self._bytes]:
            # print("getting %02x at [%02x:%02x]" % (b, self._addr, self._addr + self._bytes))
            temp_value = (temp_value << 8) | b

        temp_value = temp_value & self._mask

        # print("temp_value ws %08x" % (temp_value >> self._shift))
        return self._read(temp_value >> self._shift)

    def setValueInData(self, data, value):
        new_data = list(data)   # creates copy
        temp_value = 0
        for b in data[self._addr:self._addr + self._bytes]:
            temp_value = (temp_value << 8) | b

        temp_value = temp_value & ~self._mask   # clear to be written values
        temp_value |= (int(self._write(value)) << self._shift) & self._mask     # fill (only) those values

        for i in range(self._addr, self._addr + self._bytes):
            # print("about to insert %02x in 0x%02x with shift %d" %( (temp_value >> ((self._addr+self._bytes-1-i)*8)) & 0xff, i, ((self._addr+self._bytes-1-i)*8) ))
            new_data[i] = (temp_value >> ((self._addr + self._bytes - 1 - i) * 8)) & 0xff

        return bytes(new_data)

    def getPossibleValues(self):
        return [self._read(0), self._read(self._mask >> self._shift)]
