import re
import time
import logging
from ._CrcItem import CrcItem
from ._RegisterItem import RegisterItem
from ._StringItem import StringItem

log = logging.getLogger(__name__.split(".")[-1])

## This is meant to be the baseclass for the hotends managed by the hotendCartridgeManager.
# this class is also meant to be used for a hotend cartridge with no eeprom or possibly a malfunctioning eeprom.
class Hotend():
    ## use this to set a number a floating point digits behind the '.'
    _FLOATING_POINT_PRECISION_NR_OF_DIGITS = [
        [None, None],
        [lambda x: x/10.0, lambda x: x * 10.0],
        [lambda x: x/100.0, lambda x: x * 100.0],
        [lambda x: x/1000.0, lambda x: x * 1000.0],
    ]

    _layout = { "static": {
        # Make sure the dummy hotend data has a hotend_cartridge_id, else the rest of the system falls apart.
        "hotend_cartridge_id":  StringItem( 0x0e, 10, "hotend cartridge identification string assigned by manufacturer")
    }, "reserved": {} }
    
    ## creates a Hotend
    #  @param nr location of this Hotend.
    #  @param serial bytes object containing the serial number of the Hotend.
    #  @param data bytes object with data read from eeprom.
    #  @param empty indicates that this is a clean from the factory eeprom to be programmed(the UNKNOWN_HOTEND_WIZARD will be run if the Hotend is empty).
    def __init__(self, nr, serial, data, empty = False):
        self._nr = nr
        self._serial = serial
        self.current_data = data
        self._new_data = data
        self._empty = empty
        self._last_changed = None

    ## return the keys of all the properties stored.
    #  @return list of key strings.
    def getValueKeys(self):
        return [ i for i in self._layout["static"] ]

    #  @return The numeric position of the Hotend.
    def getNr(self):
        return self._nr

    # @return A byte object containing the serial number hard coded in the eeprom chip.
    def getSerial(self):
        return self._serial
    
    # @return the current and new data objects.
    def getCachedData(self):
        return [self.current_data, self._new_data]

    # @return empty indicate that this is a clean from the factory eeprom (the UNKNOWN_HOTEND_WIZARD will be run if the Hotend is empty).
    def isEmpty(self):
        return self._empty

    # Clears the empty flag
    def clearEmpty(self):
        self._empty = False
        self._last_changed = 0 # program inside the next minute

    ## Tests if the hotend's data has been written to since it was last updated.
    def hasChanged(self):
        return self.current_data != self._new_data

    ## set now as the first time the hotend was changed
    def setChanged(self):
        self._last_changed = time.monotonic()

    ## gets time elapsed since it was first changed
    def getTimeSinceChanged(self):
        if self._last_changed is None:
            return None
        else:
            return time.monotonic() - self._last_changed

    ## this should typically be called after data is written to the hotend.
    # clears the getTimeSinceChanged time elapsed since it was first changed
    def clearLastChanged(self):
        self._last_changed = None

    ## Gets the data for the RegisterItem found via the key from the new data
    def getValue(self, key):
        if key in self._layout["static"]:
            tmp = self._layout["static"].get(key)
            if isinstance(tmp, RegisterItem):
                return tmp.getValueFromData(self._new_data)
        return None

    ## Sets the data for the RegisterItem found via the key to value in the new data
    def setValue(self, key, value):
        tmp = self._layout["static"].get(key)
        if isinstance(tmp, RegisterItem):
            self._new_data = tmp.setValueInData(self._new_data, value)
        else:
            raise ValueError("no registerItem")

    ## gives the possible values to be used with the key given.
    # @return A list of values, in a numeric range it's the [min, max] values, for an enum it's the whole range, etc.
    def getPossibleValues(self, key):
        if key in self._layout["static"]:
            tmp = self._layout["static"].get(key)
            if isinstance(tmp, RegisterItem):
                return tmp.getPossibleValues()

    ## sets the data overwriting both old_data and new_data, only use when data is read from eeprom.
    def setData(self, data): # data is read
        self.current_data = data
        self._new_data = data

    ## overwrites old data with new data, confirming that it's been written.
    def setWrittenData(self, data):
        self.current_data = data

    # @return A dictionary with all the values read from the new data.
    def getAll(self, info = 0):
        tmp = self._layout["static"] # should have a 2nd list comprehension for dynamic stuff if that gets added.
        if info > 0:
            return { item: [ tmp[item].getValueFromData(self._new_data), tmp[item].getDescription() ] for item in tmp }
        return { item: tmp[item].getValueFromData(self._new_data) for item in tmp }

    ## Returns the new data in a multi line string with a hexdump format.
    def getHexRepresentation(self):
        out = ""
        for i in range(0, int(len(self._new_data)/16)):
            addr = i * 16
            end = addr + 16
            out += ("%04x: " % addr) + " ".join( [ "%02x" % b for b in self._new_data[addr:end] ])
            out += " " + re.sub(r"\\x[0-9a-fA-D]{2}", ".", str(self._new_data[addr:end])) + "\n"
        return out

    ## Takes a dictionary and for each key that exists (see getValueKeys()) as Hotend property it sets its value.
    #  @param values a dictionary who's keys match keys that exist in the Hotend.
    def setAll(self, values):
        if not isinstance(values, dict):
            return
        tmp = self._layout["static"] # should have a 2nd list comprehension for dynamic stuff if that gets added
        for item in values:
            if item in tmp:
                self._new_data = tmp[item].setValueInData(self._new_data, values[item])

    ## Recalculates the CRC checksums in the new data.
    def updateCrcItems(self):
        tmp = self._layout["static"]
        for j in [ i for i in tmp if isinstance(tmp[i], CrcItem) ]:
            self.setValue(j, None) # set with dummy value just recalculates checksum.

    ## checks if all the crc items match.
    # @return True if all CrcItems match false otherwise.
    def checkCrcItems(self):
        crc_ok = True
        tmp = self._layout["static"]
        for j in [ i for i in tmp if isinstance(tmp[i], CrcItem) ]:
            crc_ok &= self.getValue(j)
        return crc_ok
