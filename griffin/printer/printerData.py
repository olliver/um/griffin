import json
import logging
import importlib
import copy
import os

log = logging.getLogger(__name__.split(".")[-1])


class PrinterData(object):
    ## Search paths to load printer data from,
    ## These paths are searched in order to find machine json configuration files.
    __SEARCH_PATHS = [
        "/var/lib/griffin/machines",
        os.path.join(os.path.dirname(__file__), "..", "..", "griffin/machines"),
    ]

    def __init__(self, json_filename):
        self._properties = {}
        self._procedure_list = []
        self._code_to_procedure = {}

        self._load(json_filename)

    def _load(self, json_filename):
        # Search for the json file to load. Go over the search paths and try to find our json file in there and parse it.
        json_data = None
        for path in PrinterData.__SEARCH_PATHS:
            full_filename = os.path.join(path, json_filename)
            if os.path.isfile(full_filename):
                try:
                    with open(full_filename, "rt") as f:
                        json_data = json.load(f)
                    break # We have read the json file, stop searching.
                except:
                    log.exception("Failed to load file: %s", full_filename)
        if json_data is None:
            raise FileNotFoundError("Failed to find machine configuration file: %s", json_filename)

        # Use an inheritance structure to have a base json file and machine types that overrule on add on this.
        # I rather not due to the added complexity, however, with the procedure definitions being in the json file,
        # this would result in a lot of duplication otherwise.
        if "inherit" in json_data:
            self._load(json_data["inherit"])
        # Log that we are loading this file after the inheritance, so we see the order in which data is loaded and overloaded.
        log.info("Loading machine data from %s", json_filename)

        if "properties" in json_data:
            self._properties.update(json_data["properties"])
        else:
            log.warning("No properties defined in the machine json file: %s", json_filename)

        if "procedures" in json_data:
            for key, procedure_data in json_data["procedures"].items():
                inserted = False
                for index in range(0, len(self._procedure_list)):
                    if self._procedure_list[index][0] == key:
                        self._procedure_list[index] = (key, procedure_data)
                        inserted = True
                        break
                if not inserted:
                    self._procedure_list.append((key, procedure_data))

        if "code_procedures" in json_data:
            for code, info in json_data["code_procedures"].items():
                data = {
                    "procedure": None,
                    "arguments": {},
                    "static_arguments": {},
                    "preferences": {},
                }
                if "arguments" in info:
                    data["arguments"] = info["arguments"]
                if "static_arguments" in info:
                    data["static_arguments"] = info["static_arguments"]
                data["procedure_key"] = info["procedure"]
                self._code_to_procedure[code] = data

    def getMachineProperties(self):
        return copy.deepcopy(self._properties)

    def getProcedures(self):
        procedures = []
        # Create the procedures when they are requested, not when the configuration file is parsed.
        # This because dependencies for the procedures creation are not setup properly during parsing.
        for key, procedure_data in self._procedure_list:
            procedure_name = procedure_data[0]
            procedure_package, procedure_class = procedure_name.rsplit(".", 1)

            module = importlib.import_module("griffin.printer.procedures.%s" % procedure_package)
            constructor = getattr(module, procedure_class)

            kwargs = None
            if isinstance(procedure_data[-1], dict):
                kwargs = procedure_data[-1]
                args = procedure_data[1:-1]
            else:
                args = procedure_data[1:]

            try:
                if kwargs is not None:
                    procedures.append(constructor(key, *args, **kwargs))
                else:
                    procedures.append(constructor(key, *args))
            except:
                log.exception("Failed to construct: %s\n%s", key, procedure_data)
        return procedures

    def getCodeToProcedureMapping(self):
        return self._code_to_procedure
