import logging

log = logging.getLogger(__name__.split(".")[-1])

## Since X and Y have the same offset I'm including the calculation to prove this by adding only a single dictionary to translate user input to offset.
#  Actual offset can be verified with the line below:
#  tt = { str(int(i - n / 2)): n1_y_align[int(y_match_in_n2_index[0] + (i - n / 2) * 2)][1] - n2_y_align[int(y_match_in_n2_index[0] + (i - n / 2) * 2)][1] for i in range(0, n) }
#  @param count The number of offsets
#  @param frequency_difference The frequency frequency_difference
#  @return Returns a dictionary with index -count to +count having floating offsets as values
def generateTranslationTable(count, frequency_difference):
    half_count = count / 2
    return { str(int(i - half_count)): int(i - half_count) * frequency_difference for i in range(0, count + 1) }

## This class implements the XY translation table using classmethods and a simple initialization function
class TranslationXYNozzleOffset():
    DISTANCE = 120  # total length of the ruler (like) print in mm.
    FREQUENCY_DISTANCE = 3  # base frequency of lines on the ruler.
    FREQUENCY_DISTANCE_2 = DISTANCE / (DISTANCE / FREQUENCY_DISTANCE + 1) # 2nd frequency, the exact spacing you need to include 1 extra line in the same distance as the base freqency).
    __XY_TRANSLATION_TABLE = generateTranslationTable(int(DISTANCE / FREQUENCY_DISTANCE), FREQUENCY_DISTANCE - FREQUENCY_DISTANCE_2)

    ## @brief Translates from the given offset value to the index of the XY_TRANSLATION_TABLE
    #  @param X The offset to find the x axis index for.
    #  @param Y The offset to find the y axis index for.
    #  @return Returns 0 if the value cannot be found (which is also 0.0 in the table) or the actual index value; returns dictionary of indexes if both X and Y are specified.
    @classmethod
    def getIndex(cls, X=None, Y=None):
        assert X is not None or Y is not None
        if X is not None and Y is None:
            offset = X
        elif Y is not None and X is None:
            offset = Y
        else:
            return { "X": cls.getIndex(X=X), "Y": cls.getIndex(Y=Y)}

        offset = float(offset)
        for index, offset_value in cls.__XY_TRANSLATION_TABLE.items():
            if (offset == offset_value):
                log.info("Offset {offset} found at index {index}".format(offset=offset, index=index))
                return index
        log.info("No index found for offset {offset}, returning default index 0".format(offset=offset))
        return 0

    ## @brief Translates from index to the offset value using the the XY_TRANSLATION_TABLE
    #  @param X The index to find the x axis offset value for.
    #  @param Y The index to find the y axis offset value for.
    #  @return Returns 0.0 if the index cannot be found or the actual index value; returns dictionary of offsets if both X and Y are specified.
    @classmethod
    def getOffset(cls, X=None, Y=None):
        assert X is not None or Y is not None
        if X is not None and Y is None:
            index = X
        elif Y is not None and X is None:
            index = Y
        else:
            return { "X": cls.getOffset(X=X), "Y": cls.getOffset(Y=Y)}

        index = str(index)
        if index in cls.__XY_TRANSLATION_TABLE:
            offset = cls.__XY_TRANSLATION_TABLE[index]
            log.info("Ofset at index {index}: {offset}".format(index=index, offset=offset))
            return offset
        log.info("Index {index} not available, returning default offset 0.0".format(index=index))
        return 0.0
