import logging

from .avrProgrammer import AvrProgrammer
from .intelHex import readHex
from griffin.printer.faultHandler import FaultHandler, FaultCode

log = logging.getLogger(__name__.split(".")[-1])


class FirmwareUpdate():
    def __init__(self, started = None):
        self._started = started
        self._firmware_data = None
        self._programmer = None

    def init(self):
        if self._programmer is None:
            return False
        error_msg = ""
        for n in range(0, 5):
            if self._programmer.enterProgrammingMode():
                break
            error_msg = "Failed to enter programming mode..."
            log.warning(error_msg)
            # , FaultCode.MOTION_CONTROLLER_UPDATE_FAILED)
            self._programmer.leaveProgrammingMode()
            if n == 4:
                FaultHandler.error(log.name + ": " + error_msg, FaultCode.MOTION_CONTROLLER_UPDATE_FAILED)
                return False

        # Check if we have an atmega2560
        if self._programmer.readSignature() != b"\x1e\x98\x01":
            FaultHandler.error(log.name + ": Wrong device found connected...", FaultCode.MOTION_CONTROLLER_UPDATE_FAILED)
            return False
        # Write the proper fuses
        if not self._programmer.writeFuse(b"\xFF\xD0\xFD"):
            FaultHandler.error(log.name + ": Failed to properly write fuses", FaultCode.MOTION_CONTROLLER_UPDATE_FAILED)
            return False
        # Check if any lockbits are programmed, and erase those.
        if self._programmer.readLock() != 0xFF:
            log.info("Chip is locked, erasing it, so it is unlocked again.")
            self._programmer.eraseChip()
        return True

    def setFile(self, firmware_file):
        try:
            self._firmware_data = readHex(firmware_file)
            self._programmer = AvrProgrammer()
        except:
            log.exception("Failed to start programmer for firmware update tool")
            self._firmware_data = None
            self._programmer = None

    def close(self):
        if self._programmer is None:
            return False

        self._programmer.leaveProgrammingMode()
        self._programmer = None
        self._firmware_data = None
        return True

    # Check if the firmware is already installed, returns true if the proper firmware is already installed.
    def checkFirmware(self, quick_test=True):
        if self._programmer is None:
            return False
        if quick_test:
            # Check if the first page is the same (contains interrupt tables, which quickly change)
            if self._programmer.readMemory(0, 0x100) != self._firmware_data[0:0x100]:
                log.info("First 0x100 bytes did not match")
                return False
            # Check if the byte after the program is 0xFF, as this should be unprogrammed and thus 0xFF
            if self._programmer.readMemory(len(self._firmware_data), len(self._firmware_data) + 1) != bytearray([0xFF]):
                log.info("End if firmware was not 0xFF")
                return False
            # Check if the last 0x100 bytes match the firmware, as these easy change when the firmware changes.
            if self._programmer.readMemory(len(self._firmware_data) - 0x100, len(self._firmware_data)) != self._firmware_data[-0x100:]:
                log.info("Last 0x100 bytes did not match")
                return False
        else:
            if not self._programmer.verifyMemory(self._firmware_data):
                log.info("Firmware did not match.")
                return False
        log.info("Firmware check done. Firmware matched.")
        return True

    # Program the actual firmware, returns false on an error
    def programFirmwareWrite(self):
        if self._programmer is None:
            return False
        if self._started is not None:
            self._started()
        ret = self._programmer.writeMemory(self._firmware_data)
        if ret == False:
            FaultHandler.error("Error writing firmware", FaultCode.MOTION_CONTROLLER_UPDATE_FAILED)

        return ret

    def programFirmwareVerify(self):
        if self._programmer is None:
            return False
        if self._started is not None:
            self._started()
        ret = self._programmer.verifyMemory(self._firmware_data)
        if ret == False:
            FaultHandler.error("Error verifying firmware", FaultCode.MOTION_CONTROLLER_UPDATE_FAILED)

        return ret

    def progress(self):
        if self._programmer is not None:
            return self._programmer.progress()
        else:
            return None


def main():
    fu = FirmwareUpdate()
    fu.setFile("firmware/Marlin.hex")
    if not fu.init():
        log.info("Failed to initialize firmware update tool")
        fu.close()
        return False
    if not fu.checkFirmware():
        log.info("Updating firmware")
        fu.programFirmwareWrite()
        if not fu.programFirmwareVerify():
            log.error("Failed to properly update the firmware")
            fu.close()
            return False
    fu.close()
    return True

if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)-15s %(levelname)-8s %(name)-15s %(message)s", level=logging.DEBUG)

    main()
