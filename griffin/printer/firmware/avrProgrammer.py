import logging
import time

from .spi import SpiBusBitBang as SpiBus

log = logging.getLogger(__name__.split(".")[-1])


class AvrProgrammer():
    PROG_ENABLE_COMMAND = b"\xAC\x53\x00\x00"
    CHIP_ERASE_COMMAND = b"\xAC\x80\x00\x00"
    PROG_POLL_RDY = b"\xF0\x00\x00\x00"

    LOAD_EXT_ADDR = b"\x4D\x00"
    LOAD_FLASH_HIGH = b"\x48\x00"
    LOAD_FLASH_LOW = b"\x40\x00"

    READ_LOCK_BITS = b"\x58\x00\x00\x00"
    READ_SIGNATURE_0 = b"\x30\x00\x00\x00"
    READ_SIGNATURE_1 = b"\x30\x00\x01\x00"
    READ_SIGNATURE_2 = b"\x30\x00\x02\x00"
    READ_LFUSE = b"\x50\x00\x00\x00"
    READ_HFUSE = b"\x58\x08\x00\x00"
    READ_EFUSE = b"\x50\x08\x00\x00"

    WRITE_LFUSE = b"\xAC\xA0\x00"
    WRITE_HFUSE = b"\xAC\xA8\x00"
    WRITE_EFUSE = b"\xAC\xA4\x00"
    WRITE_FLASH_PAGE = b"\x4C"

    READ_HIGH = b"\x28"
    READ_LOW = b"\x20"

    def _reset(self, enable):
        with open("/dev/gpio/avr_reset", "w") as f:
            if enable:
                f.write("0")
            else:
                f.write("1")

    def __init__(self):
        self._spi = SpiBus("/dev/spidev32765.0")
        self._writing = False
        self._verifying = False
        self._n = 0
        self._size = 1  # no div/0

    def enterProgrammingMode(self):
        self._reset(False)
        time.sleep(0.100)
        self._reset(True)
        time.sleep(0.100)
        res = self._spi.transfer(self.PROG_ENABLE_COMMAND)
        if res[2] != 0x53:
            log.debug("PROG_ENABLE_COMMAND response: %s", res)
            return False
        self.waitWhileBusy()
        return True

    def leaveProgrammingMode(self):
        self._reset(False)
        time.sleep(1.0)

    def isBusy(self):
        ret = self._spi.transfer(self.PROG_POLL_RDY)
        return (ret[3] & 0x01) == 0x01

    def waitWhileBusy(self):
        while self.isBusy():
            pass

    def readSignature(self):
        ret = bytearray(3)
        data = self._spi.transfer(self.READ_SIGNATURE_0)
        ret[0] = data[3]
        data = self._spi.transfer(self.READ_SIGNATURE_1)
        ret[1] = data[3]
        data = self._spi.transfer(self.READ_SIGNATURE_2)
        ret[2] = data[3]
        return ret

    def readLock(self):
        return self._spi.transfer(self.READ_LOCK_BITS)[3]

    def readFuse(self):
        ret = bytearray(3)
        data = self._spi.transfer(self.READ_LFUSE)
        ret[0] = data[3]
        data = self._spi.transfer(self.READ_HFUSE)
        ret[1] = data[3]
        data = self._spi.transfer(self.READ_EFUSE)
        ret[2] = data[3]
        return ret

    def writeFuse(self, fuses):
        assert(len(fuses) == 3)

        wrote_fuse = False
        current_fuses = self.readFuse()
        if current_fuses[0] != fuses[0]:
            log.info("Programming low fuse")
            wrote_fuse = True
            self._spi.transfer(self.WRITE_LFUSE + fuses[0:1])
        if current_fuses[1] != fuses[1]:
            log.info("Programming high fuse")
            wrote_fuse = True
            self._spi.transfer(self.WRITE_HFUSE + fuses[1:2])
        if current_fuses[2] != fuses[2]:
            log.info("Programming extended fuse")
            wrote_fuse = True
            self._spi.transfer(self.WRITE_EFUSE + fuses[2:3])

        if wrote_fuse:
            self.waitWhileBusy()
            current_fuses = self.readFuse()
        return current_fuses == fuses

    def readMemory(self, low_addr, high_addr):
        ret = bytearray(high_addr - low_addr)
        last_extended_mem_byte = -1
        for self._n in range(low_addr, high_addr):

            extended_mem_byte = self._n >> 17
            if last_extended_mem_byte != extended_mem_byte:
                self._spi.transfer(self.LOAD_EXT_ADDR + extended_mem_byte.to_bytes(1, "big") + b"\x00")
                last_extended_mem_byte = extended_mem_byte

            if self._n & 1 == 0:
                cmd = self.READ_LOW + ((self._n >> 1) & 0xFFFF).to_bytes(2, "big") + b"\x00"
            else:
                cmd = self.READ_HIGH + ((self._n >> 1) & 0xFFFF).to_bytes(2, "big") + b"\x00"
            ret[self._n - low_addr] = self._spi.transfer(cmd)[3]
            if self._n % 256 == 0:
                log.debug("Reading page %d", int(self._n / 256))
        return ret

    def verifyMemory(self, program):
        last_extended_mem_byte = -1
        self._n = 0
        self._size = len(program)
        self._verifying = True
        for self._n in range(0, len(program)):
            extended_mem_byte = self._n >> 17
            if last_extended_mem_byte != extended_mem_byte:
                self._spi.transfer(self.LOAD_EXT_ADDR + extended_mem_byte.to_bytes(1, "big") + b"\x00")
                last_extended_mem_byte = extended_mem_byte

            if self._n & 1 == 0:
                cmd = self.READ_LOW + ((self._n >> 1) & 0xFFFF).to_bytes(2, "big") + b"\x00"
            else:
                cmd = self.READ_HIGH + ((self._n >> 1) & 0xFFFF).to_bytes(2, "big") + b"\x00"
            if program[self._n] != self._spi.transfer(cmd)[3]:
                self._verifying = False
                return False
            if self._n % 256 == 0:
                log.debug("Verifying page %d", int(self._n / 256))

        self._verifying = False
        return True

    def writeMemory(self, program):
        self.eraseChip()
        page_size = 128 * 2
        page_count = int((len(program) + page_size - 1) / page_size)
        self._n = 0
        self._size = page_count * page_size
        self._writing = True
        log.info("Writing %d pages", page_count)
        program = program + bytearray([0xFF] * (page_count * page_size - len(program)))
        last_extended_mem_byte = -1
        for page_nr in range(0, page_count):
            extended_mem_byte = (page_nr * page_size) >> 17
            if last_extended_mem_byte != extended_mem_byte:
                self._spi.transfer(self.LOAD_EXT_ADDR + extended_mem_byte.to_bytes(1, "big") + b"\x00")
                last_extended_mem_byte = extended_mem_byte

            for word_nr in range(0, int(page_size / 2)):
                index = page_nr * page_size + word_nr * 2
                self._n = index
                self._spi.transfer(self.LOAD_FLASH_LOW + (word_nr).to_bytes(1, "big") + program[index: index + 1])
                self._spi.transfer(self.LOAD_FLASH_HIGH + (word_nr).to_bytes(1, "big") + program[index + 1: index + 2])

            log.debug("Write page %d/%d", page_nr, page_count)
            self._spi.transfer(self.WRITE_FLASH_PAGE + (((page_nr * page_size) >> 1) & 0xFFFF).to_bytes(2, "big") + b"\x00")
            self.waitWhileBusy()
        self._writing = False

    def progress(self):
        if self._writing or self._verifying:
            return (self._n, self._size)
        else:
            return (0, 1)

    def eraseChip(self):
        log.debug("Erasing chip")
        self._spi.transfer(self.CHIP_ERASE_COMMAND)
        self.waitWhileBusy()
