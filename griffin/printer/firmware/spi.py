import fcntl
import array
import ctypes
import struct
import os

# from asm-generic/ioctl.h
IOC_NRBITS   =  8
IOC_TYPEBITS =  8
IOC_SIZEBITS = 14
IOC_DIRBITS  =  2

IOC_NRSHIFT    = 0
IOC_TYPESHIFT  = (IOC_NRSHIFT+IOC_NRBITS)
IOC_SIZESHIFT  = (IOC_TYPESHIFT+IOC_TYPEBITS)
IOC_DIRSHIFT   = (IOC_SIZESHIFT+IOC_SIZEBITS)

IOC_NONE   = 0
IOC_WRITE  = 1
IOC_READ   = 2


def _IOC(direction, type_, nr, size):   # [CodeStyle: Match the linux kernel header macro name]
    return (((direction) << IOC_DIRSHIFT) |
            ((size) << IOC_SIZESHIFT) |
            ((type_) << IOC_TYPESHIFT) |
            ((nr) << IOC_NRSHIFT))


def _IOR(type_, number, size):  # [CodeStyle: Match the linux kernel header macro name]
    return _IOC(IOC_READ, type_, number, size)


def _IOW(type_, number, size):  # [CodeStyle: Match the linux kernel header macro name]
    return _IOC(IOC_WRITE, type_, number, size)

# from spi/spidev.h
SPI_CPHA        = 0x01
SPI_CPOL        = 0x02

SPI_MODE_0      = (0|0)
SPI_MODE_1      = (0|SPI_CPHA)
SPI_MODE_2      = (SPI_CPOL|0)
SPI_MODE_3      = (SPI_CPOL|SPI_CPHA)

SPI_CS_HIGH     = 0x04
SPI_LSB_FIRST   = 0x08
SPI_3WIRE       = 0x10
SPI_LOOP        = 0x20
SPI_NO_CS       = 0x40
SPI_READY       = 0x80
SPI_TX_DUAL     = 0x100
SPI_TX_QUAD     = 0x200
SPI_RX_DUAL     = 0x400
SPI_RX_QUAD     = 0x800

SPI_IOC_MAGIC   = ord("k")


def SPI_IOC_MESSAGE(count): # [CodeStyle: Match the linux kernel header macro name]
    return _IOW(SPI_IOC_MAGIC, 0, count)

# Read / Write of SPI mode (SPI_MODE_0..SPI_MODE_3)
SPI_IOC_RD_MODE             = _IOR(SPI_IOC_MAGIC, 1, 1)
SPI_IOC_WR_MODE             = _IOW(SPI_IOC_MAGIC, 1, 1)

# Read / Write SPI bit justification
SPI_IOC_RD_LSB_FIRST        = _IOR(SPI_IOC_MAGIC, 2, 1)
SPI_IOC_WR_LSB_FIRST        = _IOW(SPI_IOC_MAGIC, 2, 1)

# Read / Write SPI device word length (1..N)
SPI_IOC_RD_BITS_PER_WORD    = _IOR(SPI_IOC_MAGIC, 3, 1)
SPI_IOC_WR_BITS_PER_WORD    = _IOW(SPI_IOC_MAGIC, 3, 1)

# Read / Write SPI device default max speed hz
SPI_IOC_RD_MAX_SPEED_HZ     = _IOR(SPI_IOC_MAGIC, 4, 4)
SPI_IOC_WR_MAX_SPEED_HZ     = _IOW(SPI_IOC_MAGIC, 4, 4)


class SpiBus():
    def __init__(self, device_name):
        self.fd = os.open(device_name, os.O_RDWR)

        mode = SPI_MODE_0
        bits = 8
        speed = 400000

        val8 = array.array("B", [0])
        val8[0] = mode
        if fcntl.ioctl(self.fd, SPI_IOC_WR_MODE, val8):
            raise Exception("Cannot write SPI Mode")
        if fcntl.ioctl(self.fd, SPI_IOC_RD_MODE, val8):
            raise Exception("Cannot read SPI Mode")
        self.mode = struct.unpack("B", val8)[0]
        assert(self.mode == mode)

        val8[0] = bits
        if fcntl.ioctl(self.fd, SPI_IOC_WR_BITS_PER_WORD, val8):
            raise Exception("Cannot write SPI Bits per word")
        if fcntl.ioctl(self.fd, SPI_IOC_RD_BITS_PER_WORD, val8):
            raise Exception("Cannot read SPI Bits per word")
        self.bits = struct.unpack("B", val8)[0]
        assert(self.bits == bits)

        val32 = array.array("I", [0])
        if speed > 0:
            val32[0] = speed
            if fcntl.ioctl(self.fd, SPI_IOC_WR_MAX_SPEED_HZ, val32):
                raise Exception("Cannot write SPI Max speed")
        if fcntl.ioctl(self.fd, SPI_IOC_RD_MAX_SPEED_HZ, val32):
            raise Exception("Cannot read SPI Max speed")
        self.speed = struct.unpack("I", val32)[0]
        assert((self.speed == speed) or (speed == 0))

    def __str__(self):
        return "SPI(chip=%d, mode=%d, speed=%dHz)" % (self.chip, self.mode, self.speed)

    def transfer(self, txbuff):
        length = len(txbuff)
        txbuff = bytes(txbuff)
        txptr = ctypes.create_string_buffer(txbuff)
        rxptr = ctypes.create_string_buffer(length)

        data = struct.pack("QQLLHBBL",  # 64 64 32 32 16 8 8 32 b = 32B
                           ctypes.addressof(txptr),
                           ctypes.addressof(rxptr),
                           length,
                           self.speed,
                           0,  # delay
                           self.bits,
                           0,  # cs_change
                           0   # pad
                           )

        fcntl.ioctl(self.fd, SPI_IOC_MESSAGE(len(data)), data)
        rxbuff = ctypes.string_at(rxptr, length)
        return bytearray(rxbuff)


class SpiBusBitBang():
    def __init__(self, device_name):
        self._avr_sck = os.open("/dev/gpio/avr_sck", os.O_WRONLY)
        self._avr_miso = os.open("/dev/gpio/avr_miso", os.O_RDONLY)
        self._avr_mosi = os.open("/dev/gpio/avr_mosi", os.O_WRONLY)

    def _clock(self, high):
        os.lseek(self._avr_sck, 0, os.SEEK_SET)
        if high:
            os.write(self._avr_sck, b"1")
        else:
            os.write(self._avr_sck, b"0")

    def _output(self, high):
        os.lseek(self._avr_mosi, 0, os.SEEK_SET)
        if high:
            os.write(self._avr_mosi, b"1")
        else:
            os.write(self._avr_mosi, b"0")

    def _input(self):
        os.lseek(self._avr_miso, 0, os.SEEK_SET)
        line = os.read(self._avr_miso, 1)
        return line == b"1"

    def _byte(self, b):
        ret = 0
        for n in range(0, 8):
            if (b & (0x80 >> n)):
                self._output(True)
            else:
                self._output(False)
            self._clock(False)
            self._clock(True)
            if self._input():
                ret = ret | (0x80 >> n)
        self._clock(False)
        return ret

    def transfer(self, txbuff):
        rxbuff = bytearray(len(txbuff))
        for n in range(0, len(txbuff)):
            rxbuff[n] = self._byte(txbuff[n])
        return rxbuff
