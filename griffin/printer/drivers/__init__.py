# This packages contains the different motion controller drivers.

# The main function of a motion control driver is to continuously drive the stepper motors.
# It does this by having a queue of planned motions.
# The rest of the software executes motions by putting these motions into this buffer.
