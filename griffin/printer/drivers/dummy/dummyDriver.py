import queue
import time
import re
import logging
import random

from griffin.timer import Timer
from griffin import thread
from griffin import signal

log = logging.getLogger(__name__.split(".")[-1])


# Local function used to extract a value behind a "key" in GCode lines.
def _getValue(key, line, default=None, return_type=int):
    res = re.search("%s([0-9\.]+)" % (key), line)
    if res is None:
        return default
    try:
        return return_type(res.group(1))
    except ValueError:
        return default


class DummyDriver:
    _NORMAL_QUEUE_SIZE = 4
    _INSTANT_QUEUE_SIZE = 10

    def __init__(self, controller, port_name):
        super().__init__()
        # Configuration
        self.__controller = controller
        self.__hotend_count = controller.getPropertyValue("hotend_count")

        # Runtime data
        self._serial = None
        self._send_queue = queue.Queue(self._NORMAL_QUEUE_SIZE)
        self._instant_queue = queue.Queue(self._INSTANT_QUEUE_SIZE)
        self._state = "DISCONNECTED"
        self._thread = thread.Thread("Communication thread", self._communicationThreadFunction, daemon=True)
        self._thread.start()
        self._error_condition = ""

        self._current_hotend_temperature = [0.0] * self.__hotend_count
        self._target_hotend_temperature = [0.0] * self.__hotend_count
        self._is_hotend_heating = [False] * self.__hotend_count
        self._hotend_temperature_request_nr = 0

        self._active_hotend = 0
        self._current_bed_temperature = 0.0
        self._target_bed_temperature = 0.0
        self._is_bed_heating = False

        self.__controller.setPreference("SWITCH_OFFSET", [10, 10])   # dummy can never calibrate this
        self.__controller.setPreference("MATERIAL_0", "6976d020-18d1-4d46-9f3e-411189a1e230")    # Set the material of the primary nozzle to PLA
        self.__controller.setPreference("MATERIAL_1", "6976d020-18d1-4d46-9f3e-411189a1e230")    # Set the material of the second nozzle to PLA

        self._check_for_hotends_timer = Timer("dummyDriver(insertHotends)", 5, self._insertHotends)
        self._check_for_hotends_timer.start()

        self._thread = thread.Thread("InsertHotends", self._insertHotends, daemon=False)
        self._thread.start()

        self.onBedTemperatureUpdate = signal.Signal(type=signal.Signal.ASync)
        self.onHotendTemperatureUpdate = signal.Signal(type=signal.Signal.ASync)
        # The status changed signal is emitted every time the status of the player changes.
        self.onStatusChanged = signal.Signal(type=signal.Signal.ASync)
        self.onCartridgePresentChanged = signal.Signal(type=signal.Signal.ASync)
        self.onCartridgeEnabledChanged = signal.Signal(type=signal.Signal.ASync)

    # Start the motion controller driver.
    def start(self):
        pass

    # @brief Signal the hotend cartridge manager that hotends are present
    def _insertHotends(self):
        for hotend_index in range(self.__hotend_count):
            log.info("Inserting simulated hotend %d" % hotend_index)
            self.onCartridgePresentChanged.emit(hotend_index, True)

    def _communicationThreadFunction(self):
        random.seed()
        while True:
            if self._state == "DISCONNECTED":
                time.sleep(1.0)
                self._setState("CONNECTED")

            send_line = None
            send_ack_handle_function = None
            try:
                send_line, send_ack_handle_function = self._instant_queue.get(False)
                self._instant_queue.task_done()
            except queue.Empty:
                pass
            if send_line is None:
                try:
                    send_line = self._send_queue.get(False)
                    self._send_queue.task_done()
                except queue.Empty:
                    pass
            if send_line is not None:
                reply = self._processLine(send_line)
                if send_ack_handle_function is not None:
                    send_ack_handle_function(reply)
                time.sleep(0.01)
            else:
                time.sleep(0.1)

            # Fake hotend/bed heatup. Horrible simulation, but works good enough for testing.
            for i in range(0, self.__hotend_count):
                if (self._is_hotend_heating[i]):
                    if self._current_hotend_temperature[i] < self._target_hotend_temperature[i]:
                        self._current_hotend_temperature[i] += random.uniform(1.5, 2.5)
                else:
                    if self._current_hotend_temperature[i] > self._target_hotend_temperature[i]:
                        self._current_hotend_temperature[i] -= random.uniform(1.5, 2.5)
            if (self._is_bed_heating):
                if (self._current_bed_temperature < self._target_bed_temperature):
                    self._current_bed_temperature += random.uniform(0.5, 1.5)
            else:
                if (self._current_bed_temperature > self._target_bed_temperature):
                    self._current_bed_temperature -= random.uniform(0.5, 1.5)

    def _processLine(self, line):
        # return string
        ret = ""
        # Parse the line recevied from the rest of the system, and in some cases handle this to update internal states.
        mcode = _getValue("M", line)
        if mcode == 104:
            self._target_hotend_temperature[_getValue("T", line, self._active_hotend, int)] = _getValue("S", line, 0.0, float)
        if mcode == 140:
            self._target_bed_temperature = _getValue("S", line, 0.0, float)
        if mcode is None:
            tcode = _getValue("T", line)
            if tcode is not None:
                self._active_hotend = tcode
        gcode = _getValue("G", line)
        if gcode is not None:
            if gcode == 30:
                ret += "0.98\n"
            elif gcode == 28:
                ret += "home_distance = 100.404"

        log.info("Dummy: %s", line)
        return ret + "ok"

    def _setError(self, error):
        if self._state != "ERROR":
            self._error_condition = error
            self._setState("ERROR")

    def _setState(self, new_state):
        if new_state != self._state:
            self._state = new_state
            self.onStatusChanged.emit(new_state)

    # Queue a line to be executed as soon as there is room in the planner buffer.
    # This should be used to stream a gcode file to the printer
    def queueNormal(self, line):
        if not self.isConnected():
            return
        if ";" in line:
            line = line[:line.find(";")]
        line = line.strip()
        if len(line) > 0:
            self._send_queue.put(line)

    # Send a line to the printer as soon as possible. Does not wait for the planner buffer to have room.
    # This should be used to send commands like temperature changes to the printer.
    # @param line: The GCode line that needs to be send to Marlin
    def queueInstant(self, line):
        if not self.isConnected():
            return
        if ";" in line:
            line = line[:line.find(";")]
        line = line.strip()
        if len(line) > 0:
            self._instant_queue.put((line, None))

    # Send a line to the printer as soon as possible, and catch the reply.
    # This function blocks till the reply is received.
    # @param line The line of data to send to Marlin.
    # @return The reply is returned as an array with lines.
    def queueAndWaitForReply(self, line):
        if not self.isConnected():
            return "NOT CONNECTED"
        if ";" in line:
            line = line[:line.find(";")]
        line = line.strip()
        if len(line) > 0:
            e = thread.Event()
            self._instant_queue.put((line, lambda reply: self.__handleSendReply(e, reply)))
            e.wait()
            return e.reply
        return ""

    def __handleSendReply(self, e, reply):
        e.reply = reply
        e.set()

    # Abort the current queue and all moves in Marlin. This causes a full stop of the machine.
    # Note that any position tracking at this point is invalid.
    def abort(self):
        while True:
            try:
                self._send_queue.get(False)
            except queue.Empty:
                break

    def waitForQueueToBeEmpty(self, wait_for_motion_controller=False):
        self._send_queue.join()

    # @return: bool, True if communication is possible, False if there is no communication possible yet in the startup, or the whole communication fails.
    def isConnected(self):
        return self._state != "DISCONNECTED"

    ## @briefs Gets the size of the instant or normal queue, depending on the flag
    #  @param get_instant_queue_size If True, the size of the instant queue will be returned, otherwise the size of the normal queue
    #  @return Returns the size of the specified queue
    def getQueueSize(self, *, get_instant_queue_size=False):
        if get_instant_queue_size:
            return self._instant_queue.qsize()
        else:
            return self._send_queue.qsize()

    # @return: A dictionary containing debugging information about the queue. Should only be used for debugging!
    def debugGetInfo(self):
        return {"type": "dummy"}
