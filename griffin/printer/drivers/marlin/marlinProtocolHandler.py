import time
import os
import logging
import re

from griffin import thread
from griffin import signal

from .marlinLowLevelCommunication import MarlinLowLevelCommunication


log = logging.getLogger(__name__.split(".")[-1])



# Local function used to extract a value behind a "key" in GCode lines.
def _getValue(key, line, default=None, return_type=int):
    res = re.search("%s([0-9\.]+)" % (key), line)
    if res is None:
        return default
    try:
        return return_type(res.group(1))
    except ValueError:
        return default


## MarlinProtocolHandler
#  This object handles all the nasty complex details of dealing with the Marlin protocol.
#  It sends data to Marlin, and reads back the results. It keeps track of which reply belongs to which results.
#  And it accounts for resend requests.
class MarlinProtocolHandler:
    # Amount of moves that marlin can put in the planner before a motion move will block the communications.
    __MARLIN_PLANNER_BUFFER_SIZE = 15
    # Amount of lines marlin can receive in its buffer without causing buffer overflows.
    __MARLIN_RECEIVE_BUFFER_SIZE = 4
    # Maximum line number (Which is actually a sequence number) before we reset the line numbering to 1.
    __MAX_LINE_NUMBER = 1000

    def __init__(self, controller, port_name, queue):
        self.__controller = controller
        self.__queue = queue
        self.__idle_handler = None
        self.__comm = MarlinLowLevelCommunication(port_name)
        self.__communication_thread = thread.Thread("Serial communication thread", self.__communicationThreadFunction)

        self.__planner_buffer_space = MarlinProtocolHandler.__MARLIN_PLANNER_BUFFER_SIZE
        self.__connected = False
        self.__received_data = []
        self.__last_received_data_time = time.monotonic()
        self.__resend_number = None
        self.__resend_data = []
        self.__current_line_number = 1
        # A list of all messages that have been send, but still need a reply. Each entry contains a tuple with (line number, message, reply_handler)
        self.__send_messages_that_still_need_ack = []

        # Hack to prevent problems with the communications when probing. When probing the bed the communication is stalled for a long time. So we need to disable the timeout.
        self.__is_probing = False

        self.onConnect = signal.Signal(type=signal.Signal.ASync)
        self.onDisconnect = signal.Signal(type=signal.Signal.ASync)
        self.onError = signal.Signal(type=signal.Signal.ASync)
        self.onWarning = signal.Signal(type=signal.Signal.ASync)

    # Set the "idle command handler", which is a callback handler that is called whenever the communication needs
    # a command to send to Marlin to keep the communication active. This handler is called when the planner buffer is full and there is no instant gcode to send.
    # This handler function should return a tuple containing the GCode line and a reply handler for this line of gcode.
    # @param: handler, a function which will be called when there is no command to send to Marlin.
    def setIdleCommandHandler(self, handler):
        self.__idle_handler = handler

    # Start the communication with Marlin. This is not started in the __init__ so the owner of this object has time to register callbacks.
    def start(self):
        self.__communication_thread.start()

    # @return: bool, True if we are properly connected with Marlin and communication is possible. False if we are still trying to connect.
    def isConnected(self):
        return self.__connected

    # @return: The amount of movements currently still planned to be executed by Marlin. When this is 0 the printer is standing still.
    def getPlannedMovementCount(self):
        return MarlinProtocolHandler.__MARLIN_PLANNER_BUFFER_SIZE - self.__planner_buffer_space

    def __communicationThreadFunction(self):
        # Before we do any communication, check and possibly update the firmware in the marlin board.
        self.__handleFirmwareUpdate()

        while True:
            while not self.__comm.isOpen():
                self.__comm.open()
            self.__communicationTryReceive()
            self.__communicationTrySend()

    def __communicationTryReceive(self):
        try:
            line = self.__comm.readline()
        except:
            # If we get an exception during the serial read, we better close the serial port.
            log.exception("Exception during serial read, closing serial port.")
            self.__comm.close()
            self.__connected = False
            self.onDisconnect.emit()
            line = ""

        if line != "":
            self.__last_received_data_time = time.monotonic()
            self.__processIncommingData(line.strip())
        else:
            self.__processReceiveTimeout(time.monotonic() - self.__last_received_data_time)

    def __processIncommingData(self, line):
        if line.startswith("LOG:"):
            log.info(line[4:])
            return
        log.debug("Recv: %s", line)
        if "Error:" in line or "ERROR" in line:
            log.warning("Got error line from printer: %s", line)
            self.onError.emit(line.split(":")[1].strip(), line)
        if "Warning:" in line or "WARNING:" in line:
            log.warning("Got warning line from printer: %s", line)
            self.onWarning.emit(line.split(":")[1].strip(), line)
        if not self.__connected:
            if "ok" in line:
                # Reset any errors that might have happened already. If the error is still there, this will trigger a new error message which we can capture properly.
                # Else the error message might have been send before we where listening.
                self.__comm.write("M999\n")

                # Initialize the line numbering with M110. Without this we cannot send properly lines with checksums.
                self.__comm.writeChecksumLine(self.__current_line_number - 1, "M110")

                # Emit the connect signal, this indicates we are ready to send data now.
                self.__connected = True
                self.onConnect.emit()
        else:
            self.__received_data.append(line)
            if "ok" in line:
                self.__processIncomingAck()

    def __processIncomingAck(self):
        receive_index = None
        resend = None
        log.debug("ProcessACK: %s", str(self.__received_data))
        for line in self.__received_data:
            if "Resend: " in line:
                resend = _getValue("Resend: ", line)
            if "ok" in line:
                n_value = _getValue("N", line)
                if n_value is not None:
                    for index in range(0, len(self.__send_messages_that_still_need_ack)):
                        if self.__send_messages_that_still_need_ack[index][0] == n_value:
                            receive_index = index
                            break
                p_value = _getValue("P", line)
                if p_value is not None and 0 <= p_value <= MarlinProtocolHandler.__MARLIN_PLANNER_BUFFER_SIZE:
                    self.__planner_buffer_space = p_value
        if resend is None and receive_index is not None:
            # If the receive_index is not position 0, then we missed an ack for earlier messages. Be sure to call the callback handlers for these messages.
            for n in range(0, receive_index):
                log.warning("Did not see an ack for line: %d: %s", self.__send_messages_that_still_need_ack[n][0], self.__send_messages_that_still_need_ack[n][1])
                if self.__send_messages_that_still_need_ack[n][2] is not None:
                    self.__send_messages_that_still_need_ack[n][2]("")
            # Call the callback function for the current ack.
            if self.__send_messages_that_still_need_ack[receive_index][2] is not None:
                self.__send_messages_that_still_need_ack[receive_index][2]("\n".join(self.__received_data))
            # Remove the entry and the previous entries from the list of messages that still need an ack.
            self.__send_messages_that_still_need_ack = self.__send_messages_that_still_need_ack[receive_index + 1:]
        elif resend is not None:
            self.__handleResend(resend)
        self.__received_data = []

    def __handleResend(self, resend_nr):
        if self.__resend_number is None:
            log.debug("Marlin request to resend: %d %s", resend_nr, repr(self.__send_messages_that_still_need_ack))
            self.__resend_number = resend_nr

    def __processReceiveTimeout(self, timeout):
        if not self.__connected:
            if timeout > 1.0:
                self.__last_received_data_time = time.monotonic() # Reset the last_received_data_time here to prevent a flood of M105 requests
                log.debug("Sending M105 during connection phase.")
                self.__comm.write("M105\n")
        elif self.__resend_number is not None:
            if timeout > 0.3:
                index = None
                for n in range(0, len(self.__send_messages_that_still_need_ack)):
                    if self.__send_messages_that_still_need_ack[n][0] == self.__resend_number:
                        index = n
                        break
                if index is None:
                    index = 0
                    if len(self.__send_messages_that_still_need_ack) > 0:
                        while self.__send_messages_that_still_need_ack[0][0] > self.__resend_number:
                            self.__send_messages_that_still_need_ack.insert(0, (self.__send_messages_that_still_need_ack[0][0] - 1, "M105", None))
                log.debug("Planning resend data %d %d", index, len(self.__send_messages_that_still_need_ack))
                self.__resend_data += self.__send_messages_that_still_need_ack[index:]
                self.__send_messages_that_still_need_ack = self.__send_messages_that_still_need_ack[:index]
                self.__resend_number = None
                self.__resend_data.sort(key=lambda data: data[0])
        else:
            if timeout > 15.0 and not self.__is_probing:
                self.__last_received_data_time = time.monotonic()
                log.debug("Sending M105 because communication looks stalled.")
                self.__comm.write("M105\n")

    def __communicationTrySend(self):
        if self.__connected and len(self.__send_messages_that_still_need_ack) < MarlinProtocolHandler.__MARLIN_RECEIVE_BUFFER_SIZE and self.__resend_number is None:
            if len(self.__resend_data) > 0:
                line_number, send_line, send_ack_handle_function = self.__resend_data.pop(0)
                self.__comm.writeChecksumLine(line_number, send_line)
                self.__send_messages_that_still_need_ack.append((line_number, send_line, send_ack_handle_function))
            else:
                if self.__current_line_number > MarlinProtocolHandler.__MAX_LINE_NUMBER:
                    # If we are over 1000 lines, reset the line numbering. So we do not overflow the line number counter.
                    # (Marlin currently does not handler proper wrap-around)
                    send_line = "M110"
                    send_ack_handle_function = None
                    self.__current_line_number = 1
                else:
                    # If we have room in the planner, get something from the queue to send.
                    send_line, send_ack_handle_function = self.__queue.get(self.__planner_buffer_space > 0)
                    if send_line is not None:
                        # If we got something from the queue, we have less room in the planner buffer when we send this. So already account for this.
                        self.__planner_buffer_space -= 1
                if send_line is None and self.__idle_handler is not None:
                    send_line, send_ack_handle_function = self.__idle_handler(self.__current_line_number)
                if send_line is not None:
                    if "G30" in send_line:   # Dirty hack, disable the timeout handling while probing with the capacitive sensor
                        self.__is_probing = True
                        send_ack_handle_function = self.__generateProbingFinishedFunction(send_ack_handle_function)
                    self.__comm.writeChecksumLine(self.__current_line_number, send_line)
                    self.__send_messages_that_still_need_ack.append((self.__current_line_number, send_line, send_ack_handle_function))
                    self.__current_line_number += 1

    def __generateProbingFinishedFunction(self, ack_handle_function):
        return lambda reply: self.__probingFinished(reply, ack_handle_function)

    def __probingFinished(self, reply, ack_function):
        self.__is_probing = False
        if ack_function is not None:
            ack_function(reply)

    def __handleFirmwareUpdate(self):
        fup = self.__controller.getProcedure("FIRMWARE_UPDATE")
        while fup is None:
            time.sleep(0.5)
            fup = self.__controller.getProcedure("FIRMWARE_UPDATE")
        firmware_file = self.__controller.getPropertyValue("marlin_firmware_filename")
        firmware_file = os.path.join(os.path.dirname(__file__), "..", "..", "..", "machines", firmware_file)
        fup.setup({"file": firmware_file})
        fup.start()
        while fup.isActive():
            time.sleep(0.5)

    # @return: A dictionary containing debugging information about the queue. Should only be used for debugging!
    def debugGetInfo(self):
        result = {
            "planner_buffer_space": self.__planner_buffer_space,
            "last_received_data_time": self.__last_received_data_time - time.monotonic(),
            "connected": self.__connected,
            "current_line_number": self.__current_line_number,
            "is_probing": self.__is_probing,
        }
        if len(self.__send_messages_that_still_need_ack) > 0:
            result["send_messages_that_still_need_ack"] = {}
            for line_number, message, callback in self.__send_messages_that_still_need_ack:
                result["send_messages_that_still_need_ack"]["%d" % line_number] = message
        return result
