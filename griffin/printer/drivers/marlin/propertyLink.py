from griffin.timer import Timer
import logging
log = logging.getLogger(__name__.split(".")[-1])


## Class to link a property to a GCode command.
#  This class makes sure that a change in a property is reflected by a change in Marlin
#  To prevent communication channels from blocking there are a few different ways this class can handle updating Marlin.
class PropertyLink:
    # Direct queue update, every change to this property is directly queued into the Marlin queue.
    # this should be used for properties that hardly change, or need to be in perfect sync with the movement GCodes.
    DIRECT_QUEUE = "direct"
    # delayed queue update, this sends the update as quickly as possible. But will delay the update if the previous update has not been acknowledged yet.
    # This should be used for properties that can vary very quickly because of user input, but not every value change needs to be replicated to Marlin.
    # For example tuning parameters.
    DELAYED_QUEUE = "delayed"

    # @param driver: the MarlinDriver object which will be used to send the gcode command.
    # @param queuing_method: One of DIRECT_QUEUE or DELAYED_QUEUE. See those items for details how these work.
    # @param property: A property instance (not the key/name) to monitor for changes.
    # @param gcode_format_string: A string containing a single "%" format entry which will be formatted with the value of the property before the GCode line is send.
    # @param conversion_to_gcode_function  Sets an optional function to convert a human readable value to the value-range used by gcode (E.G: 0-100 percentage based range to to 0-255 range).
    def __init__(self, driver, queuing_method, property, gcode_format_string, conversion_to_gcode_function=lambda value: value ):
        assert queuing_method in (PropertyLink.DIRECT_QUEUE, PropertyLink.DELAYED_QUEUE)
        assert property is not None
        assert "%" in gcode_format_string

        self.__driver = driver
        self.__queuing_method = queuing_method
        self.__property = property
        self.__gcode_format_string = gcode_format_string
        self.__property.onChange.connect(self.__onPropertyChange)

        self.__command_in_transmission = False
        self.__require_send_after_previous_handled = False
        self.__conversion_to_gcode_function = conversion_to_gcode_function

    # Force the linked property to be updated to Marlin. This is used during the startup of the system to guarantee Marlin has the same value for properties as the printer service.
    def forceUpdate(self):
        self.__driver.queueNormal(self.__getFormattedGCodeLine())

    # @return: A line of gcode in unicode, with the property value filled in.
    def __getFormattedGCodeLine(self):
        value = self.__property.get()
        if self.__property.get() == "": # an empty string in a property is meant as None so no string needs or can be sent to gcode.
            value = 0

        try:
            return self.__gcode_format_string % (self.__conversion_to_gcode_function(value))
        except TypeError:
            log.exception("attempted to format the following line %s with %s for property %s", self.__gcode_format_string, value, self.__property.getKey())

        return ""

    # Called when the linked property is changed.
    # Used to queue the GCode line to marlin if required, different actions depending on the queuing method.
    def __onPropertyChange(self, property_key, new_value):
        if self.__queuing_method == PropertyLink.DIRECT_QUEUE:
            self.__driver.queueNormal(self.__getFormattedGCodeLine())
        else:
            if not self.__command_in_transmission:
                self.__command_in_transmission = True
                if self.__queuing_method == PropertyLink.DELAYED_QUEUE:
                    if not self.__driver.queueNormal(self.__getFormattedGCodeLine(), callback_function=self.__onCommandDoneOrResent, allow_block=False):
                        self.__command_in_transmission = False
                        self.__require_send_after_previous_handled = True
                        Timer("PropertyLink(%s)" % self.__property.getKey(), 0.5, self.__onCommandDoneOrResent, None).start()
                else:
                    assert True, "programmer error"
            else:
                self.__require_send_after_previous_handled = True

    # Called once the previous gcode line was sent or when the previous command to send gcode was blocking.
    # When A new change has already been received, this is sent ASAP, but only one change is in the pipeline at a time.
    # @param reply_from_marlin unused parameter added to satisfy callback requirements
    def __onCommandDoneOrResent(self, reply_from_marlin):
        self.__command_in_transmission = False
        if self.__require_send_after_previous_handled:
            if self.__driver.queueInstant(self.__getFormattedGCodeLine(), callback_function=self.__onCommandDoneOrResent, allow_block=False):
                self.__require_send_after_previous_handled = False
                self.__command_in_transmission = True
            else:
                Timer("PropertyLink(%s)" % self.__property.getKey(), 0.5, self.__onCommandDoneOrResent, None).start()
