import random
import logging
from serial import Serial

log = logging.getLogger(__name__.split(".")[-1])

rand = random.Random(0)


# The TestSerial class is a wrapper around the pyserial Serial class which introduces extra read/write errors.
#  This class is only used during development to test what happens when transmission errors happen.
class TestSerialWrapper(Serial):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._send_error_rate = 0.01
        self._recv_error_rate = 0

    def write(self, data):
        for n in range(0, len(data)):
            if rand.uniform(0, 100) < self._send_error_rate:
                data = data[:n] + int(rand.randint(0, 255)).to_bytes(1, "big") + data[n + 1:]
                log.critical("Inserting error: %s", data)
        return super().write(data)

    def read(self, size=1):
        ret = super().read(size)
        for n in range(0, len(ret)):
            if rand.uniform(0, 100) < self._recv_error_rate:
                ret = ret[:n] + int(rand.randint(0, 255)).to_bytes(1, "big") + ret[n + 1:]
        return ret
