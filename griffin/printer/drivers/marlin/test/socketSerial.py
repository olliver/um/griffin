import socket


## This class provides the same interface as the default serial.Serial from pyserial.
#  But then provides a socket as communication method instead of talking directly to serial hardware.
class SocketSerial:
    def __init__(self, host, port):
        self.__socket = socket.create_connection((host, port))
        self.__socket.setblocking(False)
        self.__read_buffer = b""

    def setParity(self, parity):
        pass

    def flush(self):
        pass

    def flushInput(self):
        pass

    def readline(self):
        while True:
            try:
                data = self.__socket.recv(1)
            except BlockingIOError:
                return b""
            self.__read_buffer += data
            if self.__read_buffer.endswith(b"\n"):
                result = self.__read_buffer
                self.__read_buffer = b""
                return result

    def close(self):
        self.__socket.close()

    def write(self, data):
        self.__socket.send(data)
