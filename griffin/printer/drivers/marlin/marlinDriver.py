import time
import re
import logging

from griffin import thread
from griffin import signal
from griffin import dataLogger
from griffin.printer.faultHandler import FaultHandler, FaultCode

from .marlinQueue import MarlinQueue
from .marlinProtocolHandler import MarlinProtocolHandler
from .propertyLink import PropertyLink
from griffin.printer.properties.printer.printJobStateProperty import PrintJobStateProperty

log = logging.getLogger(__name__.split(".")[-1])


# This Driver class handles communication with the Marlin firmware.
# Implements two communication queues. One queue is used for the normal printing commands.
# The other queue is used for high priority commands.
# The normal queue is only send when there is room in the planner buffer on the marlin side.
# This to prevent the communication channel from blocking.
# Commands are send with checksums and line numbers to keep track of ordering and replies of commands.
# Resends are implemented by a "stop&go" mechanism. Where the communication is stalled for 0.1 seconds to clear out buffers
class MarlinDriver:
    ##< dictionary used to translate the state strings as numeric values.
    ENCODE_JOBSTATE_AS_NUMBER_TABLE = {
            PrintJobStateProperty.NO_JOB: 10,
            PrintJobStateProperty.PRINTING: 0,
            PrintJobStateProperty.PAUSING: 11,
            PrintJobStateProperty.PAUSED: 12,
            PrintJobStateProperty.RESUMING:13,
            PrintJobStateProperty.PRE_PRINT: 14,
            PrintJobStateProperty.POST_PRINT: 15,
            PrintJobStateProperty.WAIT_CLEANUP: 16
        }

    def __init__(self, controller, port_name):
        super().__init__()
        # Configuration
        self.__controller = controller
        self.__hotend_count = controller.getPropertyValue("hotend_count")

        self.__queue = MarlinQueue()
        self.__protocol_handler = MarlinProtocolHandler(controller, port_name, self.__queue)
        self.__protocol_handler.onConnect.connect(self.__onConnect)
        self.__protocol_handler.onDisconnect.connect(self.__onDisconnect)
        self.__protocol_handler.onError.connect(self.__onError)
        self.__protocol_handler.onWarning.connect(self.__onWarning)
        self.__protocol_handler.setIdleCommandHandler(self.__getIdleCommand)

        self.__last_temp_request = time.monotonic()

        self.__hotend_enabled = [False] * self.__hotend_count
        self.__hotend_present = [None] * self.__hotend_count	# initialize to None to ensure we fire events on both missing and present cartridges.

        self.onBedTemperatureUpdate = signal.Signal(type=signal.Signal.ASync)
        self.onHotendTemperatureUpdate = signal.Signal(type=signal.Signal.ASync)
        # The status changed signal is emitted every time the status of the player changes.
        self.onStatusChanged = signal.Signal(type=signal.Signal.ASync)
        self.onCartridgePresentChanged = signal.Signal(type=signal.Signal.ASync)
        self.onCartridgeEnabledChanged = signal.Signal(type=signal.Signal.ASync)

        data_column_names = []
        for idx in range(0, self.__hotend_count):
            data_column_names += [
                "temperature%d" % (idx),
                "target%d" % (idx),
                "heater%d" % (idx),
                "flow_sensor%d" % (idx),
                "flow_steps%d" % (idx)
            ]
        data_column_names += [
            "bed_temperature",
            "bed_target",
            "bed_heater",
            "active_hotend_or_state"
        ]
        self.__data_logger = dataLogger.DataLogger("TemperatureFlow", data_column_names)

    # Start the motion controller driver.
    # This registers the property links as well, as at this point in time the properties are all created in the system.
    def start(self):
        pc = self.__controller.getPropertyContainer()

        self.__property_links = [
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("build_volume_x"), "M12000 X%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("build_volume_y"), "M12000 Y%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("build_volume_z"), "M12000 Z%g"),

            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("feature_filament_flow_sensor"), "M12002 S%d"),

            PropertyLink(self, PropertyLink.DELAYED_QUEUE, pc.get("movement_speed_modifier"), "M220 S%g"),
            PropertyLink(self, PropertyLink.DELAYED_QUEUE, pc.get("extrusion_amount_modifier"), "M221 S%g"),

            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("acceleration_xzy"), "M204 S%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("acceleration_e"), "M204 T%g"),

            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("max_speed_x"), "M203 X%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("max_speed_y"), "M203 Y%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("max_speed_z"), "M203 Z%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("max_speed_e"), "M203 E%g"),

            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("jerk_xy"), "M205 Y%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("jerk_z"), "M205 Z%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("jerk_e"), "M205 E%g"),

            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("motor_current_xy"), "M907 X%d"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("motor_current_z"), "M907 Z%d"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("motor_current_e"), "M907 E%d"),

            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("steps_per_mm_x"), "M92 X%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("steps_per_mm_y"), "M92 Y%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("steps_per_mm_z"), "M92 Z%g"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("steps_per_mm_e"), "M92 E%g"),

            # M290 P<powerBudget> I<idle_power_consumption>
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("total_power_budget"), "M290 P%f"),
            PropertyLink(self, PropertyLink.DIRECT_QUEUE, pc.get("idle_power_consumption"), "M290 I%f"),

            PropertyLink(self, PropertyLink.DELAYED_QUEUE, self.__controller.getPrintHeadController().getPropertyContainer().get("cooling_fan_speed"), "M106 S%d", lambda value: int(value * 255 / 100))
        ]
        for n in range(0, self.__hotend_count):
            hotend_slot = self.__controller.getPrintHeadController().getHotendSlot(n)
            self.__property_links.append(PropertyLink(self, PropertyLink.DELAYED_QUEUE, hotend_slot.getPropertyAsObject("target_temperature"), "M104 T%d S%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_Kff"), "M301 T%d F%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_Kp"), "M301 T%d P%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_Ki"), "M301 T%d I%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_Ki_max"), "M301 T%d i%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_Kd"), "M301 T%d D%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_Kpcf"), "M301 T%d C%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_Ke"), "M301 T%d E%%g" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("pid_functional_range"), "M301 T%d R%%g" % (n)))
            # M292 T<hotend_nr> P<max_power> R<nominal_resistance> V<voltage>
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("max_power"), "M292 T%d P%%f" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("voltage"), "M292 T%d V%%f" % (n)))
            self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, hotend_slot.getPropertyAsObject("nominal_resistance"), "M292 T%d R%%f" % (n), lambda x: 1 if x == "" else x ))

        heated_bed = self.__controller.getBed()
        self.__property_links.append(PropertyLink(self, PropertyLink.DELAYED_QUEUE, heated_bed.getPropertyAsObject("target_temperature"), "M140 S%g"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("pid_Kff"), "M304 F%g"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("pid_Kp"), "M304 P%g"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("pid_Ki"), "M304 I%g"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("pid_Ki_max"), "M304 i%g"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("pid_Kd"), "M304 D%g"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("pid_Kpcf"), "M304 C%g"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("pid_functional_range"), "M304 R%g"))
        # M291  R<nominal_bed_resistance> A<bed_resistance_per_degree> V<bed_voltage>
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("nominal_resistance"), "M291 R%f"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("resistance_per_degree"), "M291 A%f"))
        self.__property_links.append(PropertyLink(self, PropertyLink.DIRECT_QUEUE, heated_bed.getPropertyAsObject("voltage"), "M291 V%f"))

        self.__protocol_handler.start()

    # Get an idle command handler.
    # @param current_line_number: Current communication line number from the Marlin protocol handler. This is a hack to prevent issue we have with send/reply mismatches that happen sometimes.
    # @return: A tuple containing a line of gcode and a callback function to handle the result of this line of gcode. Or None if we do not want to send a command right now.
    def __getIdleCommand(self, current_line_number):
        if time.monotonic() - self.__last_temp_request < 0.1:
            return None, None
        self.__last_temp_request = time.monotonic()
        send_line = "M105"
        send_ack_handle_function = lambda reply: self.__handleTemperatureReply(current_line_number, reply)
        return send_line, send_ack_handle_function

    ## Surprise, surprise! this function handles a reply from the M105 temperature request!
    #  Really, it does. You wouldn't expect this.
    #  @param expected_line_number: This parameter contains the line number that was expected. Yes, really. Added because this sometimes does not match with the actual returned line number. Which indicates a bug somewhere. Currently unused.
    #  @param reply: You wouldn't expect it, but this parameter contains the reply from Marlin. As a string.
    def __handleTemperatureReply(self, expected_line_number, reply):
        log_data = [0.0, 0.0, 0.0, 0.0, 0.0] * self.__hotend_count + [0.0, 0.0, 0.0, 0]
        # Parsing: "T{int}:{float}/{float}@{int}p{int}e{int}f{int}/{int}"
        for m in re.finditer("T(\d+):(-?\d+(?:\.\d+)?)/(-?\d+(?:\.\d+)?)@(-?\d+)p(-?\d+)e(-?\d+)f(-?\d+)/(-?\d+)", reply):
            try:
                hotend_nr = int(m.group(1))
                current_temperature = float(m.group(2))
                target_temperature = float(m.group(3))
                heater_output = int(m.group(4))
                present = bool(int(m.group(5)))
                enabled = bool(int(m.group(6)))
                flow_sensor = int(m.group(7))
                flow_steps = int(m.group(8))

                log_data[hotend_nr * 5 + 0] = current_temperature
                log_data[hotend_nr * 5 + 1] = target_temperature
                log_data[hotend_nr * 5 + 2] = heater_output
                log_data[hotend_nr * 5 + 3] = flow_sensor
                log_data[hotend_nr * 5 + 4] = flow_steps

                self.onHotendTemperatureUpdate.emit(hotend_nr, current_temperature)

                if present != self.__hotend_present[hotend_nr]:
                    self.__hotend_present[hotend_nr] = present
                    self.onCartridgePresentChanged.emit(hotend_nr, self.__hotend_present[hotend_nr])
                if enabled != self.__hotend_enabled[hotend_nr]:
                    self.__hotend_enabled[hotend_nr] = enabled
                    self.onCartridgeEnabledChanged.emit(hotend_nr, self.__hotend_enabled[hotend_nr])
            except:
                log.warning("failed parsing temperature: %s", reply)
                return

        # Parsing: "B{float}/{float}@{int}"
        m = re.search("B(-?\d+(?:\.\d+)?)/(-?\d+(?:\.\d+)?)@(-?\d+)", reply)
        if m:
            try:
                current_bed_temperature = float(m.group(1))
                target_bed_temperature = float(m.group(2))
                heater_output = int(m.group(3))
                self.onBedTemperatureUpdate.emit(current_bed_temperature)

                log_data[self.__hotend_count * 5 + 0] = current_bed_temperature
                log_data[self.__hotend_count * 5 + 1] = target_bed_temperature
                log_data[self.__hotend_count * 5 + 2] = heater_output
            except:
                log.warning("failed parsing temperature: %s", reply)

        job = self.ENCODE_JOBSTATE_AS_NUMBER_TABLE[self.__controller.getPropertyValue("job_state")]
        log_data[self.__hotend_count * 5 + 3] = self.__controller.getActiveHotend() if job == 0 else job
        self.__data_logger.addData(*log_data)

    #  @param error: The error string from Marlin.
    #  @param line: The full line containing the error from marlin.
    def __onError(self, error, line):
        self.__addWarningOrError(FaultHandler.error, error, line)

    #  @param error: The warning string from Marlin.
    #  @param line: The full line containing the warning from marlin.
    def __onWarning(self, error, line):
        self.__addWarningOrError(FaultHandler.warning, error, line)

    ## General function for both error or warning handling from Marlin.
    #  @param handler: A static function of the FaultHandler class, containing which level needs to be added there.
    #  @param error: The error/warning string from Marlin.
    #  @param line: The full line containing the error/warning from marlin.
    def __addWarningOrError(self, handler, error, line):
        fc = FaultCode.decode(error) or FaultCode.MARLIN_GENERIC
        data = {}
        try:
            # This is dirty, but in some cases marlin reports a hotend nr after the error seperated by a :
            # So we catch this as a general case for all errors/warning and pretty much hope we do not need to give other parameters to errors in the future.
            parameter = int(line.split(":")[2])
            data["hotend_nr"] = parameter
            data["display_hotend_nr"] = parameter + 1
        except Exception:
            pass

        handler(str(line), fc, data=data)

    def __onConnect(self):
        self.onStatusChanged.emit("CONNECTED")
        self.queueInstant("M115", callback_function=log.info)

        # After we connect to Marlin and we notice it can receive commands. We need to configure Marlin with our initial state.
        for property_link in self.__property_links:
            property_link.forceUpdate()

    def __onDisconnect(self):
        self.onStatusChanged.emit("DISCONNECTED")

    # Queue a line to be executed as soon as there is room in the planner buffer.
    # This should be used to stream a gcode file to the printer
    def queueNormal(self, line, callback_function=None, allow_block=True):
        if not self.isConnected():
            return False
        return self.__queue.putNormal(line, callback_function, allow_block=allow_block)

    # Wait for the transmit queue to the printer to be empty
    # @param max_planned_motions An optional parameter for limiting the maximum number of active motions in the controller.
    #        When you want to make sure the motion_controller is finished you specify this as 0.
    def waitForQueueToBeEmpty(self, max_planned_motions=None):
        self.__queue.join()
        if max_planned_motions is not None:
            # no M400 because it blocks the communication channel and potentially generates error prone situations (reply -> command matching breaks)
            while self.__protocol_handler.getPlannedMovementCount() > max_planned_motions:
                time.sleep(0.1)

    # Send a line to the printer as soon as possible. Does not wait for the planner buffer to have room.
    # This should be used to send commands like temperature changes to the printer.
    # @param line: The GCode line that needs to be send to Marlin
    # @param callback_function: A function which will be called with the reply from marlin when this command is fully handled, it will get a single parameter which is the full string containing the reply from Marlin.
    # @param allow_block: boolean, true if the send command should block if the queue is full. False if the send function should return false if the queue is full.
    # @return bool: True if the command was put in the instant queue and will be send soon. False if it was rejected, only possible if allow_block is False or Marlin is not connected yet.
    def queueInstant(self, line, *, callback_function=None, allow_block=True):
        if not self.isConnected():
            return False
        return self.__queue.putInstant(line, callback_function, allow_block=allow_block)

    # Send a line to the printer as soon as possible, and catch the reply.
    # This function blocks till the reply is received.
    # @param line The line of data to send to Marlin.
    # @return The reply is returned as an array with lines.
    def queueAndWaitForReply(self, line):
        if not self.isConnected():
            return "NOT CONNECTED"
        e = thread.Event()
        e.reply = None
        self.__queue.putInstant(line, lambda reply: self.__handleSendReply(e, reply))
        e.wait()
        return e.reply

    def __handleSendReply(self, e, reply):
        e.reply = reply
        e.set()

    # Abort the current queue and all moves in Marlin. This causes a full stop of the machine.
    # Note that any position tracking at this point is invalid.
    def abort(self):
        # Get all the items in the current queue and just drop them.
        for item, function in iter(self.__queue.get, (None, None)):
            if function is not None:
                function("ABORT")
        self.queueNormal("M401")  # Send a quickstop to abort the moves inside Marlin planner.

    # @return: bool, True if communication is possible, False if there is no communication possible yet in the startup, or the whole communication fails.
    def isConnected(self):
        return self.__protocol_handler.isConnected()

    ## @briefs Gets the size of the instant or normal queue, depending on the flag
    #  @param get_instant_queue_size If True, the size of the instant queue will be returned, otherwise the size of the normal queue
    #  @return Returns the size of the specified queue
    def getQueueSize(self, *, get_instant_queue_size=False):
        if get_instant_queue_size:
            return self.__queue.getInstantQueueSize()
        else:
            return self.__queue.getNormalQueueSize()

    # @return: A dictionary containing debugging information about the queue. Should only be used for debugging!
    def debugGetInfo(self):
        result = {
            "type": "marlinDriver",
            "protocol_info": self.__protocol_handler.debugGetInfo(),
            "queue_info": self.__queue.debugGetInfo(),
        }
        return result
