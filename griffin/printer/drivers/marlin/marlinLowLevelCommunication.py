import logging
import time
import functools

from serial import Serial
from .test.socketSerial import SocketSerial

# Enable to test the data communication stability with an introduced error rate.
# from .test.testSerialWrapper import TestSerialWrapper as Serial

log = logging.getLogger(__name__.split(".")[-1])


## Lowest level of communication to the Marlin board.
#  Handles the opening and setting up of serial ports and the error checking related to that.
#  Also has a function for the low level checksum lines.
class MarlinLowLevelCommunication:
    __BAUDRATE = 250000     # Baudrate which is used to communicate with Marlin.

    # Create a new MarlinLowLevelCommunication object.
    # @port_name: The serial port name to communicate with, as a unicode string. Usually "/dev/ttyS1". For desktop simulation "socket:[port]" can be used to connect to a socket on your local machine.
    def __init__(self, port_name):
        self.__port_name = port_name
        self.__serial = None

    # Try to open the serial port. This function blocks till the opening of the port was successful.
    def open(self):
        log.info("Opening serial port: %s", self.__port_name)
        while self.__serial is None:
            try:
                if self.__port_name.startswith("socket:"):
                    self.__serial = SocketSerial("127.0.0.1", int(self.__port_name.split(":", 1)[-1]))
                else:
                    self.__serial = Serial(self.__port_name, self.__BAUDRATE, timeout=0.1)
                # After opening, wait a short while and flush the input buffers. We could still have old data from the previous "run" of Marlin in buffers.
                # So we want to discard that.
                time.sleep(1.0)
                self.__serial.flushInput()
            except TypeError as err:
                log.exception("Failed to open serial port: %s. Reason: %s", self.__port_name, err)
                log.error("Most likely there you see a division here somewhere in pyserial. Which means you need to update your pyserial package.")
                log.error("This is already done for the latest full image update. So you could do an [update firmware] got get this package.")
                time.sleep(30.0)
            except Exception as err:
                log.exception("Failed to open serial port: %s. Reason: %s", self.__port_name, err)
                time.sleep(30.0)

        # For some reason some linux versions have problems with serial ports not configuring correctly.
        # Setting and unsetting the parity bit works around this issue.
        self.__serial.setParity("E")
        self.__serial.setParity("N")
        time.sleep(0.5)
        self.__serial.flush()
        self.__serial.readline()

    # Close the currently open serial port.
    def close(self):
        log.warning("Closing serial port")
        if self.__serial is not None:
            self.__serial.close()
        self.__serial = None

    # @return: bool, indicate if the serial port is opened or not.
    def isOpen(self):
        return self.__serial is not None

    # Write raw data to the serial port.
    # @param: data, a unicode string to send through the serial port. Note that this is converted to 7 bit ascii before sending.
    def write(self, data):
        try:
            log.debug("Sending: %s", data)
            self.__serial.write(data.encode("ascii", "replace"))
        except:
            log.exception("Failure during serial write.")

    # Write a line of GCode to marlin. With the proper checksum protocol.
    # @param: line_number, integer to indicate the current line number.
    # @param: line, unicode string containing the data to send, without a newline.
    def writeChecksumLine(self, line_number, line):
        with_line_num = "N%d %s" % (line_number, line)
        checksum = functools.reduce(lambda x, y: x ^ y, map(ord, with_line_num))
        data = with_line_num + "*%d\n" % checksum
        self.write(data)

    # Read a full line from the serial port, this function does not block.
    # @return: A full line read back from Marlin, or an empty string in case no data is available.
    def readline(self):
        return self.__serial.readline().decode("ascii", "replace")
