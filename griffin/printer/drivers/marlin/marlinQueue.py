import logging
import queue

log = logging.getLogger(__name__.split(".")[-1])


## Class which handles queue towards the marlin driver.
#  We provide 2 different queue. The normal queue where movements are planned. And an instant queue.
#  If there are items in the instant queue, these are ALWAYS send first before there is data send from the normal queue.
class MarlinQueue:
    # Default size of the normal queue. We want to block pretty quick if this queue is full, so we only make it a size of 4, which matches the amount of lines
    # that can be send to Marlin when the communication buffers on the Marlin side are empty.
    __DEFAULT_NORMAL_QUEUE_SIZE = 4
    # The default size of the instant queue, we make this 10 items large, as this queue is emptied ASAP, we generally do not want to block when putting things in this queue.
    __DEFAULT_INSTANT_QUEUE_SIZE = 10

    # @param normal_queue_size: Size of the normal queue as integer.
    # @param instant_queue_size: Size of the instant queue as integer.
    def __init__(self, normal_queue_size=__DEFAULT_NORMAL_QUEUE_SIZE, instant_queue_size=__DEFAULT_INSTANT_QUEUE_SIZE):
        self.__normal_queue = queue.Queue(normal_queue_size)
        self.__instant_queue = queue.Queue(instant_queue_size)

    # Put a GCode into the normal queue. This command is handled in the normal sequence and properly accounted for with the planner buffer.
    # Moves should go into the normal queue.
    # @param: data, a line of GCode without the newline as unicode string.
    # @param: result_handler, a callback function which will be called with the answer from Marlin when this command is handled. Or None if no function needs to be called.
    # @return: bool, True if this command was queued, False if this put would block but allow_block is False.
    def putNormal(self, data, result_handler, *, allow_block=True):
        return self.__put(self.__normal_queue, data, result_handler, allow_block)

    # Put a GCode into the instant queue. This command bypasses the normal queue and is send fore the normal queue is handled.
    # Movement command should not be put into this queue as this queue bypasses the planner buffer handling.
    # @param: data, a line of GCode without the newline as unicode string.
    # @param: result_handler, a callback function which will be called with the answer from Marlin when this command is handled. Or None if no function needs to be called.
    # @return: bool, True if this command was queued, False if this put would block but allow_block is False.
    def putInstant(self, data, result_handler, *, allow_block=True):
        return self.__put(self.__instant_queue, data, result_handler, allow_block)

    # Retrieve an item from the queue.
    # @return: A tuple containing the GCode line and the result handler function.
    def get(self, allow_normal_queue=True):
        try:
            data, result_handler = self.__instant_queue.get(False)
            self.__instant_queue.task_done()
            return data, result_handler
        except queue.Empty:
            pass
        if allow_normal_queue:
            try:
                data, result_handler = self.__normal_queue.get(False)
                self.__normal_queue.task_done()
                return data, result_handler
            except queue.Empty:
                pass
        return None, None

    # Blocks till all items from the queue are handled.
    def join(self):
        self.__instant_queue.join()
        self.__normal_queue.join()

    ## @brief Gets the queue size for the instant queue
    #  @return The instant queue size
    def getInstantQueueSize(self):
        return self.__instant_queue.qsize()

    ## @brief Gets the queue size for the normal queue
    #  @return The normal queue size
    def getNormalQueueSize(self):
        return self.__normal_queue.qsize()

    # Private put function to generalize the putNormal and putInstant into a single piece of code. Puts a line of gcode with a result handler in a queue.
    # @param put_queue: the queue in which the gcode and result handler will be put.
    # @param: data, a line of GCode without the newline as unicode string.
    # @param: result_handler, a callback function which will be called with the answer from Marlin when this command is handled. Or None if no function needs to be called.
    # @return: bool, True if this command was queued, False if this put would block but allow_block is False.
    def __put(self, put_queue, data, result_handler, allow_block):
        data = self.__stripGCode(data)
        if len(data) > 0:
            if allow_block:
                put_queue.put((data, result_handler))
            else:
                try:
                    put_queue.put((data, result_handler), block=False)
                except queue.Full:
                    return False
        else:
            result_handler("")
        return True

    # Strip comments from the GCode and beginning/end whitepspace which does not do anything for the GCode handler in Marlin.
    # This just reduces data transfers a bit.
    # @param data: string containing gcode.
    # @return: string containing gcode without whitespace and no comments
    def __stripGCode(self, data):
        if ";" in data:
            data = data[:data.find(";")]
        return data.strip()

    # @return: A dictionary containing debugging information about the queue. Should only be used for debugging!
    def debugGetInfo(self):
        return {
            "instant_queue_size": self.__instant_queue.qsize(),
            "normal_queue_size": self.__normal_queue.qsize(),
        }
