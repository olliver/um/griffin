import re
import logging
from enum import IntEnum

from griffin import signal

log = logging.getLogger(__name__.split(".")[-1])


class FaultLevel(IntEnum):
    EMERGENCY   = 0
    ALERT       = 1
    CRITICAL    = 2
    ERROR       = 3
    WARNING     = 4
    NOTICE      = 5
    INFO        = 6
    DEBUG       = 7
    NO_ERROR    = 255

    @classmethod
    def decode(cls, string):
        if string in cls.__members__:
            return cls.__members__[string.upper()]
        return None

    @classmethod
    def stringList(cls, seperator):
        string = ""
        for m in cls.__members__:
            string += m
            string += seperator
        if len(string) >= len(seperator):
            string = string[:(-1 * len(seperator))]
        return string

    def __str__(self):
        return re.sub(r"<" + type(self).__name__ + "\.(\w+):\s*\d+>", "\\1", self.__repr__())

    def getLogLevel(self):
        if self <= type(self).CRITICAL:
            return logging.CRITICAL
        elif self == type(self).ERROR:
            return logging.ERROR
        elif self <= type(self).NOTICE:
            return logging.WARNING
        elif self < type(self).DEBUG:
            return logging.INFO
        else:
            return logging.DEBUG

## Fault codes reproduce errors from marlin but also add internal griffin errors and warnings.
# XXX A copy of this list is held in the jedi.display package in src/Menus/MessageScreen.cpp.
class FaultCode(IntEnum):
    GENERIC                 = 0     ## The most basic error code if nothing is known about where the error comes from.
    EXTRUDER_MAXTEMP_ERROR  = 1     ## Maximum temperature error from the motion controller.
    EXTRUDER_MINTEMP_ERROR  = 2     ## Minimum temperature error from the motion controller.
    BED_MAXTEMP_ERROR       = 3     ## Maximum temperature error from the motion controller.
    HEATER_ERROR            = 4     ## Heater error from the motion controller.
    Z_ENDSTOP_ERROR         = 5     ## Z endstop error from the motion controller.
    XY_ENDSTOP_ERROR        = 6     ## XY endstop error from the motion controller.
    I2C_HEAD_COMM_ERROR     = 7     ## communication with the printhead was lost by the motion controller.
    I2C_COMM_ERROR          = 8     ## I2C communication error from the motion controller.
    SAFETY_ERROR            = 9     ## Safety error from the motion controller.
    CAP_SENSOR              = 10    ## capacitive sensor error from the motion controller.
    CARTRIDGE_GONE          = 11    ## cartridge removed during operation (UNUSED).
    HEAD_OPEN               = 12    ## The head is open while attempting to move it (UNUSED).
    MARLIN_GENERIC          = 13    ## An error comes from the motion controller, nothing else is known.
    FILAMENT_FLOW           = 14    ## A filament flow warning is detected, when printing this may cause the print to pause.
    BUILD_PLATE_LEVEL_UNKNOWN = 15  ## buildplate level not known while leveling mode is manual/Never.
    ACTIVE_LEVELING_PROBE_RETRIES_EXCEEDED = 16 ## nr of Automatic Build Plate Level detection probe retries exceeded.
    SWITCHING_CALIBRATION   = 17    ## The switching bay calibration data is not found.
    MATERIAL_TEMPERATURE    = 18    ## Incorrect printing temperature specified.
    BED_TEMPERATURE         = 19    ## Build plate temperature out of spec.
    MATERIAL_NOT_LOADED     = 20    ## No material loaded for printcore (but expected)
    HEADER_NOT_PRESENT      = 21    ## The gcode header was not present in the file to be printed!
    HEADER_MISSING_ITEM     = 22    ## The header is missing an item
    NOZZLE_AMOUNT_MISMATCH  = 23    ## The number of nozzles indicated by the gcode header does not match the machine settings.
    NOZZLE_MISMATCH         = 24    ## The nozzle diameter indicated by the gcode header does not match the machine settings.
    GUID_MISMATCH           = 25    ## The material GUID indicated by the gcode header does not match the inserted materials.
    MACHINE_TOO_SMALL_FOR_GCODE = 26 ## the machine size in the gcode header is too big for this machine.
    VERSION_TOO_OLD         = 27    ## Slicer version is too old
    FILE_ERROR              = 28    ## Problems with file access (read error, usb removed etc)
    NOZZLE_Z_DIFF_TOO_SMALL = 29    ## Same height detected for both nozzles. Verify switching bay calibration.
    NOZZLE_Z_DIFF_TOO_LARGE = 30    ## Difference between detected height of different nozzles exceeds realistic values
    HOTEND_INCOMPATIBLE_WITH_MATERIAL = 31 ## The new hotend is not compatible with the existing material.
    MATERIAL_INCOMPATIBLE_WITH_HOTEND = 32 ## The new material is not compatible with the existing hotend.
    MATERIAL_HOTEND_INCOMPATIBILITY   = 33 ## There is a hotend + material incompatibility.
    MOTION_CONTROLLER_UPDATE_FAILED   = 34 ## There was some kind of error in the motion controller update.
    CAPACITIVE_SENSOR_ERROR = 35 ## There was some kind of error with the capacitive sensor
    EEPROM_NOT_RECOGNIZED   = 36 ## not data in eeprom. + could not start eeprom programming wizard.
    NFC_DETECTS_TOO_MANY_MATERIALS = 37 ## The NFC service detects too many materials, tell the user to remove them!
    UNKNOWN_MATERIAL_COMPATIBILITY_WITH_HOTEND = 38 ## The material compatibility with this hotend is unknown.
    XY_CALIBRATION_NOT_DONE = 39 ## 2 hotends used but no xy calibration done!

    @classmethod
    def decode(cls, string):
        if string in cls.__members__:
            return cls.__members__[string]
        return None

    @classmethod
    def stringList(cls, seperator):
        string = ""
        for m in cls.__members__:
            string += m
            string += seperator
        if len(string) >= len(seperator):
            string = string[:(-1 * len(seperator))]
        return string

    def __str__(self):
        return re.sub(r"<" + type(self).__name__ + "\.(\w+):\s*\d+>", "\\1", self.__repr__())


class Fault():
    def __init__(self, level, code, msg, data = {}):
        if isinstance(level, str):
            level = FaultLevel.decode(level)
        self._level = level
        if isinstance(code, str):
            code = FaultCode.decode(code)
        self._code = code
        self._msg = msg
        self._data = data

    def __str__(self):
        return "<Fault: " + str(self._level) + ", " + str(self._code) + ", " + self._msg + " \>"

    def getErrorRepr(self):
        return [int(self._level), int(self._code), self._msg, self._data]

    def getLevel(self):
        return self._level

    def getLogLevel(self):
        return self._level.getLogLevel()

    def getCode(self):
        return self._code

    def getMsg(self):
        return self._msg

    def getData(self):
        return self._data

    def __eq__(self, other):
        if isinstance(other, list) and len(other) == 3:
            return (self.getLevel() == other[0] and
                    self.getCode() == other[1] and
                    self.getMsg() == other[2])
        if not isinstance(other, Fault):
            return False
        return (self.getLevel() == other.getLevel() and
                self.getCode() == other.getCode() and
                self.getMsg() == other.getMsg())


class FaultHandler():
    def __init__(self):
        self._lists = [[], [], [], [], [], [], [], []]
        self._current_error = None
        # The "error" returned in case there is no error set.
        self.__no_error = Fault(FaultLevel.NO_ERROR, 0, "")

        # A signal fired as soon as a new error is added in the fault handler, even if this is not the top most fault
        # This signal has 1 parameter, the Fault object that was raised.
        # The error is added to the fault list at this point, but not raised at possible top most fault yet.
        # This means you can clear the error to prevent it from being raised with onError (which is linked to showing it on the display)
        self.onNewError = signal.Signal()

        # Signal fired when a new error is set as top level error. This can be called on addFault when there are no errors yet.
        # Or it can be called on FaultHandler.clear() when the higher level error is cleared and a new error is the top most error.
        # This signal has 1 parameter, the Fault object that was raised.
        self.onError = signal.Signal()

    def addFault(self, level, code, msg, data):
        assert(isinstance(level, FaultLevel))
        f = Fault(level, code, msg, data)
        self._lists[level].append(f)
        log.log(f.getLogLevel(), "added Fault: L:%s C:%s M:%s" % (str(level), str(code), msg))

        self.onNewError.emit(f)

        err = self.getError()
        if self._current_error != err:
            self.onError.emit(err)

    def getError(self):
        # returns the most toplevel ERROR not all of them
        for errList in self._lists:
            if len(errList) > 0:
                return errList[0]   # .getError()
        return self.__no_error

    _instance = None

    @classmethod
    def getInstance(cls):
        if cls._instance is None:
            cls._instance = FaultHandler()
        return cls._instance

    @classmethod
    def clear(cls, level, code = None):
        if cls._instance is None:
            return
        if isinstance(level, Fault):
            f = level
            # remove f from error list
            errList = cls._instance._lists[f.getLevel()]
            for err in errList:
                if err == f:
                    errList.remove(f)
        else:
            try:
                level = int(level)
                code = int(code)
            except Exception:
                pass

            err_list = cls._instance._lists[level]
            del_list = []
            for err in err_list:
                if code is None or err.getCode() == code:
                    del_list.append(err)

            for err in del_list:
                err_list.remove(err)

        err = cls._instance.getError()
        if cls._instance._current_error != err:
            cls._instance.onError.emit(err)


    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def emergency(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.EMERGENCY, code, msg, data)

    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def alert(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.ALERT, code, msg, data)

    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def critical(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.CRITICAL, code, msg, data)

    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def error(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.ERROR, code, msg, data)

    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def warning(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.WARNING, code, msg, data)

    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def notice(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.NOTICE, code, msg, data)

    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def info(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.INFO, code, msg, data)

    ## Insert an Emergency level fault in the system.
    # @param msg a developer error string.
    # @param code If included this should be a specific code for the error so that the display can show a specific user error message for this situation.
    # @param data: An optional dictionary containing extra information about the fault. This is also passed to the display for inclusion in the display formatting.
    @classmethod
    def debug(cls, msg, code = FaultCode.GENERIC, data={}):
        if cls._instance is not None:
            cls._instance.addFault(FaultLevel.DEBUG, code, msg, data)
