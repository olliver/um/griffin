import logging
import time

from griffin.printer.controller import Controller

log = logging.getLogger(__name__.split(".")[-1])


# This class converts a given location name to a set of coordinates.
class BuildPlateLocations():
    __EDGE_OFFSET = 20                      ## This is the distance from the edges
    __CLIP  =  10                           ## Additional offset to avoid the glass holding clips.
    __NOZZLE_2_X_DISPLACEMENT_IN_MM = 3     ## Additional offset for 2nd nozzle probe location (so it does not probe on the material left by nozzle 1)

    ## Height above the bed which we travel from one location to another.
    __SAFE_TRAFEL_HEIGHT = 15

    ## Height from the "virtual" zero position after homing at which the active bed leveling starts.
    __ACTIVE_LEVEL_PROBE_START_HEIGHT = 8

    ## Additional offset for corners during active leveling
    __CORNER_OFFSET = 30

    def __init__(self):
        controller = Controller.getInstance()

        self.__build_volume_x = controller.getPropertyValue("build_volume_x")
        self.__build_volume_y = controller.getPropertyValue("build_volume_y")
        self.__build_volume_z = controller.getPropertyValue("build_volume_z")
        self.__hotend_offset_1_x = controller.getPropertyValue("hotend_offset_1_x")

        self.__LEFT = 0 + self.__EDGE_OFFSET
        self.__RIGHT = self.__build_volume_x - self.__hotend_offset_1_x
        self.__FRONT = 0 + self.__EDGE_OFFSET
        self.__REAR = self.__build_volume_y - self.__EDGE_OFFSET
        self.__BOTTOM = self.__build_volume_z
        self.__XCENTER = self.__build_volume_x / 2
        self.__YCENTER = self.__build_volume_y / 2

        # if this would be the center my hand would still be stuck-ish, and note that this is an offset from the front of the machine.
        # Also imagine if you will have a machine with an Y-axis of 1m, you don't want to have to walk into the machine to replace the cartridge.
        self.__Y_CHANGE_HOTENDS = min(150, self.__build_volume_y)
        self.__baseline_z = 0 ## this value is used to set the Z zero position during manual leveling.

        ## Height to move to 1 mm below the 2nd nozzle
        self.__RELATIVE_MOVEZ_CLOSE_TO_2ND_NOZZLE = controller.getPropertyValue("nominal_nozzle_z_offset") + 1

    ## Gives the buildplate coordinates for the specified location name.
    # @param location is the string name for the desired position coordinates
    # @param hotend_index is the nozzle index (0 = left nozzle, 1 = right nozzle)
    # @param overrule_coordinates allows you to set different coordinates then the stored location, for instance to set the Z to 0 during manual bed leveling.
    # @return  the X, Y and Z coordinates in a dictionary.
    def getPosition(self, location, hotend_index=0, overrule_coordinates={}):
        location = location.upper()
        controller = Controller.getInstance()

        position = None
        if location == "LEFT_FRONT":
            position = {"x": self.__LEFT + self.__CLIP, "y": self.__FRONT,      "z": self.__SAFE_TRAFEL_HEIGHT}
        elif location == "RIGHT_FRONT":
            position = {"x": self.__RIGHT,              "y": self.__FRONT,      "z": self.__SAFE_TRAFEL_HEIGHT}
        elif location == "CENTER_REAR":
            position = {"x": self.__XCENTER,            "y": self.__REAR,       "z": self.__SAFE_TRAFEL_HEIGHT}
        elif location == "CENTER_REAR_UNDER_2ND_NOZZLE":
            position = {"x": self.__XCENTER,            "y": self.__REAR,       "z": self.__RELATIVE_MOVEZ_CLOSE_TO_2ND_NOZZLE}
        elif location == "CHANGE_HOTENDS_MAINTENANCE":
            position = {"x": self.__XCENTER,            "y": self.__Y_CHANGE_HOTENDS,    "z": self.__BOTTOM}
        elif location == "CLEAN_NOZZLES_MAINTENANCE":
            position = {"x": self.__XCENTER,            "y": self.__FRONT,    "z": self.__BOTTOM}

        #  Note these positions are currently hardcoded for the Ultimaker 3 and Ultimaker 3 extended, and defined by Marc from electronics and Jan from mechanical.
        elif location == "ACTIVE_LEVEL_1":  # Left back
            position = {"x": 45, "y": 165, "z": self.__ACTIVE_LEVEL_PROBE_START_HEIGHT}
        elif location == "ACTIVE_LEVEL_2":  # Left front
            position = {"x": 45, "y": 25, "z": self.__ACTIVE_LEVEL_PROBE_START_HEIGHT}
        elif location == "ACTIVE_LEVEL_3":  # Right middle
            position = {"x": 198, "y": 90, "z": self.__ACTIVE_LEVEL_PROBE_START_HEIGHT}

        if position is not None:
            position.update(overrule_coordinates)
            position["z"] += self.__baseline_z
            # Correct for the the nozzle offset
            position["x"] -= self.__getNozzleOffset(hotend_index)

        if location.startswith("ACTIVE_LEVEL") and hotend_index == 1:
            # Special case for calibrating 2nd nozzle position: a small displacement so it will not probe in left over material(s) from nozzle 1
            position["x"] += BuildPlateLocations.__NOZZLE_2_X_DISPLACEMENT_IN_MM
            # Also correct the starting Z by the nominal Z offset between nozzles, so we have to move less distance when probing.
            position["z"] += controller.getPropertyValue("nominal_nozzle_z_offset")

        return position

    ## @brief Set the base offset for all values used during manual bed leveling
    def setBaselineZ(self, value):
        self.__baseline_z = value
        log.info("baseline set to %s", str(value))

    ## @brief Returns the offset, depending on the hotend specified
    #  @param hotend_index The hotend index (0 for the left, 1 for the right) for the nozzle
    #  @return The nozzle offset
    def __getNozzleOffset(self, hotend_index=0):
        assert (hotend_index == 0 or hotend_index == 1), "Only 0 (left nozzle) or 1 (right nozzle) supported"
        return 0 if hotend_index == 0 else self.__hotend_offset_1_x

    ## @brief Gets the default probe height for the bed
    #  @return Return the height in mm
    @classmethod
    def getSafeTravelHeight(cls):
        return cls.__SAFE_TRAFEL_HEIGHT

    ## @brief Gets the bottom height for the bed
    #  @return Return the height in mm
    def getBottomHeight(self):
        return self.__BOTTOM

    _instance = None

    @classmethod
    def getInstance(cls):
        if cls._instance is None:
            cls._instance = BuildPlateLocations()
        return cls._instance
