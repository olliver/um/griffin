from griffin import dbusif


## A generic class to expose properties from a PropertyContainer on DBus.
#  This prevents a lot of code duplications as we need to expose multiple parts of the printer service as properties, on different service paths.
class DBusServiceWithProperties(dbusif.ServiceObject):
    def __init__(self, property_container, service_name, service_path=None):
        super().__init__(service_name, service_path)

        self.__property_container = property_container
        property_container.onChange.connect(self.__propertyChanged)

    # Retrieve all properties on the printer service
    # @return: dictionary containing key(string)->value(variant) values for all properties.
    @dbusif.method("", "a{sv}")
    def getProperties(self):
        return self.__property_container.getPropertiesAsKeyValueDictionary()

    # Retrieve a single property value on the printer service
    # @return: value of the property
    @dbusif.method("s", "v")
    def getProperty(self, key):
        return self.__property_container.get(key).get()

    ## Set a value of a property by the key of the property.
    #  @return bool, True if the property was changed. False if the property does not exists or cannot be set to this value.
    @dbusif.method("sv", "b")
    def setProperty(self, key, value):
        return self.__property_container.setPropertyValue(key, value)

    ## Small wrapper function to call the proper dbus signal with the proper parameters.
    #  We get a property object from the property container signal,
    #  but we want to pass a string to the dbus signal.
    def __propertyChanged(self, property, value):
        self.propertyChanged(property.getKey(), value)

    ## Dbus signal when a printer service property changes.
    @dbusif.signal("sv")
    def propertyChanged(self, key, value):
        return True
