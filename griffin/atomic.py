from threading import Lock


## Simple atomic counter, which can be increased or decreased in a thread safe way.
class AtomicCounter:
    def __init__(self, counter_value=0):
        self.__value = counter_value
        self.__lock = Lock()

    ## Get the current value of this counter.
    #  @return the current value of the counter.
    def getValue(self):
        return self.__value

    ## Increase the counter with the given amount, or 1 if no amount is given.
    #  @param amount: The amount to add to the counter.
    #  @return the new value of the counter.
    def increase(self, amount=1):
        with self.__lock:
            self.__value += amount
            result = self.__value
        return result

    ## Decrease the counter with the given amount, or 1 if no amount is given.
    #  @param amount: The amount to remove from the counter.
    #  @return the new value of the counter.
    def decrease(self, amount=1):
        return self.increase(-amount)
