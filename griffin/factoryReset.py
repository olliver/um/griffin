import logging

log = logging.getLogger(__name__.split(".")[-1])

## Class that only defines some factory types which may be used to handle factory resets in different ways
class FactoryReset:
    # Factory Reset: DESTROY keywords

    ## This will erase all settings and only leave the minimal, depending on service implementation
    FACTORY_RESET_KEYWORD_DESTROY = "DESTROY"

    ## This will erase settings but might leave a number soft settings as they were, depending on service implementation
    FACTORY_RESET_KEYWORD_SOFT_DESTROY = "SOFT_DESTROY"

