import urllib.request
import base64
import logging
import json
import socket

from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


# Interface to the camera attached to the system
# TODO: dbus might not be the best transport mechanism for large datasets like jpeg images
class CameraService(dbusif.ServiceObject):
    def __init__(self):
        super().__init__("camera")
        self._url = "http://localhost:8080/?action=snapshot"
        self._controls_url = "http://localhost:8080/input_0.json"
        self._control_command_url = "http://localhost:8080/?action=command_ng&dest=0&plugin=0&id=%d&group=%d&value=%s"
        self._online = True

        self._controls = []

        try:
            response = urllib.request.urlopen(self._controls_url, timeout=2.0)
            data = response.read()
            data = data.decode("utf-8")
            data = json.loads(data)
            self._controls = data["controls"]
        except:
            log.error("Error while trying to fetch data about camera controls (mjpg_streamer not running?)")
            self._online = False

    # Get the list of camera controls. Returns: (name, value, min, max)
    @dbusif.method("", "a(svvv)")
    def getControls(self):
        ret = []
        for control_data in self._controls:
            try:
                type_num = int(control_data["type"])
                if type_num == 1:
                    ret.append([control_data["name"], int(control_data["value"]), int(control_data["min"]), int(control_data["max"])])
                elif type_num == 2:
                    ret.append([control_data["name"], bool(control_data["value"]), 0, 1])
                elif type_num == 3:
                    ret.append([control_data["name"], int(control_data["value"]), int(control_data["min"]), int(control_data["max"])])
            except:
                log.exception("Exception during control list generation")
        return ret

    @dbusif.method("sv", "b")
    def setControlValue(self, control_name, value):
        try:
            for control in self._controls:
                if control["name"] == control_name:
                    response = urllib.request.urlopen(self._control_command_url % (int(control["id"]), int(control["group"]), value), timeout=2.0)
                    response.read()
                    # Ignore the reply.
        except:
            log.exception("Error while setting camera control value")
            return False
        return True

    @dbusif.method("", "b")
    def isOnline(self):
        return self._online

    @dbusif.method("", "s")
    def grabFrameJpeg(self):
        try:
            response = urllib.request.urlopen(self._url, timeout=2.0)
        except urllib.error.URLError:
            self._online = False
            return ""
        except socket.timeout:
            self._online = False
            return ""
        except:
            log.exception("Unknown exception during camera frame grab")
            return ""
        data = response.read()
        return base64.encodebytes(data)
