import logging

from griffin import dbusif

from .connmanAgent import ConnmanAgent
from .connmanServiceObject import ConnmanServiceObject
from .connmanTechnologyObject import ConnmanTechnologyObject


log = logging.getLogger(__name__.split(".")[-1])


# Class to interface with connman. Connman handles most of our network related
# functionality. But has a pretty extensive dbus interface.
# For details, see: http://git.kernel.org/cgit/network/connman/connman.git/tree/doc
class ConnmanManager():
    ETHERNET_PATH = "/net/connman/technology/ethernet"
    WIFI_PATH = "/net/connman/technology/wifi"

    def __init__(self):
        self._services = {}
        self._technologies = {}
        self._agent = ConnmanAgent()
        self._manager = None

        self.__dbus = dbusif.RemoteObject("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus")
        self.__dbus.connectSignal("NameOwnerChanged", self.__dbusNameOwnerChanged)

        self.__setupManager()

    def __dbusNameOwnerChanged(self, name, old_owner, new_owner):
        if name == "net.connman" and new_owner != "":
            # If connman changes from name, re-load the connection to the manager and every other dbus object that is connected to connman.
            log.info("Connman seems to be restarted, reload our manager connection.")
            self.__setupManager()

    ## Setup a new manager connection.
    def __setupManager(self):
        if self._manager is not None:
            self._manager.cleanSignals()
            for path, service in self._services.items():
                service.clean()
            self._services = {}
            for path, technology in self._technologies.items():
                technology.clean()
            self._technologies = {}

        self._manager = dbusif.RemoteObject("net.connman", "/", "net.connman.Manager")
        try:
            self._manager.RegisterAgent(self._agent.getPath())
        except:
            log.exception("RegisterAgent failed?")
        self._manager.connectSignal("ServicesChanged", self._servicesChanged)
        self._manager.connectSignal("TechnologyAdded", self._technologyAdded)
        self._manager.connectSignal("TechnologyRemoved", self._technologyRemoved)

        self._servicesChanged(self._manager.GetServices(), {})
        for path, properties in self._manager.GetTechnologies():
            self._technologyAdded(path, properties)

    # Callback from dbus when the services are changed.
    def _servicesChanged(self, changed_services, removed_services):
        for path, properties in changed_services:
            if path not in self._services:
                self._services[path] = ConnmanServiceObject(path)
            for key, value in properties.items():
                self._services[path].propertyChanged(key, value)
        for path in removed_services:
            if path in self._services:
                self._services[path].clean()    # Call clean, which removes the dbus binding and registered signals.
                del self._services[path]

    # Callback from dbus when technologies are added.
    def _technologyAdded(self, path, properties):
        if path not in self._technologies:
            self._technologies[path] = ConnmanTechnologyObject(path)
        for key, value in properties.items():
            self._technologies[path].propertyChanged(key, value)

    # Callback from dbus when a technology is removed.
    def _technologyRemoved(self, path):
        if path in self._technologies:
            self._technologies[path].clean()    # Call clean, which removes the dbus binding and registered signals.
            del self._technologies[path]

    # Get the reference to a certain technology.
    # @param path: ETHERNET_PATH or WIFI_PATH
    # Note that this can return None when a technology is not available yet (wifi is hotplugged and it not always available)
    # Returns ConnmanTechnologyObject
    def getTechnology(self, path):
        if path not in self._technologies:
            return None
        return self._technologies[path]

    # Get a dictionary of all available services.
    # The key is the path of the service, the value is a ConnmanServiceObject that contains the data of this service.
    def getServices(self):
        return self._services

    # Shorthand function to set a property to a technology.
    # If the technology is not available thie function will return false. Else it will set the property to the value given if it is not that value yet.
    def setTechnologyProperty(self, path, key, value):
        technology = self.getTechnology(path)
        if technology is None:
            return False
        if technology.getProperty(key) != value:
            log.debug("Setting technology property: %s %s %s", path, key, value)
            technology.setProperty(key, value)
        return True

    # Shorthand function to get a property to a certain value on a technology.
    # Will return None if the technology is not available, else it will return the value of this property.
    # If the property is not available on this technology, the technology object will return a None as well.
    def getTechnologyProperty(self, path, key):
        technology = self.getTechnology(path)
        if technology is None:
            return None
        return technology.getProperty(key)

    # Connect to a network, specified by a service path.
    # If the network requires a password, the password should be given in the passphrase parameter.
    # If no password is required, this parameter is ignored.
    # @return True when network is found and connection process is initiated.
    # @return False when network is not found.
    def connectToNetwork(self, path, passphrase):
        for service_path, service in self._services.items():
            if service.isWifi() and path == service_path:
                self._agent.setNextPassphrase(path, passphrase)
                service.connect()
                return True
        return False

    # Retrieve a connecting error. When connecting to a wifi network, the registered agent can get an error callback.
    # This error is stored and will be retrieved by this function. This error is reset when a new connection attempt is made.
    # @return None when no error is raised
    # @return error string when an error was raised during connecting to the wifi network
    def getConnectingError(self):
        return self._agent.getError()
