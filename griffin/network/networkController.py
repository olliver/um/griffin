import logging
import time
import re
import subprocess

from griffin.factoryReset import FactoryReset
from griffin import thread
from griffin import dbusif
from griffin.preferences.registryFile import RegistryFile

from .connmanManager import ConnmanManager
from .connmanClock import ConnmanClock

log = logging.getLogger(__name__.split(".")[-1])


class NetworkController:
    # Location of system wide printer preferences
    __PREFERENCES_FILE = "network_preferences.json"

    # Preference key: Network mode
    __PREF_KEY_MODE = "mode"
    # Preference key: Wifi Hotspot SSID to connect to
    __PREF_KEY_HOTSPOT_SSID = "hotspot_ssid"
    # Preference key: Hotspot password to use for connecting to wifi
    __PREF_KEY_HOTSPOT_PASSPHRASE = "hotspot_passphrase"

    # Different network modes that we can operate in
    MODE_AUTO_CONNECT = "AUTO"
    MODE_HOTSPOT = "HOTSPOT"
    MODE_WIFI_SETUP = "WIFI SETUP"
    MODE_CABLE = "CABLE"
    MODE_WIRELESS = "WIRELESS"
    MODE_OFFLINE = "OFFLINE"

    # During wifi setup, we have a state machine to go through.
    # We start by enabling the wifi hardware, and scan for networks.
    # After we scanned for X amount of time, we enable the wifi hotspot, while remembering the networks we saw during scanning.
    # When the hotspot is active, the user can connect to this, and select a network to connect to.
    # When a network is selected
    SETUP_STATE_WIFI_STARTUP = "STARTUP"
    SETUP_STATE_WIFI_SCAN = "SCAN"
    SETUP_STATE_WIFI_HOTSPOT = "HOTSPOT"
    SETUP_STATE_WIFI_WAIT_FOR_NETWORK = "WAIT FOR NETWORK"
    SETUP_STATE_WIFI_TRY_TO_CONNECT = "TRY TO CONNECT"
    SETUP_STATE_WIFI_FAILED = "FAILED"

    # These settings needs to be saved during a soft factory reset
    __SAVE_SETTINGS_ON_SOFT_RESET = []
    # These settings need to be set
    __ADD_SETTINGS_ON_SOFT_RESET = {}
    # Connman settings location
    __CONNMAN_SETTINGS_PATH = "/var/lib/connman"

    # Amount of time in seconds to scan for wifi networks when running the wifi setup wizard.
    __WIFI_SETUP_SCAN_TIME = 30
    # Amount of retries done when doing the initial wifi setup connecting. This sometimes fails on the first try.
    __WIFI_CONNECT_ATTEMPTS = 10
    # Amount of maximum wifi detection attempts to be done (restarting connman also causes eth0 to be restarted)
    __WIFI_DETECT_ATTEMPTS = 3

    # Amount of time we allow a network to be in the "association" state, before we toggle the wifi.
    # Sometimes connman drops a wifi network back to this state and never recovers. So after a certain amount of time toggle the wifi power
    __FAILURE_RETRY_TIME = 5

    __FATAL_CONNECTION_ERROR_STATUSES = [ "invalid-key" ]

    def __init__(self, service):
        self.__settings = RegistryFile(self.__PREFERENCES_FILE)
        self.__initSettings()

        self.__wifi_hotspot_hack_fix_counter = 0
        self.__connman_no_wifi_counter = 0
        self.__restart_counter = 0
        self.__wifi_check_failure_time = time.monotonic()

        # Store a reference to the network service object, which controls the dbus interface to the rest of the system
        self._service = service
        # The connman manager controls all the network interfaces and handles most of the dirty wifi complexity.
        self._manager = ConnmanManager()
        # Create a ConnmanClock object, which will manage the timeservers, as connman fails to setup a proper fallback.
        self._clock = ConnmanClock()

        # Create and start the management thread. We run the management of the network system on a thread.
        # Because running it event based is much more complex.
        self._manage_thread = thread.Thread(name="Network state manager", target=self._manageNetworkState)
        self._manage_thread.start()

        # Setup a connection to the system service to monitory country changes.
        # When our country changes we need to setup our regulatory domain.
        self._system_service = dbusif.RemoteObject("system")
        self._system_service.connectSignal("countryChanged", self._onCountryChanged)
        self._onCountryChanged(self._system_service.getCountry())

        ## Add callback to dbus network service which implements the call to the resetToFactorySettings
        self._system_service.addFactoryResetCallback("network", "resetSettings")

    ## @brief Implementation of the factory reset callback
    #  @param reset_type Specified which type of reset is requested
    def resetToFactorySettings(self, reset_type):
        log.info("Factory Reset executing...")

        self.__eraseConnmanConfigurationFiles()

        if reset_type == FactoryReset.FACTORY_RESET_KEYWORD_SOFT_DESTROY:
            self.__settings.backupAndSetup(self.__SAVE_SETTINGS_ON_SOFT_RESET, self.__ADD_SETTINGS_ON_SOFT_RESET)
        else:
            self.__settings.erase()

    ## This function erases the connman configuration files.
    def __eraseConnmanConfigurationFiles(self):
        log.info("Going to erase all connman configuration files.")
        # Erase all connman stored settings (including cable)
        p = subprocess.Popen(["sudo", "/bin/rm", "-rf", NetworkController.__CONNMAN_SETTINGS_PATH], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()

        ## Recreate main settings folder for connman
        p = subprocess.Popen(["sudo", "/bin/mkdir", NetworkController.__CONNMAN_SETTINGS_PATH], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()

        ## Restart connman after deleting the preferces.
        p = subprocess.Popen(["sudo", "/bin/systemctl", "restart", "connman"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()
        log.info("Finished erasing all connman configuration files.")

    # @brief Load the settings
    def __initSettings(self):
        self._mode = self.__settings.getAsString(self.__PREF_KEY_MODE, self.MODE_CABLE)
        if self._mode == NetworkController.MODE_WIFI_SETUP:
            log.warning("Loaded mode from settings is MODE_WIFI_SETUP, reverting to MODE_WIRELESS as MODE_WIFI_SETUP is not a valid mode setting.")
            self._mode = NetworkController.MODE_WIRELESS
        self._hotspot_ssid = self.__settings.getAsString(self.__PREF_KEY_HOTSPOT_SSID, "UM-NO-HOTSPOT-NAME-SET")
        self._hotspot_passphrase = self.__settings.getAsString(self.__PREF_KEY_HOTSPOT_PASSPHRASE, "NO_PASSWORD_SET")
        # In case no settings exist yet, save the defaults
        self.__saveSettings()

        # These don't need saving: used for setting up wifi only
        self._setup_ssid = "UM-NO-HOTSPOT-NAME-SET"
        self._wifi_setup_active = False
        self._wifi_setup_state = self.SETUP_STATE_WIFI_STARTUP
        self._wifi_setup_networks = {}
        self._wifi_setup_service_path = ""
        self._wifi_setup_service_password = ""

    def __saveSettings(self):
        self.__settings.setAsString(self.__PREF_KEY_MODE, self._mode)
        self.__settings.setAsString(self.__PREF_KEY_HOTSPOT_SSID, self._hotspot_ssid)
        self.__settings.setAsString(self.__PREF_KEY_HOTSPOT_PASSPHRASE, self._hotspot_passphrase)

    # When the country code changes in the system service,
    # update our regulatory domain for the wifi.
    def _onCountryChanged(self, country_code):
        # When no proper country code is set yet, set the regulatory domain to the world domain.
        # This limits wifi transimitions to safe values across the world.
        # Pre default the country_code is an empty string, and thus triggers this.
        if not re.match("^[A-Z]{2}$", country_code):
            country_code = "00"

        log.info("Setting regulatory wifi domain to %s", country_code)
        p = subprocess.Popen(["sudo", "/sbin/iw", "reg", "set", country_code], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()

    # Manage the state machine of the network service.
    # We made this state/timer driven out of simplicity of the code. As a lof of events are received from connman that have to be
    # managed otherwise. (Example, when a hotspot is disabled, the wifi technology disappears for a while)
    # For all modes except the wifi setup, this thread just forces connman into a certain mode of operation.
    def _manageNetworkState(self):
        while True:
            time.sleep(1)
            if self._wifi_setup_active:
                # Wifi setup overrules all selected modes, so when wifi setup is active then ignore the mode and handle the wifi setup state.
                self._manageWifiSetupState()
            elif self._mode == self.MODE_AUTO_CONNECT:
                self._manager.setTechnologyProperty(ConnmanManager.ETHERNET_PATH, "Powered", True)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", True)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", False)
                self.__wifiCheckFailure()
            elif self._mode == self.MODE_HOTSPOT:
                self._manager.setTechnologyProperty(ConnmanManager.ETHERNET_PATH, "Powered", False)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", True)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringIdentifier", self._hotspot_ssid)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringPassphrase", self._hotspot_passphrase)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", True)
            elif self._mode == self.MODE_CABLE:
                self._manager.setTechnologyProperty(ConnmanManager.ETHERNET_PATH, "Powered", True)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", False)
            elif self._mode == self.MODE_WIRELESS:
                self._manager.setTechnologyProperty(ConnmanManager.ETHERNET_PATH, "Powered", False)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", True)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", False)
                self.__wifiCheckFailure()
            elif self._mode == self.MODE_OFFLINE:
                self._manager.setTechnologyProperty(ConnmanManager.ETHERNET_PATH, "Powered", False)
                self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", False)

            # Very ugly hackerdy hack... connman reports a tethering enabled, but wifi is not powered sometimes, or loses power.
            # dmesg reports the following in this case:
            # [ 1855.240365] device wlan0 entered promiscuous mode
            # [ 1855.246138] tether: port 1(wlan0) entered forwarding state
            # [ 1855.251966] tether: port 1(wlan0) entered forwarding state
            # [ 1948.617902] tether: port 1(wlan0) entered disabled state
            # [ 1948.656073] tether: port 1(wlan0) entered disabled state
            # [ 1948.665997] IPv6: ADDRCONF(NETDEV_UP): wlan0: link is not ready
            # [ 1949.921632] IPv6: ADDRCONF(NETDEV_UP): wlan0: link is not ready
            if self._manager.getTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered") == True and self._manager.getTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering") == True:
                try:
                    with open("/sys/class/net/wlan0/operstate", "rt") as f:
                        state = f.read()
                    if state.strip() != "up":
                        self.__wifi_hotspot_hack_fix_counter += 1
                        if self.__wifi_hotspot_hack_fix_counter > 5:
                            log.warning("Connman is reporting tethering enabled, but sysfs says wlan0 is %s. Cycling Tether property in attempt to recover", state)
                            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", False)
                            time.sleep(5)
                            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", True)
                except IOError:
                    pass
            else:
                self.__wifi_hotspot_hack_fix_counter = 0
            if self._manager.getTechnology(ConnmanManager.WIFI_PATH) is None:
                self.__connman_no_wifi_counter += 1
                if self.__connman_no_wifi_counter > 60 and self.__restart_counter < NetworkController.__WIFI_DETECT_ATTEMPTS:
                    self.__restart_counter += 1
                    p = subprocess.Popen(["sudo", "/bin/systemctl", "restart", "connman"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    out, err = p.communicate()
                    p.wait()
                    self.__connman_no_wifi_counter = 0
            else:
                self.__connman_no_wifi_counter = 0
                self.__restart_counter = 0

    # Manage the state machine of the wifi setup mode.
    def _manageWifiSetupState(self):
        if self._wifi_setup_state == self.SETUP_STATE_WIFI_STARTUP:
            # Wifi setup start state. Disable ethernet, enable wifi.
            # Enabling wifi can take a bit of time, so wait for it to finish before going to the scan state.
            self._manager.setTechnologyProperty(ConnmanManager.ETHERNET_PATH, "Powered", False)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", True)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", False)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringIdentifier", self._setup_ssid)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringPassphrase", "")
            # Move to the next state when the wifi device is powered.
            if self._manager.getTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered") == True:
                self._wifi_setup_state = self.SETUP_STATE_WIFI_SCAN
                # Force a wifi scan when the power is up on the wifi device.
                self._manager.getTechnology(ConnmanManager.WIFI_PATH).scan()
                self._wifi_setup_service_path = ""
                self._wifi_setup_service_password = ""
        elif self._wifi_setup_state == self.SETUP_STATE_WIFI_SCAN:
            # Wifi scan state. Enable the wifi hardware, and scan for networks. We wait 30 seconds to gather all responses from access points.
            for n in range(0, NetworkController.__WIFI_SETUP_SCAN_TIME):  # Scan for wifi networks for 30 seconds.
                time.sleep(1)
                if not self._wifi_setup_active:
                    return
            # Store the current services, as connman will clear the services list as soon as we start our hotspot.
            self._wifi_setup_networks = self._manager.getServices().copy()
            log.debug("Stored %d wifi networks to referer to while hotspot is active", len(self._wifi_setup_networks))
            self._wifi_setup_state = self.SETUP_STATE_WIFI_HOTSPOT
        elif self._wifi_setup_state == self.SETUP_STATE_WIFI_HOTSPOT:
            # Wifi hotspot state, wait till the user selects the wifi network to connect.
            # The connectToWifiNetwork function will be called, which will move us to the next state.
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringIdentifier", self._setup_ssid)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringPassphrase", "")
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", True)
        elif self._wifi_setup_state == self.SETUP_STATE_WIFI_WAIT_FOR_NETWORK:
            # Wifi network is selected during the hotspot mode, disable the hotspot, and wait for connman to connect to the network.
            # We can have 3 different results:
            # * Successfully connected to the wifi network (expected result)
            # * Failed to connect to wifi network (wrong password)
            # * Wifi network is not found anymore (timeout)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", True)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", False)

            if self._manager.getTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering") == False:
                service = self._manager.getServices().get(self._wifi_setup_service_path)
                if service is not None:
                    # First forget before trying to connect, so old passwords do not apply.
                    self.forgetWifiNetwork(self._wifi_setup_service_path)
                    self._wifi_setup_retry = NetworkController.__WIFI_CONNECT_ATTEMPTS
                    self._wifi_setup_state = self.SETUP_STATE_WIFI_TRY_TO_CONNECT
                    self._manager.connectToNetwork(self._wifi_setup_service_path, self._wifi_setup_service_password)
        elif self._wifi_setup_state == self.SETUP_STATE_WIFI_TRY_TO_CONNECT:
            error = self._manager.getConnectingError()
            if error is not None:
                # If the error code is listed in the fatal connection errors (e.g. the auth key is wrong), 
                # there's no point in retrying the connection attempt, so better off moving to the failed
                # state right away.
                if error in self.__FATAL_CONNECTION_ERROR_STATUSES:
                    log.warn("fatal connection error: " + error)
                    self._wifi_setup_state = self.SETUP_STATE_WIFI_FAILED
                    return

                # If we ran out of possible retries, fail
                if self._wifi_setup_retry <= 0:
                    log.warn("connection retried too many times")
                    self.wifi_setup_state = self.SETUP_STATE_WIFI_FAILED
                    return

                self._wifi_setup_retry -= 1
                self._manager.connectToNetwork(self._wifi_setup_service_path, self._wifi_setup_service_password)
                return

            service = self._manager.getServices().get(self._wifi_setup_service_path)
            if service is not None:
                service_state = service.getProperty("State")
                if service_state == "failure":
                    if self._wifi_setup_retry == 0:
                        # Failed to connect, go to the error state.
                        self._wifi_setup_state = self.SETUP_STATE_WIFI_FAILED
                        return

                    self._wifi_setup_retry -= 1
                    self._manager.connectToNetwork(self._wifi_setup_service_path, self._wifi_setup_service_password)

                elif service_state == "online" or service_state == "ready":
                    # Connecting was successful, goto the wireless connect state, as we are finished now.
                    self.setMode(self.MODE_WIRELESS)
                    self.__settings.forceSave()
            else:
                # Service disappeared, this isn't good.
                self._wifi_setup_state = self.SETUP_STATE_WIFI_FAILED
        elif self._wifi_setup_state == self.SETUP_STATE_WIFI_FAILED:
            # We've failed to connect to the wifi network. Enable the hotspot again to report this failure, and possibly try again.
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", True)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringIdentifier", self._setup_ssid)
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "TetheringPassphrase", "")
            self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering", True)

    def __wifiCheckFailure(self):
        for path, service in self._manager.getServices().items():
            if service.getProperty("State") == "online" or service.getProperty("State") == "ready":
                self.__wifi_check_failure_time = time.monotonic()
                return
        for path, service in self._manager.getServices().items():
            if service.getProperty("State") == "failure":
                if time.monotonic() - self.__wifi_check_failure_time > NetworkController.__FAILURE_RETRY_TIME:
                    self._manager.setTechnologyProperty(ConnmanManager.WIFI_PATH, "Powered", False)
                    time.sleep(2)
                    self.__wifi_check_failure_time = time.monotonic()
                return
        self.__wifi_check_failure_time = time.monotonic()

    # Set the SSID for the wifi setup state. This can be a different SSID then in hotspot mode.
    # @param ssid: SSID for the wifi hotspot during wifi setup.
    def setSetupSSID(self, ssid):
        self._setup_ssid = ssid

    ## @brief Sets the wifi hotspot parameters
    #  @param ssid The wifi network identifier
    #  @param password The wifi access password
    def setHotspotParameters(self, ssid, password):
        self._hotspot_ssid = ssid
        self._hotspot_passphrase = password
        self.__saveSettings()

    def getHotspotSSID(self):
        return self._hotspot_ssid

    def getHotspotPassword(self):
        return self._hotspot_passphrase

    ## @brief Change the mode of operation for the network service.
    #  The network service will force the network into this new mode.
    #  @param new_mode The mode to switch to. In case of an incorrect mode, an exception will be thrown
    def setMode(self, new_mode):
        if new_mode not in [self.MODE_AUTO_CONNECT, self.MODE_HOTSPOT, self.MODE_CABLE, self.MODE_OFFLINE, self.MODE_WIRELESS]:
            log.error("Changing to unknown network mode " + str(new_mode) + ", ignoring change request!")
            return

        # On a mode change, disable the wifi setup.
        if self._wifi_setup_active:
            self._wifi_setup_active = False

        if self._mode is not new_mode:
            self._mode = new_mode
            self.__saveSettings()
            self._service.modeChanged(self.getMode())

    ## Start the wifi setup.
    def startWiFiSetup(self):
        self._wifi_setup_active = True
        # Changed into wifi setup mode. Start the wifi setup state machine.
        self._wifi_setup_state = self.SETUP_STATE_WIFI_STARTUP
        self._service.modeChanged(self.getMode())

    # Return the current mode of operation for the network service.
    def getMode(self):
        if self._wifi_setup_active:
            return NetworkController.MODE_WIFI_SETUP
        return self._mode

    def getWifiSetupState(self):
        if self._wifi_setup_active:
            return self._wifi_setup_state
        return "NONE"

    # @brief Connect to a wifi network, return true when the network is found, and a connection attempt is initized
    # Success cannot be monitored from here, as connecting is a asynchronized process.
    # @param network_id The wifi network ssid
    # @param password The network access password
    def connectToWifiNetwork(self, network_id, password):
        # When we are in wifi setup mode, we cannot connect to a wifi network directly, as connman does not know about the networks.
        # We first need to stop serving our wifi hotspot. So store the information and push the state machine to the proper step.
        if self._wifi_setup_active and (self._wifi_setup_state == self.SETUP_STATE_WIFI_HOTSPOT or self._wifi_setup_state == self.SETUP_STATE_WIFI_FAILED):
            if network_id in self._wifi_setup_networks:
                thread.Thread(name="Connect to network during setup", target=self.__startConnection, args=(network_id, password)).start()
                return True
            return False
        # Not in wifi setup mode, just connect straight to the wifi network given (if available)
        return self._manager.connectToNetwork(network_id, password)

    def __startConnection(self, network_id, password):
        # Remove all the connman configuration files. This removes old wifi settings and prevents the printer from connecting to an old configured network.
        self.__eraseConnmanConfigurationFiles()

        self._wifi_setup_service_path = network_id
        self._wifi_setup_service_password = password
        self._wifi_setup_state = self.SETUP_STATE_WIFI_WAIT_FOR_NETWORK

    def forgetWifiNetwork(self, network_id):
        service = self._manager.getServices().get(network_id)
        if service is not None and service.isWifi():
            service.remove()
            return True
        return False

    # Return the way we are connected as a string.
    def getConnectedMethod(self):
        if self._manager.getTechnologyProperty(ConnmanManager.WIFI_PATH, "Tethering") == True:
            return "HOTSPOT"
        if self._manager.getTechnologyProperty(ConnmanManager.ETHERNET_PATH, "Connected") == True:
            return "ETHERNET"
        if self._manager.getTechnologyProperty(ConnmanManager.WIFI_PATH, "Connected") == True:
            return "WIFI"
        return "NONE"

    # Get the reference to the service which is currently in the state "online"
    # This is the service we use as active network connection.
    # This function will return None when no service is currently active as connection.
    def getOnlineService(self):
        for path, service in self._manager.getServices().items():
            if service.getProperty("State") == "online":
                return service
        # If we don't find an online state, we might be connected to the network that is ready (connected without internet behind it)
        for path, service in self._manager.getServices().items():
            if service.getProperty("State") == "ready":
                return service
        return None

    # Return a list of key-value dictionaries of wifi networks that are available.
    def getWifiServices(self):
        # Check if we need to get the services from our stored list, or from the connman manager.
        if self._wifi_setup_active and (self._wifi_setup_state == self.SETUP_STATE_WIFI_HOTSPOT or self._wifi_setup_state == self.SETUP_STATE_WIFI_FAILED):
            services = self._wifi_setup_networks
        else:
            services = self._manager.getServices()

        # List all the wifi services, and certain properties in those services.
        # This list is not sorted.
        service_list = []
        for path, service in services.items():
            if service.isWifi():
                security = service.getProperty("Security")
                if security is None or len(security) < 1 or security == ["none"]:
                    security = False
                else:
                    security = True
                service_list.append({
                    "id": path,
                    "state": service.getProperty("State") or "",
                    "name": service.getProperty("Name") or "",
                    "strength": service.getProperty("Strength") or 0,
                    "security": security,
                })
        return service_list
