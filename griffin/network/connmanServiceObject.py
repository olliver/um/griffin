import logging

from griffin import signal
from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


# Class that interfaces with a connman service.
# A connman service is used to store properties about possible network connections.
# Every connection that connman can make, is is currently maintaining is stored as a service.
# As these are managed as different dbus remote objects, this object is used to manage this RemoteObject
# See http://git.kernel.org/cgit/network/connman/connman.git/tree/doc/service-api.txt for details on the dbus API.
class ConnmanServiceObject(signal.SignalEmitter):
    def __init__(self, path):
        super().__init__()
        self._path = path
        self._properties = {}
        self._dbus_object = dbusif.RemoteObject("net.connman", path, "net.connman.Service")
        self._dbus_object.connectSignal("PropertyChanged", self.propertyChanged)
        log.debug("New connman service object: %s", path)

    # public function to set a property.
    # In some conditions, the connman.Manager API will update the properties instead of the connman.Service
    # so the service object needs a public interface to set these properties.
    # This function is also attached to the "PropertyChanged" signal from the connman.Service dbus API.
    def propertyChanged(self, key, value):
        self._properties[key] = value

    # Get the value of a property, or None if this property is not set.
    # Properties are cached, so this function is lightweight.
    def getProperty(self, key):
        return self._properties.get(key, None)

    # Try to change a property. On success the property is changed and you get a propertyChanged event
    def setProperty(self, key, value):
        try:
            self._dbus_object.SetProperty.callAsync(key, value)
        except:
            log.exception("Exception while trying to set propery: %s to %s", key, value)

    # Check if this services object referers to a wifi network.
    # As the ethernet connection is also listed as a server and we want to filter on just (possible) wifi connections.
    def isWifi(self):
        return self.getProperty("Type") == "wifi"

    # Connect to this network. Only available for wifi networks.
    def connect(self):
        if self.isWifi():
            self._dbus_object.Connect.callAsync()

    # Remove this network from the remembered list. Used to forget wifi networks.
    def remove(self):
        if self.isWifi():
            self._dbus_object.Remove.callAsync()

    # Clean is called when this object needs to be destroyed. As dbus holds a reference to the dbusif.RemoteObject the remoteobject will not be properly garbage collected
    # unless this reference is properly cleaned.
    def clean(self):
        self._dbus_object.cleanSignals()
