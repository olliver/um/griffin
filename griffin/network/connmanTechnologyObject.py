import logging

from griffin import signal
from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


# Connman technology object.
# This object is used to track properties of the connman technology interface.
# See http://git.kernel.org/cgit/network/connman/connman.git/tree/doc/technology-api.txt for the dbus API from connman.
# This object is created and destroyed by the connman.Manager.
class ConnmanTechnologyObject(signal.SignalEmitter):
    def __init__(self, path):
        super().__init__()
        self._path = path
        self._properties = {}
        self._dbus_object = dbusif.RemoteObject("net.connman", path, "net.connman.Technology")
        self._dbus_object.connectSignal("PropertyChanged", self.propertyChanged)
        log.debug("New connman technology object: %s", path)

    # public function to set a property.
    # In some conditions, the connman.Manager API will update the properties instead of the connman.Service
    # so the service object needs a public interface to set these properties.
    # This function is also attached to the "PropertyChanged" signal from the connman.Service dbus API.
    def propertyChanged(self, key, value):
        log.debug("Property changed: %s %s %s", self._path, key, value)
        self._properties[key] = value

    # Get the value of a property, or None if this property is not set.
    # Properties are cached, so this function is lightweight.
    def getProperty(self, key):
        return self._properties.get(key, None)

    # Try to set a property on this technology.
    # It's not always possible to set a property, and in some cases connman throws an exception.
    # To simplify use we only log this exception. As we are using this polling based.
    # When a property is properly set, the "PropertyChanged" signal will update the internal state of this object.
    def setProperty(self, key, value):
        try:
            self._dbus_object.SetProperty.callAsync(key, value)
        except:
            log.exception("Exception while trying to set property: %s to %s", key, value)

    # Initiate a wifi scan. Only acceptable on the wifi technology.
    def scan(self):
        self._dbus_object.Scan.callAsync()

    # When this object needs to be destroyed, clean needs to be called.
    # dbus holds a global reference to the dbus.RemoteObject when signals are connected. Causing the RemoteObject to never be garbage collected.
    # This function cleans up those references and allows the object to be properly garbage collected.
    def clean(self):
        self._dbus_object.cleanSignals()
