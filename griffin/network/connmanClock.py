from griffin import dbusif
from griffin import thread
import time


## Simple monitor for the Connman clock DBUS API.
#  This class monitors if connman has ntp servers configured, and if not, it adds pool.ntp.org to the list of servers.
#  This increases the chance that we have a valid system time.
class ConnmanClock:
    # Time between checks to see if we have an NTP server.
    __NTP_SERVER_CHECK_DELAY = 30

    def __init__(self):
        self.__dbus = dbusif.RemoteObject("org.freedesktop.DBus", "/org/freedesktop/DBus", "org.freedesktop.DBus")
        self.__dbus.connectSignal("NameOwnerChanged", self.__dbusNameOwnerChanged)
        self.__connman_clock = dbusif.RemoteObject("net.connman", "/", "net.connman.Clock")

        thread.Thread("ConnmanClock", self.__checkTimeserverThread).start()

    ## Connman restarts in some occasions. This means the name owner changes of net.connman
    #  So we need to get a new remoteObject when this happens.
    # @param name: string, name that has changed ownership.
    # @param old_owner: string, old name owner, empty if nobody owned it.
    # @param new_owner: string, new name owner, empty if nobody owns it after this call.
    def __dbusNameOwnerChanged(self, name, old_owner, new_owner):
        if name == "net.connman" and new_owner != "":
            # If connman changes from name, re-load the connection to the manager and every other dbus object that is connected to connman.
            self.__connman_clock = dbusif.RemoteObject("net.connman", "/", "net.connman.Clock")

    ## Thread that checks if we have a timeserver configuration.
    #  This loops and keeps checking if connman has a timeserver configuration, if not, it puts pool.ntp.org as ntpserver.
    def __checkTimeserverThread(self):
        while True:
            time.sleep(ConnmanClock.__NTP_SERVER_CHECK_DELAY)
            if len(self.__connman_clock.GetProperties()["Timeservers"]) < 1:
                self.__connman_clock.SetProperty("Timeservers", ["pool.ntp.org"])
