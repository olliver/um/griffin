import logging

from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


# Agent for connman. Connman uses an agent mechanism to connect to password protected wifi networks.
# After connecting to a network, the registered agent is asked to supply any additional information.
# NOTE: This implementation currently only supports simple passphrase required networks.
#       The connman API documentation describes more possible fields:
#       http://git.kernel.org/cgit/network/connman/connman.git/tree/doc/agent-api.txt?id=HEAD
#       WISPr, hidden networks and WPS are currently not supported.
class ConnmanAgent(dbusif.ServiceObject):
    def __init__(self):
        super().__init__("network_agent")
        self._service_path = ""
        self._passphrase = ""
        self._error = None

    # Get the path on dbus where the agent is located.
    def getPath(self):
        for location in self.locations:
            return location[1]

    # Set the passphrase and service path for the next possible request.
    # This needs to be set before a connection to the wifi network is made.
    def setNextPassphrase(self, service_path, passphrase):
        self._error = None  # Clear the error before a new connection attempt is made.
        self._service_path = service_path
        self._passphrase = passphrase

    def getError(self):
        return self._error

    @dbusif.method("", "", "net.connman.Agent")
    def Release(self):  # [CodeStyle: dbus method of the network agent needs upper case]
        # This means this agent is released. And thus will no longer be called when a connection attempt is made.
        # Do we need to do something in this case? Or does the re-initialize from ConnmanManager already handle this?
        log.info("NetworkAgent Release")

    @dbusif.method("os", "", "net.connman.Agent")
    def ReportError(self, service, error):  # [CodeStyle: dbus method of the network agent needs upper case]
        # Connecting gave an error, store this, so the controller can see the connection attempt has failed.
        log.warning("Error while connecting to network: %s", error)
        self._error = error

    @dbusif.method("os", "", "net.connman.Agent")
    def ReportPeerError(self, peer, error):     # [CodeStyle: dbus method of the network agent needs upper case]
        log.info("NetworkAgent ReportPeerError %s %s", peer, error)

    @dbusif.method("os", "", "net.connman.Agent")
    def RequestBrowser(self, service, url):     # [CodeStyle: dbus method of the network agent needs upper case]
        log.info("NetworkAgent RequestBrowser %s %s", service, url)

    @dbusif.method("oa{sv}", "a{sv}", "net.connman.Agent")
    def RequestInput(self, service, fields):    # [CodeStyle: dbus method of the network agent needs upper case]
        log.info("Got input request for: %s", service)
        if service != self._service_path:
            log.warning("%s does not match expected service: %s", service, self._service_path)
            return {}

        ret = {}
        for key, value in fields.items():
            if key == "Passphrase":
                log.info("Supplying passphrase")
                ret[key] = self._passphrase
                self._passphrase = ""
            else:
                log.info("NetworkAgent RequestInput: %s", key)
        return ret

    @dbusif.method("os", "", "net.connman.Agent")
    def RequestPeerAuthorization(self, peer, fields):   # [CodeStyle: dbus method of the network agent needs upper case]
        log.info("NetworkAgent RequestPeerAuthorization %s %s", peer, fields)

    @dbusif.method("", "", "net.connman.Agent")
    def Cancel(self):   # [CodeStyle: dbus method of the network agent needs upper case]
        log.info("NetworkAgent Cancel")
