== Network service

The network service handles all the dirty details of keeping the network interfaces running.

Most of this is handled by a 2nd application called "connman". Connman can connect to ethernet and wifi networks. And can even run a wifi hotspot.

For documentation on connman, see: http://git.kernel.org/cgit/network/connman/connman.git/tree/doc

== Interface

The external dbus interface of this service is documented in networkService.py. Which is the start point of the code.

Hyarchy wise, the could should be followed in the order of:

* networkService.py
* networkController.py
* connmanManager.py
* connmanTechnologyObject.py
* connmanServiceObject.py
* connmanAgent.py

== Hotspot

The hotspot mode uses the "tethering" option of connman. Connman uses this to connect a hotspot to the ethernet interface. As we just want to run a hotspot, but not expose the ethernet network (Which could be corporate network).
We make sure the ethernet interface is disabled when hotspot mode is active.

== Connman

We currently use a slightly modified connman code base. Which can be found at:

https://github.com/Ultimaker/connman

Modifications are made for the following goals:

* Hotspot without a password
* Hotspot does DNS poisoning, causing all DNS requests to point to the machine.
