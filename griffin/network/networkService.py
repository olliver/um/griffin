import logging
import os

from griffin import dbusif

from .networkController import NetworkController

log = logging.getLogger(__name__.split(".")[-1])


# The network service manages the different states and configurations the network can be in.
# It contains APIs to query the current network state, as well as allowing to switch to different
# configuration modes.
# Currently 5 modes of operation are supported:
# * Auto connect to available networks (ethernet and wifi enabled, connect to what is available,
#   prefering ethernet. connman can handle this for us).
# * Wifi hotspot (ethernet disabled, wifi enabled, in hotspot mode).
# * Wifi setup (ethernet disabled, wifi enabled, first scan for networks for a while, enable the
#   hotspot. Remember the networks. Give the user an option to connect to networks).
# * Cable only (ethernet enabled and wifi disabled).
# * Offline (ethernet and wifi disabled).
#
# The wifi hotspot mode is the most complex mode. During this mode, the previously seen wifi
# networks need to be remembered.
# And we need to allow the service to connect to one of these networks. But we can only connect to
# this network when the actual hotspot is no longer available.
# When connecting to the wifi network, we want to track if we are successful or not, and if we are
# not successful in connecting to the wifi network, we want to go back to hotspot mode.
#
# Next to this we want to be able to query the following data:
# * In auto connect mode, which connection method are we using.
# * Which wifi networks are available (or previously seen networks in wifi hotspot mode)
# * ip address (ipv4 and ipv6) when we are connected
# * ip address of the hotspot when in hotspot mode

# This class only serves as the description of the external interface to the rest of the system.
# The NetworkController class handles all the complexity.
class NetworkService(dbusif.ServiceObject):
    ##< Where can we find the available network interface connections on the linux filesystem
    __SYSTEM_NETWORK_INTERFACES_LOCATION = "/sys/class/net"

    def __init__(self):
        super().__init__("network")
        self._controller = NetworkController(self)

    # Change the network mode to auto connect.
    # Which will make the machine connect through ethernet or wifi depending on what it can find.
    @dbusif.method("", "")
    def setModeAutoConnect(self):
        self._controller.setMode(NetworkController.MODE_AUTO_CONNECT)

    # Change the network mode to hotspot mode.
    # This will disable ethernet, and enable a wifi hotspot.
    # Settings for the hotspot should be configured with setHotspotParameters
    @dbusif.method("", "")
    def setModeHotspot(self):
        self._controller.setMode(NetworkController.MODE_HOTSPOT)

    # Enter the wifi setup mode.
    # Wifi setup follows a state machine to configure a wifi network. See getWifiSetupState for details.
    @dbusif.method("s", "")
    def setModeWifiSetup(self, ssid):
        self._controller.setSetupSSID(ssid)
        self._controller.startWiFiSetup()

    # Change the network mode to cable only.
    # This will disable the wifi radio, and thus only allow cabled connections.
    @dbusif.method("", "")
    def setModeCable(self):
        self._controller.setMode(NetworkController.MODE_CABLE)

    # Change the network mode to wireless only.
    # This will enable the wifi radio, and disable the thus only allow wireless connections.
    @dbusif.method("", "")
    def setModeWireless(self):
        self._controller.setMode(NetworkController.MODE_WIRELESS)

    # Change the network mode to offline.
    # In offline mode, both the wifi interface and the ethernet interface are disabled.
    # No external connection can be made to the printer in this mode.
    @dbusif.method("", "")
    def setModeOffline(self):
        self._controller.setMode(NetworkController.MODE_OFFLINE)

    # Return the current  mode of operation.
    # This can be any of the modes in NetworkController
    # @return: any of "AUTO", "HOTSPOT", "WIFI SETUP", "CABLE", "OFFLINE"
    @dbusif.method("", "s")
    def getMode(self):
        return self._controller.getMode()

    # Signal when the current mode is changed.
    # @param new_mode: See getMode for options.
    @dbusif.signal("s")
    def modeChanged(self, new_mode):
        pass

    # Get the current state of the wifi setup mode.
    # As the wifi setup follows a state machine with different steps.
    # @return "STARTUP": Network service is disabling the ethernet interface, and enabling the wifi interface.
    # @return "SCAN":    Wifi interface is up, and we are scanning to see which networks are available
    # @return "HOTSPOT": Wifi interface is running a local hotspot. And waiting for the user to
    #                    connect to this hotspot to configure the machine.
    # @return "WAIT FOR NETWORK": A network is selected during hotspot mode. Tearing down the hotspot
    #                             to search for this wifi network.
    # @return "TRY TO CONNECT": The wifi network that we want to connect to has been found, trying to
    #                           connect to it. On success, the mode of operation is changed into "AUTO"
    # @return "FAILED":  Failed to connect to the wifi network. Possible causes are that the network
    #                    is misconfigured, or the password was wrong.
    #                    The hotspot is enabled again to allow the user to retry.
    @dbusif.method("", "s")
    def getWifiSetupState(self):
        return self._controller.getWifiSetupState()

    # Set the configuration parameters for the permanent hotspot mode (setModeHotspot)
    # @param ssid: The SSID we want to use for this hotspot
    # @param password: The password we want to protect this hotspot with
    @dbusif.method("ss", "")
    def setHotspotParameters(self, ssid, password):
        self._controller.setHotspotParameters(ssid, password)

    # Get the currently configured ssid for the permanent hotspot. (not for the wifi setup hotspot)
    @dbusif.method("", "s")
    def getHotspotSSID(self):
        return self._controller.getHotspotSSID()

    # Get the currently configured password for the permanent hotspot. (not for the wifi setup
    # hotspot, that uses no password)
    @dbusif.method("", "s")
    def getHotspotPassword(self):
        return self._controller.getHotspotPassword()

    # Return a list of key-value pairs that shows all available networks.
    # During wifi setup, this list is cached, as we cannot scan for wifi networks during hotspot mode.
    # This list is not sorted by any means and should be sorted by the user of this API for proper viewing.
    # @return: array of dictionaries. With the following key/value pairs:
    #         "id": Unique id to identify this network. Multiple networks can share the same SSID
    #         "state": Connection state of this network. See git.kernel.org/cgit/network/connman/connman.git/tree/doc/overview-api.txt for state options
    #         "name": The SSID of this network.
    #         "strength": Signal strenght of this network. From 0 to 100
    #         "security": Boolean if this network is secured or not. Unsecured networks do not require a password.
    @dbusif.method("", "aa{sv}")
    def getWifiNetworks(self):
        return self._controller.getWifiServices()

    # Return the current available external connection method.
    # @return "NONE": No connection available.
    # @return "HOTSPOT": Hotspot is active, other devices can connect to us.
    # @return "ETHERNET": An ethernet cable is plugged in, and connected.
    # @return "WIFI": We've successfully connected to a wifi network.
    @dbusif.method("", "s")
    def getConnectedMethod(self):
        return self._controller.getConnectedMethod()

    # Return our IPv4 address when it is available. Else return an empty string.
    @dbusif.method("", "s")
    def getIPv4Address(self):
        # TOFIX: When in hotspot mode, return the IP address of our local interface. Connman does
        # not supply a method for this. We can use the "netifaces" python package for this.
        service = self._controller.getOnlineService()
        if service is None:
            return ""
        ip = service.getProperty("IPv4")
        if ip is not None:
            ip = ip.get("Address", "")
        else:
            ip = ""
        return ip

    # Return our IPv6 address when it is available. Else return an empty string.
    @dbusif.method("", "s")
    def getIPv6Address(self):
        service = self._controller.getOnlineService()
        if service is None:
            return ""
        ip = service.getProperty("IPv6")
        if ip is not None:
            ip = ip.get("Address", "")
        else:
            ip = ""
        return ip

    ## @brief Gets the SSID or network name, depending on the online connection, or an empty string if not connected at all
    #  @return Returns the network name, which is "Wired" for an active ethernet (cable) connection
    @dbusif.method("", "s")
    def getNetworkName(self):
        service = self._controller.getOnlineService()
        if service is None:
            return ""
        return service.getProperty("Name")

    ## Get the mac addresses currently available in the system.
    # @return A dict containing mac addresses. With the device name as key, and the mac address as value
    #         The mac addresses are returned in the format xx:xx:xx:xx:xx:xx
    @dbusif.method("", "a{ss}")
    def getMacAddresses(self):
        results = {}
        for device in self.__getLanAndWifiDevices():
            try:
                address_file = os.path.join(NetworkService.__SYSTEM_NETWORK_INTERFACES_LOCATION, device, "address")
                with open(address_file, "rt") as f:
                    address = f.readline().strip()
            except OSError:
                log.exception("Error while trying to read MAC address for device: %s", device)
                address = "00:00:00:00:00:00"
            results[device] = address
        return results

    ## @brief Gets the available wireless and network devices based on the realtime filesystem data
    #         Check for device with using the uevent file:
    #         1. For Wifi Connections, the devtype == 'wlan'
    #         2. For ethernet connections, this will be based on the interface starting with "eth"
    #         If either case is true, the device name is added to the device list
    #  @return Return the list of the devices that should be used
    def __getLanAndWifiDevices(self):
        device_list = []

        for device in os.listdir(NetworkService.__SYSTEM_NETWORK_INTERFACES_LOCATION):
            device_path = os.path.join(NetworkService.__SYSTEM_NETWORK_INTERFACES_LOCATION, device)
            if os.path.isdir(device_path):
                try:
                    uevent_file = os.path.join(device_path, "uevent")
                    with open(uevent_file, "rt") as f:

                        for line in f:
                            # Prepare the data from the line to be in a certain format
                            try:
                                key, value = line.strip().split("=", 1)
                            except ValueError:
                                # Invalid line, continue with next line
                                continue

                            key = key.upper()
                            value = value.lower()

                            # Checks
                            if key == "DEVTYPE":
                                if value == "wlan":
                                    device_list.append(device)
                                    break
                            elif key == "INTERFACE":
                                if value.startswith("eth"):
                                    device_list.append(device)
                                    break
                except OSError:
                    log.exception("Unable to open device path %s, skipping...", device_path)

        log.debug("Devices found: %r", device_list)
        return device_list


    # Attempt a connection to a wifi network.
    # When a connection is successful the credentials will be stored for future use and we'll auto
    # connect to this network in the future when we are in "AUTO" mode.
    # During wifi setup, this method will break down the hotspot to connect to the wifi network.
    # See the getWifiSetupState for details.
    @dbusif.method("ss", "")
    def connectToWifiNetwork(self, network_id, password):
        self._controller.connectToWifiNetwork(network_id, password)

    # Remove a wifi network from the list of remembered networks. When credentials where stored
    # before, after this call they are removed and no connection is made with this wifi network anymore.
    @dbusif.method("s", "")
    def forgetWifiNetwork(self, network_id):
        self._controller.forgetWifiNetwork(network_id)

    ## @brief The callback function to register which needs to be called when a factory reset is issued
    #  @param reset_type One of the defined values from FactoryReset which would indicate a hard or soft reset
    @dbusif.method("s", "")
    def resetSettings(self, reset_type):
        self._controller.resetToFactorySettings(reset_type)
