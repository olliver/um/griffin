#   Author:  Thiago Marcos P. Santos
#   Author:  Christopher S. Case
#   Author:  David H. Bronke
#   Author:  Arjen Hiemstra
#   Author:  David Braam
#   License: MIT

import inspect
from weakref import WeakSet, WeakKeyDictionary

from . import thread

##  Simple implementation of signals and slots.
#
#   Signals and slots can be used as a light weight event system. A class can
#   define signals that other classes can connect functions or methods to, called slots.
#   Whenever the signal is called, it will proceed to call the connected slots.
#
#   To create a signal, create an instance variable of type Signal. Other objects can then
#   use that variable's `connect()` method to connect methods, callables or signals to the
#   signal. To emit the signal, call `emit()` on the signal. Arguments can be passed along
#   to the signal, but slots will be required to handle them. When connecting signals to
#   other signals, the connected signal will be emitted whenever the signal is emitted.
#
#   Signal-slot connections are weak references and as such will not prevent objects
#   from being destroyed. In addition, all slots will be implicitly disconnected when
#   the signal is destroyed.
#
#   \warning It is imperative that the signals are created as instance variables, otherwise
#   emitting signals will get confused. To help with this, see the SignalEmitter class.
#
#   Based on http://code.activestate.com/recipes/577980-improved-signalsslots-implementation-in-python/
#
class Signal:
    Sync = 1,
    ASync = 2

    ##  Initialize the instance.
    #
    #   \param kwargs Keyword arguments.
    def __init__(self, **kwargs):
        self.__functions = set()
        self.__methods = WeakKeyDictionary()
        self.__signals = WeakSet()
        self.__type = kwargs.get("type", Signal.Sync)

    def __call__(self):
        raise NotImplementedError("Call emit() to emit a signal")

    ##  Emit the signal, indirectly calling all connected slots.
    #
    #   \param args The positional arguments to pass along.
    #   \param kargs The keyword arguments to pass along.
    def emit(self, *args, **kwargs):
        if self.__type == Signal.ASync:
            t = thread.Thread(None, self.__emit, (args, kwargs))
            t.start()
        else:
            self.__emit(args, kwargs)

    ## Private emit call, called from a thread or directly from the public emit function depending on the signal type
    def __emit(self, args, kwargs):
        # Call handler functions
        for func in self.__functions:
            func(*args, **kwargs)

        # Call handler methods
        for dest, funcs in self.__methods.copy().items():
            for func in funcs.copy():
                func(dest, *args, **kwargs)

        # Emit connected signals
        for signal in self.__signals.copy():
            signal.emit(*args, **kwargs)

    ##  Connect to this signal.
    #   \param connector The signal or slot to connect.
    def connect(self, connector):
        if type(connector) == Signal:
            if connector == self:
                return
            self.__signals.add(connector)
        elif inspect.ismethod(connector):
            if connector.__self__ not in self.__methods:
                self.__methods[connector.__self__] = set()

            self.__methods[connector.__self__].add(connector.__func__)
        else:
            self.__functions.add(connector)

    ##  Disconnect from this signal.
    #   \param connector The signal or slot to disconnect.
    def disconnect(self, connector):
        if connector in self.__signals:
            self.__signals.remove(connector)
        elif inspect.ismethod(connector) and connector.__self__ in self.__methods:
            self.__methods[connector.__self__].remove(connector.__func__)
        else:
            if connector in self.__functions:
                self.__functions.remove(connector)

    ##  Disconnect all connected slots.
    def disconnectAll(self):
        self.__functions.clear()
        self.__methods.clear()
        self.__signals.clear()

    ##  private:

    #   To avoid circular references when importing Application, this should be
    #   set by the Application instance.
    _app = None


##  Convenience class to simplify signal creation.
#
#   This class is a Convenience class to simplify signal creation. Since signals
#   need to be instance variables, normally you would need to create all singals
#   in the class' `__init__` method. However, this makes them rather awkward to
#   document. This class instead makes it possible to declare them as class variables
#   and properly document them. During the call to `__init__()`, this class will
#   then search through all the properties of the instance and create instance
#   variables for each class variable that is an instance of Signal.
class SignalEmitter:
    ##  Initialize method.
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        for name, signal in inspect.getmembers(self, lambda i: isinstance(i, Signal)):
            setattr(self, name, Signal())
