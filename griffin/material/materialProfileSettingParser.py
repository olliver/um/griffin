

## This class is a pure static class. It contains static function for parsing of different setting values in the material profile XML files.
#  All these functions throw a ValueError when the given data is invalid.
class MaterialProfileSettingParser:

    ## Parse a temperature xml node, only validated if it contains a valid float.
    @staticmethod
    def parseTemperatureNode(node):
        if node.text is None:
            raise ValueError("Invalid empty value in node <%s>" % (node.get("key")))
        return float(node.text)

    ## Parse a temperature graph xml node, check if it has multiple child nodes in the format of <point flow="%f" temperature="%f"/>
    #  Needs at least 2 nodes to be a valid graph.
    @staticmethod
    def parseProcessingTemperatureGraph(node):
        points = []
        for sub_node in node.findall("{http://www.ultimaker.com/material}point"):
            flow = float(sub_node.get("flow"))
            temperature = float(sub_node.get("temperature"))
            points.append((flow, temperature))
        if len(points) < 1:
            raise ValueError("No points found in node <%s>" % (node.get("key")))
        if len(points) < 2:
            raise ValueError("Not enough points in node <%s>" % (node.get("key")))
        # Sort the points on the flow value
        points.sort(key=lambda point: point[0])
        return points

    ## Parse a percentage node. Can contain the text "on" or "off", as well as a float between 0 and 100 (inclusive)
    @staticmethod
    def parsePercentage(node):
        if node.text is None:
            raise ValueError("Invalid empty value in node <%s>" % (node.get("key")))
        if node.text.lower() == "on":
            return 100.0
        if node.text.lower() == "off":
            return 0.0
        percentage = float(node.text)
        if percentage < 0 or percentage > 100:
            raise ValueError("Percentage value out of range: %f in node <%s>" % (percentage, node.get("key")))
        return percentage

    ## Parse a distance xml node, only validated if it contains a valid float.
    @staticmethod
    def parseDistanceInMillimeters(node):
        if node.text is None:
            raise ValueError("Invalid empty value in node <%s>" % (node.get("key")))
        return float(node.text)

    ## Parse a speed xml node, only validated if it contains a valid float.
    @staticmethod
    def parseSpeedInMillimetersPerSecond(node):
        if node.text is None:
            raise ValueError("Invalid empty value in node <%s>" % (node.get("key")))
        return float(node.text)

    ## Parse a (hardware) compatibility xml node, must contain "yes" "no" or "unknown"
    @staticmethod
    def parseCompatibility(node):
        if node.text is None:
            raise ValueError("Invalid empty value in node <%s>" % (node.get("key")))
        if node.text.lower() == "yes":
            return "yes"
        if node.text.lower() == "no":
            return "no"
        if node.text.lower() == "unknown":
            return "unknown"
        raise ValueError("Invalid value for compatibility: %s, expected yes/no/unknown in node <%s>" % (node.text, node.get("key")))
