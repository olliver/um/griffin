## @package docstring
# Material profile database service
#
# The material profile database offers a unified (dbus and python) access to the material database.
#
# This module is responsible for material profiles

import logging
import uuid

from .materialDatabase import MaterialDatabase
from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


# Interface to the material database available to the system.
class MaterialService(dbusif.ServiceObject):
    __RESULT_GUID_KEY = "guid"
    __FILTER_BRAND_KEY = "brand"
    __FILTER_MATERIAL_KEY = "material"
    __FILTER_COLOR_KEY = "color"

    def __init__(self):
        super().__init__("material")
        self.__database = MaterialDatabase()
        self.__database.scanDirectoryForMaterials("/usr/share/fdm_materials")
        self.__database.setMaterialStoragePath("/var/lib/griffin/fdm_materials")
        self.__database.loadPublicSignatureKeysFrom("/usr/share/fdm_materials/keys")
        self.__machine_manufacturer = ""
        self.__machine_product = ""

        ## Add callback to this material service which implements the call to the resetToFactorySettings
        dbusif.RemoteObject("system").addFactoryResetCallback("material", "resetSettings")

    ## @brief The callback function to register which needs to be called when a factory reset is issued
    #  @param reset_type One of the defined values from FactoryReset which would indicate a hard or soft reset
    @dbusif.method("s", "")
    def resetSettings(self, reset_type):
        self.__database.removeMaterials()

    # DBus method for retrieving a dictionary of (filtered) materials. A typical DBus call to the materialService
    # looks could be something like:
    #   {"brand": "Ultimaker", "color": "red"}
    # Here all red materials of the brand Ultimaker will be returned.
    # Supplying an empty dictionary or no parameter at all results in all materials to be returned.
    # To find out which keys can be used to filter the materials on, getMaterialFilterKeys() can be used.
    # @param a{ss}: A dictionary containing
    # @return: Dictionary of (filtered) materials, with the material GUID as key.
    @dbusif.method("a{ss}", "aa{ss}")
    def getMaterials(self, filters=None):
        results = []
        for material_profile in self.__database.getMaterialsByFilter(filters):
            if material_profile.hasSettingsFor(self.__machine_manufacturer, self.__machine_product):
                brand, material, color = material_profile.getMetadataNameFields()
                results.append({
                    MaterialService.__RESULT_GUID_KEY: str(material_profile.getGUID()),
                    MaterialService.__FILTER_BRAND_KEY: brand,
                    MaterialService.__FILTER_MATERIAL_KEY: material,
                    MaterialService.__FILTER_COLOR_KEY: color
                })

        # Workaround for development of new machines. If no material profiles are found, return all the material profiles that are matching for all machines.
        if len(results) == 0:
            for material_profile in self.__database.getMaterialsByFilter(filters):
                brand, material, color = material_profile.getMetadataNameFields()
                results.append({
                    MaterialService.__RESULT_GUID_KEY: str(material_profile.getGUID()),
                    MaterialService.__FILTER_BRAND_KEY: brand,
                    MaterialService.__FILTER_MATERIAL_KEY: material,
                    MaterialService.__FILTER_COLOR_KEY: color
                })

        return results

    # DBus method for retrieving a dictionary of available keys to filter on
    # @param a{ss}: filters, same as for the method getMaterials
    # @return: Dictionary of (filtered) keys, e.g. {"brand": ["Ultimaker", "Generic"], "color": ["Blue"]}
    @dbusif.method("a{ss}", "a{sas}")
    def getMaterialFilterKeys(self, filters=None):
        results = {MaterialService.__FILTER_BRAND_KEY: [], MaterialService.__FILTER_MATERIAL_KEY: [], MaterialService.__FILTER_COLOR_KEY: []}
        for material_profile in self.__database.getMaterialsByFilter(filters):
            if material_profile.hasSettingsFor(self.__machine_manufacturer, self.__machine_product):
                brand, material, color = material_profile.getMetadataNameFields()
                if brand not in results[MaterialService.__FILTER_BRAND_KEY]:
                    results[MaterialService.__FILTER_BRAND_KEY].append(brand)
                if material not in results[MaterialService.__FILTER_MATERIAL_KEY]:
                    results[MaterialService.__FILTER_MATERIAL_KEY].append(material)
                if color not in results[MaterialService.__FILTER_COLOR_KEY]:
                    results[MaterialService.__FILTER_COLOR_KEY].append(color)
        return results

    ## DBus method to retrieve all settings associated with a material for a certain material
    #  @param guid: string in the format of "12345678-1234-5678-1234-567812345678"
    #  @return: dictionary of key/value containing the settings defined in the material profile. An empty dictionary will be given if the GUID is not known.
    @dbusif.method("ss", "a{sv}")
    def getMaterialSettings(self, guid, nozzle_id):
        try:
            guid = uuid.UUID(guid)
        except ValueError:
            log.warning("Tried to fetch settings for material with GUID %s, but not a valid GUID", guid)
            return {}

        material = self.__database.getMaterialByGUID(guid)
        if material is None:
            log.warning("Tried to fetch settings for material with GUID %s, but does not exist", guid)
            return {}

        return material.getSettingsFor(self.__machine_manufacturer, self.__machine_product, nozzle_id)

    ## DBus method to retrieve all properties associated with a material for a certain material
    #  @param guid: string in the format of "12345678-1234-5678-1234-567812345678"
    #  @return: dictionary of key/value containing the property defined in the material profile. An empty dictionary will be given if the GUID is not known.
    @dbusif.method("s", "a{sv}")
    def getMaterialProperties(self, guid):
        try:
            guid = uuid.UUID(guid)
        except ValueError:
            log.warning("Tried to fetch settings for material with GUID %s, but not a valid GUID", guid)
            return {}
        material = self.__database.getMaterialByGUID(guid)
        if material is None:
            log.warning("Tried to fetch settings for material with GUID %s, but does not exist", guid)
            return {}
        return material.getProperties()


    ## DBus method to retrieve the brand, material, color parts of the material profile metadata name.
    #  @param guid: string in the format of "12345678-1234-5678-1234-567812345678"
    #  @return: tuple of 3 strings, for "brand", "material" and "color"
    @dbusif.method("s", "sss")
    def getMaterialNameFields(self, guid):
        try:
            guid = uuid.UUID(guid)
        except ValueError:
            log.warning("Tried to fetch name fields for material with GUID [%s], but not a valid GUID", guid)
            return "?", "?", "?"
        material = self.__database.getMaterialByGUID(guid)
        if material is None:
            log.warning("Tried to fetch name fields for material with GUID [%s], but does not exist", guid)
            return "?", "?", "?"
        return material.getMetadataNameFields()

    ## DBus method to get the contents of an material file. This returns the full, unmodified XML contents of the material profile file.
    #  @param guid: string in the format of "12345678-1234-5678-1234-567812345678"
    #  @return: string containing the full XML material profile
    #  @return: empty string on invalid of unknown GUID
    @dbusif.method("s", "s")
    def getMaterialXMLContents(self, guid):
        try:
            guid = uuid.UUID(guid)
        except ValueError:
            log.warning("Tried to xml data for material with GUID [%s], but not a valid GUID", guid)
            return ""
        material = self.__database.getMaterialByGUID(guid)
        if material is None:
            log.warning("Tried to xml data for material with GUID [%s], but does not exist", guid)
            return ""
        return material.getXMLData()

    @dbusif.method("s", "b")
    def addNewMaterial(self, material_profile_contents):
        self.__database.addNewMaterial(material_profile_contents)
        if not self.__database.addNewMaterial(material_profile_contents):
            return False
        return True

    @dbusif.method("ss", "b")
    def addNewSignedMaterial(self, material_profile_contents, material_profile_signature):
        if not self.__database.addNewMaterial(material_profile_contents, material_profile_signature):
            return False
        return True

    @dbusif.method("ss", "b")
    def updateMaterial(self, guid, material_profile_contents):
        try:
            guid = uuid.UUID(guid)
        except ValueError:
            log.warning("Tried to update material with GUID %s, but not a valid GUID", guid)
            return False
        return self.__database.updateMaterial(guid, material_profile_contents)

    @dbusif.method("sss", "b")
    def updateSignedMaterial(self, guid, material_profile_contents, material_profile_signature):
        try:
            guid = uuid.UUID(guid)
        except ValueError:
            log.warning("Tried to update material with GUID %s, but not a valid GUID", guid)
            return False
        return self.__database.updateMaterial(guid, material_profile_contents, material_profile_signature)

    @dbusif.method("s", "b")
    def removeMaterial(self, guid):
        try:
            guid = uuid.UUID(guid)
        except ValueError:
            log.warning("Tried to remove material with GUID %s, but not a valid GUID", guid)
            return False
        return self.__database.removeMaterialByGUID(guid)

    ## Method to set the current machine manufacturer and product name.
    #  These names are used to filter the material database on materials that are applicable for this machine only.
    # @param manufacturer: String to set as new manufacturer
    # @param product: String to set as new product
    @dbusif.method("ss", "")
    def setMachineType(self, manufacturer, product):
        self.__machine_manufacturer = manufacturer
        self.__machine_product = product

    ## Retrieve the machine type set by setMachineType
    # @return a tuple of two strings, containing the manufacturer name and product name
    @dbusif.method("", "ss")
    def getMachineType(self):
        return (self.__machine_manufacturer, self.__machine_product)