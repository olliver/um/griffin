import os
import logging

from .materialProfile import MaterialProfile
from griffin.gpg import GPG

log = logging.getLogger(__name__.split(".")[-1])


## MaterialDatabase, contains a set of MaterialProfile objects.
#  The material database handles searching for the files, the storage location for new files.
#  Defines rules for which profile to use when duplicate GUIDs are detected. And checks the signatures.
#  The material database can be queried for single materials, or a list of filtered materials.
class MaterialDatabase:
    # Create the material database.
    def __init__(self):
        self.__material_storage_path = None
        self.__materials = {}
        self.__gpg = GPG()

    ## Add all public keys in a directory.
    #  We need to load multiple public keys in our system. Each material supplier will use its own private key.
    #  This to prevent compromising the whole material signature system if a single key gets abused.
    # @param path: Full path to the directory containing keys
    def loadPublicSignatureKeysFrom(self, path):
        if os.path.isdir(path):
            for filename in os.listdir(path):
                self.addPublicSignatureKey(os.path.join(path, filename))

    # Add a public key which is used to check the signatures of the material profiles.
    # Multiple public keys can be added, a single public key needs to provide a valid check for the signature to be valid.
    # @param path: Full path to the key file
    def addPublicSignatureKey(self, key_file):
        self.__gpg.addPublicKeyFile(key_file)

    # Set the directory in which new material profiles are stored.
    # This path is also scanned for material profiles which are added to the database.
    # @param path: Full path to the directory containing material profiles and where profiles are stored.
    def setMaterialStoragePath(self, material_storage_path):
        os.makedirs(material_storage_path, exist_ok=True)
        self.__material_storage_path = material_storage_path
        self.scanDirectoryForMaterials(material_storage_path)

    ## Scan a directory and add all the material profiles in this directory to the database.
    # @param path: Full path to the directory containing material profiles
    def scanDirectoryForMaterials(self, path):
        if not os.path.isdir(path):
            raise ValueError("Given path %s is not a directory" % (path))

        for filename in os.listdir(path):
            if filename.endswith(".xml.fdm_material"):
                self.__loadMaterialProfileFromFile(os.path.join(path, filename))

    ## Add a new material to the material database.
    #  If the data is valid, and provides a newer profile then currently in the database, this material profile will be stored on disk in the material_storage_path.
    # @return True if material profile is successfully parsed and stored.
    # @return False if parsing failed, or the material profile was not added for other reasons.
    def addNewMaterial(self, data, signature_data=None):
        if self.__material_storage_path is None:
            log.error("Tried to add a new material without a material storage path set.")
            return False
        try:
            material_profile = MaterialProfile(data)
        except ValueError as e:
            log.error("%s", e)
            return False

        # Find a filename to use for this material profile. It could be that a material profile with the same name already exists,
        # so find a unique name.
        # This new file will be deleted, or the old file will be deleted, depending on which material is accepted in the database.
        full_path = os.path.join(self.__material_storage_path, "%s.xml.fdm_material" % (str(material_profile.getGUID())))
        n = 1
        while os.path.isfile(full_path):
            full_path = os.path.join(self.__material_storage_path, "%s.%d.xml.fdm_material" % (str(material_profile.getGUID()), n))
            n += 1
        signature_path = "%s.sig" % full_path

        try:
            with open(full_path, "wb") as file:
                file.write(material_profile.getXMLData().encode("utf-8"))
            if signature_data is not None:
                with open(signature_path, "wb") as file:
                    file.write(signature_data)
        except OSError:
            log.exception("Error during writing of new material.")
            return False

        # Try to insert this new material into our database, and remove it if it is rejected.
        if not self.__loadMaterialProfileFromFile(full_path):
            os.unlink(full_path)
            if signature_data is not None:
                os.unlink(signature_path)
            return False
        return True

    ## Update a material profile, this has a specified GUID with it.
    #  If an old material profile with the GUID does not exists, or the given new profile does not has the same GUID this function fails.
    #  Else it works the same as addNewMaterial
    #  @return False when the GUID currently does not exists in the system.
    #  @return False when the given GUID does not match the one in the material profile.
    #  @return True on success
    def updateMaterial(self, guid, data, signature_data=None):
        if guid not in self.__materials:
            log.error("Tried to update material with %s, but does not exist" % (guid))
            return False
        try:
            material_profile = MaterialProfile(data)
        except ValueError as e:
            log.error("%s", e)
            return False
        if material_profile.getGUID() != guid:
            log.error("Tried to update material with %s, but given xml contains GUID %s, but does not exist" % (guid, material_profile.getGUID()))
            return False
        return self.addNewMaterial(data, signature_data)

    ## Remove a material specified by a GUID
    #  This function will remove the material from the internal database, and from disk.
    #  But only if the file is in the material storage path, else the file is stock in the machine and cannot be removed.
    # @return True or False depending on the success of this function.
    def removeMaterialByGUID(self, guid):
        return self.__removeMaterial(guid)

    ## Remove all the materials located in the material storage path (factory reset)
    def removeMaterials(self):
        if self.__material_storage_path is None:
            return

        # Simply remove all the files without checking for extensions. Nothing else should be in this path location
        for filename in os.listdir(self.__material_storage_path):
            file = os.path.join(self.__material_storage_path, filename)
            log.info("Removing material: %s", file)
            os.unlink(file)

    ## Return a material profile by guid.
    #  @param guid: a uuid.UUID object to identify this material.
    #  @return a MaterialProfile object, or None if the material does not exist.
    def getMaterialByGUID(self, guid):
        return self.__materials.get(guid, None)

    ## Get materials by filter
    # @param filter: key value pairs on which will be filtered. An empty dictionary means no filters and will return all materials.
    # @param filter: key options: "brand", "material", "color"
    # @return an iterable containing MaterialProfile objects.
    def getMaterialsByFilter(self, filter=None):
        if filter is None:
            return self.__materials.values()
        result = []
        for material_profile in self.__materials.values():
            brand, material, color = material_profile.getMetadataNameFields()
            if "brand" in filter and brand != filter["brand"]:
                continue
            if "material" in filter and material != filter["material"]:
                continue
            if "color" in filter and color != filter["color"]:
                continue
            result.append(material_profile)
        return result

    ## Load a material profile from disk and add it to the material database.
    #  This also tries to validate the signature with the public keys.
    # @param full_path: Path to filename containing the material profile xml.
    def __loadMaterialProfileFromFile(self, full_path):
        try:
            with open(full_path, "rb") as file:
                data = file.read()
            material_profile = MaterialProfile(data)
            log.info("Loaded material profile: %s", full_path)
        except Exception as e:
            log.warning("Failed to load material profile from: %s, %s", full_path, e)
            return False
        signature_file = "%s.sig" % (full_path)
        if os.path.isfile(signature_file):
            if self.__gpg.verifySignature(full_path, signature_file):
                material_profile.setValidSignature(True)
                log.info("%s has a valid signature", full_path)
            else:
                material_profile.setValidSignature(False)
                log.info("%s has an invalid signature", full_path)
        if not self.__addMaterial(material_profile):
            return False
        material_profile.setStorageFilename(full_path)
        return True

    ## Add a material profile to this database.
    #  This function will check if a material profile with the same GUID already exists,
    #  if it does, it will check which has the higher version number, the highest version number one will be kept in the database.
    #  @param material_profile: A MaterialProfile instance that needs to be added.
    #  @return True when the MaterialProfile is added, False if it was rejected.
    def __addMaterial(self, material_profile):
        if material_profile.getGUID() in self.__materials:
            other_profile = self.__materials[material_profile.getGUID()]
            if material_profile.getMetadataNameFields() != other_profile.getMetadataNameFields():
                log.error("Duplicate GUID with different names: %s vs %s. Keeping the first one.", material_profile, other_profile)
            if not material_profile.hasValidSignature() and other_profile.hasValidSignature():
                log.error("Refused to replace signed material %s with unsigned material %s.", other_profile, material_profile)
                return False
            if material_profile.getVersion() <= other_profile.getVersion():
                log.error("Refused to replace material %s with material %s, as already known material has a higher version number", other_profile, material_profile)
                return False
            # Remove the old material file before adding the new one.
            self.__removeMaterial(material_profile.getGUID())
        self.__materials[material_profile.getGUID()] = material_profile
        return True

    ## Remove the local stored material profile from disk and from our internal database.
    # @return False when the GUID is not known in the database
    # @return False when the material profile is not stored in the material storage path (stock firmware material)
    # @return True when the material profile is successfully removed from the storage path and our internal database.
    def __removeMaterial(self, guid):
        material_profile = self.__materials.get(guid, None)
        if material_profile is None:
            return False
        old_filename = material_profile.getStorageFilename()
        if os.path.dirname(old_filename) == self.__material_storage_path:
            os.unlink(old_filename)
            old_signature_filename = "%s.sig" % (old_filename)
            if os.path.isfile(old_signature_filename):
                os.unlink(old_signature_filename)
            del self.__materials[guid]
            return True
        return False

if __name__ == "__main__":
    import uuid
    logging.basicConfig(level=logging.DEBUG)
    md = MaterialDatabase()
    md.setMaterialStoragePath(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "fdm_materials")))
    md.scanDirectoryForMaterials(os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "..", "..", "fdm_materials_private/materials")))
    print(md.getMaterialByGUID(uuid.UUID("86a89ceb-4159-47f6-ab97-e9953803d70f")).getSettingsFor("Ultimaker B.V.", "Ultimaker 3", "AA-0.4"))
    print(md.getMaterialByGUID(uuid.UUID("86a89ceb-4159-47f6-ab97-e9953803d70f")).getSettingsFor("Ultimaker B.V.", "Ultimaker 2", "0.4mm"))
    print(md.getMaterialsByFilter({"material": "PVA"}))
