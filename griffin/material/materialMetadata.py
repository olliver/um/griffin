from griffin.preferences.registry import Registry

## This class provides a controlled interface (wrapper) to maintaining the metadata for a material profile
class MaterialMetadata():
    ##< Name (settings) key definitions
    __BRAND = "brand"
    __MATERIAL = "material"
    __COLOR = "color"

    ##< Property key definitions
    __DENSITY = "density"
    __DIAMETER = "diameter"

    def __init__(self):
        self.__data = Registry()

    ## @brief Gets the brand name
    #  @return Returns the brand name as a string
    def getBrand(self):
        return self.__data.getAsString(self.__BRAND)

    ## @brief Sets the brand name
    #  @param brand Sets the brand name as a string
    def setBrand(self, brand):
        self.__data.setAsString(self.__BRAND, brand)

    ## @brief Gets the material name
    #  @return Returns the material name as a string
    def getMaterial(self):
        return self.__data.getAsString(self.__MATERIAL)

    ## @brief Sets the material name
    #  @param brand Sets the material name as a string
    def setMaterial(self, material):
        self.__data.setAsString(self.__MATERIAL, material)

    ## @brief Gets the color description
    #  @return Returns the color description as a string
    def getColor(self):
        return self.__data.getAsString(self.__COLOR)

    ## @brief Sets the color description
    #  @param brand Sets the color descriptions as a string
    def setColor(self, color):
        self.__data.setAsString(self.__COLOR, color)

    ## @brief Gets the density of the material
    #  @return Returns the density as a float (assuming g/cm^3)
    def getDensity(self):
        return self.__data.getAsFloat(self.__DENSITY)

    ## @brief Sets the density of the material
    #  @param density Sets the density as float (assuming g/cm^3)
    def setDensity(self, density):
        self.__data.setAsFloat(self.__DENSITY, density)

    ## @brief Gets the diameter of the material
    #  @return Returns the diameter as a float (assuming mm)
    def getDiameter(self):
        return self.__data.getAsFloat(self.__DIAMETER)

    ## @brief Sets the diameter of the material
    #  @param brand Sets the diameter as a float (assuming mm)
    def setDiameter(self, diameter):
        self.__data.setAsFloat(self.__DIAMETER, diameter)

    ## @brief Gets the brand, material and color in that order
    #  @return Returns a tuple with the values of these keys
    def getBrandMaterialColor(self):
        return self.__data.getValuesFromKeysAsTuple([self.__BRAND, self.__MATERIAL, self.__COLOR])

    ## @brief Gets the density and diameter in that order
    #  @return Returns a tuple with the values of these keys
    def getDensityDiameter(self):
        return self.__data.getValuesFromKeysAsTuple([self.__DENSITY, self.__DIAMETER])

