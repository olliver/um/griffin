import uuid
import logging
import copy
from xml.etree import ElementTree

from .materialProfileSettingParser import MaterialProfileSettingParser
from .materialMetadata import MaterialMetadata

log = logging.getLogger(__name__.split(".")[-1])


## MaterialProfile
# This class can load and return data from material files according to the material profile specification at:
# https://docs.google.com/document/d/1BYSACfFfGQ3i_ajf70FWtIkoEtLW30AMFuj3FKzt7iY
# It does not implement the all the details of the specification, as not all parts are needed by the rest of the system.
# It implements the name, GUID and version from the metadata. And methods to retrieve settings for a machine/nozzle combination.
# It does not implement the other metadata fields.
# This file loads the profile from a string, not directly from a file. This means it can parse material profile "in memory"
class MaterialProfile:
    ##< Define default safe diameter (in mm)
    __DEFAULT_DIAMETER = 2.85
    ##< Define default safe density (assuming in g/cm3). Probably meant to be https://en.wikipedia.org/wiki/Specific_weight
    __DEFAULT_DENSITY = 1.24

    ## Construct a new MaterialProfile
    #  read the file given to fill this MaterialProfile with data.
    #  The __init__ will raise an ValueError exception when it fails to parse the xml file properly or any of the fields contain invalid data.
    # @param material_profile_data: a byte array (usually read from a file) that contains the XML data.
    def __init__(self, material_profile_data):
        # Store the XML raw data, the web API needs to be able to query the full xml file contents.
        self.__xml_data = material_profile_data

        self.__material_metadata = MaterialMetadata()
        self.__version = 0
        self.__guid = None
        self.__global_settings = None
        self.__per_machine_settings = {}
        self.__has_valid_signature = False
        self.__storage_filename = None

        try:
            root = ElementTree.XML(material_profile_data)
        except ElementTree.ParseError:
            raise ValueError("Material profile could not be parsed as proper XML")

        if root.tag != "{%s}fdmmaterial" % (MaterialProfile.__XML_NAMESPACE):
            raise ValueError("Material profile does not contain <{%s}fdmmaterial> root node, root node is: <%s>" % (MaterialProfile.__XML_NAMESPACE, root.tag))

        self.__parseMetadata(root)
        self.__parseProperties(root)
        self.__parseSettings(root)

    # @return a uuid.UUID object that is unique for this material.
    def getGUID(self):
        return self.__guid

    # @return a tuple containing (brand, material, color) to identify this material.
    def getMetadataNameFields(self):
        return self.__material_metadata.getBrandMaterialColor()

    # @return the version number in this material profile (integer)
    def getVersion(self):
        return self.__version

    # @return the bytes object that was use to make this material profile.
    def getXMLData(self):
        return self.__xml_data

    ## Get a dictionary of settings in this material profile.
    # The settings will be filtered for a certain manufacturer/product name combination as well as a certain hotend in this machine.
    # @param manufacturer: string containing the machine manufacturer ID
    # @param product: string containing the machine product ID
    # @param hotend_identifier: string containing the hotend type ID
    # @return a new dictionary containing the settings. The keys will be the setting key names a specified in the material profile documentation.
    #         the value can be a float (for most settings) a string (for some settings) or an array of tuples (temperature flow graph)
    def getSettingsFor(self, manufacturer, product, hotend_identifier):
        settings = copy.deepcopy(self.__global_settings)
        if manufacturer in self.__per_machine_settings:
            if product in self.__per_machine_settings[manufacturer]:
                machine_data = self.__per_machine_settings[manufacturer][product]
                settings.update(copy.deepcopy(machine_data["settings"]))
                if hotend_identifier in machine_data["hotends"]:
                    settings.update(copy.deepcopy(machine_data["hotends"][hotend_identifier]))
        return settings

    def getProperties(self):
        return {
            "density": self.__material_metadata.getDensity(),
            "diameter": self.__material_metadata.getDiameter(),
        }

    ## Set if this material profile has a valid signature or not.
    #  @param valid: bool, true when the signature is valid, false if not.
    def setValidSignature(self, valid):
        assert type(valid) == bool
        self.__has_valid_signature = valid

    ## Check if this material profile has been marked as having a valid signature.
    #  @return true or false.
    #  (Future enhancement: signatures can be valid, invalid or absent, right now invalid and absent are handled the same)
    def hasValidSignature(self):
        return self.__has_valid_signature

    ## Check if this material profile has settings for a specific machine.
    # @return bool, true if this material profile has settings for this machine, false if not.
    def hasSettingsFor(self, manufacturer, product):
        if manufacturer not in self.__per_machine_settings:
            return False
        if product not in self.__per_machine_settings[manufacturer]:
            return False
        return True

    # @param: string containing the filaname that contains this material profile
    def setStorageFilename(self, filename):
        self.__storage_filename = filename

    # @return: The filename set with the setStorageFilename, or None if no filename is known.
    def getStorageFilename(self):
        return self.__storage_filename

    # Returns the child node or None if noot found
    def __optionalChildNode(self, node, child_node_tag):
        return node.find("{%s}%s" % (MaterialProfile.__XML_NAMESPACE, child_node_tag))

    # Private function used to get child nodes of the elementree XML structure.
    # This function requires the child to exist, else an ValueError is thrown.
    def __requireChildNode(self, node, child_node_tag):
        child = self.__optionalChildNode(node, child_node_tag)
        if child is None:
            raise ValueError("Material profile does not contain <{%s}%s> node in <%s> node" % (MaterialProfile.__XML_NAMESPACE, child_node_tag, node.tag))
        return child

    # Parse the metadata part of the material profile, and store the data that we are interrested in.
    # This function can throw an ValueError when the input data is invalid.
    def __parseMetadata(self, root):
        metadata_node = self.__requireChildNode(root, "metadata")
        name_node = self.__requireChildNode(metadata_node, "name")
        self.__material_metadata.setBrand(self.__requireChildNode(name_node, "brand").text)
        self.__material_metadata.setMaterial(self.__requireChildNode(name_node, "material").text)
        self.__material_metadata.setColor(self.__requireChildNode(name_node, "color").text)
        self.__guid = uuid.UUID(self.__requireChildNode(metadata_node, "GUID").text)
        self.__version = int(self.__requireChildNode(metadata_node, "version").text)

    ## @brief Gets the (optional) properties for the material and store them
    #  @param root The root node to start searching from
    def __parseProperties(self, root):
        properties_node = self.__requireChildNode(root, "properties")
        self.__material_metadata.setDensity(self.__getOptionalNodeValue(properties_node, "density", MaterialProfile.__DEFAULT_DENSITY))
        self.__material_metadata.setDiameter(self.__getOptionalNodeValue(properties_node, "diameter", MaterialProfile.__DEFAULT_DIAMETER))


    ## @brief Gets the value for an optional node (or the default in case it does not exist)
    #  @param node The root node to search for the childnode
    #  @param key The name for the childnode to locate
    #  @param default The default value to return in case the childnode cannot be found
    #  @return The value of the child node (or the default)
    def __getOptionalNodeValue(self, node, key, default):
        child = self.__optionalChildNode(node, key)
        if child is None:
            return  default
        else:
            return child.text

    def __parseSettings(self, root):
        settings_node = self.__requireChildNode(root, "settings")
        self.__global_settings = self.__retrieveSettingsFromNode(settings_node)
        for machine_node in settings_node.findall("{%s}machine" % (MaterialProfile.__XML_NAMESPACE)):
            machine_settings = self.__retrieveSettingsFromNode(machine_node)
            machine_data = {
                "settings": machine_settings,
                "hotends": {}
            }

            # Read our settings per hotend.
            for hotend_node in machine_node.findall("{%s}hotend" % (MaterialProfile.__XML_NAMESPACE)):
                hotend_id = hotend_node.get("id")
                if hotend_id is None:
                    raise ValueError("Got a <%s> node without a \"hotend_id\" attribute" % (machine_identifier_node.tag))
                if hotend_id in machine_data["hotends"]:
                    raise ValueError("Duplicate hotend_id definition: %s" % (hotend_id))
                machine_data["hotends"][hotend_id] = self.__retrieveSettingsFromNode(hotend_node)

            # Store these machine specific settings per machine/product combo.
            for machine_identifier_node in machine_node.findall("{%s}machine_identifier" % (MaterialProfile.__XML_NAMESPACE)):
                manufacturer = machine_identifier_node.get("manufacturer")
                if manufacturer is None:
                    raise ValueError("Got a <%s> node without a \"manufacturer\" attribute" % (machine_identifier_node.tag))
                product = machine_identifier_node.get("product")
                if product is None:
                    raise ValueError("Got a <%s> node without a \"product\" attribute" % (machine_identifier_node.tag))

                if manufacturer not in self.__per_machine_settings:
                    self.__per_machine_settings[manufacturer] = {}
                if product in self.__per_machine_settings[manufacturer]:
                    raise ValueError("Duplicate manufacturer/product definition: %s/%s" % (manufacturer, product))
                self.__per_machine_settings[manufacturer][product] = machine_data

    def __retrieveSettingsFromNode(self, node):
        settings = {}
        for setting_node in node.findall("{%s}setting" % (MaterialProfile.__XML_NAMESPACE)):
            key = setting_node.get("key")
            if key is None:
                log.warning("<setting> node does not contain key attribute.")
                continue
            if key not in MaterialProfile.__SETTING_PARSE_FUNCTIONS:
                # Unknown keys is a warning, not an error, for forwards compatibility.
                log.warning("<setting key=\"%s\"> not known by material service.", key)
                continue
            settings[key] = MaterialProfile.__SETTING_PARSE_FUNCTIONS[key](setting_node)
        return settings

    def __repr__(self):
        return "MaterialProfile<%s>(%s %s %s) - diameter: %f, density: %f" % (
                self.__guid,
                self.__material_metadata.getBrand(), self.__material_metadata.getMaterial(), self.__material_metadata.getColor(),
                self.__material_metadata.getDiameter(), self.__material_metadata.getDensity(),
            )

    __XML_NAMESPACE = "http://www.ultimaker.com/material"
    __SETTING_PARSE_FUNCTIONS = {
        "print temperature": MaterialProfileSettingParser.parseTemperatureNode,
        "heated bed temperature": MaterialProfileSettingParser.parseTemperatureNode,
        "maximum heated bed temperature": MaterialProfileSettingParser.parseTemperatureNode,
        "material bed adhesion temperature": MaterialProfileSettingParser.parseTemperatureNode,
        "heated chamber temperature": MaterialProfileSettingParser.parseTemperatureNode,
        "maximum heated chamber temperature": MaterialProfileSettingParser.parseTemperatureNode,
        "standby temperature": MaterialProfileSettingParser.parseTemperatureNode,
        "processing temperature graph": MaterialProfileSettingParser.parseProcessingTemperatureGraph,
        "print cooling": MaterialProfileSettingParser.parsePercentage,
        "retraction amount": MaterialProfileSettingParser.parseDistanceInMillimeters,
        "retraction speed": MaterialProfileSettingParser.parseSpeedInMillimetersPerSecond,
        "hardware compatible": MaterialProfileSettingParser.parseCompatibility,
    }


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    import os
    mp = MaterialProfile(open(os.path.join(os.path.dirname(__file__), "..", "..", "..", "fdm_materials/materials/ultimaker_pla_silver_metallic.xml.fdm_material"), "rb").read())
    print(mp)
    print(mp.getGUID(), mp.getVersion())
    print(mp.getMetadataNameFields())
    print(mp.getProperties())
    print(mp.getSettingsFor("Ultimaker B.V.", "Ultimaker Original", "0.4mm"))
    print(mp.getSettingsFor("Ultimaker B.V.", "Ultimaker Original", "0.6mm"))
