from griffin.interface.http.exposedItems.httpExposedType import HttpExposedType


# HttpExposedInt is a special case of an HttpExposedType. Its get function returns single int value.
class HttpExposedInt(HttpExposedType):
    def __init__(self, *args, **kwargs):
        super().__init__(int, *args, **kwargs)