from griffin.interface.http.exposedItems.httpExposedType import HttpExposedType


# HttpExposedBool is a special case of an HttpExposedType. Its get function returns single bool value.
class HttpExposedBool(HttpExposedType):
    def __init__(self, *args, **kwargs):
        super().__init__(bool, *args, **kwargs)
