from griffin.interface.http.exposedItems.httpExposedType import HttpExposedType


# HttpExposedFloat is a special case of an HttpExposedItem. Its get function returns single float value.
class HttpExposedFloat(HttpExposedType):
    def __init__(self, *args, **kwargs):
        super().__init__(float, *args, **kwargs)