from griffin.interface.http.exposedItems.httpExposedItem import HttpExposedItem
from griffin.interface.http.flaskResponses import FlaskResponses

import json

import logging
log = logging.getLogger(__name__.split(".")[-1])


# HttpExposedList is a special case of an HttpExposedItem. Its get function returns a json array containing the information
# of all its children.
class HttpExposedList(HttpExposedItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        response = []
        for child in self._children:
            child_response = child.get()
            if child_response.status_code == FlaskResponses.HTTP_OK:   # If the get was successful, add the data in the response to the list
                response.append(json.loads(child_response.get_data().decode("utf-8")))
            else:
                log.warning("Child response failure: %d" % child_response.status_code)

        return FlaskResponses.createSuccess(None, response)
