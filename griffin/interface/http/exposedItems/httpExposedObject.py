from griffin.interface.http.exposedItems.httpExposedItem import HttpExposedItem
from griffin.interface.http.flaskResponses import FlaskResponses

import json

import logging
log = logging.getLogger(__name__.split(".")[-1])


# HttpExposedObject is a special case of an HttpExposedItem. Its get function returns a json object containing the information
# of all its children.
class HttpExposedObject(HttpExposedItem):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get(self):
        data = {}
        for child in self._children:
            child_response = child.get()
            if child_response.status_code == FlaskResponses.HTTP_OK:   # If the get was successful, add the data in the response to the dict
                data[child.getLocalPath().strip("/")] = json.loads(child_response.get_data().decode("utf-8"))
            else:
                log.warning("Child response failure: %d" % child_response.status_code)

        return FlaskResponses.createSuccess(None, data)

    def put(self, received_data):
        for child in self._children:
            if child.getLocalPath() in received_data:
                child.put(received_data[child.getLocalPath()])
        return FlaskResponses.createNoContent("Set")
