from griffin.interface.http.exposedItems.httpExposedType import HttpExposedType


# HttpExposedString is a special case of an HttpExposedType. Its get function returns a single string value.
class HttpExposedString(HttpExposedType):
    def __init__(self, *args, **kwargs):
        super().__init__(str, *args, **kwargs)