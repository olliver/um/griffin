from griffin.interface.http.exposedItems.httpExposedItem import HttpExposedItem
from griffin.interface.http.flaskResponses import FlaskResponses
from griffin import dbusif

import logging
log = logging.getLogger(__name__.split(".")[-1])


# HttpExposedBool is a special case of an HttpExposedItem. Its get function returns single bool value.
class HttpExposedType(HttpExposedItem):
    __property_owners = {}
    __property_data = {}

    ## Simple conversion to primary types like int, float, str, bool...
    #  @param item_type The type used for conversions
    def __init__(self, item_type, *args, **kwargs):
        self.__type = item_type

        self._get_function = kwargs.pop("get_function", self.__default_get_function)
        self._put_function = kwargs.pop("put_function", self.__default_put_function)
        self.__property_key = kwargs.pop("property", None)
        self.__property_owner = None
        if self.__property_key is not None:
            property_owner = kwargs.pop("property_owner", "printer")
            if property_owner not in HttpExposedType.__property_owners:
                owner = dbusif.RemoteObject("printer", property_owner)
                HttpExposedType.__property_owners[property_owner] = owner
                HttpExposedType.__property_data[property_owner] = owner.getProperties()
                owner.connectSignal("propertyChanged", self.__createUpdatePropertyFunction(property_owner))
            self.__property_owner = property_owner

        # If no allowed_request_methods specified, then fill this according to if a put method is specified.
        if "allowed_request_methods" not in kwargs:
            kwargs["allowed_request_methods"] = ["GET"]
            # We have a custom put function, we can safely assume we also want to allow put requests.
            if self._put_function != self.__default_put_function:
                kwargs["allowed_request_methods"].append("PUT")

        super().__init__(*args, **kwargs)

    def __createUpdatePropertyFunction(self, owner):
        return lambda key, value: self.__updatePropertyValue(owner, key, value)

    def __updatePropertyValue(self, owner, key, value):
        HttpExposedType.__property_data[owner][key] = value

    def get(self):
        data = self._get_function()
        try:
            current = self.__type(data)
        except ValueError:
            log.warning("Unable to convert data for %s" % self.getAbsolutePath())
            current = self.__getDefaultValue()

        return FlaskResponses.createSuccess(None, current)

    def put(self, received_data):
        try:
            return_value = self._put_function(self.__type(received_data))
            if return_value is not None:
                result_value = {"return_value": return_value, "result": True}
            else:
                # None cannot be send over dbus - hence conversion to None
                result_value = {"return_value": "None", "result": True}
            result_value["message"] = str(self.__type) + " set"
            # If createNoContent is used, ajax calls will not see the result for some reason? (wifi setup!)
            return FlaskResponses.createSuccess(None, result_value)
        except:
            msg = "An exception occurred while trying to set data as a(n) %s" % str(self.__type)
            log.exception(msg)
            return FlaskResponses.createFailed(msg)

    ## Default get function for HttpExposted types, this gets a property from a service.
    #  as many of the API calls are a direct link to properties.
    #  @return value from the property linked during construction.
    def __default_get_function(self):
        if self.__property_owner is None:
            return self.__getDefaultValue()
        return HttpExposedType.__property_data[self.__property_owner].get(self.__property_key, self.__getDefaultValue())

    ## Default put function, which per default does nothing.
    #  @param value: Value which the user tries to put into this item.
    def __default_put_function(self, value):
        pass

    ## @brief Determines the default value for the specified type
    #  @return Returns the default value for the set type
    def __getDefaultValue(self):
        if self.__type == str:
            return ""
        else:
            return 0
