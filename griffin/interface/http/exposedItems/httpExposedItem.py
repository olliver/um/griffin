from griffin.interface.http.server import Server
from griffin.interface.http.flaskResponses import FlaskResponses

import flask

import logging
log = logging.getLogger(__name__.split(".")[-1])


# HttpExposedItem is the base class for anything (item) exposed in the HTTP RESTfull API.
# It heavily uses lambda functions (get_function and put_function) to define how to handle get/put requests.
# This class is designed to be nested; either as an exposed 'root' object in the server or as a child of another httpexposeditem (or one of its derivatives).
# When making your own implementation of this, one should override the post, get, put or delete functions as required.
class HttpExposedItem():
    # Constructor of the http exposed item.
    def __init__(self, local_path, parent=None, allowed_request_methods=None, authenticated_methods=None):
        super().__init__()
        self._parent = None
        self._local_path = local_path
        self._children = []
        self._server = Server.getInstance()
        self._deleted = False   # Work around. Flask does not allow deletion, so we need to remember deleted objects somehow.
        if allowed_request_methods is None:
            allowed_request_methods = ["GET"]
        self._allowed_request_methods = allowed_request_methods
        if authenticated_methods is None:
            authenticated_methods = ["POST", "PUT", "DELETE"]
        self.__authenticated_methods = authenticated_methods

        if parent:
            parent.addChild(self)

    # Add a child to this object.
    # This is usefull for making nested API calls.
    # Example; If you want the api call /foo/foobar to work you should create a foo object and add foobar as a child.
    # The path is decided by the local_path variable.
    def addChild(self, child):
        # Child already has a parent. Should not happen, but it's easy enough to fix.
        if child.getParent() is not None:
            child.getParent().removeChild(child)
        child.setParent(self)
        self._children.append(child)

    def removeChild(self, child):
        if child in self._children:
            self._children.remove(child)
            child.setParent(None)

    # Register ensures that the correct functions are attached to the correct api paths.
    def register(self):
        if self._allowed_request_methods is None:
            # Some child objects can only be set by their parents (eg not called directly). These don't need a rule.
            return

        # Endpoint is a bit of a misnomer in flask. It's actually a unique ID.
        # If no endpoint is given, the function name is used to generate it.
        # This strategy caused conflicts when using multipleHTTPExposed objects.
        endpoint_prefix = str(id(self))

        # We add a single handler for all requests, so the sub classes are more generalized.
        # The subclasses themselves should add the correct request methods to the self._allowed_request_methods list
        self._server.add_url_rule(self.getAbsolutePath(), endpoint=endpoint_prefix + "_handleHttpRequest", view_func=self.handleHttpRequest, methods=["GET", "POST", "PUT", "DELETE"], authenticated_methods=self.__authenticated_methods)

    # Check if the object is considered deleted.
    # This can either be because the object in itself is deleted or one of its parents is deleted.
    def isDeleted(self):
        if self._parent is None:
            return self._deleted
        else:
            return self._deleted or self._parent.isDeleted()

    # Function that ensures that the correct methods are called based on the requests.
    # This function is put 'in between' because of how we are forced to handle delete requests.
    # Werkzeug does not support the removal of rules, so we must 'fake' the deletion (eg return 405 status codes)
    # when the object has been deleted.
    def handleHttpRequest(self):
        if flask.request.method in self._allowed_request_methods:
            if not self.isDeleted():
                if flask.request.method == "GET":
                    return self.get()
                elif flask.request.method == "DELETE":
                    return self.delete()
                elif flask.request.method == "POST":
                    return self.post(received_data=flask.request)
                elif flask.request.method == "PUT":
                    return self.put(received_data=flask.request.json)
                else:
                    return FlaskResponses.createMethodNotAllowed()
            else:
                return FlaskResponses.createNotFound()
        else:
            return FlaskResponses.createMethodNotAllowed()

    # Get list of all children (including its children children children etc.)
    # \returns list all children in this "tree"
    def getAllChildren(self):
        children = []
        children.extend(self._children)
        for child in self._children:
            children.extend(child.getAllChildren())
        return children

    # Set the parent of this object
    # \param parent The HttpExposedItem to be used as parent
    # \sa addChild
    def setParent(self, parent):
        self._parent = parent

    def getParent(self):
        return self._parent

    # Local path is the path, relative to its parent, which decides how this object can be reached.
    # In case of a default setup, this would mean that with a local path of "test" an object in the root bit of the server
    # can be reached by ip/api/v1/test. If this object has a child, that child can be reached by ip/api/v1/test/test2 (if the
    # local path of the child is "test2"
    # \param path new path of this item.
    def setLocalPath(self, path):
        self._local_path = path

    # Get the local path.
    # \sa setLocalPath
    # \returns the local path.
    def getLocalPath(self):
        return self._local_path

    def get(self):
        raise NotImplementedError()

    def post(self, received_data):
        raise NotImplementedError()

    def put(self, received_data):
        raise NotImplementedError()

    def delete(self):
        self._deleted = True

    # Get the absolute path required to call this object.
    # note that the server might add a static prefix and a version prefix.
    def getAbsolutePath(self):
        if self._parent is None:
            return "/" + self.getLocalPath()
        else:
            parent_path = self._parent.getAbsolutePath()
            if not parent_path.endswith("/"):
                parent_path += "/"
            return parent_path + self.getLocalPath()
