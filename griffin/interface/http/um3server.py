import flask

from urllib.parse import urlparse

from griffin.interface.http.server import Server

from griffin.interface.http.exposedItems.httpExposedItem import HttpExposedItem
from griffin.interface.http.exposedItems.httpExposedList import HttpExposedList
from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.exposedItems.httpExposedFloat import HttpExposedFloat
from griffin.interface.http.exposedItems.httpExposedBool import HttpExposedBool
from griffin.interface.http.exposedItems.httpExposedString import HttpExposedString
from griffin.interface.http.exposedItems.httpExposedInt import HttpExposedInt
from griffin.interface.http.endpoints.printJob import PrintJob
from griffin.interface.http.endpoints.printJobState import PrintJobState
from griffin.interface.http.authentication.authenticationAPI import AuthenticationAPI
from griffin.interface.http.endpoints.wifi import Wifi
from griffin.interface.http.endpoints.materials import Materials
from griffin.interface.http.endpoints.beep import Beep
from griffin.interface.http.endpoints.diagnostics import Diagnostics
from griffin.interface.http.systemLogItem import SystemLogItem
from griffin.interface.http.wsprint.xmlSoapEndpoint import XmlSoapEndpoint
from griffin.interface.http.wsprint import probeAction
from griffin.interface.http.wsprint import transferGetAction
from griffin.interface.http.wsprint import printGetPrinterElementsAction

from griffin.interface.http.authentication.authenticationService import AuthenticationService

from griffin import dbusif
from griffin import thread

## Ultimaker3 WebServices (REST API) implementatie
#
#    @date December 2015
#
class UM3Server(Server):
    # Cache served files for a maximum of 5 minutes to prevent problems with our dns hijacking mechanism on the captive portal.
    FILE_CACHING_TIME = 5 * 60
    # File upload size limit in megabytes.
    FILE_UPLOAD_SIZE_LIMIT = 512

    API_BASE_PATH = "api"

    def __init__(self, import_name = "Ultimaker 3", static_file_location = "/usr/share/griffin/www", port = 80, **kwargs):
        super().__init__(import_name, **kwargs)
        self._port = port

        self.__network_service = dbusif.RemoteObject("network")
        self.__system_service = dbusif.RemoteObject("system")
        self.__printer_service = dbusif.RemoteObject("printer")
        self.__camera_service = dbusif.RemoteObject("printer")

        # Setup our special 404 handler for captive portal redirects.
        self.error_handler_spec[None][404] = self._handleFileNotFound

        # Create the base path for all our API calls.
        api = HttpExposedItem(UM3Server.API_BASE_PATH, allowed_request_methods=[])
        api_v1 = HttpExposedItem("v1", api, allowed_request_methods=[])

        # Base object setup (printer, system, camera, materials, print_job)
        printer = HttpExposedObject("printer", api_v1)
        system = HttpExposedObject("system", api_v1)
        camera = HttpExposedObject("camera", api_v1)
        materials = Materials("materials", api_v1)
        print_job = PrintJob("print_job", api_v1)
        auth = AuthenticationAPI("auth", api_v1)
        Coffee(api_v1)

        # Printer exposure
        self._setupLedInterface(printer)
        self._setupHeadInterface(printer)

        bed             = HttpExposedObject("bed", printer)
        bed_temperature = HttpExposedObject("temperature", bed)
        current = HttpExposedFloat("current", bed_temperature, property_owner="printer/bed", property="current_temperature")
        target = HttpExposedFloat("target", bed_temperature, property_owner="printer/bed", property="target_temperature", put_function=lambda data: self.__setTargetTemperature("printer/bed", data))

        printer_status = HttpExposedString("status", printer, property_owner="printer", property="state")

        beep = Beep("beep", printer)
        diagnostics = Diagnostics("diagnostics", printer)

        # Network exposure
        network_state = HttpExposedObject("network", printer)
        self._setupWifiInterface(network_state)
        self._setupEthernetInterface(network_state)

        # System exposure
        self._setupSystemInterface(system)

        # Camera exposure
        self._setupCameraInterface(camera)

        # Print job exposure
        self._setupPrintJobInterface(print_job)

        # Add certain objects as "root".
        self.addExposedObject(api_v1)

        # WebService settings
        self.setStaticFileLocation(static_file_location)
        # Configure the caching time for static files, default is 12 hours in Flask.
        self.config["SEND_FILE_MAX_AGE_DEFAULT"] = UM3Server.FILE_CACHING_TIME
        # Maximum upload file size
        self.config['MAX_CONTENT_LENGTH'] = UM3Server.FILE_UPLOAD_SIZE_LIMIT * 1024 * 1024

        # Only enable the WSPrint endpoint in developer mode right now, as it links to a non-functional driver.
        if self.__system_service.isDeveloperModeActive():
            self.__setupWSPrint()

        # Thread settings
        self._thread = thread.Thread("UM3Server thread", self.threadedRun)
        self._thread.daemon = True
        self._thread.start()

        self.__network_service.connectSignal("modeChanged", self._onNetworkModeChanged)

        # Create the authentication service, this will generate create the DBus service that is used to communicate authentication requests with the rest of the system.
        AuthenticationService(auth)

    ## Run as a service thread
    #
    def threadedRun(self):
        self.run(host="0.0.0.0", port=self._port, threaded=True)

    ## Create the WSPrint endpoint.
    #  This endpoint implemented a limited WSPrint2.0 spec, which allows Windows 10 to discover the Ultimaker 3 as printer and install the driver.
    #  The print job API is not supported.
    def __setupWSPrint(self):
        # Hardcoded UUID, until we get an answer from Microsoft about what this uuid entitles.
        # Maybe we need a fixed UUID, maybe we need a uuid per boot, maybe we need a uuid per machine (which the system server has)
        # But right now, we don't know!
        uuid = "e22995b5-b24c-42a8-a0a9-e2dd2d6c39fa"

        # Path part of the WSPrint endpoint. We can customize this as much as we like.
        wsprint_endpoint_name = "WSPrintEndpoint"

        # Create our discovery endpoint, this is is a hardcoded path that allows other devices discover us and know which endpoint to talk to.
        discovery_endpoint = XmlSoapEndpoint("StableWSDiscoveryEndpoint/schemas-xmlsoap-org_ws_2005_04_discovery")
        discovery_endpoint.addAction(probeAction.ProbeAction(uuid, wsprint_endpoint_name))
        self.addExposedObject(discovery_endpoint)

        # Our actual WSPrint endpoint. All actions are communicated to this endpoint.
        wsprint_endpoint = XmlSoapEndpoint(wsprint_endpoint_name)
        wsprint_endpoint.addAction(transferGetAction.TransferGetAction(uuid, wsprint_endpoint_name))
        wsprint_endpoint.addAction(printGetPrinterElementsAction.PrintGetPrinterElementsAction(uuid))

        self.addExposedObject(wsprint_endpoint)

    ## Setup the Led interface
    #  @param object printer The printer object
    #
    def _setupLedInterface(self, printer):
        led_service = dbusif.RemoteObject("led")
        case_led            = HttpExposedObject("led",       printer, allowed_request_methods=["GET", "PUT"])
        case_led_hue        = HttpExposedFloat("hue",        case_led,
            get_function = lambda: led_service.getHue("STRIP"),
            put_function = lambda value: led_service.setHue("STRIP", value)
        )
        case_led_saturation = HttpExposedFloat("saturation", case_led,
            get_function=lambda: led_service.getSaturation("STRIP"),
            put_function=lambda value: led_service.setSaturation("STRIP", value)
        )
        case_led_brightness = HttpExposedFloat("brightness", case_led,
            get_function=lambda: led_service.getBrightness("STRIP"),
            put_function=lambda value: led_service.setBrightness("STRIP", value)
        )

    ## Setup the head interfaces
    #  @param object printer The printer object
    #
    def _setupHeadInterface(self, printer):
        heads = HttpExposedList("heads", printer)
        head  = HttpExposedObject("0",   heads)

        head_position = HttpExposedObject("position", head, allowed_request_methods=["GET", "PUT"])
        self._setupAxisInteface(head_position, "x")
        self._setupAxisInteface(head_position, "y")
        self._setupAxisInteface(head_position, "z")

        # Marlin has no support for seperate X and Y jerk, hence the XY_JERK for both of them
        self._setupXYZInterface("jerk", head, "jerk_xy", "jerk_xy", "jerk_z", ["GET", "PUT"])
        self._setupXYZInterface("max_speed", head, "max_speed_x", "max_speed_y", "max_speed_z", ["GET", "PUT"])
        head_fan = HttpExposedFloat("fan", head, property_owner="printer/head/0", property="cooling_fan_speed")
        head_acceleration = HttpExposedFloat("acceleration", head, property="acceleration_xzy", allowed_request_methods = ["GET", "PUT"])

        self._setupExtruderAndHotEndInterface(head)

    ## Setup the Axis interface of the head
    #  @param object head_position The head position
    #  @param string axis The axis (x, y or z)
    #
    def _setupAxisInteface(self, head_position, axis):
        head_axis = HttpExposedFloat(axis, head_position,
            get_function = lambda: self.__printer_service.getProcedureMetaData("MOVE_HEAD")["current"][axis],
            put_function = lambda data: self.__printer_service.startProcedure("MOVE_HEAD", {axis: data})
        )

    ## Setup the extruders and hotends of the head
    #  @param head The head object
    #
    def _setupExtruderAndHotEndInterface(self, head):
        extruder_list = HttpExposedList("extruders", head)

        hotend_count = self.__printer_service.getProperty("hotend_count")
        for index in range(0, hotend_count):
            extruder = self._setupExtruderInterface(extruder_list, index)
            hot_end  = self._setupHotEndInterface(extruder, index)

    ## Setup the nth Extruder interface and returns it
    #  @param object extruders The available extruders
    #  @param string index The index for the extruders
    #  @return object Returns single extruder object
    #
    def _setupExtruderInterface(self, extruder_list, index):
        extruder            = HttpExposedObject(str(index), extruder_list)
        feeder              = HttpExposedObject("feeder", extruder)
        feeder_acceleration = HttpExposedFloat("acceleration",feeder, property="acceleration_e", allowed_request_methods=["GET", "PUT"])
        feeder_jerk         = HttpExposedFloat("jerk", feeder, property="jerk_e", allowed_request_methods=["GET", "PUT"])
        feeder_max_speed    = HttpExposedFloat("max_speed", feeder, property="max_speed_e", allowed_request_methods=["GET", "PUT"])
        active_material     = HttpExposedObject("active_material", extruder)
        length_remaining    = HttpExposedFloat("length_remaining", active_material, get_function=lambda: -1) # -1 is a dummy value for unknown.
        material_guid       = HttpExposedString("guid", active_material, property_owner="printer/head/0/slot/%d" % (index), property="material_guid")
        # Keep the old upper case GUID endpoint until minimal 2017. This is to have a transitional period from internal Cura releases that use this endpoint.
        material_guid_old   = HttpExposedString("GUID", active_material, property_owner="printer/head/0/slot/%d" % (index), property="material_guid")
        return extruder

    ## Setup the HotEnd interface the given extruder
    #  @param object extruder The extruder object
    #  @param int index The index of the hotend slot we are setting up.
    #
    def _setupHotEndInterface(self, extruder, index):
        property_owner = "printer/head/0/slot/%d" % (index)

        hotend = HttpExposedObject("hotend", extruder)
        temperature = HttpExposedObject("temperature", hotend)
        current = HttpExposedFloat("current", temperature, property_owner=property_owner, property="current_temperature")
        target = HttpExposedFloat("target", temperature, property_owner=property_owner, property="target_temperature", put_function=lambda data: self.__setTargetTemperature(property_owner, data))
        cartridge_id = HttpExposedString("id", hotend, get_function=lambda : self.__printer_service.getHotendCartridgeProperty(index, "hotend_cartridge_id"))
        offset = HttpExposedObject("offset", hotend)

        hotend_slot_0 = dbusif.RemoteObject("printer", "printer/head/0/slot/%d" % (0))
        hotend_slot = dbusif.RemoteObject("printer", "printer/head/0/slot/%d" % (index))

        HttpExposedFloat("x", offset, get_function=lambda: hotend_slot.getProperty("x_offset") if hotend_slot.getProperty("x_offset") != "" else 0 + self.__printer_service.getProperty("hotend_offset_1_x"))
        HttpExposedFloat("y", offset, get_function=lambda: hotend_slot.getProperty("y_offset") if hotend_slot.getProperty("y_offset") != "" else 0 + self.__printer_service.getProperty("hotend_offset_1_y"))
        HttpExposedFloat("z", offset, get_function=lambda: hotend_slot.getProperty("z_height") - hotend_slot_0.getProperty("z_height") if hotend_slot.getProperty("z_height") != "" and hotend_slot_0.getProperty("z_height") != "" else 0)
        # We only check if the X is set here, if the Z is not set that can be fine, as we are active leveling. And when the X is set, the Y is set for sure.
        HttpExposedString("state", offset, get_function=lambda: "invalid" if hotend_slot.getProperty("x_offset") == "" or hotend_slot.getProperty("z_height") == float("inf") or hotend_slot_0.getProperty("z_height") == float("inf") else "valid")

    ## Helper to create an XYZ object.
    #  @param string local_path The local file local_path
    #  @param object parent The parent object
    #  @param string x_procedure The name of the X procedure
    #  @param string y_procedure The name of the Y procedure
    #  @param string z_procedure The name of the Z procedure
    #  @param array allow_request_methods The allowed methods to be used by the interface
    #
    def _setupXYZInterface(self, local_path, parent, x_property, y_property, z_property, allowed_request_methods):
        xyz_object = HttpExposedObject(local_path, parent, allowed_request_methods = allowed_request_methods)
        x_object   = HttpExposedFloat("x", xyz_object, property = x_property, allowed_request_methods = None)
        y_object   = HttpExposedFloat("y", xyz_object, property = y_property, allowed_request_methods = None)
        z_object   = HttpExposedFloat("z", xyz_object, property = z_property, allowed_request_methods = None)

    ## Setup the Wifi interface
    #  @param object network_state The network state object
    #
    def _setupWifiInterface(self, network_state):
        wifi           = HttpExposedObject("wifi",    network_state)
        wifi_connected = HttpExposedBool("connected", wifi, get_function = lambda: dbusif.RemoteObject("network","/nl/ultimaker/network").getConnectedMethod() in ["WIFI", "HOTSPOT"] )
        wifi_enabled   = HttpExposedBool("enabled",   wifi, get_function = lambda: dbusif.RemoteObject("network","/nl/ultimaker/network").getMode() in ["AUTO", "HOTSPOT", "WIFI SETUP", "WIRELESS"] )
        wifi_ssid      = HttpExposedString("ssid",    wifi, get_function = lambda: dbusif.RemoteObject("network","/nl/ultimaker/network").getHotspotSSID() )
        wifi_networks  = Wifi("wifi_networks", network_state)

    ## Setup the Ethernet interface
    #  @param object network_state The network state object
    #
    def _setupEthernetInterface(self, network_state):
        ethernet           = HttpExposedObject("ethernet", network_state)
        ethernet_connected = HttpExposedBool("connected",  ethernet, get_function=lambda: self.__network_service.getConnectedMethod() in ["ETHERNET"])
        ethernet_enabled   = HttpExposedBool("enabled",    ethernet, get_function=lambda: self.__network_service.getMode() in ["AUTO", "CABLE"])

    ## Setup the System interface(s)
    #  @param object system The system object
    #
    def _setupSystemInterface(self, system):
        platform    = HttpExposedString("platform", system, get_function=lambda: self.__system_service.getPlatform())
        hostname    = HttpExposedString("hostname", system, get_function=lambda: self.__system_service.getHostName())
        system_name = HttpExposedString("name"    , system,
            get_function = lambda : self.__system_service.getMachineName(),
            # @todo: handle result code from setters (EM-358)
            put_function = lambda data: self.__system_service.setMachineName(data)
        )
        firmware    = HttpExposedString("firmware", system,
            get_function=lambda: self.__system_service.getVersion(),
            put_function=lambda data: self.__system_service.startUpdate()
        )

        memory       = HttpExposedObject("memory", system)
        memory_used  = HttpExposedInt("used" , memory, get_function=lambda: self.__system_service.getMemoryUsage()[0])
        memory_total = HttpExposedInt("total", memory, get_function=lambda: self.__system_service.getMemoryUsage()[1])

        HttpExposedString("type", system, get_function=lambda: "3D printer")
        HttpExposedString("variant", system, get_function=lambda: self.__printer_service.getProperty("machine_type_name"))
        
        hardware     = HttpExposedObject("hardware", system)
        HttpExposedInt("typeid", hardware, get_function=lambda: self.__system_service.getMachineBOM()[0])
        HttpExposedInt("revision", hardware, get_function=lambda: self.__system_service.getMachineBOM()[1])

        # Some clarification on the log; The system get log call returns a dbus array with strings.
        # We use some python comprehension fu to mash it into a single string.
        log      = SystemLogItem("log"         , system)
        language = HttpExposedString("language", system, get_function=lambda: self.__system_service.getLanguage())
        country  = HttpExposedString("country" , system,
            get_function=lambda: self.__system_service.getCountry(),
            put_function=lambda data: self.__system_service.setCountry(data)
        )

    ## Setup the Camera interface(s)
    #  @param object camera The camera object
    #
    def _setupCameraInterface(self, camera):
        HttpExposedString("feed", camera, get_function=lambda: "http://%s:8080/?action=stream" % urlparse(flask.request.url).hostname)
        #TODO: EM-530, extra camera API for grabbing frames and configuring settings of the camera.

    ## Setup the PrintJob interface
    #  @param object print_job The print job object
    #
    def _setupPrintJobInterface(self, print_job):
        print_job_name         = HttpExposedString("name",      print_job, get_function=lambda: self.__printer_service.getProcedureMetaData("PRINT").get("jobname",      ""))
        print_job_time_elapsed = HttpExposedInt("time_elapsed", print_job, get_function=lambda: self.__printer_service.getProcedureMetaData("PRINT").get("time_elapsed", 0))
        print_job_time_total   = HttpExposedInt("time_total",   print_job, get_function=lambda: self.__printer_service.getProcedureMetaData("PRINT").get("time_total",   0))
        print_job_progress     = HttpExposedFloat("progress",   print_job, get_function=lambda: self.__printer_service.getProcedureMetaData("PRINT").get("progress",     0))
        print_job_state        = PrintJobState("state",         print_job)

    ## Handle a 404 (file not found) error
    #  We use custom error handling for file not founds on the printer.
    #  This to generate captive portal functionality, which need a 302 redirect on a file not found.
    #  Every file-not-found error, that is NOT part of the API will redirect to the root
    #  location of the server. API 404's are handled normally.
    #  @param exception that is given for this error.
    def _handleFileNotFound(self, exception):
        # When the requested URL is part of the API, generate the standard json error.
        if flask.request.path.startswith("/%s" % (UM3Server.API_BASE_PATH)):
            return self._createJSONError(exception)
        # Else, redirect to the root folder.
        return flask.redirect("/", 302)

    ## Callback when the network service changes mode.
    #  When we switch to wifi setup, we want to disable authentication. Else authentication should be enabled.
    #  @param new_mode: New network mode from the network service.
    def _onNetworkModeChanged(self, new_mode):
        if new_mode == "WIFI SETUP":
            self.getAuthenticationController().disableAuthentication()
        else:
            self.getAuthenticationController().enableAuthentication()

    ## Helper function to set a target temperature.
    #  The printer service does not allow you to set the target temperature of the printer, you need to set the pre_tune_target_temperature.
    #  This function calculates the proper pre_tune_target_temperature to make get the resulting target temperature.
    #  @param property_owner: The dbus path of the printer service that needs to be accessed. Example "printer/head/0/slot/0" or "printer/bed"
    #  @param new_temperature: The new target temperature for the heatable object (bed/hotend). 0 is a special case and means the heater should be off and cool down.
    def __setTargetTemperature(self, property_owner, new_temperature):
        owner = dbusif.RemoteObject("printer", property_owner)
        # Do not apply the tuning offset on a target of 0, as 0 is the special case where we request things to be off.
        if new_temperature != 0.0:
            new_temperature -= owner.getProperty("tune_offset_temperature")
        return owner.setProperty("pre_tune_target_temperature", new_temperature)


class Coffee(HttpExposedObject):
    def __init__(self, parent):
        super().__init__("coffee", parent)

    def get(self):
        return flask.Response(flask.json.dumps({"Reply": "I'm a Little Teapot"}), status=418, mimetype="application/json")
