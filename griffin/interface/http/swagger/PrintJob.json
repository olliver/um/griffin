{
    "swagger": "2.0",
    "info": {
        "version": "2.0.0",
        "title": "Jedi API - Printjob",
        "description": "REST api for the Jedi 3D printer. Printjob part.",
        "license": {
            "name": "AGPL"
        }
    },
    "basePath": "/v1",
    "schemes": [
        "http"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "paths": {
        "/print_job": {
            "get": {
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Print job object",
                        "schema": {
                            "$ref": "#/definitions/PrintJob"
                        }
                    }
                }
            },
            "post": {
                "parameters": [
                    {
                        "name": "filename",
                        "in": "formData",
                        "description": "Name of print job",
                        "required": true,
                        "type": "string",
                    },
                    {
                        "name": "file",
                        "in": "formData",
                        "description": "File that needs to be printed (.gcode, .gcode.gz)",
                        "required": true,
                        "type": "file",
                    },
                    {
                        "name": "flow",
                        "in": "formData",
                        "description": "Flow of print job",
                        "required": false,
                        "type": "integer",
                        "default": 100
                    },
                    {
                        "name": "speed",
                        "in": "formData",
                        "description": "Speed of print job",
                        "required": false,
                        "type": "integer",
                        "default": 100
                    }
                ],
                "responses": {
                    "204": {
                        "description": "print job added"
                    }
                }
            }
        },
        "/print_job/name": {
            "get": {
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "Name of print job",
                        "schema": {
                            "type": "string"
                        }
                    }
                }
            }
        },
        "/print_job/source": {
            "get": {
                "description": "From what source was the print job started",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "type": "string",
                            "enum": [
                                "local",
                                "remote"
                            ]
                        }
                    }
                }
            }
        },
        "/print_job/time_elapsed": {
            "get": {
                "description": "Get the time elapsed (in seconds) since starting this print, including pauses etc.",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "type": "integer"
                        }
                    }
                }
            }
        },
        "/print_job/time_total": {
            "get": {
                "description": "Get the (estimated) total time in seconds for this print, excluding pauses etc.",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "type": "integer"
                        }
                    }
                }
            }
        },
        "/print_job/progress": {
            "get": {
                "description": "Get the (estimated) progress for the current print job, a value between 0 and 1",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "type": "number"
                        }
                    }
                }
            }
        },
        "/print_job/flow": {
            "get": {
                "description": "Get the flow % of the current print job",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "type": "integer"
                        }
                    }
                }
            },
            "put": {
                "parameters": [
                    {
                        "name": "flow",
                        "in": "body",
                        "description": "Target flow",
                        "required": true,
                        "schema": {
                            "type": "integer"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "Flow set"
                    }
                }
            }
        },
        "/print_job/speed": {
            "get": {
                "description": "Get the speed % of the current print job",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "type": "integer"
                        }
                    }
                }
            },
            "put": {
                "parameters": [
                    {
                        "name": "speed",
                        "in": "body",
                        "description": "Target speed",
                        "required": true,
                        "schema": {
                            "type": "integer"
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "Speed set"
                    }
                }
            }
        },
        "/print_job/state": {
            "get": {
                "description": "Get the print job state",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "",
                        "schema": {
                            "type": "string",
                            "enum": [
                                "none",
                                "printing",
                                "pausing",
                                "paused",
                                "resuming",
                                "pre_print",
                                "post_print",
                                "wait_cleanup"
                            ]
                        }
                    }
                }
            },
            "put": {
                "parameters": [
                    {
                        "name": "target",
                        "in": "body",
                        "description": "Change the current state of the print. Note that only changes to abort / pause are always allowed and changing to print only when state is paused.",
                        "required": true,
                        "schema": {
                            "type": "string",
                            "enum": [
                                "print",
                                "pause",
                                "abort"
                            ]
                        }
                    }
                ],
                "responses": {
                    "204": {
                        "description": "State changed"
                    }
                }
            }
        },
    },
    "definitions": {
        "PrintJob": {
            "type": "object",
            "description": "An (active) print job.",
            "properties": {
                "time_elapsed": {
                    "type": "integer"
                },
                "time_remaining": {
                    "type": "integer"
                },
                "source": {
                    "type": "string",
                    "enum": [
                        "local",
                        "remote"
                    ]
                },
                "name": {
                    "type": "string"
                },
                "flow": {
                    "type": "integer",
                    "default": 100
                },
                "speed": {
                    "type": "integer",
                    "default": 100
                },
                "state": {
                    "type": "string",
                    "enum": [
                        "none",
                        "printing",
                        "paused",
                        "pre_print",
                        "post_print",
                        "wait_cleanup"
                    ]
                }
            }
        }
    }
}




