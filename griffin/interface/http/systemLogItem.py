from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.flaskResponses import FlaskResponses

import flask
import subprocess

import logging
log = logging.getLogger(__name__.split(".")[-1])


# SystemLog is a special case of an HttpExposedObject.
class SystemLogItem(HttpExposedObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def register(self):
        super().register()

    def get(self):
        # Allow a boot parameter to get logs from previous boot sessions, default is 0 which is the current boot. -1 is the previous boot.
        boot = flask.request.args.get("boot", 0, type=int)
        # Allow a lines parameter to specify the number of lines to get from the log.
        line_count = flask.request.args.get("lines", 50, type=int)

        log_lines = self.__runJournalctlProcess(["-b", str(boot), "-n", str(line_count)])
        return FlaskResponses.createSuccess(json=log_lines)

    def __runJournalctlProcess(self, params):
        command = ["/bin/journalctl", "--no-pager"] + params
        p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()
        return out.decode("utf-8", "replace").strip().split("\n")[1:]
