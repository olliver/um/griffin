from griffin import dbusif


## Object which defines the DBus interface for the http authentication.
#  The http server needs to expose a few functions to dbus to interface with the rest of the system,
#  as authentication requests needs to be confirmed by the end user by physical access.
class AuthenticationService(dbusif.ServiceObject):
    def __init__(self, authentication_api):
        super().__init__("interface.http", "interface/http/auth")
        self.__authentication_api = authentication_api
        self.__authentication_api.onNewAuthorizationRequest.connect(self.onNewAuthorizationRequest)
        self.__system_service = dbusif.RemoteObject("system")

        ## Add callback to dbus network service which implements the call to the resetToFactorySettings
        self.__system_service.addFactoryResetCallback("auth_api", "resetSettings")

    ## Call from the DBus service when the user decides not to allow the application access to the printer.
    #  @param auth_id: The authorization id that was given from the onNewAuthorizationRequest callback.
    @dbusif.method("s", "")
    def denyAuthorizationRequest(self, auth_id):
        self.__authentication_api.denyAuthorizationRequest(auth_id)

    ## Call from the DBus service when the user decides to allow the application access to the printer.
    #  @param auth_id: The authorization id that was given from the onNewAuthorizationRequest callback.
    @dbusif.method("s", "")
    def allowAuthorizationRequest(self, auth_id):
        self.__authentication_api.allowAuthorizationRequest(auth_id)

    ## Signal which is emitted when a new API user is trying to register.
    #  3 parameters:
    #  @param string: application: name of the application that wants to register.
    #  @param string: user: name of the end user that wants to register.
    #  @param string: auth_id: internal id that is used to track this authentication.
    @dbusif.signal("sss")
    def onNewAuthorizationRequest(self, application, user, auth_id):
        pass

    ## @brief The callback function to register which needs to be called when a factory reset is issued
    #  @param reset_type One of the defined values from FactoryReset which would indicate a hard or soft reset
    @dbusif.method("s", "")
    def resetSettings(self, reset_type):
        self.__authentication_api.resetToFactorySettings(reset_type)

