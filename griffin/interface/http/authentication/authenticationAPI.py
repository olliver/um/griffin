import flask
import os
import binascii

from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.flaskResponses import FlaskResponses
from griffin import signal


## Class which defines the HTTP API for authentication
#  This class handles the calls that are related to getting a new authentication id/key combination as well as checking if this new combo is accepted by the end user.
#  It also has a call to allow for a quick check if authentication is working without making a POST/PUT/DELETE request.
class AuthenticationAPI(HttpExposedObject):
    # Construct a new Authentication API. This requires the same parameters as the HttpExposedObject
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__generated_id = None
        self.__generated_key = None
        self.__authorized_by_user = False
        self.__user_data = {}

        ## Signal which is emitted when a new API user is trying to register.
        #  3 parameters:
        #  @param string: application: name of the application that wants to register.
        #  @param string: user: name of the end user that wants to register.
        #  @param string: auth_id: internal id that is used to track this authentication.
        self.onNewAuthorizationRequest = signal.Signal()

    ## Register this API on the server object.
    #  This does intentionally not call super().register, as we do not want the recursive functionality of the HttpExposedObject.
    #  It adds the calls that we expose to the API user.
    def register(self):
        self._server.add_url_rule(self.getAbsolutePath() + "/request", view_func=self._requestNewAuth, methods=["POST"], authenticated_methods=[])
        self._server.add_url_rule(self.getAbsolutePath() + "/check/<auth_id>", view_func=self._checkId, methods=["GET"], authenticated_methods=[])
        self._server.add_url_rule(self.getAbsolutePath() + "/verify", view_func=self._verifyAuth, methods=["GET"], authenticated_methods=["GET"])

    ## Call from the DBus service when the user decides not to allow the application access to the printer.
    #  @param auth_id: The authorization id that was given from the onNewAuthorizationRequest callback.
    def denyAuthorizationRequest(self, auth_id):
        if self.__generated_id != auth_id:
            return
        self.__generated_id = None

    ## Call from the DBus service when the user decides to allow the application access to the printer.
    #  @param auth_id: The authorization id that was given from the onNewAuthorizationRequest callback.
    def allowAuthorizationRequest(self, auth_id):
        if self.__generated_id != auth_id:
            return
        if self._server.getAuthenticationController().addUser(self.__generated_id, self.__generated_key, self.__user_data):
            self.__authorized_by_user = True

    ## API call to request a new authentication token.
    #  generates a new id/key combination and reports this back as json.
    #  Also emits the onNewAuthorizationRequest signal to let the system know a new authentication request is happening.
    #  The system should respond with a "allowAuthorizationRequest" or "denyAuthorizationRequest" to allow or deny this new API user.
    #  Until this is done, the new id/key combination is not yet valid.
    def _requestNewAuth(self):
        json_data = flask.request.json
        application = flask.request.form.get("application")
        user = flask.request.form.get("user")
        if json_data is not None:
            application = json_data.get("application")
            user = json_data.get("user")
        if not application or not user:
            return FlaskResponses.createFailed("application or user not supplied.")

        self.__authorized_by_user = False
        self.__generated_id = binascii.hexlify(os.urandom(16)).decode('ascii')
        self.__generated_key = binascii.hexlify(os.urandom(32)).decode('ascii')
        self.__user_data = {
            "application": application,
            "user": user
        }

        self.onNewAuthorizationRequest.emit(application, user, self.__generated_id)

        return FlaskResponses.createSuccess(json={"id": self.__generated_id, "key": self.__generated_key})

    ## API call to check if a newly requested auth_id is already authorized or not.
    #  This should be called after requesting a new auth id/key combo to check if the end user is allowing this application access or not.
    #  @param: auth_id: string containing the id gained from the auth/request API.
    def _checkId(self, auth_id):
        # If we are not checking the last generated id, then report failure.
        if auth_id != self.__generated_id:
            return FlaskResponses.createSuccess("unauthorized")
        if self.__authorized_by_user:
            return FlaskResponses.createSuccess("authorized")
        return FlaskResponses.createSuccess("unknown")

    ## API call to verify authentication.
    #  As authentication is already handled before this function is called, we only need to return our default "ok" response here.
    def _verifyAuth(self):
        return FlaskResponses.createSuccess("ok")

    ## Call from the DBus service when there is an incoming factory reset
    #  @param reset_type One of the defined values from FactoryReset which would indicate a hard or soft reset
    def resetToFactorySettings(self, reset_type):
        self._server.getAuthenticationController().resetToFactorySettings(reset_type)
