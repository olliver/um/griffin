

## Base class for Xmlsoap actions.
#  Xmlsoap actions are called on an XmlsoapEndpoint, and part of the request is the action "uri".
#  The XmlsoapEndpoint will search for the proper action object and call the handle function on it.
#  The XmlsoapEndpoint is responsible for constructing the common part of the XML reply.
class XmlSoapAction:
    # Create a new XmlsoapAction object.
    # @param action_uri: string, uri of the action that we are responding on.
    # @param response_action_uri: string, uri of the reply action. (yes, replies are also actions)
    def __init__(self, action_uri, response_action_uri):
        self.__action_uri = action_uri
        self.__response_action_uri = response_action_uri

    # Get the namespaces that this action is using. This is used by the XmlsoapEndpoint to keep track of all the namespaces that are used in the XML reply.
    # Override this function in your subclass when you are using more namespaces.
    # @return dictionary: containing the namespace "prefix" as key, and the uri as value.
    def getNamespaces(self):
        return {}

    # Get the action_uri for this XmlSoapAction
    # @return string: the action_uri that was used to construct this object.
    def getActionUri(self):
        return self.__action_uri

    # Get the response_action_uri for this XmlSoapAction
    # @return string: the response_action_uri that was used to construct this object.
    def getResponseActionUri(self):
        return self.__response_action_uri

    # @pure Handle the Xmlsoap request. This function needs to be implemented in a subclass.
    # And should fill the response_document with any extra data that it requires.
    # The XmlsoapEndpoint has already created a default response containing the normal Xmlsoap header.
    # @param request_body: a umxml.Node tree containing the XML request that is being handled. Can be used to retrieve extra data.
    # @param response_document: a umxml.Document tree containing the XML response to the request. This is document already contains a filled header and an empty body.
    def handle(self, request_body, response_document):
        raise NotImplementedError
