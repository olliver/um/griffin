from griffin.interface.http.exposedItems.httpExposedItem import HttpExposedItem
from ..flaskResponses import FlaskResponses
import flask
import uuid
from griffin import umxml
from .xmlSoapAction import XmlSoapAction

import logging
log = logging.getLogger(__name__.split(".")[-1])


## Base class for Xmlsoap endpoints.
#  Xmlsoap is based on action, so you need to register XmlsoapAction objects in this class to handle those actions.
class XmlSoapEndpoint(HttpExposedItem):
    # Create a new XmlsoapEndpoint
    # @param path: string, path part of the URL where this endpoint resides.
    def __init__(self, path):
        super().__init__(local_path=path, allowed_request_methods=["POST"], authenticated_methods=[])
        self.__actions = {}
        self.__namespaces = {
            "soap": "http://www.w3.org/2003/05/soap-envelope",
            "wsa": "http://schemas.xmlsoap.org/ws/2004/08/addressing",
        }

    # Add a new action to this endpoint. The action will get a handle() call when a request comes in with the action specified by
    # action.getActionUri().
    # @param action: XmlsoapAction object which will handle a request.
    def addAction(self, action):
        assert isinstance(action, XmlSoapAction), "addAction requires a XmlsoapAction object."

        self.__actions[action.getActionUri()] = action
        extra_namespaces = action.getNamespaces()
        self.__namespaces.update(extra_namespaces)

    # Overriden post() handler from base class.
    # This will parse the incoming xml data, and check if it is a Xmlsoap request.
    # On errors a 400 error (BAD REQUEST) will be send back to the caller, to indicate that we do not know how to handle it's request.
    # Else the proper XmlsoapAction handler will be called to handle the request.
    # @param: received_data, flask receive object containing the information about the request.
    # @return: flask.Reponse object containing the response from the XmlsoapAction handler.
    def post(self, received_data):
        try:
            request_document = umxml.Document(text=received_data.data, namespaces=self.__namespaces)
        except ValueError:
            return FlaskResponses.createFailed()

        # Extract the action and message id from the request.
        action_node = request_document.find("./soap:Header/wsa:Action")
        message_id_node = request_document.find("./soap:Header/wsa:MessageID")
        body_node = request_document.find("./soap:Body")
        if action_node is None or message_id_node is None or body_node is None:
            # If we don't have an action or a message id, then this is not a valid request.
            return FlaskResponses.createFailed()

        action = self.__actions.get(action_node.text, None)
        if action is None:
            log.warning("Unknown Xmlsoap request: [%s] on endpoint [%s]", action_node.text, self.getLocalPath())
            return FlaskResponses.createFailed()

        # Create the common part of the Xmlsoap reply. This part is common for any request.
        response_document = umxml.Document(tag="Envelope", ns="soap", namespaces=self.__namespaces)
        response_document.createNodes([
            umxml.NodeData("soap:Header", children=[
                umxml.NodeData("wsa:To", text="http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous"),
                umxml.NodeData("wsa:Action", text=action.getResponseActionUri()),
                umxml.NodeData("wsa:MessageID", text="urn:uuid:" + str(uuid.uuid4())),
                umxml.NodeData("wsa:RelatesTo", text=message_id_node.text),
            ]),
            umxml.NodeData("soap:Body")
        ])

        action.handle(body_node, response_document)

        response_text = response_document.toString()
        return flask.Response(response_text, status=200, mimetype="application/soap+xml")
