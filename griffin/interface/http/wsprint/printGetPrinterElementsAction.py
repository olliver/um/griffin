from .xmlSoapAction import XmlSoapAction
from griffin import umxml
from griffin import dbusif


# The print/GetPrinterElements action is called from windows when it tries to add the device as a printer.
# It uses this request to get the final name to show in Windows, as well as to discover which driver to use.
class PrintGetPrinterElementsAction(XmlSoapAction):
    def __init__(self, uuid):
        super().__init__("http://schemas.microsoft.com/windows/2006/08/wdp/print/GetPrinterElements", "http://schemas.microsoft.com/windows/2006/08/wdp/print/GetPrinterElementsResponse")
        self.__uuid = uuid

        # The device_id_string tells Windows which driver to use, by magic this turns into WSDPRINT\MS3DCompatE2D2
        # "WSDPRINT" is always prefixed. "MS3D" comes from the "MFG" string, "Compat" from the "MDL". The "E2D2" is currently unknown.
        # We need to customize this later to link to our own driver.
        self.__device_id_string = "MFG:MS3D; CMD:XPS; MDL:Compat; CLS:Printer; DES:Compat; CID:MS3DWSD"

    # Override from baseclass, tell which extra namespaces this action is using.
    # @return: dictionary containing the extra namespaces.
    def getNamespaces(self):
        return {
            "wse": "http://schemas.xmlsoap.org/ws/2004/08/eventing",
            "wprt": "http://schemas.microsoft.com/windows/2006/08/wdp/print",
            "WPRNv20": "http://schemas.microsoft.com/windows/2014/04/wdp/printV20",
        }

    # Handle the GetPrinterElements action.
    # @param request_body: a umxml.Node tree containing the XML request that is being handled. Can be used to retrieve extra data.
    # @param response_document: a umxml.Document tree containing the XML response to the request. This is document already contains a filled header and an empty body.
    def handle(self, request_body, response_document):
        response_body = response_document.find("./soap:Body")
        printer_name = dbusif.RemoteObject("printer").getProperty("machine_type_name") + " - " + dbusif.RemoteObject("system").getMachineName()

        response_body.createNodes([
            umxml.NodeData("wprt:GetPrinterElementsResponse", children=[
                umxml.NodeData("wprt:PrinterElements", children=[
                    umxml.NodeData("wprt:ElementData", attrib={"Valid": "True", "Name": "wprt:PrinterDescription"}, children=[
                        umxml.NodeData("wprt:PrinterDescription", children=[
                            umxml.NodeData("wprt:DeviceId", text=self.__device_id_string),
                            umxml.NodeData("wprt:PrinterName", text=printer_name), # Name displayed after driver is added.
                            umxml.NodeData("wprt:PrinterInfo", text="Ultimaker 3 (E)"), # Not seen anywhere yet...
                            umxml.NodeData("wprt:ColorSupported", text="false"),    # Required by standard, but not used.
                            umxml.NodeData("wprt:MultipleDocumentJobsSupported", text="false"),    # Required by standard, but not used.
                            umxml.NodeData("wprt:PagesPerMinute", text="40"),    # Required by standard, but not used.
                            umxml.NodeData("wprt:PrinterLocation"),
                            umxml.NodeData("WPRNv20:SupportsWSPrintV20", text="true"),
                        ])
                    ])
                ])
            ])
        ])
