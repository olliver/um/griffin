from .xmlSoapAction import XmlSoapAction
from urllib.parse import urlparse
import flask
from griffin import umxml
from griffin import dbusif


# The transfer/Get action is called after Windows probed for the endpoint (see probeAction)
# This is used to get the actual name of the device.
class TransferGetAction(XmlSoapAction):
    # @param uuid: string, Needs to be in uuid4 format. Exact details of this uuid are unknown, as Microsoft does anwser the question on how to fill this properly.
    # @param print_endpoint_name: string, path towards the printing http endpoint.
    def __init__(self, uuid, print_endpoint_name):
        super().__init__("http://schemas.xmlsoap.org/ws/2004/09/transfer/Get", "http://schemas.xmlsoap.org/ws/2004/09/transfer/GetResponse")
        self.__uuid = uuid
        self.__print_endpoint_name = print_endpoint_name

    # Override from baseclass, tell which extra namespaces this action is using.
    # @return: dictionary containing the extra namespaces.
    def getNamespaces(self):
        return {
            "wsdp": "http://schemas.xmlsoap.org/ws/2006/02/devprof",
            "wsx": "http://schemas.xmlsoap.org/ws/2004/09/mex",
            "wprt": "http://schemas.microsoft.com/windows/2006/08/wdp/print",
            "pnpx": "http://schemas.microsoft.com/windows/pnpx/2005/10"
        }

    # Handle the TransferGetAction request, reply what type of device we are and the details of this device.
    # Windows calls this during discovery to get the actual name of the device.
    # @param request_body: a umxml.Node tree containing the XML request that is being handled. Can be used to retrieve extra data.
    # @param response_document: a umxml.Document tree containing the XML response to the request. This is document already contains a filled header and an empty body.
    def handle(self, request_body, response_document):
        response_body = response_document.find("./soap:Body")
        endpoint_url = "http://%s:80/%s" % (urlparse(flask.request.url).hostname, self.__print_endpoint_name)

        printer_name = dbusif.RemoteObject("printer").getProperty("machine_type_name") + " - " + dbusif.RemoteObject("system").getMachineName()

        metadata = response_body.createNode("Metadata", ns="wsx")
        metadata.createNodes([
            umxml.NodeData("wsx:MetadataSection", attrib={"Dialect": "http://schemas.xmlsoap.org/ws/2006/02/devprof/ThisModel"}, children=[
                umxml.NodeData("wsdp:ThisModel", children=[
                   umxml.NodeData("wsdp:Manufacturer", text="Ultimaker"),  # Shown in the device manager, as "Manufacturer"
                   umxml.NodeData("wsdp:ModelName", text=printer_name),  # Shown in the device manager, at the driver properties as "Bus reported device description" and as "Model"
                   umxml.NodeData("wsdp:ModelUrl", text="http://ultimaker.com/"),
                   umxml.NodeData("wsdp:PresentationUrl", text="http://ultimaker.com/"),
                   umxml.NodeData("pnpx:DeviceCategory", text="Printers"),
                ]),
            ])
        ])
        metadata.createNodes([
            umxml.NodeData("wsx:MetadataSection", attrib={"Dialect": "http://schemas.xmlsoap.org/ws/2006/02/devprof/ThisDevice"}, children=[
                umxml.NodeData("wsdp:ThisDevice", children=[
                   umxml.NodeData("wsdp:FriendlyName", text=printer_name),  # Name that is displayed in windows before driver is added. And as display name and friendly name in the driver properties.
                ]),
            ])
        ])

        metadata.createNodes([
            umxml.NodeData("wsx:MetadataSection", attrib={"Dialect": "http://schemas.xmlsoap.org/ws/2006/02/devprof/Relationship"}, children=[
                umxml.NodeData("wsdp:Relationship", attrib={"Type": "http://schemas.xmlsoap.org/ws/2006/02/devprof/host"}, children=[
                    umxml.NodeData("wsdp:Hosted", children=[
                        umxml.NodeData("wsa:EndpointReference", children=[
                            umxml.NodeData("wsa:Address", text=endpoint_url)
                        ]),
                        umxml.NodeData("wsdp:Types", text="wprt:PrinterServiceType"),
                        umxml.NodeData("wsdp:ServiceId", text=endpoint_url),
                        umxml.NodeData("pnpx:CompatibleId", attrib={"xmlns:pnpx": "http://schemas.microsoft.com/windows/pnpx/2005/10"}, text="http://schemas.microsoft.com/windows/2006/08/wdp/print/PrinterServiceType"),
                    ]),
                    umxml.NodeData("wsdp:Host", children=[
                        umxml.NodeData("wsa:EndpointReference", children=[
                            umxml.NodeData("wsa:Address", text="urn:uuid:" + self.__uuid)
                        ]),
                        umxml.NodeData("wsdp:Types", text="wprt:PrinterServiceType"),
                        umxml.NodeData("wsdp:ServiceId", text=endpoint_url),
                    ])
                ]),
            ])
        ])

        response_document.forceNamespaceInclusion("wprt")
