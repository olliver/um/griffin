from .xmlSoapAction import XmlSoapAction
from urllib.parse import urlparse
from griffin import umxml
import flask
import uuid


## XmlSoap Probe action as part of WSPrint2.0
#  This is a standardized way of finding a printer. This is the first request that gets made after
#  The printer is discovered with zeroconf (griffin.interface.avahi) as "_printer._tcp" service.
#  This action is only used to discover which endpoint should be talked to when talking to this device.
class ProbeAction(XmlSoapAction):
    # Create the ProbeAction object
    # @param uuid: string, Needs to be in uuid4 format. Exact details of this uuid are unknown, as Microsoft does not answer the question on how to fill this properly.
    # @param print_endpoint_name: string, path towards the printing http endpoint.
    def __init__(self, uuid, print_endpoint_name):
        super().__init__("http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe", "http://schemas.xmlsoap.org/ws/2005/04/discovery/ProbeMatches")
        self.__uuid = uuid
        self.__print_endpoint_name = print_endpoint_name
        self.__message_number = 0

    # Override from baseclass, tell which extra namespaces this action is using.
    # @return: dictionary containing the extra namespaces.
    def getNamespaces(self):
        return {
            "wsdisco": "http://schemas.xmlsoap.org/ws/2005/04/discovery",
            "wsd": "http://schemas.xmlsoap.org/ws/2006/02/devprof",
            "wprt": "http://schemas.microsoft.com/windows/2006/08/wdp/print",
        }

    # Handle the probe request. Tell the requester where to find our endpoint, and that we are a PrintDevice.
    # @param request_body: a umxml.Node tree containing the XML request that is being handled. Can be used to retrieve extra data.
    # @param response_document: a umxml.Document tree containing the XML response to the request. This is document already contains a filled header and an empty body.
    def handle(self, request_body, response_document):
        response_body = response_document.find("./soap:Body")
        response_header = response_document.find("./soap:Header")

        response_body.createNodes([
            umxml.NodeData("wsdisco:ProbeMatches", children=[
                umxml.NodeData("wsdisco:ProbeMatch", children=[
                    umxml.NodeData("wsa:EndpointReference", children=[
                        umxml.NodeData("wsa:Address", text="urn:uuid:" + self.__uuid)
                    ]),
                    umxml.NodeData("wsdisco:Types", text="wsd:Device wprt:PrintDeviceType"),
                    umxml.NodeData("wsdisco:XAddrs", text="http://%s:80/%s" % (urlparse(flask.request.url).hostname, self.__print_endpoint_name)),
                    umxml.NodeData("wsdisco:MetadataVersion", text="1"),
                ]),
            ]),
        ])

        # AppSequence, see: http://docs.oasis-open.org/ws-dd/discovery/1.1/os/wsdd-discovery-1.1-spec-os.html#_Toc234231847
        # Specs say that InstanceId should increase each boot, but we currently do not track this, Windows does not seem to care.
        self.__message_number += 1
        response_header.createNodes([
            umxml.NodeData("wsdisco:AppSequence", attrib={"InstanceId": "1", "SequenceId": str(uuid.uuid4()), "MessageNumber": str(self.__message_number)})
        ])

        # Due to the text string in the Types node, we need to force these namespaces.
        # As apparently, somewhere, someone found it a good idea to use namespaces in text nodes. Which isn't in any standard. (and thus no library will handle this)
        response_document.forceNamespaceInclusion("wsd")
        response_document.forceNamespaceInclusion("wprt")
