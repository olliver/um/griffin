from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.flaskResponses import FlaskResponses

import flask
from griffin import dbusif

from urllib import parse

import logging
log = logging.getLogger(__name__.split(".")[-1])


# Materials is a special case of an HttpExposedObject.
class Materials(HttpExposedObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, allowed_request_methods=["GET", "POST"], **kwargs)
        self.__material_service = dbusif.RemoteObject("material")

    def register(self):
        super().register()
        self._server.add_url_rule(self.getAbsolutePath() + "/<guid>", view_func=self.getMaterial, methods=["GET"])
        self._server.add_url_rule(self.getAbsolutePath() + "/<guid>", view_func=self.updateMaterial, methods=["PUT"])
        self._server.add_url_rule(self.getAbsolutePath() + "/<guid>", view_func=self.removeMaterial, methods=["DELETE"])

    def getMaterial(self, guid):
        guid = parse.unquote(guid)
        material_xml = self.__material_service.getMaterialXMLContents(guid)
        if material_xml == "":
            return FlaskResponses.createNotFound("GUID not found")
        return FlaskResponses.createSuccess(json=material_xml)

    def updateMaterial(self, guid):
        guid = parse.unquote(guid)
        return self.__handleMaterialProfileUpload(flask.request.files, guid=guid)

    ## Remove a material from the material database.
    def removeMaterial(self, guid):
        guid = parse.unquote(guid)
        if not self.__material_service.removeMaterial(guid):
            return FlaskResponses.createSuccess(json={"message": "Remove failed", "result": False})
        return FlaskResponses.createSuccess(json={"message": "Remove successful", "result": True})

    ## Get all the materials in the material database.
    def get(self):
        materials_list = []
        # The {"":""} is a workaround for the problem that dbus-python does not like empty dictionaries as parameters.
        for material in self.__material_service.getMaterials({"":""}):
            xml = self.__material_service.getMaterialXMLContents(material["guid"])
            if xml != "":
                materials_list.append(xml)
        return FlaskResponses.createSuccess(json=materials_list)

    ## Post a new material profile in the database.
    def post(self, received_data):
        return self.__handleMaterialProfileUpload(received_data.files)

    ## Private function to handle the upload of a new/updated material profile.
    #  As this logic is duplicated for both the POST on the /materials URL and the PUT on /materials/<guid>
    def __handleMaterialProfileUpload(self, files, guid=None):
        received_file = files.get("file")
        if not received_file:
            return FlaskResponses.createFailed("No file received")
        material_profile_contents = received_file.read()

        received_signature = files.get("signature_file")
        if received_signature:
            material_profile_signature = received_signature.read()
            if guid is not None:
                result = self.__material_service.updateSignedMaterial(guid, material_profile_contents, material_profile_signature)
            else:
                result = self.__material_service.addNewSignedMaterial(material_profile_contents, material_profile_signature)
        else:
            if guid is not None:
                result = self.__material_service.updateMaterial(guid, material_profile_contents)
            else:
                result = self.__material_service.addNewMaterial(material_profile_contents)

        if not result:
            return FlaskResponses.createSuccess(json={"message": "profile not stored", "result": False})
        return FlaskResponses.createSuccess(json={"message": "Material profile stored", "result": True})
