from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.flaskResponses import FlaskResponses
from griffin import dbusif
from threading import Event

import flask
import io
import numpy

from urllib import parse

import logging
log = logging.getLogger(__name__.split(".")[-1])


# Diagnostics is a special case of an HttpExposedObject.
class Diagnostics(HttpExposedObject):
    ## The default times a measurement is taken
    __DEFAULT_LOOP_COUNT = 100
    ## The default samples during one measurement
    __DEFAULT_SAMPLE_COUNT = 50
    ## The procedure that implements calculating the measurements
    __PROCEDURE_GET_CAPACITIVE_SENSOR_NOISE = "GET_CAPACITIVE_SENSOR_NOISE"
    ## 10000s samples (sample_count * loop_count) would approximately take 200s (factor 0.02), adding some extra slack time to wait for completion
    __TIME_OUT_FACTOR = 0.03
    ## Add small time factor to take into account communication issues (and sleep step)
    __TIME_OUT_ADD_OTHER_FACTORS = 3.5

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._allowed_request_methods = ["GET"]    # This object will only ever respond to a get
        self.__sensor_done = Event()
        self.__sensor_done.clear()
        self.__printer_service = dbusif.RemoteObject("printer")
        self.__printer_service.connectSignal("onProcedureFinished", self.__onProcedureFinished)

    def register(self):
        super().register()
        self._server.add_url_rule(self.getAbsolutePath() + "/cap_sensor_noise", view_func = self.__probe_sensor_noise_defaults)
        self._server.add_url_rule(self.getAbsolutePath() + "/cap_sensor_noise/<int:loop_count>", view_func = self.__probe_sensor_noise_with_loop_count)
        self._server.add_url_rule(self.getAbsolutePath() + "/cap_sensor_noise/<int:loop_count>/<int:sample_count>", view_func = self.__probe_sensor_noise)
        self._server.add_url_rule(self.getAbsolutePath() + "/temperature_flow/<int:row_count>", view_func = self.__temperature_flow)

    ## @brief Implements /cap_sensor_noise (using default values)
    def __probe_sensor_noise_defaults(self):
        return self.__probe_sensor_noise(self.__DEFAULT_LOOP_COUNT, self.__DEFAULT_SAMPLE_COUNT)

    ## @brief Implements /cap_sensor_noise/<loop_count>
    #  @param loop_count The number of measurements to take
    def __probe_sensor_noise_with_loop_count(self, loop_count):
        return self.__probe_sensor_noise(loop_count, self.__DEFAULT_SAMPLE_COUNT)

    ## @brief Implements /cap_sensor_noise/<loop_count>/<sample_count>
    #  @param loop_count The number of measurements to take
    #  @param sample_count The number of samples to take per measurement
    def __probe_sensor_noise(self, loop_count, sample_count):
        active_procedures = self.__printer_service.getActiveProcedures()
        log.info("Active procedures= %r", active_procedures)

        if active_procedures:
            return FlaskResponses.createFailed("Printer is busy, diagnostics cannot run")

        log.info("Doing %d sensor noise probes of %d samples each...", loop_count, sample_count)
        self.__sensor_done.clear()
        self.__printer_service.startProcedure(self.__PROCEDURE_GET_CAPACITIVE_SENSOR_NOISE, {"number_of_samples": loop_count, "samples_count": sample_count})
        success = self.__sensor_done.wait(timeout=self.__TIME_OUT_FACTOR * sample_count * loop_count + self.__TIME_OUT_ADD_OTHER_FACTORS)
        ## Only get the result if there was no timeout
        if success:
            data = self.__printer_service.getProcedureMetaData(self.__PROCEDURE_GET_CAPACITIVE_SENSOR_NOISE)
            return FlaskResponses.createSuccess("", data)
        return FlaskResponses.createFailed("Time-out occurred")

    def __temperature_flow(self, row_count):
        header, data = self.__printer_service.getDataLoggerData("TemperatureFlow", row_count)
        if flask.request.args.get("csv", 0, type=int):
            output = io.BytesIO()
            numpy.savetxt(output, numpy.array(data), fmt="%g", delimiter=';', header=';'.join(header))
            result = flask.Response(output.getvalue(), status=FlaskResponses.HTTP_OK, mimetype="text/csv")
            result.headers["content-disposition"] = "attachment; filename=\"TemperatureFlow.csv\""
            return result
        return FlaskResponses.createSuccess("", [header] + data)

    ## @brief Signal that the procedure has finished and that the result can be retrieved
    #  @param procedure The name of the procedure that has finished
    def __onProcedureFinished(self, procedure):
        if procedure == self.__PROCEDURE_GET_CAPACITIVE_SENSOR_NOISE:
            self.__sensor_done.set()
