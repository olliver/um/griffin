from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.flaskResponses import FlaskResponses

from griffin import dbusif

import logging
log = logging.getLogger(__name__.split(".")[-1])


# Beep is a special case of an HttpExposedObject.
class Beep(HttpExposedObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, allowed_request_methods=["POST"], **kwargs)

    def post(self, received_data):
        if "frequency" in received_data.json and "duration" in received_data.json:
            dbusif.RemoteObject("beep").beep(int(received_data.json["frequency"]), int(received_data.json["duration"]))
            return FlaskResponses.createNoContent("Beeping")
        return FlaskResponses.createFailed("Didn't get either frequency or duration")
