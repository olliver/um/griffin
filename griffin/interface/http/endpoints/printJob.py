from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.flaskResponses import FlaskResponses

import flask
import werkzeug
from griffin import dbusif
import os

import logging
log = logging.getLogger(__name__.split(".")[-1])


# PrintJob is a special case of an HttpExposedObject.
class PrintJob(HttpExposedObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__printer_service = dbusif.RemoteObject("printer")

    # This is a hack due to the inability of flask to delete routes.
    # We fake the correct behaviour by setting the deleted state to True if there is no active step in the print sequence.
    def isDeleted(self):
        if self.__printer_service.getProperty("state") != "printing":
            return True
        return False

    def post(self, received_data):
        # If the state is not idle, we cannot start a print job, because a print job is already running, the printer is not ready yet, or maintenance actions are done.
        if self.__printer_service.getProperty("state") != "idle":
            return FlaskResponses.createMethodNotAllowed()
        else:
            received_file = received_data.files.get("file")
            if not received_file:
                return FlaskResponses.createFailed("No file received")
            file_extension = received_file.filename.rsplit(".", 1)[1]
            log.info(file_extension)
            log.info("saving file")
            file_path = os.path.join("/tmp/", werkzeug.secure_filename(received_file.filename))
            try:
                received_file.save(file_path)
                log.info("File saved as %s", file_path)
                scheme = "upload://"
                if not dbusif.RemoteObject("printer").startProcedure("PRINT", {"jobname": received_file.filename, "url": (scheme + file_path), "origin": "WEB_API"}):
                    os.remove(file_path)
                    return FlaskResponses.createFailed("Print job not started")
                # File not removed, as the printer service is using the file. The printer service will delete the file when it is done with it.
                return FlaskResponses.createNoContent("Print job started")
            except:
                return FlaskResponses.createFailed("Unable to write file %s", file_path)

    # Another hack due to the inability of flask to delete routes.
    # If there is no print job, the POST method -is- allowed.
    def handleHttpRequest(self):
        if flask.request.method == "POST" and self.isDeleted():
            return self.post(received_data = flask.request)
        return super().handleHttpRequest()
