from griffin.interface.http.exposedItems.httpExposedItem import HttpExposedItem
from griffin.interface.http.flaskResponses import FlaskResponses

from griffin import dbusif

import logging
log = logging.getLogger(__name__.split(".")[-1])


class PrintJobState(HttpExposedItem):
    def __init__(self, *args, allowed_request_methods = ["GET", "PUT"],**kwargs):
        super().__init__(*args, allowed_request_methods = allowed_request_methods, **kwargs)
        self.__printer_service = dbusif.RemoteObject("printer")

    def get(self):
        state_str = self.__printer_service.getProperty("job_state")
        return FlaskResponses.createSuccess(None, state_str)

    def put(self, received_data):
        meta_data = self.__printer_service.getProcedureMetaData("PRINT")
        active_step = str(meta_data["active_step"])
        paused = bool(self.__printer_service.getProperty("job_state") == "paused")
        new_state = received_data.get("target", "")
        if new_state == "pause" and not paused and active_step == "PRINTING":
            self.__printer_service.startProcedure("PAUSE_PRINT", {"data": ""})
        elif new_state == "print" and paused:
            self.__printer_service.startProcedure("RESUME_PRINT", {"data": ""})
        elif new_state == "abort":
            self.__printer_service.messageProcedure("PRINT", "ABORT")
        return FlaskResponses.createNoContent("State set")
