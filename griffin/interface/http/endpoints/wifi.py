from griffin.interface.http.exposedItems.httpExposedObject import HttpExposedObject
from griffin.interface.http.flaskResponses import FlaskResponses

import flask
from griffin import dbusif

from urllib import parse

import logging
log = logging.getLogger(__name__.split(".")[-1])


# Wifi is a special case of an HttpExposedObject.
class Wifi(HttpExposedObject):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._network_service = dbusif.RemoteObject("network", "/nl/ultimaker/network")

    def register(self):
        super().register()
        # Hackish way of doing it, but the inability of flask to remove paths is screwing with us again.
        self._server.add_url_rule(self.getAbsolutePath() + "/<ssid>", view_func = self.connectWithNetwork, methods=["PUT"])
        self._server.add_url_rule(self.getAbsolutePath() + "/<ssid>", view_func = self.forgetNetwork, methods=["DELETE"])

    def forgetNetwork(self, ssid):
        unquoted_ssid = parse.unquote(ssid)
        for network in self._network_service.getWifiNetworks():
            if network["name"] == unquoted_ssid:
                self._network_service.forgetWifiNetwork(network["id"])
                return FlaskResponses.createSuccess()
        return FlaskResponses.createSuccess("Network not found")

    def connectWithNetwork(self, ssid):
        unquoted_ssid = parse.unquote(ssid)
        json_data = flask.request.json
        try:
            passphrase = json_data["passphrase"]
        except:
            passphrase = ""
        for network in self._network_service.getWifiNetworks():
            if network["name"] == unquoted_ssid:
                self._network_service.connectToWifiNetwork(network["id"], passphrase)
                return FlaskResponses.createSuccess()
        return FlaskResponses.createSuccess("Network not found")

    def get(self):
        force_refresh = flask.request.args.get("force_refresh", False, type=bool)
        if force_refresh:
            pass    # force refresh is ignored now. Parameter is still accepted for compatibility. Scanning is handled by the network service.

        network_list = []
        for network in self._network_service.getWifiNetworks():
            network_data = {}
            network_data["SSID"] = network["name"]
            network_data["strength"] = network["strength"]
            network_data["connected"] = str(network["state"]) == "online"
            # Pretty weird way to read it, but the getNetworkServicesInfo is straaaange. It gives an array for the security bit...)
            network_data["security_required"] = bool(network["security"])
            network_list.append(network_data)
        return FlaskResponses.createSuccess(None, network_list)
