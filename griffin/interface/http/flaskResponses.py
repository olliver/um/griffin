## This file defines a set of standard response definitions used in a lot of API calls.
#  All these functions accept a message or json parameter.
#  If the json parameter is given, the given object will be json encoded and reported back to the caller.
#  Else, the message is returned in the form of {"message": message}
import flask


class FlaskResponses:

    # Constants to HTTP codes
    HTTP_OK                 = 200
    HTTP_CREATED            = 201
    HTTP_ACCEPTED           = 202
    HTTP_NO_CONTENT         = 204
    HTTP_BAD_REQUEST        = 400
    HTTP_UNAUTHORIZED       = 401
    HTTP_FORBIDDEN          = 403
    HTTP_NOT_FOUND          = 404
    HTTP_METHOD_NOT_ALLOWED = 405

    # 200=OK, Request was successful.
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Ok"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createSuccess(message="Ok", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_OK)

    # 201=Created, Request was successful and resulted in new resource being created
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Created"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createCreated(message="Created", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_CREATED)

    # 202=Accepted, Request has been accepted for processing, but processing has not been completed
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Ok"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createAccepted(message="Accepted", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_ACCEPTED)

    # 204=NoContent, Request was successful and might only return updated data
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "NoContent"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createNoContent(message="NoContent", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_NO_CONTENT)

    # 400=BadRequest, error, something was wrong with the request and we failed to execute
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Bad request"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createFailed(message="Bad request", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_BAD_REQUEST)

    # 401=Unauthorized
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Authorization required"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createUnauthorized(message="Authorization required", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_UNAUTHORIZED)

    # 403=Forbidden
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Forbidden"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createForbidden(message="Forbidden", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_FORBIDDEN)

    # 404=NotFound, error, we cannot find what you where looking for.
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Not found"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createNotFound(message="Not found", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_NOT_FOUND)

    # 405=Method not allowed
    # @param message: The message given as a {"message": message} json result for this response. Defaults to "Method not allowed"
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def createMethodNotAllowed(message="Method not allowed", json=None):
        return FlaskResponses.__createSimpleResponse(message, json, FlaskResponses.HTTP_METHOD_NOT_ALLOWED)

    # Creates the correct flask response object
    # @param message: The message given as a {"message": message} json result for this response.
    # @param json: If the json parameter is given, the message parameter is ignored, and the json parameter is encoded as json result.
    #              Usually this parameter is a dict, list or string.
    # @return The flask response object
    @staticmethod
    def __createSimpleResponse(message, json, code):
        # Read all uploaded files. Else flask will send out a response before the uploads are completed.
        for filename, file in flask.request.files.items():
            file.stream.read()

        # Generate a message response if no json is given, else generate the json response.
        if json is None:
            response = flask.Response(flask.json.dumps({"message": message}), status=code, mimetype="application/json")
        else:
            response = flask.Response(flask.json.dumps(json), status=code, mimetype="application/json")

        # Do not cache the API responses
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
        response.headers["Pragma"] = "no-cache" # HTTP 1.0.
        response.headers["Expires"] = "0" # Proxies.

        return response
