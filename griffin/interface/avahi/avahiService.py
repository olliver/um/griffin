import logging
import platform

from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


# Register ourselves with on the Avahi server, which handles zeroconf.
# This allows the printer to be visible on the local network
class AvahiService:
    SERVER_INVALID, SERVER_REGISTERING, SERVER_RUNNING, SERVER_COLLISION, SERVER_FAILURE = range(0, 5)
    ENTRY_GROUP_UNCOMMITED, ENTRY_GROUP_REGISTERING, ENTRY_GROUP_ESTABLISHED, ENTRY_GROUP_COLLISION, ENTRY_GROUP_FAILURE = range(0, 5)
    IF_UNSPEC = -1
    PROTO_UNSPEC, PROTO_INET, PROTO_INET6 = -1, 0, 1

    def __init__(self, port):
        self.__service_name = platform.node()
        # List of each Mdns name that we expose ourselves as.
        self.__services = [
            {"type": "_ultimaker._tcp", "port": 80},    # Ultimaker specific name, so we can find our own printers really quick.
            {"type": "_printer._tcp", "port": 80},      # General name to say that we are a printer. Required for WSPrint discovery from Windows
        ]

        self.__avahi_entry_group = None
        self.__avahi_server = None

        self.__printer = dbusif.RemoteObject("printer")
        self.__printer.connectSignal("onProcedureStart", self.__printerOnProcedureStart)
        self.__printer.connectSignal("onProcedureFinished", self.__printerOnProcedureFinished)
        self.__printer.connectSignal("onCartridgeDataReady", self.__printerOnCartridgeDataReady)
        self.__system = dbusif.RemoteObject("system")

        self.__txt_values = {}

        self.__setTxtValue("name", self.__system.getMachineName())
        self.__setTxtValue("type", "printer")
        MachineBOM = self.__system.getMachineBOM()
        self.__setTxtValue("machine", "%s.%s" % (str(MachineBOM[0]), str(MachineBOM[1])))
        self.__setTxtValue("firmware_version", self.__system.getVersion())

        self.__system.connectSignal("machineNameChanged", lambda machine_name: self.__setTxtValue("name", machine_name))

        self.__avahi_server = dbusif.RemoteObject("org.freedesktop.Avahi", "/", "org.freedesktop.Avahi.Server")
        self.__avahi_server.connectSignal("StateChanged", self.__avahiServerStateChanged)
        self.__avahiServerStateChanged(self.__avahi_server.GetState(), "")

    def __avahiServerStateChanged(self, state, error):
        if state == self.SERVER_RUNNING:
            # Register ourselves on the Avahi service
            self.__avahi_entry_group = dbusif.RemoteObject("org.freedesktop.Avahi", self.__avahi_server.EntryGroupNew(), "org.freedesktop.Avahi.EntryGroup")
            self.__avahi_entry_group.connectSignal("StateChanged", self.__entryGroupStateChanged)
            for service_data in self.__services:
                self.__avahi_entry_group.AddService(
                    self.IF_UNSPEC,     # interface
                    self.PROTO_UNSPEC,  # protocol
                    0,                  # flags
                    self.__service_name, service_data["type"],
                    "", "",
                    service_data["port"],
                    self.__getServerTextDBusFormat())
            self.__avahi_entry_group.Commit()

    def __getServerTextDBusFormat(self):
        ret = []
        for key, value in self.__txt_values.items():
            ret.append(list(map(ord, "%s=%s" % (key, value))))
        return ret

    def __entryGroupStateChanged(self, state, error):
        if state == self.ENTRY_GROUP_ESTABLISHED:
            log.info("Registered service on avahi (%s)", self.__service_name)
        elif state == self.ENTRY_GROUP_FAILURE:
            log.warning("Failed to register service on avahi (%s)", self.__service_name)

    def __setTxtValue(self, key, value):
        if key in self.__txt_values and self.__txt_values[key] == value:
            return
        self.__txt_values[key] = value
        if self.__avahi_entry_group is not None:
            for service_data in self.__services:
                self.__avahi_entry_group.UpdateServiceTxt.callLater(
                    self.IF_UNSPEC,     # interface
                    self.PROTO_UNSPEC,  # protocol
                    0,                  # flags
                    self.__service_name, service_data["type"],
                    "",
                    self.__getServerTextDBusFormat())

    def __printerOnProcedureStart(self, procedure_key, step_key):
        pass

    def __printerOnProcedureFinished(self, procedure_key):
        pass

    ## Callback when a hotend cartridge is changed and new data is ready for this hotend.
    #  Update our hotend type and serial fields.
    #  @param: hotend_nr, the index of the hotend that has been changed.
    def __printerOnCartridgeDataReady(self, hotend_nr):
        hotend_cartridge_id = self.__printer.getHotendCartridgeProperty(hotend_nr, "hotend_cartridge_id")
        if hotend_cartridge_id == "":
            self.__setTxtValue("hotend_type_%d" % (hotend_nr), "N/A")
        else:
            self.__setTxtValue("hotend_type_%d" % (hotend_nr), hotend_cartridge_id)
        self.__setTxtValue("hotend_serial_%d" % (hotend_nr), self.__printer.getHotendCartridgeSerial(hotend_nr))
