

# Simple data container to be used in the createNodes function.
# Main reason for this class to exist is to improve readability of the code that creates large XML structures.
class NodeData:
    def __init__(self, tag, *, attrib=None, text=None, children=None):
        ns = None
        if ":" in tag:
            ns, tag = tag.split(":", 1)
        self.tag = tag
        self.ns = ns
        self.attrib = attrib
        self.text = text
        self.children = children
