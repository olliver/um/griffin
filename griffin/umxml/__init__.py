from xml.etree import ElementTree

## umxml -> Ultimaker XML library.
#  Why?
#   xml.etree.ElementTree is the standard way to work with XML in python, which provides an OO interface to XML documents.
#   However, the interface that it provides to create new XML documents has two major shortcomings:
#   * It does not handle prefixes names for namespaces.
#   * The creation API isn't really OO, ElementTree.SubElement(node, tag_name) instead of node.createNode(tag_name)
#   For reading documents it has a different shortcoming:
#   * You have to supply the prefix->namespace dictionary to every find() call.
#
# To remedy this, there is the umxml module, which wraps around the ElementTree module to provide an easier to use API.
# One major change means that there is a Document class, which is the "root" node of the XML document.
# The Document class contains the list of namespaces used in this document.
#
# Basic usage:
# * Parse an XML document from a string:
# document = umxml.Document(text="<xml>text</xml>")
# * Find a specific sub node:
# node = document.find("./xml/namespace:tag")
# * Create a new XML document:
# document = umxml.Document(tag="xml", ns="namespace", namespaces={"namespace": "http://example.com/namespace"})
# * Add a new node to a document:
# node = document.createNode("tag", ns="namespace")
# * Set the text of a node:
# node.text = "text"
# * Modify an attribute of a node.
# node.attrib["attribute"] = "value"

from .document import Document
from .nodeData import NodeData
