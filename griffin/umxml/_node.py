from xml.etree import ElementTree

from .nodeData import NodeData

## Protected _Node class.
#  This represents a tag in XML. Contains a reference to the root node, which is of the class Document.
#  Never instantiate this class directly.
class _Node:
    # Create a new _Node object.
    # @param document, Document object, which is the root node of this XML document.
    # @param element, ElementTree object containing the actual data.
    def __init__(self, document, element):
        self.__document = document
        self._element = element

    # Create a sub node contained within this node.
    # @param tag: string, containing the name of this XML tag.
    # @param ns: optional string, short name of the namespace that this tag belongs to.
    # @param attrib: optional, dictionary containing key value pairs that will be set as attributes on this new XML tag.
    # @return new _Node object.
    def createNode(self, tag, *, ns=None, attrib=None):
        element = ElementTree.SubElement(self._element, self._createTagName(tag, ns))
        if attrib is not None:
            for key, value in attrib.items():
                element.attrib[key] = value
        return _Node(self.__document, element)

    # Shorthand function to create a lot of nodes in an easy way.
    # @param data: dictionary containing the nodes.
    def createNodes(self, data):
        assert isinstance(data, list)
        for item in data:
            assert isinstance(item, NodeData)
            element = ElementTree.SubElement(self._element, self._createTagName(item.tag, item.ns))
            if item.children is not None:
                _Node(self.__document, element).createNodes(item.children)
            if item.attrib is not None:
                for key, value in item.attrib.items():
                    element.attrib[key] = value
            if item.text is not None:
                element.text = item.text

    # Search for a node in this XML path.
    # Works the same as the ElementTree.find(), with the exception that you do not need to supply the namespaces.
    # @param path: Search string to look up a specific node.
    # @return new _Node object referencing the XML tag that was found, or None if the tag was not found.
    def find(self, path):
        element = self._element.find(path, self.__document.getNamespaces())
        if element is None:
            return None
        return _Node(self.__document, element)

    # Custom __setattr__ override, to allow to set the .text attribute of this object directly.
    # This to match the API of the ElementTree.
    # @param name: string of the _Node attribute that is being accessed.
    # @param value: new value to set this attribute to.
    def __setattr__(self, name, value):
        if name == "text":
            self._element.text = value
        else:
            super().__setattr__(name, value)

    # Custom __getattr__ override. To allow .text and .attrib access.
    # This function is only called for attributes that do not exist.
    # So we need to throw an AttributeError if we try to access an non existing attribute.
    # @param: name: string, name of the attribute that is being read.
    # @return: value of the attribute that was being accessed.
    def __getattr__(self, name):
        if name == "text":
            return self._element.text
        elif name == "attrib":
            return self._element.attrib
        raise AttributeError("'%s' object has no attribute '%s'" % (self, name))

    # Private function to create an ElementTree tag name from a tag+namespace combination.
    # This function throws an KeyError if an unknown namespace is used.
    # @param tag: string, containing the name of the tag.
    # @param ns: optional string, containing the prefix of the namespace.
    # @return: string, containing "{fullnamespace}tag" if a namespace is given, or "tag" if the namespace is None.
    def _createTagName(self, tag, ns=None):
        if ns is None:
            return tag
        return "{%s}%s" % (self.__document.getNamespaces()[ns], tag)
