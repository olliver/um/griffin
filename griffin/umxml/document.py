from xml.etree import ElementTree

from ._node import _Node


## Root of an XML document. Provides the same interface as _Node, with added function for exporting to string and handle namespaces.
class Document(_Node):
    # Create a new XML document. Can be used in 2 different ways:
    # * Parse a document from an XML string with Document(text="", namespaces={})
    # * Create a new empty document with Document(tag="tag", namespaces={})
    # In both cases it is required to supply a dictionary of namespaces. This is to ease looking up nodes or creating new nodes.
    # (If namespaces are not needed, then there is no need to use this wrapper and use ElementTree directly)
    # @param: text, string, containing XML document data. Throws a ValueError if the document fails to parse.
    # @param: tag: string, containing the new tag name.
    # @param: ns: optional string, should only be supplied with tag is supplied. The namespace for the new tag.
    # @param: namespaces: dictionary, containing prefix->namespace matches to use short prefix strings to lookup namespaces.
    def __init__(self, *, text=None, tag=None, ns=None, namespaces=None):
        assert text is None or tag is None, "When creating an XML document, it needs to be parsed from text or a new tag needs to be created. Cannot pass both."
        assert text is not None or tag is not None, "When creating an XML document, it needs to be parsed from text or a new tag needs to be created. Cannot pass none."
        assert namespaces is not None, "Namespaces dictionary is required."

        self.__namespaces = namespaces
        for prefix, uri in self.__namespaces.items():
            ElementTree.register_namespace(prefix, uri)

        if tag is not None:
            super().__init__(self, ElementTree.Element(self._createTagName(tag, ns)))
        else:
            try:
                super().__init__(self, ElementTree.XML(text))
            except ElementTree.ParseError:
                raise ValueError("Failed to parse XML document")

    # Because Microsoft is crazy and does things that do not match the XML standard,
    # we have this function.
    # This function forces a namespace definition to be included into the root node.
    # This is because for some WSPrint functions, namespaces are used inside the text part of XML nodes.
    # Parsers don't handle this, and shouldn't handle this. So we cheat around it by forcing the proper attribute to be included.
    # @param ns: string, prefix of the namespace that needs to be included.
    def forceNamespaceInclusion(self, ns):
        self._element.attrib["xmlns:%s" % (ns)] = self.__namespaces[ns]

    # Get the list of prefix->namespaces that is used for this XML document.
    # @return: dictionary, containing the prefix as key, and the namespace as value.
    def getNamespaces(self):
        return self.__namespaces

    # Turn this XML document into a proper XML string ready to be saved or send.
    # Assumes that you will encode the final string as utf-8.
    # @return: string, containing the whole XML document.
    def toString(self):
        result = ElementTree.tostring(self._element, encoding="unicode")
        # ElementTree does not add the general XML header, as it does not know the encoding. So we need to do that ourselves.
        result = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + result
        return result

    # Private function to create an ElementTree tag name from a tag+namespace combination.
    # This function throws an KeyError if an unknown namespace is used.
    # @param tag: string, containing the name of the tag.
    # @param ns: optional string, containing the prefix of the namespace.
    # @return: string, containing "{fullnamespace}tag" if a namespace is given, or "tag" if the namespace is None.
    def _createTagName(self, tag, ns=None):
        if ns is None:
            return tag
        return "{%s}%s" % (self.__namespaces[ns], tag)
