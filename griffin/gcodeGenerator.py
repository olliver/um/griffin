import math
import copy


## Abstraction of a toolHead in gcode or a hotend in a fused filament fabrication 3d printer
class _ToolHead():
    __DEFAULT_PRINTING_TEMPERATURE = 210 # printing temperature for PLA

    # @param generator A reference to the gcode generator.
    # @param tool_index The index number of the tool.
    # @param diameter the output diameter of the filament
    # @param filament_diameter an optional parameter for the diameter of the input material/feedstock
    def __init__(self, generator, tool_index, diameter, filament_diameter = 2.85):
        self.__gen = generator
        self.__tool_index = tool_index
        self.__diameter = diameter
        self.__material_used = 0
        self.__filament_diameter = filament_diameter
        self.__initial_temperature = None
        self.__retracted = False

    # @return index number of the hotend.
    def getToolIndex(self):
        return self.__tool_index

    # @return output diameter of the hotend in mm.
    def getDiameter(self):
        return self.__diameter

    # @return the material used in mm^3
    def getMaterialUsed(self):
        return self.__material_used

    # Logs amount of feedstock used.
    # @param mm_of_material Mm of feedstock/filament used.
    def usedMaterial(self, mm_of_material):
        self.__material_used += self.getMm3(mm_of_material)

    ## Generates the conversion factor between e mm and mm of travel in an extrusion move.
    # @param layer_height the height of the layer on which the conversion factor is used.
    def getExtrusionConversionFactor(self, layer_height = 0.3):
        return (self.__diameter * layer_height) / (1 / 4 * math.pi * self.__filament_diameter ** 2)

    ## translate mm of feedstock into mm^3 of material.
    # @param mm_of_material Mm of feedstock/filament used.
    def getMm3(self, mm_of_material):
        return mm_of_material * (1 / 4 * math.pi * self.__filament_diameter ** 2)

    ## Returns the tool printing temperature in degrees celsius.
    def getInitialTemperature(self):
        return self.__initial_temperature

    ## sets the initial temperature
    # @param temp the temperature in degrees celsius.
    def setTemperature(self, temp):
        if self.__initial_temperature is None:
            self.__initial_temperature = temp
        else:
            self.__gen.lines.append("M109 T{tool} S{temperature}".format(tool=self.getToolIndex(), temperature=temp))

    ## Returns the retracted state boolean of this tools material.
    def isRetracted(self):
        return self.__retracted

    ## Sets The retracted state.
    # @param retracted The retracted state boolean.
    def setRetracted(self, retracted):
        self.__retracted = retracted

# This class aides in the generation of gcode from 2d points / line segments.
class GcodeGenerator():
    __DEFAULT_BUILDPLATE_TEMPERATURE = 60 # buildplate temperature for PLA
    __NORMAL_RETRACTION_LENGTH = 6.5
    __RETRACT = ["G92 E0", "G1 E-{E} F2400".format(E=__NORMAL_RETRACTION_LENGTH)]
    __UNRETRACT = ["G92 E0", "G1 E{E} F2400".format(E=__NORMAL_RETRACTION_LENGTH), "G92 E0"]
    X = 0
    Y = 1
    Z = 2
    E = 3

    def __init__(self):
        self.__machine_size = [215.0, 215.0, 210.0, float("inf")] # machine_size in mm
        self.__NOZZLE_SWITCH_RETRACT_LENGHT = 15 # 20
        self.__nr_of_heads = 0
        self.__pos = [None] * (3 + 1)
        self.__active_tool = None
        self.__tools = []
        self.__layer_height = 0.3
        self.__layer_count = 1
        self.__pos[self.Z] = self.__layer_height
        self.__buildplate_temperature = self.__DEFAULT_BUILDPLATE_TEMPERATURE

        ## lines is output of the generator and could be used externaly if it is to be written to a different type of file etc.
        self.lines = []
        self.__after_travel = []
        self.__print_speed = 20.0 # mm/s
        self.__travel_speed = 150.0 # mm/s
        self.__z_hopped = 0.0

    ## Creates a tool with a nozzle diameter, tools are numbered in the order they are added, the first tool has the number 0.
    # @param nozzle_diameter Is the output diameter of the nozzle
    def createTool(self, nozzle_diameter):
        self.__tools.append(_ToolHead(self, self.__nr_of_heads, nozzle_diameter))
        self.__nr_of_heads += 1

    ## starts a new layer, the fist layer is started automatically with a layer height of 0.3mm
    # this also generates gcode and LAYER:nr tag and keeps track of layer count
    # @param layer_height the height of the layer.
    def startNextLayer(self, layer_height = 0.2):
        self.__pos[self.Z] += layer_height

        self.lines.append(";LAYER:{}".format(self.__layer_count))
        self.lines.append("G1 Z{}".format(self.__pos[self.Z]))
        self.__layer_count += 1

    # Set the print speed in any number of ways, only one method of specification can be used at any 1 time.
    # @param in_mmps If this parameter is used the print speed will be calculated based on this speed in mm/s.
    # @param in_mmpm If this parameter is used the print speed is set to this speed in mm/min.
    # @param in_mm3ps if this parameter is used the print speed will be calculated based on the desired filament flow in mm^3/s.
    def setExtrudeSpeed(self, in_mmps=None, in_mmpm=None, in_mm3ps=None):
        speed = None
        if in_mmps is not None:
            speed = float(in_mmps)

        if in_mmpm is not None and speed is not None:
            raise ValueError("only one measure of speed may be given at any 1 time")
        elif speed is None:
            speed = float(in_mmpm) / 60.0

        if in_mm3ps is not None and speed is not None:
            raise ValueError("only one measure of speed may be given at any 1 time")
        elif speed is None:
            raise NotImplementedError("speed based on mm^3/s is not implemented yet")

        if speed is None:
            return

        self.__print_speed = speed

    ## set the additional retraction length for a tool switch.
    def setToolSwitchRetractionLength(self, length):
        self.__NOZZLE_SWITCH_RETRACT_LENGHT = length

    ## Switches to a specific tool, this should be done before any movements are planned.
    # @param tool_nr the number of the tool to switch to.
    def switchToTool(self, tool_nr):
        if not tool_nr in range(self.__nr_of_heads):
            raise ValueError("tool_nr out of bounds {}".format(tool_nr))
        if self.__active_tool is not None:
            self.lines.append("G1 E{:.5f} F1200".format(-self.__NOZZLE_SWITCH_RETRACT_LENGHT))
        self.__active_tool = tool_nr
        self.lines.append("T{:d}".format(tool_nr))

        self.__after_travel.append("G92 E0")
        self.__pos[self.E] = -self.__NOZZLE_SWITCH_RETRACT_LENGHT

    ## returns an active tool if one is selected.
    # notes raises a TypeError if no tool is selected
    def getActiveTool(self):
        return self.__tools[self.__active_tool]

    ## Returns a specific tool
    # @param tool_nr the number of the tool to switch to.
    def getTool(self, tool_nr):
        return self.__tools[tool_nr]

    ## sets the buildplate temperature
    # @param temp the temperature in degrees celsius
    def setBuildplateTemperature(self, temp):
        self.__buildplate_temperature = temp

    ## Returns the buildplate temperature in degrees celsius.
    def getBuildplateTemperature(self):
        return self.__buildplate_temperature

    ## Sets the machine size as an [X,Y,Z,E] array members left out or set to None will be ignored.
    def setMachineSize(self, size):
        for i, value in enumerate(size):
            if value is not None:
                self.__machine_size[i] = float(value)

    ## Translates points with the given vector, vector is the list of additional arguments.
    # @param points must be a list of lists (aka a list of points), or a list of segments (each segments being a list of points).
    # @param args a list of arguments to be added to the points, each point must be at least as long as the amount of arguments given.
    # @return list An array of points or an array of segments.
    @classmethod
    def translate(cls, points, *args):
        if isinstance(points, list):
            if isinstance(points[0], list):
                if isinstance(points[0][0], list):
                    ret = []
                    for segment in points:
                        ret += [ cls.translate(segment, *args) ]
                    return ret
        try:
            return [ [p[i] + args[i] for i in range(0, len(args))] for p in points ]
        except:
            return None

    ## scales points with the given scaling vector, scaling vector is the list of additional arguments.
    # @param points must be a list of lists (aka a list of points), or a list of segments (each segments being a list of points).
    # @param args a list of arguments to be added to the points, each point must be at least as long as the amount of arguments given.
    # @return list An array of points or an array of segments.
    @classmethod
    def scale(cls, points, *args):
        if isinstance(points, list):
            if isinstance(points[0], list):
                if isinstance(points[0][0], list):
                    ret = []
                    for segment in points:
                        ret += [cls.scale(segment, *args)]
                    return ret
        try:
            return [ [p[i] * args[i] for i in range(0, len(args))] for p in points ]
        except:
            return None

    ## rotate a set of points about the origin [0,0]
    # @param points the bag of points to be rotated.
    # @param angle to rotate about.
    @classmethod
    def rotate(cls, points, angle):
        angle = angle/360 * 2 * math.pi
        matrix = [[math.cos(angle), -math.sin(angle)],
                  [math.sin(angle),  math.cos(angle)]]

        return cls.applyMatrixTo(points, matrix)

    ## Rotate about a point.
    # @param point the point to rotate about.
    # @param points the bag of points to be rotated.
    # @param angle to rotate about.
    @classmethod
    def rotateAbout(cls, point, points, angle):
        points = cls.translate(points, *[-p for p in point])
        points = cls.rotate(points, angle)
        points = cls.translate(points, *point)
        return points

    ## find the center of the bounding box of the set of points you specify
    # @param points the set of points to put a bounding box around.
    @classmethod
    def findCenterPoint(cls, points):
        min = [float( "inf")] * len(points[0])
        max = [float("-inf")] * len(points[0])
        for point in points:
            for axis, value in enumerate(point):
                value = float(value) # ensure the value is the coordinate of a point not some list, etc.
                if min[axis] > value:
                    min[axis] = value
                if max[axis] < value:
                    max[axis] = value

        # Calculate the center point.
        return [(min[i] + max[i]) / 2 for i in range(len(points[0]))]

    ## this assumes a 2x2 dimensional matrix [[,],[,]]
    # and assumes points to be 2 dimensional vectors
    @classmethod
    def applyMatrixTo(cls, points, matrix):
        if isinstance(points, list):
            if isinstance(points[0], list):
                if isinstance(points[0][0], list):
                    ret = []
                    for segment in points:
                        ret += [cls.applyMatrixTo(segment, matrix)]
                    return ret

        return [ [matrix[cls.X][0] * point[cls.X] + matrix[cls.X][1] * point[cls.Y], matrix[cls.Y][0] * point[cls.X] + matrix[cls.Y][1] * point[cls.Y] ] for point in points]

    ## Returns the delta between two positions and updates the old position with the new one.
    # Note that the position is also updated.
    # @param new_point The new position, note that pos and new pos should be equal sized lists.
    # @return a list with the difference between pos and new_point.
    def __getDelta(self, new_point):
        ret = [None] * len(new_point)
        for i in range(0, len(new_point)):
            try:
                ret[i] = abs(self.__pos[i] - new_point[i])
            except TypeError:
                pass # __pos was None most of the time position is known, there also can't be a delta when the previous position is not known..
            self.__pos[i] = new_point[i]
        return ret

    ## generates gcode lines, traveling to the start of a list of points and extruding between points.
    # or in case of a list of segments which each contain points we travel to the start of each segment, etc.
    # X- and Y-moves only.
    # @param points must be a list of lists (aka a list of points), or a list of segments (each segment being a list of points).
    # @param nozzle_diam is used to calculate the amount of material to extrude.
    # @param travel, make every other move a travel move (no retraction)
    # @param retract adds a retraction between each segment. 
    def plot(self, points, travel = False, retract = True):
        if isinstance(points, list):
            if isinstance(points[0], list):
                if isinstance(points[0][0], list):
                    for segment in points:
                        self.plot(segment, travel, retract)     # recursive call
                    return

        # e per mm of travel
        epmm = self.getActiveTool().getExtrusionConversionFactor(self.__layer_height)
        prev_move = None
        do_unretract = retract
        e_pos = 0
        for point in points:
            delta = self.__getDelta(point)
            out = "G1 X{:.5f} Y{:.5f} ".format(*point)
            if prev_move == None or (travel and prev_move == "extrude"):
                #travel move
                if prev_move is not "travel":
                    out += "F{:f} ".format(self.__travel_speed * 60.0)
                    prev_move = "travel"
            else:
                #extrude move
                if prev_move is not "extrude":
                    out += "F{:f} ".format(self.__print_speed * 60.0)
                    prev_move = "extrude"
                e = epmm * math.sqrt(delta[0] ** 2 + delta[1] ** 2)
                e_pos += e
                out += "E{:.5f}".format(e_pos + self.__pos[self.E])
            self.lines.append(out)
            if self.__after_travel: # actually checking this as current move now.
                self.lines += self.__after_travel
                self.__after_travel = []
            if do_unretract:
                do_unretract = False
                e_pos = 0
                self.lines += copy.deepcopy(self.__UNRETRACT)
                self.getActiveTool().setRetracted(False)

                self.__pos[self.E] = 0
        self.getActiveTool().usedMaterial(e_pos)
        if retract:
            self.lines += copy.deepcopy(self.__RETRACT)
            self.getActiveTool().setRetracted(True)
            e_pos = 0

        if self.__pos[self.E] is not None:
            self.__pos[self.E] += e_pos
        else:
            self.__pos[self.E] = e_pos

    ## Travel to a point. Allows Z-moves as well.
    # @param the point to travel to.
    # @param zHop, optional parameter to lift the nozzle above the print bed during move. Defaults to False.
    def travelTo(self, point, zHop=False):
        if zHop:
            self.retract()
            self.zHopStart()
            
        line = "G1 F{:f} ".format(self.__travel_speed * 60.0)
        axes = ["X", "Y", "Z"]
        self.__getDelta(point)
        for i, p in enumerate(point):
            line += "{}{} ".format(axes[i], float(p))
        self.lines.append(line)

        if zHop:
            self.zHopEnd()
            self.unretract()

    ## Retracts the nozzle immediately
    def retract(self):
        if self.getActiveTool().isRetracted():
            return
        self.lines += copy.deepcopy(self.__RETRACT)
        self.getActiveTool().setRetracted(True)
        self.__pos[self.E] = -self.__NORMAL_RETRACTION_LENGTH

    ## Undoes the retraction on the nozzle immediately.
    def unretract(self):
        if not self.getActiveTool().isRetracted():
            return
        self.getActiveTool().setRetracted(False)
        self.lines += copy.deepcopy(self.__UNRETRACT)
        self.__pos[self.E] = 0

    ## Lift nozzle above the print.
    # @param hop_height Optionally you can specify the height to lift above the print.
    def zHopStart(self, hop_height=10):
        self.__z_hopped += hop_height
        self.lines.append("G1 Z%f F9000" % (self.__pos[self.Z] + self.__z_hopped))

    ## If the nozzle is lifted above the print, return it to print height.
    def zHopEnd(self):
        self.lines.append("G1 Z%f F9000" % self.__pos[self.Z])
        self.__z_hopped = 0.0

    # @return True if the nozzle is lifted above the print.
    def isZHopped(self):
        return self.__z_hopped != 0.0


    ## internal function to generate the JEDI header
    # @param time the total print time in seconds, as we have no time estimation code, the default of 5 minutes can be overwritten by the user.
    def __createHeader(self, time = 300):
        header = [
            ";START_OF_HEADER",
            ";HEADER_VERSION:0.1",
            ";FLAVOR:Griffin",
            ";GENERATOR.NAME:xy calibration script",
            ";GENERATOR.VERSION:2.1",
            ";GENERATOR.BUILD_DATE:2016-12-01",
            ";TARGET_MACHINE.NAME:Ultimaker Jedi"
        ]
        for tool_nr, tool in enumerate(self.__tools):
            header += [
                ";EXTRUDER_TRAIN.{0}.INITIAL_TEMPERATURE:{1}".format(tool_nr, tool.getInitialTemperature()),
                ";EXTRUDER_TRAIN.{0}.MATERIAL.VOLUME_USED:{1}".format(tool_nr, tool.getMaterialUsed()),
                ";EXTRUDER_TRAIN.{0}.NOZZLE.DIAMETER:{1}".format(tool_nr, tool.getDiameter()),
            ]
        header += [
            ";BUILD_PLATE.INITIAL_TEMPERATURE:%d" % self.getBuildplateTemperature(),
            ";PRINT.TIME:%d" % time,
            ";PRINT.SIZE.MIN.X:0",
            ";PRINT.SIZE.MIN.Y:0",
            ";PRINT.SIZE.MIN.Z:0",
            ";PRINT.SIZE.MAX.X:{0}".format(*self.__machine_size),
            ";PRINT.SIZE.MAX.Y:{1}".format(*self.__machine_size),
            ";PRINT.SIZE.MAX.Z:{2}".format(*self.__machine_size),
            ";END_OF_HEADER",
            "",
            ";Layer count: {}".format(self.__layer_count),
            ";LAYER:0",
            "G1 Z0.3 F9000",
            "M204 S500", # set acceleration to super slow!
            "M205 X5",   # set (xy) jerk to be super low
            "M107",
            ";TYPE:WALL-OUTER"
        ]
        return header

    ## Primes the active nozzle.
    def prime(self):
        self.lines += [
            "G0 F7200 X{X} Y6 Z3".format(X=175 + 5 * self.getActiveTool().getToolIndex()),
            "G280"
        ]
        self.__after_travel.append("G1 Z{Z} F9000".format(Z=self.__pos[self.Z]))
        self.retract()

    ## this function exists to apply a specific transform to the gcode lines in the buffer of the generator class before it is written to a file.
    # this could have been done via the exposed lines parameter.
    # @param function a function that takes one argument, an array of gcode commands, this array may be modified to effect change.
    def postProcess(self, function):
        function(self.lines)

    ## Writes the lines to the buffer after generating the Header.
    # @param filename the name/path of the file to write to.
    # @param user_comments can be an array of gcode or comments to be included between the header and the main body of gcode.
    # @param time you can add a time estimate, the default is 10 minutes.
    # @remark This function will throw an OSError if something goes wrong while writing the file.
    def writeToFile(self, filename, user_comments=None, time=600):
        with open(filename, "w") as f:
            for line in self.__createHeader(time):
                f.write(line + "\n")
            if isinstance(user_comments, list):
                for line in user_comments:
                    f.write(line + "\n")
            for line in self.lines:
                f.write(line + "\n")
