import binascii
from griffin import dbusif
from .contents.material.materialRecordFactory import MaterialRecordFactory
from .contents.stat.statRecordBase import StatRecordBase
import logging
import uuid
import time

log = logging.getLogger(__name__.split(".")[-1])


## Class to handle NFC related information linked to a hotend.
#  This keeps track of which tag is linked to a hotend, and which possible new tag should be linked to a hotend.
class NfcPrinterHotendInfo:
    ##< Defines the maximum difference in stat record timing. If this is too much, something has gone wrong and needs to be corrected
    __STAT_RECORD_TOO_OLD_TIME_THRESHOLD = 60 * 60 # 60 minutes threshold

    ## @brief Initializes the hotend info and connects to the signal of the material extrusion monitor
    # @param nfc_controller Instance of the nfc_controller
    # @param registry: A griffin.preferences.registry.Registry object which is used to store the tag links in, to make this data non-volatile.
    # @param index: The index of the hotend.
    def __init__(self, nfc_controller, registry, index):
        self.__nfc_controller = nfc_controller
        self.__registry = registry
        self.__index = index
        self.__printer_hotend_service = dbusif.RemoteObject("printer", "printer/head/0/slot/%d" % (index))
        self.__material_service = dbusif.RemoteObject("material")
        self.__printer = dbusif.RemoteObject("printer")
        self.__properties = self.__printer_hotend_service.getProperties()

        self.__printer_hotend_service.connectSignal("propertyChanged", self.__propertyChanged)
        self.__linked_tag = None
        self.__resetTag()
        self.__printer.connectSignal("onExtrusionAmountChanged", self.__onExtrusionAmountChanged)

    # Callback called when a property in the printer hotend slot changes.
    # We trigger on changes of the material guid and material guid source to setup or break down our link with a tag.
    def __propertyChanged(self, key, value):
        self.__properties[key] = value
        if key == "material_guid":
            if value == "":
                self.setLinkedTag(None)
            elif self.__new_tag_link_request is not None and uuid.UUID(value) == self.__new_tag_link_request.getRecordsByType(MaterialRecordFactory.getType())[0].getMaterialId():
                log.info("Material selection confirmed for hotend %d", self.__index)
                self.setLinkedTag(self.__new_tag_link_request)
        elif key == "material_guid_source" and value == "USER":
            self.setLinkedTag(None)

    ## Set a new tag request, this sets up the tag to be a possible new tag for this hotend.
    #  The user still needs to confirm, so the link is not actual at this point.
    #  @param tag: a nfcTag.NfcTag object containing material and stat records.
    def setNewTagRequest(self, tag):
        self.__new_tag_link_request = tag

    # @return the UID of the tag for a link request, or None if no link is currently being requested. Returned as bytes object.
    def getUidForRequest(self):
        if self.__new_tag_link_request is None:
            return None
        return self.__new_tag_link_request.getUid()

    # @return the UID of the tag that is currently linked. A zero length bytes object is returned when no link is set.
    def getLinkedTagUid(self):
        return binascii.unhexlify(self.__registry.getAsString("hotend_%d" % (self.__index), "").encode('ascii'))

    # @return a nfcTag.NfcTag object that is linked to this hotend. Or None if there is no link set or the tag has not been seen yet by the system.
    def getLinkedTag(self):
        return self.__linked_tag

    # Set the currently linked tag. This stores the tag UID for re-use after reboot, and the reference to the tag so data can be read.
    # @param tag: a nfcTag.NfcTag object.
    def setLinkedTag(self, tag):
        if tag is None:
            self.__registry.set("hotend_%d" % (self.__index), "")
        else:
            self.__registry.set("hotend_%d" % (self.__index), binascii.hexlify(tag.getUid()).decode('ascii', 'ignore'))
        self.__linked_tag = tag
        self.__resetTag()

        if self.__linked_tag is not None:
            material = self.__linked_tag.getRecordsByType(MaterialRecordFactory.getType())[0]
            material_guid = str(material.getMaterialId())
            material_properties = self.__material_service.getMaterialProperties(material_guid)
            log.info("Material GUID %s => properties = %r", material_guid, material_properties)
            stat_records = self.__linked_tag.getRecordsByType(StatRecordBase.getType())
            for stat_record in stat_records:
                stat_record.setMaterialProperties(material_properties)

            # After setting the properties we can check if there are corrections needed
            self.__applyStatRecordCorrections()
            # For new tags, both are the same value (0 seconds), so don't care which one is going to be used at this point
            stat_record = max(stat_records, key=lambda record: record.getTotalUsageDuration())
            self.__remaining_length = stat_record.getMaterialRemaining()
            # Both stat records should have recorded the same total length!
            self.__total_length = stat_record.getMaterialTotal()
            if self.__remaining_length > self.__total_length:
                raise ValueError("The remaining length can never be more then the total length!")

    ## @brief Apply stat record corrections implements corrections needed when loading tags, like fixing broken stat record,
    #         or resetting values to allow correct handling of the data
    def __applyStatRecordCorrections(self):
        assert self.__linked_tag is not None, "This should only be used when a tag is linked to the hotend!"

        # Get the two stat records
        stat_records = self.__linked_tag.getRecordsByType(StatRecordBase.getType())
        oldest_record = min(stat_records, key=lambda record: record.getTotalUsageDuration())
        latest_record = max(stat_records, key=lambda record: record.getTotalUsageDuration())

        # First check: if the usage_times are to far apart, bring the oldest closer to the newest (timewise)
        if abs(latest_record.getTotalUsageDuration() - oldest_record.getTotalUsageDuration()) > NfcPrinterHotendInfo.__STAT_RECORD_TOO_OLD_TIME_THRESHOLD:
            # Bring the oldest back, but one minute before the latest
            oldest_record.setTotalUsageDuration(latest_record.getTotalUsageDuration() - 60)
            # In this case, also check if the older one has less remaining material then the latest, and if this is the case, update the latest to have the same amount
            if (oldest_record.getMaterialRemaining() < latest_record.getMaterialRemaining()):
                latest_record.setRemainingMaterial(latest_record.getMaterialRemaining(), 0)

    ## @brief Retrieves the total and remaining amount of material. The total is constant until a tag is unlinked or a new tag is linked
    #  @return Returns a tuple of (remaining, total) lengths
    def getMaterialAmount(self):
        return (self.__remaining_length, self.__total_length)

    ## @brief Reset tag is actually resetting the tag request and restoring settings to proper values
    def __resetTag(self):
        self.__new_tag_link_request = None
        # Reset remaining length and total length as well
        self.__remaining_length = -1.0
        self.__total_length = -1.0

    ## @brief Function to handle the extruded amount of material for the given printcore
    #  @param hotend_index The hotend index
    #  @param extrusion_amount The amount extruded
    #  @param usage_time The estimated time it took to extrude/retract the amount
    def __onExtrusionAmountChanged(self, hotend_index, extrusion_amount, usage_time):
        # Not indended for this hotend
        if self.__index != hotend_index:
            return
        if self.__linked_tag is None:
            log.debug("No tag linked to extrusion-train %d, likely no smart material used. Unable to update remaining material." % (hotend_index))
            return

        self.__remaining_length = max(0, self.__remaining_length - extrusion_amount)
        # Update the oldest stat record with the new values
        stat_records = self.__linked_tag.getRecordsByType(StatRecordBase.getType())
        record = min(stat_records, key=lambda record: record.getTotalUsageDuration())
        # Only queue record if the remaining lengths differ!
        if record.getMaterialRemaining() == self.__remaining_length:
            return

        record.setRemainingMaterial(self.__remaining_length, usage_time)
        log.info("PrintCore {hotend_index} extruded {extrusion_amount} mm in {usage_time}, remaining length = {remaining_length} mm, stat record usage duration: {total_usage_duration}".format(
            hotend_index=hotend_index,
            extrusion_amount=extrusion_amount,
            usage_time=usage_time,
            remaining_length=record.getMaterialRemaining(),
            total_usage_duration=record.getTotalUsageDuration()))
        self.__nfc_controller.writeBack(hotend_index, self.__linked_tag, record)
