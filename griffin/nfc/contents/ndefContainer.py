import struct


## Class to parse NDEF records.
#  This class is designed to be able to parse partial NDef records for size, to limit the amount of reads required to the RFID tag.
#  Bytes should be feed to this class with addData, till getRequiredSize <= getCurrentSize.
class NDefContainer:
    # Bits in the first byte of the NDEF header.
    __HEADER_MESSAGE_BEGIN_BIT = 7
    __HEADER_MESSAGE_END_BIT = 6
    __HEADER_CHUNK_BIT = 5
    __HEADER_SHORT_BIT = 4
    __HEADER_ID_LENGTH_BIT = 3
    # Mask for the TYPE NAME in first byte of the the NDEF header.
    __HEADER_TYPE_NAME_FORMAT_MASK = 0x07

    # Possible values of the __HEADER_TYPE_NAME_FORMAT_MASK according to the NDEF specifications.
    TYPE_NAME_FORMAT_EMPTY = 0x00
    TYPE_NAME_FORMAT_KNOWN = 0x01      # NFC RTD
    TYPE_NAME_FORMAT_MEDIA_TYPE = 0x02 # according to RFC2046
    TYPE_NAME_FORMAT_URI = 0x03        # according to RFC3986
    TYPE_NAME_FORMAT_EXTNAL = 0x04     # NFC RTD
    TYPE_NAME_FORMAT_UNKNOWN = 0x05
    TYPE_NAME_FORMAT_UNCHANGED = 0x06
    TYPE_NAME_FORMAT_RESERVED = 0x07

    ##< Start address of this ndef record
    _start_address = 0

    # Create a new ndef record.
    def __init__(self, start_index, extra_bytes=None):
        self.__data = b""
        self.__start_index = start_index
        if extra_bytes is not None:
            self.addData(extra_bytes)
            self.__start_index -=  len(extra_bytes)
            # Should not happen, if it does, someone who uses the library is using it the wrong way!
            assert self.__start_index >= 0, "Extra bytes should belong to previous ndef record!"

    # Add data to this ndef record.
    # @param extra_bytes: a bytes object containing the extra data read from the rfid tag.
    def addData(self, extra_bytes):
        self.__data = self.__data + extra_bytes

    ## Gets the starting address (offset in bytes)
    # @return The starting address
    def getStartIndex(self):
        return self.__start_index

    # @return bool: True if we have the header flags available. (at least 1 byte available)
    def hasHeaderFlags(self):
        return len(self.__data) > 0

    # @return bool: True if this NDEF container has the begin flag set, which is the first NDEF container in the tag.
    def isHeaderFlagsMessageBeginBitSet(self):
        return self.__data[0] & (1 << NDefContainer.__HEADER_MESSAGE_BEGIN_BIT)

    # @return bool: True if this NDEF container has the end flag set, which is the last NDEF container in the tag.
    def isHeaderFlagsMessageEndBitSet(self):
        return self.__data[0] & (1 << NDefContainer.__HEADER_MESSAGE_END_BIT)

    # @return bool: True if this NDEF container has the chunk flag set, which means the data continues in the next NDEF container as well.
    def isHeaderFlagsChunkBitSet(self):
        return self.__data[0] & (1 << NDefContainer.__HEADER_CHUNK_BIT)

    # @return bool: True if this NDEF container is a short record, meaning it has a payload up to 255 bytes. A long record can have a payload up to 2^32-1 bytes
    def isHeaderFlagsShortBitSet(self):
        return self.__data[0] & (1 << NDefContainer.__HEADER_SHORT_BIT)

    # @return bool: Indicates that the Id field+length is present or not.
    def isHeaderFlagsIdLengthBitSet(self):
        return self.__data[0] & (1 << NDefContainer.__HEADER_ID_LENGTH_BIT)

    # @return int: One of the TYPE_NAME_FORMAT_* values to indicate the format of this type name. Note, the "getType" function uses this to modify it's result.
    def getHeaderFlagsTypeNameFormat(self):
        return self.__data[0] & NDefContainer.__HEADER_TYPE_NAME_FORMAT_MASK

    # @return int: The size of the data currently stored in this NDEF record. The record is incomplete as long as getCurrentSize() < getRequiredSize()
    def getCurrentSize(self):
        return len(self.__data)

    # @return int: The size we think we need to for a complete NDEF record. Note that we cannot fully know which size the record will be until we have the full header.
    #              so until the header is complete, the result from this function can change.
    def getRequiredSize(self):
        if len(self.__data) < 3:
            # No header yet, assume we need the basic shortest header.
            return 3
        total_size = 3
        if not self.isHeaderFlagsShortBitSet():
            # If we do not have a short header, then we need 3 extra bytes for the payload size.
            total_size += 3
        # Add the type length to the header size.
        total_size += self.__data[1]
        # Add the payload length to the size
        if self.isHeaderFlagsShortBitSet():
            total_size += self.__data[2]
            index = 3
        else:
            if len(self.__data) < 6:
                return total_size
            total_size += struct.unpack('>L', self.__data[2:6])[0]
            index = 6
        # If we have the id part, add it's length as well.
        if self.isHeaderFlagsIdLengthBitSet():
            total_size += 1 # Add the size of the length byte.
            if len(self.__data) <= index:
                return max(index, total_size)
            total_size += self.__data[index]
            index += 1
        return total_size

    # Precondition: getCurrentSize() >= getRequiredSize()
    # @return: the type id stored in this NDEF record, as a bytes() object.
    #          the result is changed according to the type name format in the flags byte.
    def getType(self):
        if self.isHeaderFlagsShortBitSet():
            index = 3
        else:
            index = 6
        if self.isHeaderFlagsIdLengthBitSet():
            index += 1
        length = self.__data[1]
        my_type = self.__data[index:index+length]
        if self.getHeaderFlagsTypeNameFormat() == NDefContainer.TYPE_NAME_FORMAT_KNOWN:
            return b"urn:nfc:wkt:" + my_type
        if self.getHeaderFlagsTypeNameFormat() == NDefContainer.TYPE_NAME_FORMAT_EXTNAL:
            return b"urn:nfc:ext:" + my_type
        return my_type

    # Precondition: getCurrentSize() >= getRequiredSize()
    # @return: The id in this ndef record as bytes object, or a zero length bytes object if no id is available.
    def getId(self):
        if not self.isHeaderFlagsIdLengthBitSet():
            return b""

        if self.isHeaderFlagsShortBitSet():
            index = 3
        else:
            index = 6
        length = self.__data[index]
        index += 1
        index += self.__data[1]
        return self.__data[index:index+length]

    # Calculate the position and length of the payload in this NDEF record.
    # @return: tuple containing the position and length of the payload.
    def calculatePayloadPositionAndSize(self):
        if self.isHeaderFlagsShortBitSet():
            length = self.__data[2]
            index = 3
        else:
            length = struct.unpack('>L', self.__data[2:6])[0]
            index = 6
        if self.isHeaderFlagsIdLengthBitSet():
            index += self.__data[index]
            index += 1
        index += self.__data[1]
        return index, length

    # Precondition: getCurrentSize() >= getRequiredSize()
    # @return: The payload of this NDEF record, the payload contains the actual data stored in this record.
    def getPayload(self):
        index, length = self.calculatePayloadPositionAndSize()
        return self.__data[index:index + length]

    # Precondition: getCurrentSize() >= getRequiredSize()
    # @return: Extra data stored in this ndef record container with addData which is not actually part of this NDEF record and thus could be part of the next NDEF record.
    def getExtraData(self):
        index, length = self.calculatePayloadPositionAndSize()
        return self.__data[index + length:]
