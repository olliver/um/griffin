from .statRecordBase import StatRecordBase
from griffin.crc8 import Crc8
import struct


# Version 0 stat record according to specs at:
# https://docs.google.com/document/d/1QlCaHTPI9n3rWuUkVwX_0l9z5IzrI1hfUMJQ5V2Lirk/edit
class StatRecordV0(StatRecordBase):
    ##< Record size, see specificationed above
    __RECORD_SIZE = 20

    def __init__(self, ndef_container):
        super().__init__(ndef_container)

        self._material_unit = self._data[2]
        self._material_total = struct.unpack(">L", self._data[3:7])[0]
        self._material_remaining = struct.unpack(">L", self._data[7:11])[0]
        self._total_usage_duration = struct.unpack(">Q", self._data[11:19])[0]

        # V0 specs define units up to Volume, so everything above that is not valid.
        if self._material_unit > StatRecordBase.MATERIAL_QUANTITY_VOLUME_CM3:
            self._material_unit = StatRecordBase.MATERIAL_UNIT_UNUSED

        crc_data = self._data[19]
        crc_result = Crc8(self._data[ :19]).digest()[0]
        if crc_data != crc_result:
            raise ValueError("Crc error on record: %02x %02x" % (crc_data, crc_result))

    def getData(self):
        data = bytearray(self.getRecordSize())
        data[0] = self._version
        data[1] = self._compatibility_version
        data[2] = self._material_unit
        data[3:7] = struct.pack(">L", self._material_total)
        # Need a cast, because during printing the material remaining becomes a float for somewhat more precision
        data[7:11] = struct.pack(">L", int(self._material_remaining))
        # Same for total usage duration!
        data[11:19] = struct.pack(">Q", int(self._total_usage_duration))
        data[19] = Crc8(data[:19]).digest()[0]
        return data

    def getRecordSize(self):
        return StatRecordV0.__RECORD_SIZE