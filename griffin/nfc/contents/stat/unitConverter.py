import math

import logging

log = logging.getLogger(__name__.split(".")[-1])


## Basic class to convert
# from mass to length or
# from volume (cm^3) to length or
# from length to mass
# Short reminder of math rules:
# diameter = mass / volume
class UnitConverter:
    ## @brief Initializes the conversion properties and pre-calculations
    #  @param density The density
    #  @param diameter The diameter
    #  @param kwargs Eat the rest of the possible arguments
    def __init__(self, *, density, diameter, **kwargs):
        self.__filament_density = density
        self.__diameter = diameter
        filament_radius = self.__diameter / 2.0
        self.__cross_section_surface_area = math.pi * (filament_radius ** 2)

    ## @brief convert a value to no known length.
    #  @param input Some value
    #  @return Returns None
    def convertUnknown(self, input):
        return None

    ## @brief convert the input in millimeters filament to millimeters filament.
    #  @param input The length in mm
    #  @return Returns the length in mm (same as the input)
    def convertFromLengthToLength(self, input):
        return input

    ## @brief Convert the input in mass (milligrams) to millimeters filament
    # @param input The mass in milligrams
    # @return The length in milimeters
    def convertFromMassToLength(self, input):
        # Density in grams per cubic centimeter
        return self.convertFromVolumeToLength(input / self.__filament_density)

    ## @brief Convert volume in cubic milimeters
    #  @param input Volume in cm3
    #  @return The length in mm
    def convertFromVolumeToLength(self, input):
        return input / self.__cross_section_surface_area


    ## @brief Converts length in mm to mass in mg
    #  @param input Length in mm
    #  @return Returns the mass in mg
    def convertFromLengthToMass(self, input):
        volume = input * self.__cross_section_surface_area
        return volume * self.__filament_density
