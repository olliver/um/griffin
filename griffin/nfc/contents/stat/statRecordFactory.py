from griffin.nfc.contents.recordFactoryBase import RecordFactoryBase
from .statRecordV0 import StatRecordV0

import logging

log = logging.getLogger(__name__.split(".")[-1])


class StatRecordFactory(RecordFactoryBase):
    @classmethod
    def _getRecordClasses(cls):
        return { 0: StatRecordV0 }