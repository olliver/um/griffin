from griffin.nfc.contents.recordBase import RecordBase
from .unitConverter import UnitConverter


# Base class for stat records, this contains all data that a stat record could contain,
# and will fill it will basic "null" data. Specific versions of the stat record will fill this record with the proper data.
# This base class ensures a uniform interface to all versions of the stat record.
class StatRecordBase(RecordBase):
    ##< The NDEF record type that should be used for this record base.
    TYPE_ID = b"urn:nfc:ext:ultimaker.nl:stat"

    ##< The unit types that can be used for the materials
    MATERIAL_UNIT_UNUSED = 0
    MATERIAL_QUANTITY_LENGTH_MM = 1
    MATERIAL_QUANTITY_MASS_GR = 2
    MATERIAL_QUANTITY_VOLUME_CM3 = 3

    def __init__(self, ndef_container):
        super().__init__(ndef_container)
        self._version = 0
        self._compatibility_version = 0
        self._material_unit = StatRecordBase.MATERIAL_UNIT_UNUSED
        self._material_total = 0
        self._material_remaining = 0
        self._total_usage_duration = 0
        self.__material_properties = None
        self.__temp_usage_duration = 0

    @classmethod
    def getType(cls):
        return StatRecordBase.TYPE_ID

    ## @brief Gets the version of this data record
    #  @return The version as an integer
    def getVersion(self):
        return self._version

    ## @brief Gets the compatible version of this data record
    #  @return The version as an integer
    def getCompatibilityVersion(self):
        return self._compatibility_version

    ## @brief Gets the material unit used in this data record
    #  @return The material unit identifier
    def getMaterialUnit(self):
        return self._material_unit

    ## @brief Gets the total amount of material (length)
    #  @return Returns the total material (in mm)
    def getMaterialTotal(self):
        return self.__convertTo(self._material_total, self.MATERIAL_QUANTITY_LENGTH_MM)

    ## @brief Gets the material_remaining (length)
    #  @return Returns the material remaining (in mm)
    def getMaterialRemaining(self):
        return self.__convertTo(self._material_remaining, self.MATERIAL_QUANTITY_LENGTH_MM)

    ## @brief Gets the total usage duration (time)
    #  @return Returns the accumulated duration in seconds
    def getTotalUsageDuration(self):
        return self._total_usage_duration

    ## @brief Sets the total usage duration (time)
    #  @param usage_duration The duration in seconds
    def setTotalUsageDuration(self, usage_duration):
        self._total_usage_duration = usage_duration

    ## @brief Sets the material properties to be used for conversions
    #  @param material_properties The material properties as a dictionary
    def setMaterialProperties(self, material_properties):
        self.__material_properties = material_properties

    ## @brief Updates the remaining amount by converting the given amount in mm to mg
    #  @param material_remaining Remaining length in mm
    #  @param elapsed_time_seconds The elapsed time since the last time the remaining amount was set
    def setRemainingMaterial(self, material_remaining, elapsed_time_in_seconds):
        assert self.__material_properties is not None, "Programmer error: material properties not set but required for conversion"
        self._material_remaining = UnitConverter(**self.__material_properties).convertFromLengthToMass(material_remaining)
        self.__temp_usage_duration += elapsed_time_in_seconds

    ## @brief Prepare to write the record, in which case there need some handling to be taken care off with the data before actual writing
    def prepareForWriting(self):
        self._total_usage_duration += self.__temp_usage_duration
        self.__temp_usage_duration = 0.0

    ## @brief Converts the given amount to the destination unit
    #  @param amount The amount to convert
    #  @param convert_to_unit To which unit the value should be converted
    #  @return The converted amount in the convert_to_unit type
    def __convertTo(self, amount, convert_to_unit=MATERIAL_QUANTITY_LENGTH_MM):
        assert self.__material_properties is not None, "Programmer error: material properties not set but required for conversion"

        self.__verifyConversionImplemented(convert_to_unit)
        if self._material_unit == self.MATERIAL_UNIT_UNUSED or self._material_unit == convert_to_unit:
            return amount

        if self._material_unit == StatRecordBase.MATERIAL_QUANTITY_LENGTH_MM:
            return UnitConverter(**self.__material_properties).convertFromLengthToLength(amount)

        if self._material_unit == StatRecordBase.MATERIAL_QUANTITY_MASS_GR:
            return UnitConverter(**self.__material_properties).convertFromMassToLength(amount)

        if self._material_unit == StatRecordBase.MATERIAL_QUANTITY_VOLUME_CM3:
           return UnitConverter(**self.__material_properties).convertFromVolumeToLength(amount)

    ## @brief Verify if the given unit is one of the valid defined unit types
    #         At this time we only convert to length
    #  Throws an assert error if this is not the case
    def __verifyConversionImplemented(self, unit):
        assert unit in [self.MATERIAL_QUANTITY_LENGTH_MM], "Programmer error: the conversion to %d is not yet supported!" % (unit)