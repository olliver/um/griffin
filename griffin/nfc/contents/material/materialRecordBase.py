from griffin.nfc.contents.recordBase import RecordBase

import uuid


# Base class for material records, this contains all data that a material record could contain,
# and will fill it will basic "null" data. Specific versions of the material record will fill this record with the proper data.
# This base class ensures a uniform interface to all versions of the material record.
class MaterialRecordBase(RecordBase):
    ##< The NDEF record type that should be used for this record base.
    TYPE_ID = b"urn:nfc:ext:ultimaker.nl:material"
    ##< Default UUID in case of no material
    NO_MATERIAL = uuid.UUID('00000000-0000-0000-0000-000000000000')

    def __init__(self, ndef_container):
        super().__init__(ndef_container)
        self._version = 0
        self._compatibility_version = 0
        self._serial_number = None
        self._manufacturing_timestamp = 0
        self._material_id = MaterialRecordBase.NO_MATERIAL
        self._manufacturer_id = 0
        self._programming_station_id = 0
        self._batch_code = ""

    @classmethod
    def getType(cls):
        return MaterialRecordBase.TYPE_ID

    def getVersion(self):
        return self._version

    def getCompatibilityVersion(self):
        return self._compatibility_version

    def getSerialNumber(self):
        return self._serial_number

    def getManufacturingTimestamp(self):
        return self._manufacturing_timestamp

    def getMaterialId(self):
        return self._material_id
    
    def getManufacturerId(self):
        return self._manufacturer_id
    
    def getProgrammingStationId(self):
        return self._programming_station_id

    def getBatchCode(self):
        return self._batch_code
