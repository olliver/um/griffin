from .materialRecordBase import MaterialRecordBase
import struct
import uuid


# Version 0 material record according to specs at:
# https://docs.google.com/document/d/1QlCaHTPI9n3rWuUkVwX_0l9z5IzrI1hfUMJQ5V2Lirk/edit
class MaterialRecordV0(MaterialRecordBase):
    ##< Record size, see specificationed above
    __RECORD_SIZE = 106

    def __init__(self, ndef_container):
        super().__init__(ndef_container)

        self._serial_number = self._data[2:16]
        self._manufacturing_timestamp = struct.unpack(">Q", self._data[16:24])
        self._material_id = uuid.UUID(bytes=self._data[24:40])
        self._programming_station_id = struct.unpack(">H", self._data[40:42])
        self._batch_code = self._data[42:106].decode("utf-8").split("\x00")[0]

    def getData(self):
        return self._data

    def getRecordSize(self):
        return MaterialRecordV0.__RECORD_SIZE