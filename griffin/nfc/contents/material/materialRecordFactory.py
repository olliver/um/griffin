from griffin.nfc.contents.recordFactoryBase import RecordFactoryBase
from .materialRecordV0 import MaterialRecordV0

import logging

log = logging.getLogger(__name__.split(".")[-1])


class MaterialRecordFactory(RecordFactoryBase):
    @classmethod
    def _getRecordClasses(cls):
        return { 0: MaterialRecordV0 }