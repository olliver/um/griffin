

## Abstract class to define the record interface to be used
class RecordBase:

    ## @brief Initializes the record base
    #  This includes calculating the payload data offset and verifying the payload datasize.
    #  If this is correct, it will retrieve the version and compatibility_version
    #  If the payload is not of the required size, an exceptionm will be thrown
    def __init__(self, ndef_container):
        (self.__data_start_index, self.__data_length) = ndef_container.calculatePayloadPositionAndSize()
        # Take into account the actual position of the ndef_record as a whole
        self.__data_start_index += ndef_container.getStartIndex()

        self._data = ndef_container.getPayload()
        assert isinstance(self._data, bytes)
        if len(self._data) < self.getRecordSize():
            raise ValueError("Payload data not long enough for %s (%d < %d)" % (self.__class__.__name__, len(self._data), self.getRecordSize()))

        self._version = self._data[0]
        self._compatibility_version = self._data[1]


    ## @brief Get start index for the data record
    #  @return Returns the starting index of the byte for this record
    def getStartIndex(self):
        return self.__data_start_index

    ## @brief Get the lenght of the data in bytes
    #  #return Returns the length
    def getLength(self):
        return self.__data_length

    ## @brief Gets the bytes array with the encoded data
    #  @return Returns the data bytes
    def getData(self):
        raise NotImplementedError

    ## @brief Determines the unique type of this type of record
    #  @return Returns a byte string for identifcation
    #  @pure
    @classmethod
    def getType(cls):
        raise NotImplementedError

    ## @brief Gets the size
    #  @return Returns the record size of the data
    def getRecordSize(self):
        raise NotImplementedError

    ## @brief Allow the record to manipulate data just before the actual writing would take place
    def prepareForWriting(self):
        pass