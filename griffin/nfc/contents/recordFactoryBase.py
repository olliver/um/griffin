from .ndefContainer import NDefContainer
from .recordBase import RecordBase

import logging

log = logging.getLogger(__name__.split(".")[-1])


class RecordFactoryBase:
    def __init__(self):
        pass

    ## @brief Parse a class derived from record base. Will look at the first 2 bytes of the payload to see which version we can load.
    #  @param ndef_container The complete ndef_record to be loaded into the record
    #  @return Returns a subclass of the RecordBasse class which is filled with the data, or None if the data or record class is invalid.
    @classmethod
    def parseRecord(cls, ndef_container):
        assert isinstance(ndef_container, NDefContainer), "The data passed on must be an instance of the NDefContainer"
        data = ndef_container.getPayload()
        if len(data) < 2:
            return None
        record_class = cls.getType()
        try:
            record_class = cls.___determineClassRecord(data)
            if record_class is not None:
                return record_class(ndef_container)
        except ValueError as e:
            log.error("Error while parsing tag data for type %s: %s", record_class, e)
        # Invalid data or not a correct record class definition found
        return None

    ## @brief Uses the record base class to get the type
    #  @return Returns the record identifcation type
    @classmethod
    def getType(cls):
        classes = cls._getRecordClasses()
        assert isinstance(classes, dict), "getRecordClasses did not return a dict!"
        # Using the first index for the record base should suffice, since they all should be derived from the same class
        return next(iter(classes.values())).getType()

    ## @brief Gets the base record classes, abstract function needs to be implemented by derived classes
    #  @return Returns a dictionary of version as the key and the value being a class name which should be a subclass of RecordBase
    #  @pure
    @classmethod
    def _getRecordClasses(cls):
        raise NotImplementedError

    ## @brief Gets the most recent of the record class to be used, based on the version/compatible_version that can be used to handle the data
    ## @param data The data which contains the ndef record data
    ## @return Returns the record class name to be used for handling the data
    @classmethod
    def ___determineClassRecord(cls, data):
        record_version = data[0]
        compatible_version = data[1]

        classes = cls._getRecordClasses()
        assert isinstance(classes, dict), "getRecordClasses did not return a dict!"
        version = record_version
        # Find the version, or last compatible version for this record class
        while version >= compatible_version and version >= 0:
            log.debug("Checking for version: %d", version)
            if version in classes:
                record_class  = classes[version]
                assert issubclass(record_class, RecordBase), "The referred class is not derived from RecordBase"
                log.debug("Record class found: %s", record_class)
                return record_class
            version -= 1

        # No compatible one found!
        log.error("No record implementation found for type %s with version %d and compatibility version %d", classes[0], version, compatible_version)
        return None
