import ctypes

###
### libnfc ctype wrappers.
### Here be some dragons, as ctypes interfaces directly with C libraries.
### This code was hand-written by looking at the header files from libnfc 1.7.1
### See www.libnfc.org/api/ for documentation of these functions.
###
### This file does not match coding standards due to being a direct interface with libnfc.
###


## Find libnfc, which comes in the form libnfc.so.5, as we need API version 5.
__libnfc = ctypes.cdll.LoadLibrary("libnfc.so.5")

## Types used by the nfc library, defined in nfc-types.h
class nfc_context(ctypes.Structure):
    _fields_ = []

nfc_context_ptr = ctypes.POINTER(nfc_context)

class nfc_device(ctypes.Structure):
    _fields_ = []

nfc_device_ptr = ctypes.POINTER(nfc_device)

nfc_connstring = ctypes.c_char * 1024

class nfc_iso14443a_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("abtAtqa",  ctypes.c_int8 * 2), 
        ("btSak",  ctypes.c_uint8), 
        ("szUidLen",  ctypes.c_size_t), 
        ("abtUid",  ctypes.c_uint8 * 10), 
        ("szAtsLen",  ctypes.c_size_t), 
        ("abtAts",  ctypes.c_uint8 * 254), 
    ]

class nfc_felica_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("szLen",  ctypes.c_size_t), 
        ("btResCode",  ctypes.c_int8), 
        ("abtId",  ctypes.c_uint8 * 8), 
        ("abtId",  ctypes.c_uint8 * 8), 
        ("abtId",  ctypes.c_uint8 * 2), 
    ]

class nfc_iso14443b_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("abtPupi",  ctypes.c_uint8 * 4), 
        ("abtApplicationData",  ctypes.c_uint8 * 4), 
        ("abtProtocolInfo",  ctypes.c_uint8 * 3), 
        ("ui8CardIdentifier",  ctypes.c_uint8), 
    ]

class nfc_iso14443bi_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("abtDIV",  ctypes.c_int8 * 4), 
        ("btVerLog",  ctypes.c_int8), 
        ("btConfig",  ctypes.c_int8), 
        ("szAtrLen",  ctypes.c_size_t), 
        ("abtAtr",  ctypes.c_int8 * 33), 
    ]

class nfc_iso14443b2sr_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("abtUID",  ctypes.c_int8 * 8), 
        ("btId",  ctypes.c_int8 * 4), 
    ]

class nfc_iso14443b2ct_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("abtUID",  ctypes.c_int8 * 4), 
        ("btProdCode",  ctypes.c_int8), 
        ("btFabCode",  ctypes.c_int8), 
    ]

class nfc_jewel_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("btSensRes",  ctypes.c_int8 * 2), 
        ("btId",  ctypes.c_int8 * 4), 
    ]

class nfc_dep_info(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ("abtNFCID3",  ctypes.c_uint8 * 10), 
        ("btDID",  ctypes.c_uint8), 
        ("btBS",  ctypes.c_uint8), 
        ("btBR",  ctypes.c_uint8), 
        ("btTO",  ctypes.c_uint8), 
        ("btPP",  ctypes.c_uint8), 
        ("abtGB",  ctypes.c_uint8 * 48), 
        ("szGB",  ctypes.c_size_t), 
        ("ndm",  ctypes.c_int), 
    ]

class nfc_target_info(ctypes.Union):
    _fields_ = [
        ('nai', nfc_iso14443a_info),
        ('nfi', nfc_felica_info),
        ('nbi', nfc_iso14443b_info),
        ('nii', nfc_iso14443bi_info),
        ('nsi', nfc_iso14443b2sr_info),
        ('nci', nfc_iso14443b2ct_info),
        ('nji', nfc_jewel_info),
        ('ndi', nfc_dep_info),
    ]

class nfc_modulation(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ('nmt', ctypes.c_int),
        ('nbr', ctypes.c_int),
    ]

class nfc_target(ctypes.Structure):
    _pack_ = 1
    _fields_ = [
        ('nti', nfc_target_info),
        ('nm', nfc_modulation),
    ]

## Enums used by the nfc library, defined in nfc-types.h
# NFC device properties
NP_TIMEOUT_COMMAND = 0
NP_TIMEOUT_ATR = 1
NP_TIMEOUT_COM = 2
NP_HANDLE_CRC = 3
NP_HANDLE_PARITY = 4
NP_ACTIVATE_FIELD = 5
NP_ACTIVATE_CRYPTO1 = 6
NP_INFINITE_SELECT = 7
NP_ACCEPT_INVALID_FRAMES = 8
NP_ACCEPT_MULTIPLE_FRAMES = 9
NP_AUTO_ISO14443_4 = 10
NP_EASY_FRAMING = 11
NP_FORCE_ISO14443_A = 12
NP_FORCE_ISO14443_B = 13
NP_FORCE_SPEED_106 = 14

# NFC Modulation baudrate options
NBR_UNDEFINED = 0,
NBR_106 = 1
NBR_212 = 2
NBR_424 = 3
NBR_847 = 4

# NFC Modulation types
NMT_ISO14443A = 1
NMT_JEWEL = 2
NMT_ISO14443B = 3
NMT_ISO14443BI = 4
NMT_ISO14443B2SR = 5
NMT_ISO14443B2CT = 6
NMT_FELICA = 7
NMT_DEP = 8


## Functions defined by nfc.h

nfc_init = __libnfc.nfc_init
nfc_init.argtypes = [ctypes.POINTER(nfc_context_ptr)]
nfc_init.restype = None

nfc_exit = __libnfc.nfc_exit
nfc_exit.argtypes = [nfc_context_ptr]
nfc_exit.restype = None

nfc_open = __libnfc.nfc_open
nfc_open.argtypes = [nfc_context_ptr, nfc_connstring]
nfc_open.restype = nfc_device_ptr

nfc_close = __libnfc.nfc_close
nfc_close.argtypes = [nfc_device_ptr]
nfc_close.restype = None

nfc_abort_command = __libnfc.nfc_abort_command
nfc_abort_command.argtypes = [nfc_device_ptr]
nfc_abort_command.restype = ctypes.c_int

nfc_idle = __libnfc.nfc_idle
nfc_idle.argtypes = [nfc_device_ptr]
nfc_idle.restype = ctypes.c_int

nfc_initiator_init = __libnfc.nfc_initiator_init
nfc_initiator_init.argtypes = [nfc_device_ptr]
nfc_initiator_init.restype = ctypes.c_int

nfc_device_set_property_bool = __libnfc.nfc_device_set_property_bool
nfc_device_set_property_bool.argtypes = [nfc_device_ptr, ctypes.c_int, ctypes.c_bool]
nfc_device_set_property_bool.restype = ctypes.c_int

nfc_initiator_list_passive_targets = __libnfc.nfc_initiator_list_passive_targets
nfc_initiator_list_passive_targets.argtypes = [nfc_device_ptr, nfc_modulation, ctypes.POINTER(nfc_target), ctypes.c_size_t]
nfc_initiator_list_passive_targets.restype = ctypes.c_int

nfc_initiator_select_passive_target = __libnfc.nfc_initiator_select_passive_target
nfc_initiator_select_passive_target.argtypes = [nfc_device_ptr, nfc_modulation, ctypes.POINTER(ctypes.c_uint8), ctypes.c_size_t, ctypes.POINTER(nfc_target)]
nfc_initiator_select_passive_target.restype = ctypes.c_int

nfc_initiator_transceive_bytes = __libnfc.nfc_initiator_transceive_bytes
nfc_initiator_transceive_bytes.argtypes = [nfc_device_ptr, ctypes.POINTER(ctypes.c_uint8), ctypes.c_size_t, ctypes.POINTER(ctypes.c_uint8), ctypes.c_size_t, ctypes.c_int]
nfc_initiator_transceive_bytes.restype = ctypes.c_int
