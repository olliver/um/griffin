

## nfc.DummyDevice - This class is the dummy variant of the nfc.Device.
#  it has the same functions, but does not talk to actual hardware.
#  it is used to start the service even when the NFC hardware is failing.
class DummyDevice:
    # Initialize the Device class.
    # @param connstring: A hardware connection string. Example: "pn532_i2c:/dev/i2c-2" for the PN532 hardware in the Ultimaker3. See libnfc documentation for details.
    # @param context: An instance of the nfc.context class, or None if this device must create it's own context. If multiple Devices are constructed, the context should be shared. With a single hardware endpoint you should leave this to None.
    def __init__(self):
        pass

    # Close the current device. This pretty much makes this object useless from this point on.
    def close(self):
        pass

    # Abort the current active command to the hardware.
    def abortCommand(self):
        pass

    # Put the hardware into idle mode. Can be called after "listPassiveTargets" to put the hardware back into idle.
    # As "listPassiveTargets" will enable the transmitter on the RFID hardware.
    def idle(self):
        pass

    # Get all the UIDs of the tags in range of the RFID hardware.
    def listPassiveTargets(self, max_targets=32):
        return []

    # Select a target for data transfer.
    # @return bool: True if the selection was succesful, False if not (the tag could have disappeared)
    def selectPassiveTarget(self, target_uid):
        return False

    # Send and receive data from an RFID tag. This is a raw data transfer.
    # @param send_data: A bytes object containing the data to be send.
    # @param receive_data_length: Integer which is the amount of data expected back from the tag.
    # @return bytes object containing the resulting data, or None if a transmission error occured. Calling code needs to handle the None, as tags can disappear from the field at any time.
    def transceiveToSelected(self, send_data, receive_data_length, timeout=500):
        return None
