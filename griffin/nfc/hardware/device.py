from . import _nfc
import ctypes
import logging
import time
from threading import Lock
from .context import Context


log = logging.getLogger(__name__.split(".")[-1])


## nfc.Device - This class is the interface to NFC hardware, and uses libnfc to communicate with hardware.
#  This class allows detection of RFID tags and sending raw commands to these tags.
#  Only a limited subset of libnfc is implemented. Which is the most essential part to communicate with the tags we are using.
#  This device should only be opened once, hence a locking mechanism on creation is implemented.
class Device:
    ## The static lock which handles that only one thread can access the device
    __lock = Lock()

    # Initialize the Device class.
    # @param connstring: A hardware connection string. Example: "pn532_i2c:/dev/i2c-2" for the PN532 hardware in
    #                 the Ultimaker3. See libnfc documentation for details.
    # @param context: An instance of the nfc.context class, or None if this device must create its own context.
    #                 If multiple Devices are constructed, the context should be shared. With a single hardware
    #                 endpoint you should leave this to None.
    def __init__(self, connstring, context=None):
        self.__pnd = None

        log.debug("Acquiring device lock")
        if self.__lock.acquire(False) == False:
            log.debug("Device, already in use, waiting for release...")
            self.__lock.acquire()

        # Context should be None or an instance of the Context class
        assert context is None or isinstance(context, Context)
        assert len(connstring) < ctypes.sizeof(_nfc.nfc_connstring)

        self.__context = context  # Store the context as reference in a local member. We want to prevent the context from being garbage collected while the device is active.

        # If no context is given as parameter, create our own private one.
        if self.__context is None:
            self.__context = Context()

        # Convert the connstring to the fixed size
        s = _nfc.nfc_connstring()
        connstring = connstring.encode()
        for n in range(len(connstring)):
            s[n] = connstring[n]
        # Opening/closing needs a little bit of sleep in between, so make sure it does. Requirement by Nfc hardware
        time.sleep(0.1)
        log.debug("Opening NFC hardware: %s", connstring)
        self.__pnd = _nfc.nfc_open(self.__context.getContextCTypesPointer(), s)
        if not self.__pnd:
            self.__lock.release()
            self.__pnd = None
            raise OSError("Failed to open NFC device: %s" % (connstring))

        # Create our modulation configuration.
        # This implementation only supports ISO14443A communication to simplify the API.
        self.__nm = _nfc.nfc_modulation()
        self.__nm.nmt = _nfc.NMT_ISO14443A
        self.__nm.nbr = _nfc.NBR_106

    # When the Device is collected for garbage, close the actual device reference in libnfc.
    def __del__(self):
        self.close()

    # Close the current device. This pretty much makes this object useless from this point on.
    def close(self):
        if self.__pnd:
            _nfc.nfc_close(self.__pnd)
            self.__pnd = None

            log.debug("Releasing device lock")
            self.__lock.release()

    # Abort the current active command to the hardware.
    def abortCommand(self):
        if not self.__pnd:
            return
        _nfc.nfc_abort_command(self.__pnd)

    # Put the hardware into idle mode. Can be called after "listPassiveTargets" to put the hardware back into idle.
    # As "listPassiveTargets" will enable the transmitter on the RFID hardware.
    def idle(self):
        if not self.__pnd:
            return
        _nfc.nfc_idle(self.__pnd)

    # Get all the UIDs of the tags in range of the RFID hardware.
    def listPassiveTargets(self, max_targets=32):
        if not self.__pnd:
            return []

        targets = (_nfc.nfc_target * max_targets)()

        _nfc.nfc_initiator_init(self.__pnd)
        _nfc.nfc_device_set_property_bool(self.__pnd, _nfc.NP_INFINITE_SELECT, False)
        count = _nfc.nfc_initiator_list_passive_targets(self.__pnd, self.__nm, targets, max_targets)
        results = []
        for n in range(count):
            results.append(bytes(targets[n].nti.nai.abtUid[0:targets[n].nti.nai.szUidLen]))
        log.debug("Seeing NFC tags: %s", results)
        return results

    # Select a target for data transfer.
    # @return bool: True if the selection was successful, False if not (the tag could have disappeared)
    def selectPassiveTarget(self, target_uid):
        if not self.__pnd:
            return False
        result_data = _nfc.nfc_target()
        uid = (ctypes.c_uint8 * len(target_uid))()
        uid[:] = target_uid
        log.debug("Trying to select NFC UID: %s", target_uid)
        count = _nfc.nfc_initiator_select_passive_target(self.__pnd, self.__nm, uid, len(target_uid), ctypes.byref(result_data))
        log.debug("Resulting tag count: %d", count)
        return count == 1

    # Send and receive data from an RFID tag. This is a raw data transfer.
    # @param send_data: A bytes object containing the data to be send.
    # @param receive_data_length: Integer which is the amount of data expected back from the tag.
    # @return bytes Object containing the resulting data, or None if a transmission error occurred.
    # Calling code needs to handle the None, as tags can disappear from the field at any time.
    #
    # In case it the functionm is called for writing the result code works differently:
    # @return bytes Returns no bytes b'' or None if a transmission error occurred
    def transceiveToSelected(self, send_data, receive_data_length, timeout=500):
        receive_buffer = (ctypes.c_uint8 * receive_data_length)()
        send_buffer = (ctypes.c_uint8 * len(send_data))()
        send_buffer[:] = send_data
        log.debug("Tranceiving data to NFC: %s", send_data)
        result = _nfc.nfc_initiator_transceive_bytes(self.__pnd, send_buffer, len(send_buffer), receive_buffer, len(receive_buffer), timeout)
        log.debug("Result (raw): %r", result)
        if result < 0:
            log.error("Nfc Transceive error... Error code: %d", result)
            return None
        return bytes(receive_buffer[:result])
