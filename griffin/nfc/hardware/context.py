from . import _nfc

## Simple wrapper around the nfc_context structure and related functions.
#  Which manages the libnfc nfc_context state. We need an nfc_context for an nfc_device
#  We simply use this object to manage it, so we can properly garbage collect the context.
class Context:
    def __init__(self):
        self._context = _nfc.nfc_context_ptr()
        _nfc.nfc_init(self._context)
    
    def __del__(self):
        _nfc.nfc_exit(self._context)

    def getContextCTypesPointer(self):
        return self._context
