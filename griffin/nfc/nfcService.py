import logging
import binascii
from griffin import dbusif

from .nfcController import NfcController

log = logging.getLogger(__name__.split(".")[-1])


## NFC service DBus object.
# This ServiceObject will create the DBus interface which the rest of the system can use to talk to the NFC service.
# This object acts as an interface wrapper around the rest of the NFC service.
class NfcService(dbusif.ServiceObject):
    def __init__(self):
        super().__init__("nfc")
        self.__controller = NfcController()

    # Retrieve the linked tag UID to a hotend.
    # @return: a string containing the hex code of the UID that is linked to a hotend. Or an empty string if no tag is linked.
    @dbusif.method("i", "s")
    def getLinkedTagUID(self, hotend_nr):
        hotend_info = self.__controller.getHotendInfo(hotend_nr)
        if hotend_info is None:
            return ""
        return binascii.hexlify(hotend_info.getLinkedTagUid()).decode('ascii', 'ignore')

    # @return: Tuple containing the remaining material amount and the original full material amount. These values are -1 when the amounts are not known.
    #           The returned values are in mm filament.
    @dbusif.method("i", "dd")
    def getMaterialAmount(self, hotend_nr):
        hotend_info = self.__controller.getHotendInfo(hotend_nr)
        if hotend_info is None:
            return -1.0, -1.0
        current, full = hotend_info.getMaterialAmount()
        return current, full

    ## @brief The callback function to register which needs to be called when a factory reset is issued
    #  @param reset_type One of the defined values from FactoryReset which would indicate a hard or soft reset
    @dbusif.method("s", "")
    def resetSettings(self, reset_type):
        self.__controller.resetToFactorySettings(reset_type)