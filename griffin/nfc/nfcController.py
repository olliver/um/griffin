from .contents.material.materialRecordFactory import MaterialRecordFactory
from .contents.stat.statRecordFactory import StatRecordFactory
from .nfcTag import NfcTag
from .hardware.device import Device
from .hardware.dummyDevice import DummyDevice
from .nfcPrinterHotendInfo import NfcPrinterHotendInfo

from griffin.thread import Thread
from griffin.thread import Event
from griffin import dbusif
from griffin.preferences.registryFile import RegistryFile

from binascii import hexlify
from threading import Lock
import signal
import logging
import time

log = logging.getLogger(__name__.split(".")[-1])


## Main NFC controller.
#  Monitors the RFID hardware for RFID chips to appear and disappear.
#  Keep track of which RFID tag belongs to which hotend.
class NfcController:
    ##< The default Nfc device: using the i2c bus
    __DEFAULT_CONNECTION_STRING = "pn532_i2c:/dev/i2c-2"
    #__DEFAULT_CONNECTION_STRING = "pn532_uart:/dev/ttyUSB0"

    ##< Registry file for material detected per hotend
    __HOTEND_TAG_LINK_STORAGE = "nfc_hotend_tag_links.json"

    ##< Printer procedures to connect to to handle detection
    __MATERIAL_WIZARDS = ("LOAD_MATERIAL_WIZARD", "CHANGE_MATERIAL_WIZARD", "SELECT_MATERIAL_WIZARD")
    ##< Printer procedures to connect to to handle detection of the special case
    __NO_MATERIAL_WAIT_WIZARDS = ("WELCOME_SETUP_WIZARD",)

    ##< Measured by trying: reading 1 tag is about 2 seconds, so 2 tags is the double time
    __TAG_VISIBILITY_TIMEOUT = 5.0

    ##< To prevent wearing of tags, tags are only updated every 5 minutes
    __TAG_UPDATE_TIMEOUT = 5 * 60

    def __init__(self):
        self.__device = None

        self.__printer_service = dbusif.RemoteObject("printer")
        self.__printer_service.connectSignal("onProcedureStart", self.__onProcedureStart)
        self.__printer_service.connectSignal("onProcedureNextStep", self.__onProcedureNextStep)
        self.__printer_service.connectSignal("onProcedureFinished", self.__onProcedureFinished)

        # Managing detected materials per hotend
        hotend_count = self.__printer_service.getProperty("hotend_count")
        self.__tag_link_registry = RegistryFile(NfcController.__HOTEND_TAG_LINK_STORAGE)
        self.__hotends = []
        for index in range(0, hotend_count):
            self.__hotends.append(NfcPrinterHotendInfo(self, self.__tag_link_registry, index))
        self.__visible_tags = {}

        # Handling of updating tags with new data
        self.__tag_update_cache_lock = Lock()
        self.__tag_update_cache = {}
        # Keep time out per hotend!
        self.__tag_update_cache_update_time = [None] * hotend_count

        # Variables for handling of (un)loading materials
        self.__new_material_procedure = None
        self.__no_material_procedure = None
        self.__scan_for_new_tags_to_link = Event()

        # Thread to handle tag manipulations (both reading and writing)
        self.__run_update_thread = True
        Thread("NfcUpdate", self.__updateThread).start()

        for procedure, step in self.__printer_service.getActiveProcedures():
            self.__onProcedureNextStep(procedure, step)

        signal.signal(signal.SIGTERM, self.__handleTerminateSignal)

        ## Add callback to dbus printer service which implements the call to the resetToFactorySettings
        dbusif.RemoteObject("system").addFactoryResetCallback("nfc", "resetSettings")

    ## @brief Implementation of the factory reset callback
    #  @param reset_type Specified which type of reset is requested
    def resetToFactorySettings(self, reset_type):
        self.__tag_link_registry.erase()

    # Get a hotend info object.
    # @param hotend_index: the index of the hotend you want info off.
    # @return a NfcPrinterHotendInfo object, or None if the index was out of range.
    def getHotendInfo(self, hotend_index):
        try:
            return self.__hotends[hotend_index]
        except IndexError:
            return None

    ## @brief Marks the tag to be written back at some point in the (near) future
    #  @param tag Identifies the tag to be written to
    #  @param record Identifies the record to be updated
    def writeBack(self, hotend_index, tag, record):
        with self.__tag_update_cache_lock:
            log.info("Queueing tag for hotend %d, tag: %r, record: %r", hotend_index, tag, record)
            self.__tag_update_cache[hotend_index] = { "tag": tag, "record": record }

    # Handle procedure starts the same as procedure next steps. In case the first step is a USER_SELECT_MATERIAL step.
    def __onProcedureStart(self, procedure, step):
        self.__onProcedureNextStep(procedure, step)

    # When one of the change/load/select material procedures runs, we monitor for the USER_SELECT_MATERIAL step.
    # During this step we want to link new material to our hotends. Outside of these steps we do not want to monitor for NFC material changes.
    def __onProcedureNextStep(self, procedure, step):
        if procedure in NfcController.__MATERIAL_WIZARDS:
            if step == "USER_SELECT_MATERIAL":
                self.__new_material_procedure = procedure
                log.info("Start NFC scanning because USER_SELECT_MATERIAL procedure step activated")
            else:
                self.__new_material_procedure = None
        if procedure in NfcController.__NO_MATERIAL_WAIT_WIZARDS:
            if step == "CHECK_RFID_TAGS_PRESENT":
                self.__no_material_procedure = procedure
                log.info("Start NFC scanning because CHECK_RFID_TAGS_PRESENT procedure step activated")
            else:
                self.__no_material_procedure = None
        self.__updateScanForNewTagsFlag()

    # When the change/load/select material procedure finishes, we no longer want to monitor for NFC tag updates.
    # This can happen if USER_SELECT_MATERIAL was the last step in the procedure, or if the procedure is aborted in this step.
    def __onProcedureFinished(self, procedure):
        if procedure in NfcController.__MATERIAL_WIZARDS:
            self.__new_material_procedure = None
        if procedure in NfcController.__NO_MATERIAL_WAIT_WIZARDS:
            self.__no_material_procedure = None
        self.__updateScanForNewTagsFlag()

    # Update the __scan_for_new_tags_to_link flag according to the procedures we are watching.
    # Is is called when procedures change from steps or finish.
    def __updateScanForNewTagsFlag(self):
        if self.__new_material_procedure is None and self.__no_material_procedure is None:
            if self.__scan_for_new_tags_to_link.is_set():
                log.info("Stop NFC scanning because other step started")
            self.__scan_for_new_tags_to_link.clear()
        else:
            self.__scan_for_new_tags_to_link.set()

    # Main update thread, this thread runs for the lifetime of the controller.
    # Note: This thread needs to shutdown properly to prevent problems with the I2C RFID hardware.
    def __updateThread(self):
        try:
            log.debug("__updateThread: INITIAL NFC scanner start scanning")
            self.__detectTags()
            while self.__run_update_thread:
                if self.__scan_for_new_tags_to_link.wait(1):
                    log.debug("__updateThread: NFC scanner start scanning")
                    self.__detectTags()
                self.__updateTags()
        except:
            log.exception("Nfc Update Thread crashed")

        # Abort the main loop, which exits the service as this will exit the main thread.
        dbusif.abortMainLoop()

    ## @brief The actual implementation of the update thread where all the work is being done when it comes to scanning / reading tags
    def __detectTags(self):
        new_material_tags = self.__scanForNewTags()
        nr_of_tags = len(new_material_tags)

        if self.__new_material_procedure is not None:
            if nr_of_tags == 1:
                log.debug("__detectTags: new_tags = %r. self.__new_material_procedure: %s ", [tag.getUid for tag in new_material_tags], self.__new_material_procedure)
                tag = new_material_tags[0]
                procedure_meta_data = self.__printer_service.getProcedureMetaData(self.__new_material_procedure)

                log.debug("__detectTags: procedure_meta_data['selected_material'] = %s ", procedure_meta_data["selected_material"])
                # Only try to link the material if there is no attempt to link a material yet.
                if procedure_meta_data["selected_material"] == "":
                    hotend_nr = procedure_meta_data["hotend_nr"]
                    log.debug("__detectTags: Found a single new tag: %s for hotend %d, requesting selection.", tag.getUid(), hotend_nr)
                    # Set the new tag as option for the hotend.
                    self.__hotends[hotend_nr].setNewTagRequest(tag)
                    # Send a request for this new tag to be used as material.
                    material_guid = tag.getRecordsByType(MaterialRecordFactory.getType())[0].getMaterialId()
                    log.debug("__detectTags: tag %s has material %s", tag.getUid(), material_guid)

                    self.__printer_service.messageProcedure(self.__new_material_procedure, "SELECT_MATERIAL:%s:RFID" % material_guid)
                else:
                    log.debug("__detectTags: procedure_meta_data['selected_material'] != '' so not selecting material on this tag.")
            elif nr_of_tags > 1:
                self.__printer_service.messageProcedure(self.__new_material_procedure, "TOO_MANY_MATERIALS_DETECTED")

        if self.__no_material_procedure is not None:
            if nr_of_tags == 0:
                self.__printer_service.messageProcedure(self.__no_material_procedure, "NO_MATERIALS_DETECTED")

    ## Handler for the SIGTERM signal,
    #  we use this handler to indicate the main thread that it needs to exit.
    #  @param: signum, signal number (signal.SIGTERM)
    #  @param: frame object or None. See signal module documentation.
    def __handleTerminateSignal(self, signum, frame):
        log.info("Got a request to terminate the service.")
        # Set a flag to the update thread that we need to shutdown. We need a graceful shutdown to prevent the NFC hardware from bugging out.
        self.__run_update_thread = False
        # Set our event so the main loop can exit.
        self.__scan_for_new_tags_to_link.set()

    ## Scan for new tags that we have not linked to anything yet.
    #  @return list of NfcTag objects.
    def __scanForNewTags(self):
        results = []
        tags = self.__scanForTags()
        log.debug("Tags found: %r", tags)
        for tag in tags:
            if not self.__isTagLinked(tag):
                results.append(tag)
        return results

    ## Check if a tag is linked to any hotend already.
    #  @param tag: a NfcTag object.
    #  @return bool indicating if the tag is linked.
    def __isTagLinked(self, tag):
        for hotend in self.__hotends:
            log.debug("Expecting link tag uid = %s", hotend.getLinkedTagUid())
            if tag.getUid() == hotend.getLinkedTagUid():
                if hotend.getLinkedTag() is None:
                    # If the UID for this tag is set in the hotend info, but the tag itself is not yet known, then
                    # set is. This happens when the system starts up and links need to be restored.
                    hotend.setLinkedTag(tag)
                    log.info("Tag %s found and linked to hotend: %r", tag.getUid(), hotend)
                return True
            if tag.getUid() == hotend.getUidForRequest():
                return True
        return False

    # Open the NFC hardware after aqcuiring a device lock.
    # This contains a simple retry as opening after restart sometimes fails.
    # Will use a dummy device in case the hardware device cannot be opened (yet, it would still be locked)
    def __openDevice(self):
        log.debug("Opening Nfc device: %s", NfcController.__DEFAULT_CONNECTION_STRING)
        try:
            self.__device = Device(NfcController.__DEFAULT_CONNECTION_STRING)
        except OSError:
            # When restarting this service, we occasionally get an OSError because the i2c NFC hardware was still in a partial communication.
            # When we re-try it will work.
            try:
                self.__device = Device(NfcController.__DEFAULT_CONNECTION_STRING)
            except OSError:
                log.error("Failed to open NFC hardware. Starting the service with dummy hardware, so the system does not fail as a whole.")
                self.__device = DummyDevice()

    ## @brief Close the device
    def __closeDevice(self):
        log.debug("Closing Nfc device")
        self.__device.close()

    ## Scan for tags.
    #  @return a list of NfcTag objects filled with records.
    def __scanForTags(self):
        try:
            self.__openDevice()
            for target in self.__device.listPassiveTargets():
                if target in self.__visible_tags:
                    self.__visible_tags[target].updateVisibleTime()
                else:
                    tag = NfcTag(target)
                    tag.addRecordFactory(MaterialRecordFactory)
                    tag.addRecordFactory(StatRecordFactory)
                    if tag.readTagData(self.__device):
                        self.__visible_tags[target] = tag
            self.__device.idle()
        finally:
            self.__closeDevice()

        # Remove all tags which have timed out. If we see them again after this, we will re-read the whole tag.
        for uid in list(self.__visible_tags.keys()):
            if self.__visible_tags[uid].getTimeAfterLastVisible() > NfcController.__TAG_VISIBILITY_TIMEOUT:
                log.info("Tag %s is past the visibility timeout, so remove from __visible tags. " %  uid)
                for hotend in self.__hotends:
                    if hotend.getUidForRequest() == uid:
                        hotend.setNewTagRequest(None)
                del self.__visible_tags[uid]

        # Return a list of valid material tags.
        results = []
        for tag in self.__visible_tags.values():
            if self.__isValidMaterialTag(tag):
                results.append(tag)
        return results

    ## @brief This handles the writing of tags (if/when deemed necessary)
    def __updateTags(self):
        with self.__tag_update_cache_lock:
            # Iterating over de keys allows the dictionary to change!
            hotend_indexes = list(self.__tag_update_cache.keys())
            for hotend_index in hotend_indexes:
                if not self.__shouldWriteTag(hotend_index):
                    continue
                data = self.__tag_update_cache[hotend_index]
                log.info("Writing tag: %r, record: %r for hotend index # %d", data["tag"], data["record"], hotend_index)
                if self.__writeTag(data["tag"], data["record"]):
                    del self.__tag_update_cache[hotend_index]
                self.__tag_update_cache_update_time[hotend_index] = time.monotonic()

    ## @brief Checks whether or not a tag should be written. This means the tag for the hotend index is cached and it's timeout is not passed the set threshold
    #  @param hotend_index The hotend index for which the tag should be written back
    #  @return Returns True if the tag should be written (exists and it's time), otherwise False!
    def __shouldWriteTag(self, hotend_index):
        if hotend_index not in self.__tag_update_cache:
            return False

        # If we do not have an last update time yet, we can update.
        if self.__tag_update_cache_update_time[hotend_index] is None:
            return True

        # If our current time is more then __TAG_UPDATE_TIMEOUT after the last update, we can write the tag.
        if time.monotonic() > self.__tag_update_cache_update_time[hotend_index] + NfcController.__TAG_UPDATE_TIMEOUT:
            return True

        return False

    ## @brief Wrapper function to write the stat record to the correct tag device
    #  @param tag The tag which contains the tag
    #  @param record The record to be written
    #  @return Returns the result of updating of the tag
    def __writeTag(self, tag, record):
        result = False
        try:
            self.__openDevice()
            record.prepareForWriting()
            result = tag.updateTagRecord(self.__device, record)
        finally:
            self.__closeDevice()
            return result

    # Check if a found tag is a valid RFID tag for our system.
    # This means it has the proper records we need, and these contain valid data.
    # @return bool: True if this tag is valid and can be used. False if this tag is not a proper tag according to our definitions.
    def __isValidMaterialTag(self, tag):
        # Check if we have a material record. If we do the material record serial number should match the UID of the tag.
        # Note that the UID in the tag is stored as a hex string, while it is returned as binary by the getUid function.
        material_records = tag.getRecordsByType(MaterialRecordFactory.getType())
        if not material_records:
            return False
        assert len(material_records) == 1, "There should only be one material record in this tag"
        material_record = material_records[0]
        if material_record.getSerialNumber().lower() != hexlify(tag.getUid()).lower():
            return False

        # Check if we have a stats record. We require a stats record
        stat_records = tag.getRecordsByType(StatRecordFactory.getType())
        if not stat_records:
            return False

        # We also need to have two of them!
        # TODO: EM-974 There is a need for an update/automated fix of the data (if possible)
        stat_record_count = len(stat_records)
        if stat_record_count != 2:
            log.error("Missing stat records (2 are required, got %d)", stat_record_count)
            return False

        # TODO: Signature check EM-751
        return True

