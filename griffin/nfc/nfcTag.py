from .contents.ndefContainer import NDefContainer
from .contents.recordBase import RecordBase
import time
import logging
import binascii

log = logging.getLogger(__name__.split(".")[-1])


## The NfcTag class keeps track of data about a single RFID tag.
#  This class handles the reading and converting of tag data to proper data records.
#  It does this in 2 steps, first is parses NDEF records. Then it can pass these NDEF records to record factories supplied to this tag.
#  These record factories parse the actual data and return an object to store. Or nothing if the data is not a valid record.
class NfcTag:
    ##< Start sector for reading first ndef record
    __START_SECTOR = 0x00
    ##< Start of sector number that contains the user data
    __START_USER_DATA_SECTOR = 4
    ##< Amount of bytes per sector that can be read/written at once
    __AMOUNT_OF_BYTES_PER_SECTOR = 4
    ##< Amount of sectors read at once
    __AMOUNT_OF_SECTORS_READ = 4
    ##< Amount of sectors to write at once. The official A2 command only allows for 1 sector to be written at any given time
    __AMOUNT_OF_SECTORS_WRITE = 1
    ##< The read command
    __NTAG_READ_COMMAND = 0x30
    ##< The write command
    __NTAG_WRITE_COMMAND = 0xA2


    # Create a new NfcTag object.
    # @param uid: a bytes object containing the UID of this tag. This is usually acquired from nfc.Device.listPassiveTargets
    def __init__(self, uid):
        self.__uid = uid
        self.__record_types = {}
        self.__records = []
        self.__last_visible_time = time.monotonic()
        ## The tag data byte array is a simplified implementation of the whole tag stored in a buffer
        ## This is required for writing back the records which are part of a tag.
        ## The records know their starting offset within this buffer, so we can calculate the correct aligned index and the actual
        ## number of records to write back to the tag
        self.__tag_data = bytearray()
        ##< When an error occurs during writing this tag, only log this every X times. This will increase over time.
        self.__write_error_threshold = 10
        ##< The nuber of write errors detected
        self.__write_errors_logged = 0

        # Update the last visible time, should be called when the UID of this tag is seen by hardware.
    def updateVisibleTime(self):
        self.__last_visible_time = time.monotonic()

    # @return: The amount of time sinds the last time this tag was visible, in seconds as a float.
    def getTimeAfterLastVisible(self):
        return time.monotonic() - self.__last_visible_time

    ## @brief Add a new record factory. This factory will be identified by its getType() and used to decode ndef records being read.
    #  The result will be stored in the records dictionary, which can be accessed with getRecords
    def addRecordFactory(self, factory_class):
        self.__record_types[factory_class.getType()] = factory_class

    # @return the UID of this tag as a bytes() object.
    def getUid(self):
        return self.__uid

    # Get the records found in this tag, after being parsed by the record factories generated during the readTagData function.
    # @return list containing the records
    def getRecords(self):
        return self.__records.copy()

    # Get the records found in the tag based on type
    # @return Returns the records that are of the specified type as a list as references
    def getRecordsByType(self, type_id):
        return [record for record in self.__records if record.getType() == type_id]

    # Read the NFC tag. This will will the records property by using the recordtype factories for known NDEF records.
    # Note: This function will return a successful read on a bad RFID tag, as we  can read it, but it does not contain valid data.
    #       The user should look at the records to see what data this tag contains.
    # @param hardware_device: An instance of the nfc.hardware.device.Device class which is used to talk to the NFC hardware.
    # @return bool: True if the reading was succesful, False if the tag could not be read.
    def readTagData(self, hardware_device):
        start_sector = NfcTag.__START_SECTOR
        max_bytes_to_read = NfcTag.__AMOUNT_OF_SECTORS_READ * NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR
        if not hardware_device.selectPassiveTarget(self.__uid):
            # Failed to select the tag, most likely it moved out of range.
            return False
        tag_contents = hardware_device.transceiveToSelected(bytes([NfcTag.__NTAG_READ_COMMAND, start_sector]), max_bytes_to_read)

        if tag_contents is None or len(tag_contents) < max_bytes_to_read:
            # Failed to read first 4 pages of data, most likely the tag moved out of the field.
            return False

        # Fill the tag data buffer
        self.__tag_data += tag_contents

        # The capability contained is an NFC standard, it's located at page 3 of the tag.
        capability_container = tag_contents[3 * 4:4 * 4]
        # The NFC forum standard defines the first byte of the capability container as 0xE1, if this is not the case.
        # We are not reading a valid tag for the system.
        if capability_container[0] != 0xE1:
            return True # Return true, as we can read the tag, but there is no valid data.
        # The NFC forum standard defines the second byte of the capability container as 0x10, which is the version number
        # of the capabilities. The high nibble defines the major, and the low nibble the minor version. Check if the major version is 1.
        if capability_container[1] & 0xF0 != 0x10:
            return True # Return true, as we can read the tag, but there is no valid data.

        # The size of the tag is defined in the capability container third byte. Specified as multiples of 8 bytes.
        # TODO: EM-840 incompatibility issue with standard need to be fixed (probably around here)
        tag_size = capability_container[2] * 8

        # We start reading at sector 4, which is where the user data starts.
        sector_number_to_read = NfcTag.__START_USER_DATA_SECTOR
        ndef = NDefContainer(sector_number_to_read * NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR)
        ndef_records = []
        while True:
            while ndef.getCurrentSize() < ndef.getRequiredSize():
                if ndef.getRequiredSize() > tag_size:
                    # If we require more size then the tag contains, we have an invalid NDef record,
                    # So skip this tag.
                    return True  # Return true, as we can read the tag, but there is no valid data.
                tag_contents = hardware_device.transceiveToSelected(bytes([NfcTag.__NTAG_READ_COMMAND, sector_number_to_read]), max_bytes_to_read)
                sector_number_to_read += NfcTag.__AMOUNT_OF_SECTORS_READ  # We read 4 sectors per read action.
                if not tag_contents or len(tag_contents) < max_bytes_to_read:
                    return False
                # Add to the tag data buffer as well
                self.__tag_data += tag_contents
                ndef.addData(tag_contents)
            if len(ndef_records) < 1 and not ndef.isHeaderFlagsMessageBeginBitSet():
                # Check if the first record has the message begin flag set, if this is not the case, then we are not reading valid ndef data.
                return True  # Return true, as we do see a tag, and we can access it, but it just not contains valid ndef data.
            ndef_records.append(ndef)
            # If this is the last NDef record, we stop reading.
            if ndef.isHeaderFlagsMessageEndBitSet():
                break
            next_ndef = NDefContainer(sector_number_to_read * NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR, ndef.getExtraData())
            ndef = next_ndef

        ## Parse the found ndef records
        for ndef_record in ndef_records:
            type_id = ndef_record.getType()
            if type_id in self.__record_types:
                record = self.__record_types[type_id]().parseRecord(ndef_record)
                if record is not None:
                    assert isinstance(record, RecordBase), "Due to filtering, a Record must be derived from the RecordBase class."
                    self.__records.append(record)

        self.updateVisibleTime()
        log.debug("New tag[%s] with records: %r", self.getUid(), self.__records)
        return True


    ## @brief Writes a record to the tag.
    #         This differs from the readTagData because of the following:
    #         1: readTagData reads all of the tag and divides it into different (ndef) records
    #         2: It was decided that not the whole tag would be written
    #         3: Hence, there is a need to indicate which record needs to be written.
    #         4: The easiest way is to pass this record (which is part of the tag data) as an argument as well
    #         Yes, this is not the cleanest solution, and maybe writeing of the records can be more sophisticated, but
    #         this is just the first start / attempt to write (parts of) the tag data
    #  @param hardware_device Which device to write to
    #  @param record The record to write
    def updateTagRecord(self, hardware_device, record):
        assert isinstance(record, RecordBase), "Due to filtering, a Record must be derived from the RecordBase class."

        if not hardware_device.selectPassiveTarget(self.__uid):
            # Failed to select the tag, most likely it moved out of range.
            return False

        index = record.getStartIndex()
        ## TODO EM-886: this needs to be checked further
        # Ignore an index of 0, as this means a timeout occurred. This can happen during loading (and probably unloading) of material
        if index == 0:
            return True

        length = record.getLength()
        data = record.getData()
        log.debug("Writing to hardware_device @ address %r (from ndef record, byte index) the data %r using length %r, stat record: %s", index, binascii.hexlify(data), length, record.getTotalUsageDuration())

        # Find locations in tag data
        start_index = self.__calculateAlignedIndex(index, is_start_index=True)
        end_index = self.__calculateAlignedIndex(index + length)
        log.debug("Actual start index = %r (byte index), end index = %r (byte)", start_index, end_index)
        tag_memory = memoryview(self.__tag_data)
        # Write the record data back into the tag data buffer, so it can be written next!
        tag_memory [index:index + length] = data

        # Since the maximum number of bytes written is limited to 16 we use a loop to write the sectors
        write_max_bytes = NfcTag.__AMOUNT_OF_SECTORS_WRITE * NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR
        for index_part in range(start_index, end_index, write_max_bytes):
            write_sector = int(index_part / NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR)
            data_to_send = bytes([NfcTag.__NTAG_WRITE_COMMAND, write_sector]) + bytes(tag_memory[index_part:index_part + write_max_bytes])

            log.debug("Data to send: %r", binascii.hexlify(data_to_send))
            # 16 bytes reserved for the result to be returned
            result = hardware_device.transceiveToSelected(data_to_send, 4)
            log.debug("Result of writing # %r sector at page %02x results = %r", index_part, write_sector, result)

            # Not testing for result as bytes, since we don't expect any data back (would be b''). None indicates a problem
            if result is None and self.__write_errors_logged % self.__write_error_threshold == 0:
                self.__write_errors_logged += 1
                log.error("Error writing tag detected; count %d, code: %r, ", self.__write_errors_logged, result)
                self.__write_error_threshold *= 10 # Gradually decrease the amount of logging (so a real bad tag will not cause an overflow in logging)
                return False
        return True

    ## @brief Calculates the correct byte index for writing (multiple of the bytes per sector)
    #  @param index The index to be corrected
    #  @param is_start_index A flag to indicate if this is a starting index or not
    #  If start index is 6, the new_index will be 8. This would mean, missing out on 2 bytes.
    #  Substracting the sector size will give us starting index 4, so 2 bytes of the previous record will be written as well
    #  @return Returns the adjusted index, which will be aligned on a multiple of a sector address
    def __calculateAlignedIndex(self, index, *, is_start_index=False):
        index_modulo = index % NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR

        # If it's a modulo of the sector size, no calculations need to be done
        if index_modulo == 0:
            return index

        # Determine the closest index based on modulo
        new_index = index + (NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR - index_modulo)

        # If the start index is calculated, make sure compensation is in place.
        if is_start_index:
            return new_index - NfcTag.__AMOUNT_OF_BYTES_PER_SECTOR

        return new_index