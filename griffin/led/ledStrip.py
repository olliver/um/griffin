from griffin.led.led import Led
import logging
import time
log = logging.getLogger(__name__.split(".")[-1])


## This class controls the LED strip that is in the case of the printer
class LedStrip(Led):
    def __init__(self, printer):
        super().__init__("STRIP", printer)

    def _setLeds(self, hue, saturation, brightness):
        rgb = self.getHsbToRgb(hue, saturation, self.brightnessCorr(brightness))
        r = rgb[0]; g = rgb[1]; b = rgb[2]
        # Use the min total value of the RGB to set the white led.
        # And dim the RGB leds by that amount.
        # (Note, this assumes the LEDs are linear, which they are not really. But it is close enough)
        w = min(r, g, b, Led._RGB_MAX) # white can't be more than 255,
        # now if brightness is over 100 the rgb leds can add some light!
        r -= w
        g -= w
        b -= w
        # M142 r255 g150 b150 w0 = white (enough)
        rgbw = [r, g, b, w]
        rgb_factors = [1.0, 150 / Led._RGB_MAX, 150 / Led._RGB_MAX]
        rgbw = [int(rgbw[i] * rgb_factors[i]) for i in range(0, len(rgb))] + [w]

        if self._last_led_output == rgbw: #[r, g, b, w]:
            return
        self._last_led_output = rgbw #[r, g, b, w]
        log.debug("Setting %s to %s", self._key, self._last_led_output)
        while not self._printer.debugSendGCodeNoWait("M142 r%d g%d b%d w%d" % tuple(rgbw)):
            time.sleep(0.1)