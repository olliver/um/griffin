from griffin.led.led import Led
from griffin import dbusif
import logging
import time

log = logging.getLogger(__name__.split(".")[-1])


## This class controls the leds that are in the print head.
class LedHead(Led):
    def __init__(self, printer, nr):
        super().__init__("HEAD_%d" % nr, printer)
        self._hotend_nr = nr
        self._interaction_required = False
        self.__hotend_service = dbusif.RemoteObject("printer", "printer/head/0/slot/%d" % (nr))
        self.__old_log_msg = ""
        self._is_heating = False
        self._is_cooling = False
        self._enabled = False
        self._present = False
        self._current_temperature = 0
        self._target_temperature = 0

    def _initializeSignals(self):
        self._enabled = self.__hotend_service.getProperty("is_enabled")
        self._present = self.__hotend_service.getProperty("is_present")
        self._current_temperature = self.__hotend_service.getProperty("current_temperature")
        self._target_temperature = self.__hotend_service.getProperty("target_temperature")
        self.__hotend_service.connectSignal("propertyChanged", self.__onHotendPropertyChanged)

    ## Callback when one of the hotend slot properties change.
    #  Check if it a property we care about, and then update our state to let the leds reflect the hotend state.
    #  @param key: name of the property that is updated.
    #  @param value: new value of the property.
    def __onHotendPropertyChanged(self, key, value):
        changed = True
        if key == "is_enabled":
            self._enabled = value
        elif key == "is_present":
            self._present = value
        elif key == "current_temperature":
            self._current_temperature = value
        elif key == "target_temperature":
            self._target_temperature = value
        elif key == "interaction_required":
            self._interaction_required = value
        else:
            changed = False

        if changed:
            self._is_heating = self._enabled and self._current_temperature < self._target_temperature - 10
            self._is_cooling = self._enabled and self._current_temperature > self._target_temperature + 10
            self._reEvaluateLedState()

    def _setLeds(self, hue, saturation, brightness):
        rgb = self.getHsbToRgb(hue, saturation, self.brightnessCorr(brightness))

        # (Note, this assumes the LEDs are linear, which they are not really. But it is close enough)
        # r255 g80 b70 = white (enough)
        rgb_factors = [1.0, 80 / Led._RGB_MAX, 70 / Led._RGB_MAX]
        rgb = [int(rgb[i] * rgb_factors[i]) for i in range(0, len(rgb))]

        if self._last_led_output == rgb:
            return
        self._last_led_output = rgb
        log.debug("Setting %s to %s", self._key, self._last_led_output)
        while not self._printer.debugSendGCodeNoWait("M143 T%d r%d g%d b%d" % tuple([self._hotend_nr] + rgb)):
            time.sleep(0.1)

    ## This function integrates basic job states with additional info on the state of the cartridges
    # and displays a color indicating the state of the print cartridge, etc.
    def _reEvaluateLedState(self):
        pulse = False
        color = self.COLOR_OFF

        if self._job_state == "printing" or self._job_state == "pausing":
            color = self.COLOR_PRINTING
        elif self._enabled:
            color = self.COLOR_HOT
        elif self._present:
            color = self.COLOR_ULTI_BLUE
        elif self._interaction_required:
            color = self.COLOR_ULTI_BLUE

        if self._interaction_required:
            pulse = True
        elif self._is_heating:
            pulse = True
        elif self._is_cooling:
            pulse = True

        assert color is not None

        if pulse != self.isPulsing():
            log.debug("Changing pulse mode of %s from %s to %s", self.getKey(), self.isPulsing(), pulse)
            self.setPulse(pulse)
        if self.isBlinking():
            self.setBlink(False)
        if color != self.getHsv():
            log.debug("Changing color of %s from %s to %s", self.getKey(), self.getHsv(), color)
            self.setHsv(*color)

    def setPulse(self, pulse):
        self._pulse = pulse
        while not self._printer.debugSendGCodeNoWait("M143 T%d P%dB3" % (self._hotend_nr, 2 if pulse else 0)):
            time.sleep(0.1)

    def _pulseLeds(self, update_rate, step_size):
        pass
