import logging
from griffin import dbusif
from griffin.led.ledStrip import LedStrip
from griffin.led.ledHead import LedHead
from griffin.led.ledRing import LedRing

log = logging.getLogger(__name__.split(".")[-1])


# Interface to the LEDs attached to the system.
# Currently talks to the printer service, as it needs to control the leds through the
# GCode interface with M142 commands.
class LedService(dbusif.ServiceObject):
    def __init__(self):
        super().__init__("led")

        self.__printer = dbusif.RemoteObject("printer")

        leds = [
            LedStrip(self.__printer),
            LedHead(self.__printer, 0),
            LedHead(self.__printer, 1),
            LedRing(self.__printer)
        ]
        self.__leds = {led.getKey(): led for led in leds}
        self.__running = False
        self.__printer.connectSignal("propertyChanged", self.__onPrinterPropertyChanged)

        # If marlin is connected start the leds right away.
        # If this is not the case, marlin is either updating firmware (after this is done, the leds are started) or there
        # is no connection with marlin, but we can't do anything about that.
        if self.__printer.getProperty("state") != "booting":
            log.info("_startLeds from constructor")
            self._startLeds()

    @dbusif.method("", "as")
    def getLedTypes(self):
        return list(self.__leds.keys())

    @dbusif.method("sd", "")
    def setHue(self, led, hue):
        try:
            self.__leds[led].setHue(hue)
        except KeyError:
            pass

    @dbusif.method("s", "d")
    def getHue(self, led):
        try:
            return float(self.__leds[led].getHue())
        except KeyError:
            return -1.0

    @dbusif.signal("sd")
    def hueChanged(self, led, hue):
        log.info("Hue updated %s: %s", led, hue)
        return True

    @dbusif.method("sd", "")
    def setSaturation(self, led, saturation):
        try:
            self.__leds[led].setSaturation(saturation)
        except KeyError:
            pass

    @dbusif.method("s", "d")
    def getSaturation(self, led):
        try:
            return float(self.__leds[led].getSaturation())
        except KeyError:
            return -1.0

    @dbusif.signal("sd")
    def saturationChanged(self, led, saturation):
        log.info("Saturation updated %s: %s", led, saturation)
        return True

    @dbusif.method("sd", "")
    def setBrightness(self, led, brightness):
        try:
            self.__leds[led].setBrightness(brightness)
        except KeyError:
            log.warning("SetBrightness() unknown led key: %s", led)

    @dbusif.method("s", "d")
    def getBrightness(self, led):
        try:
            return float(self.__leds[led].getBrightness())
        except KeyError:
            return -1.0

    @dbusif.signal("sd")
    def brightnessChanged(self, led, brightness):
        log.info("Brightness updated %s: %s", led, brightness)
        return True

    @dbusif.method("sd", "")
    def setBlink(self, led, blink):
        try:
            self.__leds[led].setBlink(blink)
        except KeyError:
            pass

    ## This function causes the selected let to turn off for 250 milliseconds, once, before turning back on to whatever it was before.
    # @param led the name of the led class to be blinked.
    @dbusif.method("s", "")
    def blinkOnce(self, led):
        try:
            self.__leds[led].blinkOnce()
        except KeyError:
            pass

    @dbusif.method("sd", "")
    def setPulse(self, led, blink):
        try:
            self.__leds[led].setPulse(blink)
        except KeyError:
            pass

    def __onPrinterPropertyChanged(self, key, value):
        if key == "state":
            self.__onPrinterStateChanged(value)

    def __onPrinterStateChanged(self, state):
        if not self.__running and state != "booting":
            self._startLeds()

    def _startLeds(self):
        for key in self.__leds:
            self.__leds[key].start()
            log.info("led controller for %s started", key)
        self.__running = True
