from griffin.led.led import Led
import logging
import time
log = logging.getLogger(__name__.split(".")[-1])


## This class controls the LEDs that light up the ring around the front control knob.
class LedRing(Led):
    __blinking = False

    def __init__(self, printer):
        super().__init__("RING", printer)
        self._path = "/sys/class/leds/pca963x:"
        self._colors = ["red", "green", "blue"]
        self._initLeds(self.COLOR_ULTI_BLUE)
        self._interaction_required = False

    def _setLeds(self, hue, saturation, brightness):
        rgb = self.getHsbToRgb(hue, saturation, self.brightnessCorr(brightness))

        # (Note, this assumes the LEDs are linear, which they are not really. But it is close enough)
        rgb_factors = [1.0, 60 / Led._RGB_MAX, 50 / Led._RGB_MAX]
        rgb = [int(rgb[i] * rgb_factors[i]) for i in range(0, len(rgb))]

        if self._last_led_output == rgb:
            return
        self._last_led_output = rgb

        log.debug("Setting %s to %s", self._key, self._last_led_output)

        for i in range(0, len(rgb)):
            self._writeOutput(self._colors[i], rgb[i])

    def _initializeSignals(self):
        pass
        #TODO: subscribe to onInteractionRequiredChanged signals in EM-293 after EM-284 is finished

    ## Handles the interaction required event.
    # @param interaction_required A boolean indication whether interaction with the user is required.
    def _onInteractionRequiredChanged(self, interaction_required):
        self._interaction_required = interaction_required
        self._reEvaluateLedState()

    def _reEvaluateLedState(self):
        pulse = False
        blink = False
        if self._printer_state == "error":
            self.setHsv(*self.COLOR_ULTI_BLUE)
            pulse = True
            log.info("Setting %s to PULSING BLUE %s", self._key, self._printer_state)
        # maintenance state removed!
        elif self._interaction_required:
            self.setHsv(*self.COLOR_ULTI_BLUE)
            pulse = True
            log.info("Setting %s to PULSING BLUE because interaction is required", self._key)
        elif self._printer_state == "idle":
            self.setHsv(*self.COLOR_ULTI_BLUE)
            log.info("Setting %s to BLUE %s", self._key, self._printer_state)
        elif self._printer_state == "printing":
            if self._job_state == "none":
                self.setHsv(*self.COLOR_ULTI_BLUE)
                log.info("Setting %s to PULSING BLUE %s", self._key, self._printer_state)
            elif self._job_state == "paused":
                self.setHsv(*self.COLOR_ULTI_BLUE)
                pulse = True
                log.info("Setting %s to PULSING BLUE %s and %s", self._key, self._printer_state, self._job_state)
            elif self._job_state == "wait_cleanup":
                self.setHsv(*self.COLOR_ULTI_BLUE)
                pulse = True
                log.info("Setting %s to PULSING BLUE %s and %s", self._key, self._printer_state, self._job_state)
            else:
                self.setHsv(*self.COLOR_PRINTING)
                log.info("Setting %s to PRINTING COLOR %s and %s", self._key, self._printer_state, self._job_state)
        else:
            self.setHsv(*self.COLOR_ULTI_BLUE)
            log.info("Setting %s to BLUE %s", self._key, self._printer_state)

        if pulse != self.isPulsing():
            self.setPulse(pulse)
        if blink != self.isBlinking():
            self.setBlink(blink)

    ## This function writes the LED output using the LED class kernel interface.
    #
    # These LEDs are controlled via the /sys led class kernel interface. It
    # assumes the name of each individual led corresponds to the color it
    # controls the outputs.
    #
    # @param[in] color The color to modify the output for.
    # @param[in] value The brightness value to set the output to.
    def _writeOutput(self, color, value):
        with open(self._path + color + "/brightness", "w") as f:
            f.write("%d\n" % (value))

    ## Blink LED
    #
    # We override the setBlink method of the base class, as we need to disable
    # the hardware whenever setBlink is set to false.
    #
    # @note This function does still perform all actions of the base class
    # after disabling the blink.
    #
    # @param blink Boolean indicating if blink is on or off.
    def setBlink(self, blink):
        if not blink:
            self._disableBlinking()
        super().setBlink(blink)

    ## Enable blinking on the led ring.
    #
    # The linux led interface supports (hardware) based blinking via for
    # example a 'timer' trigger. Once the trigger is enabled, the time is set
    # and the LEDs will start blinking at the brightness previously set.
    # The RGB components of the led ring are connected to the same led driver
    # and thus are treated as one and will blink them together in sync.
    #
    # @param period[in] Set the time (in seconds) of a full blink period.
    def _enableBlinking(self, period):
        for color in self._colors:
            self._setLeds(*self._target)
            with open(self._path + color + "/trigger", "w") as f:
                f.write("timer\n")
            time.sleep(0.1) # Sleep to give udev time to modify permissions
            with open(self._path + color + "/delay_off", "w") as f:
                f.write("%d\n" % (period * 1000 / 2)) # Half period in ms
            with open(self._path + color + "/delay_on", "w") as f:
                f.write("%d\n" % (period * 1000 / 2)) # Half period in ms
        self.__blinking = True

    ## Disable blinking on the led ring.
    #
    # Disable blinking by changing the trigger back to 'none'. The brightness
    # set previous the blinking will continue being used after blinking.
    def _disableBlinking(self):
        for color in self._colors:
            with open(self._path + color + "/trigger", "w") as f:
                f.write("none\n")
            self._setLeds(*self._target)
        self.__blinking = False

    def _blinkLeds(self, period):
        if not self.__blinking:
            self._enableBlinking(period)

    ## Initialize the leds so they are in a known state.
    #
    # Whenever the led service (re)starts, it may not know what current active
    # trigger is. So at start, we put all the lights in a known good state.
    def _initLeds(self, color):
        for col in self._colors:
            with open(self._path + col + "/trigger", "w") as f:
                f.write("none\n")
        self.setHsv(*color)
