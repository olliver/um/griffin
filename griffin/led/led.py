import logging
import time
import math

from griffin import thread

log = logging.getLogger(__name__.split(".")[-1])


## this class is the base for all the LED controls.
# besides creating all the basic HSV getters and setters it has a thread which runs the function _manageLeds().
# the _manageLeds() thread generates all the color, brightness and saturation shifts and blinking and pulsing of the leds.
# the LEDs are controlled by Hue Saturation and Value (aka Brightness).
# a good description of HSV can be found here: https://en.wikipedia.org/wiki/HSL_and_HSV.
# http://colorizer.org/ is a good place to play around with the HSV sliders to see how they effect the RGB values etc.

# A simple fade effect is implemented in the base LED class as well this also takes care of the startup ramp up (slowly getting brighter at startup).
# But each color change will not be applied instantly instead the HSV values are adjusted slowly untill the target HSV values are reached.
class Led():
    _RGB_MAX = 255 # rgb values can be PWM controlled by one byte

    ## internally lists are used to store the HSV values this represents the index in the array for the hue.
    HUE = 0
    ## internally lists are used to store the HSV values this represents the index in the array for the saturation.
    SAT = 1
    ## internally lists are used to store the HSV values this represents the index in the array for the value/brightness (or even lightness/intensity).
    VAL = 2

    # colors
    COLOR_ULTI_BLUE = [196.19, 94.71, 100.0]          # Ultimaker blue (aprox #00bfff)
    COLOR_ERROR = [0, 100.0, 100.0]               # error red
    COLOR_HOT = [15, 100.0, 100.0]               # Color when print head is hot.
    COLOR_PRINTING = [0, 0, 100.0]
    COLOR_OFF = [0, 0, 0]

    __PULSE_MIN = 0.2       ## Minimum intensity during pulsing, 0.2 is not completely off.
    __PULSE_MAX = 1.2       ## Maximum intensity during pulsing
    __PULSE_SPEED = 0.03    ## step per timeslice, defines how fast to pulse

    def __init__(self, key, printer):
        self._printer = printer
        self._key = key
        self._fade_time = 3.0 ## seconds to switch to full color/brightness.
        self._delta_t = 0.05  ## smallest delta T in seconds
        self._time_slices = self._fade_time / self._delta_t ## number of changes in a fade period
        self._blink_period = 1 ## Period time (in seconds) to the blink
        hue = 0.0
        saturation = 0.0
        brightness = 100.0
        self._target = [hue, saturation, brightness]
        self._current = [hue, saturation, 0.0]

        self._last_led_output = -1
        self._blink = False
        self._blink_once = False
        self._pulse = False
        self._pulse_count = 0
        self._brightness_override = None
        self._brightness_override_end = None

        self._thread = None
        self._led_thread_working = thread.Event()

        self._printer_state = printer.getProperty("state")
        self._job_state = printer.getProperty("job_state")

        # These variables are internally for the led class only and are waiting to find a new home after a rework.
        self.__blink_dir = 1
        self.__pulse_dir = -1
        self.__pulse_multiplier = 1.0
        self.__prev_color = []
        self._prev_target_color = [0,0,0]
        self._fade_step_delta = [0,0,0]


    ## Code called to start the LED control thread and ramp up the brightness from this point onwards.
    def start(self):
        if self._thread is not None:# can only start once
            return

        self._thread = thread.Thread("%s state manager thread" % self._key, self._manageLeds)
        self._thread.daemon = True
        self._thread.start()

        self._printer_state = self._printer.getProperty("state")
        self._job_state = self._printer.getProperty("job_state")
        self._printer.connectSignal("propertyChanged", self._onPrinterPropertyChanged)
        self._initializeSignals()
        self._reEvaluateLedState()

    # Function called by the onPropertyChanged signal of the printer dbus remoteobject.
    # @param property_key, key of the changed property
    # @param value, new value of the property
    def _onPrinterPropertyChanged(self, property_key, value):
        # Check if one of our monitored properties change and
        if property_key == "state":
            self._printer_state = value
            self._reEvaluateLedState()
        if property_key == "job_state":
            self._job_state = value
            self._reEvaluateLedState()

    ## This function is meant to allow subclasses to subscribe to additional signals before the _reEvaluateLedState() is called for the first time but after the thread has been started
    def _initializeSignals(self):
        pass

    ## Optionally implemented in specific LED sub class to react to some basic state changes.
    def _reEvaluateLedState(self):
        pass

    ## Returns the key/name of the LED object
    def getKey(self):
        return self._key

    ## Controls the target Hue of the Led.
    # @param hue The hue/color of light the LED emits, hue is specified as 0-360 where 0 is equal to 360.
    def setHue(self, hue):
        self._target[self.HUE] = hue
        self._led_thread_working.set()

    ## Returns the target Hue of the Led, hue is specified as 0-360 where 0 is equal to 360.
    def getHue(self):
        return self._target[self.HUE]

    ## sets all the values controlling Hue Saturation and Value/brightness at once, likely avoiding unintentional changes in the mean time.
    # @param h The hue/color of light the LED emits.
    # @param s The saturation of light the LED emits.
    # @param v The brightness of light the LED emits.
    def setHsv(self, h, s, v):
        self._target = [h, s, v]
        self._led_thread_working.set()

    ## Returns an array containing the target Hue, Saturation and value/brightness used by the Led object.
    def getHsv(self):
        return self._target.copy()

    ## Controls the target saturation percentage of the LED.
    # @param saturation The target saturation of light the LED emits.
    def setSaturation(self, saturation):
        self._target[self.SAT] = saturation
        self._led_thread_working.set()

    ## Returns the target saturation percentage of the LED.
    def getSaturation(self):
        return self._target[self.SAT]

    ## Controls the target Brightness/Value percentage of the Led.
    # @param brightness The n of light the LED emits.
    def setBrightness(self, brightness):
        self._target[self.VAL] = brightness
        self._led_thread_working.set()

    ## Returns the target Brightness/Value percentage of the Led.
    def getBrightness(self):
        return self._target[self.VAL]

    ## Controls the blinking of the Led.
    def setBlink(self, blink):
        self._blink = blink
        self._led_thread_working.set()

    ## Makes the led blink to OFF for 250 milliseconds, then back to on or whatever it was before.
    def blinkOnce(self):
        self._blink_once = True
        self._led_thread_working.set()

    ## Returns the blinking of the Led.
    def isBlinking(self):
        return self._blink

    ## Controls the pulsing of the Led.
    def setPulse(self, pulse):
        self._pulse = pulse
        self._led_thread_working.set()

    ## Returns the pulsing of the Led.
    def isPulsing(self):
        return self._pulse

    ## internal thread for managing all the transitions.
    def _manageLeds(self):
        # Run the startup glow
        # handled by start values and the default fade in/out effect!

        self._prev_target_color = [0, 0, 0]
        self.__prev_color = self._current.copy()
        while True:
            if self._target == self._current and not self._blink and not self._pulse:
                self._led_thread_working.wait()

                self.__blink_dir = 1 # so it always starts with being ON after reaching the correct hsv.
                self.__pulse_dir = -1
                self.__pulse_multiplier = 1.0

            # clear before action so events triggered during run are handled at a later pass through
            self._led_thread_working.clear()

            if self._blink_once:
                self._setLeds(self._target[self.HUE], self._target[self.SAT], 0)
                time.sleep(0.250)
                self._setLeds(self._target[self.HUE], self._target[self.SAT], self._target[self.VAL])
                continue

            log.debug("prev_target_color %s  _target %s", self._prev_target_color, self._target)
            if self._prev_target_color != self._target:
                self._calculateCrossFadeStepDelta()
                self._prev_target_color = self._target.copy()
                continue

            if self._current != self._target:
                old = self._current.copy()
                self._crossFadeStep()
                log.debug("fade_step %s -> %s", old, self._current)
            elif self._blink:
                self._blinkLeds(self._blink_period)
            elif self._pulse:
                self._pulseLeds(self._delta_t, self.__PULSE_SPEED)

    ## This function calculates the incremental change to be applied in each pass of the crossFadeStep function.
    #
    # @returns Returns the fade_time newly calculated fade_time table.
    def _calculateCrossFadeStepDelta(self):
        if self._current[self.SAT] <= 0.1 or self._current[self.VAL] <= 0.1:
            self._current[self.HUE] = self._target[self.HUE]
        if self._current[self.VAL] <= 0.1:
            self._current[self.SAT] = self._target[self.SAT]
        if self._target[self.SAT] <= 0.1 or self._target[self.VAL] <= 0.1:
            self._target[self.HUE] = self._current[self.HUE]
        if self._target[self.VAL] <= 0.1:
            self._target[self.SAT] = self._current[self.SAT]


        for i, (t, c) in enumerate(zip(self._target, self._current)):
            delta = t - c
            if i == self.HUE:
                if delta > 360 / 2:
                    delta -= 360
                elif delta < -360 / 2:
                    delta += 360
                log.debug("hue delta %s", delta)
            self._fade_step_delta[i] = delta / self._time_slices


    ## This function does one step of the cross-fade the current and target
    # values.
    #
    # When changing from either blink or pulse to blink or pulse, we want this
    # to happen gradually. E.g. first go fully on, then pulsate/blink from this
    # known state. Technically we do not purely fade, only the saturation and
    # Value (brightness) fade. The Hue actually rotates around as it naturally
    # already fades.
    # The _manageLeds class constantly calls this function to update the Led
    # output.
    #
    # @note Currently, this function relies on the self.__prev_color to be
    # available in the class and have the initial copy done.
    # @note This blocks the Led class where it is run from.
    #
    def _crossFadeStep(self):
        flags = [False] * len(self._current)
        for i, (inc, c) in enumerate(zip(self._fade_step_delta, self._current)):
            c += inc

            # value clamping and roll over for hue
            if i == self.HUE:
                if c < 0:
                    c+= 360
                elif c > 360:
                    c -= 360
                self._current[i] = max(min(c, 360), 0)
            else:
                self._current[i] = max(min(c, 100), 0)

            # make sure the increment stops at some point
            if inc == 0 or abs(self._current[i] -  self._target[i]) <= abs(inc):
                log.debug("%s: triggered end condition %s <= %s", self.getKey(), abs(self._current[i] -  self._target[i]), abs(inc) )
                flags[i] = True
                self._fade_step_delta[i] = 0.0 # end reached no more change

        if flags == ([True] * len(self._current)):
            log.debug("%s all end conditions reached!", self.getKey())
            self._current = self._target.copy()

        self.__prev_color = self._current.copy()
        log.debug("_crossFadeStep(%s, %s, %s)", str(self._fade_step_delta), str(self._current), str(self._target))

        self._setLeds(*self._current)
        time.sleep(self._delta_t)

    ## This function implements a software blink function.
    # The _manageLeds class constantly calls this function to update the Led
    # output.
    #
    # @note This blocks the Led class where it is run from.
    #
    # @param[in] period The blink rate in seconds, notice that half the period is
    # the on time, the other half the off time.
    def _blinkLeds(self, period):
        self._setLeds(self._target[self.HUE], self._target[self.SAT], self.__blink_dir * self._target[self.VAL])
        self.__blink_dir = 1 if self.__blink_dir == 0 else 0
        time.sleep(period / 2)

    ## This function implements a software pulse function.
    # The _manageLeds class constantly calls this function to update the Led
    # output.
    #
    # @note This blocks the Led class where it is run from.
    # @todo The API interface should be rewritten to supply a period and the
    # number of steps for during this period.
    #
    # @param[in] update_rate The pulse update rate in seconds.
    # @param[in] step_size The time (in seconds) for each color change.
    def _pulseLeds(self, update_rate, step_size):
        log.debug("pulsing in baseclass for %s (%s)", self.getKey(), str(self))
        hue = self._target[self.HUE]

        self._setLeds(hue, self._target[self.SAT], min(1.0, max(0.0, self.__pulse_multiplier)) * self._target[self.VAL])
        self.__pulse_multiplier += self.__pulse_dir * step_size
        if self.__pulse_multiplier >= self.__PULSE_MAX: # stay ON a bit longer
            self.__pulse_dir = -1
        elif self.__pulse_multiplier <= self.__PULSE_MIN: # don't turn completely off
            self.__pulse_dir = 1
            self._pulse_count += 1

        time.sleep(update_rate)

    ## This function does the mapping to the physical control of each LED to be implemented by each Led subclass.
    # @param hue The hue/color of light the LED emits, hue is specified as 0-360 where 0 is equal to 360.
    # @param saturation The target saturation of light the LED emits.
    # @param brightness The brightness of light the LED emits.
    def _setLeds(self, hue, saturation, brightness):
        log.warning("not implemented!")

    ## Returns output brightness for a given linear brightness of brightness, unfortunately eyes and brains do not perceive this Linearly.
    # @param brightness The brightness of light the LED emits.
    def brightnessCorr(self, brightness):
        # f(x) ~ (exp(sin(x)) - 1/e) * 100/(e-1/e)
        # -1/e = -0.36787944117144233, and 100/(e - 1/e) = 42.54590641196608 are constants, and can be pre-calcuated.
        # pi/2 is there to make sure the sinusoid is applied to the right range
        max_output = 100 # since it's percent
        nb = (math.exp(math.sin((brightness - max_output / 2) * math.pi / max_output)) - 0.36787944117144233) * 42.54590641196608
        return nb

    ## Internal function that translates HSV/HSB values into RGB values.
    # @param hue The hue/color of light the LED emits, hue is specified as 0-360 where 0 is equal to 360.
    # @param saturation The target saturation of light the LED emits.
    # @param brightness The brightness of light the LED emits.
    @classmethod
    def getHsbToRgb(cls, hue, saturation, brightness):
        # Turn HSV values into RGB.
        c = brightness / 100.0 * saturation / 100.0
        x = c * (1.0 - abs(((hue / 60.0) % 2.0) - 1.0))
        m = brightness / 100.0 - c

        if hue < 60:
            r = c + m
            g = x + m
            b = m
        elif hue < 120:
            r = x + m
            g = c + m
            b = m
        elif hue < 180:
            r = m
            g = c + m
            b = x + m
        elif hue < 240:
            r = m
            g = x + m
            b = c + m
        elif hue < 300:
            r = x + m
            g = m
            b = c + m
        else:
            r = c + m
            g = m
            b = x + m

        r = int(r * cls._RGB_MAX)
        g = int(g * cls._RGB_MAX)
        b = int(b * cls._RGB_MAX)

        return [r, g, b]

