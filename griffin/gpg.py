import os
import logging
import subprocess
import io

log = logging.getLogger(__name__.split(".")[-1])


## GPG class to interface with GNU Privacy Guard.
#  This class can be used to sign data, verify signatures.
#  It works by using the gpg executable on the system.
class GPG:
    def __init__(self):
        self.__gpg_executable = "/usr/bin/gpg"
        self.__public_key_files = []
        self.__private_key_files = []

    ## Generate a new public/private key pair.
    # Note that this function will take a really long time (minutes), as it will need to collect a lot of entropy (random data).
    # This function does not add the generated files to the list of active keys.
    def generatePublicPrivateKeyFiles(self, public_key_file, private_key_file):
        if os.path.isfile(public_key_file):
            raise ValueError("Supplied public key filename (%s) already exists" % (public_key_file))
        if os.path.isfile(private_key_file):
            raise ValueError("Supplied private key filename (%s) already exists" % (private_key_file))

        batch_data = "\n".join(["Key-Type: RSA", "Key-Length: 2048", "Name-Real: Ultimaker", "Name-Email: info@ultimaker.com", "Expire-Date: 0", "%commit"])
        log.info("Starting generation of public/private key pair: %s %s" % (public_key_file, private_key_file))
        if self.__runGPGProcess("--secret-keyring", private_key_file, "--keyring", public_key_file, "--gen-key", "--batch", stdin=batch_data) != 0:
            raise ValueError("Failed to generate the private key for %s" % (private_key_file))

    # Add a public key file to be used to verify or decrypt data.
    def addPublicKeyFile(self, key_file):
        if not os.path.isfile(key_file):
            raise ValueError("Supplied key filename (%s) does not exist" % (key_file))
        # Run "--list-keys" to verify that the key is a valid gpg public key and contains a public key. (exit == 0)
        if self.__runGPGProcess("--keyring", key_file, "--list-keys") != 0:
            raise ValueError("Failed to find public key in %s" % (key_file))
        # Run "--list-secret-keys" to verify that no private key is included. (exit != 0)
        if self.__runGPGProcess("--secret-keyring", key_file, "--list-secret-keys") == 0:
            raise ValueError("Found a private key in %s, while it is added as public key" % (key_file))
        self.__public_key_files.append(key_file)

    # Add a private key file to be used to sign or encrypt data.
    def addPrivateKeyFile(self, key_file):
        if not os.path.isfile(key_file):
            raise ValueError("Supplied key filename (%s) does not exist" % (key_file))
        # Run "--list-secret-keys" to verify that the key is a valid gpg private key and contains a private key. (exit == 0)
        if self.__runGPGProcess("--secret-keyring", key_file, "--list-secret-keys") != 0:
            raise ValueError("Failed to find private key in %s" % (key_file))
        self.__private_key_files.append(key_file)

    # Verify if the given signed_file was properly signed in the signature file by one of the private keys belonging to the public keys that we have.
    def verifySignature(self, signed_file, signature_file):
        if not os.path.isfile(signed_file):
            raise ValueError("Supplied signed file (%s) does not exist" % (signed_file))
        if not os.path.isfile(signature_file):
            raise ValueError("Supplied signature file (%s) does not exist" % (signature_file))
        for key_file in self.__public_key_files:
            if self.__runGPGProcess("--keyring", key_file, "--verify", signature_file, signed_file) == 0:
                return True
        return False

    # Generate a signature file with the first public+private key that we have, and store this signature in the given file.
    def generateSignature(self, signed_file, signature_file):
        if not os.path.isfile(signed_file):
            raise ValueError("Supplied file (%s) to be signed does not exist" % (signed_file))
        if os.path.isfile(signature_file):
            raise ValueError("Supplied signature filename (%s) already exists" % (signature_file))
        if len(self.__private_key_files) < 1:
            raise ValueError("Tried to sign a file, but no private keys added to GPG instance.")
        if len(self.__public_key_files) < 1:
            raise ValueError("Tried to sign a file, but no public keys added to GPG instance.")

        if self.__runGPGProcess("--secret-keyring", self.__private_key_files[0], "--keyring", self.__public_key_files[0], "--output", signature_file, "--detach-sig", signed_file) != 0:
            raise ValueError("Failed to sign file.")

    # Run the gpg application and return the resulting return code. Which is 0 on success.
    # @param parameters: A list of arguments given to the gpg application
    # @return the return code of the gpg application. Generally 0 for success.
    def __runGPGProcess(self, *parameters, **kwargs):
        # Run gpg with "--no-default-keyring" to not fetch keys from the home directory.
        # Run gpg with "--ignore-time-conflict" and "--ignore-valid-from" to avoid problems with incorrect system time (keys from the future)
        command = [self.__gpg_executable, "--no-sig-cache", "--no-auto-check-trustdb", "--homedir=/tmp", "--no-default-keyring", "--ignore-time-conflict", "--ignore-valid-from"] + list(parameters)
        log.debug("Running: %s", command)
        p = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdin = kwargs.get("stdin", "")
        out, err = p.communicate(stdin.encode("utf-8"))
        log.debug("gpg output: %s", (out + err).decode("utf-8", "ignore"))
        return p.wait()

if __name__ == "__main__":
    import uuid
    logging.basicConfig(level=logging.DEBUG)

    gpg = GPG()
    #gpg.generatePublicPrivateKeyFiles("./temp_public.key", "./temp_private.key")
    gpg.addPublicKeyFile("./temp_public.key")
    gpg.addPrivateKeyFile("./temp_private.key")
    open("./temp_file", "wb").write(uuid.uuid4().bytes)
    try:
        os.unlink("./temp_file.sig")
    except OSError:
        pass
    gpg.generateSignature("./temp_file", "./temp_file.sig")
    log.info("Verify result: %s", gpg.verifySignature("./temp_file", "./temp_file.sig"))
    open("./temp_file", "wb").write(uuid.uuid4().bytes)
    log.info("Verify result: %s", gpg.verifySignature("./temp_file", "./temp_file.sig"))
