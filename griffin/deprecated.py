import traceback
import logging

log = logging.getLogger(__name__.split(".")[-1])


def function(deprecated_message):
    def add_wrapper(func):
        def wrapper(*args):
            calling_function_info = traceback.extract_stack(limit=2)[-2]
            log.warning("Call to deprecated function: %s from %s:%d:%s, %s", func, calling_function_info[0], calling_function_info[1], calling_function_info[2], deprecated_message)
            func(*args)
        return wrapper
    return add_wrapper
