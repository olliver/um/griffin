import time
import logging
from queue import PriorityQueue
from griffin import thread

log = logging.getLogger(__name__.split(".")[-1])


## The timer class is meant to call a function periodically or just once after a specific delay.
# Time starts counting when the timer's start function is called.
# Approximately 1.5 millisecond accurate timing should be maintained.
# Repeating Timers will not drift unless the start function is called on an already running timer. Exception
# are large changes to the system's time caused by NTP, then current time + delay will be used.
# Timers are assumed to be single shot unless otherwise specified.
# Timers can be executed in the thread shared by all timers (this is the default but potentially degrading timing accuracy)
# or in a new thread for each run, not super expensive if the Thread object is actually a Threadpool.
class Timer():
    ## Construct a Timer, aka call a function with a time delay.
    # Example: Timer("txt l8er", 12.4, print, "some text to print later.").start()
    # The above timer will call the print function with the argument(some text...) supplied after 12 seconds.
    # Repeated timers have to be created, and then set to timer.setSingleShot(false)
    # Example: t = Timer("txt l8er", 4, logging.warning, "some repeated delayed warning %f", 4).setSingleShot(False).start()
    # and use t.stop() to stop the timer.
    # @param name       A name for logging and debug purposes
    # @param delay      The time to wait before calling the function
    # @param function   The function to be called when the timer runs out, any arguments or
    #                   keyword arguments will be sent on to the function.
    def __init__(self, name, delay, function, *args, **kwargs):
        self._name = name
        self._delay = delay
        self._single_shot = True
        self._function = function
        self._args = args
        self._kwargs = kwargs
        self._running = False
        self._wrapper = _TimeWrapper(self)
        self._threaded = False

        if args is None:
            self._args = ()
        if kwargs is None:
            self._kwargs = {}

    ## starts the timer, delay counts from now.
    # @return self for chaining commands.
    def start(self):
        self._running = True
        self._wrapper.start()
        return self

    ## stops the timer, removes the timer from the list of timers.
    # @return self for chaining commands.
    def stop(self):
        self._running = False
        self._wrapper.start()
        return self

    ## stops the timer and then starts it again, delay counts from now (again).
    # @return self for chaining commands.
    def restart(self):
        self.stop()
        self.start()
        return self

    ## returns the name of the timer, this is used for debugging only.
    def getName(self):
        return self._name
    
    ## returns the name of the timer, this is used for debugging only.
    # @return self for chaining commands.
    def setName(self, name):
        self._name = name
        return self
    
    ## returns whether the timer is currently running.
    def isRunning(self):
        return self._running

    ## Sets whether the timer is run once (default) or if it should be adjusted and re-queued until stop().
    # It is NOT intended to set this after the timer has started.
    # @return self for chaining commands.
    def setSingleShot(self, single_shot):
        if self._running:
            log.warning("Timer(%s) setSingleShot() called while timer is supposed to be running.", self.getName())
        self._single_shot = single_shot
        return self

    ## Indicates whether the timer is run once (default) or if it should be adjusted and re-queued until stop().
    def isSingleShot(self):
        return self._single_shot

    ## Returns the time to wait before the timer is ran, after being started.
    def getDelay(self):
        return self._delay

    ## Sets the time to wait before the timer is run, after being started.
    # It is NOT intended to set this after the timer has started.
    # @return self for chaining commands.
    def setDelay(self, delay):
        if self._running:
            log.warning("Timer(%s) setDelay() called while timer is supposed to be running.", self.getName())
        self._delay = delay
        return self

    ## returns whether the function should be ran as a separate thread or in the runtime of the timer thread(one single thread for all Timer objects)
    def isThreaded(self):
        return self._threaded

    ## returns whether the function should be ran as a separate thread or in the runtime of the timer thread(one single thread for all Timer objects)
    # It is NOT intended to set this after the timer has started.
    # @return self for chaining commands.
    def setThreaded(self, threaded):
        if self._running:
            log.warning("Timer(%s) setThreaded() called while timer is supposed to be running.", self.getName())
        self._threaded = threaded
        return self


# This class takes care of the data that is not desirable to be user accessible from the Timer class
# Such as tracking when next this timer will run. Such as the next delay period.
# it redirects and implements only the functions needed by _TimeRunner and no more.
# This allows the implementation of _TimeRunner to remain clear and concise.
# And not use a extra tuple or list item just for sorting.
class _TimeWrapper():
    def __init__(self, timer):
        self._timer = timer
        self._next_time = None

    # calls the Timer's function.
    def run(self):
        if self._timer.isThreaded():
            t = thread.Thread("thread for Timer(%s)" % self._timer.getName(), self._run, daemon = True)
            t.start()
        else:
            self._run()

        # assignments are atomic as far as the GIL is concerned so no need for a Lock here, XXX the modification is not XXX!
        self._next_time += self._timer.getDelay()
        # Limit the number of triggers when the system time jumps, for example by NTP-deamon or computer sleep.
        if self._next_time < time.monotonic():
            # assignments are atomic as far as the GIL is concerned so no need for a Lock here
            self._next_time = time.monotonic() + self._timer.getDelay()

    def _run(self):
        try:
            self._timer._function(*self._timer._args, **self._timer._kwargs)
        except:
            log.exception("Timer(%s) function had an exception!", self._timer.getName())

    # called by the timer, allows the wrapper to fix the _next_time.
    def start(self):
        # doing this recalculation here is also a very nice feature of having a wrapper here.
        # re-calculating this in the _TimeRunner class seemed off and awkward, searching the queue also took a lot more
        # code since we had to look inside the list/tuple we put the timer object in.

        # assignments are atomic as far as the GIL is concerned so no need for a Lock here
        if not self._timer.isRunning():
            self._next_time = 0
        else:
            self._next_time = time.monotonic() + self._timer.getDelay()

        _TimeRunner.getInstance().update(self)

    def getNextTime(self):
        return self._next_time

    def unWrap(self):
        return self._timer

    def isRunning(self):
        return self._timer.isRunning()

    def getName(self):
        return self._timer.getName()

    def isSingleShot(self):
        return self._timer.isSingleShot()

    def __cmp__(self, o):
        if isinstance(o, _TimeWrapper):
            return (self.getNextTime() == o.getNextTime())
        else:
            return False

    def __lt__(self, o):
        if isinstance(o, _TimeWrapper):
            return (self.getNextTime() < o.getNextTime())
        else:
            raise TypeError("_TimeWrapper can only be compared(<) to other _TimeWrapper objects not %s" % str(type(o)))

    def __gt__(self, o):
        if isinstance(o, _TimeWrapper):
            return (self.getNextTime() > o.getNextTime())
        else:
            raise TypeError("_TimeWrapper can only be compared(>) to other _TimeWrapper objects not %s" % str(type(o)))


## This class runs each timer object as close to the intended time as possible.
# As it was made to avoid the usage of huge amounts of threads.
class _TimeRunner():
    def __init__(self):
        self._running = True
        self._queue = PriorityQueue()
        self._thread = thread.Thread("_TimeRunner", self._run, daemon = True)
        self._event = thread.Event()
        self._thread.start()

    def _run(self):
        while self._running:
            timer = self._queue.get()

            # discard stopped timer
            if not timer.isRunning():
                continue
                
            time_left = timer.getNextTime() - time.monotonic()
            if time_left > 0.001:
                # originally this section was a sleep, but this allows you to
                # always wait here and just adjust if something is added to the queue.
                # in a roundabout way this does use the kernel's timers as those are used to give semaphore its timeout.
                if self._event.wait(timeout = time_left): # if true: something was added to the queue
                    self._event.clear()
                    self._queue.put(timer) # re-queue old timer and
                    continue # get whichever is now the latest timer
                # end sleep
                time_left = timer.getNextTime() - time.monotonic()

            if abs(time_left) > 1.0:
                log.warning("Timer(%s) ran more than a second out of sync! (%f)", timer.getName(), time_left)

            log.debug("Timer(%s) running now", timer.getName())
            timer.run()

            # re-queue
            if not timer.isSingleShot():
                self._queue.put(timer)
            else:
                log.debug("Timer(%s) removed from queue due to single_shot", timer.getName())

    # Call this function whenever a timer's properties are changed.
    # Re-evaluate the timer list and add the given timer to the queue if not already in it.
    def update(self, timer):
        if isinstance(timer, _TimeWrapper):
            timer_is_unique = True
            with self._queue.mutex:
                if timer in self._queue.queue:
                    timer_is_unique = False
            if timer_is_unique:
                self._queue.put(timer)

            # some timer changed or added, re-evaluate the whole timer list!
            self._event.set()
        else:
            log.warning("timer added was not a Timer but %s", str(type(timer)))

    # singleton class instance
    _instance = None

    @classmethod
    def getInstance(cls):
        if _TimeRunner._instance is None:
            _TimeRunner._instance = _TimeRunner()
        return _TimeRunner._instance
        
