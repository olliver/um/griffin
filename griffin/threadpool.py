from threading import Thread as OriginalThread
from griffin.atomic import AtomicCounter
import queue
import logging
import time
import os

log = logging.getLogger(__name__.split(".")[-1])


#  Private threadpool class
#  This object will manage a pool of threads that run jobs.
#  The Thread object in this module will actually ask the ThreadPool to run the actual job,
#  instead of creating a seperate system thread for ever job.
#  The threadpool also has a watchdog thread that checks if the queue does not get too big, and when it does, it creates
#  extra threads for this pool.
#
class ThreadPool:
    _instance = None

    # Initialise and verify singleton
    def __init__(self):
        if ThreadPool._instance:
            raise ValueError("Duplicate singleton creation of ThreadPool")
        self._queue = queue.Queue()
        self._worker_thread_count = 0
        self._idle_workers = AtomicCounter(0)   # Semaphore used as an atomic counter
        self._createWorkers(10)
        self._createWatchdog()

    # Add thread to queue
    # @param object Thread The thread to be put in the queue
    def queueThread(self, thread):
        self._queue.put(thread)

    # Worker jobs - each one handles a queued thread until completion
    # @TODO Time out on get - decreasing number of idle workers
    def _worker(self):
        while True:
            # The idle workers semaphore is used to count the # of workers currently blocking in queue.get()
            self._idle_workers.increase()    # increment
            thread = self._queue.get()
            self._idle_workers.decrease()
            try:
                thread.run()
            except:
                log.exception("Exception in thread worker...")
            self._queue.task_done()

    #  The watchdog makes sure there will be enough worker jobs to run the threads put in the queue.
    #  Currently the running average size is calculated and when if the queue size is 1 for 3 consecutive times (5 seconds interval,
    #  meaning a thread has been waiting in the queue for 15 seconds) extra workers are added to the threadpool
    #  So when more threads would be queued, extra workers are created more frequently
    def _watchdog(self):
        average = 0.0
        while True:
            queue_size = self._queue.qsize()
            log.debug("ThreadPool %s with pid %d => Queue size: %d, worker threads: %d", self, os.getpid(), queue_size, self._worker_thread_count)
            average /= 1.5
            average += queue_size - self._idle_workers.getValue()
            if average > 2:
                self._createWorkers(int(average) + 2)
            log.debug("Idle workers: %d/%d", self._idle_workers.getValue(), self._worker_thread_count)
            time.sleep(5)

    # Creates a number of worker jobs
    # @param int count The number of worker jobs to be created
    def _createWorkers(self, count):
        log.info("Threadqueue increasing, currently has %d workers, starting %d extra worker(s)", self._worker_thread_count, count)
        for _ in range(0, count):
            self._createWorker()

    # Creates a single worker job and starts it (idle)
    def _createWorker(self):
        thread = OriginalThread(target = self._worker)
        self._worker_thread_count += 1
        thread.daemon = True
        thread.start()

    # Creates the watchdog job (only one per threadpool)
    def _createWatchdog(self):
        thread = OriginalThread(target = self._watchdog)
        thread.daemon = True
        thread.start()

    # Gets the instance for the threadpool or create it if not yet existing
    # @retval object Returns the instance of the threadpool to be used
    @classmethod
    def getInstance(cls):
        if ThreadPool._instance is None:
            ThreadPool._instance = cls()
        return ThreadPool._instance
