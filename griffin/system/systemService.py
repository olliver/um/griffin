import logging
import time
import subprocess
import platform
import shutil
import re
import struct
import os
import uuid

from griffin.factoryReset import FactoryReset
from griffin import dbusif
from griffin import thread
from griffin.system import systemUpdate
from griffin.preferences.registryFile import RegistryFile

log = logging.getLogger(__name__.split(".")[-1])


# Basic system service, used to query some information about the system as a whole.
class SystemService(dbusif.ServiceObject):
    # Location of the public pgp public key
    __PUBLIC_KEY_FILE = "/usr/share/griffin/griffin/system/ultidev.gpg"
    # Location of the sysem wide preferences file
    __PREFERENCES_FILE = "system_preferences.json"

    # Preference key: machine name
    __PREF_KEY_MACHINE_NAME = "machine_name"
    # Preference key: developer mode
    __PREF_KEY_DEVELOPER_MODE = "developer_mode"
    # Preference key: country code
    __PREF_KEY_COUNTRY_CODE = "country_code"
    # Preference key: system language
    __PREF_KEY_SYSTEM_LANGUAGE = "system_language"

    # Location of the EEPROM in the filesystem (eeprom is exposed as a file-like object by linux)
    __SYSTEM_EEPROM_FILE = "/sys/bus/i2c/devices/1-0050/eeprom"
    # Offsets for various parts in the EEPROM see docs/Board_EEPROM_layout.txt for details.
    __SYSTEM_EEPROM_BOM_OFFSET = 0x100
    __SYSTEM_EEPROM_REV_OFFSET = 0x104
    __SYSTEM_EEPROM_GUID_OFFSET = 0x108

    # These settings needs to be saved during a soft factory reset
    __SAVE_SETTINGS_ON_SOFT_RESET = []
    # These settings need to be set
    __ADD_SETTINGS_ON_SOFT_RESET = { }

    def __init__(self):
        super().__init__("system")

        self.__settings = RegistryFile(self.__PREFERENCES_FILE)

        self._system_update = systemUpdate.SystemUpdate()
        if os.path.exists(self.__PUBLIC_KEY_FILE):
            self._system_update.addPublicKey(self.__PUBLIC_KEY_FILE)
        self._system_update.onUpdateStart.connect(self.onUpdateProcedureStart)

        self.__firmware_check = ""
        self._system_update.onFirmwareVersionLoaded.connect(self._handleFirmwareUpdate)

        self.__reset_callbacks = {}

        self._machine_bom_number = 0
        self._machine_revision_number = 0
        self._machine_guid = ""

        self._readEEPROM()
        self._initMachineName()

        if self.isDeveloperModeActive():
            log.info("Starting dropbear SSH server because developer mode is active.")
            p = subprocess.Popen(["sudo", "/bin/systemctl", "start", "dropbear"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            p.wait()

    # Load the machine BOM and machine revision from the board EEPROM
    #
    # Read the board EEPROM and read the machine BOM and REVISION from there. These are stored as unsigned 32bit integers at a certain offset in the EEPROM.
    # Writing the EEPROM needs to be done as root. Till we have the proper facilities for this, the following commandline can be used:
    # python3 -c 'bom_number = 1; revision_number = 0xFFFFFFFF; import struct; f = open("/sys/bus/i2c/devices/1-0050/eeprom", "r+b"); f.seek(0x100); f.write(struct.pack("!I", bom_number)); f.seek(0x104); f.write(struct.pack("!I", revision_number)); f.close()'
    def _readEEPROM(self):
        try:
            with open(self.__SYSTEM_EEPROM_FILE, "rb") as f:
                f.seek(self.__SYSTEM_EEPROM_BOM_OFFSET)
                bom_number = struct.unpack("!I", f.read(4))[0]
                f.seek(self.__SYSTEM_EEPROM_REV_OFFSET)
                rev_number = struct.unpack("!I", f.read(4))[0]
                f.seek(self.__SYSTEM_EEPROM_GUID_OFFSET)
                guid = str(uuid.UUID(bytes=f.read(16)))
        except OSError as e:
            log.error("Exception while reading machine BOM/Revision/GUID: %s\nUsing default values for BOM, GUID and revision.", e)
            bom_number = 0
            rev_number = 0
            guid = "00000000-0000-0000-0000-000000000000"
        if bom_number == 0xFFFFFFFF:
            bom_number = 0
        if rev_number == 0xFFFFFFFF:
            rev_number = 0
        self._machine_bom_number = bom_number
        self._machine_revision_number = rev_number
        self._machine_guid = guid
        log.info("Machine BOM: %d", bom_number)
        log.info("Machine Revision: %d", rev_number)
        log.info("Machine GUID: %s", guid)

    # Gets the machine name and defines a default when it's not set or 'Ultimaker Jed'
    #  If a default name needs to be set, the settings are also saved
    #  @param settings The global settings
    #
    def _initMachineName(self):
        machine_name = self.__settings.getAsString("machine_name")
        if machine_name is None or machine_name == "Ultimaker Jedi":    # For backwards compatibility to early beta versions that used a static machine name
            # Create a user readable machine name based on MAC address
            self.setMachineName("Ultimaker-" + os.uname().nodename[-6:])   # last 6 characters of the hostname

    # Determine if the machine name is valid (RFC 952 and RFC 1123)
    #  @param machine_name The machine name to be validated
    #  @return Returns True if the name is valid, otherwise False
    def _isValidMachineName(self, machine_name):
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        return allowed.match(machine_name)

    ## @brief Get the version of the current install firmware
    #  @return Returns the current version
    @dbusif.method("", "s")
    def getVersion(self):
        return self._system_update.getVersion()

    ## @brief Check if a new firmware for the specified release type is available
    #  @param release_type The firmware release type to check
    @dbusif.method("s", "")
    def checkForFirmwareUpdates(self, release_type):
        log.info("Check for new firmware version for '%s' initiated...", release_type)
        self.__firmware_check = release_type
        self._system_update.getUpdateTargetVersions()

    ## @brief If a new version is found, send a signal
    def _handleFirmwareUpdate(self, key):
        if key == self.__firmware_check:
            if self._system_update.hasNewFirmware(self.__firmware_check):
                self.onHasNewFirmware(self.__firmware_check, self._system_update.getFirmwareVersion(self.__firmware_check))
            else:
                self.onHasNoNewFirmware(self.__firmware_check)

    ## @brief Signals that a new firmware version has been detected
    #  @param release_type Specifies which release type of firmware
    #  @param version The new version
    @dbusif.signal("ss")
    def onHasNewFirmware(self, release_type, version):
        log.info("New firmware detected for '%s', version: %s", release_type, version)
        return True

    ## @brief Signals that a new firmware version has been detected
    #  @param release_type Specifies which release type of firmware
    @dbusif.signal("s")
    def onHasNoNewFirmware(self, release_type):
        log.info("No firmware detected for '%s'", release_type)
        return True

    @dbusif.method("", "s")
    def getPlatform(self):
        return str(platform.platform())

    @dbusif.method("", "s")
    def getHostName(self):
        return str(platform.node())

    @dbusif.method("", "b")
    def isDeveloperModeActive(self):
        active = self.__settings.getAsBoolean(self.__PREF_KEY_DEVELOPER_MODE)
        if active is None:
            active = False
            self.__settings.setAsBoolean(self.__PREF_KEY_DEVELOPER_MODE, active)
        return active

    # Set the developer mode option on/off.
    # Changing the developer mode from on to off or the other way around will reboot the machine.
    # This is to make sure all systems are properly configured for developer mode or not. And is a lot easier to do than
    # re-configuring all services. This means users of the developermode flag do not need to worry about dynamic behaviour of this flag.
    @dbusif.method("b", "")
    def setDeveloperMode(self, active):
        if self.isDeveloperModeActive() != active:
            self.__settings.setAsBoolean(self.__PREF_KEY_DEVELOPER_MODE, active)
            self._reboot()

    # Set the current country code, according to https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
    # Currently this is only used for WiFi regulatory domain settings. But in the future this could be expanded for other reasons as well.
    @dbusif.method("s", "b")
    def setCountry(self, country_code):
        # Country codes need to be 2 letters. And should be in https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
        # For forwards compatbility, we just check 2 upper case characters.
        # Also allow an empty string, for "no country set", which is the default startup state.
        if not re.match("^[A-Z]{2}$", country_code) and country_code != "":
            return False
        if self.getCountry() != country_code:
            self.__settings.setAsString(self.__PREF_KEY_COUNTRY_CODE, country_code)
            self.countryChanged(country_code)  # Send the dbus signal that the country has changed.
        return True

    # DBus interface to get the currently active selected country code.
    # Will return an empty string for no country set, or a 2 upper case letter code according to https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2
    @dbusif.method("", "s")
    def getCountry(self):
        return self.__settings.getAsString(self.__PREF_KEY_COUNTRY_CODE, "")

    # Dbus signal, calling this function emits the dbus signal that the country has changed.
    @dbusif.signal("s")
    def countryChanged(self, country):
        log.info("Country changed: %s", country)

    @dbusif.method("s", "")
    def setLanguage(self, system_language):
        self.__settings.setAsString(self.__PREF_KEY_SYSTEM_LANGUAGE, system_language)
        self.languageChanged(system_language)

    @dbusif.method("", "s")
    def getLanguage(self):
        return self.__settings.getAsString(self.__PREF_KEY_SYSTEM_LANGUAGE, "")

    # Dbus signal, calling this function emits the dbus signal that the system language has changed.
    @dbusif.signal("s")
    def languageChanged(self, system_language):
        log.info("Language changed: %s", system_language)

    # If the specified machine name differs, it's set and saved into the settings file
    # @param string The new machine name
    # #return Returns True if machinename is valid and set, otherwise False
    @dbusif.method("s", "b")
    def setMachineName(self, machine_name):
        if self._isValidMachineName(machine_name):
            self.__settings.setAsString(self.__PREF_KEY_MACHINE_NAME, machine_name)
            self.machineNameChanged(machine_name)
            return True
        log.warning("Machine name '%s' is not a valid name", machine_name)
        return False

    # Returns the machine name from the settings file
    # @return string The machine
    @dbusif.method("", "s")
    def getMachineName(self):
        return self.__settings.getAsString(self.__PREF_KEY_MACHINE_NAME)

    # Dbus signal, calling this function emits the dbus signal that the machine name has changed.
    @dbusif.signal("s")
    def machineNameChanged(self, machine_name):
        log.info("Machine name changed: %s", machine_name)

    # Returns the current machine BOM and Revision number.
    # The BOM number is the number of the hardware assembly that is used to build this machine.
    # The Revision number is an optional revision.
    #
    # @return tuple of two integers. First one being the BOM number, second one being the revision number.
    #       The BOM is 0 if there is no BOM set yet.
    #       The revision is 0 if there is no revision set, or the revision is unknown. (default)
    @dbusif.method("", "(uu)")
    def getMachineBOM(self):
        return (self._machine_bom_number, self._machine_revision_number)

    # Return the machine GUID
    #
    # @return single string representing the GUID of the machine
    @dbusif.method("", "s")
    def getMachineGUID(self):
        return self._machine_guid

    ## @brief Initiate a factory reset. This will wipe all settings, and reboot the machine.
    #  @param reset_type Has a string parameter, which needs to contain the value "DESTROY" to make sure this function is not called by mistake.
    @dbusif.method("s", "")
    def factoryReset(self, reset_type):
        if reset_type not in (FactoryReset.FACTORY_RESET_KEYWORD_DESTROY, FactoryReset.FACTORY_RESET_KEYWORD_SOFT_DESTROY):
            return
        log.info("Factory Reset executing...")

        log.info("Executing factory resets for registered callbacks")
        for dbus_object_name, callback_function in self.__reset_callbacks.items():
            log.info("Calling reset callback function '%s' for dbus service '%s'", callback_function, dbus_object_name)
            ## Dirty hack because of naming issues
            if dbus_object_name == "auth_api":
                service = dbusif.RemoteObject("nl.ultimaker.interface.http", "interface/http/auth")
            else:
                service = dbusif.RemoteObject(dbus_object_name)
            try:
                method = getattr(service, callback_function)
                method(reset_type)
            except:
                # Ignore non existing function or dbus timeouts
                log.exception("Callable function '%s' does not exists for dbus service '%s'", callback_function, dbus_object_name)
                pass
        log.info("Callbacks finished")

        log.info("Cleaning up before reboot")
        if reset_type == FactoryReset.FACTORY_RESET_KEYWORD_SOFT_DESTROY:
            self.__settings.backupAndSetup(self.__SAVE_SETTINGS_ON_SOFT_RESET, self.__ADD_SETTINGS_ON_SOFT_RESET)
        else:
            self.__settings.erase()

        log.info("rebooting")
        self._reboot()

    ## @brief This function registers a callback function to the dictionary. Each callback will be
    #  executed (in random order) when a factory reset is called for
    #  @param dbus_service_name The name of the dbus service object
    #  @param function_name The name of the function to be called (implemented in the specified dbus service object)
    @dbusif.method("ss", "")
    def addFactoryResetCallback(self, dbus_service_name, function_name):
        assert dbus_service_name not in self.__reset_callbacks.keys()
        log.info("Added dbus service '%s' to the reset callbacks with function '%s'", dbus_service_name, function_name)
        self.__reset_callbacks[dbus_service_name] = function_name

    @dbusif.method("", "at")
    def getMemoryUsage(self):
        return self._getMemory()

    @dbusif.method("", "as")
    def getLog(self):
        p = subprocess.Popen(["/bin/journalctl", "-n250", "-o", "cat", "-u", "griffin.*"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()
        return out.decode("utf-8", "replace").split("\n")

    @dbusif.signal("")
    def onUpdateProcedureStart(self):
        pass

    # onReboot is fired when the system is going to reboot.
    @dbusif.signal("")
    def onReboot(self):
        pass

    @dbusif.method("", "sd")
    def getUpdateState(self):
        return self._system_update.getState(), self._system_update.getDownloadProgress()

    ## Initiate a system update. This will cause a download of the a new image, and this image will be checked with the signature keys in the current image.
    #  If the signature check is valid, then the update will be installed.
    #  As this is async process, feedback from this process is given with the getUpdateState function and onUpdateStart signal.
    #  @param release_type: One of the keys in the SystemUpdate.__SYSTEM_UPDATE_URLS dict. To indicate which type of firmware to install.
    @dbusif.method("s", "")
    def startUpdate(self, release_type):
        self._system_update.downloadUpdate(release_type)

    ## Initiate a system update. This will copy the given files to tmpfs, and this image will be checked with the signature keys in the current image.
    #  If the signature check is valid, then the update will be installed.
    #  As this is async process, feedback from this process is given with the getUpdateState function and onUpdateStart signal.
    #  @param update_filename: tar.xz filename to install, needs to exist in the system.
    #  @param signature_filename: tar.xz filename to install, needs to exist in the system.
    @dbusif.method("ss", "")
    def startLocalUpdate(self, update_filename, signature_filename):
        self._system_update.installLocalUpdate(update_filename, signature_filename)

    ## DBus method to get the possible update target types (given to "release_type") which their possible version number.
    #  @return a dictionary with the release_type as key, and the version string as value. The version string is empty when the version string is not yet known.
    @dbusif.method("", "a{ss}")
    def getUpdateTargetVersions(self):
        return self._system_update.getUpdateTargetVersions()

    def _getMemory(self):
        info = {}
        info["MemTotal"] = 0
        info["MemFree"] = 0
        info["Buffers"] = 0
        info["Cached"] = 0
        with open("/proc/meminfo", "r") as f:
            for line in f:
                key, value = map(str.strip, line.split(":", 2))
                try:
                    if value.endswith(" kB"):
                        info[key] = int(value[:-3]) * 1024
                    else:
                        info[key] = int(value)
                except ValueError:
                    pass
        total_memory = info["MemTotal"]
        total_free = info["MemFree"] + info["Buffers"] + info["Cached"]
        return [total_memory - total_free, total_memory]

    def _reboot(self):
        t = thread.Thread("reboot", self._rebootThread)
        t.start()

    def _rebootThread(self):
        log.info("Reboot starting")
        self.onReboot()
        self.__settings.forceSave()
        # Give the system 1 second to react on the reboot signal before we go and shutdown
        time.sleep(1.0)

        # the linux sync command to make sure all files are synced to disc before rebooting.
        p = subprocess.Popen(["/bin/sync"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        p.communicate()
        p.wait()

        p = subprocess.Popen(["/usr/bin/sudo", "/bin/systemctl", "reboot"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        log.info("rebooting %r\n%r", p.communicate())
        p.wait()
