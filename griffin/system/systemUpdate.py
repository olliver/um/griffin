import logging
import subprocess
import urllib.request
import os
import time
import socket
import shutil
from griffin import thread
from griffin import signal
from griffin import gpg

log = logging.getLogger(__name__.split(".")[-1])


# SystemUpdate object
# This class handles updating the system with new images.
# Updating the system is a relative complex process, the following features are supported:
# * Download a remote file
# * Checking a file signature (with gpg)
# * Initiate an update with the file
class SystemUpdate(signal.SignalEmitter):
    STATE_IDLE = "IDLE"
    STATE_INIT_DOWNLOAD = "INIT_DOWNLOAD"
    STATE_DOWNLOADING = "DOWNLOADING"
    STATE_COPYING = "COPYING"
    STATE_VERIFYING = "VERIFYING"
    STATE_INSTALLING = "INSTALLING"
    STATE_FAILED_DOWNLOAD = "FAILED_DOWNLOAD"
    STATE_FAILED_USB = "FAILED_USB"
    STATE_FAILED_SIGNATURE = "FAILED_SIGNATURE"

    # Location of the version identifcation file
    __VERSION_FILE = "/etc/jedi_version"
    # Special development version
    __DEVELOPMENT_VERSION = "DEVELOPMENT_VERSION"

    __IMAGE_TEMP_FILENAME = "/tmp/update.img"
    __IMAGE_TEMP_SIGNATURE_FILENAME = "/tmp/update.sig"

    __SYSTEM_UPDATE_URLS = {
        "stable": {
            "rootfs": "http://software.ultimaker.com/jedi/releases/latest.tar.xz",
            "signature": "http://software.ultimaker.com/jedi/releases/latest.tar.xz.sig",
            "version": "http://software.ultimaker.com/jedi/releases/latest.version",
        },
        "testing": {
            "rootfs": "http://software.ultimaker.com/jedi/releases/testing/latest.tar.xz",
            "signature": "http://software.ultimaker.com/jedi/releases/testing/latest.tar.xz.sig",
            "version": "http://software.ultimaker.com/jedi/releases/testing/latest.version",
        },
    }
    __HAS_VERSION_STRINGS_UPDATE_TIMEOUT = 60 * 60
    __NO_VERSION_STRINGS_UPDATE_TIMEOUT = 2 * 60

    onUpdateStart = signal.Signal()
    onFirmwareVersionLoaded = signal.Signal()

    def __init__(self):
        super().__init__()

        self.__gpg = gpg.GPG()
        self._update_thread = None

        self._download_progress = 0.0
        self._state = self.STATE_IDLE

        self.__update_versions = {}
        for key in self.__SYSTEM_UPDATE_URLS.keys():
            self.__update_versions[key] = ""
        self.__update_versions_thread = None

        socket.setdefaulttimeout(120)

    # Add a public key file to be used by the system update process.
    # Updates need to be signed with the private key to be accepted by the update process.
    def addPublicKey(self, key_filename):
        self.__gpg.addPublicKeyFile(key_filename)

    # Download an update and install it.
    # @param release_type: A key in the __SYSTEM_UPDATE_URLS dict to indicate which release should be downloaded.
    def downloadUpdate(self, release_type):
        # Check if there is already an update running.
        if self._update_thread is not None and self._update_thread.is_alive():
            return False

        # Get our urls from the __SYSTEM_UPDATE_URLS dict
        url_info = self.__SYSTEM_UPDATE_URLS.get(release_type, None)
        if url_info is None:
            return False
        image_url = url_info["rootfs"]
        image_signature_url = url_info["signature"]

        self._download_progress = 0.0
        self._state = self.STATE_INIT_DOWNLOAD

        self._update_thread = thread.Thread("SystemUpdate", self._downloadUpdateThread, (image_url, image_signature_url))
        self._update_thread.start()

    # Download an update and install it.
    #  @param update_filename: tar.xz filename to install, needs to exist in the system.
    #  @param signature_filename: tar.xz filename to install, needs to exist in the system.
    def installLocalUpdate(self, update_filename, signature_filename):
        # Check if there is already an update running.
        if self._update_thread is not None and self._update_thread.is_alive():
            return False

        self._download_progress = 0.0
        self._state = self.STATE_INIT_DOWNLOAD

        self._update_thread = thread.Thread("SystemUpdate", self._installLocalUpdateThread, (update_filename, signature_filename))
        self._update_thread.start()

    def getState(self):
        return self._state

    def getDownloadProgress(self):
        return self._download_progress

    ## Get the version strings for firmware updates.
    #  @return a string-string dictionary containing the known release names and version numbers we can update to.
    #          if a version string is not yet known, an empty string is returned.
    def getUpdateTargetVersions(self):
        # When the thread to get version numbers is not yet running, create a thread an fetch the version numbers.
        if self.__update_versions_thread is None or not self.__update_versions_thread.is_alive():
            self.__update_versions_thread = thread.Thread("SystemUpdateVersions", self.__retrieveVersionStringsThread, daemon=True)
            self.__update_versions_thread.start()
        return self.__update_versions

    ## @brief Read and parses the version file for the current version
    #  @return The current version
    def getVersion(self):
        if os.path.isfile(self.__VERSION_FILE):
            try:
                with open(self.__VERSION_FILE, "r") as f:
                    return ("".join(f.readlines())).strip()
            except:
                log.error("Error while accessing %s", self.__VERSION_FILE)

        return self.__DEVELOPMENT_VERSION

    ## @brief Determines if there is a new firmware version  of the specified release type
    #  @param release_type The release type
    #  @retval True if the current version is a development version or when there is actually a newer version
    #  @retval False if there is no new version (yet) known/available or when the current version is more recent
    def hasNewFirmware(self, release_type = "stable"):
        if release_type not in self.__update_versions:
            log.info("Release type '%s' version not yet available", release_type)
            return False

        current_version = self.getVersion()

        if "DEV" in current_version:
            # Takes care of the older [major].[minor].[revision]-DEV.YYYYMMDD version
            return True

        # Remove "<something>-" if it is in front of the version number
        current_version = current_version.split("-", 1)[-1]

        # Having the development version to be outdated all the time makes sense so that a development version can allways be overwritten by any version
        if (current_version == self.__DEVELOPMENT_VERSION):
            return True

        new_version = self.__update_versions[release_type]
        is_newer = False
        try:
            is_newer = self._createTupleFromVersion(new_version) > self._createTupleFromVersion(current_version)
        except ValueError:
            log.info("Error comparing release '%s' current version '%s' and new version '%s' - for safety assuming the current version is fine", release_type, current_version, new_version)
            return False

        log.info("Comparing release '%s' old version '%s' with new version '%s' with result: %r", release_type, current_version, new_version, is_newer)
        return is_newer

    ## @brief Converts a version string (major.minor.revision.YYYYMMDD) into seperate values (tuple of 4 values)
    #  @return Returns a tuple (major, minor, revision, YYMMDD)
    def _createTupleFromVersion(self, version):
        return tuple(map(int, (version.split("."))))

    ## @brief Gets the version of the specified firmware release type
    #  @return The version of a certain release type as a string or "" when release type does not exists
    def getFirmwareVersion(self, release_type):
        if release_type not in self.__update_versions:
            return ""
        return self.__update_versions[release_type]

    def _downloadProgressCallback(self, blocknum, blocksize, totalsize):
        if totalsize > 0:
            self._download_progress = float(blocknum * blocksize) / float(totalsize)

    def _downloadUpdateThread(self, image_url, image_signature_url):
        self.onUpdateStart.emit()

        try:
            log.info("Starting download of: %s", image_signature_url)
            urllib.request.urlretrieve(image_signature_url, self.__IMAGE_TEMP_SIGNATURE_FILENAME)
            self._state = self.STATE_DOWNLOADING
            log.info("Starting download of: %s", image_url)
            urllib.request.urlretrieve(image_url, self.__IMAGE_TEMP_FILENAME, self._downloadProgressCallback)
        except Exception:
            log.exception("Exception during download of update")
            self._state = self.STATE_FAILED_DOWNLOAD
            return
        self._installUpdateThread()

    def _installLocalUpdateThread(self, update_filename, signature_filename):
        self.onUpdateStart.emit()

        try:
            self._state = self.STATE_COPYING
            log.info("Copying: %s to tmpfs", signature_filename)
            shutil.copy(signature_filename, self.__IMAGE_TEMP_SIGNATURE_FILENAME)
            log.info("Copying: %s to tmpfs", update_filename)
            shutil.copy(update_filename, self.__IMAGE_TEMP_FILENAME)
        except Exception:
            log.exception("Exception during copy of update")
            self._state = self.STATE_FAILED_USB
            return
        self._installUpdateThread()

    def _installUpdateThread(self):
        self._state = self.STATE_VERIFYING
        log.info("Files downloaded")
        if not self._verifyImage():
            log.info("Signature failed to match.")
            self._state = self.STATE_FAILED_SIGNATURE
            return
        log.info("Signature verfication done")

        # Move the install image to the location where the update.rootfs.target service will look for it
        os.rename(self.__IMAGE_TEMP_FILENAME, "/tmp/rootfs.tar.xz")

        self._state = self.STATE_INSTALLING
        # After going to installing state, wait for 5 seconds before we actually do anything, this to give the rest of the system time to notify the user that we are going to update.
        # As as soon as we initiate the update command, all our services will be killed, and there won't be a way to notify the user.
        time.sleep(5)
        log.info("Going to update the system now!")
        p = subprocess.Popen(["sudo", "/bin/systemctl", "isolate", "update.rootfs.target"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        p.wait()

    # Verify if the image we have checks out with any of the public keys we have.
    def _verifyImage(self):
        return self.__gpg.verifySignature(self.__IMAGE_TEMP_FILENAME, self.__IMAGE_TEMP_SIGNATURE_FILENAME)

    ## @brief The thread function in which version numbers are retrieved from the server.
    def __retrieveVersionStringsThread(self):
        log.info("running __retrieveVersionStringsThread()")
        for key in self.__update_versions.keys():
            try:
                with urllib.request.urlopen(self.__SYSTEM_UPDATE_URLS[key]["version"], timeout=60) as f:
                    version = f.read()
                self.__update_versions[key] = version.decode("utf-8").strip()
                log.info("Version for release type '%s' found: %s", key, self.__update_versions[key])
            except Exception:
                log.exception("Error during updating of the update version strings")
            self.onFirmwareVersionLoaded.emit(key)

        if all(self.__update_versions.values()):
            # If we have all versions, only update once a hour
            time.sleep(self.__HAS_VERSION_STRINGS_UPDATE_TIMEOUT)
        else:
            # If we do not have all version strings yet, update every 2 minutes.
            time.sleep(self.__NO_VERSION_STRINGS_UPDATE_TIMEOUT)
