import logging
import fcntl
import time
import queue

from threading import Thread

from griffin.beep.beep import Beep

from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])


# Interface to the beeper attached to the system.
# This uses a queue to handle the beeps (in case beeps are added when another beep is playing)
class BeepService(dbusif.ServiceObject):
    _instance = None

    # ioctl value from <linux/kd.h> (added here as the python icotl bindings dont have it.
    _KIOCSOUND = 0x4B2F     # [CodeStyle: constant but not recognised as such]

    # Magical number! Stole this from the beep console command.
    # Background:
    # This number represents the fixed frequency of the original PC XT's
    # timer chip (the 8254 AFAIR), which is approximately 1.193 MHz. This
    # number is divided with the desired frequency to obtain a counter value,
    # that is subsequently fed into the timer chip, tied to the PC speaker.
    # The chip decreases this counter at every tick (1.193 MHz) and when it
    # reaches zero, it toggles the state of the speaker (on/off, or in/out),
    # resets the counter to the original value, and starts over. The end
    # result of this is a tone at approximately the desired frequency.
    _SAMPLING_RATE = 1193180    # [CodeStyle: constant but not recognised as such]

    __MAX_QUEUE_SIZE = 20

    def __init__(self):
        super().__init__("beep")
        self._instance = self
        self._fd = None

        self._tty = open("/dev/tty0", "wb")
        self._fd = self._tty.fileno()

        log.info("Started beep service: %s" % self._fd)
        self._queue = queue.Queue(BeepService.__MAX_QUEUE_SIZE)
        self._beep_thread = Thread(target = self._handleBeep)
        self._beep_thread.daemon = True
        self._beep_thread.start()

    def _handleBeep(self):
        while True:
            beep_data = self._queue.get()   # Blocks until a new beep is on the queue
            self._beep(beep_data.frequency, beep_data.duration)

    @dbusif.method("ii", "")
    def beep(self, frequency, duration):
        try:
            self._queue.put(Beep(frequency, duration), block=False)
        except queue.Full:
            log.warning("Dropping the beep!")

    def _beep(self, frequency, duration):
        if duration > 5000:
            duration = 5000     # We probably never want beeps longer than 5 sec
        if frequency:
            period = int(self._SAMPLING_RATE / frequency)
        else:
            period = 0
        fcntl.ioctl(self._fd, self._KIOCSOUND, period)
        time.sleep(duration / 1000)
        fcntl.ioctl(self._fd, self._KIOCSOUND, 0)   # disable sound.

    @classmethod
    def getInstance(cls):
        if cls._instance is None:
            # assume usage from service as constructor is the first/only thing that happens in the running BeepService
            cls._instance = dbusif.RemoteObject("beep")
        return cls._instance
