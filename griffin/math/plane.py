## Modified version of the UM.Math.Plane file from Uranium.
#  Modified to no longer use numpy, provides the same general interface.
from .vector3 import Vector3


##      Plane representation using normal and distance.
class Plane:
    ## Construct a new plane from a 3D vector normal direction and a distance.
    #  @normal: A Vector3 representing the normal vector of this plane.
    #  @distance: Float, distance between the origin and this plane.
    def __init__(self, normal=Vector3(), distance=0.0):
        super().__init__()

        assert isinstance(normal, Vector3)

        self.__normal = normal
        self.__distance = distance

    @property
    def normal(self):
        return self.__normal

    @property
    def distance(self):
        return self.__distance

    ## Intersect a 3D infinite ray with this plane.
    #  @param origin: A Vector3 where the ray starts.
    #  @param direction: A Vector3 pointing in the direction of the ray.
    #  @return a float giving the position on the ray where the ray intersects the plane. Or False if there is no intersection.
    def intersectRay(self, origin, direction):
        assert isinstance(origin, Vector3)
        assert isinstance(direction, Vector3)

        # w is the vector between the origin of the plane and the origin of the ray.
        w = origin - (self.__normal * self.__distance)

        n_dot_r = self.__normal.dot(direction.normalized())
        n_dot_w = -self.__normal.dot(w)

        ## The ray is laying on the plane, so we cannot calculate an intersection point.
        if n_dot_r == 0.0:
            return False

        return n_dot_w / n_dot_r

    ## Method to generate a plane from 3 points in 3D space.
    # @param v0: Vector3
    # @param v1: Vector3
    # @param v2: Vector3
    # @return a Plane object that crosses the 3 points.
    @classmethod
    def fitPlane(cls, v0, v1, v2):
        assert isinstance(v0, Vector3)
        assert isinstance(v1, Vector3)
        assert isinstance(v2, Vector3)

        v_delta_1_0 = v1 - v0
        v_delta_2_0 = v2 - v0
        normal = v_delta_2_0.cross(v_delta_1_0).normalized()

        distance = v0.dot(normal)

        return cls(normal, distance)

    def __repr__(self):
        return "Plane(normal = {0}, distance = {1})".format(self.__normal, self.__distance)
