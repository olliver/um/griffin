## Modified version of the UM.Math.Vector file from Uranium.
#  Modified to no longer use numpy, provides the same general interface.
import math


##  Simple 3D-vector class
#
#   This class represents an immutable 3-dimensional vector.
class Vector3(object):
    ##  Initialize a new vector
    #   @param x: X coordinate of vector.
    #   @param y: Y coordinate of vector.
    #   @param z: Z coordinate of vector.
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self._x = float(x)
        self._y = float(y)
        self._z = float(z)

    ##  Return the x component of this vector
    @property
    def x(self):
        return self._x

    ##  Return the y component of this vector
    @property
    def y(self):
        return self._y

    ## Return the z component of this vector
    @property
    def z(self):
        return self._z

    def normalized(self):
        length = self.length()
        if length != 0:
            return self / length
        else:
            return self

    def length(self):
        return math.sqrt(self._x * self._x + self._y * self._y + self._z * self._z)

    def dot(self, other):
        return Vector3(self._x * other.x + self._y * other.y + self._z * other.z)

    def cross(self, other):
        return Vector3(self._y * other.z - other.y * self._z, other.x * self._z - self._x * other.z, self._x * other.y - self._y * other.x)

    def __eq__(self, other):
        if self is other:
            return True
        if other is None:
            return False
        return self.equals(other)

    ## Compares this vector to another vector.
    #
    #   \param epsilon optional tolerance value for the comparison.
    #   \returns True if the two vectors are the same.
    def equals(self, other, epsilon=1e-6):
        return abs(self.x - other.x) < epsilon and \
               abs(self.y - other.y) < epsilon and \
               abs(self.z - other.z) < epsilon

    def __add__(self, other):
        if type(other) is Vector3:
            return Vector3(self._x + other.x, self._y + other.y, self._z + other.z)
        else:
            return Vector3(self._x + other, self._y + other, self._z + other)

    def __iadd__(self, other):
        return self + other

    def __sub__(self, other):
        if type(other) is Vector3:
            return Vector3(self._x - other.x, self._y - other.y, self._z - other.z)
        else:
            return Vector3(self._x - other, self._y - other, self._z - other)

    def __isub__(self, other):
        return self - other

    def __mul__(self, other):
        if type(other) is Vector3:
            return Vector3(self._x * other.x, self._y * other.y, self._z * other.z)
        else:
            return Vector3(self._x * other, self._y * other, self._z * other)

    def __imul__(self, other):
        return self * other

    def __rmul__(self, other):
        return self * other

    def __truediv__(self, other):
        if type(other) is Vector3:
            return Vector3(self._x / other.x, self._y / other.y, self._z / other.z)
        else:
            return Vector3(self._x / other, self._y / other, self._z / other)

    def __itruediv__(self, other):
        return self / other

    def __neg__(self):
        return Vector3(-self._x, -self._y, -self._z)

    def __repr__(self):
        return "Vector({0}, {1}, {2})".format(self._x, self._y, self._z)

    def __lt__(self, other):
        return self._x < other.x and self._y < other.y and self._z < other.z

    def __gt__(self, other):
        return self._x > other.x and self._y > other.y and self._z > other.z

    def __le__(self, other):
        return self._x <= other.x and self._y <= other.y and self._z <= other.z

    def __ge__(self, other):
        return self._x >= other.x and self._y >= other.y and self._z >= other.z


def _isNumber(value):
    return type(value) in [float, int]
