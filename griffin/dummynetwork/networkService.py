import logging

from griffin import dbusif

log = logging.getLogger(__name__.split(".")[-1])

class NetworkService(dbusif.ServiceObject):
    def __init__(self):
        super().__init__("network")
        self.__ssid = None
        self.__password = None

    @dbusif.method("", "")
    def setModeAutoConnect(self):
        log.debug("This function is a stub")

    @dbusif.method("", "")
    def setModeHotspot(self):
        log.debug("This function is a stub")

    @dbusif.method("s", "")
    def setModeWifiSetup(self, ssid):
        log.debug("This function is a stub")
        log.debug("ssid: %s", ssid)

    @dbusif.method("", "")
    def setModeCable(self):
        log.debug("This function is a stub")

    @dbusif.method("", "")
    def setModeWireless(self):
        log.debug("This function is a stub")

    @dbusif.method("", "")
    def setModeOffline(self):
        log.debug("This function is a stub")

    @dbusif.method("", "s")
    def getMode(self):
        log.debug("This function is a stub")
        return "CABLE"

    @dbusif.signal("s")
    def modeChanged(self, new_mode):
        log.debug("This function is a stub")
        log.debug("new_mode: %s", new_mode)

    @dbusif.method("", "s")
    def getWifiSetupState(self):
        log.debug("This function is a stub")
        return "FAILED"

    @dbusif.method("ss", "")
    def setHotspotParameters(self, ssid, password):
        log.debug("This function is a stub")
        log.debug("ssid: %s", ssid)
        log.debug("password: %s", password)
        self.__ssid = ssid
        self.__password = password

    @dbusif.method("", "s")
    def getHotspotSSID(self):
        log.debug("This function is a stub")
        return self.__ssid

    @dbusif.method("", "s")
    def getHotspotPassword(self):
        log.debug("This function is a stub")
        return self.__password

    @dbusif.method("", "aa{sv}")
    def getWifiNetworks(self):
        log.debug("This function is a stub")
        return []

    @dbusif.method("", "s")
    def getConnectedMethod(self):
        log.debug("This function is a stub")
        return "ETHERNET"

    @dbusif.method("", "s")
    def getIPv4Address(self):
        log.debug("This function is a stub")
        return ""

    @dbusif.method("", "s")
    def getIPv6Address(self):
        log.debug("This function is a stub")
        return ""

    @dbusif.method("", "s")
    def getNetworkName(self):
        log.debug("This function is a stub")
        return ""

    @dbusif.method("", "a{ss}")
    def getMacAddresses(self):
        log.debug("This function is a stub")
        results = { "00:00:00:00:00", "00:00:00:00:00" }
        return ""

    @dbusif.method("ss", "")
    def connectToWifiNetwork(self, network_id, password):
        log.debug("This function is a stub")
        log.debug("network_id: %s", network_id)
        log.debug("password: %s", password)

    @dbusif.method("s", "")
    def forgetWifiNetwork(self, network_id):
        log.debug("This function is a stub")
        log.debug("network_id: %s", network_id)

    @dbusif.method("s", "")
    def resetSettings(self, reset_type):
        log.debug("This function is a stub")
        log.debug("reset_type: %s", reset_type)
