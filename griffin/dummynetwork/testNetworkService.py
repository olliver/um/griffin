import unittest
import time
from griffin.network import networkService


class NetworkServiceTestCase(unittest.TestCase):

    maxDiff = None

    def setUp(self):
        self._network_service = networkService.NetworkService()

    def testEthernetPower(self):
        self.assertEqual(self._network_service.isEthernetPowered(), False)
        self._network_service.setEthernetPower(True)
        self.assertEqual(self._network_service.isEthernetPowered(), True)
        self._network_service.setEthernetPower(False)
        self.assertEqual(self._network_service.isEthernetPowered(), False)

    def testEthernetConnection(self):
        self.assertEqual(self._network_service.isEthernetConnected(), False)
        self._network_service.setEthernetConnection(True)
        self.assertEqual(self._network_service.isEthernetConnected(), True)
        self._network_service.setEthernetConnection(False)
        self.assertEqual(self._network_service.isEthernetConnected(), False)

    def testWifiPowerScan(self):
        self.assertEqual(self._network_service.isWifiPowered(), False)
        self._network_service.setWifiPower(True)
        self.assertEqual(self._network_service.isWifiPowered(), True)
        self.assertEqual(self._network_service.scanWifi(), True)
        self._network_service.setWifiPower(False)
        self.assertEqual(self._network_service.isWifiPowered(), False)
        self.assertEqual(self._network_service.scanWifi(), False)

    def testWifiConnection(self):
        self.assertEqual(self._network_service.isWifiConnected(), False)
        self._network_service.setWifiConnection(True)
        self.assertEqual(self._network_service.isWifiConnected(), True)
        self._network_service.setWifiConnection(False)
        self.assertEqual(self._network_service.isWifiConnected(), False)

    def testHotspotEnable(self):
        self.assertEqual(self._network_service.isHotspotEnabled(), False)
        self.assertEqual(self._network_service.isPermanentHotspot(), False)
        self._network_service.setHotspotEnabled(True, False)
        self.assertEqual(self._network_service.isHotspotEnabled(), True)
        self.assertEqual(self._network_service.isPermanentHotspot(), False)
        self._network_service.setHotspotEnabled(False, True)
        self.assertEqual(self._network_service.isHotspotEnabled(), False)
        self.assertEqual(self._network_service.isPermanentHotspot(), True)
        self._network_service.setHotspotEnabled(True, True)
        self.assertEqual(self._network_service.isHotspotEnabled(), True)
        self.assertEqual(self._network_service.isPermanentHotspot(), True)
        self._network_service.setHotspotEnabled(False, False)
        self.assertEqual(self._network_service.isHotspotEnabled(), False)
        self.assertEqual(self._network_service.isPermanentHotspot(), False)
        self._network_service.setWifiPower(True)
        self._network_service.setHotspotEnabled(True, False)
        time.sleep(1)
        self.assertEqual(self._network_service.getRemainingHotspotTimeout(), 299)
        self._network_service.setWifiPower(False)
        self._network_service.setHotspotEnabled(False, False)

    def testAirPlaneModeEnable(self):
        self.assertEqual(self._network_service.isAirplaneModeEnabled(), False)
        self._network_service.setAirplaneModeEnabled(True)
        self.assertEqual(self._network_service.isAirplaneModeEnabled(), True)
        self.assertEqual(self._network_service.isEthernetPowered(), False)
        self.assertEqual(self._network_service.isEthernetPowered(), False)
        self.assertEqual(self._network_service.isHotspotEnabled(), False)
        self._network_service.setEthernetPower(True)
        self._network_service.setWifiPower(True)
        self._network_service.setHotspotEnabled(True, False)
        self._network_service.setAirplaneModeEnabled(False)
        self.assertEqual(self._network_service.isAirplaneModeEnabled(), False)
        self.assertEqual(self._network_service.isEthernetPowered(), False)
        self.assertEqual(self._network_service.isWifiPowered(), False)
        self.assertEqual(self._network_service.isHotspotEnabled(), False)
        self._network_service.setAirplaneModeEnabled(True)
        self.assertEqual(self._network_service.isAirplaneModeEnabled(), True)
        self.assertEqual(self._network_service.isEthernetPowered(), True)
        self.assertEqual(self._network_service.isWifiPowered(), True)
        self.assertEqual(self._network_service.isHotspotEnabled(), True)

    def testHotspotTimeout(self):
        self.assertEqual(self._network_service.getHotspotTimeout(), 60 * 5)
        self._network_service.setHotspotTimeout(60)
        self.assertEqual(self._network_service.getHotspotTimeout(), 60)

    def testHotspotPassphrase(self):
        self.assertEqual(self._network_service.getHotspotPassphrase(), "um123456")
        self._network_service.setHotspotPassphrase("um12345678")
        self.assertEqual(self._network_service.getHotspotPassphrase(), "um12345678")

    def testHotspotSSID(self):
        self.assertEqual(self._network_service.getHotspotSSID(), "Jedi")
        self._network_service.setHotspotSSID("JediHotspot")
        self.assertEqual(self._network_service.getHotspotSSID(), "JediHotspot")

    def testNetworkServicesInfoConnectForgetWifiNetwork(self):
        self.assertListEqual(self._network_service.getNetworkServicesInfo(True), [("/net/connman/service/wifi_000e8e4888ab_546573745f666f725f4a656469_managed_psk", "online", "", "Office network", "wifi", 80, True, False, True, "100.101.102.103", "0000:1111:2222:3333:aaaa:bbbb", ["psk", "ieee821x"]), ("/org/connman/ethernet/1", "ready", "", "Wired network", "ethernet", 0, True, False, True, "200.201.202.203", "4444:5555:6666:7777:cccc:dddd", [""]), ("/", "online", "", "Jedi", "tethering", 0, False, False, False, "192.168.1.1", "fe80::f835:5dff:fe5f:a03e", ["psk"])])
        self.assertEqual(self._network_service.connectWifiNetwork("/net/connman/service/wifi_000e8e4888ab_546573745f666f725f4a656469_managed_psk", "um123456"), True)
        self.assertEqual(self._network_service.disconnectWifiNetwork("/net/connman/service/wifi_000e8e4888ab_546573745f666f725f4a656469_managed_psk"), True)
        self.assertEqual(self._network_service.disconnectWifiNetwork("/net/connman/service/wifi_000e8e4888ab_53574e_managed_psk"), False)
        self.assertEqual(self._network_service.connectWifiNetwork("/org/connman/ethernet/1", "um123456"), False)
        self.assertEqual(self._network_service.connectWifiNetwork("/org", "um123456"), False)
        self.assertEqual(self._network_service.forgetWifiNetwork("/net/connman/service/wifi_000e8e4888ab_546573745f666f725f4a656469_managed_psk"), True)
        self.assertListEqual(self._network_service.getNetworkServicesInfo(False), [("/org/connman/ethernet/1", "ready", "", "Wired network", "ethernet", 0, True, False, True, "200.201.202.203", "4444:5555:6666:7777:cccc:dddd", [""]), ("/net/connman/service/wifi_000e8e4888ab_53574e_managed_psk", "failure", "invalid-key", "Guest network", "wifi", 75, False, False, True, "255.255.255.255", "8888:9999:0000:1111:eeee:ffff", ["psk"]), ("/", "online", "", "Jedi", "tethering", 0, False, False, False, "192.168.1.1", "fe80::f835:5dff:fe5f:a03e", ["psk"])])
        self.assertEqual(self._network_service.connectWifiNetwork("/net/connman/service/wifi_000e8e4888ab_546573745f666f725f4a656469_managed_psk", "um123456"), False)
        self.assertEqual(self._network_service.forgetWifiNetwork("/org/connman/ethernet/1"), False)
        self.assertEqual(self._network_service.forgetWifiNetwork("/org"), False)

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(NetworkServiceTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
