import numpy
import time
import logging

from griffin import thread

log = logging.getLogger(__name__.split(".")[-1])


## Class for high volume data logging.
#  This class keeps track of a large volume of data in memory.
#  This data is stored in rows of equal size so numpy can be used to store the data, which is memory space efficient.
#  For each record the time (time.monotonic) is also stored.
class DataLogger:
    __instances = {}

    # Default amount of memory used for a data logger.
    __DEFAULT_MAX_MEMORY = 3

    ## Create a new data logger. It will be stored for global reference by the name given.
    #  @param name string: name to give this data logger.
    #  @param amount_of_data_columns: The amount of columns stored in this DataLogger.
    #  @param data_type: The data type stored in this data logger. Must be a numpy dtype.
    #  @param maximum_memory_usage_mb: Maximum memory claimed for the numpy array, in megabytes. This is used to calculate the amount of rows that we can store.
    def __init__(self, name, data_column_names, data_type=numpy.float32, maximum_memory_usage_mb=None):
        assert name not in DataLogger.__instances
        if maximum_memory_usage_mb is None:
            maximum_memory_usage_mb = DataLogger.__DEFAULT_MAX_MEMORY
        amount_of_data_columns = len(data_column_names) + 1

        self.__max_rows = int(maximum_memory_usage_mb * 1024 * 1024 / amount_of_data_columns / data_type().itemsize)
        self.__data = numpy.zeros((self.__max_rows, amount_of_data_columns), dtype=data_type)
        self.__data_column_names = ["Time"] + data_column_names
        self.__insert_row_index = 0
        self.__lock = thread.RLock()
        log.info("Created data logger %s with %d rows", name, self.__max_rows)
        DataLogger.__instances[name] = self

    ## Get the names of all the data columns
    #  @return array of strings that contain the names of all the data columns stored in the DataLogger
    def getColumnNames(self):
        return self.__data_column_names.copy()

    ## Add a new row of data to this log.
    #  This overrides the oldest log row.
    #  @param args: Data points that will be stored in this DataLogger, amount must match the amount_of_data_columns given during construction of this class.
    def addData(self, *args):
        with self.__lock:
            self.__data[self.__insert_row_index][0] = time.monotonic()
            self.__data[self.__insert_row_index][1:] = args
            self.__insert_row_index = (self.__insert_row_index + 1) % self.__max_rows

    ## Get data rows from this logger.
    #  @param max_row_count: The maximum amount of rows to return. This can be more then the amount of rows stored in this DataLogger, and will be clamped to that amount.
    #  @return: a new numpy array containing the data rows.
    def getData(self, max_row_count):
        max_row_count = min(max_row_count, self.__max_rows)
        with self.__lock:
            start_index = self.__insert_row_index - max_row_count
            end_index = self.__insert_row_index - 1
            if end_index == -1:
                return self.__data[start_index:].copy()
            elif start_index < 0:
                return numpy.concatenate((self.__data[start_index:], self.__data[0:end_index+1]), 0)
            return self.__data[start_index:end_index+1].copy()

    ## Get a DataLogger instance that was created.
    #  @param name: The name given during creation of this data logger.
    #  @return: A DataLogger instance.
    @staticmethod
    def getInstance(name):
        return DataLogger.__instances.get(name, None)
