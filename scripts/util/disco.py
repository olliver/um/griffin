import subprocess
import re
import os
import pexpect
import json
import sys
from griffin import thread
import time
import requests
import logging

log = logging.getLogger(__name__.split(".")[-1])

def send(ip_addr, gcode):
    url = 'http://'+ip_addr+"/api/v1/debug/gcode"
    tmp = requests.post(url, json = {"gcode":gcode}).json()
    try:
        return tmp["reply"]
    except:
        return tmp


def getHsbToRgb(hue, saturation, brightness):
    # Turn HSV values into RGB.
    c = brightness / 100.0 * saturation / 100.0
    x = c * (1.0 - abs(((hue / 60.0) % 2.0) - 1.0))
    m = brightness / 100.0 - c

    if hue < 60:
        r = c + m
        g = x + m
        b = m
    elif hue < 120:
        r = x + m
        g = c + m
        b = m
    elif hue < 180:
        r = m
        g = c + m
        b = x + m
    elif hue < 240:
        r = m
        g = x + m
        b = c + m
    elif hue < 300:
        r = x + m
        g = m
        b = c + m
    else:
        r = c + m
        g = m
        b = x + m

    r = int(r * 255)
    g = int(g * 255)
    b = int(b * 255)

    return [r,g,b]

def setLeds(hue, saturation, brightness):
    rgb = getHsbToRgb(hue, saturation, brightness)
    r = rgb[0]; g = rgb[1]; b = rgb[2]
    # Use the min total value of the RGB to set the white led.
    # And dim the RGB leds by that amount.
    # (Note, this assumes the LEDs are linear, which they are not reality. But it is close enough)
    w = min(r, g, b, 255) # white can't be more than 255,
    # now if brightness is over 100 the rgb leds can add some light!
    r -= w
    g -= w
    b -= w
    # M142 r255 g150 b150 w0 = white (enough)
    rgbw = [r, g, b, w]
    rgb_factors = [1.0, 0.5882352941176471, 0.5882352941176471]
    rgbw = [int(rgbw[i] * rgb_factors[i]) for i in range(0, len(rgb)) ] + [w]

    return 'M142 r%d g%d b%d w%d' % tuple(rgbw)

threads = {}

class LedHue():
    def __init__(self, ip):
        self._ip = ip
        self._thread = None

    def start(self):
        if self._thread is None:# can only start once    
            self._thread = thread.Thread("%s state manager thread" % self._ip, self.run)
            self._thread.daemon = True
            self._thread.start()
            print("started thread for "+self._ip)

    def run(self):
        hue = 0
        while True:
            try:
                send(self._ip, setLeds(hue, 100, 100))
                hue += 110
                if hue > 360:
                    hue -= 360
                time.sleep(.5)

            except:
                log.exception("some exception in hue thread")
                # end thread and remove reference so a new one can be spawned.
                print("ending thread for "+self._ip)
                del(threads[self._ip])


while True:
    printers_list = subprocess.check_output("avahi-browse -p -k _ultimaker._tcp -t -r", shell = True)
    #printers_list = sys.stdin.readline()
    for ip in re.findall("(" + "\.".join([ "\d{1,3}" for i in range(4) ]) + ")", str(printers_list)):
        if not ip in threads:
            threads[ip] = LedHue(ip)
            threads[ip].start()

    time.sleep(60)


