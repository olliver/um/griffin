import sys
import json
from PyQt5.QtWidgets import QWidget, QPushButton, QApplication
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtCore import QUrl
from PyQt5.QtCore import QTimer
from PyQt5.QtNetwork import QHttpMultiPart, QHttpPart, QNetworkRequest, QNetworkAccessManager



class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

        self.__address = "10.180.0.25"
        self.__nam = QNetworkAccessManager()
        self.__nam.finished.connect(self._onFinished)
        self.__nam.authenticationRequired.connect(self._onAuthenticationRequired)

        self.__auth_id = None
        self.__auth_key = None

        self.__nam.get(QNetworkRequest(QUrl("http://" + self.__address + "/api/v1/auth/verify")))
        #self._doUpload()

        self.__test_auth_timer = QTimer()
        self.__test_auth_timer.setInterval(2000)
        self.__test_auth_timer.setSingleShot(False)
        self.__test_auth_timer.timeout.connect(self._checkAuth)

    def initUI(self):
        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Blablabla')
        self.show()

    def _doUpload(self):
        file_name = "test.gcode.gz"

        self.___post_multi_part = QHttpMultiPart(QHttpMultiPart.FormDataType)

        self.___post_part = QHttpPart()
        self.___post_part.setHeader(QNetworkRequest.ContentDispositionHeader, "form-data; name=\"file\"; filename=\"%s\"" % file_name)
        self.___post_part.setBody("bla".encode())
        self.___post_multi_part.append(self.___post_part)

        self.___url = QUrl("http://" + self.__address + "/api/v1/print_job")
        self.___post_request = QNetworkRequest(self.___url)

        self.__nam.post(self.___post_request, self.___post_multi_part)

    def _checkAuth(self):
        self.__nam.get(QNetworkRequest(QUrl("http://" + self.__address + "/api/v1/auth/verify")))

    def _onFinished(self, reply):
        status_code = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)
        url = reply.url().toString()
        if url.endswith("auth/verify"):
            if status_code == 401:
                print("Auth verification failed, requesting a new auth key.")
                request = QNetworkRequest(QUrl("http://" + self.__address + "/api/v1/auth/request"))
                request.setHeader(QNetworkRequest.ContentTypeHeader, "application/json")
                self.__nam.post(request, json.dumps({"application": "test", "user": "test"}))
            elif status_code == 200:
                print("Auth verification success")
                #self.__test_auth_timer.stop()
                #self._doUpload()
            else:
                print("Auth failed for unknown reason: %d" % (status_code))

        elif url.endswith("auth/request"):
            data = json.loads(bytes(reply.readAll()).decode("utf-8"))
            self.__auth_id = data.get("id")
            self.__auth_key = data.get("key")
            print("Got new auth id, waiting for authorization: %s" % (self.__auth_id))
            self.__nam.get(QNetworkRequest(QUrl("http://" + self.__address + "/api/v1/auth/check/%s" % (self.__auth_id))))
        elif '/auth/check/' in url:
            data = json.loads(bytes(reply.readAll()).decode("utf-8"))
            if data.get("message", "unknown") == "unknown":
                # TODO: Do not hammer the API like this. This is just an example, you should do this on a timer.
                self.__nam.get(QNetworkRequest(QUrl("http://" + self.__address + "/api/v1/auth/check/%s" % (self.__auth_id))))
            elif data.get("message", "unknown") == "authorized":
                print("User allowed access. Testing authentication.")
                self.__test_auth_timer.start()
            else:
                print("User denied you access...")
        else:
            print("%s:%d" % (url, status_code))
            print(bytes(reply.readAll()).decode("utf-8"))

    def _onAuthenticationRequired(self, reply, authenticator):
        if self.__auth_id is not None:
            authenticator.setUser(self.__auth_id)
            authenticator.setPassword(self.__auth_key)

if __name__ == '__main__':

    app = QApplication(sys.argv)
    ex = Example()

    sys.exit(app.exec_())
