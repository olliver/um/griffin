import subprocess
import re
import os
import pexpect
import json
import sys
from threading import Thread
import time
import requests

# this script is intended to keep the buildplate of your printer warm!

def send(ip_addr, gcode):
    url = 'http://'+ip_addr+"/api/v1/debug/gcode"
    tmp = requests.post(url, json = {"gcode":gcode}).json()
    try:
        return tmp["reply"]
    except:
        return tmp


threads = {}

class KeepWarm():
    def __init__(self, ip):
        self._ip = ip
        self._thread = None

    def start(self):
        if self._thread is None:# can only start once    
            self._thread = Thread(target = self.run)
            self._thread.daemon = True
            self._thread.start()
            print("started thread for "+self._ip)

    def run(self):
        hue = 0
        while True:
            try:
                send(self._ip, "M140 S60")
                time.sleep(5)
            except:
                log.exception("some exception in keepwarm thread")
                # end thread and remove reference so a new one can be spawned.
                #print("ending thread for "+self._ip)
                #del(threads[self._ip])


while True:
    #printers_list = subprocess.check_output("avahi-browse -p -k _ultimaker._tcp -t -r", shell = True)
    printers_list = sys.stdin.readline()
    for ip in re.findall("(" + "\.".join([ "\d{1,3}" for i in range(4) ]) + ")", str(printers_list)):
        if not ip in threads:
            threads[ip] = KeepWarm(ip)
            threads[ip].start()

    #time.sleep(60)


