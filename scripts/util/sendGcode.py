import json
import sys
import requests
ip_addr = sys.argv[1]

def send(gcode):
    url = "http://"+ip_addr+"/api/v1/debug/gcode"
    tmp = requests.post(url, json = {"gcode":gcode}).json()
    try:
        return tmp["reply"]
    except:
        return tmp

for line in sys.stdin:
    print(send(line))
