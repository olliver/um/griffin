import time
import random
from griffin import dbusif

class Test:
    def __init__(self):
        self.p = dbusif.RemoteObject("printer")

    def step(self):
        procedures = {}
        try:
            for p, s in self.p.getActiveProcedures():
                procedures[p] = s
        except:
            return
        
        if "MANUAL_LEVEL_BED_WIZARD" in procedures:
            step = procedures["MANUAL_LEVEL_BED_WIZARD"]
            if step == "WAIT_MOVE_NEXT_POS_1":
                if self.moveBed():
                    self.p.messageProcedure("MANUAL_LEVEL_BED_WIZARD", "MOVE_NEXT_POS")
            if step == "WAIT_MOVE_NEXT_POS_2":
                self.p.messageProcedure("MANUAL_LEVEL_BED_WIZARD", "MOVE_NEXT_POS")
            if step == "WAIT_MOVE_NEXT_POS_3":
                self.p.messageProcedure("MANUAL_LEVEL_BED_WIZARD", "MOVE_NEXT_POS")
            if step == "WAIT_MOVE_NEXT_POS_4":
                if self.moveBed():
                    self.p.messageProcedure("MANUAL_LEVEL_BED_WIZARD", "MOVE_NEXT_POS")
            if step == "WAIT_MOVE_NEXT_POS_5":
                self.p.messageProcedure("MANUAL_LEVEL_BED_WIZARD", "MOVE_NEXT_POS")
            if step == "WAIT_DONE":
                self.p.messageProcedure("MANUAL_LEVEL_BED_WIZARD", "DONE")
        elif "MANUAL_NOZZLE_OFFSET_WIZARD" in procedures:
            step = procedures["MANUAL_NOZZLE_OFFSET_WIZARD"]
            if step == "WAIT":
                if self.moveBed():
                    self.p.messageProcedure("MANUAL_NOZZLE_OFFSET_WIZARD", "CONTINUE")
        elif "SET_BED_LEVEL_CORRECTION_MODE_WIZARD" in procedures:
            step = procedures["SET_BED_LEVEL_CORRECTION_MODE_WIZARD"]
            if step == "BED_LEVEL_CORRECTION_MODE":
                self.p.messageProcedure("SET_BED_LEVEL_CORRECTION_MODE_WIZARD", "manual")
        elif "MANUAL_BED_PLUS_NOZZLE_OFSET_WIZARD" not in procedures:
            self.p.startProcedure("MANUAL_BED_PLUS_NOZZLE_OFSET_WIZARD", {"": ""})

    def moveBed(self):
        self.p.startProcedure("MOVE_BED", {"target": "0.25"})
        return random.randint(0, 100) < 5

    def _onProcedureStart(self, procedure, step):
        print("Start:", procedure, step)

t = Test()
while True:
    t.step()
    time.sleep(0.2)
