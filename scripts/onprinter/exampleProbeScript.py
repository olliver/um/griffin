# place file on printer in /usr/share/griffin/
# run file while in that directory.

from griffin import dbusif
from griffin.thread import Event, Thread
import json
import numpy
import sys, os

Thread("dbus_loop", dbusif.runMainLoop).start()

probe_done = Event()
home_done = Event()

def onProcedureFinished(procedure):
    if procedure == "PROBE":
        probe_done.set()
    elif procedure.startswith("HOME_") or procedure == "SWITCH_ACTIVE_HOTEND":
        home_done.set()

printer = dbusif.RemoteObject("printer")

printer.connectSignal("onProcedureFinished", onProcedureFinished)

printer.startProcedure("HOME_BED", {"dummy": 0})
home_done.wait()
home_done.clear()
printer.startProcedure("HOME_HEAD", {"dummy": 0})
home_done.wait()
home_done.clear()
printer.startProcedure("SWITCH_ACTIVE_HOTEND", {"target": 0})
home_done.wait()
home_done.clear()
timestamp = None

output = {}
TMP_FILE = "/tmp/current_bed_progress.json"

if os.path.exists(TMP_FILE):
    with open(TMP_FILE, "r") as f:
        output = json.load(f)
    for key in output:
        output[key][1] = True

    yn = "?"
    while yn != "y" and yn != "n" and yn != "":
        print("found %d records, resume y/n [y]?" % len(output.values()))
        yn = sys.stdin.readline().strip()

    if yn == "n":
        output = {}
                 # don't probe again the first time

size = {"X": 215, "Y": 215}
step_size = 5
probe_locations = []

for x in range(step_size, size["X"]-step_size, step_size):
    for y in range(step_size, size["Y"]-step_size, step_size):
        probe_locations.append([x,y])

center_height = None
loop_count = 0
while(len([i for i in output if output[i][1]]) != len(probe_locations) and loop_count < 10):
    loop_count += 1
    for x, y in probe_locations:
        if output.get(str(x) + ","+ str(y), [0, False])[1]:
            continue

        redo_loop = True
        while(redo_loop): # loop until both center and probe were without subsequent errors, we don't want the homing switch inaccuracy messing this up.
            redo_loop = False
            try:
                printer.startProcedure("PROBE", {"x":x, "y": y, "z": 10}) # z is the start probing height
                probe_done.wait()
                probe_done.clear()
                data = printer.getProcedureMetaData("PROBE")
                assert data["last_probe_timestamp"] != timestamp
                timestamp = data["last_probe_timestamp"]

                if data["loopcount_of_probe"] > 0:
                    center_height = None

                print("probe at ({x}, {y}) is {z}".format(x=x, y=y, z=data["last_probe_height"]))

                ## add known probe in center of bed
                if center_height is None:
                    printer.startProcedure("PROBE", {"x":size["X"]/2, "y": size["Y"]/2, "z": 10}) # z is the start probing height
                    probe_done.wait()
                    probe_done.clear()
                    data_center = printer.getProcedureMetaData("PROBE")
                    assert data_center["last_probe_timestamp"] != timestamp
                    timestamp = data_center["last_probe_timestamp"]
                    if data_center["loopcount_of_probe"] > 0:
                        redo_loop = True
                    center_height = data_center["last_probe_height"]
                    print("new center_height: %f" % center_height)

                output[str(x) + ","+ str(y)] = [data["last_probe_height"] - center_height, False]
                with open(TMP_FILE, "w") as f:
                    json.dump(output, f, indent=4)

            except Exception:
                redo_loop = True



    ## do statistical analysis
    # determine standard deviation
    values = list(output.values())
    values = numpy.array([unit[0] for unit in values])
    std_dev = numpy.std(values)
    print("standard deviation this run: %f" % std_dev)
    for key in output:
        ## key is valid if less then 3 std deviations of the norm
        output[key][1] = abs(output[key][0]) < std_dev * 3

## save data
save = {key: output[key][0] for key in output}
with open("bed_probes.json", "w") as f:
    json.dump(save, f, indent=4)




