import time

from griffin import dbusif


## Class to judge the PID controller on a few criteria.
#  Cools down and heats up hotend slot 0 with the current PID settings and measures how long it takes to heat up,
#  and how much over/undershoot it gets after that.
#  This can be used to give the PID settings a score and objectively measure different PID settings in exactly the same way.
class PIDJudge:
    def __init__(self, target=200):
        self.__slot = dbusif.RemoteObject("printer", "printer/head/0/slot/0")
        self.__target_temperature = target

        self.__heating_start_time = 0.0
        self.__heating_finished_time = 0.0
        self.__top_temperature = 0.0
        self.__bottom_temperature = 0.0
        self.__average_diff = 0.0

    # Run the PID judge.
    def run(self):
        print("Running PID judge: %gC" % (self.__target_temperature))
        print("Settings: Kff: %g Kp: %g Ki: %g Kd: %g Ki_max: %g" % (self.__slot.getProperty("pid_Kff"), self.__slot.getProperty("pid_Kp"), self.__slot.getProperty("pid_Ki"), self.__slot.getProperty("pid_Kd"), self.__slot.getProperty("pid_Ki_max")))
        self._setTarget(0.0)
        while self._getCurrent() > 60:
            time.sleep(0.2)
        self._setTarget(self.__target_temperature)
        while self._getCurrent() < 60:
            time.sleep(0.2)
        self.__heating_start_time = time.monotonic()
        while self._getCurrent() < self.__target_temperature:
            time.sleep(0.2)
        self.__heating_finished_time = time.monotonic()
        self.__top_temperature = self._getCurrent()
        self.__bottom_temperature = self._getCurrent()
        self.__average_diff = 0.0
        sample_count = 0
        while time.monotonic() - self.__heating_finished_time < 60.0:
            time.sleep(0.2)
            current = self._getCurrent()
            self.__top_temperature = max(self.__top_temperature, current)
            self.__bottom_temperature = min(self.__bottom_temperature, current)
            self.__average_diff += abs(current - self.__target_temperature)
            sample_count += 1
        self.__average_diff /= sample_count
        self._setTarget(0.0)

    # Report the results of a run to stdout.
    def report(self):
        print("Heating up time: %g" % (self.__heating_finished_time - self.__heating_start_time))
        print("Overshoot: %g" % (self.__top_temperature - self.__target_temperature))
        print("Undershoot: %g" % (self.__bottom_temperature - self.__target_temperature))
        print("Average diff: %g" % (self.__average_diff))
        print("Score: %g" % (self.score()))
        print("Settings: Kff: %g Kp: %g Ki: %g Kd: %g Ki_max: %g" % (self.__slot.getProperty("pid_Kff"), self.__slot.getProperty("pid_Kp"), self.__slot.getProperty("pid_Ki"), self.__slot.getProperty("pid_Kd"), self.__slot.getProperty("pid_Ki_max")))

    # Get the resulting score after a pid run.
    # @return float: A score given to this PID run, a lower value is better.
    def score(self):
        heat_time_score = (self.__heating_finished_time - self.__heating_start_time) - 100.0
        overshoot_score = (self.__top_temperature - self.__target_temperature) * 3.0
        undershoot_score = (self.__bottom_temperature - self.__target_temperature) * 3.0
        average_diff_score = self.__average_diff * 10.0
        return heat_time_score + overshoot_score + undershoot_score + average_diff_score

    def _setTarget(self, target):
        self.__slot.setProperty("pre_tune_target_temperature", target)

    def _getCurrent(self):
        return self.__slot.getProperty("current_temperature")


## An attempt at a PID tuner. Note that this class takes about 24 hours to run.
#  Experimental code that just tries a whole bunch of different settings and prints the result to stdout.
class PIDTuner:
    def __init__(self):
        self.__defaults = {"Kff": 0.5, "Kp": 2.0, "Ki": 0.5, "Kd": 100.0, "Ki_max": 80}
        self.__steps = {}
        for k, v in self.__defaults.items():
            self.__steps[k] = v / 10.0
        self.__variations = []
        self.__buildVariations({}, list(self.__steps.keys()))

    def run(self):
        results = []
        for variation in self.__variations:
            self.__apply(variation)
            pj = PIDJudge()
            pj.run()
            pj.report()
            results.append("%s %g" % (str(variation), pj.score()))
        for result in results:
            print(result)

    def __apply(self, variation):
        slot = dbusif.RemoteObject("printer", "printer/head/0/slot/0")
        for k, v in variation.items():
            slot.setProperty("pid_" + k, v)

    def __buildVariations(self, data, keys):
        if len(keys) == 0:
            self.__variations.append(data.copy())
        else:
            key = keys.pop(0)
            data[key] = self.__defaults[key] + self.__steps[key]
            self.__buildVariations(data, keys.copy())
            data[key] = self.__defaults[key]
            self.__buildVariations(data, keys.copy())
            data[key] = self.__defaults[key] - self.__steps[key]
            self.__buildVariations(data, keys.copy())

if __name__ == "__main__":
    if True:
        pj = PIDJudge()
        pj.run()
        pj.report()
    else:
        PIDTuner().run()
