import time
from griffin import dbusif

# This script will forever run the Auto Bed Leveling wizard.

class Test:
    def __init__(self):
        self._printer = dbusif.RemoteObject("printer")
        _count = 0;

    def step(self):
        # Get all active procedures
        active_procedures = {}
        try:
            for p, s in self._printer.getActiveProcedures():
                active_procedures[p] = s
        except:
            return
        
        # Start procedure if not running yet.
        if "AUTO_LEVEL_BED" not in active_procedures:
            self._printer.startProcedure("AUTO_LEVEL_BED", {"": ""})

t = Test()
while True:
    t.step()
    time.sleep(0.2)
