#!/usr/bin/python3

import cmd
import sys
import traceback

from griffin import dbusif


class GriffinCmd(cmd.Cmd):
    __SERVICES = [
        "beep",
        "camera",
        "interface.http interface/http/auth",
        "led",
        "material",
        "nfc",
        "printer",
        "printer printer/head/0"
        "printer printer/head/0/slot/0",
        "printer printer/head/0/slot/1",
        "printer printer/bed",
        "system",
    ]

    def __init__(self):
        super().__init__()
        self.__selected_service = dbusif.RemoteObject("printer")

    def do_select(self, args):
        'Select a service to command.'
        try:
            self.__selected_service = dbusif.RemoteObject(*args.split(" ", 1))
        except:
            traceback.print_exc()

    def complete_select(self, text, line, begidx, endidx):
        return self.__completeFromList(text, self.__SERVICES)

    def do_get(self, args):
        '''
        Get a specific property value from the selected service and print it.
        Example: "get current_temperature target_temperature"
        '''
        for key in args.split(" "):
            try:
                print("%s = %s" % (key, self.__selected_service.getProperty(key)))
            except:
                traceback.print_exc()

    def do_set(self, args):
        'Set a specific property to a value on the selected service.'
        try:
            key, value = args.split(" ", 1)
            try:
                value = float(value)
            except:
                pass
            if not self.__selected_service.setProperty(key, value):
                print("Failed to set property.")
        except:
            traceback.print_exc()

    def do_exec(self, args):
        'Execute python code in the selected service.'
        try:
            print(self.__selected_service.debugExecCode(args))
        except:
            traceback.print_exc()

    def do_list(self, args):
        'List all the properties and their values in the selected service'
        try:
            for key, value in sorted(self.__selected_service.getProperties().items()):
                print("%s = %s" % (key, value))
        except:
            traceback.print_exc()

    def do_sendgcode(self, args):
        print(dbusif.RemoteObject("printer").debugSendGCode(args, timeout=120))

    def do_exit(self, args):
        'Exit the command tool.'
        return True

    def __completeFromList(self, text, items):
        results = []
        for service in items:
            if service.startswith(text):
                results.append(service)
        return results

if __name__ == "__main__":
    if len(sys.argv) > 1:
        GriffinCmd().onecmd(" ".join(sys.argv[1:]))
    else:
        GriffinCmd().cmdloop()
