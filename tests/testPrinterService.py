import os
import sys
import time
import unittest
import threading
from subprocess import Popen

sys.path.insert(0, os.path.abspath( sys.path[0]+"/.."))
from griffin import dbusif

## TestCase for functions exposed by the PrinterService
# of course there are a whole bunch of things that need to be setup
# for the dummy printerService to run on the host so the test may fail due to that.
class PrinterServiceTestCase(unittest.TestCase):

    ## Start Services (uncomment those needed)
    #
    def setUp(self):
        self._startSystemService()
        self._startMaterialService()
        self._startSystemService()
        self._startPrinterService()
        self._thread = threading.Thread(target = dbusif.runMainLoop)
        self._thread.daemon = True
        self._thread.start()

    ## Start the system service
    #
    def _startSystemService(self):
        self._po_system_service = Popen(args=["python3.4", "main.py", "griffin.system.systemService", "SystemService"])
        time.sleep(1)
        self._system_service = dbusif.RemoteObject("system")

    ## Start the material service
    #
    def _startMaterialService(self):
        self._po_material_service = Popen(args=["python3.4", "main.py", "griffin.material.materialService", "MaterialService"])
        time.sleep(1)
        self._material_service = dbusif.RemoteObject("material")

    ## Start the printer service
    #
    def _startPrinterService(self):
        self._po_printer_service = Popen(args=["python3.4", "main.py", "griffin.printer.printerService", "PrinterService"])
        time.sleep(1)
        self._printer_service = dbusif.RemoteObject("printer")
        time.sleep(2)
        self._printer_service.connectSignal("onProcedureStart", self._printerProcedureStart)
        self._printer_service.connectSignal("onProcedureNextStep", self._printerProcedureNextStep)
        self._printer_service.connectSignal("onProcedureFinished", self._printerProcedureFinished)

    ## Stop the services (uncomment those needed - should be the same bunch as in setUp)
    #
    def tearDown(self):
        self._stopPrinterService()
        self._stopMaterialService()
        self._stopSystemService()
        dbusif.abortMainLoop()

    ## Stops the printer service
    #
    def _stopPrinterService(self):
        self._printer_service.cleanSignals()
        del(self._printer_service)
        self._printer_service = None
        self._po_printer_service.terminate()

    ## Stops the material service
    #
    def _stopMaterialService(self):
        self._material_service.cleanSignals()
        del(self._material_service)
        self._material_service = None
        self._po_material_service.terminate()

    ## Stops the system service
    #
    def _stopSystemService(self):
        self._system_service.cleanSignals()
        del(self._system_service)
        self._system_service = None
        self._po_system_service.terminate()

    ## simple do a print and wait for it to finish test.
    #
    def testPrint(self):
        #self._printer_service.connectSignal("onProcedureNextStep", self._printerProcedureNextStep)
        time.sleep(3)
        self._printer_service.startProcedure("PRINT", {"url": "file://" + os.path.abspath(sys.path[1] + "/../test_gcode_files/008PLA Traffic cone_ dual material.gcode.gz"), "name": "test"})

        self._waiting = True
        t0 = time.monotonic()

        while self._waiting:
            time.sleep(15)
            self.assertLess(time.monotonic(), t0 + 15 * 60, "timeout, print should have finished!")
        #TODO: add test for resulting gcode?

    def _printerProcedureStart(self, procedure_key, step_key):
        #print("_printerProcedureStart(%s, %s)" % (procedure_key, step_key))
        pass

    def _printerProcedureNextStep(self, procedure_key, step_key):
        #print("_printerProcedureNextStep(%s, %s)" % (procedure_key, step_key))
        if procedure_key == "PRINT" and step_key == "WAIT_FOR_CLEANUP":
            self._printer_service.messageProcedure("PRINT", "PRINTER_CLEANED")
            self._waiting = False

    def _printerProcedureFinished(self, procedure_key):
        #print("_printerProcedureFinished(%s)" % procedure_key)
        pass

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(PrinterServiceTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
