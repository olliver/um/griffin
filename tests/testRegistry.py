import os
import sys
import unittest
import json
import time

sys.path.insert(0, os.path.abspath( sys.path[0]+"/.."))

from griffin.preferences.registry import Registry
from griffin.preferences.registryFile import RegistryFile

# Helper class for dictionaries
class DictDiffer(object):
  """
  Calculate the difference between two dictionaries as:
  (1) items added
  (2) items removed
  (3) keys same in both but changed values
  (4) keys same in both and unchanged values
  """
  def __init__(self, current_dict, past_dict):
    self.current_dict, self.past_dict = current_dict, past_dict
    self.set_current, self.set_past = set(current_dict.keys()), set(past_dict.keys())
    self.intersect = self.set_current.intersection(self.set_past)

  def added(self):
    return self.set_current - self.intersect

  def removed(self):
    return self.set_past - self.intersect

  def changed(self):
    return set(o for o in self.intersect if self.past_dict[o] != self.current_dict[o])

  def unchanged(self):
    return set(o for o in self.intersect if self.past_dict[o] == self.current_dict[o])

## TestCase for functions exposed by the Registry component
class RegistryFileTestCase(unittest.TestCase):

    ## Full qualified path name for the json test file
    __PREFERENCES_FILE = "/tmp/test.json"
    ## Whether or not to display output (comparison original with expected, good for debugging tests)
    __DISPLAY_OUTPUT = False

    ## @brief Clean up and create empty settings
    def setUp(self):
        self.__removePreferences()
        # Write settings every second
        self.__registry = RegistryFile(self.__PREFERENCES_FILE, 1)

    ## @brief Clean up after tests
    def tearDown(self):
        self.__removePreferences()
        pass

    ## @brief Remove the preferences file
    def __removePreferences(self):
        try:
            os.remove(self.__registry.getFilename())
        except:
            pass

    ## @brief Simple set value / wait for save / verify save test (implies get/set test)
    #  Because of the 1 it will be done first - if it fails, other test are kinda useless
    def test1SavePreference(self):
        # Arrange
        key = "TestKey"
        value = "dummy_value"

        # Act
        self.__registry.setAsString(key, value)
        settings = self.__loadSettingsFromFile()

        # Assert
        self.assertEqual(value, settings[key])


    ## @brief Test unknown key with default value
    #  Because of the 1 it will be done first - if it fails, other test are kinda useless
    def testUnkownkKeySetting(self):
        # Arrange
        key = "KeyDoesNotExist"
        default_value = "the_default_value"

        # Act
        value = self.__registry.get(key, default_value)

        # Assert
        self.assertEqual(value, default_value)

    ## @brief Test getAsString conversions
    def testGetAsString(self):
        # Arrange
        settings = { "int": 101, "float": 13.501, "bool": True, "string": "test_case" }
        expected = { "int": "101", "float": "13.501", "bool": "True", "string": "test_case" }

        # Act
        self.__updateRegistry(settings)

        # Assert
        for key, value in expected.items():
            actual_value = self.__registry.getAsString(key)
            if self.__DISPLAY_OUTPUT:
                print("Expecting value %s for key %s with original value %s => actual value: %s" % (value, key, settings[key], actual_value))
            self.assertEqual(value, actual_value)

    ## @brief Test setAsString conversions
    def testSetAsString(self):
        # Arrange
        settings = { "int": 101, "float": 13.501, "bool": True, "string": "test_case" }
        expected = { "int": "101", "float": "13.501", "bool": "True", "string": "test_case" }

        # Act
        for key, value in settings.items():
            self.__registry.setAsString(key, value)

        # Assert
        for key, value in expected.items():
            actual_value = self.__registry.get(key)
            if self.__DISPLAY_OUTPUT:
                print("Expecting value %s for key %s with original value %s => actual value: %s" % (value, key, settings[key], actual_value))
            self.assertEqual(value, actual_value)

    ## @brief Test getAsBoolean conversions
    def testGetAsBoolean(self):
        # Arrange
        settings = { "int_zero": 0, "int_not_zero": 1, "int_positive": 1, "float": 13.501, "bool_true": True, "bool_false": False, "string_yes": "yes", "string_true": "true", "string_no": "no", "string_false": "false", "string_1": "1", "string_0": "0", "float_0": "0.00", "float_not_zero": "13.37" }
        expected = { "int_zero": False, "int_not_zero": True, "int_positive": True, "float": True, "bool_true": True, "bool_false": False, "string_yes": True, "string_true": True, "string_no": False, "string_false": False, "string_1": True, "string_0": False, "float_0": False, "float_not_zero": True }

        # Act
        self.__updateRegistry(settings)

        # Assert
        for key, value in expected.items():
            actual_value = self.__registry.getAsBoolean(key)
            if self.__DISPLAY_OUTPUT:
                print("Expecting value %s for key %s with original value %s => actual value: %s" % (value, key, settings[key], actual_value))
            self.assertEqual(value, actual_value)

    ## @brief Test setAsBoolean conversions
    def testSetAsBoolean(self):
        # Arrange
        settings = { "int_zero": 0, "int_not_zero": 1, "int_positive": 1, "float": 13.501, "bool_true": True, "bool_false": False, "string_yes": "yes", "string_true": "true", "string_no": "no", "string_false": "false", "string_1": "1", "string_0": "0", "float_0": "0.00", "float_not_zero": "13.37" }
        expected = { "int_zero": False, "int_not_zero": True, "int_positive": True, "float": True, "bool_true": True, "bool_false": False, "string_yes": True, "string_true": True, "string_no": False, "string_false": False, "string_1": True, "string_0": False, "float_0": False, "float_not_zero": True }

        # Act
        for key, value in settings.items():
            self.__registry.setAsBoolean(key, value)

        # Assert
        for key, value in expected.items():
            actual_value = self.__registry.get(key)
            if self.__DISPLAY_OUTPUT:
                print("Expecting value %s for key %s with original value %s => actual value: %s" % (value, key, settings[key], actual_value))
            self.assertEqual(value, actual_value)

    ## @brief Test some unable conversions (from string to int and vv)
    def testUnableConversions(self):
        # Arrange
        settings = { "not_a_number": "nan", "not_numeric": "abc" }
        expected = { "not_a_number": None, "not_numeric": None }

        # Act
        for key, value in settings.items():
            self.__registry.setAsString(key, value)

        # Assert
        for key, value in expected.items():
            actual_value = self.__registry.getAsInt(key)
            if self.__DISPLAY_OUTPUT:
                print("Expecting value %s for key %s with original value %s => actual value: %s" % (value, key, settings[key], actual_value))
            self.assertEqual(value, actual_value)

    ## @brief Test setting a dictionary value as registry
    def testSetAsRegistry(self):
        # Arrange
        settings = { "int": 101, "float": 13.501, "bool": True, "string": "test_case" }
        key = "test_reg"
        len_settings = len(settings)

        # Act
        registry = Registry(settings)
        self.__registry.setAsRegistry(key, registry)

        # Assert
        expected = self.__registry.get(key)
        self.assertTrue(expected, dict)
        diff = DictDiffer(expected, settings)
        self.assertEqual(len_settings, len(expected))
        self.assertEqual(len_settings, len(diff.unchanged()))

    ## @brief Test getting a registry value
    def testGetAsRegistry(self):
        # Arrange
        settings = { "int": 101, "float": 13.501, "bool": True, "string": "test_case" }
        key = "test_reg"
        len_settings = len(settings)

        # Act
        self.__registry.set(key, settings)

        # Assert
        registry = self.__registry.getAsRegistry(key)
        self.assertTrue(isinstance(registry, Registry))
        value = registry.getAll()
        self.assertEqual(len_settings, len(value))
        diff = DictDiffer(value, settings)
        self.assertEqual(len_settings, len(diff.unchanged()))

    ## @brief Test setting a registry using a boolean value
    def testSetBooleanAsRegistry(self):
        # Arrange
        setting = True
        key = "test_set_boolean_registry"
        expected = None

        # Act
        registry = Registry()
        registry.setAsRegistry(key, setting)

        # Assert
        value = registry.get(key)
        self.assertEqual(expected, value)

    ## @brief Test getting a registry using from a boolean value
    def testGetAsRegistryFromBoolean(self):
        # Arrange
        setting = True
        key = "test_get_boolean_registry"
        expected = None

        # Act
        registry = Registry()
        registry.set(key, setting)

        # Assert
        value = registry.getAsRegistry(key)
        self.assertEqual(expected, value)

    ## @brief Test getting a registry using from an unknown key
    def testGettingRegistryFromUnknownKey(self):
        # Arrange
        key = "test_bogus_registry"
        expected = { }

        # Act
        registry = Registry()
        value = registry.getAsRegistry(key)

        # Assert
        self.assertTrue(isinstance(value, Registry))
        self.assertEqual(expected, value.getAll())

    ## @brief Test setting a list
    def testSetAsList(self):
        # Arrange
        settings = [ "aap", "noot", "mies" ]
        key = "list_set_key"

        # Act
        self.__registry.setAsList(key, settings)

        # Assert
        value = self.__registry.get(key)
        self.assertTrue(isinstance(value, list))
        self.assertEqual(len(settings), len(value))
        self.assertTrue(settings == value)

    ## @brief Test getting a list
    def testGetAsList(self):
        # Arrange
        settings = [ "aap", "noot", "mies" ]
        key = "list_get_key"

        # Act
        self.__registry.set(key, settings)

        # Assert
        value = self.__registry.getAsList(key)
        self.assertTrue(isinstance(value, list))
        self.assertEqual(len(settings), len(value))
        self.assertTrue(settings == value)

    ## @brief Test setting a list using a boolean value
    def testSetBooleanAsList(self):
        # Arrange
        setting = True
        key = "test_set_boolean_list"
        expected = None

        # Act
        registry = Registry()
        registry.setAsList(key, setting)

        # Assert
        value = registry.get(key)
        self.assertEqual(expected, value)

    ## @brief Test get list from a boolean
    def testGetAsListFromBoolean(self):
        # Arrange
        setting = True
        key = "test_get_boolean_list"
        expected = None

        # Act
        registry = Registry()
        registry.set(key, setting)

        # Assert
        value = registry.getAsList(key)
        self.assertEqual(expected, value)

    ## @brief Test getting a list using from an unknown key
    def testGettingListFromUnknownKey(self):
        # Arrange
        key = "test_bogus_list"
        expected = [ ]

        # Act
        registry = Registry()
        value = registry.getAsList(key)

        # Assert
        self.assertTrue(isinstance(value, list))
        self.assertEqual(expected, value)

    ## @brief Test nested registry
    def testNestedRegistry(self):
        # Arrange
        settings = {
            "nozzle": {
                "1": {
                    "material_id": "6976d020-18d1-4d46-9f3e-411189a1e230",
                    "skew": [ 0.0, -0.02],
                    "size": 0.4
                },
                "2": {
                    "material_id": "6976d020-18d1-4d46-9f3e-411189a1e230",
                    "skew": [ 0.1, -0.01],
                    "size": 0.25
                }
            }
        }
        key = "head"
        self.__registry.set(key, settings)

        # Act
        head = self.__registry.getAsRegistry(key)
        self.assertTrue(head != None)
        nozzle = head.getAsRegistry("nozzle")

        nozzle_1 = nozzle.getAsRegistry("1", None)
        nozzle_2 = nozzle.getAsRegistry("2", None)
        nozzle_3 = nozzle.getAsRegistry("3")

        nozzle_1.setAsInt("max_temperature", 120)
        nozzle_2.setAsFloat("size", 0.4)
        nozzle_3.setAsFloat("new_data", 0.4)

        # Assert
        self.assertTrue(nozzle_1 != None)
        self.assertTrue(nozzle_2 != None)
        updated_settings = self.__registry.get(key)
        self.assertTrue("max_temperature" in updated_settings["nozzle"]["1"])
        self.assertEqual(120, updated_settings["nozzle"]["1"]["max_temperature"])
        self.assertEqual(0.4, updated_settings["nozzle"]["2"]["size"])
        self.assertEqual(0.4, updated_settings["nozzle"]["3"]["new_data"])

    ## @brief Loads the json file created by the registry and return the contents as a dictionary
    #  @return Returns a dictionary instance of the jsond data file
    def __loadSettingsFromFile(self):
        time.sleep(2)
        settings = { }
        print("loading file " + self.__registry.getFilename())
        with open(self.__registry.getFilename(), "r") as f:
            settings = json.load(f)
        return settings

    ## @brief Add settings to the registry
    #  @param settings A dictionary of settings
    def __updateRegistry(self, settings):
        for key, value in settings.items():
            self.__registry.set(key, value)

## Main entry to create the instance and execute the test(s)
if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(RegistryFileTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
