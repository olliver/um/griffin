import os
import sys
import time
import unittest
sys.path.insert(0, os.path.abspath( sys.path[0]+"/.."))

# function to be called by the timer.
def func(name, count, first_time):
    tn = time.monotonic() - first_time
    print("%s %.3f %d" % (name, tn, count[1]))
    count[1] += 1
    if name == "22  sec" and tn > 11:
        count[3] += abs((tn - 1) % count[0])
    else:
        count[3] += abs(tn % count[0])


class TimerTestCase(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test(self):
        from griffin.timer import Timer
        #           name     : [delay,  count,  Timer,  accumulated error,  expected_count]
        timers = {  "1   sec": [1,      0,      None,   0,                  int(20/1)],
                    "22  sec": [2,      0,      None,   0,                  int(10/2+9/2)],
                    "600msec": [0.6,    0,      None,   0,                  int(20/0.6)],
                    "55555 s": [5,      0,      None,   0,                  int(20/5)     ]}

        dummy =                [7,      0,      None,   0,                  1]
        dummy_name = "once upon a time"

        print("\nstarting timers")
        first_time = time.monotonic()
        for t_key in timers:
            timers[t_key][2] = Timer(t_key, timers[t_key][0], func, t_key, timers[t_key], first_time).setSingleShot(False)
            timers[t_key][2].start()

        Timer(dummy_name, 7, func, dummy_name, dummy, first_time).start()

        time.sleep(11)
        timers["22  sec"][2].restart()
        time.sleep(9)

        print("stopping timers")
        for t_key in timers:
            timers[t_key][2].stop()

        err = 0
        times = 0
        for t_key in timers:
            err += timers[t_key][3]
            times += timers[t_key][1]

        err += dummy[3]
        times += 1
        average_error = err/times
        print("average error %f (slightly off due to start stop of 2 second timer)" % average_error)

        self.assertLess(average_error, .007, ("average time difference between expected runtime and actual run-time is too high\n"
                                            + "(average error %f should be smaller than .007 seconds)") % average_error)

        for t_key in timers:
            self.assertEqual(timers[t_key][1], timers[t_key][4], ("Timer(%s) did not run the expected amount of times\n"
                                                                + "(%d in stead of the expected %d)") % (t_key, timers[t_key][1], timers[t_key][4]))

        self.assertEqual(dummy[1], dummy[4], ("Timer(%s) did not run the expected amount of times\n"
                                            + "(%d in stead of the expected %d)") % (dummy_name, dummy[1], dummy[4]))

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TimerTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
