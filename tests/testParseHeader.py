import unittest
import copy
import sys
import os
sys.path.insert(0, os.path.abspath( sys.path[0]+"/.."))

## this class stands in for the controller and the systemservice.
class DummyController:
    def __init__(self):
        self._properties = {
            "machine_type_name": "Ultimaker 3 unknown",
            "marlin_firmware_filename": "jedi.hex",
            "hotend_count": 2,
            "feature_heated_bed": True,
            "feature_bed_leveling_sensor": True,
            "bowden_tube_length": 700,
            "build_volume_x": 220,
            "build_volume_y": 220,
            "build_volume_z": 210
        }

    def getPropertyValue(self, property_name):
        return self._properties[property_name]

    def getSystem(self):
        return self
    
    def isDeveloperModeActive(self):
        return False

    def getHotendCartridgeManager(self):
        return DummyHotendManager()

## this class fakes the HotendCartridgeManager
class DummyHotendManager():
    def __init__(self):
        self._properties = {
            "major_version_number": 1,
            "minor_version_number": 0,
            "manufacturer_id":      "Ultimaker\u0000\u0000\u0000\u0000",
            "hotend_cartridge_id":  "AA 0.4\u0000\u0000\u0000\u0000",
            "PID_Kp":               0,
            "PID_Ki":               0,
            "PID_Kd":               0,
            "hardware_revision":    0,
            "crc8_page_0":          True,

            "nozzle_size":          0.4,
            "hot_zone_size":        21,
            "maximum_temperature":  350,
            "filament_size":        2.85,
            "angle":                70,
            "flat_size":            0.3,
            "insert_type":          "teflon",
            "crc8_page_1":          True,

            "last_material":        "ffffffffffffffffffffffffffffffff",
            "material_extruded":    666,
            "time_spend_hot":       666,
            "max_exp_temperature":  666,
            "crc8_page_2":          True,
            "crc8_page_3":          True
        }

    def isPresent(self, hotend_nr):
        if hotend_nr >= 0 and hotend_nr < 2:
            return True
        return False

    def getValue(self, hotend_nr, key):
        return self._properties[key]

## this class generates and parses the header.
class HeaderParseTestCase(unittest.TestCase):
    ## list of header items each item has a list like this:
    # [type, (value_1, valid_value_bool_1), (value_2, valid_value_bool_2), .... (value_N, valid_value_bool_N) ]
    # on the special value None the item is omitted for the header.
    # the first tuple in the list should be accepted! so there is a guaranteed right header on which variations can be made.
    HEADER = {
        ";HEADER_VERSION": [float, (0.1, True), (None, False), (0, False)], # no header falls back to valid no-header data with only a FaultHandler warning
        ";FLAVOR":[str, ("Griffin", True), (None, False), ("dsfsd", False)],
        ";GENERATOR.NAME": [str, ("Cura_SteamEngine", True), (None, True)],
        ";GENERATOR.VERSION": [str, ("2.1.99-master.20160510", True), (None, True)],
        ";GENERATOR.BUILD_DATE": [str, ("2016-05-10", True), (None, True), ("216-05-10", False)],
        ";EXTRUDER_TRAIN.0.INITIAL_TEMPERATURE": [float, (210.4, True), (0.1, True), (0, True), (None, True), ("210.4.4", False), (".4c", False)],
        ";EXTRUDER_TRAIN.1.INITIAL_TEMPERATURE": [float, (210.4, True), (None, True), ("210.4.4", False), (".4c", False)],
        ";EXTRUDER_TRAIN.0.MATERIAL.VOLUME_USED": [float, (0.1, True), (None, False), (0, False)],
        ";EXTRUDER_TRAIN.1.MATERIAL.VOLUME_USED": [float, (0.1, True), (None, False), (0, False)],
        ";EXTRUDER_TRAIN.0.NOZZLE.DIAMETER": [float, (0.4, True), (0.1, False), (".4c", False)],
        ";EXTRUDER_TRAIN.1.NOZZLE.DIAMETER": [float, (0.4, True), (0.1, False), (".4c", False)],
        ";BUILD_PLATE.INITIAL_TEMPERATURE": [float, (210.4, True), (0.1, True), (None, False), ("210.4.4", False), (".4c", False)],
        ";PRINT.TIME": [int, (5233, True), (None, False), (1, True), (0, False)],
        ";PRINT.SIZE.MIN.X": [float, (10, True), (200, True), (-10, False), (350, False), (None, False)],
        ";PRINT.SIZE.MIN.Y": [float, (10, True), (200, True), (-10, False), (350, False), (None, False)],
        ";PRINT.SIZE.MIN.Z": [float, (10, True), (200, True), (-10, False), (350, False), (None, False)],
        ";PRINT.SIZE.MAX.X": [float, (10, True), (200, True), (-10, False), (350, False), (None, False)],
        ";PRINT.SIZE.MAX.Y": [float, (10, True), (200, True), (-10, False), (350, False), (None, False)],
        ";PRINT.SIZE.MAX.Z": [float, (10, True), (200, True), (-10, False), (350, False), (None, False)],
    }
    FILENAME = "/tmp/test.gcode"

    ## Puts the fake classes in place.
    def setUp(self):
        from griffin.printer.controller import Controller
        Controller._instance = DummyController()
        
    ## Removes the fake classes and generated data.
    def tearDown(self):
        from griffin.printer.controller import Controller
        Controller._instance = None
        os.unlink(self.FILENAME)



    ## generate headers and parse them expecting either an exception or not.
    def testheaders(self):
        ok_header = { key: self.HEADER[key][1][0] for key in self.HEADER.keys() }
        
        self.__writeHeader(ok_header)
        try:
            self.__parseHeader()
        except:
            self.fail("did not expect exception in basic good header test")

        for key_to_be_changed in sorted(self.HEADER.keys()):
            for value, expect_success in self.HEADER[key_to_be_changed][1:]:
                header = copy.deepcopy(ok_header)
                msg = "key_to_be_changed %s good value %s testvalue %s" % (key_to_be_changed, header[key_to_be_changed], value)
                header[key_to_be_changed] = value
                self.__writeHeader(header)
                try:
                    self.__parseHeader()
                except:
                    if expect_success == True:
                        self.fail("did not expect exception (for %s)" % msg)
                else:
                    if expect_success == False:
                        self.fail("did expect exception (for %s)" % msg)

    ## Parses the header, there may be some exceptions raised.
    def __parseHeader(self):
        from griffin.printer.fileHandlers import createFileHandlerFactory
        file_handler_factory = createFileHandlerFactory(self.FILENAME)
        file_handler = file_handler_factory.getFileHandler()
        metadata_handler = file_handler_factory.getMetaDataHandler()
        metadata = metadata_handler.parseMetaData()

    ## write the header to the file.
    def __writeHeader(self, header):
        with open(self.FILENAME, "w") as w:
            w.write(";START_OF_HEADER\n")
            for key in header.keys():
                if header[key] is not None:
                    w.write(key + ":" + str(header[key]) + "\n")
            w.write(";END_OF_HEADER\n")

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(HeaderParseTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
