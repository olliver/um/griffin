import os
import sys
import unittest
import time
import random
import string
import threading
from subprocess import Popen

sys.path.insert(0, os.path.abspath( sys.path[0]+"/.."))
from griffin import dbusif

## TestCase for functions exposed by the SystemService
#
# TODO rewrite to a base class to manage starting / stopping of the processes (including dependencies) (EM-208)
# TODO - when rewrite, capture timeouts/exception
# self._po_system_service.wait(), see https://docs.python.org/3/library/subprocess.html#subprocess.Popen.wait
#
class SystemServiceTestCase(unittest.TestCase):

    ## Start Services (uncomment those needed)
    #
    def setUp(self):
        self._startSystemService()
        #self._startMaterialService()
        #self._startPrinterService()
        self._thread = threading.Thread(target = dbusif.runMainLoop)
        self._thread.daemon = True
        self._thread.start()
        pass

    ## Start the system service
    #
    def _startSystemService(self):
        self._po_system_service = Popen(args=["python3", "main.py", "griffin.system.systemService", "SystemService"])
        time.sleep(1)
        self._system_service = dbusif.RemoteObject("system")

    ## Start the material service
    #
    def _startMaterialService(self):
        self._po_material_service = Popen(args=["python3", "main.py", "griffin.material.materialService", "MaterialService"])
        time.sleep(1)
        self._material_service = dbusif.RemoteObject("material")

    ## Start the printer service
    #
    def _startPrinterService(self):
        self._po_printer_service = Popen(args=["python3", "main.py", "griffin.printer.printerService", "PrinterService"])
        time.sleep(1)
        self._printer_service = dbusif.RemoteObject("printer")

    ## Stop the services (uncomment those needed - should be the same bunch as in setUp)
    #
    def tearDown(self):
        #self._stopPrinterService()
        #self._stopMaterialService()
        self._stopSystemService()
        dbusif.abortMainLoop()
        pass

    ## Stops the printer service
    #
    def _stopPrinterService(self):
        self._printer_service.cleanSignals()
        del(self._printer_service)
        self._printer_service = None
        self._po_printer_service.terminate()

    ## Stops the material service
    #
    def _stopMaterialService(self):
        self._material_service.cleanSignals()
        del(self._material_service)
        self._material_service = None
        self._po_material_service.terminate()

    ## Stops the system service
    #
    def _stopSystemService(self):
        self._system_service.cleanSignals()
        del(self._system_service)
        self._system_service = None
        self._po_system_service.terminate()

    ## Simple test case:  Set the name and get it back -> it should be the same
    #
    def testSetMachineName(self):
        # Arrange
        random_name            = self._name_generator()
        random_minimal_name    = self._name_generator(1)
        random_maximum_name    = self._name_generator(63)
        random_name_too_big    = self._name_generator(64)
        name_start_with_hyphen = '-' + random_name
        name_ends_with_hyphen  = random_name + '-'

        default_name = "Ultimaker-"  + os.uname().nodename[-6:]

        machine_names = [

           # These 2 cannot be tested because these value would only be set when existing name is this (on startup)
           #( None               , default_name ),
           #( "Ultimaker Jedi"   , default_name ),

           ( random_name            , True,   random_name        ),
           ( random_minimal_name    , True,   random_minimal_name),
           ( random_maximum_name    , True,   random_maximum_name),
           ( random_name_too_big    , False,  ""                 ),
           ( name_start_with_hyphen , False,  ""                 ),
           ( name_ends_with_hyphen  , False,  ""                 ),
           ( ""                     , False,  ""                 ),
           ( "μάρκα"                , False,  ""                 ),
           ( "���������"            , False,  ""                 ),
           ( "τυχαίος"              , False,  ""                 ),
           ( "\t\n\t\n"             , False,  ""                 ),
        ]

        # Act
        for test_data in machine_names:
            result = self._system_service.setMachineName(test_data[0])
            # Assert return code
            self.assertEqual(result, test_data[1])

            # Only verify name if it was set
            if (result):
                # Assert machine name set
                machine_name = self._system_service.getMachineName()
                self.assertEqual(machine_name, test_data[2])

    def testSetCountyCode(self):
        codes = [
            # Test setting an empty country, which is accepted as no country set.
            ("", True),
            # NL is valid, while lower case is not. It also needs 2 letters, so USA is wrong while US is poper.
            ("NL", True),
            ("nl", False),
            ("US", True),
            ("USA", False),
            ("DE", True),
            ("A", False),
            # Test if we can go back to no-country after setting it.
            ("", True)
        ]

        # Attach a signal to the system service to catch country changed events.
        self._signaled_country = self._system_service.getCountry()
        self._system_service.connectSignal("countryChanged", self._onCountryChanged)

        # Run each test case and check the result.
        for code, result in codes:
            self.assertEqual(self._system_service.setCountry(code), result)
            if result:
                self.assertEqual(self._system_service.getCountry(), code)
                time.sleep(0.1) # Give some time for the signal to be handled
                self.assertEqual(self._signaled_country, code)

    def _onCountryChanged(self, new_country):
        self._signaled_country = new_country

    ## Generate a random name from the specified characaters of certain length
    #  @param int size The length of the name to be generated
    #  @param string (optional)A string of characters to choose from, defaults to uppercase characters and digits
    #
    def _name_generator(self, size=12, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

## Main entry to create the instance and execute the test(s)
if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(SystemServiceTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
