import unittest
import uuid
import shutil
import tempfile
import logging
import os

from griffin.material.materialDatabase import MaterialDatabase


class MaterialServiceTestCase(unittest.TestCase):
    def setUp(self):
        self.__database = MaterialDatabase()
        self.__temp_path = tempfile.mkdtemp()
        self.__database.setMaterialStoragePath(self.__temp_path)

    def tearDown(self):
        shutil.rmtree(self.__temp_path, ignore_errors=True)
        self.__database = None

    def __bareMinimumXml(self, material, brand="Brand", color="Color", version=0, guid=None, settings=None):
        if guid is None:
            guid = uuid.uuid4()
        if settings is None:
            settings = {
                "print temperature": 210,
                "heated bed temperature": 60,
                "standby temperature": 175,
            }
        if material == "ABS":
            density = 1.25
            diameter = 3.15
        else:
            density = 1.3
            diameter = 2.85
        return """<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<fdmmaterial xmlns=\"http://www.ultimaker.com/material\">
<metadata>
    <name>
        <brand>%s</brand>
        <material>%s</material>
        <color>%s</color>
    </name>
    <GUID>%s</GUID>
    <version>%d</version>
</metadata>
<properties>
  <density>%s</density>
  <diameter>%s</diameter>
</properties>
<settings>
    %s
</settings>
</fdmmaterial>""" % (brand, material, color, guid, version, str(density), str(diameter), "\n".join(map(lambda item: "<setting key=\"%s\">%s</setting>" % (item[0], item[1]), settings.items())))

    def testAddMaterial(self):
        self.assertFalse(self.__database.addNewMaterial(""))
        self.assertEqual(len(os.listdir(self.__temp_path)), 0)

        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("PLA")))
        self.assertEqual(len(os.listdir(self.__temp_path)), 1)
        material = self.__database.getMaterialsByFilter({"material": "PLA"})[0]
        self.assertEqual(material.getProperties()["density"], 1.3)
        self.assertEqual(material.getProperties()["diameter"], 2.85)

        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("ABS")))
        self.assertEqual(len(os.listdir(self.__temp_path)), 2)
        material = self.__database.getMaterialsByFilter({"material": "ABS"})[0]
        self.assertEqual(material.getProperties()["density"], 1.25)
        self.assertEqual(material.getProperties()["diameter"], 3.15)

        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("A\u0394S"))) # Try adding a unicode character instead of a normal ascii
        self.assertEqual(len(os.listdir(self.__temp_path)), 3)
        self.assertFalse(self.__database.addNewMaterial(self.__bareMinimumXml("ERR", guid="10AA"))) #Invalid GUID
        self.assertEqual(len(os.listdir(self.__temp_path)), 3)

    def testParseMaterial(self):
        error_tests = [
            {"print temperature": "bla"}, # invalid value for print temperature
            {"print temperature": ""}, # invalid value for print temperature
            {"hardware compatible": 0}, # invalid value for hardware compatible
            {"hardware compatible": 1}, # invalid value for hardware compatible
            {"hardware compatible": ""}, # invalid value for hardware compatible
            {"processing temperature graph": ""}, # invalid value for processing temperature graph
            {"processing temperature graph": "<point flow=\"2\" temperature=\"180\"/>"}, # invalid value for processing temperature graph, single point is not a graph
            {"print cooling": 101},
            {"print cooling": -1},
        ]
        ok_tests = [
            {"print temperature": 0}, # valid value for print temperature
            {"bla": 0}, # unknown key is a valid condition
            {"processing temperature graph": "<point flow=\"2\" temperature=\"180\"/><point flow=\"5\" temperature=\"200\"/>"}, # valid value for processing temperature graph
            {"print cooling": 0},
            {"print cooling": 50},
            {"print cooling": 100},
            {"hardware compatible": "yes"}, # valid value for hardware compatible
            {"hardware compatible": "no"}, # valid value for hardware compatible
            {"hardware compatible": "unknown"}, # valid value for hardware compatible
        ]
        for test in error_tests:
            with self.subTest(str(test)):
                xml = self.__bareMinimumXml("PLA", settings=test)
                self.assertFalse(self.__database.addNewMaterial(xml), xml)
        for test in ok_tests:
            with self.subTest(str(test)):
                xml = self.__bareMinimumXml("PLA", settings=test)
                self.assertTrue(self.__database.addNewMaterial(xml), xml)

    def testDuplicateGUID(self):
        self.assertEqual(len(os.listdir(self.__temp_path)), 0)
        guid = uuid.uuid4()
        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("PLA", guid=guid, version=0)))
        self.assertEqual(len(os.listdir(self.__temp_path)), 1)
        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("PLA", guid=guid, version=1)))
        self.assertEqual(len(os.listdir(self.__temp_path)), 1)
        self.assertFalse(self.__database.addNewMaterial(self.__bareMinimumXml("PLA", guid=guid, version=0)))
        self.assertEqual(len(os.listdir(self.__temp_path)), 1)

    def testGetMaterial(self):
        guid = uuid.uuid4()
        self.assertEqual(self.__database.getMaterialByGUID(guid), None)
        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("PLA", guid=guid)))
        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("ABS")))
        self.assertIsNotNone(self.__database.getMaterialByGUID(guid))
        self.assertEqual(len(self.__database.getMaterialsByFilter({})), 2)
        self.assertEqual(len(self.__database.getMaterialsByFilter({"material": "CPE"})), 0)
        self.assertEqual(len(self.__database.getMaterialsByFilter({"material": "PLA"})), 1)
        self.assertEqual(len(self.__database.getMaterialsByFilter({"brand": "Brand"})), 2)
        self.assertEqual(len(self.__database.getMaterialsByFilter({"brand": "Brand", "material": "PLA"})), 1)

    def testRemoveMaterial(self):
        guid = uuid.uuid4()
        self.assertEqual(self.__database.getMaterialByGUID(guid), None)
        self.assertTrue(self.__database.addNewMaterial(self.__bareMinimumXml("PLA", guid=guid)))
        self.assertEqual(len(self.__database.getMaterialsByFilter({})), 1)
        self.assertTrue(self.__database.removeMaterialByGUID(guid))
        self.assertEqual(self.__database.getMaterialByGUID(guid), None)
        self.assertEqual(len(self.__database.getMaterialsByFilter({})), 0)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    suite = unittest.TestLoader().loadTestsFromTestCase(MaterialServiceTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
