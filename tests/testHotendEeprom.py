import unittest
import sys
import os
sys.path.insert(0, os.path.abspath( sys.path[0]+"/.."))

class HotendEepromTestCase(unittest.TestCase):

    def setUp(self):
        pass
        
    def tearDown(self):
        pass

    def testOverlap(self):
        from griffin.printer.hotendCartridgeManager.hotendV1 import HotendV1
        h = HotendV1(0, b'bla', bytes(128))
        for key_under_test in h.getValueKeys():
            if "crc8" in key_under_test:
                continue
            #set key to min value (asumed to be all 0's)
            h.setValue(key_under_test, h.getPossibleValues(key_under_test)[0])
            for other_key in  h.getValueKeys():
                if other_key != key_under_test:
                    # set other key to max value (assumed to be all 1's or close to it)
                    if "crc8" in other_key:
                        h.setValue(other_key, None)
                    else:
                        h.setValue(other_key, h.getPossibleValues(other_key)[-1])

                    self.assertEqual(h.getValue(key_under_test), h.getPossibleValues(key_under_test)[0], "item '%s' overlapped with item '%s'" % (key_under_test, other_key) )

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(HotendEepromTestCase)
    unittest.TextTestRunner(verbosity=2).run(suite)
